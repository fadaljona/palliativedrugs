<div id="article_tabs">
<ul>
  <li><a href="#tgeneral">Add/Edit User Types</a></li>
</ul>
<div id="tgeneral">
  <div id="form_container">	
	<form action="<?php echo url::site().'usertypes/usertype_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addusertype">
	<div class="form_description">
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul> 	
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="title" name="title" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['title']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter title.</small></p>
		</li>
		<li>
			<?php
				$marr	=	explode(',',$form['module_permissions']);
			?>
			<label class="description" for="module">Module Permission</label>
			<span>
				<input name="module_user" class="element checkbox" type="checkbox" value="users" <?php if(in_array("users",$marr)){?>checked<? } ?> />
				<label class="choice" for="module_user">Users</label>
				<input name="module_pages" class="element checkbox" type="checkbox" value="pages" <?php if(in_array("pages",$marr)){?>checked<? } ?> />
				<label class="choice" for="module_pages">Pages</label>
				<input name="module_formulary" class="element checkbox" type="checkbox" value="formulary" <?php if(in_array("formulary",$marr)){?>checked<? } ?> />
				<label class="choice" for="module_formulary">Formulary</label>
				<input name="module_members" class="element checkbox" type="checkbox" value="members" <?php if(in_array("members",$marr)){?>checked<? } ?> />
				<label class="choice" for="module_members">Members</label>
				<input name="module_news" class="element checkbox" type="checkbox" value="news" <?php if(in_array("news",$marr)){?>checked<? } ?> />
				<label class="choice" for="module_news">News</label>
				<input name="module_assets" class="element checkbox" type="checkbox" value="assets" <?php if(in_array("assets",$marr)){?>checked<? } ?> />
				<label class="choice" for="module_assets">Assets</label>
			</span> 
		</li>			
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_usertype();">Submit</button>
        </li>
	 </ul>			
	</form>
</div>
</div>
</div>