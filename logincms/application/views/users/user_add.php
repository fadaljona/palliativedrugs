<div id="article_tabs">
<ul>
  <li><a href="#tgeneral">General</a></li>
</ul>
<div id="tgeneral">
<div id="form_container">	
	<form action="<?php echo url::site().'users/user_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="adduser">
	<div class="form_description">
		<h2>Add/Edit User</h2>
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul>
    <li>
      <label class="description" for="element_3">Full name<span class="required">*</span></label>
      <span>
      <input id="user_firstname" name="user_firstname" class="element text" type="text" maxlength="255" value="<?php echo html::specialchars($form['user_firstname']) ?>"/>
      <label class="tam">Firstname</label>
      </span> <span>
      <input id="user_lastname" name="user_lastname" class="element text" type="text" maxlength="255" value="<?php echo html::specialchars($form['user_lastname']) ?>"/>
      <label class="tam">Lastname</label>
      </span>
	  <p class="guidelines"><small>Please enter name.</small></p>
	  </li>
       	
		<li>
			<label class="description" for="email">Email<span class="required">*</span></label>
			<div>
				<input id="user_email" name="user_email" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['user_email']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter email.</small></p>
		</li>
		<li>
			<label class="description" for="user_name">Username<span class="required">*</span></label>
			<div>
				<input id="user_username" name="user_username" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['user_username']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter user name.</small></p>
		</li>
		<li>
			<label class="description" for="password">Password<span class="required">*</span></label>
			<div>
				<input id="user_password" name="user_password" class="element text medium" type="password" maxlength="255" value="<?php echo html::specialchars($form['user_password']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter password.</small></p>
		</li>		
		<li>
			<label class="description" for="password">Re-Type Password<span class="required">*</span></label>
			<div>
				<input id="re_password" name="re_password" class="element text medium" type="password" maxlength="255" value="<?php echo html::specialchars($form['user_password']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter password.</small></p>
		</li>		
		<li>
			<label class="description" for="dept">User Type<span class="required">*</span></label>
			<div>
				<select name="type_id" id="type_id" class="element select medium">
					<option value="">-Select Type-</option>
					 <?php foreach ($types as $type): ?>
					 <option value="<?php echo html::specialchars($type->type_id) ?>" <?php if($form['type_id'] == $type->type_id){?>selected<? } ?>><?php echo html::specialchars($type->title) ?></option>
					 <?php endforeach ?>
				</select>
			</div> 
			<p class="guidelines"><small>Please select type.</small></p>
		</li>
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_user();">Submit</button>
        </li>
	 </ul>			
	</form>
</div>
</div>
</div>