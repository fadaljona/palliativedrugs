<div id="form_container">
  <div class="form_description">
    <h2>Add/Edit Member</h2>
    <p class="error">
    <ul>
      <?php foreach ($errors as $error): ?>
      <li><?php echo $error ?></li>
      <?php endforeach ?>
    </ul>
    </p>
  </div>  
  <div id="article_tabs">
  <ul>
    <li><a href="#tcontact">Contact Details</a></li>
    <li><a href="#torganis">Organisation Details</a></li>
    <li><a href="#tlogin">Login and Account Details</a></li>
    <li><a href="#tdemo">Demographic Details</a></li>
  </ul>
  <form action="<?php echo url::site().'members/member_add'?>" class="appnitro" enctype="multipart/form-data" method="post" name="addmember" id="addmember">
  <input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
    <div id="tcontact">
      <ul>
        <li>
		  <label class="description" for="element_3">Full name<span class="required">*</span></label>
		  <span>
			<select name="title" id="title" class="element select">
				<option value="">Please select</option>
				<option value="Professor" <? if($form['title']== "Professor"){?>selected<? } ?>>Professor</option>
				<option value="Doctor"  <? if($form['title']== "Doctor"){?>selected<? } ?>>Doctor</option>
				<option value="Mr"  <? if($form['title']== "Mr"){?>selected<? } ?>>Mr</option>
				<option value="Mrs"  <? if($form['title']== "Mrs"){?>selected<? } ?>>Mrs</option>
				<option value="Ms"  <? if($form['title']== "Ms"){?>selected<? } ?>>Ms</option>
				<option value="Miss"  <? if($form['title']== "Miss"){?>selected<? } ?>>Miss</option>
				<option value="Sister"  <? if($form['title']== "Sister"){?>selected<? } ?>>Sister</option>
			</select>
		  <label class="tam">Title</label>
		  </span>
		  <span>
		  <input id="FirstName" name="FirstName" class="element text" type="text" maxlength="255" value="<?php echo html::specialchars($form['FirstName']) ?>"/>
		  <label class="tam">Firstname</label>
		  </span> <span>
		  <input id="LastName" name="LastName" class="element text" type="text" maxlength="255" value="<?php echo html::specialchars($form['LastName']) ?>"/>
		  <label class="tam">Lastname</label>
		  </span>
		  <p class="guidelines"><small>Please enter name.</small></p>
        </li> 
		<li>
		  <label class="description" for="title">Professional group</label>
		  <div>
			<input id="role" name="role" class="element text small" type="text" maxlength="255" value="<?php echo html::specialchars($form['role']) ?>" />
			<select name="sel_role" id="sel_role" class="element select medium" onchange="$('#role').val(this.value);">
				<option value="">Please select</option>
				<?php foreach ($roles as $role): ?>
				 <option value="<?php echo html::specialchars($role->role) ?>" <?php if($form['role'] == $role->role){?>selected<? } ?>><?php echo html::specialchars($role->role) ?></option>
				 <?php endforeach ?>					
			</select>
		  </div>
		  <p class="guidelines"><small>Please select group.</small></p>				 
		</li>
		<li>
		  <label class="description" for="regno">Reg Number</label>
		  <div>
			<input id="prof_reg_num" name="prof_reg_num" class="element text medium" type="text" maxlength="255" value="<?=$form['prof_reg_num']?>" />					
		  </div>
		  <p class="guidelines"><small>Please enter reg number.</small></p>				 
		</li>
		<li>
		  <label class="description" for="title">Nurse Prescriber</label>
		  <span><input type="radio" name="nurse_prescriber" id="nurse_prescriber" value="1" <?php if($form['nurse_prescriber']=="1" || $form['nurse_prescriber']==""){?>CHECKED<?php } ?>><label class="choice" for="status">Yes</label><input type="radio" name="nurse_prescriber" value="0" <?php if($form['nurse_prescriber']=="0"){?>CHECKED <?php } ?>><label class="choice" for="status">No</label></span>					  			 
		</li>
		<li>
		  <label class="description" for="title">Primary or main specialty</label>
		  <div>
			<input id="speciality" name="speciality" class="element text small" type="text" maxlength="255" value="<?=$form['speciality']?>" />
			<select name="sel_special" id="sel_special" class="element select medium" onchange="$('#speciality').val(this.value);">
				<option value="">Please select</option>
				<?php foreach ($speciality as $spec): ?>
				 <option value="<?php echo html::specialchars($spec->speciality) ?>" <?php if($form['speciality'] == $spec->speciality){?>selected<? } ?>><?php echo html::specialchars($spec->speciality) ?></option>
				 <?php endforeach ?>	
			</select>
		  </div>
		  <p class="guidelines"><small>Please select speciality.</small></p>				 
		</li>
		<li>
		  <label class="description" for="title">Sub specialty</label>
		  <div>
			<input type="text" name="sub_speciality" id="sub_speciality" class="element text small" maxlength="200" value="<?=$form['sub_speciality']?>">
			<select name="sel_subspecial" id="sel_subspecial" class="element select medium" onchange="$('#sub_specialty').val(this.value);">
				<option value="n/a">None</option>
				<?php foreach ($sub_spec as $sspec): ?>
				 <option value="<?php echo html::specialchars($sspec->sub_specialty)?>" <?php if($form['sub_speciality']==$sspec->sub_specialty){?>selected<? } ?>><?php echo html::specialchars($sspec->sub_specialty) ?></option>
				 <?php endforeach ?>
			</select> 				 
		  </div>
		  <p class="guidelines"><small>Please select sub speciality.</small></p>				 
		</li>		        
		  <li class="buttons">
			<a class="next-tab button" rel="1">Next</a>
		  </li>
      </ul>
    </div>
    <div id="torganis">
      <ul>
        <li>
		  <label class="description" for="title">Organisation name</label>
		  <div>
			<input id="organisation" name="organisation" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['organisation'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter organisation name.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Address1</label>
		  <div>
			<input id="addr1" name="addr1" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['addr1'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter address.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Address2</label>
		  <div>
			<input id="addr2" name="addr2" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['addr2'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter address.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Address3</label>
		  <div>
			<input id="addr3" name="addr3" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['addr3'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter address.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Address4</label>
		  <div>
			<input id="addr4" name="addr4" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['addr4'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter address.</small></p>
		</li>
		<li>
		  <label class="description" for="title">City</label>
		  <div>
			<input id="TownOrCity" name="TownOrCity" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['TownOrCity']);?>" />
		  </div>
		  <p class="guidelines"><small>Please enter city.</small></p>
		</li>
		<li>
		  <label class="description" for="title">State/Province/County</label>
		  <div>
			<input id="StateOrCounty" name="StateOrCounty" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['StateOrCounty'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter state.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Zip/Postal Code</label>
		  <div>
			<input id="PostalCode" name="PostalCode" class="element text medium" type="text" maxlength="255" value="<?=$form['PostalCode']?>" />
		  </div>
		  <p class="guidelines"><small>Please enter zipcode.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Country</label>
		  <div>
			<select name="Country" id="Country" class="element select">
				<option value="">Please select</option>
				<?php foreach ($country as $cy): ?>
				 <option value="<?php echo html::specialchars($cy->name) ?>" <?php if($form['Country'] == $cy->name){?>selected<? } ?>><?php echo html::specialchars($cy->name) ?></option>
				 <?php endforeach ?>
			</select>
		  </div>
		  <p class="guidelines"><small>Please select country.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Telephone Number (Inc. International/Area code)</label>
		  <div>
			<input id="phone" name="phone" class="element text medium" type="text" maxlength="255" value="<?=$form['phone']?>" />
		  </div>
		  <p class="guidelines"><small>Please enter phone number.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Main work place</label>
		  <div><input type="text" name="main_workplace" id="main_workplace" class="element text small" maxlength="200" value="<?=stripslashes($form['main_workplace'])?>">
			<select id="sel_mworkplace" name="sel_mworkplace" class="element select medium" onchange="$('#txt_mworkplace').val(this.value);">
				<option value="">Please select</option>
				<?php foreach ($place as $pl): ?>
				 <option value="<?php echo html::specialchars($pl->main_workplace) ?>" <?php if($form['main_workplace'] == $pl->main_workplace){?>selected<? } ?>><?php echo html::specialchars($pl->main_workplace) ?></option>
				 <?php endforeach ?>
			</select>	
		  </div>
		  <p class="guidelines"><small>Please select main workplace.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Current post/position</label>
		  <div>	<input type="text" name="current_post" id="current_post" class="element text small" maxlength="200" value="<?=stripslashes($form['current_post'])?>">
			<select id="sel_position" name="sel_position" class="element select medium" onchange="$('#current_post').val(this.value);">
				<option value="">Please select</option>
				<?php foreach ($post as $pl): ?>
				 <option value="<?php echo html::specialchars($pl->current_post) ?>" <?php if($form['current_post'] == $pl->current_post){?>selected<? } ?>><?php echo html::specialchars($pl->current_post) ?></option>
				 <?php endforeach ?>
			</select>	
		  </div>
		  <p class="guidelines"><small>Please select position.</small></p>
		</li>
		<li class="buttons">
			<a class="next-tab button" rel="2">Next</a>
		  </li>
      </ul>
    </div>
    <div id="tlogin">
      <ul>
        <li id="li_1" >
		  <label class="description" for="title">User Name</label>
		  <div>
			<input id="username" name="username" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['username'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter user name.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Email</label>
		  <div>
			<input id="email" name="email" class="element text medium" type="text" maxlength="255" value="<?=stripslashes($form['email'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter email.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Password</label>
		  <div>
			<input id="pwd" name="pwd" class="element text medium" type="password" maxlength="255" value="<?=stripslashes($form['pwd'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter password.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Re-enter Password</label>
		  <div>
			<input id="txt_rpwd" name="txt_rpwd" class="element text medium" type="password" maxlength="255" value="<?=stripslashes($form['pwd'])?>" />
		  </div>
		  <p class="guidelines"><small>Please enter password.</small></p>
		</li>
		<li>
			<label class="description" for="duration">Accept e-mails relating to site updates</label>		
			<span><input type="radio" name="accept_site_emails" id="accept_site_emails" value="1" <?php if($form['accept_site_emails']=="1" || $form['accept_site_emails']==""){?>CHECKED<?php } ?>><label class="choice" for="status">Yes</label><input type="radio" name="accept_site_emails" value="0" <?php if($form['accept_site_emails']=="0"){?>CHECKED <?php } ?>><label class="choice" for="status">No</label></span>				   
		</li>
		<li>
			<label class="description" for="duration">Accept bulletin board digest e-mails</label>		
			<span><input type="radio" name="accept_bb_emails" id="accept_bb_emails" value="1" <?php if($form['accept_bb_emails']=="1" || $form['accept_bb_emails']==""){?>CHECKED<?php } ?>><label class="choice" for="status">Yes</label><input type="radio" name="accept_bb_emails" value="0" <?php if($form['accept_bb_emails']=="0"){?>CHECKED <?php } ?>><label class="choice" for="status">No</label></span>				   
		</li>
		<li class="buttons">
			<a class="next-tab button" rel="3">Next</a>
		  </li>
      </ul>
    </div>
    <div id="tdemo">
      <ul>  
		 <li>
		  <label class="description" for="title">Years in current post</label>
		  <div>
			<select id="years_in_post" name="years_in_post" class="element select">
				<option value="">Please select</option>
				<?php for($k=0;$k<=50;$k++){?>
				 <option value="<?=$k?>" <?php if($form['years_in_post']==$k){?>selected<?php } ?>><?=$k?></option>
				<?php } ?>
			</select>			 
		  </div>	
		  <p class="guidelines"><small>Please select years.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Time spent in direct patient care</label>
		  <div>
			<select id="patient_care_time" name="patient_care_time" class="element select small">
				<option value="">Please select</option>
				<option value="1" <?php if($form['patient_care_time']=="1"){?>selected<?php } ?>><25%</option>  
				<option value="2" <?php if($form['patient_care_time']=="2"){?>selected<?php } ?>>25-50%</option>
				<option value="3" <?php if($form['patient_care_time']=="3"){?>selected<?php } ?>>51-75%</option>
				<option value="4" <?php if($form['patient_care_time']=="4"){?>selected<?php } ?>>>75%</option>
			</select>
		  </div>	
		   <p class="guidelines"><small>Please select time.</small></p>
		</li>
		<li>
		  <label class="description" for="title">Gender</label>
		  <div>
			<select id="gender" name="gender" class="element select small">
				<option value="">please select</option>
				<option value="M" <?php if($form['gender']=="M"){?>selected<?php } ?>>Male</option>
				<option value="F" <?php if($form['gender']=="F"){?>selected<?php } ?>>Female</option>
			</select>
		  </div>					 
		</li>
		<li id="li_2" >
		  <label class="description" for="title">Year of birth</label>
		  <div>
			<select id="year_of_birth" name="year_of_birth" class="element select small">
				<option value="">please select</option>
				<?php for($k=1938;$k<=1995;$k++){?>
				 <option value="<?=$k?>" <?php if($form['year_of_birth']==$k){?>selected<? } ?> ><?=$k?></option>
				<?php } ?>
			</select>
		  </div>					 
		</li>
		<li>
		   <label class="description" for="title">Heared By</label>
		   <div>
			<select id="lead" name="lead" class="element select">	
				<option value="none">Please select</option>
				<option value="web search" <?php if($form['lead']=="web search"){?>selected<? } ?>>web search</option>
				<option value="word of mouth" <?php if($form['lead']=="word of mouth"){?>selected<? } ?>>word of mouth</option>
				<option value="book version" <?php if($form['lead']=="book version"){?>selected<? } ?>>book version</option>
				<option value="CD ROM version" <?php if($form['lead']=="CD ROM version"){?>selected<? } ?>>CD ROM version</option>
				<option value="mail shot" <?php if($form['lead']=="mail shot"){?>selected<? } ?>>mail shot</option>
				<option value="magazine advert" <?php if($form['lead']=="magazine advert"){?>selected<? } ?>>magazine advert</option>
				<option value="conference" <?php if($form['lead']=="conference"){?>selected<? } ?>>conference</option>
				<option value="link from other website" <?php if($form['lead']=="link from other website"){?>selected<? } ?>>link from other website</option>
				<option value="other" <?php if($form['lead']=="other"){?>selected<? } ?>>Others</option>
			  </select>
		   </div>
		</li>
		<li>
			<label class="description" for="duration">Status</label>		
			<span><input type="radio" name="active" id="active" value="1" <?php if($form['active']=="1" || $form['active']==""){?>CHECKED<?php } ?>><label class="choice" for="status">Active</label><input type="radio" name="active" value="0" <?php if($form['active']=="0"){?>CHECKED <?php } ?>><label class="choice" for="status">InActive</label></span>				   
		</li>
        <li class="buttons">
          <a id="saveForm" class="button" onclick="validate_member();">Submit</a>
        </li>
      </ul>
    </div>
  </form>
</div>
</div>