<div id="article_tabs">
<ul>
  <li><a href="#tgeneral">Add/Edit Asset Category</a></li>
</ul>
<div id="tgeneral">
  <div id="form_container">	
	<form action="<?php echo url::site().'assetcat/assetcat_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addassetcat">
	<div class="form_description">	
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul> 	
		<li>
			<label class="description" for="first_name">Category Title<span class="required">*</span></label>
			<div>
				<input id="asset_category_title" name="asset_category_title" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['asset_category_title']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter title.</small></p>
		</li>		
		<li>
			<label class="description" for="type">Type<span class="required">*</span></label>
			<div>
				<select id="asset_category_type" name="asset_category_type" class="element select small"> 
					<option value='audio' <?php if($form['asset_category_type']=='audio'){?>selected<? } ?>>Audio</option>
					<option value='documents' <?php if($form['asset_category_type']=='documents'){?>selected<? } ?>>Documents</option>
					<option value='images' <?php if($form['asset_category_type']=='images'){?>selected<? } ?>>Images</option>
					<option value='video' <?php if($form['asset_category_type']=='video'){?>selected<? } ?>>Video</option>
				</select>
			</div> 
			<p class="guidelines"><small>Please select type.</small></p>
		</li>		
		<li>
			<label class="description" for="last_name">Description</label>
			<div>
				<textarea id="asset_category_description" name="asset_category_description" class="element textarea medium"><?php echo html::specialchars($form['asset_category_description']) ?></textarea> 
			</div> 
			<p class="guidelines"><small>Please enter description.</small></p>
		</li>		
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_assetcat();">Submit</button>
        </li>
	 </ul>			
	</form>
</div>
</div>
</div>