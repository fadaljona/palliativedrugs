<div id="article_tabs">
<ul>
  <li><a href="#tgeneral">Add/Edit Assets</a></li>
</ul>
<div id="tgeneral">
  <div id="form_container">	
	<form action="<?php echo url::site().'assets/asset_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addasset">
	<div class="form_description">		
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul> 
		<li>
			<label class="description" for="categ">Category<span class="required">*</span></label>
			<div>
				<select name="cat_id" id="cat_id" class="element select medium">
					<option value="">-Select Category-</option>
					 <?php foreach ($assetcats as $cat): ?>
					 <option value="<?php echo html::specialchars($cat->asset_category_id) ?>" <?php if($form['asset_category_id'] == $cat->asset_category_id){?>selected<? } ?>><?php echo html::specialchars($cat->asset_category_title) ?></option>
					 <?php endforeach ?>
				</select>
			</div> 
			<p class="guidelines"><small>Please select level.</small></p>
		</li>
		<li>
			<label class="description" for="asset_title">Asset Title<span class="required">*</span></label>
			<div>
				<input id="asset_title" name="asset_title" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['asset_title']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter asset title.</small></p>
		</li>		
		<li>
			<label class="description" for="asset_name">Asset</label>
			<div>
				<input id="asset_name" name="asset_name" class="element file" type="file"/><img src='/assets/thumbs/<?=$form['asset_category_id']?>/<?=$form['asset_name']?>' border='0' /> 
			</div> 
			<p class="guidelines"><small>Please select asset.</small></p>
		</li>
		<li>
			<label class="description" for="asset_alt">Asset Alt<span class="required">*</span></label>
			<div>
				<input id="asset_alt" name="asset_alt" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['asset_alt']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter asset alt.</small></p>
		</li>
		<li>
			<label class="description" for="asset_height">Asset Height</label>
			<div>
				<input id="asset_height" name="asset_height" class="element text medium" type="text" maxlength="255" disabled="disabled" value="<?php echo html::specialchars($form['asset_height']) ?>"/> 
			</div> 			
		</li>
		<li>
			<label class="description" for="asset_width">Asset Width</label>
			<div>
				<input id="asset_width" name="asset_width" class="element text medium" type="text" maxlength="255" disabled="disabled" value="<?php echo html::specialchars($form['asset_width']) ?>"/> 
			</div> 			
		</li>				
		<li>
			<label class="description" for="asset_size">Asset Size</label>
			<div>
				<input id="asset_size" name="asset_size" class="element text medium" type="text" maxlength="255" disabled="disabled" value="<?php echo html::specialchars($form['asset_size']) ?>"/> 
			</div> 			
		</li>				
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_asset();">Submit</button>
        </li>
	 </ul>			
	</form>
 </div>
 </div>
</div>