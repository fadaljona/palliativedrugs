<div id="form_container">
  	<div class="form_description">
	  <h2>Add/Edit News</h2>
	   <p class="error">
		<ul>
		  <?php foreach ($errors as $error): ?>
		  <li><?php echo $error ?></li>
		  <?php endforeach ?>
		</ul>
		</p> 
	</div>
		<div id="article_tabs">
		<ul>
		  <li><a href="#tgeneral">General</a></li>      
		  <li><a href="#tcontent">Content</a></li>      
		  <li><a href="#tlinks">Links</a></li>      
		</ul>		
		<form action="<?php echo url::site().'news/news_add'?>" class="appnitro" enctype="multipart/form-data" method="post" name="addnews" id="addnews">
		 <input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />		
		<div id="tgeneral">
		<ul>
		  <li>
			<label class="description" for="title">News Title<span class="required">*</span></label>
			<div>
			  <input id="title" name="title" class="element text large" type="text" maxlength="255" value="<?php echo html::specialchars($form['title']) ?>"/>
			</div>
			<p class="guidelines"><small>Please enter title.</small></p>
		  </li>	
		  <li>
			<label class="description" for="atitle">Alias Title</label>
			<div>
			  <input id="alias_title" name="alias_title" class="element text large" type="text" maxlength="255" value="<?php echo html::specialchars($form['alias_title']) ?>"/>
			</div>
			<p class="guidelines"><small>Please enter alias title.</small></p>
		  </li>		  
		  <li>
			<label class="description" for="type">News Type<span class="required">*</span></label>
			<div>
			  <select id='type' name='type' class="element select small">
				<option value="addition" <?php if($form['type']=="addition"){?>selected <? } ?>>Addition</option>
				<option value="news" <?php if($form['type']=="news"){?>selected <? } ?>>News</option>                
				<option value="press" <?php if($form['type']=="press"){?>selected <? } ?>>Press</option>                
			  </select>
			</div>
		  </li>        
		  <li>
			<label class="description" for="category">Category<span class="required">*</span></label>
			<div>
			  <select id='category' name='category' class="element select small">
				<option value="monograph" <? if($form['category'] =="monograph"){?>selected<? } ?>>Monograph</option>
				<option value="pall" <? if($form['category'] =="pall"){?>selected<? } ?>>Pall</option>
				<option value="hot" <? if($form['category'] =="hot"){?>selected<? } ?>>Hot</option>
				<option value="drug" <? if($form['category'] =="drug"){?>selected<? } ?>>Drug</option>
				<option value="survey" <? if($form['category'] =="survey"){?>selected<? } ?>>Survey</option>
				<option value="doclib" <? if($form['category'] =="doclib"){?>selected<? } ?>>Doclib</option>           
			  </select>
			</div>
		  </li> 
		  <li class='schedule'>
			<label class="description" for="start_date">News Date</label>
			<div>
			  <input id="news_date" name="news_date" class="element text small" type="text" maxlength="255" value="<?php echo html::specialchars($form['news_date']) ?>"/>
			</div>
			<p class="guidelines"><small>Please enter news date.</small></p>			
		  </li>	
		  <li>
			<label class="description" for="status">Status </label>
			<span>
			<input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
			<label class="choice" for="status">InActive</label>
			</span> 
			</li>
		  </ul>
		</div>
		<div id="tcontent">
		 <ul>
			<li>
			  <label class="description" for="himage">News Image</label>
			  <div class="buttons">
				<?php 	$obj1	=	new Selectpattern();
					echo $obj1->Select("yes","images",'no','addnews','news-shortimage');?><a href="javascript:;"  onclick="assignImage()">Assign</a>
			  </div>
			  <p class="guidelines"><small>Please select news image.</small></p>
			</li>	
			  <li>
				<label class="description" for="short_text">Short Text</label>
				<div>
				  <textarea id="short_text" name="short_text" class="mceEditor-small"><?php echo html::specialchars($form['short_text']) ?></textarea>
				</div>
				<p class="guidelines"><small>Please enter short text.</small></p>
			  </li>
			 <li>
				<label class="description" for="content">Content<span class="required">*</span></label>
				<div>
				  <textarea id="content" name="content" class="mceEditor"><?php echo html::specialchars($form['content']) ?></textarea>
				</div>
				<p class="guidelines"><small>Please enter long text.</small></p>
			  </li>
			</ul>
		  </div>
		  <div id="tlinks">
			<ul>
			  <li>
				<label class="description" for="ndown">News Download</label>
				  <div class="buttons">
					<?php echo $obj1->Select("yes","assets",'no','addnews','news-downloads');?> 
				  </div>
				  <p class="guidelines"><small>Please select news downloads.</small></p>
				</li>
				 <li>
					<label class="description" for="dlinktext">Link Text</label>
					<div>
					  <input type="text" id="link_text" name="link_text" value="<?php echo html::specialchars($form['link_text']) ?>" class="element text large">
					</div>
					<p class="guidelines"><small>Please enter link text.</small></p>
				  </li>
				   <li>
					<label class="description" for="url">Tags</label>
					<div>
					  <input type="text" id="news_tags" name="news_tags" value="<?php echo html::specialchars($form['news_tags']) ?>" class="element text large">
					</div>			
				  </li>
				  <li>
					<label class="description" for="related_items">Related Items</label>
					<div>
					  <input type="text" id="related_items" name="telated_items" value="" class="element text medium"><a rel="facebox" href="/logincms/includes/select_related.php" class="button">Select</a><a class="button" onclick="clear_items();">Clear</a>
					</div>
					<input type="hidden" name="related_pages" id="related_pages" value="<?=$form['related_pages'];?>" />
					<input type="hidden" name="related_news" id="related_news" value="<?=$form['related_news'];?>" />			
					<span id='span_page'><?php if($form['span_pages']!=''){echo '<label>Pages</label>'.$form['span_pages'];}?></span>
					<br/>			
					<span id='span_news'><?php if($form['span_news']!=''){echo '<label>News</label>'.$form['span_news'];}?></span>
				  </li>
				  <li>
					<label class="description" for="url">URL</label>
					<div>
					  <textarea id="url" name="url" class="element textarea medium"><?php echo html::specialchars($form['url']) ?></textarea>
					</div>
					<p class="guidelines"><small>Please enter url.</small></p>
				  </li>
				  <li>
					<label class="description" for="links">Link</label>
					<div>
					  <input type="text" id="link" name="link" value="<?php echo html::specialchars($form['link']) ?>" class="element text large">
					</div>
					<p class="guidelines"><small>Please enter link.</small></p>
				  </li>		  
				  <li class="buttons">
					<button id="savePage" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_news();">Submit</button>
				  </li>
				</ul>
			</div>
		</form>	  		
	</div>					
</div>
<script>
	window.onload=function(){
		$('#news_date').datepicker({dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:true}); 
	}
	
	<?	$allpid	=	$form['related_pages'];
		$pid	=	explode(',',$allpid);
		for($i=0;$i<count($pid);$i++){
			if($pid[$i]!=''){
	?>
	pageSelected[<?=$i?>] =  '<?=$pid[$i]?>';
	<? 		}
		} 
		$allnid	=	$form['related_news'];
		$nid	=	explode(',',$allnid);
		for($i=0;$i<count($nid);$i++){
			if($nid[$i]!=''){
	?>
	newsSelected[<?=$i?>] =  '<?=$nid[$i]?>';
	<? 		}
		}
		$allpname	=	$form['span_pages'];
		$pnam	=	explode(',',$allpname);
		for($i=0;$i<count($pnam);$i++){
			if($pnam[$i]!=''){
	?>
	pageNames[<?=$i?>] =  '<?=$pnam[$i]?>';
	<? 		}
		} 
		$allnname	=	$form['span_news'];
		$nnam	=	explode(',',$allnname);
		for($i=0;$i<count($nnam);$i++){
			if($nnam[$i]!=''){
	?>
	newsNames[<?=$i?>] =  '<?=$nnam[$i]?>';
	<? 		}
		} 
	?>
	$('#news_downloadsid').val('<?=$form['news_download']?>');
	$('#file_names2').html('<?=$form['span_download']?>');
</script>
      
