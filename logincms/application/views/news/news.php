<div id="filters">
  <h3><a href="#">Filters</a></h3>
  <div>	
  <input type='hidden' name='search_type' id='search_type' value='A'> 
  <ul id='news_type'>
	  <li class="filtertitle">By News Type</li>
	  <li id='fcon_A'><a class='selected' href="javascript:;" onclick="show_news('A');">Any / All</a></li>
	  <li id='fcon_news'><a href="javascript:;" onclick="show_news('news');">News</a></li>
	  <li id='fcon_addition'><a href="javascript:;" onclick="show_news('addition');">Addition</a></li>
	  <li id='fcon_press'><a href="javascript:;" onclick="show_news('press');">Press</a></li>		
  </ul>
 </div>
</div>
<div id="activitytabs">
  <ul>
	<li><a href="#newslisting">News Listings</a></li>
  </ul>
   <div id="newslisting"> 
	<table class='display' id="newslist" cellspacing="0" cellpadding="0">
	   <col id="colname" />
	   <col id="colstart" />
	   <col id="colcategory" />
	   <col id="colstatus" />
	   <col id="colactions" />
	   <thead>
		<tr>
		  <th>Title</th>
		  <th>Date</th>
		  <th>Type</th>
		  <th>Category</th>
		  <th>Status</th>
		  <th>Action</th>
		</tr>
		</thead>	
		<tbody>
		</tbody>	
	</table>
 </div>
</div>