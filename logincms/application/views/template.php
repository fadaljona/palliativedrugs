<?php  $urlaction		=	'';
	   if($_SERVER['REQUEST_URI']=='/logincms/'){
			$urlvar		= 'home';
	   }else{
			$pathinfo	= $_SERVER['PATH_INFO'];
			$exp		= explode("/",$pathinfo);					
			$urlvar		= $exp[1];
			if(!empty($exp[2]))
				$urlaction	= $exp[2];	
	   }

	   require_once("../siteconfig/classes/select-pattern.class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title><?php echo html::specialchars($title) ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link href="/min/g=Main-CMS-CSS" rel="stylesheet" type="text/css" media="screen">
<link href="/logincms/assets/css/temp.css" rel="stylesheet" type="text/css" media="screen">

<?php 
/*if($urlvar=='members'){
	echo html::stylesheet(array('assets/css/TableTools',),array('screen',));
} 


<script src="http://cdn.jquerytools.org/1.2.0/full/jquery.tools.min.js"></script>
<script src="http://layout.jquery-dev.net/lib/js/jquery.layout-latest.js" type="text/javascript" charset="utf-8"></script>
*/?>
<script type="text/javascript" src="/min/g=JSGroup1"></script>
<?php if($urlvar=='members'){
	echo html::script(array('assets/ZeroClipboard/ZeroClipboard.js',
							'assets/js/TableTools.js',
							));
 }if($urlvar!='home'){ 
	echo html::script(array('assets/js/functions_'.$urlvar.'.js'));
 }
?>
<script type="text/javascript" src="/min/g=JSGroup2"></script>                              
<?php echo html::script(array('tiny_mce/jquery.tinymce.js','tiny_mce/tiny_mce_gzip.js'));?>

<script type="text/javascript">	
jQuery(document).ready(function($) {
	  $('a[rel*=facebox]').facebox({
		loading_image : 'loading.gif',
		close_image   : 'closelabel.gif'
	  });
      
      $("#side-nav dt a").click(function(){
			$("dd:visible").slideUp("slow");
			$(this).parent().next().slideDown("fast");
			return false;
	 });
	 
     $('.dashdate').cuteTime();	
	
})

tinyMCE_GZ.init({
	plugins : 'style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,'+ 
        'searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras',
	themes : 'simple,advanced',
	languages : 'en',
	disk_cache : true,
	debug : false
});

tinyMCE.init({
	mode : "specific_textareas",
	theme : "simple",
	content_css : "/logincms/assets/css/formulary.css",
	width : "900",	
	height: "150",
	body_class : "container",
	entity_encoding : "raw",
	editor_selector : "mceSimple"
});
tinyMCE.init({
		mode : "specific_textareas",
		theme : "advanced",
		editor_selector : "mceEditor",
		width:"900",	
		height:"500",
		relative_urls : true, 
		remove_script_host : false, 
		document_base_url : "http://pd2016.palliativedrugs.com/",
		entity_encoding : "named", 
		convert_urls : false,
		plugins : "safari,spellchecker,style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,pagebreak,table",
		theme_advanced_buttons1_add_before : "save,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect,emotions,iespell",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "spellchecker,separator,styleprops,|,template,blockquote,|,insertfile,insertimage,",
		theme_advanced_buttons3_add : "tablecontrols",
		table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
		table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
		table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
		table_cell_limit : 100,
		table_row_limit : 5,
		table_col_limit : 5,
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		content_css : "/logincms/assets/css/formulary.css",
		body_class : "container",
		plugin_insertdate_dateFormat : "%Y-%m-%d",
		plugin_insertdate_timeFormat : "%H:%M:%S",
		external_link_list_url : "example_data/example_link_list.js",
		external_image_list_url : "example_data/example_image_list.js",
		flash_external_list_url : "example_data/example_flash_list.js",
		template_external_list_url : "example_data/example_template_list.js",
		theme_advanced_resize_horizontal : false,
		theme_advanced_resizing : true,
		apply_source_formatting : true,
		extended_valid_elements : "ol[start|type]",
		spellchecker_languages : "+English=en"
});
tinyMCE.init({
		mode : "specific_textareas",
		theme : "advanced",
		editor_selector : "mceEditor-small",
		width:"900",	
		height:"200",
		relative_urls : true, 
		remove_script_host : false, 
		document_base_url : "http://palliativedrugs.com/",
		entity_encoding : "named", 
		convert_urls : false,
		plugins : "safari,spellchecker,style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,pagebreak,table",
		theme_advanced_buttons1_add_before : "save,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect,emotions,iespell",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "spellchecker,separator,styleprops,|,template,blockquote,|,insertfile,insertimage,",
		theme_advanced_buttons3_add : "tablecontrols",
		table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
		table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
		table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
		table_cell_limit : 100,
		table_row_limit : 5,
		table_col_limit : 5,
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		content_css : "/logincms/assets/css/formulary.css",
		body_class : "container",
		plugin_insertdate_dateFormat : "%Y-%m-%d",
		plugin_insertdate_timeFormat : "%H:%M:%S",
		external_link_list_url : "example_data/example_link_list.js",
		external_image_list_url : "example_data/example_image_list.js",
		flash_external_list_url : "example_data/example_flash_list.js",
		template_external_list_url : "example_data/example_template_list.js",
		theme_advanced_resize_horizontal : false,
		theme_advanced_resizing : true,
		apply_source_formatting : true,
		extended_valid_elements : "ol[start|type]",
		spellchecker_languages : "+English=en"
});
</script>
<!--[if IE]>
<style type="text/css">
#nav ul {display:inline-block;}
#nav ul {display:inline;}
#nav ul li {float:left;}
#nav {text-align:center;}
</style>
<![endif]-->
</head>

<body class="<?=$urlvar?>">
<div id="container">
	<div class="ui-layout-north" id="mastheadnavication">
	<div class="ui-layout-north" id='mastheadnavication'>
      <div class="super">
		<p>Logged in as <?php echo $_SESSION['fullname'];?> <span class="seperate">|</span><? echo date('l, jS F, Y'); ?><span class="seperate">|</span><a href="<?php echo url::site();?>login/logout"> Logout</a></p>
	  </div>
	  <h1>Palliative Drugs CMS V3.5b</h1>
	  <div id="featureTabsContainer">
	  <?php  $k			= 0;
		foreach ($links as $link => $url): 					
	  ?>
			<a href="<?php echo url::site().$url;?>" class="tab 
					<?php  if(($urlvar=='users' || $urlvar=='usertypes')&&($url=='users')){
						   	  echo "tab_selected";
						   }else if(($urlvar=='pages' || $urlvar=='articles' || $urlvar=='templates' || $urlvar=='language')&&($url=='pages')){
						   	  echo "tab_selected";	
						   }else if(($urlvar=='formulary' || $urlvar=='pubmed' || $urlvar=='snippets' || $urlvar=='drugs' )&&($url=='formulary')){
						   	  echo "tab_selected";
						   }else if(($urlvar=='members')&&($url=='members')){
						   	  echo "tab_selected";
						   }else if(($urlvar=='news')&&($url=='news')){
						   	  echo "tab_selected";						  
					       }else if(($urlvar=='assets' || $urlvar=='assetcat')&&($url=='assets')){
						   	  echo "tab_selected";
						   }else if(($url=='home' && $_SERVER['REQUEST_URI']=='/')||($urlvar=='home' && $url=='home')){
							  echo "tab_selected"; 
						   }
					?> "><?php echo ucfirst($link);?></a>
     <?php 	$k++;
					endforeach 
			?>
     </div>
     <div class="breadCrumbHolder module">
		<div id="breadCrumb3" class="breadCrumb module">
	        <ul>
		      <li><a href="<?php echo url::site()."home";?>">Home</a></li>
			  <?php
					
					switch($urlvar){
						case "users":				
							echo "<li><a href='#'>Users List</a></li>";
						break;
						case "usertypes":				
							echo "<li><a href='#'>User Type List</a></li>";
						break;
						case "departments":				
							echo "<li><a href='#'>Department List</a></li>";
						break;
						case "pages":				
							echo "<li><a href='#'>Pages List</a></li>";
						break;
						case "articles":				
							echo "<li><a href='#'>Article List</a></li>";
						break;
						case "templates":				
							echo "<li><a href='#'>Template List</a></li>";
						break;
						case "snippets":				
							echo "<li><a href='#'>Snippet List</a></li>";
						break;
						case "formulary":				
							echo "<li><a href='#'>Formulary List</a></li>";
						break;
						case "pubmed":				
							echo "<li><a href='#'>Pubmed List</a></li>";
						break;
						case "drugs":				
							echo "<li><a href='#'>Drugs List</a></li>";
						break;
						case "language":				
							echo "<li><a href='#'>Language List</a></li>";
						break;		
						case "members":				
							echo "<li><a href='#'>Members List</a></li>";
						break;
						case "news":				
							echo "<li><a href='#'>News List</a></li>";
						break;																							
						case "assets":				
							echo "<li><a href='#'>Assets List</a></li>";
						break;
						case "assetcat":				
							echo "<li><a href='#'>Asset Category List</a></li>";
						break;									
						default:
							echo "<li><a href='#'>Dashboard</a></li>";
						break;
					}
				?>
			</ul>
      </div>
    </div>
  </div>
</div>
<div class="outer-center">
    <div class="middle-center">
      <div class="inner-center">
		   <!-- start of inner content -->
		   <div class="dialog-confirm" title="Delete the record?">     
			  <p><span class="ui-icon ui-icon-alert" style='float: left; margin: 0pt 7px 20px 0pt;'></span>Are you sure you want to delete?</p>
			</div>
			<?php echo $content ?>
			<!-- end of inner content -->
	  </div>
	  <?php if($urlaction=='formulary_add'){?>
      <!-- Inner South Toolbox Area containing-->
      <div class="ui-layout-south">
        <!-- start of inner content -->
        <div id="activitytabs">
			<ul>
			  <li><a href="#refs">Refs</a></li>
			  <li><a href="#snippets">Snippets</a></li>
			  <!-- li><a href="#drugs">Drugs</a></li -->
			  <li><a href="#tformulary">Formulary</a></li>
			</ul>
			<div id="refs">
				 <table cellspacing='0' cellpadding='0' border='0' width='100%' class="display" id='pubmedlist'>
				   <col id="colid" />
				   <col id="colstatus" />
				   <col id="colid" />	 		   
				   <col id="colactions" />
				   <thead>		   			
					<tr>				
						<th><a href="#" class="sort">ID</a></th> 
						<th><a href="#" class="sort">PMID</a></th> 
						<th><a href="#" class="sort">Title</a></th> 				
						<th><a href="#" class="sort">Action</a></th> 				
					</tr>
					</thead>
					<tbody>								
					</tbody>
				</table>
			</div>
			<div id="snippets">
              <table cellspacing='0' cellpadding='0' border='0' width='100%' class="display" id='snippetlist'>				  
				   <thead>		   			
					<tr>				
						<th><a href="#" class="sort">Type</a></th> 
						<th><a href="#" class="sort">Ref No</a></th> 
						<th><a href="#" class="sort">Title</a></th> 				
						<th><a href="#" class="sort">Status</a></th> 												
					</tr>
					</thead>
					<tbody>								
					</tbody>
				</table>
			</div>
            <!-- div id="drugs">
				 <table cellspacing='0' cellpadding='0' border='0' width='100%' class="display" id='supplylist'>
				   <thead>		   			
					<tr>				
						<th>Drug Name</th> 
						<th>Brand</th> 
						<th>Manufacture</th> 
						<th>Strength</th> 				
						<th>Formulation</th>
						<th>Packsize</th>
						<th>Cost Quoted</th>
						<th>NO Of Dose</th>
						<th>Cost/Dose</th>
						<th>Dose/Day</th>
						<th>28 Days Treatment</th>																			
					</tr>
					</thead>
					<tbody>								
					</tbody>
				</table>
			</div -->
			<div id="tformulary">
				 <table cellspacing='0' cellpadding='0' border='0' width='100%' class="display" id='lformlist'>
				   <thead>		   			
					<tr>				
						<th>Title</th> 
						<th>Anchors</th> 
						<th>Content Type</th> 
						<th>Version</th> 
						<th>Publish Status</th> 				
						<th>Status</th>
					</tr>
					</thead>
					<tbody>								
					</tbody>
				</table>
			</div>
		</div>
        <!-- end of inner content -->
      </div>
      <!-- Inner South Toolbox Area End-->
	  <?php }else if($urlaction=='snippet_add'){?>
		  <!-- Inner South Toolbox Area containing-->
		  <div class="ui-layout-south">
			<!-- start of inner content -->
			<div id="activitytabs">
				<ul>
				  <li><a href="#refs">Refs</a></li>				 
				</ul>
				<div id="refs">			     
					 <table cellspacing='0' cellpadding='0' border='0' width='100%' class="display" id='pubmedlist'>
					   <col id="colid" />
					   <col id="colstatus" />
					   <col id="colid" />	 		   
					   <col id="colactions" />
					   <thead>		   			
						<tr>				
							<th><a href="#" class="sort">ID</a></th> 
							<th><a href="#" class="sort">PMID</a></th> 
							<th><a href="#" class="sort">Title</a></th> 				
							<th><a href="#" class="sort">Action</a></th> 				
						</tr>
						</thead>
						<tbody>								
						</tbody>
					</table>
				</div>				
			</div>
			<!-- end of inner content -->
		  </div>
		  <!-- Inner South Toolbox Area End-->
		  <?php } ?>
    </div>
	<div class="middle-west" id="sidenavigation">
      <!-- start of content -->
      <div id="side-nav">
		<?php  
		if(is_array($leftnav)){ $m=0;?>
		<dl>      
		  <?php foreach ($leftnav as $link => $url):
			
			   if(preg_match("/".$urlvar."/", str_replace(" ","",strtolower($link))) || preg_match("/".$urlvar."/", strtolower($url))){
					$style	=	'style="display:block;"';
				}else{
				    $style	=	'style="display:none;"';
				}
				if($url ==''){
						if($m!=0){
		  ?>
			</ul>
		  </dd>
		  <?			} ?>
		  <dt><a href="#"><?=$link?></a></dt>
		  
			<dd <?=$style?>>
				<ul>
		  <?php		}else{ ?>
				<li> <?php echo html::anchor($url,$link)?> </li>
		  <?php		} 
				$m++;
		   endforeach ?>
				</ul>
			 <dd>
		</dl>
		<?php }else{?>
		<dl>
		  <dt>
			<ul>
				<li><? echo $_SERVER["HTTP_USER_AGENT"]; ?></li>
			    <li><? echo $_SERVER["SERVER_SIGNATURE"]; ?></li>
			</ul>
		  </dt>
		</dl>
		<?php } ?>
	  </div>
      <!-- end of content -->
    </div>
	<?php if($urlaction=='formulary_add' || $urlaction=='snippet_add'){?>
		<div class="middle-east">
			<div id="dialog" title="References">
				<h2>References</h2>
				<ol></ol>
				<a class="button" onclick="refresh_dialog()">Refresh</a>
			</div>
		</div>
		<?php } ?>
  </div>
</div>
<script language="javascript" type="text/javascript">
		var sess_id = '<?=session_id();?>';
		var project_host='https://<?=$_SERVER['HTTP_HOST']?>/';
		var server_time= '<?=date("Y/m/d H:i:s");?>';
</script>
</body>
</html>