<div id="article_tabs">
<ul>
  <li><a href="#tgeneral">Add/Edit Language</a></li>
</ul>
<div id="tgeneral">	
  <div id="form_container">	
	<form action="<?php echo url::site().'language/language_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addlanguage" name='addlanguage'>
	<div class="form_description">		
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul> 	
		<li>
			<label class="description" for="lang_name">Language Name<span class="required">*</span></label>
			<div>
				<input id="language_name" name="language_name" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['language_name']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter name.</small></p>
		</li>		
		<li>
			<label class="description" for="code">Language Code<span class="required">*</span></label>
			<div>
				<input id="language_code" name="language_code" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['language_code']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter code.</small></p>
		</li>
		<li>
			<label class="description" for="def">Default</label>
			<div>
				<input id="is_default" name="is_default" type="checkbox" <?php if(!empty($form['is_default'])){?>checked <? } ?> value='1'/> 
			</div> 			
		</li>		
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_language();">Submit</button>
        </li>
	 </ul>			
	</form>
</div>
</div>
</div>