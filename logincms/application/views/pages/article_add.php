<div id="form_container">
  <div class="form_description">
    <h2>Add/Edit Article</h2>
    <p class="error">
    <ul>
      <?php foreach ($errors as $error): ?>
      <li><?php echo $error ?></li>
      <?php endforeach ?>
    </ul>
    </p>
  </div>  
  <div id="article_tabs">
  <ul>
    <li><a href="#tgeneral">General</a></li>
    <li><a href="#tcontent">Content</a></li>
    <li><a href="#timages">Images</a></li>
    <li><a href="#tmeta">Meta</a></li>
  </ul>
  <form action="<?php echo url::site().'articles/article_add'?>" class="appnitro" enctype="multipart/form-data" method="post" name="addarticle" id="addarticle">
  <input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
    <div id="tgeneral">
      <ul>
        <li>
          <label class="description" for="article_title">Article Title<span class="required">*</span></label>
          <div>
            <input id="article_title" name="article_title" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['article_title']) ?>"/>
          </div>
          <p class="guidelines"><small>Please enter title.</small></p>
        </li>
        <li>
          <label class="description" for="owner">Owner</label>
          <div>
            <input id="owner" name="owner" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['owner']) ?>"/>
          </div>
          <p class="guidelines"><small>Please enter owner.</small></p>
        </li>
        <li>
          <label class="description" for="status">Status </label>
          <span>
          <input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
          <label class="choice" for="status">Active</label>
          <input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
          <label class="choice" for="status">InActive</label>
          </span> </li>
      </ul>
    </div>
    <div id="tcontent">
      <ul>
        <li>
          <label class="description" for="snippet">Insert Snippets</label>
          <div>
            <select id='snippetid' name='snippetid' class="element select small">
              <option value="">-Select Snippet-</option>
              <?php foreach ($snippets as $snippet): ?>
              <option value="<?php echo html::specialchars($snippet->snippet_title) ?>"><?php echo html::specialchars($snippet->snippet_title) ?></option>
              <?php endforeach ?>
            </select>
            <a class="button" href='javascript:void(0);' onclick='insertSnippet(document.addarticle)'><img src="/logincms/assets/images/icons/textfield_add.png" width="16" height="16" />Insert</a> </div>
        </li>
        <li>
          <label class="description" for="short_text">Short Text</label>
          <div>
            <textarea id="short_text" name="short_text" class="mceEditor"><?php echo html::specialchars($form['short_text']) ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter short text.</small></p>
        </li>
        <li>
          <label class="description" for="long_text">Long Text<span class="required">*</span></label>
          <div>
            <textarea id="long_text" name="long_text" class="mceEditor"><?php echo html::specialchars($form['long_text']) ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter long text.</small></p>
        </li>
      </ul>
    </div>
    <div id="timages">
      <ul>
        <li>
          <label class="description" for="himage">Header Image</label>
          <div class="buttons">
            <?php $obj1	=	new Selectpattern();
							  echo $obj1->Select("yes","images",'no','addarticle','header-image');
						?>
          </div>
          <p class="guidelines"><small>Please select header image.</small></p>
        </li>
        <li>
          <label class="description" for="aimage">Article Image</label>
          <div> <?php echo $obj1->Select("yes","images",'no','addarticle','article-image');?> </div>
          <p class="guidelines"><small>Please select article image.</small></p>
        </li>
        <li>
          <label class="description" for="image_text">Image Text</label>
          <div>
            <textarea id="image_text" name="image_text" class="element textarea medium"><?php echo html::specialchars($form['image_text']) ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter image text.</small></p>
        </li>
      </ul>
    </div>
    <div id="tmeta">
      <ul>
        <li>
          <label class="description" for="meta_title">Meta Title</label>
          <div>
            <input id="meta_title" name="meta_title" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['meta_title']) ?>"/>
          </div>
          <p class="guidelines"><small>Please select meta title.</small></p>
        </li>
        <li>
          <label class="description" for="meta_desc">Meta Description</label>
          <div>
            <textarea id="meta_desc" name="meta_desc" class="element textarea medium"><?php echo html::specialchars($form['meta_desc']) ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter meta description.</small></p>
        </li>
        <li>
          <label class="description" for="meta_keyword">Meta Keywords</label>
          <div>
            <textarea id="meta_keywords" name="meta_keywords" class="element textarea medium"><?php echo html::specialchars($form['meta_keywords']) ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter meta meta keyword.</small></p>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_article();">Submit</button>
        </li>
      </ul>
    </div>
  </form>
</div>
</div>
<script type='text/javascript'>
	document.forms.addarticle.article_imageid.value="<?=$form['article_image']?>";
	document.forms.addarticle.article_image.value="<?=$form['article_image_img']?>";
	document.forms.addarticle.header_imageid.value="<?=$form['header_image']?>";
	document.forms.addarticle.header_image.value="<?=$form['header_image_img']?>";
</script>
