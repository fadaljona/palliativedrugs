<h2>List Pages</h2>
<div id="article_tabs">
    <ul>
      <li><a href="#pglobal">Global Nav</a></li>	 
      <li><a href="#psecondary">Secondary Nav</a></li>
      <li><a href="#pshop">Footer Nav</a></li>
      <li><a href="#pother">Article Nav</a></li>
	  <li><a href="#pformulary">Formulary Nav</a></li>
    </ul>    
	<div id='pglobal'>
		<table cellspacing='0' cellpadding='0' border='0' width='100%' class="display">
		   <thead>		   			
			<tr>
				<th>&nbsp;</th>
				<th><a href="#" class="sort">Title</a></th> 
				<th><a href="#" class="sort">Language</a></th> 
				<th><a href="#" class="sort">Level</a></th> 
				<th><a href="#" class="sort">URL</a></th> 
				<th><a href="#" class="sort">Template</a></th> 
				<th><a href="#" class="sort">Status</a></th> 
				<th><a href="#" class="sort">Action</a></th> 				
			</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="8">Global Nav <a class='button' href='javascript:;' onclick="openOrderPop('/logincms/includes/cat_order.php?type_id=Global Nav')"><img alt="Order Pages" title="Order Pages" src="/logincms/assets/images/icons/table_sort.png" border="0" />Order</a></td>
				</tr>
				<?=$global_nav?>
			</tbody>
		</table>
	</div>
	<div id='psecondary'>
		<table cellspacing='0' cellpadding='0' border='0' width='100%' class="display">
		   <thead>			
			<tr>
				<th>&nbsp;</th>
				<th><a href="#" class="sort">Title</a></th> 
				<th><a href="#" class="sort">Language</a></th> 
				<th><a href="#" class="sort">Level</a></th> 
				<th><a href="#" class="sort">URL</a></th> 
				<th><a href="#" class="sort">Template</a></th> 
				<th><a href="#" class="sort">Status</a></th> 
				<th><a href="#" class="sort">Action</a></th> 	
			</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="8">Secondary Global Nav <a class='button' href='javascript:;' onclick="openOrderPop('/logincms/includes/cat_order.php?type_id=Secondary Global Nav')"><img alt="Order Pages" title="Order Pages" src="/logincms/assets/images/icons/table_sort.png" border="0" />Order</a></td>
				</tr>
				<?=$sglobal_nav?>
			</tbody>
		</table>
	</div>
 	<div id='pshop'>
	<table cellspacing='0' cellpadding='0' border='0' width='100%' class="display">
	   <thead>		
		<tr>
			<th>&nbsp;</th>
			<th><a href="#" class="sort">Title</a></th>
			<th><a href="#" class="sort">Language</a></th> 
			<th><a href="#" class="sort">Level</a></th> 
			<th><a href="#" class="sort">URL</a></th> 
			<th><a href="#" class="sort">Template</a></th> 
			<th><a href="#" class="sort">Status</a></th> 
			<th><a href="#" class="sort">Action</a></th> 	
		</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="8">Footer Nav <a class='button' href='javascript:;' onclick="javascript:openOrderPop('/logincms/includes/cat_order.php?type_id=Footer Nav')"><img alt="Order Pages" title="Order Pages" src="/logincms/assets/images/icons/table_sort.png" border="0" />Order</a></td>
			</tr>
			<?=$footer_nav?>
		</tbody>
	</table>
   </div>
   <div id='pother'>
	  <table cellspacing='0' cellpadding='0' border='0' width='100%' class="display">
	   <thead>		
		<tr>
			<th>&nbsp;</th>
			<th><a href="#" class="sort">Title</a></th> 
			<th><a href="#" class="sort">Language</a></th> 
			<th><a href="#" class="sort">Level</a></th> 
			<th><a href="#" class="sort">URL</a></th> 
			<th><a href="#" class="sort">Template</a></th> 
			<th><a href="#" class="sort">Status</a></th> 
			<th><a href="#" class="sort">Action</a></th> 			
		</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="8">Article Nav <a class='button' href='javascript:;' onclick="openOrderPop('/logincms/includes/cat_order.php?type_id=Article Nav')"><img alt="Order Pages" title="Order Pages" src="/logincms/assets/images/icons/table_sort.png" border="0" />Order</a></td>
			</tr>
			<?=$article_nav?>
		</tbody>
	</table>
   </div>
    <div id='pformulary'>	
		<?php
		
			$this->db->query("SELECT * FROM 3bit_categories");
			$selMonograph		=	$this->db->query("SELECT f.* FROM 3bit_formulary f WHERE f.formulary_parent_id='0' ORDER BY f.formulary_order ASC")->result_array(FALSE);
			$nummono			=	count($selMonograph);
			if($nummono>0){
				$returnNav		=	'<ul class="sortable">
										<span id="formulary"></span>';
				foreach($selMonograph as $mrow){				
					$ftitle		=	stripslashes($mrow['formulary_title']);
					
					$selSec			=	$this->db->query("SELECT f.* FROM 3bit_formulary f WHERE f.formulary_parent_id='".$mrow['formulary_id']."' GROUP BY f.formulary_id ORDER BY f.formulary_order ASC")->result_array(FALSE);

					$numsec			=	count($selSec);
					if($numsec>0){
					   if ($mrow['status']=="1"){
							$mactive	=	"<span id='".$mrow['formulary_id']."3bit_formulary'><a href=\"javascript:;\" onclick=\"change_status(".$mrow['formulary_id'].",'3bit_formulary','status','0','formulary_id');\"><font color='#006600'>Active</font></a></span>";
						}else{
							$mactive	=	"<span id='".$mrow['formulary_id']."3bit_formulary'><a href=\"javascript:;\" onclick=\"change_status(".$mrow['formulary_id'].",'3bit_formulary','status','1','formulary_id');\"><font color='#FF0000'>InActive</font></a></span>";		
						}
					   $returnNav		.=	'<li id="form-'.$mrow['formulary_id'].'">
								                   <div><img src="/logincms/assets/images/icons/document-text-image.png" align="left"/>&nbsp;'.stripslashes($mrow['formulary_title']).'&nbsp;'.$mactive.' </div>';
					   $returnNav		.=	'<ul class="sortable">';
					   foreach($selSec as $srow){				
							$stitle		=	stripslashes($srow['formulary_title']);		
							if ($srow['status']=="1"){
								$sactive	=	"<span id='".$srow['formulary_id']."3bit_formulary'><a href=\"javascript:;\" onclick=\"change_status(".$srow['formulary_id'].",'3bit_formulary','status','0','formulary_id');\"><font color='#006600'>Active</font></a></span>";
							}else{
								$sactive	=	"<span id='".$srow['formulary_id']."3bit_formulary'><a href=\"javascript:;\" onclick=\"change_status(".$srow['formulary_id'].",'3bit_formulary','status','1','formulary_id');\"><font color='#FF0000'>InActive</font></a></span>";		
							}
							$returnNav		.=	'<li id="form-'.$srow['formulary_id'].'">
								                   <div><img src="/logincms/assets/images/icons/document-text-image.png" align="left"/>&nbsp;'.stripslashes($srow['formulary_title']).'&nbsp;'.$sactive.'</div>';

							$selThird			=	$this->db->query("SELECT f.*,a.formulary_id FROM 3bit_formulary f,3bit_formulary_articles a WHERE f.formulary_id=a.formulary_id AND f.formulary_parent_id='".$srow['formulary_id']."' GROUP BY f.formulary_id ORDER BY f.formulary_order ASC")->result_array(FALSE);
							$numthi				=	count($selThird);
							if($numthi>0){
							  $returnNav		.=	'<ul class="sortable">';
							  foreach($selThird as $trow){				
									$ttitle		=	stripslashes($trow['formulary_title']);
									if ($trow['status']=="1"){
										$tactive	=	"<span id='".$trow['formulary_id']."3bit_formulary'><a href=\"javascript:;\" onclick=\"change_status(".$trow['formulary_id'].",'3bit_formulary','status','0','formulary_id');\"><font color='#006600'>Active</font></a></span>";
									}else{
										$tactive	=	"<span id='".$trow['formulary_id']."3bit_formulary'><a href=\"javascript:;\" onclick=\"change_status(".$trow['formulary_id'].",'3bit_formulary','status','1','formulary_id');\"><font color='#FF0000'>InActive</font></a></span>";		
									}
									$returnNav	.=	'<li id="form-'.$trow['formulary_id'].'">
						                            	<div><img src="/logincms/assets/images/icons/document-text-image.png" align="left"/>&nbsp;'.stripslashes($ttitle).'&nbsp;'.$tactive.'</div></li>';
							  }
							  $returnNav		.=	'</ul>';						
							}

							$returnNav	.=	'</li>';
					   }
					   $returnNav		.=	'</ul>';
					}
					
					$returnNav	.=	'</li>';
				}			
				$returnNav	.=	'</ul>';
			} 

			echo $returnNav;
		
		?>		
	</div>
</div>

<script type="text/javascript">

jQuery( function($) { 
	$(function() {
		$('ul.sortable').bind('mousedown', function(e) {
	      e.stopPropagation();
		});
	    $("ul.sortable").sortable({
		  opacity: 0.7,
	      helper: 'clone',
		  cursor: 'move',
	      tolerance: 'pointer',
		  update: function(event,ui) {	
		    var module= $(".sortable").find('span').attr('id'); 
			var order = $(this).sortable("serialize") + '&module='+module+'&action=reorder'; 
			$.post("/logincms/includes/orderajax.php", order, function(theResponse){
				
			}); 															 
		   }								  
		});
		$("ul.sortable").selectable();
		$("ul.sortable").disableSelection();    
    });
});

</script>  		
