<div id="article_tabs">
<ul>
  <li><a href="#tgeneral">Add/Edit Templates</a></li>
</ul>
<div id="tgeneral">
	<div id="form_container">	
		<form action="<?php echo url::site().'templates/temp_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addtemp" name='addtemp'>
		<div class="form_description">			
			<p class="error">
				<ul>
			<?php foreach ($errors as $error): ?>
				<li><?php echo $error ?></li>
			<?php endforeach ?>
				</ul>
			</p>
		</div>		
		<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
		<ul> 	
			<li>
				<label class="description" for="first_name">Template Name<span class="required">*</span></label>
				<div>
					<input id="temp_filename" name="temp_filename" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['temp_filename']) ?>"/> 
				</div> 
				<p class="guidelines"><small>Please enter name.</small></p>
			</li>
			<li>
				<label class="description" for="first_name">Custom Template</label>
				<div>
					<input id="custom_template" name="custom_template" type="checkbox" <?php if(!empty($form['custom_template'])){?>checked <? } ?> value='1'/> 
				</div> 			
			</li>
			<li>
				<label class="description" for="last_name">Content<span class="required">*</span></label>
				<div>
					<textarea id="temp_content" name="temp_content" class="element textarea medium"><?php echo html::specialchars($form['temp_content']) ?></textarea> 
				</div> 
				<p class="guidelines"><small>Please enter content.</small></p>
			</li>	
			<li>
			  <label class="description" for="himage">Template Image</label>
			  <div class="buttons">
				<?php $obj1	=	new Selectpattern();
								  echo $obj1->Select("yes","images",'no','addtemp','template-thumbs');
							?>
			  </div>
			  <p class="guidelines"><small>Please select header image.</small></p>
			</li>
			<li>
			  <label class="description" for="status">Status </label>
			  <span>
				<input id="temp_status" name="temp_status" class="element radio" type="radio" value="1" checked="checked" <?php if($form['temp_status']=='1'){?>checked="checked"<? } ?> />
				<label class="choice" for="status">Active</label>
				<input name="temp_status" class="element radio" type="radio" value="0" <?php if($form['temp_status']=='0'){?>checked="checked"<? } ?> />
				<label class="choice" for="status">InActive</label>
			  </span>
			</li>
			<li class="buttons">
			  <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_template();">Submit</button>
			</li>
		 </ul>			
		</form>
	</div>
  </div>
</div>
<script type='text/javascript'>
	document.forms.addtemp.template_thumbsid.value="<?=$form['temp_thumbs']?>";
	document.forms.addtemp.template_thumbs.value="<?=$form['temp_thumbs_img']?>";	
</script>
