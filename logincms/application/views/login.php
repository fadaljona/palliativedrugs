<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Palliative Drugs CMS 2010</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Start Css Things-->
<link href="/min/g=Main-CMS-CSS" rel="stylesheet" type="text/css" media="screen">
<!--
<link type="text/css" rel="stylesheet" href="/logincms/assets/css/cupertino/jquery-ui-1.8rc2.custom.css" />
<link type="text/css" rel="stylesheet" href="/logincms/assets/css/Base.css" />
<link type="text/css" rel="stylesheet" href="/logincms/assets/css/forms.css" />
<link type="text/css" rel="stylesheet" href="/logincms/assets/css/facebox.css" />
<link type="text/css" rel="stylesheet" href="/logincms/assets/css/TableTools.css" />
<link type="text/css" rel="stylesheet" href="/logincms/assets/css/custom.css" />
-->
<!-- End CSS Rules-->

<!-- Javascript and Plugins-->
<script type="text/javascript" src="/min/g=jsLogin"></script>
<!--
<script type="text/javascript" src="/logincms/assets/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/logincms/assets/js/jquery-ui-1.8rc2.custom.min.js"></script>
<script type="text/javascript" src="/logincms/assets/js/jquery.highlight.js"></script>

<script type="text/javascript" src="/logincms/assets/js/login.js"></script>-->
<!-- End JS Things-->
<!-- Jquery things-->
<script type="text/javascript">

      $(document).ready(function(){	    

		$('#submitLogin').click(function(){

		   validate_login();

		});

      	$('#Reset, #submitLogin').button();

		$('#password, #username, #last_name, #first_name, #submitLogin').keypress(function(event){
			if(event.which=='13'){
				validate_login();
			}
		});

	  });

	  

	

</script>

</head>
<body id="loginbox" onload='$("#first_name").focus();'>
<div class="loginarea" style="text-align:center">
  <form id="login_form" name="login_form" class="appnitro"  method="post" action="/logincms/index.php/login/auth">
    <div class="form_description">
      <h2>CMS Administration.</h2>
      <p>Access to this area is strictly private, Insert your details below. If you require access contact the CMS Administrator.</p>
    </div>
    <ul >
      <li id="fname">
        <label class="description" for="fname">Full Name </label>
        <span>
        <input id="first_name" name= "first_name" class="element text" maxlength="255" size="8" value=""/>
        <label>First</label>
        </span> <span>
        <input id="last_name" name= "last_name" class="element text" maxlength="255" size="14" value=""/>
        <label>Last</label>
        </span> <span id='name_error' style="display:none">Unable to vertify these details</span></li>
      <li id="uname">
        <label class="description" for="uname">Access Username </label>
        <div>
          <input id="username" name="username" class="element text medium" type="text" maxlength="255" value=""/>
        </div>
      <span id='uname_error' style="display:none">Invalid Username</span> </li>
      <li id="pwd">
        <label class="description" for="pwd">Access Password </label>
        <div>
          <input id="password" name="password" class="element text medium" type="password" maxlength="255" value=""/>
        </div>
      <span id='pwd_error' style="display:none">Invalid Password</span> </li>
      <li class="buttons">
        <input id="submitLogin" class="button_text" type="button" name="submitLogin" value="Submit" />
        <input name="Reset" type="reset" value="Reset" id="Reset" />
      </li>
    </ul>
  </form>
</div>
</body>
</html>
