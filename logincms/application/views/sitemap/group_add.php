<div id="form_container">	
	<form action="<?php echo url::site().'sitemapgroups/group_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addsitemapgroup">
	<div class="form_description">
		<h2>Add/Edit Groups</h2>
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>			
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul> 	
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="title" name="title" class="element text medium" type="text" maxlength="30" value="<?php echo html::specialchars($form['smgroup_title']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter title.</small></p>
		</li>		
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_sitemap_group();">Submit</button>
        </li>
	 </ul>		
	</form>
</div>