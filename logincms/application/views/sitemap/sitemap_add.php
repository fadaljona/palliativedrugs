<div id="form_container">	
	<form action="<?php echo url::site().'sitemap/sitemap_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addsitemap">
	<div class="form_description">
		<h2>Add/Edit Sitemap Link</h2>
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul> 	
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="title" name="title" class="element text medium" type="text" maxlength="30" value="<?php echo html::specialchars($form['title']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter title.</small></p>
		</li>
		<li>
			<label class="description" for="group">Group<span class="required">*</span></label>
			<div>
				<select name="smgroup_id" id="smgroup_id" class="element select medium">
					<option value="">-Select Group-</option>
					 <?php foreach ($groups as $group): ?>
					 <option value="<?php echo html::specialchars($group->smgroup_id) ?>" <?php if($form['smgroup_id'] == $group->smgroup_id){?>selected<? } ?>><?php echo html::specialchars($group->smgroup_title) ?></option>
					 <?php endforeach ?>
				</select>
			</div> 
			<p class="guidelines"><small>Please select group.</small></p>
		</li>
		<li>
			<label class="description" for="category">Category<span class="required">*</span></label>
			<div>
				<select name="cat_id" id="cat_id" class="element select medium">
					<option value="">-Select Category-</option>
					 <?=$list_cat?>
				</select>
			</div> 
		</li>
		<li>
			<label class="description" for="url">URL<span class="required">*</span></label>
			<div>
				<input id="link" name="link" class="element text medium" type="text" value="<?php echo html::specialchars($form['link']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter url.</small></p>
		</li>	
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked <?php if($form['status']=='1'){?>checked<? } ?>/>
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_sitemap();">Submit</button>
        </li>
	 </ul>			
	</form>
</div>