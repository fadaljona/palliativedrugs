<h2>List Sitemap Groups</h2>
<a href="/logincms/includes/cat_order_groups.php?table=sgroups" class="button" id="ordersitemap" rel="facebox"><img alt="Order Groups" title="Order Groups" src="/logincms/assets/images/chart_organisation.png" border="0" />Order Sitemap Groups</a>

<table class="grid">
  <col id="colsitemap" />
  <col id="colstatus" />
  <col id="colactions" />
  <thead>
    
    <tr>
      <th><a class="sort" href="<?php echo url::site().'sitemapgroups/page/'.$page_no.'/smgroup_title/'.$sortorder;?>">Group Title<span class="button"></span></a></th>
	  <th></th>
      <th><a class="sort" href="<?php echo url::site().'sitemapgroups/page/'.$page_no.'/smgroup_status/'.$sortorder;?>">Status<span class="button"></span></a></th>
      <th>Action</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td colspan="4"><?php echo $pagination ?></td>
    </tr>
  </tfoot>
  <tbody>
    <?php foreach ($sitemapgroups as $item): ?>
    <tr>
      <td><?php echo html::specialchars($item->smgroup_title) ?></td>
	  <td></td>
      <td><span id="<?=$item->smgroup_id?>sitemap_groups">
        <?php if($item->smgroup_status=='1'){?>
        <a onclick='change_status(<?=$item->smgroup_id?>,"sitemap_groups","smgroup_status","0","smgroup_id");' href="javascript:;"><font color="green">Active</font></a>
        <? }else{?>
        <a onclick='change_status(<?=$item->smgroup_id?>,"sitemap_groups","smgroup_status","1","smgroup_id");' href="javascript:;"><font color="red">Inactive</font></a>
        <?} ?>
        </span></td>
      <td><a class="button" href="<?php echo url::site().'sitemapgroups/group_add/'.$item->smgroup_id?>"><img title="Edit" src="/logincms/assets/images/icons/page_white_edit.png">Edit</a><span id = "<?=$item->smgroup_id?>" <? if($item->smgroup_status=='1'){?> style='display:none' <? } ?>><a class="button" onclick="return open_dialog('<?php echo url::site().'sitemapgroups/group_delete/'.$item->smgroup_id?>');"><img title="Delete" src="/logincms/assets/images/icons/delete.png"/>Delete</a></span></td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>
