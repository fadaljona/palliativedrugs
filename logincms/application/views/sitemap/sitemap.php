<h2>List Links</h2>
<table class="grid">
  <col id="colname" />
  <col id="colmail"/>
  <col id="colstatus" />
  <col id="colactions" />
  <thead>
    <tr>
      <th>Group</th>
      <th>Title</th>
      <th>Status</th>
      <th>Action</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
  </tfoot>
  <tbody>
    <?php foreach ($sitemap as $item): ?>
    <tr>
      <td><?php if($item->smgroup_title != $temp){						
						echo html::specialchars($item->smgroup_title);
				  }else{
						echo "----";
				  }
			?><?php if($item->smgroup_title != $temp){ 			
				$temp	=	$item->smgroup_title;	
		?>
        <a class="button sitemapgrouporder" href="/logincms/includes/cat_order_groups.php?table=sitemap&amp;group_id=<?=$item->smgroup_id?>" rel="facebox"><img alt="Order Groups" title="Order Groups" src="/logincms/assets/images/icons/table_sort.png" border="0" />Order</a>
        <?php } ?></td>
      <td><?php echo html::specialchars($item->title) ?></td>
      <td><span id="<?=$item->link_id?>sitemap_links">
        <?php if($item->status=='1'){?>
        <a onclick='change_status(<?=$item->link_id?>,"sitemap_links","status","0","link_id");' href="javascript:;"><font color="green">Active</font></a>
        <? }else{?>
        <a onclick='change_status(<?=$item->link_id?>,"sitemap_links","status","1","link_id");' href="javascript:;"><font color="red">Inactive</font></a>
        <?} ?>
        </span></td>
      <td>
      <a class="button" href="<?php echo url::site().'sitemap/sitemap_add/'.$item->link_id?>"><img title="Edit" src="/logincms/assets/images/icons/page_white_edit.png">Edit</a>
      <span id = "<?=$item->link_id?>" <? if($item->status=='1'){?> style='display:none' <? } ?>><a class="button" onclick="return open_dialog('<?php echo url::site().'sitemap/sitemap_delete/'.$item->link_id?>');"><img title="Delete" src="/logincms/assets/images/icons/delete.png"/>Delete</a></span>
        </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>
