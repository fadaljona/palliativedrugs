<div id="form_container">
<div id="article_tabs">
    <ul>
      <li><a href="#fgeneral">Add/Edit Snippets</a></li>
	 </ul>
	<form action="<?php echo url::site().'snippets/snippet_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addsnippet" name="addsnippet">
	<div class="form_description">		
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<div id="fgeneral">
	<ul> 
		 <li>
			<label class="description" for="lang">Language<span class="required">*</span></label>
			<div>
			  <select id="language_id" name="language_id" class="element select small" >
				<?=$list_lang?>
			  </select>
			</div>
			<p class="guidelines"><small>Please select language.</small></p>
		  </li>		
		<li>
			<label class="description" for="first_name">Title<span class="required">*</span></label>
			<div>
				<input id="snippet_title" name="snippet_title" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['snippet_title']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter name.</small></p>
		</li>
		<li>
			<label class="description" for="no">Number<span class="required">*</span></label>
			<div>
				<input id="snippet_number" name="snippet_number" class="element text small" type="text" maxlength="10" value="<?php echo html::specialchars($form['snippet_number']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter number.</small></p>
		</li>
		<li>
			<label class="description" for="ttype">Type</label>
			<div><select name="snippet_type" id="snippet_type" tabindex='2' class="element select">				
				<option value="Box" <?php if($form['snippet_type'] == "Box"){?>selected<?php } ?>>Box</option>
				<option value="Chart" <?php if($form['snippet_type'] == "Chart"){?>selected<?php } ?>>Chart</option>
				<option value="Table" <?php if($form['snippet_type'] == "Table"){?>selected<?php } ?>>Table</option>
				<option value="Other" <?php if($form['snippet_type'] == "Other"){?>selected<?php } ?>>Other</option>				
			</select></div>
			<p class="guidelines"><small>Please select type.</small></p>
		</li>
		<li>
		  <label class="description" for="himage">Image</label>
		  <div class="buttons">
			<?php $obj1	=	new Selectpattern();
			  echo $obj1->Select("yes","images",'no','addsnippet','snippet-image');
			?> <a href="javascript:;"  onclick="assignImage()">Assign</a>
		  </div>
		  <p class="guidelines"><small>Please select snippet image.</small></p>
		</li>  		
		<li>
			<label class="description" for="last_name">Content<span class="required">*</span></label>
			<div>
				<textarea id="long_text" name="long_text" class="mceEditor"><?php echo html::specialchars(stripslashes($form['long_text'])) ?></textarea> 
			</div> 
			<p class="guidelines"><small>Please enter content.</small></p>
		</li>		
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked="checked" <?php if($form['status']=='1'){?>checked="checked"<? } ?> />
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked="checked"<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_snippet();">Submit</button>
        </li>
	 </ul>			
	</form>
	</div>
  </div>
</div>