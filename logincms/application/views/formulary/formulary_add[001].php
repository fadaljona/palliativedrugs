<div id="form_container">	
	<form action="<?php echo url::site().'formulary/formulary_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addformulary" name='addformulary'>
	<div class="form_description">
		<h2>Add/Edit Formulary</h2>
		<p class="error">
			<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
		</p>
	</div>		
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<ul> 	
	  <li>
        <label class="description" for="lang">Language<span class="required">*</span></label>
        <div>
          <select id="language_id" name="language_id" class="element select small">
			<?=$list_lang?>
		  </select>
        </div>
        <p class="guidelines"><small>Please enter title.</small></p>
      </li>
		<li>
			<label class="description" for="title">Formulary Title<span class="required">*</span></label>
			<div>
				<input id="formulary_title" name="formulary_title" class="element text medium" type="text" maxlength="255" value="<?php echo html_entity_decode(stripslashes($form['formulary_title'])) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter title.</small></p>
		</li>
		<li>
			<label class="description" for="dtype">Type<span class="required">*</span></label>
			<div><select name="formulary_type" id="formulary_type" tabindex='2' class="element select small">
			  <option value='Drug Monographs' <?php if($form['formulary_type']=="Drug Monographs"){?>SELECTED<?php } ?>>Drug Monographs</option>
			  <option value='General Topics' <?php if($form['formulary_type']=="General Topics"){?>SELECTED<?php } ?>>General Topics</option>
			  <option value='Appendicies' <?php if($form['formulary_type']=="Appendicies"){?>SELECTED<?php } ?>>Appendicies</option>					 
			</select></div>
			<p class="guidelines"><small>Please select type.</small></p>
		</li>
		<li>
			<label class="description" for="parent">Category Parent<span class="required">*</span></label>
			<div>
				<select name="formulary_parent_id" id="formulary_parent_id" tabindex='3' class="element select small">
				 <option value="0">Parent</option>
				<?=$parent_list;?>
				</select>
			</div> 
			<p class="guidelines"><small>Please select parent.</small></p>
		</li>
		<li>
			<label class="description" for="url">URL<span class="required">*</span></label>
			<div>
				<input id="url" name="url" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['url']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter url.</small></p>
		</li>
		<li>
			<label class="description" for="title">BNF Reference<span class="required">*</span></label>
			<div>
				<input id="bnf_reference" name="bnf_reference" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['bnf_reference']) ?>"/> 
			</div> 
			<p class="guidelines"><small>Please enter bnf reference.</small></p>
		</li>
		<li>
			<label class="description" for="country">Country</label>
			  <div>
				<select name="country" id="country" class="element select small">
					<option value="">Please select</option>
					<option value="PCF" <?php if($form['country']=="PCF"){?>selected <?php } ?>>UK</option>
					<option value="HPCF" <?php if($form['country']=="HPCF"){?>selected <?php } ?>>USA</option>
					<option value="APM" <?php if($form['country']=="APM"){?>selected <?php } ?>>GERMAN</option>
					<option value="CPC" <?php if($form['country']=="CPC"){?>selected <?php } ?>>CANADIAN</option>
				</select>
			  </div>
			  <p class="guidelines"><small>Please select country.</small></p>
		</li>
		<li>
			<label class="description" for="blocks"></label>
			<fieldset>
			  <legend>Content Areas</legend>
				<span>
					<?php $blocks	=	explode("#",$form['blocks']); ?>
					<input id="pre_text_article" name="pre_text_article" class="element checkbox" type="checkbox" value="1" <?php if(in_array('pre_text_article',$blocks)){?>checked<?php } ?> />
					<label class="choice" for="element_14_1">Introduction</label>
					
					<input id="class" name="class" class="element checkbox" type="checkbox" value="1" <?php if(in_array('class',$blocks)){?>checked<?php } ?> />
					<label class="choice" for="element_14_2">Class<br />
					(Stoffgruppe)</label>
					<input id="indication" name="indication" class="element checkbox" type="checkbox" value="1" <?php if(in_array('indication',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Indication<br />
					(Indikationen)</label>
					<input id="pharmacology" name="pharmacology" class="element checkbox" type="checkbox" value="1" <?php if(in_array('pharmacology',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_4">Pharmacology <br />
					(Pharmakologie)</label>
					<input id="cautions" name="cautions" class="element checkbox" type="checkbox" value="1" <?php if(in_array('cautions',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Cautions <br />
					(Warnhinweise)</label>
					<input id="undesireable_effects" name="undesireable_effects" class="element checkbox" type="checkbox" value="1" <?php if(in_array('undesireable_effects',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Undesireable Effects <br />
					(Nebenwirkungen)</label>
					<input id="dose_and_use" name="dose_and_use" class="element checkbox" type="checkbox" value="1" <?php if(in_array('dose_and_use',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Dose And Use<br />
					(Dosierung mit Art und Dauer der Anwendung)</label>
					<input id="overdose" name="overdose" class="element checkbox" type="checkbox" value="1" <?php if(in_array('overdose',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Overdose</label>
					<input id="death_rattle" name="death_rattle" class="element checkbox" type="checkbox" value="1" <?php if(in_array('death_rattle',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Death Rattle</label>
					<input id="supply" name="supply" class="element checkbox" type="checkbox" value="1" <?php if(in_array('supply',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Supply <br />
					(Verf&uuml;gbare Fertigarzneimittel (Auswahl))</label>
					<input id="post_text_article" name="post_text_article" class="element checkbox" type="checkbox" value="1" <?php if(in_array('post_text_article',$blocks)){?>checked<?php } ?>/>
					<label class="choice" for="element_14_3">Content after References</label>
			</span>
			</fieldset>
		</li>
		<li>
          <label class="description" for="status">Status </label>
          <span>
		    <input id="status" name="status" class="element radio" type="radio" value="1" checked="checked" <?php if($form['status']=='1'){?>checked="checked"<? } ?> />
			<label class="choice" for="status">Active</label>
			<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked="checked"<? } ?> />
			<label class="choice" for="status">InActive</label>
          </span>
        </li>
        <li class="buttons">
          <button id="saveForm" class="fg-button ui-state-default ui-corner-all" type="button" onclick="validate_formulary();">Submit</button>
        </li>
	 </ul>			
	</form>
</div>