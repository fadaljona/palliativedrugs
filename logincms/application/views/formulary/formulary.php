<div id="filters">
  <h3><a href="#">Filters</a></h3>
  <div>	
    <div>
	  <input type='hidden' name='search_type' id='search_type' value='A'>
	  <input type='hidden' name='search_level' id='search_level' value='A'>
	  <input type='hidden' name='search_lang' id='search_lang' value='A'>
      <ul id='form_type'>
		  <li class="filtertitle">By Content Type</li>
		  <li id='fcon_A'><a class='selected' href="javascript:;" onclick="show_form('A','0','0');">Any / All</a></li>
		  <li id='fcon_PR'><a href="javascript:;" onclick="show_form('PR','0','0');">Prelims</a></li>		  
		  <li id='fcon_DM'><a href="javascript:;" onclick="show_form('DM','0','0');">Monographs</a></li>
		  <li id='fcon_GT'><a href="javascript:;" onclick="show_form('GT','0','0');">General Topics</a></li>
		  <li id='fcon_AP'><a href="javascript:;" onclick="show_form('AP','0','0');">Appendices</a></li>		
      </ul>
      <ul id='form_level'>
        <li class="filtertitle">By Level</li>
        <li id='flev_A'><a class='selected' href="javascript:;" onclick="show_form('0','A','0');">Any / All</a></li>
        <?php foreach($levels as $level){?>
		<li id='flev_<?php echo str_replace(" ","_",$level['formulary_level'])?>'><a href="javascript:;" onclick="show_form('0','<?php echo $level['formulary_level']?>','0');"><?php echo $level['formulary_level']?></a></li>
		<?php } ?>   
      </ul>
      <ul id='form_lang'>
        <li class="filtertitle">By Language</li>
        <li id='flang_A'><a class='selected' href="javascript:;" onclick="show_form('0','0','A');">Any / All</a></li>
        <li id='flang_en'><a href="javascript:;" onclick="show_form('0','0','en');">English</a></li>
        <li id='flang_de'><a href="javascript:;" onclick="show_form('0','0','de');">German</a></li>        
      </ul>
    </div>
  </div>
</div>
<h2>Formulary List</h2>
<div id="article_tabs">
    <ul>
      <li><a href="#pglobal">Formulary List</a></li>	       
    </ul>    
	<div id='pglobal'>
		<table cellspacing='0' cellpadding='0' border='0' width='100%' class="display" id="formularylist">
		    <col id="coltitle" />
            <col id="colsnippet" />
            <col id="colctype" />
            <col id="colversion" />
            <col id="colstatus" />
            <col id="colmstatus" />
            <col id="colaction" />
		   <thead>		   			
			<tr>				
				<th>Title</th> 
				<th>Snippets</th> 
				<th>Content Type</th> 
				<th>Version</th> 
				<th>Publish Status</th> 						
				<th>Status</th> 
				<th>Action</th> 				
			</tr>
			</thead>
			<tbody>				
				<?// =$formulary_list?>
			</tbody>
		</table>
	</div>
</div>

