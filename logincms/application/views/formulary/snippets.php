<div id="article_tabs">
    <ul>
      <li><a href="#pglobal">Snippets List</a></li>	       
    </ul>    
	<div id='pglobal'>		
		<table class="display" id='snippetlist'>
		  <col id="colsnippet" />
		  <col id="collanguage" />
		  <col id="coltype" />
		  <col id="colstatus" />
		  <col id="colactions" />
		  <thead>
			<tr>
			  <th>Number</th>
			  <th>Snippet Title</th>	  			 
			  <th>Type</th>	  	 		
			  <th>Language</th>	 
			  <th>Usage</th>	 
			  <th>Status</th>
			  <th>Action </th>
			</tr>   
		  </thead>  
		  <tbody>    
		  </tbody>
		</table>
	</div>
</div>
