<div class="form_description">
	<h2>Add/Edit Formulary</h2>
	<p class="error">
		<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
	</p>
</div>	
<div id="form_container">
 <div id="article_tabs">
    <ul>
      <li><a href="#fgeneral">General</a></li>
	  <span id='new_intro'>
      <li><a href="#fintro">Introduction</a></li>
	  </span>
	  <span id='new_class'>
      <li><a href="#fclass">Class</a></li>
	  </span>
	  <span id='new_indicate'>
      <li><a href="#findicate">Indication</a></li>
	  </span>
	  <span id='new_pharma'>
      <li><a href="#fpharma">Pharmacology</a></li>
	  </span>
	  <span id='new_caution'>
      <li><a href="#fcaution">Cautions</a></li>
	  </span>
	  <span id='new_undesire'>
      <li><a href="#fundesire">Undesireable Effects</a></li>
	  </span>
	  <span id='new_dose'>
      <li><a href="#fdose">Dose and Use</a></li>
	  </span>
	  <span id='new_over'>
      <li><a href="#fover">Overdose</a></li>
	  </span>
	  <span id='new_death'>
      <li><a href="#fdeath">Death Rattle</a></li>
	  </span>
	  <span id='new_supply'>
      <li><a href="#fsupply">Supply</a></li>
	  </span>
	  <span id='new_npost'>
      <li><a href="#fnpost">Post Text</a></li>
	  </span>
    </ul>    
	<form action="<?php echo url::site().'formulary/formulary_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addformulary" name='addformulary'>
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<input type='hidden' name='action' id='action' value='' />
	<div id='fgeneral'>
		<ul> 	
		  <li>
			<label class="description" for="lang">Language<span class="required">*</span></label>
			<div>
			  <select id="language_id" name="language_id" class="element select small">
				<?=$list_lang?>
			  </select>
			</div>
			<p class="guidelines"><small>Please enter title.</small></p>
		  </li>
			<li>
				<label class="description" for="title">Formulary Title<span class="required">*</span></label>
				<div>
					<input id="formulary_title" name="formulary_title" class="element text medium" type="text" maxlength="255" value="<?php echo html_entity_decode(stripslashes($form['formulary_title'])) ?>"/> 
				</div> 
				<p class="guidelines"><small>Please enter title.</small></p>
			</li>
			<li>
				<label class="description" for="dtype">Type<span class="required">*</span></label>
				<div><select name="formulary_type" id="formulary_type" tabindex='2' class="element select small">
				  <option value='Drug Monographs' <?php if($form['formulary_type']=="Drug Monographs"){?>SELECTED<?php } ?>>Drug Monographs</option>
				  <option value='General Topics' <?php if($form['formulary_type']=="General Topics"){?>SELECTED<?php } ?>>General Topics</option>
				  <option value='Appendicies' <?php if($form['formulary_type']=="Appendicies"){?>SELECTED<?php } ?>>Appendicies</option>					 
				</select></div>
				<p class="guidelines"><small>Please select type.</small></p>
			</li>
			<li>
				<label class="description" for="parent">Category Parent<span class="required">*</span></label>
				<div>
					<select name="formulary_parent_id" id="formulary_parent_id" tabindex='3' class="element select small">
					 <option value="0">Parent</option>
					<?=$parent_list;?>
					</select>
				</div> 
				<p class="guidelines"><small>Please select parent.</small></p>
			</li>
			<li>
				<label class="description" for="url">URL<span class="required">*</span></label>
				<div>
					<input id="url" name="url" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['url']) ?>"/> 
				</div> 
				<p class="guidelines"><small>Please enter url.</small></p>
			</li>
			<li>
				<label class="description" for="title">BNF Reference<span class="required">*</span></label>
				<div>
					<input id="bnf_reference" name="bnf_reference" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['bnf_reference']) ?>"/> 
				</div> 
				<p class="guidelines"><small>Please enter bnf reference.</small></p>
			</li>
			<li>
				<label class="description" for="country">Country</label>
				  <div>
					<select name="country" id="country" class="element select small">
						<option value="">Please select</option>
						<option value="PCF" <?php if($form['country']=="PCF"){?>selected <?php } ?>>UK</option>
						<option value="HPCF" <?php if($form['country']=="HPCF"){?>selected <?php } ?>>USA</option>
						<option value="APM" <?php if($form['country']=="APM"){?>selected <?php } ?>>GERMAN</option>
						<option value="CPC" <?php if($form['country']=="CPC"){?>selected <?php } ?>>CANADIAN</option>
					</select>
				  </div>
				  <p class="guidelines"><small>Please select country.</small></p>
			</li>
			<li>
				<label class="description" for="blocks"></label>
				<fieldset>
				  <legend>Content Areas</legend>
					<span>
						<?php $blocks	=	explode("#",$form['blocks']); ?>
						<input id="pre_text_article" name="pre_text_article" type="checkbox" value="1" <?php if(in_array('pre_text_article',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_intro');" />
						<label class="choice" for="element_14_1">Introduction</label>
						
						<input id="class" name="class" type="checkbox" value="1" <?php if(in_array('class',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_class');" />
						<label class="choice" for="element_14_2">Class<br />
						(Stoffgruppe)</label>
						<input id="indication" name="indication" type="checkbox" value="1" <?php if(in_array('indication',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_indicate');" />
						<label class="choice" for="element_14_3">Indication<br />
						(Indikationen)</label>
						<input id="pharmacology" name="pharmacology" type="checkbox" value="1" <?php if(in_array('pharmacology',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_pharma');"  />
						<label class="choice" for="element_14_4">Pharmacology <br />
						(Pharmakologie)</label>
						<input id="cautions" name="cautions" type="checkbox" value="1" <?php if(in_array('cautions',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_caution');" />
						<label class="choice" for="element_14_3">Cautions <br />
						(Warnhinweise)</label>
						<input id="undesireable_effects" name="undesireable_effects" type="checkbox" value="1" <?php if(in_array('undesireable_effects',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_undesire');" />
						<label class="choice" for="element_14_3">Undesireable Effects <br />
						(Nebenwirkungen)</label>
						<input id="dose_and_use" name="dose_and_use" type="checkbox" value="1" <?php if(in_array('dose_and_use',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_dose');" />
						<label class="choice" for="element_14_3">Dose And Use<br />
						(Dosierung mit Art und Dauer der Anwendung)</label>
						<input id="overdose" name="overdose" type="checkbox" value="1" <?php if(in_array('overdose',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_over');" />
						<label class="choice" for="element_14_3">Overdose</label>
						<input id="death_rattle" name="death_rattle" type="checkbox" value="1" <?php if(in_array('death_rattle',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_death');" />
						<label class="choice" for="element_14_3">Death Rattle</label>
						<input id="supply" name="supply" type="checkbox" value="1" <?php if(in_array('supply',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_supply');" />
						<label class="choice" for="element_14_3">Supply <br />
						(Verf&uuml;gbare Fertigarzneimittel (Auswahl))</label>
						<input id="post_text_article" name="post_text_article" type="checkbox" value="1" <?php if(in_array('post_text_article',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_npost');" />
						<label class="choice" for="element_14_3">Content after References</label>
				</span>
				</fieldset>
			</li>
			<li>
			  <label class="description" for="status">Status </label>
			  <span>
				<input id="status" name="status" class="element radio" type="radio" value="1" checked="checked" <?php if($form['status']=='1'){?>checked="checked"<? } ?> />
				<label class="choice" for="status">Active</label>
				<input name="status" class="element radio" type="radio" value="0" <?php if($form['status']=='0'){?>checked="checked"<? } ?> />
				<label class="choice" for="status">InActive</label>
			  </span>
			</li>
		</ul>
	</div>
	<div id="fintro">
		<ul>
		<li>
          <label class="description" for="intro_text">Introduction Content<span class="required">*</span></label>
          <div>
            <textarea id="intro_text" name="intro_text" class="mceEditor"><?php if(!empty($form['pre_text_article'])){echo html_entity_decode($form['pre_text_article']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
      </ul>
	</div>
	<div id="fclass">
	  <ul>
		<li>
          <label class="description" for="class_content">Class Content<span class="required">*</span></label>
          <div>
            <textarea id="class_content" name="class_content" class="mceEditor"><?php if(!empty($form['class'])){echo html_entity_decode($form['class']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	  </ul>
	</div>
	<div id="findicate">
	  <ul>
		<li>
          <label class="description" for="indication_content">Indications<span class="required">*</span></label>
          <div>
            <textarea id="indication_content" name="indication_content" class="mceEditor"><?php if(!empty($form['indication'])){echo html_entity_decode($form['indication']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
		<li>
          <label class="description" for="contra_content">Contra Indications<span class="required">*</span></label>
          <div>
            <textarea id="contra_content" name="contra_content" class="mceSimple"><?php if(!empty($form['indication'])){echo html_entity_decode($form['indication']->block_content2);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	  </ul>
	</div>
	<div id="fpharma">
		<ul>
		<li>
          <label class="description" for="pharma_content">Pharmacology Content<span class="required">*</span></label>
          <div>
            <textarea id="pharma_content" name="pharma_content" class="mceEditor"><?php if(!empty($form['pharmacology'])){echo html_entity_decode($form['pharmacology']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
		<li>
          <label class="description" for="bio_availability">Bio Availability <br/>(Bioverfügbarkeit)<span class="required">*</span></label>
          <div>
            <textarea id="bio_availability" name="bio_availability" class="mceSimple"><?php if(!empty($form['pharmacology'])){echo html_entity_decode($form['pharmacology']->bio_availability);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
		<li>
          <label class="description" for="onset">Onset of Action <br/>(Wirkungseintritt)<span class="required">*</span></label>
          <div>
            <textarea id="onset_of_action" name="onset_of_action" class="mceSimple"><?php if(!empty($form['pharmacology'])){echo html_entity_decode($form['pharmacology']->onset_of_action);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
		<li>
          <label class="description" for="plasma_concentration">Time to peak plasma concentration<br/>(Zeitraum bis zum Erreichen der max. Plasma-Konzentration)<span class="required">*</span></label>
          <div>
            <textarea id="plasma_concentration" name="plasma_concentration" class="mceSimple"><?php if(!empty($form['pharmacology'])){echo html_entity_decode($form['pharmacology']->time_to_peak);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
		<li>
          <label class="description" for="plasma_halflife">Plasma halflife<br/>(Halbwertzeit)<span class="required">*</span></label>
          <div>
            <textarea id="plasma_halflife" name="plasma_halflife" class="mceSimple"><?php if(!empty($form['pharmacology'])){echo html_entity_decode($form['pharmacology']->plasma_halflife);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
		<li>
          <label class="description" for="duration_of_action">Duration of action<br/>(Wirkdauer)<span class="required">*</span></label>
          <div>
            <textarea id="duration_of_action" name="duration_of_action" class="mceSimple"><?php if(!empty($form['pharmacology'])){echo html_entity_decode($form['pharmacology']->duration_of_action);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	  </ul>
	</div>
	<div id="fcaution">
	  <ul>
		<li>
          <label class="description" for="cautions_content">Cautions<span class="required">*</span></label>
          <div>
            <textarea id="cautions_content" name="cautions_content" class="mceEditor"><?php if(!empty($form['cautions'])){echo html_entity_decode($form['cautions']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	   </ul>
	</div>
	<div id="fundesire">
	  <ul>
		<li>
          <label class="description" for="undesireable_effect">Undesireable Effects<span class="required">*</span></label>
          <div>
            <textarea id="undesireable_effect" name="undesireable_effect" class="mceEditor"><?php if(!empty($form['undesireable_effects'])){echo html_entity_decode($form['undesireable_effects']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	   </ul>
	</div>
	<div id="fdose">
	  <ul>
		<li>
          <label class="description" for="dose_use">Dose And Use<span class="required">*</span></label>
          <div>
            <textarea id="dose_use" name="dose_use" class="mceEditor"><?php if(!empty($form['dose_and_use'])){echo html_entity_decode($form['dose_and_use']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	   </ul>
	</div>
	<div id="fover">
	  <ul>
		<li>
          <label class="description" for="over_dose">Over Dose<span class="required">*</span></label>
          <div>
            <textarea id="over_dose" name="over_dose" class="mceEditor"><?php if(!empty($form['overdose'])){echo html_entity_decode($form['overdose']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	   </ul>
	</div> 
	<div id="fdeath">
	  <ul>
		<li>
          <label class="description" for="death_rattle_content">Death Rattle<span class="required">*</span></label>
          <div>
            <textarea id="death_content" name="death_content" class="mceEditor"><?php if(!empty($form['death_rattle'])){echo html_entity_decode($form['death_rattle']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	   </ul>
	</div>
	<div id="fsupply">
		<ul>
		<li>
          <label class="description" for="supply_content">Supply<span class="required">*</span></label>
          <div>
            <textarea id="supply_content" name="supply_content" class="mceEditor"><?php if(!empty($form['supply'])){echo html_entity_decode($form['supply']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	   </ul>
	</div>
	<div id="fnpost">
		<ul>
		<li>
          <label class="description" for="post_content">Additional Content (to appear after References)<span class="required">*</span></label>
          <div>
            <textarea id="post_content" name="post_content" class="mceEditor"><?php if(!empty($form['post_text_article'])){echo html_entity_decode($form['post_text_article']->block_content1);} ?></textarea>
          </div>
          <p class="guidelines"><small>Please enter content.</small></p>
        </li>
	   </ul>
	</div>
	 <ul>
        <li class="buttons">
          <a id="saveForm" class="button" onclick="validate_formulary('save');">Save</a> <?php if(!empty($form['id'])){?><a id="saveForm" class="button" onclick="validate_formulary('duplicate');">Duplicate</a><?php } ?>
        </li>
	 </ul>			
	</form>
</div>
</div>
<script type="text/javascript">
<?php if(!in_array('pre_text_article',$blocks)){?>
  $('#new_intro').hide();
  //$('#fintro').hide();
<?php }if(!in_array('class',$blocks)){?>
  $('#new_class').hide();
  //$('#fclass').hide();
<?php }if(!in_array('indication',$blocks)){?> 
  $('#new_indicate').hide();
  //$('#findicate').hide();
<?php }if(!in_array('pharmacology',$blocks)){?>
  $('#new_pharma').hide();
  //$('#fpharma').hide();
<?php }if(!in_array('cautions',$blocks)){?>
  $('#new_caution').hide();
  //$('#fcaution').hide();
<?php }if(!in_array('undesireable_effects',$blocks)){?> 
  $('#new_undesire').hide();
  //$('#fundesire').hide();
<?php }if(!in_array('dose_and_use',$blocks)){?>
  $('#new_dose').hide();
  //$('#fdose').hide();
<?php }if(!in_array('overdose',$blocks)){?>
  $('#new_over').hide();
  //$('#fover').hide();
 <?php }if(!in_array('death_rattle',$blocks)){?>
  $('#new_death').hide();
  //$('#fdeath').hide();
 <?php }if(!in_array('supply',$blocks)){?>
  $('#new_supply').hide();
  //$('#fsupply').hide();
 <?php }if(!in_array('post_text_article',$blocks)){?>
  $('#new_npost').hide();  
  //$('#fnpost').hide();
 <?php } ?>
</script>