<div class="form_description">
	<h2>Add/Edit Formulary</h2>
	<p class="error">
		<ul>
		<?php foreach ($errors as $error): ?>
			<li><?php echo $error ?></li>
		<?php endforeach ?>
			</ul>
	</p>
</div>	
<div id="form_container">
 <div id="article_tabs">
    <ul>
      <li><a href="#fgeneral">General</a></li>
	  <span id='new_intro'>
      <li><a href="#fintro">Introduction</a></li>
	  </span>
	  <span id='new_class'>
      <li><a href="#fclass">Class</a></li>
	  </span>
	  <span id='new_indicate'>
      <li><a href="#findicate">Indication</a></li>
	  </span>
	  <span id='new_pharma'>
      <li><a href="#fpharma">Pharmacology</a></li>
	  </span>
	  <span id='new_caution'>
      <li><a href="#fcaution">Cautions</a></li>
	  </span>
	  <span id='new_undesire'>
      <li><a href="#fundesire">Undesirable Effects</a></li>
	  </span>
	  <span id='new_dose'>
      <li><a href="#fdose">Dose and Use</a></li>
	  </span>
	  <span id='new_over'>
      <li><a href="#fover">Overdose</a></li>
	  </span>
	  <span id='new_death'>
      <li><a href="#fdeath">Death Rattle</a></li>
	  </span>
	  <span id='new_supply'>
      <li><a href="#fsupply">Supply</a></li>
	  </span>
	  <span id='new_npost'>
      <li><a href="#fnpost">Post Text</a></li>
	  </span>
    </ul>    
	<form action="<?php echo url::site().'formulary/formulary_add'?>" class="appnitro" enctype="multipart/form-data" method="post" id="addformulary" name='addformulary'>
	<input type='hidden' name='id' id='id' value='<?php echo $form['id'];?>' />
	<input type='hidden' name='action' id='action' value='' />
	<div id='fgeneral'>
		<ul> 	
		  <li>
			<label class="description" for="lang">Language<span class="required">*</span></label>
			<div>
			  <select id="language_id" name="language_id" class="element select small" onchange="labelDE(this.value)">
				<?=$list_lang?>
			  </select>
			</div>
		  </li>		
		  <li>
			<label class="description" for="dtype">Content Type<span class="required">*</span></label>
			<div><select name="formulary_type" id="formulary_type" tabindex='2' class="element select small">
			  <option value='Drug Monographs' <?php if($form['formulary_type']=="Drug Monographs"){?>SELECTED<?php } ?>>Drug Monographs</option>
			  <option value='General Topics' <?php if($form['formulary_type']=="General Topics"){?>SELECTED<?php } ?>>General Topics</option>
			  <option value='Routes of Administration' <?php if($form['formulary_type']=="Routes of Administration"){?>SELECTED<?php } ?>>Routes of Administration</option>
			  <option value='Appendicies' <?php if($form['formulary_type']=="Appendicies"){?>SELECTED<?php } ?>>Appendicies</option>
			  <option value='Prelims' <?php if($form['formulary_type']=="Prelims"){?>SELECTED<?php } ?>>Prelims</option>
			</select></div>
		</li>
		<li>
			<label class="description" for="title">Monograph Title<span class="required">*</span></label>
			<div>
				<input id="formulary_title" name="formulary_title" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars(stripslashes($form['formulary_title'])) ?>"/> 
			</div> 
		</li>
		<li>
			<label class="description" for="title">Alias Title<span class="required">*</span></label>
			<div>
				<input id="alias" name="alias" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars(stripslashes($form['alias'])) ?>"/> 
			</div> 
		</li>
			<li>
				<label class="description" for="parent">Category Parent<span class="required">*</span></label>
				<div>
					<select name="formulary_parent_id" id="formulary_parent_id" tabindex='3' class="element select small">
					 <option value="0">Parent</option>
					<?=$parent_list;?>
					</select>
				</div> 
			</li>			
			<li id="bnfref" <?php if($form['language_id']==2){?>style="display:none;" <? }?>>
				<label class="description" for="title">BNF Reference<span class="required">*</span></label>
				<div>
					<input id="bnf_reference" name="bnf_reference" class="element text medium" type="text" maxlength="255" value="<?php echo html::specialchars($form['bnf_reference']) ?>"/> 
				</div> 
			</li>
			<li>
				<label class="description" for="parent">Level<span class="required">*</span></label>
				<div>
					<input type="text" name="formulary_level" id="formulary_level" value="<?=$form['formulary_level']?>" class="element text small"> 
					<select name="sel_formlevel" id="sel_formlevel" class="element select small" onchange="$('#formulary_level').val(this.value);">
					 <option value="">Select level</option>
					 <?php foreach($levels as $level){ ?>
					 <option value="<?=$level['formulary_level']?>" <?php if($form['formulary_level']==$level['formulary_level']){?>selected<?php } ?>><?=$level['formulary_level']?></option>
					 <?php } ?>
					</select>
				</div> 
			</li>
			<li>
				<label class="description" for="tags">Tags</label>		
				<div>
				   <input id="formulary_tags" name="formulary_tags" class="element text large" type="text" maxlength="255" value="<?php echo html::specialchars(stripslashes($form['formulary_tags'])) ?>"/> 
				</div>
			</li>			
			<li class="section_break">
				<h3>Manage Formulary Content</h3>
				<p>Select the blocks that define the pages content</p>
			</li>
			<li>
				<label class="description" for="blocks">Content Blocks</label>				
					<span>
						<?php $blocks	=	explode("#",$form['blocks']); ?>
						<input id="pre_text_article" name="pre_text_article" type="checkbox" value="1" <?php if(in_array('pre_text_article',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_intro');" />
						<label class="choice" for="element_14_1">Introduction <p class="delabel"> | Einführung</p></label>						
						<input id="class" name="class" type="checkbox" value="1" <?php if(in_array('class',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_class');" />
						<label class="choice" for="element_14_2">Class <p class="delabel">| Stoffgruppe</p></label>
						<input id="indication" name="indication" type="checkbox" value="1" <?php if(in_array('indication',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_indicate');" />
						<label class="choice" for="element_14_3">Indication <p class="delabel">| Indikationen</p></label>
						<input id="pharmacology" name="pharmacology" type="checkbox" value="1" <?php if(in_array('pharmacology',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_pharma');"  />
						<label class="choice" for="element_14_4">Pharmacology <p class="delabel">| Pharmakologie</p></label>
						<input id="cautions" name="cautions" type="checkbox" value="1" <?php if(in_array('cautions',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_caution');" />
						<label class="choice" for="element_14_3">Cautions <p class="delabel">| Warnhinweise</p></label>
						<input id="undesireable_effects" name="undesireable_effects" type="checkbox" value="1" <?php if(in_array('undesireable_effects',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_undesire');" />
						<label class="choice" for="element_14_3">Undesireable Effects <p class="delabel">| Nebenwirkungen</p></label>
						<input id="dose_and_use" name="dose_and_use" type="checkbox" value="1" <?php if(in_array('dose_and_use',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_dose');" />
						<label class="choice" for="element_14_3">Dose And Use <p class="delabel">| Dosierung mit Art und Dauer der Anwendung</p></label>
						<input id="overdose" name="overdose" type="checkbox" value="1" <?php if(in_array('overdose',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_over');" />
						<label class="choice" for="element_14_3">Overdose <p class="delabel">| Überdosis</p></label>
						<input id="death_rattle" name="death_rattle" type="checkbox" value="1" <?php if(in_array('death_rattle',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_death');" />
						<label class="choice" for="element_14_3">Death Rattle <p class="delabel">| Röcheln</p></label>
						<input id="supply" name="supply" type="checkbox" value="1" <?php if(in_array('supply',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_supply');" />
						<label class="choice" for="element_14_3">Supply <p class="delabel">| Verf&uuml;gbare Fertigarzneimittel (Auswahl)</p></label>
						<input id="post_text_article" name="post_text_article" type="checkbox" value="1" <?php if(in_array('post_text_article',$blocks)){?>checked<?php } ?> onclick="showBlock(this,'#new_npost');" />
						<label class="choice" for="element_14_3">Content after References <p class="delabel">| Inhalt nach Referenzen</p></label>
				</span>				
				<?php if($form['language_id']!=2){?>
				<script type="text/javascript">
					  $('.delabel').removeClass("active");
				</script>
				<? } ?>
			</li>
			<!-- li>
			  <label class="description" for="status">Publish Status </label>
			  <div>
				<select name="publish_status" id="publish_status" class="element select small">
				    <option value="draft" <? if($form['publish_status']=='draft'){?>selected<? }?>>Draft</option>
				   <option value="pending" <? if($form['publish_status']=='pending'){?>selected<? }?>>Pending</option>
				   <option value="approved" <? if($form['publish_status']=='approved'){?>selected<? }?>>Approved</option>
				   <option value="final" <? if($form['publish_status']=='final'){?>selected<? }?>>Final</option>
				</select>
			  </div>
			</li -->
		</ul>
	</div>
	<?php
		$intro_title		=	'';
		$class_title		=	'';
		$indication_title	=	'';
		$pharma_title		=	'';
		$cautions_title		=	'';
		$undesireable_title	=	'';
		$dose_title			=	'';
		$over_title			=	'';
		$death_title		=	'';
		$supply_title		=	'';
		$post_title			=	'';
	
	?>
	<div id="fintro">
		<ul>
		   <?php if(!empty($form['pre_text_article']->block_title)){
						$intro_title	=  html::specialchars($form['pre_text_article']->block_title);
				 }
				 /*else if($form['language_id']==2){
						$intro_title	=  'Einführung';
				  }else if($form['language_id']==1 || $form['language_id']==''){
						$intro_title	=  'Introduction';
				 }*/					 
			?>
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="intro_title" name="intro_title" class="element text medium" type="text" maxlength="255" value="<?=$intro_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="intro_text">Introduction Content<span class="required">*</span></label>
          <div>
            <textarea id="intro_text" name="intro_text" class="mceEditor"><?php if(!empty($form['pre_text_article'])){echo html::specialchars($form['pre_text_article']->block_content1);} ?></textarea>
          </div>
        </li>
      </ul>
	</div>
	<div id="fclass">
	  <ul>
			<?php if(!empty($form['class']->block_title)){
						$class_title	=  html::specialchars($form['class']->block_title);
				 }
				 /*else if($form['language_id']==2){
						$class_title	=  'Stoffgruppe';
				  }else if($form['language_id']==1 || $form['language_id']==''){
						$class_title	=  'Class';
				 }*/					 
			?>
	   <li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="class_title" name="class_title" class="element text medium" type="text" maxlength="255" value="<?=$class_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="class_content">Class Content<span class="required">*</span></label>
          <div>
            <textarea id="class_content" name="class_content" class="mceEditor-small"><?php if(!empty($form['class'])){echo html::specialchars($form['class']->block_content1);} ?></textarea>
          </div>
        </li>
	  </ul>
	</div>
	<div id="findicate">
	  <ul>
		<?php if(!empty($form['indication']->block_title)){
						$indication_title	=  html::specialchars($form['indication']->block_title);
				 }
				 /*else if($form['language_id']==2){
						$indication_title	=  'Indikationen';
				  }else if($form['language_id']==1 || $form['language_id']==''){
						$indication_title	=  'Indication';
				 }*/					 
			?>
	    <li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="indication_title" name="indication_title" class="element text medium" type="text" maxlength="255" value="<?=$indication_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="indication_content">Indications<span class="required">*</span></label>
          <div>
            <textarea id="indication_content" name="indication_content" class="mceSimple"><?php if(!empty($form['indication'])){echo html::specialchars($form['indication']->block_content1);} ?></textarea>
          </div>
        </li>
		<li>
          <label class="description" for="contra_content">Contra Indications<span class="required">*</span></label>
          <div>
            <textarea id="contra_content" name="contra_content" class="mceSimple"><?php if(!empty($form['indication'])){echo html::specialchars($form['indication']->block_content2);} ?></textarea>
          </div>
        </li>
	  </ul>
	</div>
	<div id="fpharma">
		<ul>
		<?php if(!empty($form['pharmacology']->block_title)){
						$pharma_title	=  html::specialchars($form['pharmacology']->block_title);
				 }
				 /*else if($form['language_id']==2){
						$pharma_title	=  'Pharmakologie';
				  }else if($form['language_id']==1 || $form['language_id']==''){
						$pharma_title	=  'Pharmacology';
				 }*/					 
			?>
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="pharma_title" name="pharma_title" class="element text medium" type="text" maxlength="255" value="<?=$pharma_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="pharma_content">Pharmacology Content<span class="required">*</span></label>
          <div>
            <textarea id="pharma_content" name="pharma_content" class="mceEditor"><?php if(!empty($form['pharmacology'])){echo html::specialchars($form['pharmacology']->block_content1);} ?></textarea>
          </div>
        </li>
	  </ul>
	</div>
	<div id="fcaution">
	  <ul>	
	   <?php if(!empty($form['cautions']->block_title)){
					$cautions_title	=  html::specialchars($form['cautions']->block_title);
			 }
			 /*else if($form['language_id']==2){
					$cautions_title	=  'Warnhinweise';
			  }else if($form['language_id']==1 || $form['language_id']==''){
					$cautions_title	=  'Cautions';
			 }*/					 
		?>
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="cautions_title" name="cautions_title" class="element text medium" type="text" maxlength="255" value="<?=$cautions_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="cautions_content">Cautions<span class="required">*</span></label>
          <div>
            <textarea id="cautions_content" name="cautions_content" class="mceEditor"><?php if(!empty($form['cautions'])){echo html::specialchars($form['cautions']->block_content1);} ?></textarea>
          </div>
        </li>
	   </ul>
	</div>
	<div id="fundesire">
	  <ul>
	     <?php if(!empty($form['undesireable_effects']->block_title)){
					$undesireable_title	=  html::specialchars($form['undesireable_effects']->block_title);
			 }
			 /*else if($form['language_id']==2){
					$undesireable_title	=  'Nebenwirkungen';
			  }else if($form['language_id']==1 || $form['language_id']==''){
					$undesireable_title	=  'Undesireable Efffects';
			 }*/					 
		?>
	    <li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="undesireable_title" name="undesireable_title" class="element text medium" type="text" maxlength="255" value="<?=$undesireable_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="undesireable_effect">Undesireable Effects<span class="required">*</span></label>
          <div>
            <textarea id="undesireable_effect" name="undesireable_effect" class="mceEditor"><?php if(!empty($form['undesireable_effects'])){echo html::specialchars($form['undesireable_effects']->block_content1);} ?></textarea>
          </div>
        </li>
	   </ul>
	</div>
	<div id="fdose">
	  <ul>
		<?php if(!empty($form['dose_and_use']->block_title)){
					$dose_title	=  html::specialchars($form['dose_and_use']->block_title);
			 }
			 /*else if($form['language_id']==2){
					$dose_title	=  'Dosierung mit Art und Dauer der Anwendung';
			  }else if($form['language_id']==1 || $form['language_id']==''){
					$dose_title	=  'Dose And Use';
			 }*/					 
		?>
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="dose_title" name="dose_title" class="element text medium" type="text" maxlength="255" value="<?=$dose_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="dose_use">Dose And Use<span class="required">*</span></label>
          <div>
            <textarea id="dose_use" name="dose_use" class="mceEditor"><?php if(!empty($form['dose_and_use'])){echo html::specialchars($form['dose_and_use']->block_content1);} ?></textarea>
          </div>
        </li>
	   </ul>
	</div>
	<div id="fover">
	  <ul>
		<?php if(!empty($form['overdose']->block_title)){
					$over_title	=  html::specialchars($form['overdose']->block_title);
			 }
			 /*else if($form['language_id']==2){
					$over_title	=  'Überdosis';
			  }else if($form['language_id']==1 || $form['language_id']==''){
					$over_title	=  'Over Dose';
			 }*/					 
		?>
	    <li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="over_title" name="over_title" class="element text medium" type="text" maxlength="255" value="<?=$over_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="over_dose">Over Dose<span class="required">*</span></label>
          <div>
            <textarea id="over_dose" name="over_dose" class="mceEditor"><?php if(!empty($form['overdose'])){echo html::specialchars($form['overdose']->block_content1);} ?></textarea>
          </div>
        </li>
	   </ul>
	</div> 
	<div id="fdeath">
	  <ul>
		 <?php if(!empty($form['death_rattle']->block_title)){
					$death_title	=  html::specialchars($form['death_rattle']->block_title);
			 }
			 /*else if($form['language_id']==2){
					$death_title	=  'Röcheln';
			  }else if($form['language_id']==1 || $form['language_id']==''){
					$death_title	=  'Death Rattle';
			 }*/					 
		?>
		 <li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="death_title" name="death_title" class="element text medium" type="text" maxlength="255" value="<?=$death_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="death_rattle_content">Death Rattle<span class="required">*</span></label>
          <div>
            <textarea id="death_content" name="death_content" class="mceEditor"><?php if(!empty($form['death_rattle'])){echo html::specialchars($form['death_rattle']->block_content1);} ?></textarea>
          </div>
        </li>
	   </ul>
	</div>
	<div id="fsupply">
		<ul>
		 <?php if(!empty($form['supply']->block_title)){
					$supply_title	=  html::specialchars($form['supply']->block_title);
			 }
			 /*else if($form['language_id']==2){
					$supply_title	=  'Verf&uuml;gbare Fertigarzneimittel (Auswahl)';
			  }else if($form['language_id']==1 || $form['language_id']==''){
					$supply_title	=  'Supply';
			 }*/					 
		?>
		 <li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="supply_title" name="supply_title" class="element text medium" type="text" maxlength="255" value="<?=$supply_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="supply_content">Supply<span class="required">*</span></label>
          <div>
            <textarea id="supply_content" name="supply_content" class="mceEditor"><?php if(!empty($form['supply'])){echo html::specialchars($form['supply']->block_content1);} ?></textarea>
          </div>
        </li>
	   </ul>
	</div>
	<div id="fnpost">
		<ul>
		 <?php if(!empty($form['post_text_article']->block_title)){
					$post_title	=  html::specialchars($form['post_text_article']->block_title);
			 }
			 /*else if($form['language_id']==2){
					$post_title	=  'Inhalt nach Referenzen';
			  }else if($form['language_id']==1 || $form['language_id']==''){
					$post_title	=  'Content after References';
			 }*/					 
		?>
		<li>
			<label class="description" for="title">Title<span class="required">*</span></label>
			<div>
				<input id="post_title" name="post_title" class="element text medium" type="text" maxlength="255" value="<?=$post_title?>"/> 
			</div> 
		</li>
		<li class='tmc'>
          <label class="description" for="post_content">Additional Content (to appear after References)<span class="required">*</span></label>
          <div>
            <textarea id="post_content" name="post_content" class="mceEditor"><?php if(!empty($form['post_text_article'])){echo html::specialchars($form['post_text_article']->block_content1);} ?></textarea>
          </div>
        </li>
	   </ul>
	</div>
	 <ul>
        <li class="buttons">
          <a id="saveForm" class="button" onclick="validate_formulary('save');">Save</a> <a id="saveForm" class="button" onclick="validate_formulary('save_version');">Save as Version</a>  <?php if(!empty($form['id'])){?><a id="saveForm" class="button" onclick="validate_formulary('duplicate');">Duplicate</a><?php } ?>
        </li>
	 </ul>			
	</form>
</div>
</div>
<script type="text/javascript">
<?php if(!in_array('pre_text_article',$blocks)){?>
  $('#new_intro').hide();
  //$('#fintro').hide();
<?php }if(!in_array('class',$blocks)){?>
  $('#new_class').hide();
  //$('#fclass').hide();
<?php }if(!in_array('indication',$blocks)){?> 
  $('#new_indicate').hide();
  //$('#findicate').hide();
<?php }if(!in_array('pharmacology',$blocks)){?>
  $('#new_pharma').hide();
  //$('#fpharma').hide();
<?php }if(!in_array('cautions',$blocks)){?>
  $('#new_caution').hide();
  //$('#fcaution').hide();
<?php }if(!in_array('undesireable_effects',$blocks)){?> 
  $('#new_undesire').hide();
  //$('#fundesire').hide();
<?php }if(!in_array('dose_and_use',$blocks)){?>
  $('#new_dose').hide();
  //$('#fdose').hide();
<?php }if(!in_array('overdose',$blocks)){?>
  $('#new_over').hide();
  //$('#fover').hide();
 <?php }if(!in_array('death_rattle',$blocks)){?>
  $('#new_death').hide();
  //$('#fdeath').hide();
 <?php }if(!in_array('supply',$blocks)){?>
  $('#new_supply').hide();
  //$('#fsupply').hide();
 <?php }if(!in_array('post_text_article',$blocks)){?>
  $('#new_npost').hide();  
  //$('#fnpost').hide();
 <?php } ?>
</script>