<?php

class Application_Model_Customers
{
	private $_db;    

    public function __construct()
    {
        $this->_db		= Zend_Registry::get("db");
		$this->_baseurl	= Zend_Registry::get("baseUrl");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_customers WHERE customer_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_customers','view',$id,'success');
            return $result->fetchAll();
        }
    }	

	public function getCountries()
    {
        $sql = "SELECT id,country FROM 3bit_country";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_customers SET
								title			=	'".addslashes($data['title'])."',
								fname			=	'".addslashes($data['fname'])."',
								lname			=	'".addslashes($data['lname'])."',
								email			=	'".addslashes($data['email'])."',
								address1		=	'".addslashes($data['address1'])."',
								address2		=	'".addslashes($data['address2'])."',
								city			=	'".addslashes($data['city'])."',
								state			=	'".addslashes($data['state'])."',
								county			=	'".addslashes($data['county'])."',
								country			=	'".addslashes($data['country'])."',
								postcode		=	'".addslashes($data['postcode'])."',
								phone			=	'".addslashes($data['phone'])."',
								password		=	'".base64_encode($data['password'])."',									
								modified_date	=	now() WHERE customer_id ='".$data['id']."'";
								
		$query	=	$this->_db->query($sql);

		$customerid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_customers','update',$customerid,'success');
    }	

	// Function to order history for the selected customer -------------------------------

	public function history($custid){

			$selName =  $this->_db->fetchRow("SELECT fname,lname FROM 3bit_customers WHERE customer_id =	'".$custid."'");
			
			if($selName){
			
				$fname	=	$selName['fname'];
				
				$lname	=	$selName['lname'];
				
			}

			$hisList	=	'<h3>Order History for '.ucfirst($fname).'&nbsp;'.ucfirst($lname).'</h3>';

			$selHis	=	$this->_db->fetchAll("SELECT * FROM 3bit_orders WHERE customer_id ='".$custid."'");
			
			if($selHis){				
				
				$hisList	.=	'<table cellpadding="0" cellspacing="0" width="100%" class="display">
								<thead>
								<tr>
									<th>Order Number</th>
									<th>Status</th>
									<th>Total Amount</th>								
									<th>Action</th>
								</tr>
								</thead>';
					$i=0;
				
				foreach($selHis as $row){
					$i++;
					if($i%2 == 0)
						$bgcolor = "#EDEDED";
					else
						$bgcolor = "#FFFFFF";
					
					$hisList	.=	'<tr bgcolor="'.$bgcolor.'">
										<td>'.$row['order_id'].'</td>
										<td>'.$row['status'].'</td>
										<td>'.$row['ordertotal'].'</td>';
					if($row['status']=='Refunded'){
						$hisList	.=	'<td><a href="'.$this->_baseurl.'/orders/orderhistory/id/'.$row['order_id'].'" rel="facebox">View</a>&nbsp;|&nbsp;<a style="cursor:pointer" onclick=\'javascript:preview_order("'.$this->_baseurl.'/orders/refundprint/id/'.$row['order_id'].'");\'>Invoice</a></td>';
					}else{
						$hisList	.=	'<td><a href="'.$this->_baseurl.'/orders/orderhistory/id/'.$row['order_id'].'"  rel="facebox">View</a>&nbsp;|&nbsp;<a style="cursor:pointer" onclick=\'javascript:preview_order("'.$this->_baseurl.'/orders/print/id/'.$row['order_id'].'/type/'.$row['status'].'");\'>Invoice</a>';
						if($row['status']=='New' || $row['status']=='Deposited' || $row['status']=='Accepted'){
							$hisList	.=	'&nbsp;|&nbsp;<a href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'">Process</a>';
						}else if($row['status']=='Shipped'){
							$hisList	.=	'&nbsp;|&nbsp;<a href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'">Refund</a>';
						}

						$hisList	.=	'</td>';
					}
					$hisList	.=	'</tr>';
				
				}
				$hisList	.=	'<tr>		
									<td colspan="3">
										<input type="text" id="txtHis" name="txtHis" style="width:300px;"/>
									</td>
									<td>
										<input type="button" name="add" value="Add" onclick=\'addCHistory("'.$custid.'",document.getElementById("txtHis").value)\'/>
									</td>
								</tr>
								<tr>
									<td colspan="4">
										<fieldset>
											<legend>Customer Notes</legend>
												<span id="cust_history'.$custid.'"><table width="100%">
											';
				$selChis	=	$this->_db->fetchAll("SELECT *,date_format(history_date,'%D %b %Y') as hdate,date_format(history_date,'%H:%i')as htime FROM 3bit_order_history WHERE customer_id ='".$custid."' order by history_date");	
					

				if($selChis){
						$i	=	0;					
						
						foreach($selChis as $row){								
						$i++;
						if($i%2 == 0)
							$bgcolor = "#EDEDED";
						else
							$bgcolor = "#FFFFFF";
				
					$hisList	.=	'<tr bgcolor="'.$bgcolor.'">
										<td>'.$row['hdate'].'&nbsp;@&nbsp;'.$row['htime'].'</td>
										<td colspan="2">'.$row['status'].'</td>
									<tr>';
						}	
				}
				$hisList	.=	'			</table></span>
										</fieldset>
									<td>
								</tr>	
							</table>';
				
				return $hisList;
			}else{
				return $hisList;
			}

	}
}

?>