<?php

class Application_Model_Invites
{
	private $_db;    

	public function __construct()
	{
			$this->_db = Zend_Registry::get("db");
	}  	

	public function delete($id){

		$this->_db->delete("new_users",'id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('new_users','delete',$id,'success');
	}

	//************************** Get condition name form the array **************************//

	public function getCondition($fieldname,$condition){
		 global $fields,$conditions;  

		 $type =	$fields[$fieldname];

		 $conditionname =	array_search($condition, $conditions[$type]); 

		 return $conditionname;
	}

	//************************** form the query from conditions **************************//
	 
	 public function getQuery($rule_id){

		$selcond			=	$this->_db->query("SELECT * FROM 3bit_conditions WHERE rule_id='".$rule_id."'")->fetchAll();

		if($selcond){

			foreach($selcond as $res){

				$field_name	=	$res['field_name'];
				$field_cond	=	$res['field_condition'];
				$field_value=	$res['field_value'];
				if($field_cond=="==")
					$field_cond = "=";
				if($field_cond=="{}" || $field_cond=="!{}" || $field_cond=="()" || $field_cond=="!()"){  
					if($field_cond=="{}" )
						$conditions .= 	" AND ".$field_name." LIKE ".'"%'.$field_value.'%"';
					if($field_cond=="!{}")
						$conditions .= 	" AND ".$field_name." NOT LIKE ".'"%'.$field_value.'%"';	
					if($field_cond=="()" )
						$conditions .= 	" AND FIND_IN_SET ".'('.$field_name.',"'.$field_value.'")';
					if($field_cond=="!()")
						$conditions .= 	" AND NOT FIND_IN_SET ".'('.$field_name.',"'.$field_value.'")';

				}else{
					$conditions .= 	" AND ".$field_name." ".$field_cond.' "'.$field_value.'"';
				}

			} 
		}
		return $conditions;
	}


	public function sendinvites($data)
  {
		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;*/

		$sgroup_id	=		$data['sgroup_id'];

		$subject		=		$data['subject'];

		$html_text	=		$data['html_message'];

		$plain_text	=		$data['text_message'];	

		$conditions	=		'';

		$records_limit=		0;
		
		$no_records	  =		0;	

		$this->_db->getConnection()->query("INSERT INTO 3bit_survey_invites SET
																			sgroup_id			=			'".$sgroup_id."',
																			subject				=			'".addslashes($subject)."',
																			html_message	=			'".addslashes($html_text)."',
																			text_message	=			'".addslashes($plain_text)."',
																			status				=			'1',
																			created_date	=			now(),
																			modified_date	=			now()");

		$invite_id	=	$this->_db->lastInsertId();

		$selectRule	=	$this->_db->fetchRow("SELECT rule_id FROM 3bit_survey_group WHERE sgroup_id='".$sgroup_id."' AND status ='1'");

		if($selectRule){
			$rule_id	=		$selectRule['rule_id'];
		}

		if($rule_id != ''){

			require_once $_SERVER['DOCUMENT_ROOT'].'/logincms/ext/Swift/lib/swift_required.php';

			$selC		=		$this->_db->fetchRow("SELECT combined_rules,records_limit,no_records,rule_name FROM 3bit_rules WHERE rule_id='".$rule_id."'");

			if($selC){
				$rules					=		$selC['combined_rules'];
				$records_limit	=		$selC['records_limit'];
				$no_records			=		$selC['no_records'];
				$rule_name			=		$selC['rule_name'];
				if($rules!=''){
					$exp					=		explode("##",$rules);
					$rule1				=		$exp[0];
					$rule2				=		$exp[2];
					$operator			=		$exp[1];
					$cond1				=		substr($this->getQuery($rule1),4);
					$cond2				=		substr($this->getQuery($rule2),4);
					$conditions		.= 	" AND ( ( ".$cond1." ) ".$operator." ( ".$cond2.")  ) ";
				}else{
					$qry					=		$this->getQuery($rule_id);	
					$conditions		.=	$qry;
				}			
			}
			if($records_limit>0 && $no_records>0){
			  $limits		=	" LIMIT ".$records_limit." , ".$no_records; 
			}else if($records_limit==0 && $no_records>0){
			  $limits		=	" ORDER BY RAND() LIMIT ".$no_records; 
			}		
			
			if($conditions !=''){

				$memsql		=		$this->_db->query("SELECT id,email,FirstName,LastName FROM new_users WHERE  1 ".$conditions.$limits."")->fetchAll();				

				$k				=		0;

				$insertids	=		'';				

				if($memsql){

					$ukpos	=	strpos($rule_name, "UK_LIST");

					$sppos	=	strpos($rule_name, "SP_LIST");					

					foreach($memsql as $res){

						$insertids		.=	$res['id'].",";	

						$insertsql		=	$this->_db->getConnection()->query("INSERT INTO 3bit_survey_invites_status SET invite_id='".$invite_id."',id='".$res['id']."',email='".addslashes($res['email'])."',status='0',created_date=now(),modified_date=now()");							

						$htmltag		= 	str_replace("{{id}}",$res['id'], $html_text);
						$htmltag		= 	str_replace("{{FirstName}}",stripslashes($res['FirstName']), $htmltag);
						$htmltag		= 	str_replace("{{LastName}}",stripslashes($res['LastName']), $htmltag);
						if($plain_text !=''){
							$texttag		= 	str_replace("{{id}}",$res['id'], $plain_text);
							$texttag		= 	str_replace("{{FirstName}}",stripslashes($res['FirstName']), $texttag);
							$texttag		= 	str_replace("{{LastName}}",stripslashes($res['LastName']), $texttag);
						}

						/*if($ukpos !== false) {
							$ukres			=		$this->_db->fetchRow("SELECT link FROM uk_list WHERE 1 LIMIT $k,1");

							if($ukres){

							  $url				=		$ukres['link'];

							  $htmltag		= 	str_replace("{{ALLGLOBALURL}}",stripslashes($url), $htmltag);

							  if($plain_text !='')
								  $texttag		= 	str_replace("{{ALLGLOBALURL}}",stripslashes($url), $texttag);

							  $filename		=	'uk_list.txt';

							}

						}else if($sppos !== false){
						   
						   $spres		=		$this->_db->fetchRow("SELECT link FROM sp_list WHERE 1 LIMIT $k,1");

							if($spres){

							   $url				=		$spres['link'];

							   $htmltag		= 	str_replace("{{ALLGLOBALURL}}",stripslashes($url), $htmltag);

							   if($plain_text !='')
								  $texttag		= 	str_replace("{{ALLGLOBALURL}}",stripslashes($url), $texttag);

							   $filename		=	'sp_list.txt';

							} 
						}*/		
						
						$email	=	stripslashes($res['email']);

						//$email		=	'jat@3bit.co.uk';

						$lotsOfRecipients[$k]['email']	=	$email;
						$lotsOfRecipients[$k]['html']		=	$htmltag;
						$lotsOfRecipients[$k]['text']		=	$texttag;

						//$txturls	.=	 $email."  ".$htmltag."\n";	
						$k++;															
						
					}	
									
					$this->_db->getConnection()->query("UPDATE 3bit_survey_invites SET invited_members	=	'".substr($insertids,0,-1)."' WHERE invite_id='".$invite_id."'");

					$message = Swift_Message::newInstance()
				
						->setCharset('iso-8859-2')
											
						//Give the message a subject
						->setSubject($subject)

						//Set the From address with an associative array
						->setFrom(array('hq@palliativedrugs.com' => 'Palliativedrugs.com'));

						//Set the To addresses with an associative array
						//->setTo(array('suba@3bit.co.uk'))

						//Give it a body
						//->setBody($texttag)

						//And optionally an alternative body
						//->addPart($htmltag, 'text/html');

						//if($ukpos !== false)
						//	$message->setLanguage("uk");

						//else if($sppos !== false)
						//	 $message->setLanguage("es");

					$transport = Swift_MailTransport::newInstance();

					$mailer = Swift_Mailer::newInstance($transport);

					$logger = new Swift_Plugins_Loggers_EchoLogger();

					$mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));

					// Sending lot of emails from here //
					foreach($lotsOfRecipients as $recipient) {	
						$message->setTo($recipient['email'])->setBody($recipient['text'])->addPart($recipient['html'], 'text/html');		  
						$result		=		$mailer->send($message);
						if($result){
							$updatesql	=	"UPDATE 3bit_survey_invites_status SET status='1',modified_date=now() WHERE invite_id='".$invite_id."' AND id='".$recipient['id']."'";
							$this->_db->getConnection()->query($updatesql);
						}
					}

					$log_content	=	$logger->dump();

					//echo __FILE__;

					$myFile			= $_SERVER['DOCUMENT_ROOT']."/logincms/application/models/swiftlog.txt";

					$fh				= @fopen($myFile, 'a+');				

					fwrite($fh, $log_content);

					fclose($fh);
					
					//$handle = fopen($filename, 'w+');
					//if (fwrite($handle, $txturls) !== FALSE) {
					//	  fclose($handle);
					//}   				
				}
			}
		}	
  }

  public function sendreinvites($data)
  {
		//echo "<pre>";
		//print_r($data);
		//echo "</pre>";
		

		$invite_id		=		$data['invite_id'];

		$subject		=		$data['subject'];

		$html_text		=		$data['html_message'];

		$plain_text		=		$data['text_message'];	

		$selInvites		=		$this->_db->fetchRow("SELECT concat(incompleted_members,',',never_members,',',completed_members) as members,invited_members as sent_members,subject,html_message,html_message FROM 3bit_survey_invites WHERE invite_id='".$invite_id."'");	

		if(count($selInvites)>0){

			$subject		=		$selInvites['subject'];

			$html_text		=		$selInvites['html_message'];
	
			$plain_text		=		$selInvites['text_message'];

			$invited_members	=	$selInvites['members'];

			$invited_members	=	explode(",",$invited_members);

			$sent_members			=	explode(",",$selInvites['sent_members']);

		}

		$invited_array		=	array_filter($invited_members);

		$sent_array				=	array_filter($sent_members);

		//$this->_db->getConnection()->query("DELETE FROM 3bit_survey_invites_status WHERE invite_id='14' ");

		//echo "<pre>";

		//print_r($sent_array);

		//print_r($invited_array);

		$k=1;

		if(trim($data['type'])=='resend'){

			$subject		=		$data['subject'];

			$html_text		=		$data['html_message'];
	
			$plain_text		=		$data['text_message'];	

			foreach($sent_array as $memberid){

				$selInvite	=		$this->_db->fetchRow("SELECT * FROM 3bit_survey_invites_status WHERE invite_id='".$invite_id."' AND id='".$memberid."'");

				$memsql		=		$this->_db->fetchRow("SELECT id,email,FirstName,LastName FROM new_users WHERE  id='".$memberid."'");		
					
				if(!$selInvite){					
					$insertsql	=	"INSERT INTO 3bit_survey_invites_status SET invite_id='".$invite_id."',id='".$memberid."',email='".addslashes($memsql['email'])."',status='0',created_date=now(),modified_date=now()";
					$this->_db->getConnection()->query($insertsql);
					//echo $k.".".$insertsql."<br>";
				}else{
					$updatesql	=	"UPDATE 3bit_survey_invites_status SET status='0',modified_date=now() WHERE invite_id='".$invite_id."' AND id='".$memberid."'";
					$this->_db->getConnection()->query($updatesql);
					//echo $k.".".$updatesql."<br>";
				}
				$k++;
			}
		}

		$k=1;

		foreach($invited_array as $memberid){

			$selInvite	=		$this->_db->fetchRow("SELECT * FROM 3bit_survey_invites_status WHERE invite_id='".$invite_id."' AND id='".$memberid."'");
			
			$memsql		=		$this->_db->fetchRow("SELECT id,email,FirstName,LastName FROM new_users WHERE  id='".$memberid."'");			
			if(!$selInvite){					
				$insertsql	=	"INSERT INTO 3bit_survey_invites_status SET invite_id='".$invite_id."',id='".$memberid."',email='".addslashes($memsql['email'])."',status='0',created_date=now(),modified_date=now()";
				$this->_db->getConnection()->query($insertsql);
				//echo $k.".".$insertsql."<br>";
			}else{
				$updatesql	=	"UPDATE 3bit_survey_invites_status SET status='1',modified_date=now() WHERE invite_id='".$invite_id."' AND id='".$memberid."'";
				$this->_db->getConnection()->query($updatesql);
				//echo $k.".".$updatesql."<br>";
			}

			$k++;

		}

		/*$this->_db->getConnection()->query("UPDATE 3bit_survey_invites SET																			
																			subject				=			'".addslashes($subject)."',
																			html_message	=			'".addslashes($html_text)."',
																			text_message	=			'".addslashes($plain_text)."',
																			status				=			'1',
																			created_date	=			now(),
																			modified_date	=			now() WHERE invite_id='".$invite_id."'");*/


		$selMembers		=		$this->_db->query("SELECT u.id,u.email,u.FirstName,u.LastName FROM new_users u,3bit_survey_invites_status ss WHERE u.id=ss.id AND ss.invite_id='".$invite_id."' AND ss.status='0'")->fetchAll();

		$failed_members	=	count($selMembers);

		//echo $failed_members;

		//exit;

		if($failed_members>0 && !empty($data['invite_id'])){

				$k					=		0;

				foreach($selMembers as $row){
							$htmltag		= 	str_replace("{{id}}",$row['id'], $html_text);
							$htmltag		= 	str_replace("{{FirstName}}",stripslashes($row['FirstName']), $htmltag);
							$htmltag		= 	str_replace("{{LastName}}",stripslashes($row['LastName']), $htmltag);
							if($plain_text !=''){
								$texttag		= 	str_replace("{{id}}",$row['id'], $plain_text);
								$texttag		= 	str_replace("{{FirstName}}",stripslashes($row['FirstName']), $texttag);
								$texttag		= 	str_replace("{{LastName}}",stripslashes($row['LastName']), $texttag);
							}

							$email	=	stripslashes($row['email']);
							//$email	=	'jat@3bit.co.uk';
							$lotsOfRecipients[$k]['id']	=	$row['id'];
							$lotsOfRecipients[$k]['email']	=	$email;
							$lotsOfRecipients[$k]['html']		=	$htmltag;
							$lotsOfRecipients[$k]['text']		=	$texttag;
							$k++;
				}

				/*echo "<pre>";
				print_r($lotsOfRecipients);
				echo "</pre>";
				exit;*/

				require_once $_SERVER['DOCUMENT_ROOT'].'/logincms/ext/Swift/lib/swift_required.php';

				$message = Swift_Message::newInstance()
						
								->setCharset('iso-8859-2')
													
								//Give the message a subject
								->setSubject($subject)

								//Set the From address with an associative array
								->setFrom(array('hq@palliativedrugs.com' => 'Palliativedrugs.com'));

				$transport = Swift_MailTransport::newInstance();

				$mailer = Swift_Mailer::newInstance($transport);

				$mailer->registerPlugin(new Swift_Plugins_ThrottlerPlugin(
					100, Swift_Plugins_ThrottlerPlugin::MESSAGES_PER_MINUTE
				));

				$logger = new Swift_Plugins_Loggers_EchoLogger();

				$mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));

				$txturls	=	'';

				// Sending lot of emails from here //
				foreach($lotsOfRecipients as $recipient) {	
					$message->setTo($recipient['email'])->setBody($recipient['text'])->addPart($recipient['html'], 'text/html');		  
					$result	=	$mailer->send($message);
					if($result){
						$updatesql	=	"UPDATE 3bit_survey_invites_status SET status='1',modified_date=now() WHERE invite_id='".$invite_id."' AND id='".$recipient['id']."'";
						$this->_db->getConnection()->query($updatesql);
					}
				}

				$log_content	=	$logger->dump();					

				$myFile			= $_SERVER['DOCUMENT_ROOT']."/logincms/application/models/swiftlog.txt";

				$fh				= @fopen($myFile, 'a+');				

				fwrite($fh, $log_content);

				fclose($fh);	
		}

	}

	public function getGroups()
	{

		$result		= $this->_db->query("SELECT sg.sgroup_id,sg.sgroup_title FROM 3bit_survey_group sg WHERE sg.status='1' ORDER BY sg.sgroup_title ASC");

		$res	=  $result->fetchAll();

		return $res;
	}

	public function getUserFields()
	{
		$result	=	$this->_db->query("SELECT * FROM new_users LIMIT 0,1");
		$res	=	$result->fetchAll();
		if(is_array($res[0])){
			$fields	= (array_keys($res['0']));
			foreach($fields as $k => $v) {
				$dropField	.=	 '<option value="'.$v.'">'.$v.'</option>';
			}
			return $dropField;
		}
	}




}
?>