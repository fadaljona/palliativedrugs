<?php

class Application_Model_Drugs
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_drugs WHERE drug_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_drugs','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function parent_formulary()
    {
        return $this->_db->fetchAll('SELECT formulary_title,formulary_id FROM  3bit_formulary WHERE status="1" AND formulary_parent_id="0" ORDER BY formulary_title ASC');
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_drugs SET
						formulary_id	=	'".$data['formulary_id']."',									
						block_type		=	'drugs',
						drug_name		=	'".addslashes($data['drug_name'])."',
						brand			=	'".addslashes($data['brand'])."',
						manufacture		=	'".addslashes($data['manufacture'])."',
						manufacture_url	=	'".addslashes($data['manufacture_url'])."',
						strength		=	'".addslashes($data['strength'])."',
						formulation		=	'".addslashes($data['formulation'])."',
						packsize		=	'".addslashes($data['packsize'])."',
						cost_quoted		=	'".addslashes($data['cost_quoted'])."',
						nof_doses		=	'".addslashes($data['nof_doses'])."',
						cost_per_udose	=	'".addslashes($data['cost_per_udose'])."',
						nof_dose_pday	=	'".addslashes($data['nof_dose_pday'])."',
						cost_28days		=	'".addslashes($data['cost_28days'])."',
						comments		=	'".addslashes($data['comments'])."',
						status			=	'".addslashes($data['status'])."',
						created_date	=	now(),
						modified_date	=	now()";

		$query	=	$this->_db->query($sql);

		$drugid	=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_drugs','add',$drugid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_drugs SET
							formulary_id	=	'".$data['formulary_id']."',									
							block_type		=	'drugs',
							drug_name		=	'".addslashes($data['drug_name'])."',
							brand			=	'".addslashes($data['brand'])."',
							manufacture		=	'".addslashes($data['manufacture'])."',
							manufacture_url	=	'".addslashes($data['manufacture_url'])."',
							strength		=	'".addslashes($data['strength'])."',
							formulation		=	'".addslashes($data['formulation'])."',
							packsize		=	'".addslashes($data['packsize'])."',
							cost_quoted		=	'".addslashes($data['cost_quoted'])."',
							nof_doses		=	'".addslashes($data['nof_doses'])."',
							cost_per_udose	=	'".addslashes($data['cost_per_udose'])."',
							nof_dose_pday	=	'".addslashes($data['nof_dose_pday'])."',
							cost_28days		=	'".addslashes($data['cost_28days'])."',
							comments		=	'".addslashes($data['comments'])."',
							status			=	'".addslashes($data['status'])."',										
							modified_date	=	now() WHERE drug_id='".$data['id']."'";

		$query	=	$this->_db->query($sql);

		$drugid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_drugs','update',$drugid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_drugs",'drug_id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_drugs','delete',$id,'success');
	}
}

?>