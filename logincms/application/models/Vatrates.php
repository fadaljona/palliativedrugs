<?php

class Application_Model_Vatrates
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_product_vats WHERE vat_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_product_vats','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_product_vats SET
								vat_code				=	'".addslashes($data['vat_code'])."',
								vat_rate				=	'".addslashes($data['vat_rate'])."',
								status					=	'".$data['status']."',
								created_date			=	now(),
								modified_date			=	now()";

		$query	=	$this->_db->query($sql);

		$vatid	=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_product_vats','add',$vatid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_product_vats SET
								vat_code				=	'".addslashes($data['vat_code'])."',
								vat_rate				=	'".addslashes($data['vat_rate'])."',
								status					=	'".$data['status']."',
								modified_date			=	now() WHERE vat_id='".$data['id']."'";
		$query	=	$this->_db->query($sql);

		$vatid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_product_vats','update',$vatid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_product_vats",'vat_id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_product_vats','delete',$id,'success');
	}
}

?>