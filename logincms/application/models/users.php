<?php

class Application_Model_Users
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    

    public function getUsertypes()
    {
        $sql = "SELECT type_id,title FROM 3bit_user_types";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_users WHERE user_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_users','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_users SET
								user_firstname		=	'".addslashes($data['user_firstname'])."',
								user_lastname		=	'".addslashes($data['user_lastname'] )."',
								user_username		=	'".addslashes($data['user_username'])."',
								user_password		=	'".addslashes(base64_encode($data['user_password']))."',
								user_email			=	'".addslashes($data['user_email'])."',
								type_id				=	".addslashes($data['type_id']).",	
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now()";

		$query	=	$this->_db->query($sql);

		$userid	=	$this->_db->lastInsertId();	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_users','add',$userid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_users SET
								user_firstname		=	'".addslashes($data['user_firstname'])."',
								user_lastname		=	'".addslashes($data['user_lastname'] )."',
								user_username		=	'".addslashes($data['user_username'])."',
								user_password		=	'".addslashes(base64_encode($data['user_password']))."',
								user_email			=	'".addslashes($data['user_email'])."',
								type_id				=	'".addslashes($data['type_id'])."',																
								status				=	'".$data['status']."',								
								modified_date		=	now() WHERE user_id='".$data['id']."'";
		$query	=	$this->_db->query($sql);

		$userid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_users','update',$userid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_users",'user_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_users','delete',$id,'success');
	}
}

?>