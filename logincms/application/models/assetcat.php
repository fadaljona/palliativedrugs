<?php

class Application_Model_Assetcat
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_asset_categories WHERE asset_category_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_asset_categories','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {			
		$sql	=	"INSERT INTO 3bit_asset_categories SET
								asset_category_title			=	'".addslashes($data['asset_category_title'])."',
								asset_category_type				=	'".addslashes($data['asset_category_type'])."',
								asset_category_description		=	'".addslashes($data['asset_category_description'] )."',
								status							=	'".$data['status']."',
								created_date					=	now(),
								modified_date					=	now()";

		$query	=	$this->_db->query($sql);

		$catid	=	$this->_db->lastInsertId();	
		
		$ftype		=	$data['asset_category_type'];
		
		$path = '../../assets/'.$ftype.'/'.$catid."/";
		@mkdir ($path, 0777);   
	   	
		if($ftype=='images'){
			$path_thumb = '../../assets/thumbs/'.$catid."/";
			@mkdir ($path_thumb, 0777);
		}

		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_asset_categories','add',$catid,'success');
    }

	public function update($data)
	{

		$sql	=	"UPDATE 3bit_asset_categories SET
								asset_category_title			=	'".addslashes($data['asset_category_title'])."',								
								asset_category_description		=	'".addslashes($data['asset_category_description'] )."',
								status							=	'".$data['status']."',				
								modified_date					=	now() WHERE asset_category_id='".$data['id']."'";
		
		$query	=	$this->_db->query($sql);

		$catid	=	$data['id'];
		
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_asset_categories','update',$catid,'success');
		
    }

	public function delete($id){

		$this->_db->delete("3bit_asset_categories",'asset_category_id='.$id); 
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_asset_categories','deleted',$id,'success');
	}
}

?>