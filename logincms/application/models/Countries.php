<?php

class Application_Model_Countries
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_country WHERE id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_country','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_country SET
								zone_id				=	'".addslashes($data['zone_id'])."',
								iso_code			=	'".addslashes($data['iso_code'] )."',
								country				=	'".addslashes($data['country'])."'";

		$query	=	$this->_db->query($sql);

		$cid	=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_country','add',$cid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_country SET
								zone_id				=	'".addslashes($data['zone_id'])."',
								iso_code			=	'".addslashes($data['iso_code'] )."',
								country				=	'".addslashes($data['country'])."' WHERE id='".$data['id']."'";
		$query	=	$this->_db->query($sql);

		$cid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_country','update',$cid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_country",'id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_country','delete',$id,'success');
	}
}

?>