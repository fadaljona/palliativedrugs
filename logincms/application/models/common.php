<?php defined('SYSPATH') or die('No direct script access.');

class Common_Model extends Model {

	// Function to change status of the records in all module//

	public function change_status($data)
    {
		$sID			= $data["id"];
    
		$sQuery			= $this->db->query("SELECT ".$data['f']."  FROM ".$data['t']." WHERE ".$data['idf']."='".$sID."'");

		$oCount			= $sQuery->count();

		$this->audit	= new Audits_Model;

		if( $oCount > 0){
			$updateEvent	= $this->db->query("UPDATE ".$data['t']." SET ".$data['f']." = '".$data['s']."' WHERE ".$data['idf']." = '".$sID."'");			
						
			if($data['s']=='1'){								
				$this->audit->insert_audit($data['t'],'active',$sID,'success');	
			}else{
				$this->audit->insert_audit($data['t'],'inactive',$sID,'success');	
			}

			$caseA			=	"<a onclick=\"javascript:change_status('".$sID."','".$data['t']."','".$data['f']."','0','".$data['idf']."');\" href='javascript:;'><font color=\"#008000\">Active</font></a>";
			$caseI			=	"<a onclick=\"javascript:change_status('".$sID."','".$data['t']."','".$data['f']."','1','".$data['idf']."');\" href='javascript:;'><font color=\"#ff0000\">Inactive</font></a>";

			$case			= ($data['s']=='1')?$caseA:$caseI;

	    }else{
            $case = "ID $sID doesn't exist.";
		}
		
		return ($case . "###" . $data['s']);
    }

	// Function to show user details in highslide //

	public function user_details($userid){

		$query			= $this->db->select("*")->where('user_id',$userid)->from('users')->get();		

		$count			= $query->count();

		$result			= $query->result_array();

		$level			= $this->db->query("SELECT `level_name` FROM `levels` WHERE level_id='".$result[0]->level_id."'")->result_array();

		$dept			= $this->db->query("SELECT `dept_name` FROM `departments` WHERE dept_id='".$result[0]->dept_id."'")->result_array();		

		if($count>0){
	   ?>
	   <fieldset>
			<legend>User Details</legend>
			<ul>			
				<li class="details" >First Name : <?php echo $result[0]->first_name;?> </li>
				<li class="details">Last Name : <?php echo $result[0]->last_name;?> </li>
				<li class="details">User Name : <?php echo $result[0]->username;?></li>
				<li class="details">Password  : <?php echo $result[0]->password;?></li>
				<li class="details">Email : <?php echo $result[0]->email;?></li>
				<li class="details">Department : <?php echo $dept[0]->dept_name;?></li>
				<li class="details">Level :<?php echo $level[0]->level_name;?></li>
				<li class="details">Redirect Url : <?php echo $result[0]->redirect_url;?></li>
				<li class="details">Active : <?php if($result[0]->status=='1'){ echo "Active"; }else{ echo "Inactive";}?> </li>
			</ul>
			</fieldset>
	   <?php
		}
	}
	// Function to show asset details in highslide //

	public function asset_details($assetid){

		$query			= $this->db->select("*")->where('asset_id',$assetid)->from('assets')->get();		

		$count			= $query->count();

		$result			= $query->result_array();

		$categ			= $this->db->query("SELECT `asset_category_title` FROM `asset_categories` WHERE asset_category_id='".$result[0]->asset_category_id."'")->result_array();	

		if($count>0){
	   ?>
	   <fieldset>
			<legend>Asset Details</legend>
			<ul>			
				<li class="details" >Asset Title : <?php echo $result[0]->asset_title;?> </li>
				<li class="details">Asset Name : <?php echo $result[0]->asset_name;?> </li>
				<li class="details">Category : <?php echo $categ[0]->asset_category_title;?></li>
				<li class="details">Asset Alt  : <?php echo $result[0]->asset_alt;?></li>
				<li class="details">Asset Height : <?php echo $result[0]->asset_height;?></li>
				<li class="details">Asset Width : <?php echo $result[0]->asset_width;?></li>
				<li class="details">Asset Size :<?php echo $result[0]->asset_size;?></li>				
				<li class="details">Active : <?php if($result[0]->status=='1'){ echo "Active"; }else{ echo "Inactive";}?> </li>
			</ul>
			</fieldset>
	   <?php
		}
	}

	public function article_details($articleid){

		$query			= $this->db->select("*")->where('article_id',$articleid)->from('articles')->get();		

		$count			= $query->count();

		$result			= $query->result_array();

		if($count>0){
	   ?>
	   <fieldset>
			<legend>Article Details</legend>
			<ul>			
				<li class="details" >Article Title : <?php echo $result[0]->article_title;?> </li>
				<li class="details">Long Text : <?php echo $result[0]->long_text;?> </li>
				<li class="details">Date : <?php echo $result[0]->article_date;?></li>
				<li class="details">Meta Title  : <?php echo $result[0]->meta_title;?></li>
				<li class="details">Meta Description : <?php echo $result[0]->meta_desc;?></li>
				<li class="details">Meta Keyword : <?php echo $result[0]->meta_keywords;?></li>			
				<li class="details">Status : <?php if($result[0]->status=='1'){ echo "Active"; }else{ echo "Inactive";}?> </li>
			</ul>
			</fieldset>
	   <?php
		}
	}
}
?>