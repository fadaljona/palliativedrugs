<?php

class Application_Model_News
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    

    public function getNewstypes()
    {
        $sql = "SELECT typeid,type_name FROM news_types";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function read($id)
    {
        $sql = "SELECT * FROM `3bit_news` n WHERE n.news_id ='".$id."'";
        if ($result = $this->_db->query($sql)) {
			  $data		=	$result->fetchAll();
			  $rnews	=	$data[0]['related_news'];
			  $rpages	=	$data[0]['related_pages'];
			  $download	=	$data[0]['news_download'];

			  $span_news=	'';
			  $span_pages=	'';
			  $span_download='';
			  $selN	=	$this->_db->fetchAll("SELECT title FROM 3bit_news WHERE FIND_IN_SET(news_id,'".$rnews."')");
			  if(count($selN)>0){
				foreach($selN as $res){
					$span_news	.= stripslashes($res['title']).",";	
				}
			  }
			  $selP	=	$this->_db->fetchAll("SELECT cat_title FROM 3bit_categories WHERE FIND_IN_SET(cat_id,'".$rpages."')");
			  if(count($selN)>0){
				foreach($selP as $res){
					$span_pages	.= stripslashes($res['cat_title']).",";	
				}
			  }
			  $selA	=	$this->_db->fetchAll("SELECT * FROM 3bit_assets WHERE FIND_IN_SET(asset_id,'".$download."')");
			  if(count($selA)>0){
				foreach($selA as $res){
					$span_download	.= $res['asset_name']."<br>";	
				}
			  }

			  $data[0]['span_news']		=	substr($span_news,0,-1);
			  $data[0]['span_pages']	=	substr($span_pages,0,-1);
			  $data[0]['span_download']	=	$span_download;

			  $audit	=	new Application_Model_Audits();

			  $audit->insert_audit('3bit_news','view',$id,'success');

			  return $data;
        }			
    }

	public function add($data)
    {	
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';	

		$news_tags	=	'';
		$keys = array_keys($data);
		foreach($keys as $key ){
			if(strstr($key,'as_values'))
				$news_tags=$data[$key];						
		}

		$start_date	=   $data["news_date"];	
		
		$audit	=	new Application_Model_Audits();

		if($data['alias_title']!=''){
			$alias	=	$audit->remSpecial($data['alias_title']);
		}else{
			$alias	=	$audit->remSpecial($data['title']);
		}
		$sql		=	"INSERT INTO 3bit_news SET
								title				=	'".addslashes($data['title'])."',
								alias_title			=	'".addslashes($alias)."',
								type				=	'".$data['type']."',
								category			=	'".addslashes($data['category'])."',								
								news_date			=	'".$start_date."',
								content				=	'".addslashes($data['content'])."',	
								author				=	'".addslashes($logged_user)."',
								news_tags			=	'".addslashes($news_tags)."',
								related_news		=	'".$data['related_news']."',
								related_pages		=	'".$data['related_pages']."',
								url					=	'".addslashes($data['url'])."',
								link				=	'".addslashes($data['link'])."',
								news_download		=	'".$data['news_downloadsid']."',
								link_text			=	'".addslashes($data['link_text'])."',
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now() ";  	

		$query	=	$this->_db->getConnection()->query($sql);

		$newsid	=	$this->_db->lastInsertId();	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_news','add',$newsid,'success');
    }

	public function update($data)
	{
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';	

		$news_tags	=	'';
		$keys = array_keys($data);
		foreach($keys as $key ){
			if(strstr($key,'as_values'))
				$news_tags=$data[$key];						
		}

		$start_date		=   $data["news_date"];
		
		$audit	=	new Application_Model_Audits();

		if($data['alias_title']!=''){
			$alias	=	$audit->remSpecial($data['alias_title']);
		}else{
			$alias	=	$audit->remSpecial($data['title']);
		}
		
		$sql			=	"UPDATE 3bit_news SET
								title				=	'".addslashes($data['title'])."',
								alias_title			=	'".addslashes($alias)."',
								type				=	'".$data['type']."',
								category			=	'".addslashes($data['category'])."',								
								news_date			=	'".$start_date."',
								short_text			=	'".addslashes($data['short_text'])."',	
								content				=	'".addslashes($data['content'])."',	
								author				=	'".addslashes($logged_user)."',	
								news_tags			=	'".addslashes($news_tags)."',
								related_news		=	'".$data['related_news']."',
								related_pages		=	'".$data['related_pages']."',
								url					=	'".addslashes($data['url'])."',
								link				=	'".addslashes($data['link'])."',
								news_download		=	'".$data['news_downloadsid']."',
								link_text			=	'".addslashes($data['link_text'])."',
								status				=	'".$data['status']."',							
								modified_date		=	now() WHERE news_id='".$data['id']."'"; 

		$query	=	$this->_db->getConnection()->query($sql);

		$newsid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_news','update',$newsid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_news",'newsid='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_news','delete',$id,'success');
	}
}

?>