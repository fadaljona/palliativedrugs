<?php

class Application_Model_Productcat
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
		$this->_baseurl	= Zend_Registry::get("baseUrl");
    }    
	
	public function procat_list($parentid,$i)
	{		
		$res			=	$this->_db->fetchAll("SELECT * FROM 3bit_product_category WHERE procat_parent_id='".$parentid."' ORDER BY procat_order");				
		$i					=	$i+1;
		$print_row			=	'';		
		$tab				=	'';
		foreach($res as $cat_values){			
			$catid			=	$cat_values['procat_id'];
			$catname		=	stripslashes($cat_values['procat_title']);
			$cat_level		=	$cat_values['procat_level'];
			$cat_order		=	$cat_values['procat_order'];
			$form_type		=	$cat_values['form_type'];
			$cat_active		=	$cat_values['status'];										

			$res_find_children	= $this->_db->query("SELECT * FROM 3bit_product_category WHERE procat_parent_id='".$catid."'");

			if ($cat_active=="1"){
				$active	=	"<span id='".$catid."3bit_product_category'><a href=\"javascript:;\" onclick=\"change_status(".$catid.",'3bit_product_category','status','0','procat_id');\"><font color='#006600'>Active</font></a></span>";		
				$delstyle	=	"style='display:none'";
			}else{
				$active	=	"<span id='".$catid."3bit_product_category'><a href=\"javascript:;\" onclick=\"change_status(".$catid.",'3bit_product_category','status','1','procat_id');\"><font color='#FF0000'>InActive</font></a></span>";					$delstyle	=	'';							
			}
		
			if ($cat_level =='1'){				
				$tab ='';				
			}else if ($cat_level =='2'){
				$tab ='----';
			}else if ($cat_level =='3'){
				$tab ='-----';
			}else if ($cat_level =='4'){
				$tab ='------';
			}else if ($cat_level =='5'){
				$tab ='-------';
			}		
			
			$delete_link ="<span id = '".$catid."' ".$delstyle."><a class='button'onclick=\"return open_dialog('".$this->_baseurl."/productcat/delete/id/".$cat_values['procat_id']."')\"><img border=\"0\" alt=\"Delete\" src=\"".$this->_baseurl."/public/images/icons/cross.png\">Delete</a></span>";
			
			$print_row .="<tr>";
			$print_row .="<td>&nbsp;$tab $catname </td>
						  <td>&nbsp;$cat_level</td>
						  <td>&nbsp;$form_type</td>						 
						  <td>&nbsp;$active</td>
						  <td><a class='button' href=\"".$this->_baseurl."/productcat/add/id/".$cat_values['procat_id']."\"><img alt=\"Edit\" border=\"0\" src=\"".$this->_baseurl."/public/images/icons/page_white_edit.png\">Edit</a>&nbsp;
						 $delete_link						
							</td>
						</tr>";	  
			$print_row .= $this->procat_list($catid,$i);							

	}// while
		
	return $print_row;        
   }

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_product_category WHERE procat_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_product_category','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$procat_level			=	$this->findprocatlevel($data['procat_parent_id']);

		$procat_order			=	$this->findprocatorder($procat_level);	

		$sql					=	"INSERT INTO 3bit_product_category SET
												procat_title			=	'".addslashes($data['procat_title'])."',
												procat_desc				=	'".addslashes($data['procat_desc'])."',
												procat_parent_id		=	'".addslashes($data['procat_parent_id'])."',
												procat_order			=	'".$procat_order."',
												procat_level			=	'".$procat_level."',
												form_type				=	'".addslashes($data['form_type'])."',
												status					=	'".$data['status']."',
												created_date			=	now(),
												modified_date			=	now()";

		$query	=	$this->_db->query($sql);

		$catid	=	$this->_db->lastInsertId();	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_product_category','add',$catid,'success');
    }

	public function update($data)
	{
		$procat_level			=	$this->findprocatlevel($data['procat_parent_id']);

		$sql					=	"UPDATE 3bit_product_category SET
										procat_title			=	'".addslashes($data['procat_title'])."',
										procat_desc				=	'".addslashes($data['procat_desc'])."',
										procat_parent_id		=	'".addslashes($data['procat_parent_id'])."',
										procat_level			=	'".$procat_level."',
										form_type				=	'".addslashes($data['form_type'])."',
										status					=	'".$data['status']."',
										modified_date			=	now() WHERE procat_id='".$data['id']."'";

		$query	=	$this->_db->query($sql);

		$catid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_product_category','update',$catid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_product_category",'procat_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_product_category','delete',$id,'success');
	}

	public function active_procats($parentid){
	
		if($parentid=='0'){
			$sublib_qry  = $this->_db->fetchAll("SELECT procat_id,procat_title,procat_level,procat_parent_id FROM `3bit_product_category` WHERE procat_parent_id='$parentid'");
		}else{
			$sublib_qry  = $this->_db->fetchAll("SELECT procat_id,procat_title,procat_level,procat_parent_id FROM `3bit_product_category` WHERE procat_parent_id='$parentid' AND procat_level !='1' ORDER BY procat_level");
		}		
		
		$last_child  = '';
		
		if($sublib_qry){ 			
			foreach($sublib_qry as $sublib_values)	{
				$tmp				= '';
				$procat_id			= $sublib_values["procat_id"];				
				$subparent_id		= $sublib_values["procat_parent_id"];				
				$procat_title		= $sublib_values["procat_title"];																			
				$qry				= $this->_db->fetchRow("SELECT procat_id,procat_title,procat_level,procat_parent_id FROM `3bit_product_category` WHERE procat_parent_id='$procat_id'");	
				if($qry){
					$last_child			.= $this->active_procats($procat_id);//recursive call to the function						
				}else{
					if($subparent_id!='0'){
						$last_child		.=	'<dd><a href="/products/add/'.$procat_id.'">Add '.$procat_title.'</a></dd>';
					}
				}
							
			}	
			return $last_child;
		}else{
			return $last_child;
		}
	}

	public function list_procat($passdir,$i,$selected){
	
		$i.="---";
			
		$sublib_qry  = $this->_db->fetchAll("SELECT procat_id,procat_parent_id,procat_title FROM `3bit_product_category` WHERE procat_parent_id='$passdir'");		

		$second = '';
			
		if ($sublib_qry){ 			
			foreach($sublib_qry as $sublib_values)	{				
				$sublib_id		= $sublib_values["procat_id"];			
				$subparent_id	= $sublib_values["procat_parent_id"];				
				$sublib_name	= $sublib_values["procat_title"];	  				
				if ($selected == $sublib_id){								  						
					$sel ="selected";										  					
				}else{														  				
					$sel ="";												  					
				}															  				
				$second.= "<option value='$sublib_id' $sel>$i$sublib_name</option>";									
					
				$second.= $this->list_procat($sublib_id,$i,$selected);//recursive call to the function				
			}
			return $second;
		}else{			   		
			return $second;
		}
	}


	public function findprocatlevel($parent_id){

		$sublib_qry  = 	$this->_db->fetchRow("SELECT procat_id,procat_level FROM `3bit_product_category` WHERE procat_id='$parent_id'");
		
		if (!$sublib_qry){	
			$level	= 1;
		}else{
			$sublib_id	 = $sublib_qry['procat_id'];

			$cat_level	 = $sublib_qry['procat_level'];

			$level		 = $cat_level + 1;
		}
		return $level; 
	}

	public function findprocatorder($procat_level){

		$sublib_qry  = $this->_db->fetchRow("SELECT max(procat_order) as max_order from `3bit_product_category` where procat_level='$procat_level'");	
		
		if (!$sublib_qry){	
			$new_order	 = 1;
		}else{
			$new_order	 = $sublib_qry['max_order'] + 1;
		}

		return $new_order;

	}
}

?>