<?php

class Application_Model_Fieldsets
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    

    public function getDatasource()
    {
        $sql = "SELECT datasource_id,title FROM 3bit_datasource";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function getGroups()
    {
        $sql = "SELECT group_type FROM 3bit_fieldsets GROUP BY group_type";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_fieldsets WHERE fieldset_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_fieldsets','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function getfields($id)
	{
		$sql	=	"SELECT * FROM 3bit_fields WHERE fieldset_id='".$id."'";

		$res	=	$this->_db->fetchAll($sql);

		$fields	=	'';

		$i		=	1;	

		foreach($res as $data){
			if($data['fieldstatus']=='1')
				$fstatus	=	'Active';
			else
				$fstatus	=	'Inactive';

			$fields	.='<tr class="'.$data['fieldname'].'"><td><input type="hidden" name="fieldname[]" value="'.$data['fieldname'].'">'.$data['fieldname'].'</td><td><input type="hidden" name="fieldtype[]" value="'.$data['fieldtype'].'">'.$data['fieldtype'].'</td><td><input type="hidden" name="fieldvar[]" value="'.$data['fieldvar'].'">'.$data['fieldvar'].'</td><td><input type="hidden" name="objecttype[]" value="'.$data['objecttype'].'">'.$data['objecttype'].'</td><td><input type="hidden" name="fieldstatus[]" value="'.$data['fieldstatus'].'">'.$fstatus.'</td></tr>';

			$i++;

		}

		return $fields;

	}

	public function add($data)
    {			
		$sql	=	"INSERT INTO 3bit_fieldsets SET
								`title`				=	'".addslashes($data['title'])."',
								`group_type`		=	'".addslashes($data['group_type'] )."',
								`description`		=	'".addslashes($data['description'])."',
								`datasource_id`		=	'".addslashes($data['datasource_id'])."',
								`advance_sql`		=	'".addslashes($data['advsql'])."',
								`usage`				=	'".addslashes($data['usage'])."',	
								`status`			=	'".$data['status']."',		
								created_date		=	now(),
								modified_date		=	now()";

		$query	=	$this->_db->query($sql);

		$fieldsetid	=	$this->_db->lastInsertId();	

		for($i=0;$i<count($data['fieldname']);$i++){
			
			$fsql	=	"INSERT INTO 3bit_fields SET
								fieldset_id			=	'".$fieldsetid."',
								fieldname			=	'".$data['fieldname'][$i]."',
								fieldtype			=	'".$data['fieldtype'][$i]."',
								fieldvar			=	'".$data['fieldvar'][$i]."',
								objecttype			=	'".$data['objecttype'][$i]."',
								fieldstatus			=	'".$data['fieldstatus'][$i]."',
								created_date		=	now(),
								modified_date		=	now()";
			$this->_db->query($fsql);
			
		}		
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_fieldsets','add',$fieldsetid,'success');
    }

	public function update($data)
	{		
		$sql	=	"UPDATE 3bit_fieldsets SET
								`title`				=	'".addslashes($data['title'])."',
								`group_type`		=	'".addslashes($data['group_type'] )."',
								`description`		=	'".addslashes($data['description'])."',
								`datasource_id`		=	'".addslashes($data['datasource_id'])."',
								`advance_sql`		=	'".addslashes($data['advsql'])."',
								`usage`				=	'".addslashes($data['usage'])."',	
								`status`			=	'".$data['status']."',				
								`modified_date`		=	now() WHERE fieldset_id='".$data['id']."'";
		$query	=	$this->_db->query($sql);

		$fieldsetid	=	$data['id'];

		$this->_db->query("DELETE FROM 3bit_fields WHERE fieldset_id='".$fieldsetid."'");

		for($i=0;$i<count($data['fieldname']);$i++){
			
			$fsql	=	"INSERT INTO 3bit_fields SET
								fieldset_id			=	'".$fieldsetid."',
								fieldname			=	'".$data['fieldname'][$i]."',
								fieldtype			=	'".$data['fieldtype'][$i]."',
								fieldvar			=	'".$data['fieldvar'][$i]."',
								objecttype			=	'".$data['objecttype'][$i]."',
								fieldstatus			=	'".$data['fieldstatus'][$i]."',
								created_date		=	now(),
								modified_date		=	now()";
			$this->_db->query($fsql);
			
		}		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_fieldsets','update',$fieldsetid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_fieldsets",'fieldset_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_fieldsets','delete',$id,'success');
	}
	
	
	//***************Database configuration***************//

	public function checksql($data){
		mysql_connect("localhost","pall_dbuser-dev","xe97BRAXuFrUChe9")or die(mysql_error());
		mysql_select_db("pall_formulary-dev")or die(mysql_error());
		$chkfields = '';
		$fieldname = array();
		$fieldtype = array();
		
		if(!empty($data['fieldsetid'])){
			$selfie	=	mysql_query("SELECT fieldname,fieldtype FROM 3bit_fields WHERE fieldset_id='".$data['fieldsetid']."'")or die(mysql_error());
			if(mysql_num_rows($selfie)>0){
				while($fres	=	mysql_fetch_array($selfie)){
					$fieldname[]	=	$fres[0];
					$fieldtype[]	=	$fres[1];
				}
			}
		}
		
		if(!empty($data['advsql'])){
			$sql	=	$data['advsql'];
			$result	=	mysql_query($sql)or die(mysql_error());	
			$fields	=	mysql_num_fields($result);
			for ($i=0; $i < $fields; $i++) {
				$name  = mysql_field_name($result, $i);
				$type  = mysql_field_type($result, $i);		
				if(in_array($name,$fieldname) && in_array($type,$fieldtype)){
					$chkfields	.=	'<input type="checkbox" id="check'.$name.'" value="'.$name.'" alt="'.$type.'" checked /><label for="check'.$name.'">'.$name.'</label>';	 
				}else{
					$chkfields	.=	'<input type="checkbox" id="check'.$name.'" value="'.$name.'" alt="'.$type.'" /><label for="check'.$name.'">'.$name.'</label>';
				}
			}
		}else if(!empty($data['datasource_id'])){
			$ssql	=	mysql_query("SELECT content FROM 3bit_datasource WHERE datasource_id='".$data['datasource_id']."' ");
			$sql	=	stripslashes(mysql_result($ssql,0,0)); 				
			$result	=	mysql_query($sql)or die(mysql_error());	
			$fields	=	mysql_num_fields($result);
			for ($i=0; $i < $fields; $i++) {
				$name  = mysql_field_name($result, $i);
				$type  = mysql_field_type($result, $i);		
				if(in_array($name,$fieldname) && in_array($type,$fieldtype)){
					$chkfields	.=	'<input type="checkbox" id="check'.$name.'" value="'.$name.'" alt="'.$type.'" checked /><label for="check'.$name.'">'.$name.'</label>';	 
				}else{
					$chkfields	.=	'<input type="checkbox" id="check'.$name.'" value="'.$name.'" alt="'.$type.'" /><label for="check'.$name.'">'.$name.'</label>';	 
				}
			}
		}
		if($chkfields!=''){
			$chkfields	= ' <div id="format">'.$chkfields.'</div>';

			die($chkfields."^"."S");
		}else{
			die(" "."^"."F");
		}
	}
}

?>