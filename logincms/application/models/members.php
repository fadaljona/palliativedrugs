<?php

class Application_Model_Members
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM new_users WHERE id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('new_users','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function readsubscribtion($id)
    {
        $sql = "SELECT subscriber_id, DATE_FORMAT(start_date, '%Y-%m-%d') as start_date, DATE_FORMAT(end_date, '%Y-%m-%d') as end_date, duration, status FROM 3bit_subscribers WHERE subscriber_id ='".$id."' ";
		// $sql = "SELECT DATE_FORMAT(start_date, '%Y-%m-%d') as start_date, DATE_FORMAT(end_date, '%Y-%m-%d') as end_date, duration, status FROM 3bit_subscribers WHERE user_id ='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			//$audit->insert_audit('new_users','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		 $sql	=	"INSERT INTO `new_users` SET															
							username		=		'".addslashes($data['username'])."',
							email			=       '".addslashes($data['email'])."',
							pwd				=       '".addslashes($data['pwd'])."',
							FirstName		=       '".addslashes($data['FirstName'])."',
							LastName		=       '".addslashes($data['LastName'])."',
							title			=       '".addslashes($data['title'])."',
							role			=       '".addslashes($data['role'])."',
							prof_reg_num	=       '".addslashes($data['prof_reg_num'])."',
							nurse_prescriber=       '".addslashes($data['nurse_prescriber'])."',  
							speciality		=       '".addslashes($data['speciality'])."',
							sub_specialty	=       '".addslashes($data['sub_specialty'])."',
							organisation	=       '".addslashes($data['organisation'])."',
							addr1			=       '".addslashes($data['addr1'])."',
							addr2			=       '".addslashes($data['addr2'])."',
							addr3			=       '".addslashes($data['addr3'])."',
							addr4			=       '".addslashes($data['addr4'])."',
							TownOrCity		=       '".addslashes($data['TownOrCity'])."',
							StateOrCounty	=       '".addslashes($data['StateOrCounty'])."',
							PostalCode		=       '".addslashes($data['PostalCode'])."',
							Country			=       '".addslashes($data['Country'])."',
							phone			=       '".addslashes($data['phone'])."',
							main_workplace	=       '".addslashes($data['main_workplace'])."',
							current_post	=       '".addslashes($data['current_post'])."',         
							years_in_post   =       '".addslashes($data['years_in_post'])."',        
							patient_care_time=      '".addslashes($data['patient_care_time'])."',    
							gender           =      '".addslashes($data['gender'])."',               
							year_of_birth    =      '".addslashes($data['year_of_birth'])."',        
							accept_site_emails=     '".addslashes($data['accept_site_emails'])."',   
							accept_bb_emails =     '".addslashes($data['accept_bb_emails'])."',     
							UserType		 =     '".$data['user_type']."',     
							active           =      '".addslashes($data['active'])."',
							RegistrationChecked		=	'".$data['status']."',
							previous_visit	 =		'".date('Y-m-d')."',
							current_visit	 =		'".date('Y-m-d')."' ";

		$query	=	$this->_db->query($sql);

		$memid	=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('new_users','add',$memid,'success');
    }

	public function createsubscriber($data)
    {	
		$arrStartDate	= explode('-',$data['start_date']);
		$startDate		= $arrStartDate[2].'-'.$arrStartDate[1].'-'.$arrStartDate[0];
		$arrEndDate		= explode('-',$data['end_date']);
		$endDate		= $arrEndDate[2].'-'.$arrEndDate[1].'-'.$arrEndDate[0];
		$sql			=	"INSERT INTO `3bit_subscribers` SET	
							user_id			=	'".$data['id']."',
							start_date		=	'".$startDate."',
							end_date		=	'".$endDate."',
							duration		=	'".$data['duration']."',
							group_id		=	'0',
							order_id		=	'0',
							status			=	'".addslashes($data['active'])."',
							created_date	=	'".date('Y-m-d')."',
							modified_date	=	'".date('Y-m-d')."' ";

		$query			=	$this->_db->query($sql);
		$subscriber_id	=	$this->_db->lastInsertId();

		$selFeature		=	$this->_db->fetchRow("SELECT u.Country, c.hinari FROM new_users u, 3bit_country c WHERE u.id='".$data['id']."' AND u.active='1' AND u.Country =c.country ");

		$isHinari		= $selFeature['hinari'];
		
		if($data['formaction'] == 'addupdate_notify'  && $isHinari != '0' ){
			$message_type			=	'Hinari Subscription Activation';	
		}else if($data['formaction'] == 'addupdate_notify'  && $isHinari == '0'){
			$message_type			=	'Subscription Added';
		}

		$this->sendMails($subscriber_id,$message_type,'0','Customer');

		//$memid	=	$this->_db->lastInsertId();
		//$audit	=	new Application_Model_Audits();
		//$audit->insert_audit('new_users','add',$memid,'success');
	}

	
	public function updatesubscriber($data)
    {
		$arrStartDate			= explode('-',$data['start_date']);
		$startDate				= $arrStartDate[2].'-'.$arrStartDate[1].'-'.$arrStartDate[0];
		$arrEndDate				= explode('-',$data['end_date']);
		$endDate				= $arrEndDate[2].'-'.$arrEndDate[1].'-'.$arrEndDate[0];

		$sql					=	"UPDATE `3bit_subscribers` SET	
							start_date		=	'".$startDate."',
							end_date		=	'".$endDate."',
							duration		=	'".$data['duration']."',
							status			=	'".addslashes($data['active'])."',
							modified_date	=	'".date('Y-m-d')."' WHERE subscriber_id	=	'".$data['subid']."'";

		$query					=	$this->_db->query($sql);

		$subscriber_id			=	$data['subid'];

		$selFeature				=	$this->_db->fetchRow("SELECT u.Country, c.hinari FROM new_users u, 3bit_country c WHERE u.id='".$data['id']."' AND u.active='1' AND u.Country =c.country ");

		$isHinari				= $selFeature['hinari'];

		if($data['formaction'] == 'addupdate_notify'  && $isHinari != '0' ){
			$message_type			=	'Hinari Subscription Activation';	
		}else if($data['formaction'] == 'addupdate_notify'  && $isHinari == '0'){
			$message_type			=	'Subscription Added';
		}

		$this->sendMails($subscriber_id,$message_type,'0','Customer');
		/*
		//$memid	=	$this->_db->lastInsertId();
		
		//$audit	=	new Application_Model_Audits();

		//$audit->insert_audit('new_users','add',$memid,'success');
		*/
    }

	public function prevnext_member($id)
    {
				
		// for quickview prev next pagination
		if ( $_SESSION['search_type'] != "A" && $_SESSION['search_type'] != "unchecked" && $_SESSION['search_type'] != "pending" && $_SESSION['search_type'] != "hinari" ){
			$sQuery		=	"SELECT a.id FROM new_users a, 3bit_subscribers s WHERE a.id = s.user_id ".$_SESSION['sWhere']."  GROUP BY s.user_id ".$_SESSION['sOrder'];

			$qvResult	=	$this->_db->fetchAll($sQuery);	
		}else if($_SESSION['search_type'] == "hinari"){

			$sQuery		=	"SELECT a.id FROM new_users a, 3bit_country c WHERE  a.Country =c.country  AND c.hinari <> '0' $sWhere $sOrder $sLimit ";
			$qvResult	=	$this->_db->fetchAll($sQuery);	

		}else{
			$sQuery		=	"SELECT id FROM new_users WHERE 1 ".$_SESSION['sWhere']." ".$_SESSION['sOrder'];
			$qvResult	=	$this->_db->fetchAll($sQuery);	
		}

		foreach( $qvResult as $qkey => $qaRow){

			if ($qvResult[$qkey]['id'] == $id){
			$next		= $qkey + 1;
			$next		= $qvResult[$next]['id'];
			
			$prev		= $qkey - 1;
			$prev		= $qvResult[$prev]['id'];
			}
		}

		$arrMem[]			= $prev;
		$arrMem[]			= $next;

		return $arrMem;
	}

	public function updatequickview($data)
    {	
		//$sql	=	"UPDATE `new_users` SET	RegistrationChecked		=	'".$data['status']."', UserType = '".$data['user_type']."' WHERE id	=	'".$data['id']."'";

		//$query	=	$this->_db->query($sql);

		$sql	=	"UPDATE `new_users` SET															
							username		=		'".addslashes($data['username'])."',
							email			=       '".addslashes($data['email'])."',
							pwd				=       '".addslashes($data['pwd'])."',
							FirstName		=       '".addslashes($data['FirstName'])."',
							LastName		=       '".addslashes($data['LastName'])."',
							title			=       '".addslashes($data['title'])."',
							role			=       '".addslashes($data['role'])."',
							prof_reg_num	=       '".addslashes($data['prof_reg_num'])."',
							nurse_prescriber=       '".addslashes($data['nurse_prescriber'])."',  
							speciality		=       '".addslashes($data['speciality'])."',
							sub_specialty	=       '".addslashes($data['sub_specialty'])."',
							organisation	=       '".addslashes($data['organisation'])."',
							addr1			=       '".addslashes($data['addr1'])."',
							addr2			=       '".addslashes($data['addr2'])."',
							addr3			=       '".addslashes($data['addr3'])."',
							addr4			=       '".addslashes($data['addr4'])."',
							TownOrCity		=       '".addslashes($data['TownOrCity'])."',
							StateOrCounty	=       '".addslashes($data['StateOrCounty'])."',
							PostalCode		=       '".addslashes($data['PostalCode'])."',
							Country			=       '".addslashes($data['Country'])."',
							phone			=       '".addslashes($data['phone'])."',
							main_workplace	=       '".addslashes($data['main_workplace'])."',
							current_post	=       '".addslashes($data['current_post'])."',         
							years_in_post   =       '".addslashes($data['years_in_post'])."',        
							patient_care_time=      '".addslashes($data['patient_care_time'])."',    
							gender           =      '".addslashes($data['gender'])."',               
							year_of_birth    =      '".addslashes($data['year_of_birth'])."',        
							accept_site_emails=     '".addslashes($data['accept_site_emails'])."',   
							accept_bb_emails  =     '".addslashes($data['accept_bb_emails'])."',
							UserType		 =     '".$data['user_type']."',
							active           =      '".addslashes($data['active'])."',
							RegistrationChecked		=	'".$data['status']."',
							previous_visit	 =		'".date('Y-m-d')."',
							current_visit	 =		'".date('Y-m-d')."'	WHERE id='".$data['id']."'	";
		
		$query	=	$this->_db->query($sql);

		
    }

	public function update($data)
	{
		
		$sql	=	"UPDATE `new_users` SET															
							username		=		'".addslashes($data['username'])."',
							email			=       '".addslashes($data['email'])."',
							pwd				=       '".addslashes($data['pwd'])."',
							FirstName		=       '".addslashes($data['FirstName'])."',
							LastName		=       '".addslashes($data['LastName'])."',
							title			=       '".addslashes($data['title'])."',
							role			=       '".addslashes($data['role'])."',
							prof_reg_num	=       '".addslashes($data['prof_reg_num'])."',
							nurse_prescriber=       '".addslashes($data['nurse_prescriber'])."',  
							speciality		=       '".addslashes($data['speciality'])."',
							sub_specialty	=       '".addslashes($data['sub_specialty'])."',
							organisation	=       '".addslashes($data['organisation'])."',
							addr1			=       '".addslashes($data['addr1'])."',
							addr2			=       '".addslashes($data['addr2'])."',
							addr3			=       '".addslashes($data['addr3'])."',
							addr4			=       '".addslashes($data['addr4'])."',
							TownOrCity		=       '".addslashes($data['TownOrCity'])."',
							StateOrCounty	=       '".addslashes($data['StateOrCounty'])."',
							PostalCode		=       '".addslashes($data['PostalCode'])."',
							Country			=       '".addslashes($data['Country'])."',
							phone			=       '".addslashes($data['phone'])."',
							main_workplace	=       '".addslashes($data['main_workplace'])."',
							current_post	=       '".addslashes($data['current_post'])."',         
							years_in_post   =       '".addslashes($data['years_in_post'])."',        
							patient_care_time=      '".addslashes($data['patient_care_time'])."',    
							gender           =      '".addslashes($data['gender'])."',               
							year_of_birth    =      '".addslashes($data['year_of_birth'])."',        
							accept_site_emails=     '".addslashes($data['accept_site_emails'])."',   
							accept_bb_emails  =     '".addslashes($data['accept_bb_emails'])."',
							UserType		 =     '".$data['user_type']."',
							active           =      '".addslashes($data['active'])."',
							RegistrationChecked		=	'".$data['status']."',
							previous_visit	 =		'".date('Y-m-d')."',
							current_visit	 =		'".date('Y-m-d')."'	WHERE id='".$data['id']."'	";
		
		$query	=	$this->_db->query($sql);

		$memid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('new_users','update',$memid,'success');

		//exit;
    }

	public function delete($id){

		$this->_db->delete("new_users",'id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('new_users','delete',$id,'success');
	}

	public function active_roles()
    {
		$sql = "SELECT role FROM new_users WHERE active ='1' GROUP BY role ORDER BY role ";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function active_speciality()
    {
		$sql = "SELECT speciality FROM new_users WHERE active ='1' AND speciality!='' GROUP BY speciality ORDER BY speciality ";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }         
    }
	public function active_sub_spec()
    {
        $sql = "SELECT sub_specialty FROM new_users WHERE active ='1' AND sub_specialty!='' GROUP BY sub_specialty ORDER BY sub_specialty ";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }  		
    }
	public function active_country()
    {
		$sql = "SELECT name FROM country WHERE name!='' GROUP BY name ORDER BY name ";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }          
    }
	public function active_place()
    {
		$sql = "SELECT main_workplace FROM new_users WHERE main_workplace!='' GROUP BY main_workplace ORDER BY main_workplace ";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }        
    }
	public function active_post()
    {
		$sql = "SELECT current_post FROM new_users WHERE current_post!='' GROUP BY current_post ORDER BY current_post ";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }         
    }

	public function view($id)
    {
        $sql			= "SELECT a.id, a.title, a.FirstName, a.LastName , a.email ,DATE_FORMAT(s.start_date, '%D-%M-%Y') as start_date, DATE_FORMAT(s.end_date, '%D-%M-%Y') as end_date, s.duration FROM new_users a, 3bit_subscribers s WHERE a.id = s.user_id AND id='".$id."' ORDER BY s.created_date DESC ";
        if ($result		= $this->_db->query($sql)) {
			$audit		=	new Application_Model_Audits();
			//$audit->insert_audit('new_users','view',$id,'success');
            return $result->fetchAll();
        }
    }

	

	public function getsubscribtion($id)
    {
        $sql			= $this->_db->fetchRow("SELECT s.subscriber_id FROM new_users a, 3bit_subscribers s WHERE a.id = s.user_id AND id='".$id."' ORDER BY s.created_date DESC  LIMIT 0,1");
      
		return $sql['subscriber_id'];
        
    }

	public function getSubcribtionDetails($id)
	{
		$data	=	$this->_db->fetchAll("SELECT a.id, a.title, a.FirstName, a.LastName , a.email ,DATE_FORMAT(s.start_date, '%D-%M-%Y') as start_date, DATE_FORMAT(s.end_date, '%D-%M-%Y') as end_date, s.duration, s.status, s.order_id, s.group_id FROM new_users a, 3bit_subscribers s WHERE a.id = s.user_id AND id='".$id."'  ORDER BY s.created_date DESC ");

		$details = '';

		if($data){

				foreach($data AS $key => $value){

							$details		.=	'<tr> 
											<td >'.$value['start_date'].'</td>
											<td>'.$value['end_date'].'</td>
											<td>'.$value['duration'].' '.($value['duration']=='365'?'Year':'Days').'</td>
											<td>'.$value['order_id'].'</td>                
											<td>'.($value['status']=='1'?'Active':'Inactive').'</td>                
											<td>'.($value['group_id']>0?$value['group_id']:'-').'</td>                
										</tr>';
				}
		}

		return $details;
	}

	public function quickview($id)
	{
		$sql = "SELECT * FROM new_users WHERE id='".$id."'";

		if ($result = $this->_db->query($sql)) {
			return $result->fetchAll();
		}
	}

	public function get_all_subscribers()
    {
		$sql = "SELECT user_id FROM 3bit_subscribers WHERE 1";
        if ($result = $this->_db->query($sql)) {
            $res	=  $result->fetchAll();

			$arrSubscribers	= array();

			foreach($res AS $key => $val){
				$arrSubscribers[]	= $val['user_id'];

			}

			return $arrSubscribers;
        }         
    }

	public function sendMails($subid,$message_type,$preview,$towhom){

			$sql			= $this->_db->fetchRow("SELECT a.id, a.title, a.FirstName, a.LastName , a.email FROM new_users a, 3bit_subscribers s WHERE a.id = s.user_id AND s.subscriber_id ='".$subid."'");

			$customer_title	=	$sql['title'];
			$customer_fname	=	$sql['FirstName'];
			$customer_lname	=	$sql['LastName'];
			$customer_email	=	$sql['email'];

		    if($towhom=='Customer'){
				// Send Email To Customer ----------------------------------
				
				$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
				$res				= 	$this->_db->fetchAll($sql_select);

			}else if($towhom=='Admin'){
				// Send Email To Customer ----------------------------------				
				$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
				$res				= 	$this->_db->fetchAll($sql_select); 
			}

		

			if(count($res)>0){
				foreach($res as $row){
					$body			= 	stripslashes($row['message_text']);
					$text			= 	stripslashes($row['text']);
					$subject		= 	stripslashes($row['mail_subject']);

					if($body !=''){

						$bodytag		= 	str_replace("{title}",$customer_title, $body);
						$bodytag		= 	str_replace("{fname}",$customer_fname, $bodytag);
						$bodytag		= 	str_replace("{lname}",$customer_lname, $bodytag);
						$bodytag		=	nl2br($bodytag);
					}

					if($text !=''){
						$texttag		= 	str_replace("{title}",$customer_title, $text);
						$texttag		= 	str_replace("{fname}",$customer_fname, $texttag);
						$texttag		= 	str_replace("{lname}",$customer_lname, $texttag);
					}

					if($body !='' || $text !=''){

						$send_email	=	array('0'=>$customer_email);

						require_once $_SERVER['DOCUMENT_ROOT'].'/logincms/ext/Swift/lib/swift_required.php';
						//SEND MAIL USING SWIFT LIBRARY
						$message = Swift_Message::newInstance()
						->setCharset('iso-8859-2')
						//Give the message a subject
						->setSubject($subject)
						//Set the From address with an associative array
						->setFrom(array('hq@palliativedrugs.com' => 'Palliative Drugs'))
						//Set the To addresses with an associative array
						->setTo($send_email)
						//Give it a body
						->setBody($texttag)
						//And optionally an alternative body
						->addPart($bodytag, 'text/html');
						$transport		= Swift_MailTransport::newInstance();
						$mailer			= Swift_Mailer::newInstance($transport);
						$result			= $mailer->send($message);
					}
				}
			}
		}
}
?>