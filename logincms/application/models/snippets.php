<?php

class Application_Model_Snippets
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }        

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_snippets WHERE snippet_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_snippets','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_snippets SET
								snippet_title		=	'".addslashes($data['snippet_title'])."',
								long_text			=	'".addslashes($data['long_text'] )."',
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now()";

		$query	=	$this->_db->getConnection()->query($sql);

		$snippetid	=	$this->_db->lastInsertId();	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_snippets','add',$snippetid,'success');
    }

	public function update($data)
	{				
		$sql	=	"UPDATE 3bit_snippets SET
								snippet_title		=	'".addslashes($data['snippet_title'])."',
								long_text			=	'".addslashes($data['long_text'] )."',
								status				=	'".$data['status']."',						
								modified_date		=	now() WHERE snippet_id='".$data['id']."'";
		$query	=	$this->_db->getConnection()->query($sql);

		$snippetid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_snippets','update',$snippetid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_snippets",'snippet_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_snippets','delete',$id,'success');
	}

	public function getFields(){

		$returnfields	=	'';

		$sql = $this->_db->fetchAll("SELECT * FROM 3bit_fieldsets WHERE 1 AND status='1' ");

		foreach($sql as $res){

			$fsql	=	$this->_db->fetchAll("SELECT * FROM 3bit_fields WHERE 1 AND fieldset_id='".$res['fieldset_id']."' AND fieldstatus='1' ");

			if(count($fsql)>0){

				$returnfields	.=	'<div>
										<h3><a href="#">'.stripslashes($res['title']).'</a></h3>
										<ul>';
				foreach($fsql as $fres){
					$returnfields	.=	'<li><a href="javascript:void(0);" onclick=\'wraptext(document.addsnippet.long_text, "'.stripslashes($fres['fieldvar']).'","");\'>'.stripslashes($fres['fieldvar']).'</a></li>';
				}

				$returnfields	.=		'</ul>
									</div>';
			}
		}		

		return $returnfields;
	}
}

?>