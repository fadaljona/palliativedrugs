<?php

class Application_Model_Products
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    
	
	public function getProductcats($product_cat)
    {
		$cats	=	$this->_db->fetchAll("SELECT * from `3bit_product_category` where 1 ");

		if($cats){
			$cat_list		=	'';
			$pcats		=	explode(",",$product_cat);							
			foreach($cats as $cat){
				if(in_array($cat['procat_id'],$pcats))
					$sel	=	'selected';
				else
					$sel	=	'';

				$cat_list	.=	'<option value="'.$cat['procat_id'].'" '.$sel.'>'.$cat['procat_title'].'</option>';
			}

		}
        return $cat_list;
    }

	public function getTemplates()
    {
        $sql = "SELECT temp_id,temp_filename FROM 3bit_article_template WHERE temp_type='products'";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function getVats()
    {
        $sql = "SELECT vat_id,vat_code FROM 3bit_product_vats WHERE 1";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function style_list($id){
		 $data		=	$this->_db->fetchAll("SELECT * FROM `3bit_products` WHERE (parent_id ='".$id."' OR product_id='".$id."') ORDER BY product_id ");		
		 $style_list	=	'';
		 if($data){			   
			for($i=0;$i<count($data);$i++){
				$vvarid		=	$data[$i]['product_id'];
				$vvarpcode	=	$data[$i]['product_code'];		
				$varstyle	=	$data[$i]['style'];		
				$varprice	=	$data[$i]['price'];		
				$varsup		=	$data[$i]['supplier_stock'];
				$varsite	=	$data[$i]['site_stock'];
				$varava		=	$data[$i]['available_stock'];
				$varvat		=	$data[$i]['vat_id'];
				$varvmoss	=	$data[$i]['vat_moss'];
				$varstat	=	$data[$i]['status'];
				if($varstat=='1')
					$dis	=	'Active';
				else
					$dis	=	'InActive';

				$selVcode	=	$this->_db->fetchRow("SELECT vat_code FROM 3bit_product_vats WHERE vat_id ='".$varvat."' ");

				if($selVcode){

					$vatval	=	$selVcode['vat_code'];
				}

				$style_list	.=	'<tr>
							 <input type="hidden" id="varProdid'.$i.'" name="varProdid[]" value="'.$vvarid.'"/>	
							 <td><nobr><input type="hidden" id="txtCode_'.$i.'" name="txtCode[]" value="'.$vvarpcode.'" />'.$vvarpcode.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtStyle_'.$i.'" name="txtStyle[]" value="'.$varstyle.'" />'.$varstyle.'</nobr></td>
	 						 <td><nobr><input type="hidden" id="txtPrice_'.$i.'" name="txtPrice[]" value="'.$varprice.'" />'.$varprice.'</nobr></td>	  			
							 <td><nobr><input type="hidden" id="txtSup_'.$i.'" name="txtSup[]" value="'.$varsup.'" />'.$varsup.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtSite_'.$i.'" name="txtSite[]" value="'.$varsite.'" />'.$varsite.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtAva_'.$i.'" name="txtAva[]" value="'.$varava.'" />'.$varava.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtVat_'.$i.'" name="txtVat[]" value="'.$varvat.'" />'.$vatval.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtVmoss_'.$i.'" name="txtVmoss[]" value="'.$varvmoss.'" />'.$varvmoss.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtStat_'.$i.'" name="txtStat[]" value="'.$varstat.'" />'.$dis.'</nobr></td>
							 <td><input type="button" name="btnedit"  value="Edit" onClick=\'editSRowVar(this.parentNode.parentNode.rowIndex,document.getElementById("txtCode_'.$i.'").value,document.getElementById("txtStyle_'.$i.'").value,document.getElementById("txtPrice_'.$i.'").value,document.getElementById("txtSup_'.$i.'").value,document.getElementById("txtSite_'.$i.'").value,document.getElementById("txtAva_'.$i.'").value,document.getElementById("txtVat_'.$i.'").value,document.getElementById("txtVmoss_'.$i.'").value,document.getElementById("txtStat_'.$i.'").value)\' class="button"></td>';
				if($i<>0){
						$style_list	.=	'<td><input type="button" name="btnview"  value="Delete" onClick="deleteSRowVar(this.parentNode.parentNode.rowIndex)" class="button"></td>';
				}
				$style_list	.=	'</tr>';

			}
		}

	return $style_list;
   }



	public function generalread($id)
    {
        $sql = "SELECT * FROM 3bit_products WHERE product_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
              $data					=	$result->fetchAll();
			  $product_image		= 	$data[0]['product_image'];		  
			  $product_thumb		=	$data[0]['product_thumb'];
			  $product_lb			=	$data[0]['product_lb'];
			  $image1				=	'';
			  $image2				=	'';	
			  $image3				=	'';	
			  $lb_imagename			=	'';

			  if($product_image<>"" && $product_image <>'0'){
				$imgsql				=	$this->_db->fetchRow("SELECT asset_name FROM 3bit_assets WHERE asset_id='".$product_image."'");						
				if(!empty($imgsql))
					$image1			=	$imgsql['asset_name'];
			  }
			  if($product_thumb<>"" && $product_thumb <>'0'){
				$imgsql2			=	$this->_db->fetchRow("SELECT asset_name FROM 3bit_assets WHERE asset_id='".$product_thumb."'");	
				if(!empty($imgsql2))
					$image2			=	$imgsql2['asset_name'];
			  }
			  if($product_lb<>"" && $product_lb <>'0'){
				$imgsql3			=	$this->_db->fetchAll("SELECT asset_category_id,asset_name FROM 3bit_assets WHERE FIND_IN_SET(asset_id,'".$product_lb."')");	
				if(!empty($imgsql3)){
					foreach($imgsql3 as $res){
						$image3			.=	'<img src="/assets/thumbs/'.$res['asset_category_id'].'/'.$res['asset_name'].'">';					
						$lb_imagename	.=	$res['asset_name'].",";
					}
				}
			  }
			  $data[0]['product_image_img']		=	$image1;
			  $data[0]['product_thumb_img']		=	$image2;
			  $data[0]['product_lb_img']		=	$image3;
			  $data[0]['product_lb_name']		=	$lb_imagename;
				
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_products','view',$id,'success');
            return $data;
        }
    }

	public function generaladd($data)
    {	
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';	
		
		$pcatid		=	'';

		for($i=0;$i<count($data["procat_id"]);$i++){
			$pcatid			.= $data["procat_id"][$i].",";
		}

		$vat_count	=	count($data['txtCode']);
		if($vat_count>1)
			$exist	=	1;
		else
			$exist	=	0;

		if(!empty($data['featured']))
			$featured = $data['featured'];
		else
			$featured = 0;

		if(!empty($data['txtVmoss']['0']))
			$vat_moss = 1;
		else
			$vat_moss = 0;	

		if(!empty($data['external_product']))
			$external_product = $data['external_product'];
		else
			$external_product = 0;	


		$query	=	$this->_db->getConnection()->query("INSERT INTO 3bit_products SET	
											procat_id		=	'".addslashes($pcatid)."',
											temp_id			=	'".addslashes($data['temp_id'])."',
											product_name	=	'".addslashes($data['product_name'])."',
											product_shortname=	'".addslashes($data['product_shortname'])."',
											short_text		=	'".addslashes($data['short_text'])."',
											description		=	'".addslashes($data['description'])."',
											testimonials	=	'".addslashes($data['testimonials'])."',
											product_code	=	'".addslashes($data['txtCode']['0'])."',
											style			=	'".addslashes(urldecode($data['txtStyle']['0']))."',											
											price			=	'".$data['txtPrice']['0']."',
											supplier_stock	=	'".$data['txtSup']['0']."',
											site_stock		=	'".$data['txtSite']['0']."',
											available_stock	=	'".$data['txtAva']['0']."',
											vat_id			=	'".$data['txtVat']['0']."',
											product_image	=	'".$data['product_imageid']."',
											product_thumb	=	'".$data['product_thumbid']."',
											variation_exist	=	'".$exist."',
											parent_id		=	'0',
											user_id			=	'".$logged_user."',
											shipping_type	=	'".$data['shipping_type']."',
											product_order	=	'".$product_order."',
											meta_title		=	'".addslashes($data['meta_title'])."',
											meta_desc		=	'".addslashes($data['meta_desc'])."',
											meta_keywords	=	'".addslashes($data['meta_keywords'])."',
											featured		=	'".$featured."',
											vat_moss		=		'".$vat_moss."',
											external_product=	'".$external_product."',
											status			=	'".$data['status']."',
											created_date	=	now(),
											modified_date	=	now()");	

		$productid	=	$this->_db->lastInsertId();	

		if($vat_count>1){

			for($i=1;$i<$vat_count;$i++){

					if(!empty($data['txtVmoss'][$i]))
						$vat_moss = 1;
					else
						$vat_moss = 0;	

				  $query	=	$this->_db->getConnection()->query("INSERT INTO 3bit_products SET	
												procat_id		=	'".addslashes($pcatid)."',
												product_name	=	'".addslashes($data['product_name'])."',
												product_shortname=	'".addslashes($data['product_shortname'])."',
												short_text		=	'".addslashes($data['short_text'])."',
												description		=	'".addslashes($data['description'])."',	
												testimonials	=	'".addslashes($data['testimonials'])."',
												product_image	=	'".$data['product_imageid']."',
												product_thumb	=	'".$data['product_thumbid']."',
												variation_exist	=	'1',
												parent_id		=	'".$productid."',
												product_code	=	'".addslashes($data['txtCode'][$i])."',
												style			=	'".addslashes(urldecode($data['txtStyle'][$i]))."',
												price			=	'".$data['txtPrice'][$i]."',
												supplier_stock	=	'".$data['txtSup'][$i]."',
												site_stock		=	'".$data['txtSite'][$i]."',
												available_stock	=	'".$data['txtAva'][$i]."',
												vat_id			=	'".$data['txtVat'][$i]."',
												status			=	'".$data['txtStat'][$i]."',											
												user_id			=	'".$logged_user."',
												shipping_type	=	'".$data['shipping_type']."',
												meta_title		=	'".addslashes($data['meta_title'])."',
												meta_desc		=	'".addslashes($data['meta_desc'])."',
												meta_keywords	=	'".addslashes($data['meta_keywords'])."',
												featured		=	'".$featured."',
												vat_moss		=		'".$vat_moss."',
												external_product=	'".$external_product."',
												created_date	=	now(),
												modified_date	=	now()");
			}
		}
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_products','add',$productid,'success');
    }

	public function generalupdate($data)
	{
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '0';	
		
		$pcatid		=	'';

		for($i=0;$i<count($data["procat_id"]);$i++){
			$pcatid			.= $data["procat_id"][$i].",";
		}

		$vat_count	=	count($data['txtCode']);

		if($vat_count>1)
			$exist	=	1;
		else
			$exist	=	0;

		if(!empty($data['featured']))
			$featured = $data['featured'];
		else
			$featured = 0;	
		
		if(!empty($data['txtVmoss']['0']))
			$vat_moss = 1;
		else
			$vat_moss = 0;	

		if(!empty($data['external_product']))
			$external_product = $data['external_product'];
		else
			$external_product = 0;	

		$query	=	$this->_db->getConnection()->query("UPDATE 3bit_products SET	
											procat_id		=	'".addslashes($pcatid)."',
											temp_id			=	'".addslashes($data['temp_id'])."',
											product_name	=	'".addslashes($data['product_name'])."',
											product_shortname=	'".addslashes($data['product_shortname'])."',
											short_text		=	'".addslashes($data['short_text'])."',
											description		=	'".addslashes($data['description'])."',
											testimonials	=	'".addslashes($data['testimonials'])."',
											product_code	=	'".addslashes($data['txtCode']['0'])."',
											style			=	'".addslashes(urldecode($data['txtStyle']['0']))."',											
											price			=	'".$data['txtPrice']['0']."',
											supplier_stock	=	'".$data['txtSup']['0']."',
											site_stock		=	'".$data['txtSite']['0']."',
											available_stock	=	'".$data['txtAva']['0']."',
											vat_id			=	'".$data['txtVat']['0']."',
											product_image	=	'".$data['product_imageid']."',
											product_thumb	=	'".$data['product_thumbid']."',											
											variation_exist	=	'".$exist."',
											user_id			=	'".$logged_user."',
											shipping_type	=	'".$data['shipping_type']."',
											meta_title		=	'".addslashes($data['meta_title'])."',
											meta_desc		=	'".addslashes($data['meta_desc'])."',
											meta_keywords	=	'".addslashes($data['meta_keywords'])."',
											featured		=	'".$featured."',
											vat_moss		=	'".$vat_moss."',
											external_product=	'".$external_product."',
											status			=	'".$data['status']."',											
											modified_date	=	now() WHERE product_id='".$data['id']."'");
											
		if($vat_count>1){
		
			for($i=1;$i<$vat_count;$i++){
				  $variation_id	 = '';
				  if(!empty($data["varProdid"][$i]))
					  $variation_id	=	trim($data["varProdid"][$i]);

					if(!empty($data['txtVmoss'][$i]))
						$vat_moss = 1;
					else
						$vat_moss = 0;	

				  $res	= $this->_db->fetchRow("SELECT * FROM `3bit_products` WHERE product_id ='".$variation_id."'");

				  if($res){
					  $this->_db->getConnection()->query("UPDATE 3bit_products SET
												procat_id		=	'".addslashes($pcatid)."',
												product_name	=	'".addslashes($data['product_name'])."',
												product_shortname=	'".addslashes($data['product_shortname'])."',
												short_text		=	'".addslashes($data['short_text'])."',
												description		=	'".addslashes($data['description'])."',
												testimonials	=	'".addslashes($data['testimonials'])."',
												product_image	=	'".$data['product_imageid']."',
												product_thumb	=	'".$data['product_thumbid']."',											
												variation_exist	=	'1',
												parent_id		=	'".$data['id']."',
												product_code	=	'".addslashes($data['txtCode'][$i])."',
												style			=	'".addslashes(urldecode($data['txtStyle'][$i]))."',
												price			=	'".$data['txtPrice'][$i]."',
												supplier_stock	=	'".$data['txtSup'][$i]."',
												site_stock		=	'".$data['txtSite'][$i]."',
												available_stock	=	'".$data['txtAva'][$i]."',
												vat_id			=	'".$data['txtVat'][$i]."',
												status			=	'".$data['txtStat'][$i]."',
												meta_title		=	'".addslashes($data['meta_title'])."',
												meta_desc		=	'".addslashes($data['meta_desc'])."',
												meta_keywords	=	'".addslashes($data['meta_keywords'])."',	
												user_id			=	'".$logged_user."',
												shipping_type	=	'".$data['shipping_type']."',
												vat_moss	=	'".$vat_moss."',
												external_product=	'".$external_product."',
												featured		=	'".$featured."' WHERE product_id='".$variation_id."'");

				  }else{
					  $query	=	$this->_db->getConnection()->query("INSERT INTO 3bit_products SET	
													procat_id		=	'".addslashes($pcatid)."',
													product_name	=	'".addslashes($data['product_name'])."',
													product_shortname=	'".addslashes($data['product_shortname'])."',
													short_text		=	'".addslashes($data['short_text'])."',
													description		=	'".addslashes($data['description'])."',
													testimonials	=	'".addslashes($data['testimonials'])."',
													product_image	=	'".$data['product_imageid']."',
													product_thumb	=	'".$data['product_thumbid']."',
													variation_exist	=	'1',
													parent_id		=	'".$data['id']."',
													product_code	=	'".addslashes($data['txtCode'][$i])."',
													style			=	'".addslashes(urldecode($data['txtStyle'][$i]))."',												
													price			=	'".$data['txtPrice'][$i]."',
													supplier_stock	=	'".$data['txtSup'][$i]."',
													site_stock		=	'".$data['txtSite'][$i]."',
													available_stock	=	'".$data['txtAva'][$i]."',
													vat_id			=	'".$data['txtVat'][$i]."',
													status			=	'".$data['txtStat'][$i]."',
													user_id			=	'".$logged_user."',
													shipping_type	=	'".$data['shipping_type']."',
													meta_title		=	'".addslashes($data['meta_title'])."',
													meta_desc		=	'".addslashes($data['meta_desc'])."',
													meta_keywords	=	'".addslashes($data['meta_keywords'])."',	
													featured		=	'".$featured."',
													vat_moss	=	'".$vat_moss."',
													external_product=	'".$external_product."',
													created_date	=	now(),
													modified_date	=	now()");
				  }

			}
		}

		$productid	=	$data['id'];

		$audit		=	new Application_Model_Audits();

		$audit->insert_audit('3bit_products','update',$productid,'success');
    }

	public function var_list($id){
		 $data		=	$this->_db->fetchAll("SELECT * FROM `3bit_products` WHERE (parent_id ='".$id."' OR product_id='".$id."') ORDER BY product_id ");		
		 $var_list	=	'';
		 if($data){			   
			for($i=0;$i<count($data);$i++){
				$vvarid		=	$data[$i]['product_id'];
				$vvarpcode	=	$data[$i]['product_code'];		
				$varsize	=	$data[$i]['size'];		
				$varprice	=	$data[$i]['price'];		
				$varsup		=	$data[$i]['supplier_stock'];
				$varsite	=	$data[$i]['site_stock'];
				$varava		=	$data[$i]['available_stock'];
				$varvat		=	$data[$i]['vat_id'];
				$varstat	=	$data[$i]['status'];

				$selVcode	=	$this->_db->fetchRow("SELECT vat_code FROM 3bit_product_vats WHERE vat_id ='".$varvat."' ");
				if($selVcode){																									
					$vatval	=	$selVcode['vat_code'];
				}
				if($varstat=='1')
					$dis	=	'Active';
				else
					$dis	=	'InActive';

				$var_list	.=	'<tr>
							 <input type="hidden" id="varProdid'.$i.'" name="varProdid[]" value="'.$vvarid.'"/>	
							 <td><nobr><input type="hidden" id="txtCode_'.$i.'" name="txtCode[]" value="'.$vvarpcode.'" />'.$vvarpcode.'</nobr></td>					
							 <td><nobr><input type="hidden" id="txtSize_'.$i.'" name="txtSize[]" value="'.$varsize.'" />'.$varsize.'</nobr></td>
	 						 <td><nobr><input type="hidden" id="txtPrice_'.$i.'" name="txtPrice[]" value="'.$varprice.'" />'.$varprice.'</nobr></td>	  			
							 <td><nobr><input type="hidden" id="txtSup_'.$i.'" name="txtSup[]" value="'.$varsup.'" />'.$varsup.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtSite_'.$i.'" name="txtSite[]" value="'.$varsite.'" />'.$varsite.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtAva_'.$i.'" name="txtAva[]" value="'.$varava.'" />'.$varava.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtVat_'.$i.'" name="txtVat[]" value="'.$varvat.'" />'.$vatval.'</nobr></td>
							 <td><nobr><input type="hidden" id="txtStat_'.$i.'" name="txtStat[]" value="'.$varstat.'" />'.$dis.'</nobr></td>
							 <td><input type="button" name="btnedit"  value="Edit" onClick=\'editRowVar(this.parentNode.parentNode.rowIndex,document.getElementById("txtCode_'.$i.'").value,document.getElementById("txtSize_'.$i.'").value,document.getElementById("txtPrice_'.$i.'").value,document.getElementById("txtSup_'.$i.'").value,document.getElementById("txtSite_'.$i.'").value,document.getElementById("txtAva_'.$i.'").value,document.getElementById("txtVat_'.$i.'").value,document.getElementById("txtStat_'.$i.'").value)\' class="button"></td>';
				if($i<>0){
						$var_list	.=	'<td><input type="button" name="btnview"  value="Delete" onClick="deleteRowVar(this.parentNode.parentNode.rowIndex)" class="button"></td>';
				}
				$var_list	.=	'</tr>';

			}
		}

	return $var_list;
   }  

	public function delete($id){	

		$this->_db->delete("3bit_products",'parent_id='.$id); 	

		$this->_db->delete("3bit_products",'product_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_products','delete',$id,'success');
	}

	public function active_procats($parentid){
	
		if($parentid=='0'){
			$sublib_qry  = $this->_db->fetchAll("SELECT procat_id,procat_title,procat_level,procat_parent_id FROM `3bit_product_category` WHERE procat_parent_id='$parentid'");
		}else{
			$sublib_qry  = $this->_db->fetchAll("SELECT procat_id,procat_title,procat_level,procat_parent_id FROM `3bit_product_category` WHERE procat_parent_id='$parentid' AND procat_level !='1' ORDER BY procat_level");
		}		
		
		$last_child  = '';
		
		if($sublib_qry){ 			
			foreach($sublib_qry as $sublib_values)	{
				$tmp				= '';
				$procat_id			= $sublib_values["procat_id"];				
				$subparent_id		= $sublib_values["procat_parent_id"];				
				$procat_title		= $sublib_values["procat_title"];																			
				$qry				= $this->_db->fetchRow("SELECT procat_id,procat_title,procat_level,procat_parent_id FROM `3bit_product_category` WHERE procat_parent_id='$procat_id'");	
				if($qry){
					$last_child			.= $this->active_procats($procat_id);//recursive call to the function						
				}else{
					if($subparent_id!='0'){
						$last_child		.=	'<dd><a href="/products/add/'.$procat_id.'">Add '.$procat_title.'</a></dd>';
					}
				}
							
			}	
			return $last_child;
		}else{
			return $last_child;
		}
	}

	public function list_procat($passdir,$i,$selected){
	
		$i.="---";
			
		$sublib_qry  = $this->_db->fetchAll("SELECT procat_id,procat_parent_id,procat_title FROM `3bit_product_category` WHERE procat_parent_id='$passdir'");		

		$second = '';
			
		if ($sublib_qry){ 			
			foreach($sublib_qry as $sublib_values)	{				
				$sublib_id		= $sublib_values["procat_id"];			
				$subparent_id	= $sublib_values["procat_parent_id"];				
				$sublib_name	= $sublib_values["procat_title"];	  				
				if ($selected == $sublib_id){								  						
					$sel ="selected";										  					
				}else{														  				
					$sel ="";												  					
				}															  				
				$second.= "<option value='$sublib_id' $sel>$i$sublib_name</option>";									
					
				$second.= $this->list_procat($sublib_id,$i,$selected);//recursive call to the function				
			}
			return $second;
		}else{			   		
			return $second;
		}
	}


	public function findprocatlevel($parent_id){

		$sublib_qry  = 	$this->_db->fetchRow("SELECT procat_id,procat_level FROM `3bit_product_category` WHERE procat_id='$parent_id'");
		
		if (!$sublib_qry){	
			$level	= 1;
		}else{
			$sublib_id	 = $sublib_qry['procat_id'];

			$cat_level	 = $sublib_qry['procat_level'];

			$level		 = $cat_level + 1;
		}
		return $level; 
	}

	public function findprocatorder($procat_level){

		$sublib_qry  = $this->_db->fetchRow("SELECT max(procat_order) as max_order from `3bit_product_category` where procat_level='$procat_level'");	
		
		if (!$sublib_qry){	
			$new_order	 = 1;
		}else{
			$new_order	 = $sublib_qry['max_order'] + 1;
		}

		return $new_order;

	}
}

?>