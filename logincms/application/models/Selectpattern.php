<?php

class Application_Model_Selectpattern
{

	private $imageonly;
	
	private $imageupload;
	
	private	$assetonly;
	
	private $assetupload;	

	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
		$this->_baseurl	= Zend_Registry::get("baseUrl");
    }       


	public function Select($upload,$category,$multiple,$formName,$uploadcat){
	
		if($upload=="no" && $category == "images" && $multiple == "no"){
	
			$returnVal	= $this->createForImages($formName,$uploadcat);
			
			return $returnVal;
		}
		
		if($upload=="yes" && $category == "images" && $multiple == "no"){
		
			$returnVal	=	$this->createForImageupload($formName,$uploadcat);
			
			return $returnVal;
		}	
		
		if($upload=="yes" && $category == "images" && $multiple == "yes"){
		
			$returnVal	=	$this->createForMultiImage($formName,$uploadcat);
			
			return $returnVal;
		}
		
		if($upload=="no" && $category == "assets" && $multiple == "no"){
		
			$returnVal	=	$this->createForAssets($formName,$uploadcat);
			
			return $returnVal;
		}
		if($upload=="yes" && $category == "assets" && $multiple == "no"){
		
			$returnVal	= $this->createForAssetupload($formName,$uploadcat);
			
			return $returnVal;
		}
		
		if($upload=="yes" && $category == "both" && $multiple == "no"){
		
			$returnVal	= $this->createForBothupload($formName,$uploadcat);
			
			return $returnVal;
		}
		
		if($upload=="yes" && $category == "both" && $multiple == "single"){
		
			$returnVal	= $this->createForBothsingleupload($formName,$uploadcat);
			
			return $returnVal;
		}
			
		if($upload=="yes" && $category == "assets" && $multiple == "yes"){
		
			$returnVal	= $this->createForMultipleAssetupload($formName,$uploadcat);
			
			return $returnVal;
		}
		
		if($upload=="yes" && $category == "assets" && $multiple == "single"){
		
			$returnVal	= $this->createForSingleupload($formName,$uploadcat);
			
			return $returnVal;
		}
			
	
	}
	
	public function Selectedit($upload,$category,$multiple,$formName,$upPath1,$chkPath1,$upPath2,$chkPath2,$uploadcat,$editId){
		
		if($upload=="yes" && $category == "assets" && $multiple == "no"){
		
			$returnVal	= $this->createForMultipleAssetedit($formName,$upPath1,$chkPath1,$upPath2,$chkPath2,$uploadcat,$editId);
		}
		
		return $returnVal;
	
	}
	
	public function remHyphen($str){
		$string	 	= 	strtolower($str);
		$special 	=	array('/','!','&','*',',','!','~','@','#','$','%','^','(',')','+','=','-'); 
		$dash		=	array('____','___','__');
		$spaceStr	=	str_replace($special,' ',$string);
		$dashStr	=	str_replace(' ','_',$spaceStr);
		$outStr		=	str_replace($dash,'_',$dashStr);
		return $outStr;
	}
	
	#	Function to select image only 	======================================

	public function createForImages($formName,$uploadcat){
		
			$imageonly	=	'';
			
			$name		=	$this->remHyphen($uploadcat);
			
			$id			=	$name."id";
			
			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/image/uploadcat/'.$uploadcat.'/uniqueid/1';
			
			$imageonly		.=	"<input type='text'  class='field text medium' name='".$name."'>&nbsp;&nbsp;<a class='button' href='".$sendURL."' rel='facebox'>Select</a>
			<a class='button' href='javascript:void(0)' onclick=\"javascript:ClearText(document.".$formName.".".$name.",document.".$formName.".".$id.");\">Clear</a><input type=\"hidden\" name=\"$id\" id=\"$id\" value='' />";	

			//".$featureImgID."
													
			$this->imageonly	=	$imageonly;
		
		return $this->imageonly;
	}
	
	#	Function to upload images and select image ==========================
	
	public function createForImageupload($formName,$uploadcat){
	
			$imageupload	=	'';
			
			$name			=	$this->remHyphen($uploadcat);
			
			$id				=	$name."id";				

			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/image/uploadcat/'.$uploadcat.'/uniqueid/1';
						
			$imageupload	.=	"<input type='text' class='field text medium' name='".$name."' id='".$name."'>&nbsp;&nbsp;
								<a class='button'    href='".$sendURL."' rel='facebox'>Select</a>
								<a class='button' href='javascript:void(0)' onclick=\"javascript:ClearText(document.".$formName.".".$name.",document.".$formName.".".$id.");$('#span_".$name."').html('');\">Clear</a>
								<input type=\"hidden\" name=\"$id\" id=\"$id\" value='' />
								<span id=\"span_$name\"></span>";	
					
			$this->imageupload	=	$imageupload;
		
		return $imageupload;
	
	}
	
	#	Function to upload assets and select assets ==========================
	
	public function createForMultiImage($formName,$uploadcat){
	
			$assetupload	=	'';
			
			$name			=	$this->remHyphen($uploadcat);
			
			$id				=	$name."id";
			
			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/image-upload/uploadcat/'.$uploadcat.'/uniqueid/multiple';
					
			$assetupload	.=	"<input type='text'  class='field text medium' name='".$name."'>&nbsp;&nbsp;<a class='button'  href='".$sendURL."' rel='facebox'>Select</a>
					<a class='button' href='javascript:void(0)' onclick=\"document.getElementById('".$name."2').innerHTML='';document.getElementById('".$id."').value='';\">Clear</a><br><input type='hidden' id='".$id."' name='".$id."'><input type='hidden' id='name_".$id."' name='name_".$id."'><span id='".$name."2'></span>";	
					
			$this->assetupload	=	$assetupload;
		
		return $assetupload;
	
	}
	
	
	#	Funtion to select assets only =====================================
	
	public function createForAssets($formName,$uploadcat){
	
			$assetonly		=	'';
			
			$name			=	$this->remHyphen($uploadcat);
			
			$id				=	$name."id";
			
			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/asset/uploadcat/'.$uploadcat.'/uniqueid/1';
			
			$assetonly		.=	"<input type='text'  class='field text medium' name='".$name."' >&nbsp;&nbsp;<a class='button'  href='".$sendURL."' rel='facebox'>Select</a>
					<a class='button' href='javascript:void(0)' onclick=\"document.getElementById('file_names1').innerHTML='';document.getElementById('".$name."').value='';\">Clear</a><br><input type='hidden' id='$id' name='$id'><span id='file_names1'></span>";	
		
		$this->assetonly	=	$assetonly;
				
		return $assetonly;
	}
	
	
	#	Function to upload assets and select assets ==========================
	
	public function createForAssetupload($formName,$uploadcat){
	
			$assetupload	=	'';
			
			$name			=	$this->remHyphen($uploadcat);
			
			$id				=	$name."id";
			
			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/asset-upload/uploadcat/'.$uploadcat.'/uniqueid/2';
					
			$assetupload	.=	"<input type='text'  class='field text medium' name='".$name."'>&nbsp;&nbsp;<a class='button' href='".$sendURL."' rel='facebox'>Select</a>
					<a class='button' href='javascript:void(0)' onclick=\"document.getElementById('file_names2').innerHTML='';document.getElementById('".$id."').value='';\">Clear</a><br><input type='hidden' id='".$id."' name='".$id."'><span id='file_names2'></span>";	
					
			$this->assetupload	=	$assetupload;
		
		return $assetupload;
	
	}
	
	#	Function to upload single both asset ==============================
	
	public function createForBothsingleupload($formName,$uploadcat){
	
			$singleupload	=	'';
			
			$name			=	$this->remHyphen($uploadcat);
			
			$id				=	$name."id";
			
			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/both-upload/uploadcat/'.$uploadcat.'/uniqueid/single';
					
			$singleupload	.=	"<input type='text'  class='field text medium' id='".$name."' name='".$name."' value=''>&nbsp;&nbsp;<a class='button' href='".$sendURL."' rel='facebox'>Select</a>
			<a class='button' href='javascript:void(0)' onclick=\"document.getElementById('".$id."').value='';document.getElementById('".$name."').value='';\">Clear</a><br><input type='hidden' id='".$id."' name='".$id."'>";	
					
			$this->singleupload	=	$singleupload;
		
		return $singleupload;
	
	}
	
	#	Function to upload single asset ==============================
	
	public function createForSingleupload($formName,$uploadcat){
	
			$singleupload	=	'';
			
			$name			=	$this->remHyphen($uploadcat);
			
			$id				=	$name."id";
			
			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/asset-upload/uploadcat/'.$uploadcat.'/uniqueid/single';
					
			$singleupload	.=	"<input type='text'  class='field text medium' name='".$name."' id='".$name."' value=''>&nbsp;&nbsp;
			<a class='button' href='".$sendURL."' rel='facebox'>Select</a>
			<a class='button' href='javascript:void(0)' onclick=\"javascript:ClearText(document.".$formName.".".$name.",document.".$formName.".".$id.");\">Clear</a><br><input type='hidden' id='".$id."' name='".$id."'>";	
					
			$this->singleupload	=	$singleupload;
		
		return $singleupload;
	
	}
	
	#	Function to upload both assets and images ==============================
	
	public function createForBothupload($formName,$uploadcat){
	
			$bothupload	=	'';
			
			$name			=	"both".$this->remHyphen($uploadcat);
			
			$id				=	$name."id";
			
			$sendURL		=	$this->_baseurl.'/common/selectimages/number/'.$name.'/form/'.$formName.'/category/both-upload/uploadcat/'.$uploadcat.'/uniqueid/1';
					
			$bothupload	.=	"<input type='text'  class='field text medium' id='both1' name='".$name."' value='".$both1."'>&nbsp;&nbsp;<a class='button' href='".$sendURL."' rel='facebox'>Select</a>
			<a class='button' href='javascript:void(0)' onclick=\"document.getElementById('file_names1').innerHTML='';document.getElementById('".$id."').value='';\">Clear</a><br><input type='hidden' id='$id' name='$id'><span id='file_names1' style='font-family: verdana; font-size: 9pt; font-style: normal;'></span>";	
					
			$this->bothupload	=	$bothupload;
		
		return $bothupload;
	
	}
		
	#	Function to upload assets and select assets with multiple download option ==========================
	
	public function createForMultipleAssetupload($formName,$uploadcat){
	
			//$multiassetupload	=	$this->script;
			
			$multiassetupload	=	"<script name='javascript'>
										function OpenSelectPop1(url,EditId,val1,val2,uniqueID){
											//alert(uniqueID);
											var passMe	=	url+'&editId='+EditId+'&category='+val1+'&uploadcat='+val2+'&uniqueID='+uniqueID;
											window.open(passMe,'SelectImage','scrollbars=yes,menubar=no,width=700,height=500');
										}
										var i=2;
						
									function InsertImageBox(frmName,path1,chkpath1,path2,chkpath2,upcat) 
									{
										var x = document.getElementById('myTable').insertRow(0);
										var w = x.insertCell(0);
										var y = x.insertCell(1);
										var z=x.insertCell(2);
										ran_unrounded=Math.random()*1000;
										ran_number=Math.floor(ran_unrounded); 
										var fidname	=	eval('document.forms.'+frmName+'.getAllId');
										fidname.value+=ran_number+'#';	
										txtBoxName		=	'txtProperty_download_name'+ran_number;
										hiddenBoxName	=	'asset'+ran_number;
										spanName		=	'file_names'+ran_number;
										upcatgo			=	upcat;
										urlSend			=	'select_images.php?path1='+path1+'&path2='+chkpath1+'&tpath1='+path2+'&tpath2='+chkpath2+'number=asset2&form='+frmName;
										//alert(urlSend);
										
										w.innerHTML 	= 	'<td align=\"right\" style=\"text-align:right;padding-left:106px; padding-right:9px;\"><span style=\"font-size:11px;vertical-align:top;text-align:right;\">Downloads Title</span></td>';
										y.innerHTML		=	'<td><span  style=\"font-size:11px;vertical-align:top;vertical-align:top;\">:</span></td>';
										
										z.innerHTML		=	'<td align=\"left\" style=\"padding-left:9px;\"><input type=\"text\" class=\"css_input_add\" name='+txtBoxName+' style=\"border: 1px solid #D3D3FC;height:15px;width:170px;\" >&nbsp;<input type=\"button\" class=\"button\" value=\"Select\" onclick=\"OpenSelectPop1(urlSend,document.getElementById(hiddenBoxName).value,\'asset-upload\',upcatgo,ran_number);\">&nbsp;<input type=\"button\" name=\"btnview\"  value=\"Delete\" onClick=\"deleteRowimg1(this.parentNode.parentNode.rowIndex)\" class=\"button\">&nbsp;<input type=\"hidden\" id='+hiddenBoxName+' name='+hiddenBoxName+'><span style=\"font-size:11px;text-align:left\" id='+spanName+'></span></td>';
										i++;
										
										//alert(fidname.value);
									}
									function deleteRowimg1(i)
									{	
										document.getElementById('myTable').deleteRow(i);
									}	
			
						</script>";
			
			$multiassetupload	.=	"<input type='hidden' name='getAllId' id='getAllId' value='A1#'><input type='text' name='txtProperty_download_nameA1' class='field text medium' />&nbsp;&nbsp;<input type='button' class='button' value='Select' onclick=\"OpenSelectPop1('select_images.php?number=asset2&form=".$formName."',document.getElementById('assetA1').value,'asset-upload','".$uploadcat."','A1');\">&nbsp;<input type='button' name='addnew' value='Add' onclick=\"javascript:InsertImageBox('".$formName."','".$upPath1."','".$chkPath1."','".$upPath2."','".$chkPath2."','".$uploadcat."');\" class='button'>
				<input type='hidden' id='assetA1' name='assetA1'><span id='file_namesA1'></span>";
				
			$this->multiassetupload	=	$multiassetupload;
		
		return $multiassetupload;
	
	}
	
	
	#	Function to upload assets and select assets ==========================
	
	public function createForMultipleAssetedit($formName,$uploadcat,$editId){
	
			$assetedit	=	"<script typt='javascript'>
								function deleteRowimg(frmName,i,j)
									{	
										var fieldname	=	eval('document.forms.'+frmName+'.getAllDeleteId');
										fieldname.value+=j+'#';	
										document.getElementById('dbTable').deleteRow(i);
									}
							</script>";
								
			$assetedit	.=	"<input type='text'  class='field text medium' id='txtProperty_download_name".$editId."' name='txtProperty_download_name".$editId."'><input type='hidden'  class='field text medium' id='asset".$editId."' name='asset".$editId."' value='".$asset2."'>&nbsp;&nbsp;<input type='button' value='Select' onclick=\"OpenSelectPop1('".$this->_baseurl."/common/selectimages/number/asset2/form/".$formName."',document.getElementById('asset".$editId."').value,'asset-upload','".$uploadcat."','".$editId."');\" class=\"button\">
					<input type='Button' name='' value='Delete' class='button'  onClick=\"deleteRowimg('".$formName."',this.parentNode.parentNode.rowIndex,'".$editId."')\" /><br><input type='hidden' id='hassetid".$editId."' name='hassetid".$editId."'><span id='file_names".$editId."'></span>";	
					
			$this->assetedit	=	$assetedit;
		
		return $assetedit;
	
	}
	

}

?>