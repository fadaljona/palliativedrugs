<?php

class Application_Model_Newstypes
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM news_types WHERE typeid='".$id."' ";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';

		$sql	=	"INSERT INTO news_types SET
								type_name			=	'".addslashes($data['type_name'])."',
								note				=	'".addslashes($data['note'] )."',
								user_id				=	'".addslashes($logged_user)."',
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now()";

		$query	=	$this->_db->query($sql);

		$typeid	=	$this->_db->lastInsertId();		
    }

	public function update($data)
	{
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';

		$sql	=	"UPDATE news_types SET
								type_name			=	'".addslashes($data['type_name'])."',
								note				=	'".addslashes($data['note'] )."',
								user_id				=	'".addslashes($logged_user)."',
								status				=	'".$data['status']."',						
								modified_date		=	now() WHERE typeid='".$data['id']."'";
		
		$query	=	$this->_db->query($sql);

		$userid	=	$data['id'];

		
    }

	public function delete($id){

		$this->_db->delete("news_types",'typeid='.$id); 			
	}
}

?>