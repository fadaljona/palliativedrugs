<?php

class Application_Model_Sddb
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
		$this->_baseurl	=	Zend_Registry::get("baseUrl");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM sddb WHERE id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('sddb','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function GetDbTimestamp() 
	{
		$now		= getdate();
		$mday		= $now['mday'];
		$month		= $now['mon'];
		$year		= $now['year'];
		$hours		= $now['hours'];
		$minutes	= $now['minutes'];
		$seconds	= $now['seconds'];
		$timestamp	= "$year-$month-$mday $hours:$minutes:$seconds";
		return $timestamp;
	}


	public function add($data)
    {	
		$unit_code		=		$data['unit_code'];
		$unit_ref		=		$data['unit_ref'];
		$drug1_name		=		trim($data['drug1_name']);
		$drug1_dose		=		($data['drug1_dose']) ? $data['drug1_dose'] : 0;
		$drug2_name		=		$data['drug2_name'];
		$drug2_dose		=		($data['drug2_dose']) ? $data['drug2_dose'] : 0;
		$drug3_name		=		$data['drug3_name'];
		$drug3_dose		=		($data['drug3_dose']) ? $data['drug3_dose'] : 0;
		$drug4_name		=		$data['drug4_name'];
		$drug4_dose		=		($data['drug4_dose']) ? $data['drug4_dose'] : 0;
		$drug5_name		=		$data['drug5_name'];
		$drug5_dose		=		($data['drug5_dose']) ? $data['drug5_dose'] : 0;
		$drug6_name		=		$data['drug6_name'];
		$drug6_dose		=		($data['drug6_dose']) ? $data['drug6_dose'] : 0;
		$volume			=		($data['volume']) ? $data['volume'] : 0 ;
		$diluent		=		$data['diluent'];
		if ($diluent == 'Please select ...') { $diluent = 'unknown'; }
		$duration		=		$data['time'];
		if ($duration == 'Please select ...') { $duration = 'unknown'; }
		$duration_other =		$data['duration_other'];
		$compatibility	=		$data['compatibility'];
		if ($compatibility == 'Please select ...') { $compatibility = 'unknown'; }
		$datatype		=		$data['datatype'];
		if ($datatype == 'Please select ...') { $datatype = 'unknown'; }
		$site_reaction	=		$data['reaction'];
		if ($site_reaction == 'Please select ...') { $site_reaction = 'unknown'; }
		$comments		=		$data['comments'];
		$support_data	=		$data['support_data'];	
		
		$approved		=		(isset($data["chkApproved"])) ? 1 : 0;	
		$show_details_link =	(isset($data["chkShowDetailsLink"])) ? 1 : 0;
		$added_date		=		$this->GetDbTimestamp();

		$sql			=		"INSERT INTO sddb SET 
												unit_code		=	'".$unit_code."',
												unit_ref		=	'".$unit_ref."',
												drug1_name		=	'".$drug1_name."',
												drug1_dose		=	'".$drug1_dose."',
												drug2_name		=	'".$drug2_name."',
												drug2_dose		=	'".$drug2_dose."',";
		
		if(!empty($drug3_name)) {
			$sql		.=		"drug3_name		=	'".$drug3_name."', "
								."drug3_dose	=	'".$drug3_dose."', ";
		} else {
			$sql		.=		"drug3_name		=	'0', "
								."drug3_dose	=	'0', ";
		}
		if(!empty($drug4_name)) {
			$sql		.=		"drug4_name		=	'".$drug4_name."', "
								."drug4_dose	=	'".$drug4_dose."', ";
		} else {
			$sql		.=		"drug4_name		=	'0', "
								."drug4_dose	=	'0', ";
		}
		if(!empty($drug5_name)) {
			$sql		.=		"drug5_name		=	'".$drug5_name."', "
								."drug5_dose	=	'".$drug5_dose."', ";
		} else {
			$sql		.=		"drug5_name		=	'0', "
								."drug5_dose	=	'0', ";
		}
		if(!empty($drug6_name)) {
			$sql		.=		"drug6_name		=	'".$drug6_name."', "
								."drug6_dose	=	'".$drug6_dose."', ";
		} else {
			$sql		.=		"drug6_name		=	'0', "
								."drug6_dose	=	'0', ";
		}

		$sql			.=		"volume			=	'".$volume."', "
								."diluent		=	'".$diluent."', "
								."duration		=	'".$duration."', "
								."duration_other=	'".$duration_other."', "
								."compatibility	=	'".$compatibility."', "
								."datatype		=	'".$datatype."', "
								."site_reaction	=	'".$site_reaction."', "			
								."comments		=	'".$comments."', "
								."support_data	=	'".$support_data."', "
								."show_details_link='".$show_details_link."', ";

		if(isset($added_by)) {
			$sql		.=		"added_by		=	'".$added_by."', ";
		}
				
		if(isset($added_date)) {
			$sql		.=		"added_date		=	'".$added_date."', ";
		}

		$sql			.=		"approved		=	'".$approved."' ";		
												

		$query	=	$this->_db->query($sql);

		$id		=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('sddb','add',$id,'success');
    }

	public function update($data)
	{
		$unit_code		=		$data['unit_code'];
		$unit_ref		=		$data['unit_ref'];
		$drug1_name		=		trim($data['drug1_name']);
		$drug1_dose		=		($data['drug1_dose']) ? $data['drug1_dose'] : 0;
		$drug2_name		=		$data['drug2_name'];
		$drug2_dose		=		($data['drug2_dose']) ? $data['drug2_dose'] : 0;
		$drug3_name		=		$data['drug3_name'];
		$drug3_dose		=		($data['drug3_dose']) ? $data['drug3_dose'] : 0;
		$drug4_name		=		$data['drug4_name'];
		$drug4_dose		=		($data['drug4_dose']) ? $data['drug4_dose'] : 0;
		$drug5_name		=		$data['drug5_name'];
		$drug5_dose		=		($data['drug5_dose']) ? $data['drug5_dose'] : 0;
		$drug6_name		=		$data['drug6_name'];
		$drug6_dose		=		($data['drug6_dose']) ? $data['drug6_dose'] : 0;
		$volume			=		($data['volume']) ? $data['volume'] : 0 ;
		$diluent		=		$data['diluent'];
		if ($diluent == 'Please select ...') { $diluent = 'unknown'; }
		$duration		=		$data['time'];
		if ($duration == 'Please select ...') { $duration = 'unknown'; }
		$duration_other =		$data['duration_other'];
		$compatibility	=		$data['compatibility'];
		if ($compatibility == 'Please select ...') { $compatibility = 'unknown'; }
		$datatype		=		$data['datatype'];
		if ($datatype == 'Please select ...') { $datatype = 'unknown'; }
		$site_reaction	=		$data['reaction'];
		if ($site_reaction == 'Please select ...') { $site_reaction = 'unknown'; }
		$comments		=		$data['comments'];
		$support_data	=		$data['support_data'];	
		
		$approved		=		(isset($data["chkApproved"])) ? 1 : 0;	
		$show_details_link =	(isset($data["chkShowDetailsLink"])) ? 1 : 0;		

		$sql			=		"UPDATE sddb SET 
												unit_code		=	'".$unit_code."',
												unit_ref		=	'".$unit_ref."',
												drug1_name		=	'".$drug1_name."',
												drug1_dose		=	'".$drug1_dose."',
												drug2_name		=	'".$drug2_name."',
												drug2_dose		=	'".$drug2_dose."',";
		
		if(!empty($drug3_name)) {
			$sql		.=		"drug3_name		=	'".$drug3_name."', "
								."drug3_dose	=	'".$drug3_dose."', ";
		} else {
			$sql		.=		"drug3_name		=	'0', "
								."drug3_dose	=	'0', ";
		}
		if(!empty($drug4_name)) {
			$sql		.=		"drug4_name		=	'".$drug4_name."', "
								."drug4_dose	=	'".$drug4_dose."', ";
		} else {
			$sql		.=		"drug4_name		=	'0', "
								."drug4_dose	=	'0', ";
		}
		if(!empty($drug5_name)) {
			$sql		.=		"drug5_name		=	'".$drug5_name."', "
								."drug5_dose	=	'".$drug5_dose."', ";
		} else {
			$sql		.=		"drug5_name		=	'0', "
								."drug5_dose	=	'0', ";
		}
		if(!empty($drug6_name)) {
			$sql		.=		"drug6_name		=	'".$drug6_name."', "
								."drug6_dose	=	'".$drug6_dose."', ";
		} else {
			$sql		.=		"drug6_name		=	'0', "
								."drug6_dose	=	'0', ";
		}

		$sql			.=		"volume			=	'".$volume."', "
								."diluent		=	'".$diluent."', "
								."duration		=	'".$duration."', "
								."duration_other=	'".$duration_other."', "
								."compatibility	=	'".$compatibility."', "
								."datatype		=	'".$datatype."', "
								."site_reaction	=	'".$site_reaction."', "			
								."comments		=	'".$comments."', "
								."support_data	=	'".$support_data."', "
								."show_details_link='".$show_details_link."', ";

		if(isset($added_by)) {
			$sql		.=		"added_by		=	'".$added_by."', ";
		}
				
		if(isset($added_date)) {
			$sql		.=		"added_date		=	'".$added_date."', ";
		}

		$sql			.=		"approved		=	'".$approved."' "
								." WHERE id		=	'".$data['id']."'";	

		//die($sql);

		$query	=	$this->_db->query($sql);

		$id		=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('sddb','update',$id,'success');
    }

	public function delete($id){

		$this->_db->delete("sddb",'id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('sddb','delete',$id,'success');
	}

	public function listings($data)
	{

		if($data['qry_type']=='search' || !empty($data['drug1_name'])){
			
			$drug1 = $data['drug1_name'];
			$drug2 = $data['drug2_name'];
			$drug3 = $data['drug3_name'];
			$drug4 = $data['drug4_name'];
			$drug5 = $data['drug5_name'];
			$drug6 = $data['drug6_name'];
			$exact = isset($data['exact']) ? 1 : 0;
			
			if($exact) {
				$operator = "AND";
			} else {
				$operator = "OR";
			}
			
			$sql = "SELECT * FROM sddb WHERE (drug1_name='$drug1' OR drug2_name='$drug1' OR drug3_name='$drug1' OR drug4_name='$drug1' OR drug5_name='$drug1' OR drug6_name='$drug1')";
			if($drug2) {
				$sql .= " $operator (drug1_name='$drug2' OR drug2_name='$drug2' OR drug3_name='$drug2' OR drug4_name='$drug2' OR drug5_name='$drug2' OR drug6_name='$drug2')";
			}
			if($drug3) {
				$sql .= " $operator (drug1_name='$drug3' OR drug2_name='$drug3' OR drug3_name='$drug3' OR drug4_name='$drug3' OR drug5_name='$drug3' OR drug6_name='$drug3')";
			}
			
			if($drug4) {
				$sql .= " $operator (drug1_name='$drug4' OR drug2_name='$drug4' OR drug3_name='$drug4' OR drug4_name='$drug4' OR drug5_name='$drug4' OR drug6_name='$drug4')";
			}
			
			if($drug5) {
				$sql .= " $operator (drug1_name='$drug5' OR drug2_name='$drug5' OR drug3_name='$drug5' OR drug4_name='$drug5' OR drug5_name='$drug5' OR drug6_name='$drug5')";
			}
			
			if($drug6) {
				$sql .= " $operator (drug1_name='$drug6' OR drug2_name='$drug6' OR drug3_name='$drug6' OR drug4_name='$drug6' OR drug5_name='$drug6' OR drug6_name='$drug6')";
			}			

		}else{
			if($data['type']=='A'){
				$sql	=	"SELECT * FROM sddb WHERE 1";
				if(!empty($data['diluent']) && $data['diluent']!='A' )
					$sql	.=  " AND diluent ='".$data['diluent']."' ";
			}

			if($data['type']=='ap'){
				$sql	=	"SELECT * FROM sddb WHERE approved='1' ";
				if(!empty($data['diluent']) && $data['diluent']!='A')
					$sql	.=  " AND diluent ='".$data['diluent']."' ";
			}

			if($data['type']=='lu'){
				$sql	=	"SELECT * FROM sddb WHERE approved='0' ";
				if(!empty($data['diluent']) && $data['diluent']!='A')
					$sql	.=  " AND diluent ='".$data['diluent']."' ";
			}

			if($data['type']=='ua'){
				$sql	=	"SELECT * FROM sddb_temp WHERE approved='0' ";
				if(!empty($data['diluent']) && $data['diluent']!='A')
					$sql	.=  " AND diluent ='".$data['diluent']."' ";
			}	
			
			$sql		.=	" ORDER BY unit_code, unit_ref";
		}
		
		//echo($sql);
		$rs			=	$this->_db->query($sql)->fetchAll();		
		$list		=	'
	<table width="100%" class="display" id="sddb">
		<thead>
		<tr>
			<th rowspan="2">Centre code / Record num</th>
			<th rowspan="2">Drug</th>
			<th rowspan="2">Dose in syringe (mg)</th>
			<th rowspan="2">Final volume in syringe (ml)</th>
			<th rowspan="2">Concentration (mg/ml)</th>
			<th rowspan="2">Diluent</th>
			<th colspan="3">Compatibility information</th>
			<th rowspan="2">Site reaction reported</th>
			<th rowspan="2">User Name</th>
			<th rowspan="2">&nbsp;</th>
		</tr>
		<tr>
			<th>Outcome</th>
			<th>Duration</th>
			<th>Data type</th>				
		</tr>
		</thead>';

		if(count($rs)==0) {
			$list.= "<tr>
					<td colspan='11'>No matching entries found.</td>
				  </tr>";
		} else {		
			$k=1;
			foreach($rs as $row) {
				//need to know how many drugs in this combo for rowspanning				
				if($row['drug6_name']) {
					$numdrugs=6;
				} elseif($row['drug5_name']) {
					$numdrugs=5;
				} elseif($row['drug4_name']) {
					$numdrugs=4;
				} elseif($row['drug3_name']) {
					$numdrugs=3;
				} else {
					$numdrugs=2;
				}	
				if($row['duration']=='other') {
					$duration	=	$row['duration_other'];
				} else {
					$duration	=	$row['duration'];
				}

				$username = $this->_db->fetchOne("SELECT username FROM new_users WHERE id='".$row['added_by']."'");							
				if(!empty($username))
					$addedby	=	$username;
				else
					$addedby	=	"Unknown";

				if($data['type']=='ua'){
					$url		=	$this->_baseurl.'/sddb/surveyedit/id/'.$row['id'];
				}else{
					$url		=	$this->_baseurl.'/sddb/add/id/'.$row['id'];
				}

				$drug_cons		=	array();
				$drug_name		=	array();
				$drug_dose		=	array();
					
				for($i=1;$i<=$numdrugs;$i++){
					$drug_cons[$i]				=	@number_format($row['drug'.$i.'_dose'] / $row['volume'],2);
					$drug_name[$drug_cons[$i]]	=	$row['drug'.$i.'_name'];
					$drug_dose[$drug_cons[$i]]	=	$row['drug'.$i.'_dose'];
				}
				sort($drug_cons);
				ksort($drug_name);
				ksort($drug_dose);

				/*echo("<pre>");
				print_r($drug_cons);
				print_r($drug_name);
				print_r($drug_dose);
				echo("</pre>");*/

				if($k%2==0)
					$class	=	' class="sddb"';
				else
					$class	=	'';
				$list.= '<tr'.$class.'>
					<td rowspan="'.$numdrugs.'"><input type="checkbox" autocomplete="OFF" value="'.$row['id'].'" name="check_'.$row['id'].'" id="check_'.$row['id'].'">'.$row['unit_code'].' / '.$row['unit_ref'].'</td>
					<td>'.$drug_name[$drug_cons['0']].'</td>
					<td align="center">'.$drug_dose[$drug_cons['0']].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['volume'].'</td>
					<td align="center">'.$drug_cons['0'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['diluent'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['compatibility'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$duration.'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['datatype'].'</td>				
					<td align="center" rowspan="'.$numdrugs.'">'.$row['site_reaction'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$addedby.'</td>
					<td align="center" rowspan="'.$numdrugs.'"><a class="button"  href="'.$url.'"><img border="0" title="Edit" src="'.$this->_baseurl.'/public/images/icons/page_white_text.png"/>Edit<span class="button"></span></a>						
					</td>
				</tr>
				<tr>
					<td>'.$drug_name[$drug_cons['1']].'</td>
					<td align="center">'.$drug_dose[$drug_cons['1']].'</td>
					<td align="center">'.$drug_cons['1'].'</td>
				</tr>';
				
				if($row['drug3_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['2']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['2']].'</td>
								<td align="center">'.$drug_cons['2'].'</td>
							</tr>';
				}
				
				if($row['drug4_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['3']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['3']].'</td>
								<td align="center">'.$drug_cons['3'].'</td>
							</tr>';
				} 			
				
				if($row['drug5_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['4']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['4']].'</td>
								<td align="center">'.$drug_cons['4'].'</td>
							</tr>';
				 } 			
				
				if($row['drug6_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['5']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['5']].'</td>
								<td align="center">'.$drug_cons['5'].'</td>
							</tr>';
				}			
				
				
				$k++;
				
			} //foreach			
		}
		$list.=	'</table>';
		if($data['qry_type']!='search' && empty($data['drug1_name'])){
			$list.=	'^S';
			die($list);
		}else{
			return ($list);
		}
		
	}

	public function browseDrugs($data)
	{

		$form_count=0;

		$filter		=	$data['drug'];

		if($filter=="A" || $filter=="2") { 
		?>
		<table align="center" width="100%" class="display">
			<thead>
			<tr><th colspan="3">Two Drugs</th></tr>
			<tr><th>drug 1</th><th>drug 2</th><th></th></tr>
			</thead>
		<?
		$sql = "select distinct drug1_name, drug2_name from sddb "."where  drug3_name='0' and drug4_name='0' and drug5_name='0' and drug6_name='0' order by drug1_name, drug2_name asc ";

		$rs			=		$this->_db->query($sql)->fetchAll();

			foreach($rs as $row) {
		?>
				<tr>
					<td><?=$row['drug1_name'];?></td>
					<td><?=$row['drug2_name'];?></td>					
					<td align="center">
						<form action="search" method="POST">
						<input type="hidden" name="drug1_name" value="<?=$row['drug1_name']?>">
						<input type="hidden" name="drug2_name" value="<?=$row['drug2_name']?>">
						<input type="hidden" name="exact" value="1">
						<a href="javascript:document.forms[<?=$form_count?>].submit();" class="button"><img border="0" title="Edit" src="<?=$this->_baseurl?>/public/images/icons/details.gif"/>View<span class="button"></span></a>
						</form>
					</td>									
				</tr>
		<? $form_count++;
				
			}
		?>
		</table>
		<br /><br />
		<? }
		if($filter=="A" || $filter=="3") { 
		?>
		<table align="center" width="100%" class="display">
		  <thead>
			<tr><th colspan="4">Three Drugs</th></tr>
			<tr>
				<th>drug 1</th>
				<th>drug 2</th>
				<th>drug 3</th>
				<th></th>
			</tr>
		  </thead>
		<?
		$sql = "select distinct drug1_name, drug2_name, drug3_name from sddb where  drug3_name!='0' and drug4_name='0' and drug5_name='0' and drug6_name='0' order by drug1_name, drug2_name, drug3_name asc";

		$rs			=		$this->_db->query($sql)->fetchAll();
		
			foreach($rs as $row){
		?>
			<tr>
			<td><?=$row['drug1_name'];?></td>
			<td><?=$row['drug2_name'];?></td>
			<td><?=$row['drug3_name'];?></td>			
			<td align="center">
				<form action="search" method="POST">
				<input type="hidden" name="drug1_name" value="<?=$row['drug1_name']?>">
				<input type="hidden" name="drug2_name" value="<?=$row['drug2_name']?>">
				<input type="hidden" name="drug3_name" value="<?=$row['drug3_name']?>">
				<input type="hidden" name="exact" value="1">
				<a href="javascript:document.forms[<?=$form_count?>].submit();" class="button"><img border="0" title="Edit" src="<?=$this->_baseurl?>/public/images/icons/details.gif"/>View<span class="button"></span></a>
				</form>
			</td>			
			</tr>
		<?  $form_count++;
			}
		?>
		</table>
		<br /><br />
		<? }	
		
		if($filter=="A" || $filter=="4") { 
		
		?>
		<table align="center" width="100%" class="display">
		<thead>
			<tr><th colspan="5">Four Drugs</th></tr>
			<tr>
				<th>drug 1</th>
				<th>drug 2</th>
				<th>drug 3</th>
				<th>drug 4</th>
				<th></th>
			</tr>
		</thead>
		<?
		$sql	=	"select distinct drug1_name, drug2_name, drug3_name, drug4_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name='0' and drug6_name='0' order by drug1_name, drug2_name, drug3_name, drug4_name asc";

		$rs			=		$this->_db->query($sql)->fetchAll();
		
			foreach($rs as $row){
		?>
				<tr>
					<td><?=$row['drug1_name']?></td>
					<td><?=$row['drug2_name']?></td>
					<td><?=$row['drug3_name']?></td>
					<td><?=$row['drug4_name']?></td>					
					<td align="center">
						<form action="search" method="POST">
						<input type="hidden" name="drug1_name" value="<?=$row['drug1_name']?>">
						<input type="hidden" name="drug2_name" value="<?=$row['drug2_name']?>">
						<input type="hidden" name="drug3_name" value="<?=$row['drug3_name']?>">
						<input type="hidden" name="drug4_name" value="<?=$row['drug4_name']?>">
						<input type="hidden" name="exact" value="1">
						<a href="javascript:document.forms[<?=$form_count?>].submit();" class="button"><img border="0" title="Edit" src="<?=$this->_baseurl?>/public/images/icons/details.gif"/>View<span class="button"></span></a>
						</form>				
					</td>					
				</tr>
		<? $form_count++;
			} 
		?>
		</table>
		<br /><br />
		<? } ?>

		<? if($filter=="A" || $filter=="5") { ?>
		<table align="center" width="100%" class="display">
		<thead>
			<tr><th colspan="6">Five Drugs</th></tr>
			<tr>
				<th>drug 1</th>
				<th>drug 2</th>
				<th>drug 3</th>
				<th>drug 4</th>
				<th>drug 5</th>
				<th></th>
			</tr>
		</thead>
		<?
		$sql = "select distinct drug1_name, drug2_name, drug3_name, drug4_name, drug5_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name!='0' and drug6_name='0' order by drug1_name, drug2_name, drug3_name, drug4_name, drug5_name asc";
		
		$rs			=		$this->_db->query($sql)->fetchAll();
		
			foreach($rs as $row){
		?>
			<tr>
				<td><?=$row['drug1_name']?></td>
				<td><?=$row['drug2_name']?></td>
				<td><?=$row['drug3_name']?></td>
				<td><?=$row['drug4_name']?></td>
				<td><?=$row['drug5_name']?></td>				
				<td align="center">
					<form action="search" method="POST">
					<input type="hidden" name="drug1_name" value="<?=$row['drug1_name']?>">
					<input type="hidden" name="drug2_name" value="<?=$row['drug2_name']?>">
					<input type="hidden" name="drug3_name" value="<?=$row['drug3_name']?>">
					<input type="hidden" name="drug4_name" value="<?=$row['drug4_name']?>">
					<input type="hidden" name="drug5_name" value="<?=$row['drug5_name']?>">
					<input type="hidden" name="exact" value="1">
					<a href="javascript:document.forms[<?=$form_count?>].submit();" class="button"><img border="0" title="Edit" src="<?=$this->_baseurl?>/public/images/icons/details.gif"/>View<span class="button"></span></a>
					</form>
				</td>				
			</tr>
		<? $form_count++;
			} 
		?>
		</table>
		<br /><br />
		<? } ?>

		<? if($filter=="A" || $filter=="6") {
		
		$sql = "select distinct drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name!='0' and drug6_name!='0' order by drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name asc";
		$rs			=		$this->_db->query($sql)->fetchAll();			
			
			if(count($rs)==0) { 
				echo "<p align=\"center\">no results for six drug combinations.</p>";
			} else { 
		?>
			<table align="center" width="100%" class="display">
			<thead>
			<tr><th colspan="7">Six Drugs</th></tr>
			<tr>
				<th>drug 1</th>
				<th>drug 2</th>
				<th>drug 3</th>
				<th>drug 4</th>
				<th>drug 5</th>
				<th>drug 6</th>
				<th></th>
			</tr>
			</thead>
			<?
			$sql = "select distinct drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name!='0' and drug6_name!='0' order by drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name asc";
			$rs			=		$this->_db->query($sql)->fetchAll();
		
			foreach($rs as $row){
		?>
			<tr>
				<td><?=$row['drug1_name']?></td>
				<td><?=$row['drug2_name']?></td>
				<td><?=$row['drug3_name']?></td>
				<td><?=$row['drug4_name']?></td>
				<td><?=$row['drug5_name']?></td>
				<td><?=$row['drug6_name']?></td>				
				<td align="center">
					<form action="search" method="POST">
					<input type="hidden" name="drug1_name" value="<?=$row['drug1_name']?>">
					<input type="hidden" name="drug2_name" value="<?=$row['drug2_name']?>">
					<input type="hidden" name="drug3_name" value="<?=$row['drug3_name']?>">
					<input type="hidden" name="drug4_name" value="<?=$row['drug4_name']?>">
					<input type="hidden" name="drug5_name" value="<?=$row['drug5_name']?>">
					<input type="hidden" name="drug6_name" value="<?=$row['drug6_name']?>">
					<input type="hidden" name="exact" value="1">
					<a href="javascript:document.forms[<?=$form_count?>].submit();" class="button"><img border="0" title="Edit" src="<?=$this->_baseurl?>/public/images/icons/details.gif"/>View<span class="button"></span></a>
					</form>
				</td>				
			</tr>
		<? $form_count++;
			
			} 
		?>
		</table>
		<br />
		<?
		} 
	   }
	  ?>^S
	  <? die();
	}

	public function arrayToListBox($name,$array,$selected, $extra="") {	
		$sel	=	"<select name=\"$name\" id=\"$name\" ";
		if (IsSet($extra)) {
			 $sel	.=	" $extra>";
		} else { 
			$sel	.=	">";
		}
		
		foreach($array as $key=>$value) {
			if(is_integer($key)) {
				$sel	.=	"<option value=\"$value\"";
			} else {
				$sel	.=	"<option value=\"$key\"";
			}
			if(is_integer($key)) {
				if(trim($value)==trim($selected)) 
				$sel	.=	" selected";
			} else {
				if(trim($key)==trim($selected)) 
					$sel	.=	" selected";
			}
			$sel	.=	">$value</option>\n";
		}
		$sel	.=	"</select>\n";

		return ($sel);
	}

	public function moveup($id,$mid)
	{		
		$n		=	$mid;
		$m		=	$n -1;
		// select row..
		$sql	=	"SELECT drug".$n."_name, drug".$n."_dose, drug".$m."_name, drug".$m."_dose from sddb WHERE id='".$id."'";
		$row	=	$this->_db->fetchRow($sql);		
		
		$tmp	=	"drug".$n."_name";
		$upName =	$row[$tmp];
		$tmp	=	"drug".$n."_dose";
		$upDose =	$row[$tmp];
		
		$tmp	=	"drug".$m."_name";		
		$dnName =	$row[$tmp];
		$tmp	=	"drug".$m."_dose";
		$dnDose =	$row[$tmp];
		
		$sql = "UPDATE sddb SET " 
			. "drug".$n."_name = '".$dnName."', "
			. "drug".$n."_dose = '".$dnDose."', "
			. "drug".$m."_name = '".$upName."', "
			. "drug".$m."_dose = '".$upDose."' "
			. "WHERE id='".$id."'";
		$this->_db->query($sql);
	}

	public function getDiluents()
    {
        $sql = "SELECT DISTINCT(diluent) FROM sddb ORDER BY diluent";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function archive($data)
    {		
		$audit	=	new Application_Model_Audits();	

		$ids	=	explode(",",substr($data['ids'],0,-1));

		for($i=0;$i<count($ids);$i++){

			$id	=	$ids[$i];
			
			if($data['process']=='Archive'){
				$this->_db->query("INSERT INTO sddb_archive (id, unit_code, unit_ref, drug1_name, drug1_dose	,drug2_name,drug2_dose,drug3_name,drug3_dose,drug4_name,drug4_dose,drug5_name,drug5_dose,drug6_name,drug6_dose,volume,diluent,duration,duration_other,compatibility,datatype,site_reaction,comments,show_details_link,support_data,added_by, added_date,approved) SELECT id, unit_code, unit_ref, drug1_name, drug1_dose	,drug2_name,drug2_dose,drug3_name,drug3_dose,drug4_name,drug4_dose,drug5_name,drug5_dose,drug6_name,drug6_dose,volume,diluent,duration,duration_other,compatibility,datatype,site_reaction,comments,show_details_link,support_data,added_by, added_date,approved FROM sddb WHERE id = '".$id."'");			

				$audit->insert_audit('sddb','archive',$id,'success');
	
				$this->_db->delete("sddb",'id='.$id); 
			}elseif($data['process']=='Restore'){
				$this->_db->query("INSERT INTO sddb (id, unit_code, unit_ref, drug1_name, drug1_dose	,drug2_name,drug2_dose,drug3_name,drug3_dose,drug4_name,drug4_dose,drug5_name,drug5_dose,drug6_name,drug6_dose,volume,diluent,duration,duration_other,compatibility,datatype,site_reaction,comments,show_details_link,support_data,added_by, added_date,approved) SELECT id, unit_code, unit_ref, drug1_name, drug1_dose	,drug2_name,drug2_dose,drug3_name,drug3_dose,drug4_name,drug4_dose,drug5_name,drug5_dose,drug6_name,drug6_dose,volume,diluent,duration,duration_other,compatibility,datatype,site_reaction,comments,show_details_link,support_data,added_by, added_date,approved FROM sddb_archive WHERE id = '".$id."'");		

				$audit->insert_audit('sddb','restore',$id,'success');
	
				$this->_db->delete("sddb_archive",'id='.$id); 
				
			}
		}
        die();
    }

	public function archivelistings($data)
	{

			
		$sql	=	"SELECT * FROM sddb_archive WHERE 1 ORDER BY unit_code, unit_ref";	
		
		//echo($sql);
		$rs			=	$this->_db->query($sql)->fetchAll();		
		$list		=	'
	<table width="100%" class="display" id="sddb">
		<thead>
		<tr>
			<th rowspan="2">Centre code / Record num</th>
			<th rowspan="2">Drug</th>
			<th rowspan="2">Dose in syringe (mg)</th>
			<th rowspan="2">Final volume in syringe (ml)</th>
			<th rowspan="2">Concentration (mg/ml)</th>
			<th rowspan="2">Diluent</th>
			<th colspan="3">Compatibility information</th>
			<th rowspan="2">Site reaction reported</th>
			<th rowspan="2">User Name</th>			
		</tr>
		<tr>
			<th>Outcome</th>
			<th>Duration</th>
			<th>Data type</th>				
		</tr>
		</thead>';

		if(count($rs)==0) {
			$list.= "<tr>
					<td colspan='11'>No matching entries found.</td>
				  </tr>";
		} else {		
			$k=1;
			foreach($rs as $row) {
				//need to know how many drugs in this combo for rowspanning				
				if($row['drug6_name']) {
					$numdrugs=6;
				} elseif($row['drug5_name']) {
					$numdrugs=5;
				} elseif($row['drug4_name']) {
					$numdrugs=4;
				} elseif($row['drug3_name']) {
					$numdrugs=3;
				} else {
					$numdrugs=2;
				}	
				if($row['duration']=='other') {
					$duration	=	$row['duration_other'];
				} else {
					$duration	=	$row['duration'];
				}

				$username = $this->_db->fetchOne("SELECT username FROM new_users WHERE id='".$row['added_by']."'");							
				if(!empty($username))
					$addedby	=	$username;
				else
					$addedby	=	"Unknown";

				$drug_cons		=	array();
				$drug_name		=	array();
				$drug_dose		=	array();
					
				for($i=1;$i<=$numdrugs;$i++){
					$drug_cons[$i]				=	@number_format($row['drug'.$i.'_dose'] / $row['volume'],2);
					$drug_name[$drug_cons[$i]]	=	$row['drug'.$i.'_name'];
					$drug_dose[$drug_cons[$i]]	=	$row['drug'.$i.'_dose'];
				}
				sort($drug_cons);
				ksort($drug_name);
				ksort($drug_dose);			

				if($k%2==0)
					$class	=	' class="sddb"';
				else
					$class	=	'';
				$list.= '<tr'.$class.'>
					<td rowspan="'.$numdrugs.'"><input type="checkbox" autocomplete="OFF" value="'.$row['id'].'" name="check_'.$row['id'].'" id="check_'.$row['id'].'">'.$row['unit_code'].' / '.$row['unit_ref'].'</td>
					<td>'.$drug_name[$drug_cons['0']].'</td>
					<td align="center">'.$drug_dose[$drug_cons['0']].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['volume'].'</td>
					<td align="center">'.$drug_cons['0'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['diluent'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['compatibility'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$duration.'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$row['datatype'].'</td>				
					<td align="center" rowspan="'.$numdrugs.'">'.$row['site_reaction'].'</td>
					<td align="center" rowspan="'.$numdrugs.'">'.$addedby.'</td>					
				</tr>
				<tr>
					<td>'.$drug_name[$drug_cons['1']].'</td>
					<td align="center">'.$drug_dose[$drug_cons['1']].'</td>
					<td align="center">'.$drug_cons['1'].'</td>
				</tr>';
				
				if($row['drug3_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['2']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['2']].'</td>
								<td align="center">'.$drug_cons['2'].'</td>
							</tr>';
				}
				
				if($row['drug4_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['3']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['3']].'</td>
								<td align="center">'.$drug_cons['3'].'</td>
							</tr>';
				} 			
				
				if($row['drug5_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['4']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['4']].'</td>
								<td align="center">'.$drug_cons['4'].'</td>
							</tr>';
				 } 			
				
				if($row['drug6_name']) { 
					$list.=	'<tr>
								<td>'.$drug_name[$drug_cons['5']].'</td>
								<td align="center">'.$drug_dose[$drug_cons['5']].'</td>
								<td align="center">'.$drug_cons['5'].'</td>
							</tr>';
				}			
				
				
				$k++;
				
			} //foreach			
		}
		$list.=	'</table>^S';
		die($list);		
	}
	
}

?>