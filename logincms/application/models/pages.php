<?php

class Application_Model_Pages
{
	private $_db;    

    public function __construct()
    {
        $this->_db		= Zend_Registry::get("db");
		$this->_baseurl	= Zend_Registry::get("baseUrl");
    }  
	
	public function cat_list($parentid,$i,$type)
	{
		if($type != 'Other Nav'){
			$res			=	$this->_db->fetchAll("SELECT * FROM 3bit_categories WHERE cat_parent_id='".$parentid."' AND cat_type ='".$type."' ORDER BY cat_order");
		}else if($type =='Other Nav'){
			$res			=	$this->_db->fetchAll("SELECT * FROM 3bit_categories WHERE cat_type ='".$type."' ORDER BY cat_order");
		}
		
		$i					=	$i+1;
		$print_row			=	'';
		$k					=	0;
		foreach($res as $cat_values){			
			$catid			=	$cat_values['cat_id'];
			$catname		=	html_entity_decode(stripslashes($cat_values['cat_title']));
			$cat_level		=	$cat_values['cat_level'];
			$cat_order		=	$cat_values['cat_order'];			
			$cat_active		=	$cat_values['status'];
			$catalias		=	$cat_values['cat_alias'];

			$urlpath		=	$catalias;
			if($cat_values['ext_url']<>"")
				$urlpath	=	 $cat_values['ext_url'];
			
			$article_id		=	$cat_values['article_id'];
			$article_temp	=	$cat_values['temp_id'];
			$template_name	=	"&nbsp;";
			$article_link	=	"";

			if ($article_id != '0'){
				$article_link	=	"<a class=\"button\" href=\"".$this->_baseurl."/articles/add/id/$article_id\"><img border=\"0\" alt=\"View Article\" title=\"View Article\" src=\"".$this->_baseurl."/public/images/icons/page_white_text.png\" >Article</a>";								
			}
			if($article_temp !='0'){
				$template_name		=	$this->_db->fetchOne("select temp_filename from 3bit_article_template where temp_id = '".$article_temp."'");	
			}

			$numrows_children	= $this->_db->fetchOne("SELECT count(cat_id) FROM 3bit_categories WHERE cat_parent_id='".$catid."'");
			

			$lang_code			= $this->_db->fetchOne("SELECT language_code FROM 3bit_language WHERE language_id='".$cat_values['language_id']."'");

			if ($cat_active=="1"){
				$active	=	"<span id='".$catid."3bit_categories'><a href=\"javascript:;\" onclick=\"change_status(".$catid.",'3bit_categories','status','0','cat_id');\"><font color='#006600'>Active</font></a></span>";
				$delstyle	=	"style='display:none'";
			}else{
				$active	=	"<span id='".$catid."3bit_categories'><a href=\"javascript:;\" onclick=\"change_status(".$catid.",'3bit_categories','status','1','cat_id');\"><font color='#FF0000'>InActive</font></a></span>";								
				$delstyle	=	'';
			}
		
			if ($cat_level =='1'){
				$class ="#BDB6A5";
				$tab ='';				
			}else if ($cat_level =='2'){
				$class ="#E7E3DE";
				$tab ='----';
			}else if ($cat_level =='3'){
				$class ="#EFEFDE";
				$tab ='-----';
			}else if ($cat_level =='4'){
				$class ="#FFFBFF";
				$tab ='------';
			}else if ($cat_level =='5'){
				$class ="menu5css";
				$tab ='-------';
			}
			

			
			$delete_link ="<span id = '".$catid."' $delstyle><a class=\"button\" onclick=\"return open_dialog('".$this->_baseurl."/pages/delete/id/$cat_values[cat_id]')\"><img border=\"0\" alt=\"Delete\" src=\"".$this->_baseurl."/public/images/icons/cross.png\">Delete</a></span>"; 			
			
			$print_row .="<tr>";
			$print_row .="<td><input type='checkbox' id='chkDel' name='chkDel[]' style='height:15px' value='$catid'></td>		
							<td>&nbsp;$tab $catname </td>
							<td>&nbsp;$lang_code </td>
							<td>&nbsp;$cat_level</td>
							<td>&nbsp;$urlpath</td>
							<td>$template_name</td> 	
							<td>&nbsp;$active</td>
							<td><a class=\"button\" href=\"".$this->_baseurl."/pages/add/id/$cat_values[cat_id]\"><img alt=\"Edit\" border=\"0\" src=\"".$this->_baseurl."/public/images/icons/page_white_edit.png\">Edit</a>&nbsp;
							$article_link
							$delete_link 														
							</td>
						</tr>";	  
			if($type !='Other Nav')
				$print_row .= $this->cat_list($catid,$i,$type);					

			$k++;

	}// while
		
	return $print_row; 

	}
	
    public function getPagelist($passdir,$i,$selected)
    {
        $i.="---";
			
		$sublib_qry  = $this->_db->fetchAll("SELECT cat_id,cat_parent_id,cat_title FROM `3bit_categories` WHERE cat_parent_id='$passdir'");
			
		$second = '';
			
		if ($sublib_qry){ 			
			foreach($sublib_qry as $sublib_values)	{				
				$sublib_id					= $sublib_values["cat_id"];			
				$subparent_id				= $sublib_values["cat_parent_id"];				
				$sublib_name				= $sublib_values["cat_title"];	  				
				if ($selected == $sublib_id){								  						
					$sel ="selected";										  					
				}else{														  				
					$sel ="";												  					
				}															  				
				$second.= "<option value='$sublib_id' $sel>$i$sublib_name</option>";									
					
				$second.= $this->getPagelist($sublib_id,$i,$selected);//recursive call to the function				
			}
			return $second;
		}else{			   		
			return $second;
		}
    }

	public function findcatlevel($parent_id)
	{

		$sublib_qry  = 	$this->_db->fetchRow("SELECT cat_id,cat_level from `3bit_categories` where cat_id='$parent_id'");
		
		if (!$sublib_qry){	
			$level	= 1;
		}else{
			$sublib_id	 = $sublib_qry['cat_id'];

			$cat_level	 = $sublib_qry['cat_level'];

			$level		 = $cat_level + 1;
		}
		return $level; 
	}

	public function findcatorder($cat_level)
	{

		$sublib_qry  = $this->_db->fetchRow("SELECT max(cat_order) as max_order from `3bit_categories` where cat_level='$cat_level'");				
		if (!$sublib_qry){	
			$new_order	 = 1;
		}else{
			$new_order	 = $sublib_qry['max_order'] + 1;
		}
		return $new_order;

	}

	// Function to create page for the template //

	public function createpage($tempid,$catid,$cat_file)
	{	

		$besName		=	$this->_db->fetchOne("SELECT temp_filename FROM 3bit_article_template WHERE temp_id ='".$tempid."'");		
					
		if(!empty($besName) && strtolower($besName)<>'bespoke'){
		
			$article_write_path		=	$cat_file;

			$bespoke_query			=	$this->_db->fetchAll("SELECT * FROM `3bit_article_template` WHERE custom_template='1'");					

			if($bespoke_query){

				$arrBespokeID		=	array();

				foreach($bespoke_query as $resqry){
				
					$arrBespokeID[]		=	$resqry["temp_id"];
				
				}

			}
			
			if(!empty($arrBespokeID) && count($arrBespokeID) > 0){ 			
				
				if(in_array($tempid,$arrBespokeID))
						$check2Var		=	'1';
				else
						$check2Var		=	'0';
			}else{
				$check2Var		=	'0';
			}		
					
			if($check2Var	==	0){
				
				if(	$catid	<>	0){
					$awp		= dirname($article_write_path);
					$docroot	= $_SERVER['DOCUMENT_ROOT'];
					if($awp<>".")
						$dirpath			= $docroot.$awp;
					else
						$dirpath			= $docroot;
				}			
				
				if ($dirpath<>""){					
					if (!is_dir($dirpath)) {
						mkdir ($dirpath,0777);
					}				
				} 			

				if($tempid<>'0'){
				
					$file2Write		=	$dirpath."/".basename($article_write_path);
					
					$getFile		=	$this->_db->fetchRow("SELECT temp_content FROM `3bit_article_template` WHERE temp_id ='".$tempid."' ");					
					
					if($getFile){						
						
						$toWriteContent	=	stripslashes($getFile["temp_content"]);

						$parsedContent  =   $this->parsecontent($toWriteContent);		

						if($parsedContent!="false")
							$toWriteContent	=	$parsedContent;	
						
						$getFile		=	fopen($file2Write,"w+");					
						
						fwrite($getFile,$toWriteContent);
					}
				}
			}
				
		}
		
	}


	// parse the Snippet tags

	public function parsecontent($content)
	{
		
		$matchCount	=	preg_match_all('/(\[\[*.+\]\])/', $content, $matches);

		if($matchCount==0)
			return "false";
		
		foreach($matches[0] as $value){ 		
			$value1=preg_replace("/\[\[/","/\[\[",$value);
			$value1 =preg_replace("/\]\]/","\]\]/",$value1);
			$snippets[$value1] = $this->getSnippet($value);
		}
		$parsedContent	=	preg_replace(array_keys($snippets),array_values($snippets),$content);
		
		return $parsedContent;
	}

	// Get Snippet text by snippet name

	public function getSnippet($name)
	{
		$name =str_replace("[[","",$name);
		$name =str_replace("]]","",$name);
		$sql		=	$this->_db->fetchRow("SELECT long_text FROM 3bit_snippets WHERE snippet_title='".$name."' ");								
		if($sql){		
			$text	=	$sql['long_text'];
			return stripslashes($text);
		}	
	}


	public function read($id)
    {
        $sql = "SELECT *,DATE_FORMAT(start_date,'%Y-%m-%d')as start_date,DATE_FORMAT(end_date,'%Y-%m-%d')as end_date,DATE_FORMAT(start_date,'%h')as shour,DATE_FORMAT(start_date,'%i')as smin,DATE_FORMAT(start_date,'%p')as sform,DATE_FORMAT(end_date,'%h')as ehour,DATE_FORMAT(end_date,'%i')as emin,DATE_FORMAT(end_date,'%p')as eform FROM `3bit_categories` WHERE cat_id ='".$id."'";
        if ($result = $this->_db->query($sql)) {
			$data				=	$result->fetchAll();
			$temp_id			= 	$data[0]['temp_id'];
			$article_id			= 	$data[0]['article_id'];
			$temp_filename		=	'';
			$article_title		=	'';		  
			if($temp_id<>"" && $temp_id <>'0'){
				$imgsql				=	$this->_db->fetchRow('SELECT temp_filename FROM 3bit_article_template WHERE temp_id="'.$temp_id.'"');
				if($imgsql)
					$temp_filename	=	$imgsql['temp_filename'];
			}
			if($article_id<>"" && $article_id <>'0'){
				$imgsql2			=	$this->_db->fetchRow('SELECT article_title FROM 3bit_articles WHERE article_id="'.$article_id.'"');	
				if($imgsql2)
					$article_title	=	$imgsql2['article_title'];				
			}
			$data[0]['temp_filename']=	$temp_filename;
			$data[0]['article_title']=	$article_title;
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_categories','view',$id,'success');
            return $data;
        }
    }

	public function add($data)
    {	
		$audit	=	new Application_Model_Audits();

		$sdate				=   $data["start_date"];		
		$edate				=	$data["end_date"];

		if($data["selSHour"]	!=	"00" && strlen($data["selSHour"])==1){
			$hour			= 	"0".$data["selSHour"];
		}else{
			$hour			= 	$data["selSHour"];
		}
	
		if($data["selSMin"]		!=	"00" && strlen($data["selSMin"])==1){
			$min			= "0".$data["selSMin"];
		}else{
			$min			= $data["selSMin"];
		}

		if($data["selEHour"]	!=	"00" && strlen($data["selEHour"])==1){
			$ehour			= 	"0".$data["selEHour"];
		}else{
			$ehour			= 	$data["selEHour"];
		}
		
		if($data["selEMin"]		!=	"00" && strlen($data["selEMin"])==1){
			$emin			= "0".$data["selEMin"];
		}else{
			$emin			= $data["selEMin"];
		}

		if($data["selSFormat"]=="pm")
			$hour	=	12+$hour;
		if($data["selEFormat"]=="pm")
			$ehour	=	12+$ehour;
	
		$start_time			=   $hour.":".$min.":00";

		$end_time			=   $ehour.":".$emin.":00";

		$start_date			=	$sdate." ".$start_time;

		$end_date			=	$edate." ".$end_time;

		$parent_id			=	$data['cat_parent_id'];

		$cat_level			=	$this->findcatlevel($parent_id);

		$cat_order			=	$this->findcatorder($cat_level);

		$chkflP 			= 	strstr($data["cat_url"], '.') ? basename($data["cat_url"]) : '';

		$page_url			=	$data['cat_url'];  
		$path_info			=	parse_url($page_url);	
		
		if(!empty($path_info) && !empty($path_info['host'])){			
			$iext_url	=	$page_url;	 			
		}else{
			$iext_url	=	'';
		}	

		if($iext_url==''){
			$chkBes				=	$this->_db->fetchRow("SELECT temp_filename FROM 3bit_article_template WHERE temp_id ='".$data['temp_id']."'");
			if($chkBes){				
				$besName	=	$chkBes['temp_filename'];
			}
		
			if(strtolower($besName)<>'bespoke'){ 	

				if(strtolower($data['cat_title'])=='home'){ 		
				   $addMe2DB		=	"/index";
				   $fileName2Create	=	"/index.php";
				   $aliasName4Front	=	str_replace(".php",".html",$fileName2Create);
				}else{	
				  if($chkflP	==	''){				
					$exp			=	explode("/",$data["cat_url"]);
					$count			=	count($exp);
					if($count=='3'){			
						if($exp[2]==''){
							$fileName2Create	=	$data["cat_url"].$data["cat_title"].".php";
							$aliasName4Front	=	$data["cat_url"].$data["cat_title"].".html";
						}else{
							$fileName2Create	=	$data["cat_url"].".php";
							$aliasName4Front	=	$data["cat_url"].".html";
						}
					}else if($count	==	2){
						 $fileName2Create	=	$data["cat_url"].".php";
						 $aliasName4Front	=	$data["cat_url"].".html";
					}		
					$addMe2DB		=	$data["cat_url"];
					$string			=	array("///","//");
					$addMe2DB		=	str_replace($string,"/",$addMe2DB);			
				  }else{		
					$addMe2DB		=	$data["cat_url"];
					$addMe2DB		=	str_replace("//","/",$addMe2DB);
					$fileName2Create=	$data["cat_url"];
					$aliasName4Front=	str_replace(".php",".html",$data["cat_url"]);
				  }
				}
			}else{
			   if($chkflP	==	''){
				$addMe2DB		=	$data["cat_url"];
				$addMe2DB		=	str_replace("//","/",$addMe2DB);
				$fileName2Create=	$data["cat_url"].".php";
				$aliasName4Front=	str_replace(".php",".html",$fileName2Create);
			  }else{
				$addMe2DB		=	$data["cat_url"];
				$addMe2DB		=	str_replace("//","/",$addMe2DB);
				$fileName2Create=	$data["cat_url"];
				$aliasName4Front=	str_replace(".php",".html",$fileName2Create);
			  }
			}			
		}else{
			$fileName2Create=	'';
			$aliasName4Front=	'';
		}

		if($iext_url=='')
			$ipage_url	=	$addMe2DB;
		else
			$ipage_url	=	'';	
	

		$sql	=	"INSERT INTO 3bit_categories SET
							language_id		=	'".$data['language_id']."',
							cat_title		=	'".addslashes($data['cat_title'])."',
							menu_title		=	'".addslashes($data['menu_title'])."',
							cat_type		=	'".addslashes($data['cat_type'])."',
							cat_parent_id	=	'".$data['cat_parent_id']."',
							cat_order		=	'".$cat_order."',
							cat_url			=	'".addslashes($ipage_url)."',
							cat_level		=	'".$cat_level."',												
							temp_id			=	'".$data['temp_id']."',
							article_id		=	'".$data['article_id']."',
							ext_url			=	'".addslashes($iext_url)."',
							cat_file		=	'".addslashes($fileName2Create)."',
							cat_alias		=	'".addslashes($aliasName4Front)."',
							menu_item		=	'".$data['menu_item']."',
							start_date		=	DATE_FORMAT('".$start_date."','%Y-%m-%d %k:%i:%S'),
							end_date		=	DATE_FORMAT('".$end_date."','%Y-%m-%d %k:%i:%S'),
							login_required  =   '".$data['login_required']."',
							css_class		=	'".addslashes($data['css_class'])."',												
							cat_schedule	=	'".$data['cat_schedule']."',
							UserType		=	'".$data['type_id']."',
							status			=	'".$data['status']."',												
							created_date	=	now(),
							modified_date	=	now()";	

		$query			=	$this->_db->getConnection()->query($sql);

		$category_id	=	$this->_db->lastInsertId();	

		if(!empty($data['article_title']) && empty($data['article_id'])){
			if(!empty($_SESSION['wlsession']['user_id']))
				$logged_user = $_SESSION['wlsession']['user_id'];
			else
				$logged_user = '';

			$query	=	$this->_db->getConnection()->query("INSERT INTO 3bit_articles SET
								article_title			=	'".addslashes($data['article_title'])."',
								owner					=	'".addslashes($data['owner'] )."',
								user_id					=	'".addslashes($logged_user)."',
								short_text				=	'".addslashes($data['short_text'])."',
								long_text				=	'".addslashes($data['long_text'])."',
								header_image			=	'".addslashes($data['header_imageid'])."',
								article_image			=	'".addslashes($data['article_imageid'])."',
								image_text				=	'".addslashes($data['image_text'])."',
								meta_title				=	'".addslashes($data['meta_title'])."',
								meta_desc				=	'".addslashes($data['meta_desc'])."',
								meta_keywords			=	'".addslashes($data['meta_keywords'])."',
								status					=	'".$data['active']."',
								created_date			=	now(),
								modified_date			=	now()");

			$articleid	=	$this->_db->lastInsertId();

			$query	=	$this->_db->query("UPDATE 3bit_categories SET article_id	= '".$articleid."' WHERE cat_id='".$category_id."'");

			$audit->insert_audit('3bit_articles','add',$articleid,'success');

		}

		if($iext_url=="")
			$this->createpage($data['temp_id'],$category_id,$fileName2Create);		

		$audit->insert_audit('3bit_categories','add',$category_id,'success');
    }

	public function update($data)
	{

		$audit	=	new Application_Model_Audits();

		$sdate				=   $data["start_date"];		
		$edate				=	$data["end_date"];

		if($data["selSHour"]	!=	"00" && strlen($data["selSHour"])==1){
			$hour			= 	"0".$data["selSHour"];
		}else{
			$hour			= 	$data["selSHour"];
		}
	
		if($data["selSMin"]		!=	"00" && strlen($data["selSMin"])==1){
			$min			= "0".$data["selSMin"];
		}else{
			$min			= $data["selSMin"];
		}

		if($data["selEHour"]	!=	"00" && strlen($data["selEHour"])==1){
			$ehour			= 	"0".$data["selEHour"];
		}else{
			$ehour			= 	$data["selEHour"];
		}
		
		if($data["selEMin"]		!=	"00" && strlen($data["selEMin"])==1){
			$emin			= "0".$data["selEMin"];
		}else{
			$emin			= $data["selEMin"];
		}

		if($data["selSFormat"]=="pm")
			$hour	=	12+$hour;
		if($data["selEFormat"]=="pm")
			$ehour	=	12+$ehour;
	
		$start_time			=   $hour.":".$min.":00";

		$end_time			=   $ehour.":".$emin.":00";

		$start_date			=	$sdate." ".$start_time;

		$end_date			=	$edate." ".$end_time;

		$parent_id			=	$data['cat_parent_id'];

		$cat_level			=	$this->findcatlevel($parent_id);

		$cat_order			=	$this->findcatorder($cat_level);

		$chkflP 			= 	strstr($data["cat_url"], '.') ? basename($data["cat_url"]) : '';

		$page_url	=	$data['cat_url'];  
		$path_info	=	parse_url($page_url);					 
		if(!empty($path_info['scheme']) && !empty($path_info['host'])){
			$iext_url	=	$page_url;			
		}else{
			$iext_url	=	'';	 		
		}

		if($iext_url==''){
			
			$chkBes				=	$this->_db->fetchRow("SELECT temp_filename FROM 3bit_article_template WHERE temp_id ='".$data['temp_id']."'");
			if($chkBes){			
				$besName		=	$chkBes['temp_filename'];
			}
		
			if(strtolower($besName)<>'bespoke'){ 	

				if(strtolower($data['cat_title'])=='home'){ 		
					$addMe2DB		=	"/index";
					$fileName2Create	=	"/index.php";
					$aliasName4Front	=	str_replace(".php",".html",$fileName2Create);
				}else{	
					if($chkflP	==	''){				
						$exp			=	explode("/",$data["cat_url"]);
						$count			=	count($exp);
						if($count=='3'){			
							if($exp[2]==''){
								$fileName2Create	=	$data["cat_url"].$data["cat_title"].".php";
								$aliasName4Front	=	$data["cat_url"].$data["cat_title"].".html";
							}else{
								$fileName2Create	=	$data["cat_url"].".php";
								$aliasName4Front	=	$data["cat_url"].".html";
							}
						}else if($count	==	2){
							 $fileName2Create	=	$data["cat_url"].".php";
							 $aliasName4Front	=	$data["cat_url"].".html";
						}		
						$addMe2DB		=	$data["cat_url"];
						$string			=	array("///","//");
						$addMe2DB		=	str_replace($string,"/",$addMe2DB);			
				  }else{		
					$addMe2DB		=	$data["cat_url"];
					$addMe2DB		=	str_replace("//","/",$addMe2DB);
					$fileName2Create=	$data["cat_url"];
					$aliasName4Front=	str_replace(".php",".html",$data["cat_url"]);
				  }
				}
			}else{
			   if($chkflP	==	''){
				$addMe2DB		=	$data["cat_url"];
				$addMe2DB		=	str_replace("//","/",$addMe2DB);
				$fileName2Create=	$data["cat_url"].".php";
				$aliasName4Front=	str_replace(".php",".html",$fileName2Create);
			  }else{
				$addMe2DB		=	$data["cat_url"];
				$addMe2DB		=	str_replace("//","/",$addMe2DB);
				$fileName2Create=	$data["cat_url"];
				$aliasName4Front=	str_replace(".php",".html",$fileName2Create);
			  }
			}
		}else{
			$fileName2Create	=	'';
			$aliasName4Front	=	'';
		}

		if($iext_url=='')
			$ipage_url	=	$addMe2DB;
		else
			$ipage_url	=	'';	
		
		$query	=	$this->_db->getConnection()->query("UPDATE 3bit_categories SET
												language_id		=	'".$data['language_id']."',
												cat_title		=	'".addslashes($data['cat_title'])."',
												menu_title		=	'".addslashes($data['menu_title'])."',
												cat_type		=	'".addslashes($data['cat_type'])."',
												cat_parent_id	=	'".$data['cat_parent_id']."',												
												cat_url			=	'".addslashes($ipage_url)."',
												cat_level		=	'".$cat_level."',											
												temp_id			=	'".$data['temp_id']."',
												article_id		=	'".$data['article_id']."',
												ext_url			=	'".addslashes($iext_url)."',
												cat_file		=	'".addslashes($fileName2Create)."',
												cat_alias		=	'".addslashes($aliasName4Front)."',
												menu_item		=	'".$data['menu_item']."',
												start_date		=	DATE_FORMAT('".$start_date."','%Y-%m-%d %k:%i:%S'),
												end_date		=	DATE_FORMAT('".$end_date."','%Y-%m-%d %k:%i:%S'),
												login_required  =   '".$data['login_required']."',
												css_class		=	'".addslashes($data['css_class'])."',											
												cat_schedule	=	'".$data['cat_schedule']."',
												UserType			=	'".$data['type_id']."',
												status			=	'".$data['status']."',																			
												modified_date	=	now() WHERE cat_id='".$data['id']."'");

		$category_id		=	$data['id'];		
		
		if(!empty($data['article_title']) && empty($data['article_id'])){
			if(!empty($_SESSION['wlsession']['user_id']))
				$logged_user = $_SESSION['wlsession']['user_id'];
			else
				$logged_user = '';

			$query	=	$this->_db->getConnection()->query("INSERT INTO 3bit_articles SET
								article_title			=	'".addslashes($data['article_title'])."',
								owner					=	'".addslashes($data['owner'] )."',
								user_id					=	'".addslashes($logged_user)."',
								short_text				=	'".addslashes($data['short_text'])."',
								long_text				=	'".addslashes($data['long_text'])."',
								header_image			=	'".addslashes($data['header_imageid'])."',
								article_image			=	'".addslashes($data['article_imageid'])."',
								image_text				=	'".addslashes($data['image_text'])."',
								meta_title				=	'".addslashes($data['meta_title'])."',
								meta_desc				=	'".addslashes($data['meta_desc'])."',
								meta_keywords			=	'".addslashes($data['meta_keywords'])."',
								status					=	'".$data['active']."',
								created_date			=	now(),
								modified_date			=	now()");

			$articleid	=	$this->_db->lastInsertId();

			$query	=	$this->_db->query("UPDATE 3bit_categories SET article_id	= '".$articleid."' WHERE cat_id='".$data['id']."'");

			$audit->insert_audit('3bit_articles','add',$articleid,'success');

		}

		if($iext_url=="")
			$this->createpage($data['temp_id'],$category_id,$fileName2Create);

		
		$audit->insert_audit('3bit_categories','update',$data['id'],'success');		
    }

	public function delete($id){

		$this->_db->delete("3bit_categories",'cat_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_categories','delete',$id,'success');
	}

	public function getLanguages()
    {
        $sql = "SELECT * FROM 3bit_language";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	 public function getUsertypes()
    {
        $sql = "SELECT DISTINCT(UserType) FROM new_users";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }
}

?>