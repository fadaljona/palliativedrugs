<?php defined('SYSPATH') or die('No direct script access.');

class Country_Model extends Model {

	public function browse($limit, $offset, $sortname, $sortorder, $conditions)
    {
		
        $result	= $this->db->select('*')->from('country')->orderby(array($sortname => $sortorder));	
		
		$result	=	$result->limit($limit, $offset)->get();

		return $result;

		
    }

	 public function count_country($conditions)
    { 
        $query = $this->db->select('id')->from('country');
		
		$query	=	$query->get();

        return $query->count();

    }


    public function add($data){	

		$query	=	$this->db->query("INSERT INTO country SET
								zone_id				=	'".addslashes($data['zone_id'])."',
								iso_code			=	'".addslashes($data['iso_code'] )."',
								country				=	'".addslashes($data['country'])."'");
		$cid		 =	$query->insert_id(); 
		$this->audit = new Audits_Model; 
		$this->audit->insert_audit('country','add',$cid,'success');

    }

	public function read($id){
		  $this->audit = new Audits_Model; 
		  $this->audit->insert_audit('country','view',$id,'success');
	      return $this->db->select()->where('id',$id)->from('country')->get()->result_array();
	}

	public function update($data){	

		$this->db->query("UPDATE country SET
								zone_id				=	'".addslashes($data['zone_id'])."',
								iso_code			=	'".addslashes($data['iso_code'] )."',
								country				=	'".addslashes($data['country'])."' WHERE id='".$data['id']."'");
		$this->audit = new Audits_Model; 
		$this->audit->insert_audit('country','update',$data['id'],'success');
    }

	public function delete($id){																								  
		$this->db->delete("country", array('id' => $id));
		$this->audit = new Audits_Model; 
		$this->audit->insert_audit('country','delete',$id,'success');
	}
}
?>