<?php

class Application_Model_Datasources
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_datasource WHERE datasource_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_datasource','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_datasource SET
								title				=	'".addslashes($data['title'])."',
								content				=	'".addslashes($data['content'])."',
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now()";

		$query	=	$this->_db->query($sql);

		$typeid	=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_datasource','add',$typeid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_datasource SET
								title				=	'".addslashes($data['title'])."',
								content				=	'".addslashes($data['content'])."',
								status				=	'".$data['status']."',								
								modified_date		=	now() WHERE datasource_id='".$data['id']."'";
		$query	=	$this->_db->query($sql);

		$typeid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_datasource','update',$typeid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_datasource",'datasource_id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_datasource','delete',$id,'success');
	}
}

?>