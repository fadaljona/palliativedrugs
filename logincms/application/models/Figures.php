<?php 

class Application_Model_Figures
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }        

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_formulary_snippets WHERE snippet_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_formulary_snippets','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function findsnippetversion($snippetid)
	{

		$sublib_qry  = $this->_db->fetchAll("SELECT max(`version`) as max_version from `3bit_formulary_snippet_versions` where snippet_id='$snippetid'");		
		
		$numrows	 = count($sublib_qry);
		if($numrows == 0){	
			$new_order	 = 0.01;
		}else{
			$new_order	 = $sublib_qry[0]['max_version'] + 0.01;
		}

		return $new_order;

	}

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_formulary_snippets SET
								language_id			=	'".addslashes($data['language_id'])."',
								snippet_title		=	'".addslashes($data['snippet_title'])."',
								snippet_number		=	'".addslashes($data['snippet_number'])."',
								snippet_type		=	'".addslashes($data['snippet_type'])."',
								long_text			=	'".addslashes($data['long_text'])."',
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now()";

		$query		=	$this->_db->query($sql);

		$snippetid	=	$this->_db->lastInsertId();	

		$newversion	=	$this->findsnippetversion($snippetid);

		$query1		=	$this->_db->query("INSERT INTO 3bit_formulary_snippet_versions SET
								snippet_id			=	'".$snippetid."',
								language_id			=	'".addslashes($data['language_id'])."',
								snippet_title		=	'".addslashes($data['snippet_title'])."',
								snippet_number		=	'".addslashes($data['snippet_number'])."',
								snippet_type		=	'".addslashes($data['snippet_type'])."',
								long_text			=	'".addslashes($data['long_text'])."',
								version				=	'".$newversion."',
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now()");

		$this->_db->query("UPDATE 3bit_formulary_snippets SET version='".$newversion."' WHERE snippet_id='".$snippetid."'");
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_formulary_snippets','add',$snippetid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_formulary_snippets SET
								language_id			=	'".addslashes($data['language_id'])."',
								snippet_title		=	'".addslashes($data['snippet_title'])."',
								snippet_number		=	'".addslashes($data['snippet_number'])."',
								snippet_type		=	'".addslashes($data['snippet_type'])."',
								long_text			=	'".addslashes($data['long_text'])."',
								status				=	'".$data['status']."',						
								modified_date		=	now() WHERE snippet_id='".$data['id']."'";							
		
		$query		=	$this->_db->query($sql);
		
		$snippetid	=	$data['id'];

		$newversion	=	$this->findsnippetversion($snippetid);

		$selLatest	=  $this->_db->fetchAll("SELECT long_text,snippet_id FROM 3bit_formulary_snippet_versions WHERE snippet_id= '".$snippetid."' ORDER BY version DESC LIMIT 0,1");
		if(count($selLatest)>0){
			$existing_text	=	stripslashes($selLatest[0]['long_text']);
			if(trim($existing_text)!=trim($data['long_text'])){
			   $query1		=	$this->_db->query("INSERT INTO 3bit_formulary_snippet_versions SET
									snippet_id			=	'".$snippetid."',
									language_id			=	'".addslashes($data['language_id'])."',
									snippet_title		=	'".addslashes($data['snippet_title'])."',
									snippet_number		=	'".addslashes($data['snippet_number'])."',
									snippet_type		=	'".addslashes($data['snippet_type'])."',
									long_text			=	'".addslashes($data['long_text'])."',
									version				=	'".$newversion."',
									status				=	'".$data['status']."',
									created_date		=	now(),
									modified_date		=	now()");
				$this->_db->query("UPDATE 3bit_formulary_snippets SET version='".$newversion."' WHERE snippet_id='".$snippetid."'");	
			}
		}else{  			 
		   $query1		=	$this->_db->query("INSERT INTO 3bit_formulary_snippet_versions SET
									snippet_id			=	'".$snippetid."',
									language_id			=	'".addslashes($data['language_id'])."',
									snippet_title		=	'".addslashes($data['snippet_title'])."',
									snippet_number		=	'".addslashes($data['snippet_number'])."',
									snippet_type		=	'".addslashes($data['snippet_type'])."',
									long_text			=	'".addslashes($data['long_text'])."',
									version				=	'".$newversion."',
									status				=	'".$data['status']."',
									created_date		=	now(),
									modified_date		=	now()");

			$this->_db->query("UPDATE 3bit_formulary_snippets SET version='".$newversion."' WHERE snippet_id='".$snippetid."'");

		}		

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_formulary_snippets','update',$snippetid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_formulary_snippets",'snippet_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_formulary_snippets','delete',$id,'success');
	}
}

?>