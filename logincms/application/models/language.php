<?php

class Application_Model_Language
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_language WHERE language_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_language','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		if(!empty($data['is_default']))
			$default = $data['is_default'];
		else
			$default = '0';

		if($default=='1'){ 
		  $this->_db->query("UPDATE 3bit_language SET is_default	='0'");
		}
		$query	=	$this->_db->query("INSERT INTO 3bit_language SET
								language_name			=	'".addslashes($data['language_name'])."',
								language_code			=	'".addslashes($data['language_code'] )."',
								is_default				=	'".addslashes($default)."',							
								status					=	'1',
								created_date			=	now(),
								modified_date			=	now()");
		$langid	=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_language','add',$langid,'success');
    }

	public function update($data)
	{
		if(!empty($data['is_default']))
			$default = $data['is_default'];
		else
			$default = '0';

		if($default=='1'){ 
		  $this->_db->query("UPDATE 3bit_language SET is_default	='0'");
		}
		$query	=	$this->_db->query("UPDATE 3bit_language SET
								language_name			=	'".addslashes($data['language_name'])."',
								language_code			=	'".addslashes($data['language_code'] )."',
								is_default				=	'".addslashes($default)."',							
								status					=	'1',				
								modified_date		=	now() WHERE language_id='".$data['id']."'");
		$langid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_language','update',$langid,'success');	
    }

	public function delete($id){

		$this->_db->delete("3bit_language",'language_id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_language','delete',$id,'success');
	}
}

?>