<?php

class Application_Model_Pubmed
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    

    public function add($data){
		$id				=	$data['pmid'];
		$url			=	"http://www.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=".$id."&retmode=xml";

		$contents		=	file_get_contents($url);//Or however you what it
		$result			=	$this->xml2array($contents);
		
		$itemArr		=	$result['eSummaryResult']['DocSum']['Item'];	
		$PubDate		=	$itemArr[0];
		$dateArr		=	explode(" ",$PubDate);
		$pyear			=	$dateArr[0];
		$pdate			=	$dateArr[1];

		if(!is_array($itemArr[1]))
			$EPubDate		=	$itemArr[1];
		else
			$EPubDate		=	'';
		$Source			=	$itemArr[2];
		$authorcnt		=	count($itemArr[3]['Item']);
		$Author			=	'';	

		for($i=0;$i<$authorcnt;$i++){
			if(!empty($itemArr[3]['Item'][$i]))
			$Author		.=	$itemArr[3]['Item'][$i]."\r";
		}

		$LastAuthor		=	$itemArr[4];
		$Title			=	$itemArr[5];
		$Volume			=	$itemArr[6];
		$Issue			=	$itemArr[7];
		$Pages			=	$itemArr[8];
		$Lang			=	$itemArr[9]['Item'];
		$NlmUniqueID	=	$itemArr[10];
		$ISSN			=	$itemArr[11];
		if(!is_array($itemArr[12]))
			$ESSN		=	$itemArr[12];
		else
			$ESSN		=	'';

		$typecnt		=	count($itemArr[13]['Item']);
		$PubType		=	'';

		for($i=0;$i<$typecnt;$i++){
			if(!empty($itemArr[13]['Item'][$i]))
			$PubType .=	$itemArr[13]['Item'][$i].",";
		}

		$RecordStatus	=	$itemArr[14];
		$PubStatus		=	$itemArr[15];

		if(!is_array($itemArr[16]['Item']))
			$pubmed			=	$itemArr[16]['Item'];
		else
			$pubmed			=	'';

		if(!is_array($itemArr[18]))
			$References		=	$itemArr[18];
		else
			$References		=	'';

		$HasAbstract	=	$itemArr[19];
		$PmcRefCount	=	$itemArr[20];
		$FullJournalName=	$itemArr[21];

		if(!is_array($itemArr[22]))
			$ELocationID	=	$itemArr[22];
		else
			$ELocationID	=	'';

		$SO				=	$itemArr[23];

		$notes			=	$ISSN+$PubType;

		$selPubmed		=	$this->_db->fetchAll("SELECT id FROM endnote_refs WHERE accession_number='".$data['pmid']."'");

		if(count($selPubmed)>0){

			$pubmed_id		=	$selPubmed[0]['id'];

			$updatePubmed	=	$this->_db->query("UPDATE endnote_refs SET 
												`author`		=	'".addslashes($Author)."',
												`year`			=	'".addslashes($pyear)."',											
												`title`			=	'".addslashes($Title)."',
												`pages`			=	'".addslashes($Pages)."',
												`secondary_title`=	'".addslashes($Source)."',
												`volume`		=	'".addslashes($Volume)."',
												`number`		=	'".addslashes($Issue)."',
												`date`			=	'".addslashes($pdate)."',
												`language`		=	'".addslashes($Lang)."',
												`notes`			=	'".addslashes($notes)."',
												`accession_number`=	'".addslashes($data['pmid'])."' WHERE id = '".$pubmed_id."'");

			$audit	=	new Application_Model_Audits();

			$audit->insert_audit('endnote_refs','update',$pubmed_id,'success');		

		}else{			
			$query		=	$this->_db->query("INSERT INTO endnote_refs SET 
												`author`		=	'".addslashes($Author)."',
												`year`			=	'".addslashes($pyear)."',											
												`title`			=	'".addslashes($Title)."',
												`pages`			=	'".addslashes($Pages)."',
												`secondary_title`=	'".addslashes($Source)."',
												`volume`		=	'".addslashes($Volume)."',
												`number`		=	'".addslashes($Issue)."',
												`date`			=	'".addslashes($pdate)."',
												`language`		=	'".addslashes($Lang)."',
												`notes`			=	'".addslashes($notes)."',
												`accession_number`=	'".addslashes($data['pmid'])."' ");
			$pubmed_id = $this->_db->lastInsertId();	

			$audit	=	new Application_Model_Audits();

			$audit->insert_audit('endnote_refs','add',$pubmed_id,'success');	
		
		}	

    }


	public function manual_add($data)
	{
		$authors	=	'';
		$editors	=	'';

		if(!empty($data['txtAuth'])){
			if(count($data['txtAuth'])>0){
				for($i=0;$i<count($data['txtAuth']);$i++)
					 $authors	.=  $data['txtAuth'][$i]."\r";			
			}
		}
		
		if($data['txt_pagesto'] !='')
			$pages		=	$data['txt_pagesfrom']."&#151;".$data['txt_pagesto'];
		else
			$pages		=	$data['txt_pagesfrom'];
		
		$content		=	preg_replace(array('/<p>/','/<\/p>/'),array('',''),$data['txtContent']);

		$query			=	$this->_db->query("INSERT INTO endnote_refs SET 	
										`author`		=	'".addslashes($authors)."',		
										`year`			=	'".addslashes($data['pub_date'])."',										
										`title`			=	'".addslashes($data['title'])."',
										`secondary_title`=	'".addslashes($data['full_journalname'])."',
										`volume`		=	'".addslashes($data['volume'])."',
										`number`		=	'".addslashes($data['issue'])."',
										`pages`			=	'".addslashes($pages)."'");

		$pubmed_id		= $this->_db->lastInsertId();	

		$audit			= new Application_Model_Audits();

		$audit->insert_audit('endnote_refs','add',$pubmed_id,'success');
		
	}

	public function read($id){
		$sql = "SELECT * FROM `endnote_refs` WHERE id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('endnote_refs','view',$id,'success');
            return $result->fetchAll();
        }	     
	}

	public function manual_update($data)
	{
		$authors	=	'';
		$editors	=	'';
		if(count($data['txtAuth'])>0){
			for($i=0;$i<count($data['txtAuth']);$i++){
				 $authors	.=  $data['txtAuth'][$i]."##";
			}
			$auths			=  explode("##",$authors);			
		}if(count($data['txtEdit'])>0){
			for($i=0;$i<count($data['txtEdit']);$i++)
				 $editors	.=  $data['txtEdit'][$i]."##";
			$editors			=  explode("##",substr($editors,0,-2));			
		}
		

		if(count($auths)>2)
			$last		=	count($auths)-2;
		else
			$last		=	0;

		$lastauth		=	$auths[$last];
		if(count($editors)>2)
		   $elast		=	count($editors)-2;
		else
		   $elast		=	0;		

		$lastedit		=	$editors[$elast];

		sort($editors,SORT_REGULAR); 	

		$lasteditors	=	implode("##",$editors);

		if($data['txt_pagesto'] !='')
			$pages		=	$data['txt_pagesfrom']."&#151;".$data['txt_pagesto'];
		else
			$pages		=	$data['txt_pagesfrom'];
		
		$content		=	preg_replace(array('/<p>/','/<\/p>/'),array('',''),$data['txtContent']);

		$query			=	$this->_db->query("UPDATE 3bit_pubmed SET 										
										`pub_date`		=	'".addslashes($data['pub_date'])."',										
										`last_author`	=	'".addslashes($lastauth)."',
										`last_editor`	=	'".addslashes($lasteditors)."',
										`title`			=	'".addslashes($data['title'])."',
										`volume`		=	'".addslashes($data['volume'])."',
										`issue`			=	'".addslashes($data['issue'])."',
										`pages`			=	'".addslashes($pages)."',
										`full_journalname`=	'".addslashes($data['full_journalname'])."',
										`content`		=	'".htmlentities(addslashes($content))."',										
										`modified_date`	=	now() WHERE pubmed_id ='".$data['id']."'");

		$pubmed_id		=	$data['id'];

		$this->_db->query("DELETE FROM 3bit_pubmed_authors WHERE pubmed_id='".$pubmed_id."'");	

		for($i=0;$i<count($auths)-1;$i++){
				$author		=	$auths[$i];
				$this->_db->query("INSERT INTO 3bit_pubmed_authors SET
											pubmed_id	=	'".$pubmed_id."',											
											author_name	=	'".addslashes($author)."',
											created_date=	now(),
											modified_date=	now()
				");
		}

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_pubmed','update',$pubmed_id,'success');		
	}

	public function xml2array($contents, $get_attributes=1, $priority = 'tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;

		//Initializations
		$xml_array		= array();
		$parents		= array();
		$opened_tags	= array();
		$arr			= array();

		$current		= &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble
			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.
			$result = array();
			$attributes_data = array();
			
			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}

			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}

			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					$current = &$current[$tag];
				} else { //There was another element with the same tag name
				   if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;
						
						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						
						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
								
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
							
							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}
		
		return($xml_array);
	}
	
}

?>