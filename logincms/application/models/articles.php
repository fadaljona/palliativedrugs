<?php

class Application_Model_Articles
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    

    public function getSnippets()
    {
        $sql = "SELECT snippet_id,snippet_title FROM 3bit_snippets";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_articles WHERE article_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$data			=	$result->fetchAll();
			$header_image	= 	$data[0]['header_image'];		  
			$article_image	=	$data[0]['article_image'];
			$image1			=	'';
		    $image2			=	'';	

			if($header_image<>"" && $header_image <>'0'){
				$imgsql				=	$this->_db->fetchRow("SELECT asset_name FROM 3bit_assets WHERE asset_id='".$header_image."'");						
				if($imgsql)
					$image1			=	$imgsql['asset_name'];
			}
			if($article_image<>"" && $article_image <>'0'){
				$imgsql2			=	$this->_db->fetchRow("SELECT asset_name FROM 3bit_assets WHERE asset_id='".$article_image."'");	
				if(!empty($imgsql2)>0)
					$image2			=	$imgsql2['asset_name'];
			}
			$data[0]['header_image_img']	=	$image1;
			$data[0]['article_image_img']	=	$image2;

			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_articles','view',$id,'success');
            return $data;
        }
    }

	public function add($data)
    {	
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';

		if(!empty($data['header_imageid']))
			$header_image	=	$data['header_imageid'];
		else
			$header_image	=	0;

		if(!empty($data['article_imageid']))
			$article_image	=	$data['article_imageid'];
		else
			$article_image	=	0;

		$sql	=	"INSERT INTO 3bit_articles SET
								article_title			=	'".addslashes($data['article_title'])."',
								owner					=	'".addslashes($data['owner'] )."',
								user_id					=	'".addslashes($logged_user)."',
								short_text				=	'".addslashes($data['short_text'])."',
								long_text				=	'".addslashes($data['long_text'])."',
								header_image			=	'".addslashes($header_image)."',
								article_image			=	'".addslashes($article_image)."',
								image_text				=	'".addslashes($data['image_text'])."',
								meta_title				=	'".addslashes($data['meta_title'])."',
								meta_desc				=	'".addslashes($data['meta_desc'])."',
								meta_keywords			=	'".addslashes($data['meta_keywords'])."',
								status					=	'".$data['status']."',
								created_date			=	now(),
								modified_date			=	now()";

		$query	=	$this->_db->getConnection()->query($sql);

		$artid	=	$this->_db->lastInsertId();	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_articles','add',$artid,'success');
    }

	public function update($data)
	{
		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';

		if(!empty($data['header_imageid']))
			$header_image	=	$data['header_imageid'];
		else
			$header_image	=	0;

		if(!empty($data['article_imageid']))
			$article_image	=	$data['article_imageid'];
		else
			$article_image	=	0;

		$sql	=	"UPDATE 3bit_articles SET
								article_title			=	'".addslashes($data['article_title'])."',
								owner					=	'".addslashes($data['owner'] )."',
								user_id					=	'".addslashes($logged_user)."',
								short_text				=	'".addslashes($data['short_text'])."',
								long_text				=	'".addslashes($data['long_text'])."',
								header_image			=	'".addslashes($header_image)."',
								article_image			=	'".addslashes($article_image)."',
								image_text				=	'".addslashes($data['image_text'])."',
								meta_title				=	'".addslashes($data['meta_title'])."',
								meta_desc				=	'".addslashes($data['meta_desc'])."',
								meta_keywords			=	'".addslashes($data['meta_keywords'])."',
								status					=	'".$data['status']."',					
								modified_date		=	now() WHERE article_id='".$data['id']."'";
		$query	=	$this->_db->getConnection()->query($sql);

		$artid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_articles','update',$artid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_articles",'article_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_articles','delete',$id,'success');
	}
}

?>