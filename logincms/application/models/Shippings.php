<?php

class Application_Model_Shippings
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }        

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_shipping WHERE ship_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_shipping','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_shipping SET
								weight				=	'".addslashes($data['weight'])."',
								zone1_charge		=	'".addslashes($data['zone1_charge'] )."',
								zone2_charge		=	'".addslashes($data['zone2_charge'])."',
								zone3_charge		=	'".addslashes($data['zone3_charge'])."'";

		$query	=	$this->_db->query($sql);

		$shippingid	=	$this->_db->lastInsertId();	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_shipping','add',$shippingid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_shipping SET
								weight				=	'".addslashes($data['weight'])."',
								zone1_charge		=	'".addslashes($data['zone1_charge'] )."',
								zone2_charge		=	'".addslashes($data['zone2_charge'])."',
								zone3_charge		=	'".addslashes($data['zone3_charge'])."' WHERE ship_id='".$data['id']."'";
		$query	=	$this->_db->query($sql);

		$shippingid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_shipping','update',$shippingid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_shipping",'ship_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_shipping','delete',$id,'success');
	}
}

?>