<?php

class Application_Model_Reports
{
	private $_db; 
	private $_ns;

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
		$this->_ns = Zend_Registry::get("ns");
		$this->_baseurl	= Zend_Registry::get("baseUrl");		
    }       

	public function top_products()
    {
		$sql		=	"SELECT sum( d.quantity ) qty, d.product_id, p.product_name,p.style,p.size,o.order_id
FROM `3bit_order_details` d, `3bit_products` p, `3bit_orders` o WHERE p.product_id = d.product_id AND o.order_id = d.order_id AND (o.status='Shipped') AND month( o.shipped_date ) = '".date("m")."' AND year( o.shipped_date ) = '".date("Y")."' GROUP BY p.product_id ORDER BY qty DESC , p.product_name ASC  LIMIT 0 , 10 ";  
      
		$result		=	$this->_db->fetchAll($sql);		
		return $result;		
    }	

	public function top_lproducts()
    {
		$lastmon			=	date("m", mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));
		$lastyear			=	date("Y", mktime(0, 0, 0, date("m")-1, date("d"), date("Y")));		

		$sql		=	"SELECT sum( d.quantity ) qty, d.product_id, p.product_name,p.style,p.size,o.order_id
FROM `3bit_order_details` d, `3bit_products` p, `3bit_orders` o WHERE p.product_id = d.product_id AND o.order_id = d.order_id AND (o.status='Shipped') AND month( o.shipped_date ) = '".$lastmon."' AND year( o.shipped_date ) = '".$lastyear."' GROUP BY p.product_id ORDER BY qty DESC , p.product_name ASC  LIMIT 0 , 10 ";  
      
		$result		=	$this->_db->fetchAll($sql);		
		return $result;		
    }		

	public function acc_year($year)
	{
		$selayear	=	$this->_db->fetchAll("SELECT year( ordered_date ) AS ayear FROM 3bit_orders WHERE 1 GROUP BY year( ordered_date ) ORDER BY ordered_date");

		$ayearDrop	=	'';

		if(count($selayear)>0){
			foreach($selayear as $arow){
				if($year==$arow['ayear'])
					$asel	=	'selected';
				else
					$asel	=	'';
				$ayearDrop	.=	'<option value="'.$arow['ayear'].'" '.$asel.'>'.$arow['ayear'].'</option>';
			} 
			if(date("m")=='12'){
				$nextyear	=	date("Y")+1;
				if($year==$nextyear)
					$asel	=	'selected';
				else
					$asel	=	'';
				$ayearDrop	.=	'<option value="'.$nextyear.'" '.$asel.'>'.$nextyear.'</option>';
			}
		}  
		return 	$ayearDrop;
	}

	public function order_list($data)
	{
		$sql		=	"SELECT o.order_id, concat(c.fname, ' ',c.lname)as Name,DATE_FORMAT(CASE WHEN o.shipped_date != '0000-00-00 00:00:00' THEN o.shipped_date ELSE o.ordered_date END,'%d/%m/%Y') as order_date, o.ordertotal , o.status FROM 3bit_orders o INNER JOIN 3bit_customers c ON o.customer_id = c.customer_id WHERE 1 AND o.ccerrcode !=''";
		$start_date	=	'';
		$end_date	=	'';	
		
		if($data['txt_start']<>""){			
			$start_date	= $data['txt_start'];
		}
		if($data["txt_end"]<>""){			
			$end_date	= $data["txt_end"];
		}
		$status		=	 $data['selstatus'];

		// To combine the date with query //

		if($status =='Shipped' && $data['selayear']==''){
			if(trim($start_date) != "" && trim($end_date) == ''){
				$this->_ns->txt_start=trim($start_date);
				$monArr		=	explode("-",$start_date);
				$mon		=	$monArr[1];
				$newDate	=	$monArr[2]."-".$monArr[1]."-".$monArr[0];			
				$sql		.=  " and date(o.shipped_date)  >= '".$newDate."' "  ;  
			}else{
				$this->_ns->txt_start="";
			}		
			if(trim($end_date) != "" && trim($start_date) == ""){
				$this->_ns->txt_end=trim($end_date);
				$monArr		=	explode("-",$end_date);
				$mon		=	$monArr[1];
				$newDate	=	$monArr[2]."-".$monArr[1]."-".$monArr[0];	
			    $sql		.=  " and date(o.shipped_date)  <= '".$newDate."' "  ;  
			 }else{	 	
				$this->_ns->txt_end="";
			 }
			 if(trim($end_date) != "" && trim($start_date) != ""){
				$this->_ns->txt_start=trim($start_date);
				$this->_ns->txt_end=trim($end_date);
				$monSArr	=	explode("-",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("-",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
				$sql		.=	" and date(o.shipped_date)  <= '".$newEDate."' and date(o.shipped_date) >= '".$newSDate."'  "  ;  
			 }else{
				$this->_ns->txt_start="";
				$this->_ns->txt_end="";
			 }
		}else if($data['selayear']=='' && $status !='Shipped'){	 
			if(trim($start_date) != "" && trim($end_date) == ''){
				$this->_ns->txt_start=trim($start_date);
				$monArr		=	explode("-",$start_date);
				$mon		=	$monArr[1];
				$newDate	=	$monArr[2]."-".$monArr[1]."-".$monArr[0];			
				$sql		.=	" and date(o.ordered_date)  >= '".$newDate."' "  ;  
			}else{
				$this->_ns->txt_start="";
			}		
			if(trim($end_date) != "" && trim($start_date) == ""){
				$this->_ns->txt_end=trim($end_date);
				$monArr		=	explode("-",$end_date);
				$mon		=	$monArr[1];
				$newDate	=	$monArr[2]."-".$monArr[1]."-".$monArr[0];	
			    $sql		.=  " and date(o.ordered_date)  <= '".$newDate."' "  ;  
			 }else{	 	
				$this->_ns->txt_end="";
			 }
			 if(trim($end_date) != "" && trim($start_date) != ""){
				$this->_ns->txt_start=trim($start_date);
				$this->_ns->txt_end=trim($end_date);
				$monSArr	=	explode("-",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("-",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
			    $sql		.=  " and date(o.ordered_date)  <= '".$newEDate."' and date(o.ordered_date) >= '".$newSDate."'  "  ;  
			 }else{
				$this->_ns->txt_start="";
				$this->_ns->txt_end="";
			 }
		}

		if($data['selayear']<>""){	
			 $prev_year	=	$data['selayear']-1;	
			 $prev_date	=	$prev_year."-12-01";	
			 $sear_year	=	$data['selayear'];		
			 $sear_date =   $sear_year."-11-30";	
			 if($status =='Shipped')
				 $sql .= " and date(o.shipped_date) BETWEEN date('".$prev_date."') and date('".$sear_date."')  "  ; 
			 else
				 $sql .= " and date(o.ordered_date) BETWEEN date('".$prev_date."') and date('".$sear_date."')  "  ;			 
			 $this->_ns->txt_pyear=$prev_date;
			 $this->_ns->txt_syear=$sear_date;																			   
		}else{						   
			 $this->_ns->txt_pyear="";
			 $this->_ns->txt_syear="";
		}	

		if(!empty($data['selstatus'])){
			if($data['selstatus'] !='Cancelled')
				$sql	.=	" AND o.status = '".$data['selstatus']."'";
			else
				$sql	.=	" AND (o.status = 'Rejected' OR o.status = 'Archived') ";

			 $this->_ns->selStatus=$data['selstatus'];
		}if(!empty($data['txt_cust'])){
			$sql	.=	" AND (c.fname LIKE '%".$data['txt_cust']."%' OR c.email LIKE '%".$data['txt_cust']."%') ";
			$this->_ns->txt_cust =	$data['txt_cust'];
		}	
		
		$result	=	$this->_db->fetchAll($sql);

		return $result;

	}

	public function get_days ( $s, $e )
	{
		$arr	=	explode("-",$s);
		$d		=	$arr[2];
		$m		=	$arr[1];
		$y		=	$arr[0];

		$tstamp=mktime(0,0,0,$m,$d,$y);
	   
		$Tdate = getdate($tstamp);
	   
		$weekday=$Tdate["wday"];

		if($weekday!=1){
			for($i=0;$i<6;$i++){
				$g	=	date("N", mktime(0, 0, 0, $m, $d-$i, $y));
				if($g=='1')
				   $s	=	date("Y-m-d", mktime(0, 0, 0, $m, $d-$i, $y));
			}		
		}

		$r = array ();
		$s = strtotime ( $s . ' GMT' );
		$e = strtotime ( $e . ' GMT' );

		$day = gmdate ( 'l', $s );

		do
		{
			$r[] = gmdate ( 'd/m/Y', $s );

			$s += 86400 * 7;

		} while ( $s <= $e );

		return $r;
	}

	public function get_months($date1, $date2) 
	{
	   $time1  = strtotime($date1);
	   $time2  = strtotime($date2);
	   $my     = date('mY', $time2);

	   $months = array(date('m F Y', $time1));
	   $f      = '';

	   while($time1 < $time2) {
		  $time1 = strtotime((date('Y-m-d', $time1).' +15days'));
		  if(date('F', $time1) != $f) {
			 $f = date('F', $time1);
			 if(date('mY', $time1) != $my && ($time1 < $time2))
				$months[] = date('m F Y', $time1);
		  }
	   }

	   $months[] = date('m F Y', $time2);
	   $months	 = array_values(array_unique($months));
	   return $months;
	}

	public function get_years($date1, $date2) 
	{
	   $time1  = strtotime($date1);
	   $time2  = strtotime($date2);
	   $my     = date('mY', $time2);

	   $years  = array(date('Y', $time1));
	   $f      = '';

	   while($time1 < $time2) {
		  $time1 = strtotime((date('Y-m-d', $time1).' +15days'));
		  if(date('F', $time1) != $f) {
			 $f = date('F', $time1);
			 if(date('mY', $time1) != $my && ($time1 < $time2))
				$years[] = date('Y', $time1);
		  }
	   }

	   $years[] = date('Y', $time2);
	   $years	 = array_values(array_unique($years));
	   return $years;
	} 

	public function getdates($fromDate,$toDate)
	{
		$dateMonthYearArr	=	array();
		$fromDateTS			=	strtotime($fromDate);
		$toDateTS			=	strtotime($toDate);

		for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
			// use date() and $currentDateTS to format the dates in between
			$currentDateStr = date("d/m/Y",$currentDateTS);
			$dateMonthYearArr[] = $currentDateStr;	
		}
		return 	$dateMonthYearArr;
	 }


	 // Function to calculate VAT for a individual product 

	public function findProductvat($productid){

		$selProd	=	$this->_db->fetchAll("SELECT price,vat_id FROM 3bit_products WHERE product_id='".$productid."'");

		if(count($selProd)>0){

			foreach($selProd as $srow){

				$price_site	=	$srow['price'];			

				$vatid		=	$srow['vat_id'];

				$selVat		=	$this->_db->fetchRow("SELECT vat_rate FROM 3bit_product_vats WHERE vat_id='".$vatid."'");

				if(count($selVat)>0){
					$vatamt			= 	$selVat['vat_rate'];
				}
				$vatper			=	($vatamt/100);

				$pricewovat		=	$price_site/(1+$vatper);

				$vatprice		=	($price_site-$pricewovat);
						
			}		
		}	 
		return $vatprice;
	}	

	public function getresults($data)
	{
		$start_date	=	'';
		$end_date	=	'';

		if($data['start_date']<>""){				
			$start_date					=	$data['start_date'];
			$this->_ns->start_date		=	$start_date;
		}
		if($data["end_date"]<>""){		
			$end_date					=	$data["end_date"];
			$this->_ns->end_date		=	$end_date;
		}
		if($data['report_type']<>""){
			$report_type				=	$data['report_type'];
			$this->_ns->report_type		=	$report_type;
		}
		if($data['display_type']<>""){
			$display_type				=	$data['display_type'];
			$this->_ns->display_type	=	$display_type;
		}		   		

		$srdate	=	explode("/",$start_date);
		$erdate	=	explode("/",$end_date);

		
		$smon	=	$srdate[1];
		$sday	=	$srdate[0];
		$syer	=	$srdate[2];
		$emon	=	$erdate[1];
		$eday	=	$erdate[0];
		$eyer	=	$erdate[2];
		
		if($data['report_type']=='CartConversion'){

		 $selCus		=	$this->_db->fetchAll("SELECT a.customer_id FROM 3bit_customers_address a, 3bit_cart_items c WHERE a.cart_sessionid =  c.cart_sessionid AND c.created_date < DATE_SUB( now( ) , INTERVAL 24 HOUR) GROUP BY a.customer_id");

		 if(count($selCus)>0){
			foreach($selCus as $cres){
				$all_cust .= $cres['customer_id'].",";
			}
		 }

		if($data['display_type']=='Daily'){ 				
		
			$results	=	'<div class="report_tabs">	  
							  <ul>
								<li><a href="#details">Details</a></li>
							  </ul>
							  <div id="details">
							  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="display">
							<thead>
								<tr>
									<th>Day</th>
									<th>Total Carts Created</th>
									<th>Carts Abandoned</th>		
									<th>Abandoned %</th>
									<th>Carts Converted</th>
									<th>Conversion %</th>
								</tr>
							</thead>';
							
			$sdate	=	date("m/d/Y", mktime(0, 0, 0, $smon, $sday, $syer));
			$edate	=	date("m/d/Y", mktime(0, 0, 0, $emon, $eday, $eyer));
			$datefrom = strtotime($sdate, 0);
			$dateto  = strtotime($edate, 0);
			$fullDays    = floor(($dateto-$datefrom)/(60*60*24));						

			for($i=0;$i<=$fullDays;$i++){

			   $dispdate	=	date("m/d/Y", mktime(0, 0, 0, $smon, $sday+$i, $syer));				  

			   $abandoned	=	0;
			   $abper		=	0;
			   $placed		=	0;
			   $plper		=	0;
			   $total		=	0;

			   $order		=	$this->_db->fetchRow("SELECT count(order_id)as cnt FROM 3bit_orders WHERE DATE_FORMAT(ordered_date,'%m/%d/%Y')='".$dispdate."' AND (status !='Rejected' AND status !='Archived' AND status !='Refunded')");				   
			   if($order){
					$placed	=	$order['cnt'];
				}

			   $sql			=	$this->_db->fetchRow("SELECT count(a.cart_sessionid)as ccnt FROM 3bit_customers c,3bit_customers_address a WHERE FIND_IN_SET(c.customer_id,'".$all_cust."') AND c.customer_id=a.customer_id AND DATE_FORMAT(a.created_date,'%m/%d/%Y')='".$dispdate."' GROUP BY c.customer_id");
			   if($sql){
					$abandoned	=	$sql['ccnt'];
			   }

			   $seltemp	=	$this->_db->fetchRow("SELECT count(order_id)as ocount FROM 3bit_orders WHERE status = 'Temp' AND DATE_FORMAT(ordered_date,'%m/%d/%Y')='".$dispdate."'");
			   if($seltemp){
					 $abandoned	+= $seltemp['ocount'];
					 $abandtemp	= $seltemp['ocount'];
				} 					

				$total	=	$placed+$abandoned;

				if($abandoned>0){
					$abper	=	($abandoned/$total)*100;
					$abper	=	number_format($abper,1,'.','');
				}
				if($placed>0){
					$plper	=	($placed/$total)*100;
					$plper	=	number_format($plper,1,'.','');
				}

				if($datefrom<=$dateto && $total>0){					
					$results	.=	'<tr>
										<td>'.$dispdate.'</td>
										<td>'.$total.'</td>
										<td>'.$abandoned.'</td>
										<td>'.$abper.'</td>
										<td>'.$placed.'</td>
										<td>'.$plper.'</td>		
									</tr>';
				} 
			} 
			$results	.=	'<tr>
					 <td colspan="6"><a class="button" href="'.$this->_baseurl.'/reports/exportxls"><img src="'.$this->_baseurl.'/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
				</tr>
			</table>
			</div>
			</div>';
		}else if($data['display_type']=='Weekly'){
		
			$results	=	'<div class="report_tabs">	  
							  <ul>
								<li><a href="#details">Details</a></li>
							  </ul>
							  <div id="details">
							  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="display">
							<thead>
								<tr>
									<th>Week Starting Date</th>
									<th>Total Carts Created</th>
									<th>Carts Abandoned</th>		
									<th>Abandoned %</th>
									<th>Carts Converted</th>
									<th>Conversion %</th>
								</tr>
							</thead>';
							
			$sdate	=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
			$edate	=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
			$datefrom = strtotime($sdate, 0);
			$dateto  =  strtotime($edate, 0);

			$datearr=	$this->get_days($sdate,$edate);
			$cntday	=	count($datearr);				
			for($i=0;$i<$cntday;$i++){
				$exp	=	explode("/",$datearr[$i]);
				$m		=	$exp[1];
				$d		=	$exp[0];
				$y		=	$exp[2];
				$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

				$abandoned	=	0;
				$abper		=	0;
				$placed		=	0;
				$plper		=	0;
				$total		=	0;

			   $order		=	$this->_db->fetchRow("SELECT count(order_id)as cnt FROM 3bit_orders WHERE WEEK(ordered_date,1)='".$g."' AND YEAR(ordered_date)='".$y."' AND (status !='Rejected' AND status !='Archived' AND status !='Refunded')");					   
			   if($order){
					$placed	=	$order['cnt'];
				}

			   $sql			=	$this->_db->fetchRow("SELECT count(a.cart_sessionid)as ccnt FROM 3bit_customers c,3bit_customers_address a WHERE FIND_IN_SET(c.customer_id,'".$all_cust."') AND c.customer_id=a.customer_id AND WEEK(a.created_date,1)='".$g."' AND YEAR(a.created_date)='".$y."' GROUP BY c.customer_id");
				if($sql){
					$abandoned	=	$sql['ccnt'];
				}

				$seltemp	=	$this->_db->fetchRow("SELECT count(order_id)as ocount FROM 3bit_orders WHERE status = 'Temp' AND WEEK(ordered_date)='".$g."' AND YEAR(ordered_date)='".$y."'");

				if($seltemp){
					 $abandoned	+= $seltemp['ocount'];
				}
				
				$total	=	$placed+$abandoned;

				if($abandoned>0){
					$abper	=	($abandoned/$total)*100;
					$abper	=	number_format($abper,1,'.','');
				}
				if($placed>0){
					$plper	=	($placed/$total)*100;
					$plper	=	number_format($plper,1,'.','');
				}


			   if($datefrom<=$dateto && $total>0){	
				$results	.=	'<tr>
									 <td>'.$datearr[$i].'</td>
									 <td>'.$total.'</td>
									 <td>'.$abandoned.'</td>
									 <td>'.$abper.'</td>
									 <td>'.$placed.'</td>
									 <td>'.$plper.'</td>		
								 </tr>';
			    }			
			}
			
			$results	.=	'<tr>
								<td colspan="6"><a class="button" href="'.$this->_baseurl.'/reports/exportxls"><img src="'.$this->_baseurl.'/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
							</tr>
						</table>
						</div>
						</div>';							
		}else if($data['display_type']=='Monthly'){
			$results	=	'<div class="report_tabs">	  
							  <ul>
								<li><a href="#details">Details</a></li>
							  </ul>
							  <div id="details">
							  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="display">
							<thead>
								<tr>
									<th>Month</th>
									<th>Total Carts Created</th>
									<th>Carts Abandoned</th>		
									<th>Abandoned %</th>
									<th>Carts Converted</th>
									<th>Conversion %</th>
								</tr>
							</thead>';							
			$sdate	=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
			$edate	=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
			$datefrom = strtotime($sdate, 0);
			$dateto  = strtotime($edate, 0);

			$months	=	$this->get_months($sdate,$edate);
			$cntday	=	count($months);			

			for($i=0;$i<$cntday;$i++){					
				$exp		=	explode(" ",$months[$i]);
				$m			=	$exp[0];
				$y			=   $exp[2];
				$dispmon	=   $exp[1]." ".$exp[2];				

				$abandoned	=	0;
				$abper		=	0;
				$placed		=	0;
				$plper		=	0;
				$total		=	0;

			   $order		=	$this->_db->fetchRow("SELECT count(order_id)as cnt FROM 3bit_orders WHERE MONTH(ordered_date)='".$m."' AND YEAR(ordered_date)='".$y."' AND (status !='Rejected' AND status !='Archived' AND status !='Refunded')");				  
			   if($order){
					$placed	=	$order['cnt'];
				}


			   $sql			=	$this->_db->fetchAll("SELECT count(a.cart_sessionid)as ccnt FROM 3bit_customers c,3bit_customers_address a WHERE FIND_IN_SET(c.customer_id,'".$all_cust."') AND c.customer_id=a.customer_id AND MONTH(a.created_date)='".$m."' AND YEAR(a.created_date)='".$y."' GROUP BY c.customer_id");
			 
				if($sql){
					foreach($sql as $res)
						$abandoned	+=	$res['ccnt'];
				}

				$seltemp	=	$this->_db->fetchRow("SELECT count(order_id)as ocount FROM 3bit_orders WHERE status = 'Temp' AND MONTH(ordered_date)='".$m."' AND YEAR(ordered_date)='".$y."'");

				if($seltemp){
					 $abandoned	+= $seltemp['ocount'];
				}
				
				$total	=	$placed+$abandoned;

				if($abandoned>0){
					$abper	=	($abandoned/$total)*100;
					$abper	=	number_format($abper,1,'.','');
				}
				if($placed>0){
					$plper	=	($placed/$total)*100;
					$plper	=	number_format($plper,1,'.','');
				}
				if($datefrom<=$dateto && $total>0){					 
			
				 $results	.=	'<tr>
									 <td>'.$dispmon.'</td>
									 <td>'.$total.'</td>
									 <td>'.$abandoned.'</td>
									 <td>'.$abper.'</td>
									 <td>'.$placed.'</td>
									 <td>'.$plper.'</td>		
								 </tr>';
				} 
			}
			
			$results	.=	'<tr>
								 <td colspan="6"><a class="button" href="'.$this->_baseurl.'/reports/exportxls"><img src="'.$this->_baseurl.'/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
							</tr>
						</table>
						</div>
						</div>';				
		}else if($data['display_type']=='Yearly'){
			 $results	=	'<div class="report_tabs">	  
							  <ul>
								<li><a href="#details">Details</a></li>
							  </ul>
							  <div id="details">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" class="display">
							<thead>
								<tr>
									<th>Year</th>
									<th>Total Carts Created</th>
									<th>Carts Abandoned</th>		
									<th>Abandoned %</th>
									<th>Carts Converted</th>
									<th>Conversion %</th>
								</tr>
							</thead>';
							
			$sdate	=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
			$edate	=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
			$datefrom = strtotime($sdate, 0);
			$dateto  =  strtotime($edate, 0);
			$years	=	$this->get_years($sdate,$edate);
			$cntday	=	count($years);

			for($i=0;$i<$cntday;$i++){					
				$y			=   $years[$i];
				$abandoned	=	0;
				$abper		=	0;
				$placed		=	0;
				$plper		=	0;
				$total		=	0;

			   $order		=	$this->_db->fetchRow("SELECT count(order_id)as cnt FROM 3bit_orders WHERE YEAR(ordered_date)='".$y."' AND (status !='Rejected' AND status !='Archived' AND status !='Refunded')");				   
			   if($order){
					$placed	=	$order['cnt'];
				}


			   $sql			=	$this->_db->fetchAll("SELECT count(a.cart_sessionid)as ccnt FROM 3bit_customers c,3bit_customers_address a WHERE FIND_IN_SET(c.customer_id,'".$all_cust."') AND c.customer_id=a.customer_id AND YEAR(a.created_date)='".$y."' GROUP BY c.customer_id");

				if($sql){
					foreach($sql as $res)
						$abandoned	+=	$res['ccnt'];
				}

				$total	=	$placed+$abandoned;

				if($abandoned>0){
					$abper	=	($abandoned/$total)*100;
					$abper	=	number_format($abper,1,'.','');
				}
				if($placed>0){
					$plper	=	($placed/$total)*100;
					$plper	=	number_format($plper,1,'.','');
				}
			 if($datefrom<=$dateto && $total>0){	
				$results	.=	'<tr>
									 <td>'.$y.'</td>
									 <td>'.$total.'</td>
									 <td>'.$abandoned.'</td>
									 <td>'.$abper.'</td>
									 <td>'.$placed.'</td>
									 <td>'.$plper.'</td>		
								 </tr>';
			 } 
		}
			$results	.=	'<tr>
							 <td colspan="6"><a class="button" href="'.$this->_baseurl.'/reports/exportxls"><img src="'.$this->_baseurl.'/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
						</tr>
					</table>
					</div>
				  </div>';	
		}
		
	 }else if($data['report_type']=='OrdersByDate'){

		  if($data['display_type']=='Daily'){

			$sql = "SELECT count(o.order_id )as ordcnt,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o WHERE (o.status ='Shipped') AND o.payment_status !='Declined (General).' ";

			// Concate with query //

			if(trim($end_date) != "" && trim($start_date) != ""){	 	
				$monSArr	=	explode("/",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("/",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
				$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
			}		

			$sql	.=	" GROUP BY DATE_FORMAT(o.shipped_date,'%d/%m/%Y') ORDER BY o.shipped_date ASC";	

			$res	=	$this->_db->fetchAll($sql);

			if(count($res)>0){				
				$daytotal	=	0;
		?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
			  <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Day</th>
					<th>Number Of Orders</th>			
				</tr>
				</thead>
		<?php foreach($res as $row){ 
					$daytotal	+=	$row['ordcnt'];	
						
		?>

				<tr>
					<td><?php echo $row['odate'];?></td>
					<td><?php echo $row['ordcnt'];?></td>			
				</tr>
				
		<?php 	}?>	
				 <tr>
					<td><b>Totals</b></td>
					<td><b><?php echo $daytotal;?></b></td>			
				</tr>
				<tr>
					<td colspan='6'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
				</tr>
			</table>
			</div>
			</div>
		<?php	}
		  }else if($data['display_type']=='Weekly'){
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
			    $datearr	=	$this->get_days($sdate,$edate);
				$cntday		=	count($datearr);
				if($cntday>0){
					$weektot	=	0;
			?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Week Starting Date</th>
					<th>Number Of Orders</th>			
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp	=	explode("/",$datearr[$i]);						

						$m		=	$exp[1];
						$d		=	$exp[0];
						$y		=	$exp[2];
						$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

						$sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt FROM 3bit_orders WHERE WEEK(shipped_date,1)='".$g."' AND YEAR(shipped_date)='".$y."' AND (status ='Shipped')  AND payment_status !='Declined (General).' ");
											
						if($sql){ 
							 $ordcnt	=	$sql['ordcnt'];
							 $weektot	+=	$ordcnt;							 
							 if($ordcnt>0){

				?>
					<tr>
						 <td><?php echo $datearr[$i]?></td>
						 <td><?php echo $ordcnt;?></td>
					</tr>
				<?php		}
						} 
					  }
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $weektot;?></b></td>
					</tr>
					<tr>
						<td colspan='6'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
					</tr>
				 </table>
				 </div>
				 </div>
				<?php
				}
		  }else if($data['display_type']=='Monthly'){
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$months		=	$this->get_months($sdate,$edate);
				$cntday		=	count($months);			
				if($cntday>0){
					$monthtot	=	0;
			?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
			  <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Month</th>
					<th>Number Of Orders</th>			
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp		=	explode(" ",$months[$i]);
						$m			=	$exp[0];
						$y			=   $exp[2];
						$dispmon	=   $exp[1]." ".$exp[2];								

						$sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt FROM 3bit_orders WHERE MONTH(shipped_date)='".$m."' AND YEAR(shipped_date)='".$y."' AND (status ='Shipped')  AND payment_status !='Declined (General).'");
						if($sql){	
							 $ordcnt	=	$sql['ordcnt'];
							 $monthtot	+=	$ordcnt;
							 if($ordcnt>0){
				?>
					<tr>
						 <td><?php echo $dispmon?></td>
						 <td><?php echo $ordcnt;?></td>
					</tr>
				<?php		}
						}

					}
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $monthtot;?></b></td>
					</tr>
					<tr>
						<td colspan='6'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
					</tr>
					</table>
				</div>
				</div>
				<?php
				}
		  }else if($data['display_type']=='Yearly'){			  			
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$years		=	$this->get_years($sdate,$edate);
				$cntday		=	count($years);
				if($cntday>0){
					$yeartot	=	0;
			?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Year</th>
					<th>Number Of Orders</th>			
				</tr>
				</thead>
			<?php
				for($i=0;$i<$cntday;$i++){					
					$y			=   $years[$i];
				    $sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt FROM 3bit_orders WHERE YEAR(shipped_date)='".$y."' AND (status ='Shipped')  AND payment_status !='Declined (General).'");				
					if($sql){	
							 $ordcnt	=	$sql['ordcnt'];
							 $yeartot	+=	$ordcnt;
							 if($ordcnt>0){
				?>
					<tr>
						 <td><?php echo $y?></td>
						 <td><?php echo $ordcnt;?></td>
					</tr>
				<?php	}	
					
					}
				}
			?>
				<tr>
					 <td><b>Totals</b></td>
					 <td><b><?php echo $yeartot;?></b></td>
				</tr>
				<tr>
					<td colspan='6'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
				</tr>
				</table>
			</div>
			</div>
			<?php
			 }
		  }

	  }else if($data['report_type']=='OrdersByProduct'){  ?>
		  <div id="article_tabs">
			<ul>
			  <li><a href="#tproducts">Products</a></li>			 
			</ul>
			<div id="tproducts">
		  	<?php  	////////////////////////////// Order FOR products ////////////////////////////////////////////////////////////
				$sqlpro			=  "SELECT sum( d.quantity ) qty,count(o.order_id) as ordcnt,d.product_id,p.product_name,p.product_code,p.style,p.size,p.procat_id,pc.procat_title,o.order_id FROM `3bit_order_details` d, `3bit_products` p, `3bit_orders` o,3bit_product_category pc WHERE p.product_id = d.product_id AND o.order_id = d.order_id AND pc.procat_id=p.procat_id AND (o.status ='Shipped') " ;

				if(trim($end_date) != "" && trim($start_date) != ""){	 	
					$monSArr	=	explode("/",$start_date);
					$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
					$monEArr	=	explode("/",$end_date);
					$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
					$sqlpro		.=  " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
				}	

				$sqlpro			.=	" GROUP BY p.product_id ORDER BY qty DESC , p.product_name ASC";



				if($data['display_type']=='TOP 5')
					$sqlpro		.=	" LIMIT 0,5";
				else if($data['display_type']=='TOP 10')
					$sqlpro		.=	" LIMIT 0,10";
				else if($data['display_type']=='TOP 15')
					$sqlpro		.=	" LIMIT 0,15";
				
				//echo $sqlpro;

				$res	=	$this->_db->fetchAll($sqlpro);

				if(count($res)>0){
					$sumqty		=	0;
					$sumcnt		=	0;
					$sumuknet	=	0;
					$sumuktot	=	0;
					$sumukvat	=	0;
					$netprice	=	0;
					?><h4><?=$data['display_type']?> Products</h4>
					<table width="100%" cellspacing="0" cellpadding="0" class="display">
					<thead>
					<tr>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Type</th>
						<th>SKU</th>
						<th>Total Quantity</th>
						<th>Number Of Orders</th>
						<th>Net Sales</th>
						<th>Taxes</th>
						<th>Gross Sales</th>
					</tr>
					</thead>
					<?php foreach($res as $row){							
							$sumqty		+=	$row['qty'];
							$sumcnt		+=	$row['ordcnt'];
							
							$item_model	=	'';
							
							if($row['style'] != '')
							  $item_model		=	"&nbsp;(".stripslashes($row['style']).")";
							else if($row['size'] != '')
							  $item_model		=	"&nbsp;(".stripslashes($row['size'])." )";
						
							$sel		=	"SELECT o.order_id,o.order_vat_tax,d.* FROM 3bit_orders o,3bit_order_details d WHERE o.order_id=d.order_id AND d.product_id='".$row['product_id']."' AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."' AND (o.status ='Shipped') ";

							if($data['display_type']=='TOP 5')
								$sel		.=	" LIMIT 0,5";
							else if($data['display_type']=='TOP 10')
								$sel		.=	" LIMIT 0,10";
							else if($data['display_type']=='TOP 15')
								$sel		.=	" LIMIT 0,15";

							$selOrder	=	$this->_db->fetchAll($sel);

							$numrows	=	count($selOrder);

							$totvat		=	0;

							$totprice	=	0;

							if($numrows>0){
								
								foreach($selOrder as $result){

									$ordid		=	$result['order_id'];

									$vattax		=	$result['order_vat_tax'];

									$quanty		=	$result['quantity'];	

									$price		=	$result['price'];

									$line_price	=	$quanty*$price;

									if($vattax>0){
										if(!empty($result['product_id']))
											$vat		=	$this->findProductvat($result['product_id']);										

										$totvat		+=	$quanty*$vat;								
									}
									$totprice		+=	$line_price;														
								}
							}

							$netprice		=	$totprice-$totvat;	
											
							$currency		=	"&pound;";
							$sumuknet		+=	 $netprice;
							$sumukvat		+=	 $totvat;
							$sumuktot		+=   $totprice;
							

						?>
						<tr>
							<td><?php echo $row['product_id'];?></td>
							<td><?php echo $row['product_name'].$item_model;?></td>
							<td align="right"><?php echo $type;?></td>
							<td align="right"><?php echo $row['product_code'];?></td>
							<td align="right"><?php echo $row['qty'];?></td>
							<td align="right"><?php echo $row['ordcnt'];?></td>
							<td align="right"><?=$currency?><?php echo number_format($netprice,'2','.','');?></td>
							<td align="right"><?=$currency?><?php echo number_format($totvat,'2','.','');?></td>
							<td align="right"><?=$currency?><?php echo number_format($totprice,'2','.','');?></td>
						</tr>
					<?php	} ?>
						<tr>
							<td colspan='4'>&nbsp;<b>Totals</b></td>
							<td align="right"><B><?php echo $sumqty;?></B></td>
							<td align="right"><B><?php echo $sumcnt;?></B></td>
							<td align="right"><B>&pound;<?php echo number_format($sumuknet,2,'.','');?></B></td>
							<td align="right"><B>&pound;<?php echo number_format($sumukvat,2,'.','');?></B></td>
							<td align="right"><B>&pound;<?php echo number_format($sumuktot,2,'.','');?></B></td>
						</tr>	
					</table>
					<?php }		?>				
			    </div>				
				<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
			</div>
  <?php
	  }else if($data['report_type']=='RefundsByDate'){
		if($data['display_type']=='Daily'){

		?>
		<div id="article_tabs">
			<ul>
			  <li><a href="#trefunds">Refunds</a></li>
			  <li><a href="#tdetails">Details</a></li>			 
			</ul>
			<div id="trefunds">
			<?php $sql = "SELECT count(o.order_id )as ordcnt,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o WHERE (o.status ='Refunded') ";

			// Concate with query //

			if(trim($end_date) != "" && trim($start_date) != ""){	 	
				$monSArr	=	explode("/",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("/",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
				$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
			}		

			$sql	.=	"GROUP BY DATE_FORMAT(o.shipped_date,'%d/%m/%Y') ORDER BY o.shipped_date ASC";	

			$res	=	$this->_db->fetchAll($sql);

			if(count($res)>0){				
				$daytotal	=	0;	
		?>
			<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Day</th>
					<th>Number Of Orders</th>			
				</tr>
				</thead>

		<?php foreach($res as $row){ 
					$daytotal	+=	$row['ordcnt'];	
						
		?>

				<tr>
					<td><?php echo $row['odate'];?></td>
					<td><?php echo $row['ordcnt'];?></td>			
				</tr>
				
		<?php 	}?>	
				 <tr>
					<td><b>Totals</b></td>
					<td><b><?php echo $daytotal;?></b></td>			
				</tr>				
			</table>
		<?php } ?>
			</div>
			<div id='tdetails'>
				<?php $sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country ";

					// Concate with query //

					if(trim($end_date) != "" && trim($start_date) != ""){	 	
						$monSArr	=	explode("/",$start_date);
						$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
						$monEArr	=	explode("/",$end_date);
						$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
						$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
					}		

					$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

					$res		=	$this->_db->fetchAll($sql);

					$salerows	=	count($res);

					if($salerows>0){
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
				?>
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Order Id</th>
					<th>Invoice Number</th>
					<th>Customer Name</th>
					<th>Shipping Country</th>
					<th>Refund Date</th>
					<th>Net Amount</th>
					<th>Taxes</th>
					<th>Refund Amount</th>		
				</tr>
				</thead>
				<?php foreach($res as $row){ 
						$sales		=	$row['ordertotal'];	
						$taxes		=	$row['order_vat_tax'];
						$netsale	=	($sales-$taxes);
						$salestot	+=	$sales;
						$taxtot		+=	$taxes;									
						$nettot		+=	$netsale;  
						
				?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td class='values'><?=$row['odate'];?></td>
					<td class='values'><?=number_format($netsale,'2','.','');?></td>					
					<td class='values'><?=number_format($taxes,'2','.','');?></td>					
					<td class='values'><?=number_format($sales,'2','.','');?></td>					
				</tr>
				<?php }	?>
				 <tr>
					<td colspan='4'><b>Totals</b></td>
					<td><b><?php echo $salerows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
				<?php } ?>
			  </div>
		   <a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a>
		</div>
		<?php	
		  }else if($data['display_type']=='Weekly'){
				$sdate	=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate	=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom = strtotime($sdate, 0);
				$dateto  =	strtotime($edate, 0);

			    $datearr=	$this->get_days ($sdate,$edate);
				$cntday	=	count($datearr);
				if($cntday>0){
					$weektot	=	0;
			?>
		<div id="article_tabs">
			<ul>
			  <li><a href="#trefunds">Refunds</a></li>
			  <li><a href="#tdetails">Details</a></li>			 
			</ul> 
			<div id="trefunds">
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
					<thead>
					<tr>
						<th>Week Starting Date</th>
						<th>Number Of Orders</th>			
					</tr>
					</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp	=	explode("/",$datearr[$i]);
						$m		=	$exp[1];
						$d		=	$exp[0];
						$y		=	$exp[2];
						$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

						 $sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt FROM 3bit_orders WHERE WEEK(shipped_date,1)='".$g."' AND YEAR(shipped_date)='".$y."' AND (status ='Refunded')");

						 if(count($sql)>0){ 
							 $ordcnt	=	$sql['ordcnt'];
							 $weektot	+=	$ordcnt;	
						  if($ordcnt>0){
				?>
					<tr>
						 <td><?php echo $datearr[$i]?></td>
						 <td><?php echo $ordcnt;?></td>
					</tr>
				<?php		}
						} 
					  }
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $weektot;?></b></td>
					</tr>					
				 </table>
				 </div>
				 <div id='tdetails'>
				 <?php if($cntday>0){ 
						$sales		=	0;
						$taxes		=	0;
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
						$shiptot	=	0;
					
				 ?>
				   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				   <thead>
					<tr>
						<th>Order Id</th>
						<th>Invoice Number</th>
						<th>Customer Name</th>
						<th>Shipping Country</th>
						<th>Refund Date</th>
						<th>Net Amount</th>
						<th>Taxes</th>
						<th>Refund Amount</th>
					</tr>
					</thead>
				  <?php for($i=0;$i<$cntday;$i++){
							$exp	=	explode("/",$datearr[$i]);
							$m		=	$exp[1];
							$d		=	$exp[0];
							$y		=	$exp[2];
							$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

							$sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND WEEK(o.shipped_date,1)='".$g."' AND YEAR(o.shipped_date)='".$y."'";								

							$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

							$res		=	$this->_db->fetchAll($sql);

							$salerows	=	count($res);

							$weekrows	+=	$salerows;					

							if($salerows>0){

								foreach ($res as $row){ 
									$sales		=	$row['ordertotal'];	
									$taxes		=	$row['order_vat_tax'];
									$netsale	=	($sales-$taxes);
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;									
									$nettot		+=	$netsale;  
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td><?=$row['odate']?></td>
					<td class='values'><?=number_format($netsale,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						}				   
					}
				  ?>
				  <tr>
					<td colspan='4'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
				 <?php } ?>
			  </div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
				<?php
				}
		  }else if($data['display_type']=='Monthly'){
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$months		=	$this->get_months($sdate,$edate);
				$cntday		=	count($months);			
				if($cntday>0){
					$monthtot	=	0;
			?>
			<div id="article_tabs">
			<ul>
			  <li><a href="#trefunds">Refunds</a></li>
			  <li><a href="#tdetails">Details</a></li>			 
			</ul> 
			<div id="trefunds">
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Month</th>
					<th>Number Of Orders</th>			
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp		=	explode(" ",$months[$i]);
						$m			=	$exp[0];
						$y			=   $exp[2];
						$dispmon	=   $exp[1]." ".$exp[2];														

						$sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt FROM 3bit_orders WHERE MONTH(shipped_date)='".$m."' AND YEAR(shipped_date)='".$y."' AND (status ='Refunded')");
						if(count($sql)>0){	
							 $ordcnt	=	$sql['ordcnt'];
							 $monthtot	+=	$ordcnt;

							 if($ordcnt>0){
				?>
					<tr>
						 <td><?php echo $dispmon?></td>
						 <td><?php echo $ordcnt;?></td>
					</tr>
				<?php		}
						}

					}
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $monthtot;?></b></td>
					</tr>					
					</table>
					</div>
					<div id='tdetails'>
			  <?php if($cntday>0){ 
						$sales		=	0;
						$taxes		=	0;
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
						$shiptot	=	0;
					
				 ?>
				   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
					<thead>
					<tr>
						<th>Order Id</th>
						<th>Invoice Number</th>
						<th>Customer Name</th>
						<th>Shipping Country</th>
						<th>Refund Date</th>								
						<th>Net Amount</th>		
						<th>Taxes</th>		
						<th>Refund Amount</th>		
					</tr>
					</thead>
				  <?php for($i=0;$i<$cntday;$i++){
						    $exp		=	explode(" ",$months[$i]);
							$m			=	$exp[0];
							$y			=   $exp[2];
							$dispmon	=   $exp[1]." ".$exp[2]; 

							$sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND MONTH(o.shipped_date)='".$m."' AND YEAR(o.shipped_date)='".$y."'";								

							$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

							$res		=	$this->_db->fetchAll($sql);

							$salerows	=	count($res);

							$weekrows	+=	$salerows;					

							if($salerows>0){

								foreach($res as $row){ 
									$sales		=	$row['ordertotal'];	
									$taxes		=	$row['order_vat_tax'];
									$netsale	=	($sales-$taxes);
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;									
									$nettot		+=	$netsale;  						
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>	
					<td><?=$row['odate']?></td>
					<td class='values'><?=number_format($netsale,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						}

					}
				?>
				<tr>
					<td colspan='4'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>						
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
			  <?php } ?>
			</div>
		<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
				<?php
				}
		  }else if($data['display_type']=='Yearly'){			  			
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$years		=	$this->get_years($sdate,$edate);
				$cntday		=	count($years);
				if($cntday>0){
					$yeartot	=	0;
			?>
			<div id="article_tabs">
				<ul>
				  <li><a href="#trefunds">Refunds</a></li>
				  <li><a href="#tdetails">Details</a></li>			 
				</ul> 
				<div id="trefunds">
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
					<tr>
						<th>Year</th>
						<th>Number Of Orders</th>			
					</tr>
				</thead>
			<?php
				for($i=0;$i<$cntday;$i++){					
					$y			=   $years[$i];
				    $sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt FROM 3bit_orders WHERE YEAR(shipped_date)='".$y."' AND (status ='Refunded')");
					if(count($sql)>0){	
							 $ordcnt	=	$sql['ordcnt'];
							 $yeartot	+=	$ordcnt;
							 if($ordcnt>0){
				?>
					<tr>
						 <td><?php echo $y?></td>
						 <td><?php echo $ordcnt;?></td>
					</tr>
				<?php		}
					
					}

				}
			?>
				<tr>
					 <td><b>Totals</b></td>
					 <td><b><?php echo $yeartot;?></b></td>
				</tr>				
				</table>
			   </div>
			   <div id="tdetails">
			<?php if($cntday>0){
					$sales		=	0;
					$taxes		=	0;
					$salestot	=	0;
					$taxtot		=	0;						
					$nettot		=	0; 
					$shiptot	=	0;
				
			 ?>
			   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
			   <thead>
				<tr>
					<th>Order Id</th>
					<th>Invoice Number</th>
					<th>Customer Name</th>
					<th>Shipping Country</th>
					<th>Refund Date</th>
					<th>Net Amount</th>						
					<th>Taxes</th>						
					<th>Refund Amount</th>						
				</tr>
			 </thead>
			  <?php for($i=0;$i<$cntday;$i++){
					   $y		=   $years[$i];
					   $sql		= "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND YEAR(o.shipped_date)='".$y."'";								
				       $sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

					   $res		=	$this->_db->fetchAll($sql);

					   $salerows	=	count($res);

					   $weekrows	+=	$salerows;					

					   if($salerows>0){

							foreach($res as $row){ 
								$sales		=	$row['ordertotal'];	
								$taxes		=	$row['order_vat_tax'];
								$netsale	=	($sales-$taxes);
								$salestot	+=	$sales;
								$taxtot		+=	$taxes;									
								$nettot		+=	$netsale; 						
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td><?=$row['odate']?></td>
					<td class='values'><?=number_format($netsale,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						} 
					}
				?>
				<tr>
					<td colspan='4'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
			<?php	} ?>
			</div>
		<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
			<?php
			 }
		  }
	  }else if($data['report_type']=='RefundsByProduct'){

	?>
		<div id="article_tabs">
				<ul>
				  <li><a href="#tproducts">Products</a></li>				 
				</ul> 
				<div id="tproducts">				
				<?php ///////////////////////////// FOR products //////////////////////////////////////////

					$sqlpro			=  "SELECT sum( d.quantity ) qty,count(o.order_id) as ordcnt,d.product_id, p.product_name,p.product_code,p.style,p.size,pc.procat_title,o.order_id FROM `3bit_credit_details` d, `3bit_products` p,`3bit_orders` o,3bit_product_category pc WHERE p.product_id = d.product_id AND o.order_id = d.order_id AND p.procat_id = pc.procat_id AND (o.status ='Refunded') " ;

					if(trim($end_date) != "" && trim($start_date) != ""){	 	
						$monSArr	=	explode("/",$start_date);
						$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
						$monEArr	=	explode("/",$end_date);
						$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
						$sqlpro		.=  " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
					}	

					$sqlpro			.=	" GROUP BY p.product_id ORDER BY qty DESC , p.product_name ASC";

					if($data['display_type']=='TOP 5')
						$sqlpro		.=	" LIMIT 0,5";
					else if($data['display_type']=='TOP 10')
						$sqlpro		.=	" LIMIT 0,10";
					else if($data['display_type']=='TOP 15')
						$sqlpro		.=	" LIMIT 0,15";		

					$res	=	$this->_db->fetchAll($sqlpro);

					if(count($res)>0){		
						$sumcnt		=	0;
						$sumuktot	=	0;						

				?>	<h4><?=$data['display_type']?> Products</h4>
					<table width="100%" cellspacing="0" cellpadding="0" class="display">
					<thead>
					<tr>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Type</th>			
						<th>Number Of Refunds</th>
						<th>Product Refund Amount</th>			
					</tr>
					</thead>
						<?php foreach($res as $row){								
								$type		=	$row['procat_title'];
								$sumqty		+=	$row['qty'];
								$sumcnt		+=	$row['ordcnt'];	
								if($row['style'] != '')
								  $item_model		=	"&nbsp;(".stripslashes($row['style']).")";
								else if($row['size'] != '')
								  $item_model		=	"&nbsp;(".stripslashes($row['size']).")";

								$sel		=	"SELECT o.order_id,o.order_vat_tax,d.* FROM 3bit_orders o,3bit_credit_details d WHERE o.order_id=d.order_id AND d.product_id='".$row['product_id']."' AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."' AND o.status='Refunded'  ";

								if($data['display_type']=='TOP 5')
									$sel		.=	" LIMIT 0,5";
								else if($data['display_type']=='TOP 10')
									$sel		.=	" LIMIT 0,10";
								else if($data['display_type']=='TOP 15')
									$sel		.=	" LIMIT 0,15";

								$selOrder	=	$this->_db->fetchRow($sel);

								$numrows	=	count($selOrder);

								if($numrows>0){
									$totvat		=	0;
									$totprice	=	0;
									foreach($selOrder as $result){

										$ordid		=	$result['order_id'];

										$quanty		=	$result['quantity'];	

										$price		=	$result['price'];

										$line_price	=	$quanty*$price;						

										$totprice	+=	$line_price;							
									}
								}				    					
								$currency	=	"&pound;";
								$sumuktot	+=	$totprice;
						?>
					<tr>
						<td><?php echo $row['product_id'];?></td>
						<td><?php echo $row['product_name'].$item_model;?></td>
						<td><?php echo $type;?></td>			
						<td><?php echo $row['ordcnt'];?></td>
						<td align="right"><?=$currency?><?php echo number_format($totprice,'2','.','');?></td>
					</tr>
						<?php } ?>
					<tr>
						<td colspan='3'>&nbsp;<b>Totals</b></td>			
						<td><B><?php echo $sumcnt;?></B></td>			
						<td align="right"><B>&pound;<?php echo number_format($sumuktot,2,'.','');?></B></td>
					</tr>	
					</table>
			<?php }	?>								
			</div>					
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
	<?php }else if($data['report_type']=='SalesByDate'){

		if($data['display_type']=='Daily'){
	?>
		<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>		 	 
			</ul> 
			<div id="tsales">					
				 <?php 	   //////////// For UK SALES ///////////////////////
					$sql = "SELECT DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate,sum(o.ordertotal)as totals,sum(o.order_vat_tax)as taxes FROM `3bit_orders` o WHERE (o.status ='Shipped' OR o.status ='Refunded') ";

					// Concate with query //

					if(trim($end_date) != "" && trim($start_date) != ""){	 	
						$monSArr	=	explode("/",$start_date);
						$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
						$monEArr	=	explode("/",$end_date);
						$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
						$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
					}		

					$sql		.=	" GROUP BY DATE_FORMAT(o.shipped_date,'%d/%m/%Y') ORDER BY o.shipped_date ASC ";	

					$res		=	$this->_db->fetchAll($sql);

					$daterows	=	count($res);

					if($daterows>0){
						foreach($res as $rows){							
							$sale_date[]	=	$rows['odate'];
						}
					}

					if($daterows>0){
						$daytotal	=	0;
						$salestot	=	0;
						$taxtot		=	0;
						$disctot	=	0;
						$nettot		=	0;

				?>
					<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
						<thead>
						<tr>
							<th>Day</th>
							<th>Number Of Orders</th>
							<th>Net Sales</th>
							<th>Taxes</th>	
							<th>Order Discounts</th>	
							<th>Gross Sales</th>
						</tr>
						</thead>
						<?php  for($k=0;$k<$daterows;$k++){ 
									// For Shipped Orders//	
									$stot		=	0;
									$stax		=	0;
									$sdisc		=	0;
									$scnt		=	0;

									$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt FROM 3bit_orders WHERE status='Shipped' AND  DATE_FORMAT(shipped_date,'%d/%m/%Y')='".$sale_date[$k]."' GROUP BY DATE_FORMAT(shipped_date,'%d/%m/%Y') ");	
									if(count($selTot)>0){
									   $stot	=	$selTot['ordtot'];	
									   $stax	=	$selTot['ordtax'];
									   $scnt	=	$selTot['ocnt'];
									}

									$selDisc	=	$this->_db->fetchAll("SELECT distinct(od.discount)as orddisc FROM 3bit_orders o,3bit_order_details od WHERE DATE_FORMAT(o.shipped_date,'%d/%m/%Y')= '".$sale_date[$k]."' AND o.order_id=od.order_id AND (o.status ='Shipped') GROUP BY DATE_FORMAT(o.shipped_date,'%d/%m/%Y')");								
									if(count($selDisc)>0){ 
										foreach($selDisc as $rw){											
											$sdisc	+=	$rw['orddisc'];
										}
									}

									// For Refunded Orders
									$rtot		=	0;
									$rtax		=	0;
									$rdisc		=	0;
									$rcnt		=	0;

									$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt FROM 3bit_orders WHERE status='Refunded' AND  DATE_FORMAT(shipped_date,'%d/%m/%Y')='".$sale_date[$k]."' GROUP BY DATE_FORMAT(shipped_date,'%d/%m/%Y') ");	
									if(count($selTot)>0){
									   $rtot	=	$selTot['ordtot'];	
									   $rtax	=	$selTot['ordtax'];	
									   $rcnt	=	$selTot['ocnt'];	
									}								

									//////////////// Total Orders//									
									$order_count=	$scnt+$rcnt;   
									$daytotal	+=	$order_count;										
									$sales		=	$stot+$rtot;
									$taxes		=	$stax-$rtax;
									$discount	=	$sdisc+$rdisc;
									
									$netsale	=	($sales-$taxes)+$discount;
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;
									$disctot	+=	$discount;
									$nettot		+=	$netsale;  
						?>

								<tr>
									<td><?php echo $sale_date[$k];?></td>
									<td><?php echo $order_count;?></td>
									<td>&pound;<?php echo number_format($netsale,'2','.','')?></td>
									<td>&pound;<?php echo number_format($taxes,'2','.','');?></td>
									<td>&pound;<?php echo number_format($discount,'2','.','');?></td>
									<td>&pound;<?php echo number_format($sales,'2','.','');?></td>
								</tr>
				
						<?php  }	?>	
						 <tr>
							<td><b>Totals</b></td>
							<td><b><?php echo $daytotal;?></b></td>	
							<td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>
							<td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
							<td><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>	
							<td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
						</tr>
					</table>
			<?php	}?>
			  </div>
			  <div id='tdetails'>
				<?php $sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped' OR o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country ";

					// Concate with query //

					if(trim($end_date) != "" && trim($start_date) != ""){	 	
						$monSArr	=	explode("/",$start_date);
						$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
						$monEArr	=	explode("/",$end_date);
						$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
						$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
					}		

					$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

					$res		=	$this->_db->fetchAll($sql);

					$salerows	=	count($res);

					if($salerows>0){
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
						$disctot	=	0;
				?>
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Order Id</th>
					<th>Invoice Number</th>
					<th>Customer Name</th>
					<th>Shipping Country</th>
					<th>Shipping Cost</th>
					<th>Net Sales</th>
					<th>Discount</th>
					<th>Taxes</th>			
					<th>Gross Sales</th>		
				</tr>
				</thead>
				<?php foreach($res as $row){ 
						$sales		=	$row['ordertotal'];	
						$taxes		=	$row['order_vat_tax'];
						$discount	=	0;
						if($row['status']=='Shipped'){
						   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
						   if(count($selShip)>0){
							  $shipping	=	$selShip['shipping'];
							  $discount	=	$selShip['discount'];
						   }					  
						}else if($row['status']=='Refunded'){
						   $shipping	=	0;						   					  
						}
						$netsales	=	($sales-$taxes)+$discount;						
						$salestot	+=	$sales;
						$taxtot		+=	$taxes;						
						$nettot		+=	$netsales; 
						$shiptot	+=	$shipping;
						$disctot	+=	$discount;
				?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td class='values'><?=number_format($shipping,'2','.','');?></td>
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($discount,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php }	?>
				 <tr>
					<td colspan='3'><b>Totals</b></td>
					<td><b><?php echo $salerows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($shiptot,'2','.','');?></b></td>
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
				<?php } ?>
			  </div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>   
	  <?php }else if($data['display_type']=='Weekly'){ ?>	
	  <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>		 	 
			</ul> 
			<div id="tsales">	
				<?php 
				$sdate	=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate	=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom = strtotime($sdate, 0);
				$dateto  = strtotime($edate, 0);

			    $datearr=	$this->get_days ($sdate,$edate);
				$cntday	=	count($datearr);
				if($cntday>0){
					$weektot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Week Starting Date</th>
					<th>Number Of Orders</th>
					<th>Net Sales</th>
					<th>Taxes</th>			
					<th>Order Discounts</th>	
					<th>Gross Sales</th>		
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp	=	explode("/",$datearr[$i]);
						$m		=	$exp[1];
						$d		=	$exp[0];
						$y		=	$exp[2];
						$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

						// For Shipped Orders//	
						$stot		=	0;
						$stax		=	0;
						$sdisc		=	0;
						$scnt		=	0;

						$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt FROM 3bit_orders WHERE status='Shipped' AND WEEK(shipped_date,1)='".$g."' AND YEAR(shipped_date)='".$y."' GROUP BY WEEK(shipped_date,1) ");							
						if(count($selTot)>0){
						   $stot	=	$selTot['ordtot'];	
						   $stax	=	$selTot['ordtax'];
						   $scnt	=	$selTot['ocnt'];
						}
						$selDisc	=	$this->_db->fetchAll("SELECT distinct(od.discount)as orddisc FROM 3bit_orders o,3bit_order_details od WHERE WEEK(shipped_date,1)='".$g."' AND YEAR(shipped_date)='".$y."' AND o.order_id=od.order_id AND (o.status ='Shipped') GROUP BY DATE_FORMAT(shipped_date,'%d/%m/%Y') ");			
						if(count($selDisc)>0){ 
							foreach($selDisc as $rw){											
								$sdisc	+=	$rw['orddisc'];
							}
						}

						// For Refunded Orders
						$rtot		=	0;
						$rtax		=	0;
						$rdisc		=	0;
						$rcnt		=	0;
						$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt,WEEK(shipped_date) FROM 3bit_orders WHERE status='Refunded' AND WEEK(shipped_date,1)='".$g."' AND YEAR(shipped_date)='".$y."' GROUP BY WEEK(shipped_date,1)");	
						if(count($selTot)>0){
						   $rtot	=	$selTot['ordtot'];
						   $rtax	=	$selTot['ordtax'];
						   $rcnt	=	$selTot['ocnt'];
						}								

						//////////////// Total Orders//							
						$order_count=	$scnt+$rcnt;   						
						$sales		=	$stot+$rtot;
						$taxes		=	$stax-$rtax;
						$discount	=	$sdisc+$rdisc; 
						
						$netsale	=	($sales-$taxes)+$discount;						

						if($order_count>0){
							$weektot	+=	$order_count;				
							$salestot	+=	$sales;
							$taxtot		+=	$taxes;
							$disctot	+=	$discount;
							$nettot		+=	$netsale;
				?>
					<tr>
						 <td><?php echo $datearr[$i]?></td>
						 <td><?php echo $order_count;?></td>
						 <td>&pound;<?php echo number_format($netsale,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($taxes,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($discount,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($sales,'2','.','');?></td>	 
					</tr>
				<?php		}
						} 					  
				?>
					<tr>
						<td><b>Totals</b></td>
						<td><b><?php echo $weektot;?></b></td>
						<td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>
						<td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
						<td><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>			
						<td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>		
					</tr>	
				 </table>
				<?php } ?>
				</div>
				<div id='tdetails'>
				 <?php if($cntday>0){ 
						$sales		=	0;
						$taxes		=	0;
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
						$shiptot	=	0;
						$disctot	=	0;
					
				 ?>
				   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				   <thead>
					<tr>
						<th>Order Id</th>
						<th>Invoice Number</th>
						<th>Customer Name</th>
						<th>Shipping Country</th>
						<th>Shipping Cost</th>
						<th>Net Sales</th>
						<th>Discount</th>
						<th>Taxes</th>			
						<th>Gross Sales</th>		
					</tr>
					</thead>
				  <?php for($i=0;$i<$cntday;$i++){
							$exp	=	explode("/",$datearr[$i]);
							$m		=	$exp[1];
							$d		=	$exp[0];
							$y		=	$exp[2];
							$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

							$sql	=	"SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped' OR o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND WEEK(o.shipped_date,1)='".$g."' AND YEAR(o.shipped_date)='".$y."'";								

							$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

							$res		=	$this->_db->fetchAll($sql);

							$salerows	=	count($res);

							$weekrows	+=	$salerows;					

							if($salerows>0){

								foreach($res as $row){ 
									$sales		=	$row['ordertotal'];	
									$taxes		=	$row['order_vat_tax'];
									$discount	=	0;
									if($row['status']=='Shipped'){
									   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
									   if(count($selShip)>0){
										  $shipping	=	$selShip['shipping'];
										  $discount	=	$selShip['discount'];
									   }					  
									}else if($row['status']=='Refunded'){
									   $shipping	=	0;						   					  
									}
									$netsales	=	($sales-$taxes)+$discount;						
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;						
									$nettot		+=	$netsales; 
									$shiptot	+=	$shipping;
									$disctot	+=	$discount;
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td class='values'><?=number_format($shipping,'2','.','');?></td>
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($discount,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						}				   
					}
				  ?>
				  <tr>
					<td colspan='3'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($shiptot,'2','.','');?></b></td>
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
				 <?php } ?>
			  </div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
	  <?php }else if($data['display_type']=='Monthly'){ ?>
	   <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>		 	 
			</ul> 
			<div id="tsales">	
			<?php
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$months		=	$this->get_months($sdate,$edate);
				$cntday		=	count($months);			
				if($cntday>0){
					$monthtot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Month</th>
					<th>Number Of Orders</th>
					<th>Net Sales</th>
					<th>Taxes</th>			
					<th>Order Discounts</th>	
					<th>Gross Sales</th>	
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp		=	explode(" ",$months[$i]);
						$m			=	$exp[0];
						$y			=   $exp[2];
						$dispmon	=   $exp[1]." ".$exp[2];
						
						// For Shipped Orders//	
						$stot		=	0;
						$stax		=	0;
						$sdisc		=	0;
						$scnt		=	0;

						$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt FROM 3bit_orders WHERE status='Shipped' AND MONTH(shipped_date)='".$m."' AND YEAR(shipped_date)='".$y."' GROUP BY MONTH(shipped_date) ");
						if(count($selTot)>0){
						   $stot	=	$selTot['ordtot'];	
						   $stax	=	$selTot['ordtax'];
						   $scnt	=	$selTot['ocnt'];
						}

						$selDisc	=	$this->_db->fetchAll("SELECT distinct(od.discount)as orddisc FROM 3bit_orders o,3bit_order_details od WHERE MONTH(shipped_date)='".$m."' AND YEAR(shipped_date)='".$y."' AND o.order_id=od.order_id AND (o.status ='Shipped') GROUP BY DATE_FORMAT(shipped_date,'%d/%m/%Y') ");				
						if(count($selDisc)>0){ 
							foreach($selDisc as $rw){											
								$sdisc	+=	$rw['orddisc'];
							}
						}

						// For Refunded Orders
						$rtot		=	0;
						$rtax		=	0;
						$rdisc		=	0;
						$rcnt		=	0;
						$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt FROM 3bit_orders WHERE status='Refunded' AND MONTH(shipped_date)='".$m."' AND YEAR(shipped_date)='".$y."' GROUP BY MONTH(shipped_date) ");		
						if(count($selTot)>0){
						   $rtot	=	$selTot['ordtot'];	
						   $rtax	=	$selTot['ordtax'];
						   $rcnt	=	$selTot['ocnt'];
						}								

						//////////////// Total Orders//							
						$order_count=	$scnt+$rcnt;   
						$monthtot	+=	$order_count;										
						$sales		=	$stot+$rtot;
						$taxes		=	$stax-$rtax;
						$discount	=	$sdisc+$rdisc; 	
						
						$netsale	=	($sales-$taxes)+$discount;						
						if($order_count>0){
							$salestot	+=	$sales;
							$taxtot		+=	$taxes;
							$disctot	+=	$discount;
							$nettot		+=	$netsale;
				?>
					<tr>
						 <td><?php echo $dispmon?></td>
						 <td><?php echo $order_count;?></td>
						 <td>&pound;<?php echo number_format($netsale,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($taxes,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($discount,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($sales,'2','.','');?></td>	 
					</tr>
				<?php	}

					}
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $monthtot;?></b></td>
						 <td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>	
						 <td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
						 <td><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>
						 <td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>	 						
					</tr>	
					</table>
				<?php } ?>
			</div>
			<div id='tdetails'>
			  <?php if($cntday>0){ 
						$sales		=	0;
						$taxes		=	0;
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
						$shiptot	=	0;
						$disctot	=	0;
					
				 ?>
				   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				   <thead>
					<tr>
						<th>Order Id</th>
						<th>Invoice Number</th>
						<th>Customer Name</th>
						<th>Shipping Country</th>
						<th>Shipping Cost</th>
						<th>Net Sales</th>
						<th>Discount</th>
						<th>Taxes</th>			
						<th>Gross Sales</th>		
					</tr>
					</thead>
				  <?php for($i=0;$i<$cntday;$i++){
						    $exp		=	explode(" ",$months[$i]);
							$m			=	$exp[0];
							$y			=   $exp[2];
							$dispmon	=   $exp[1]." ".$exp[2]; 

							$sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped' OR o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND MONTH(o.shipped_date)='".$m."' AND YEAR(o.shipped_date)='".$y."'";								

							$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

							$res		=	$this->_db->fetchAll($sql);

							$salerows	=	count($res);

							$weekrows	+=	$salerows;					

							if($salerows>0){

								foreach($res as $row){ 
									$sales		=	$row['ordertotal'];	
									$taxes		=	$row['order_vat_tax'];
									$discount	=	0;
									if($row['status']=='Shipped'){
									   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
									   if(count($selShip)>0){
										  $shipping	=	$selShip['shipping'];
										  $discount	=	$selShip['discount'];
									   }					  
									}else if($row['status']=='Refunded'){
									   $shipping	=	0;						   					  
									}
									$netsales	=	($sales-$taxes)+$discount;						
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;						
									$nettot		+=	$netsales; 
									$shiptot	+=	$shipping;
									$disctot	+=	$discount;
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td class='values'><?=number_format($shipping,'2','.','');?></td>
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($discount,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						}

					}
				?>
				<tr>
					<td colspan='3'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($shiptot,'2','.','');?></b></td>
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
			  <?php } ?>
			</div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>	   
	<?php }else if($data['display_type']=='Yearly'){	?>
	 <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>		 	 
			</ul> 
			<div id="tsales">	
			<?php
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$years		=	$this->get_years($sdate,$edate);
				$cntday		=	count($years);
				if($cntday>0){
					$yeartot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Year</th>
					<th>Number Of Orders</th>
					<th>Net Sales</th>
					<th>Taxes</th>			
					<th>Order Discounts</th>	
					<th>Gross Sales</th>
				</tr>
				</thead>
			<?php
				for($i=0;$i<$cntday;$i++){					
					$y			=   $years[$i];

					// For Shipped Orders//	
					$stot		=	0;
					$stax		=	0;
					$sdisc		=	0;
					$scnt		=	0;

					$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt FROM 3bit_orders WHERE status='Shipped' AND YEAR(shipped_date)='".$y."' GROUP BY YEAR(shipped_date) ");							
					if(count($selTot)>0){
					   $stot	=	$selTot['ordtot'];	
					   $stax	=	$selTot['ordtax'];
					   $scnt	=	$selTot['ocnt'];
					}
					$selDisc	=	$this->_db->fetchAll("SELECT distinct(od.discount)as orddisc FROM 3bit_orders o,3bit_order_details od WHERE YEAR(shipped_date)='".$y."' AND o.order_id=od.order_id AND (o.status ='Shipped') GROUP BY DATE_FORMAT(shipped_date,'%d/%m/%Y') ");				

					if(count($selDisc)>0){ 
						foreach($selDisc as $rw){											
							$sdisc	+=	$rw['orddisc'];
						}
					}

					// For Refunded Orders
					$rtot		=	0;
					$rtax		=	0;
					$rdisc		=	0;
					$rcnt		=	0;
					$selTot		=	$this->_db->fetchRow("SELECT sum(ordertotal) as ordtot,sum(order_vat_tax)as ordtax,count(order_id)as ocnt FROM 3bit_orders WHERE status='Refunded' AND YEAR(shipped_date)='".$y."' GROUP BY YEAR(shipped_date) ");						

					if(count($selTot)>0){
					   $rtot	=	$selTot['ordtot'];		
					   $rtax	=	$selTot['ordtax'];	
					   $rcnt	=	$selTot['ocnt'];	
					}								

					//////////////// Total Orders//							
					$order_count=	$scnt+$rcnt;   															
					$sales		=	$stot+$rtot;
					$taxes		=	$stax-$rtax;
					$discount	=	$sdisc+$rdisc;
					
					$netsale	=	($sales-$taxes)+$discount; 					
					if($order_count>0){
						$yeartot	+=	$order_count;
						$salestot	+=	$sales;
						$taxtot		+=	$taxes;
						$disctot	+=	$discount;
						$nettot		+=	$netsale;
				?>
					<tr>
						 <td><?php echo $y?></td>
						 <td><?php echo $order_count;?></td>
						 <td>&pound;<?php echo number_format($netsale,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($taxes,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($discount,'2','.','');?></td>
						 <td>&pound;<?php echo number_format($sales,'2','.','');?></td>	  
					</tr>
				<?php	}				
					
					}
				?>
				<tr>
					 <td><b>Totals</b></td>
					 <td><b><?php echo $yeartot;?></b></td>
					 <td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>	
					 <td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
					 <td><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>	
					 <td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td> 
				</tr>	
				</table>
			<?php }  ?>
			</div>
			<div id="tdetails">
			<?php if($cntday>0){
					$sales		=	0;
					$taxes		=	0;
					$salestot	=	0;
					$taxtot		=	0;						
					$nettot		=	0; 
					$shiptot	=	0;
					$disctot	=	0;
				
			 ?>
			   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
			   <thead>
				<tr>
					<th>Order Id</th>
					<th>Invoice Number</th>
					<th>Customer Name</th>
					<th>Shipping Country</th>
					<th>Shipping Cost</th>
					<th>Net Sales</th>
					<th>Discount</th>
					<th>Taxes</th>			
					<th>Gross Sales</th>		
				</tr>
			   </thead>
			  <?php for($i=0;$i<$cntday;$i++){
					   $y		=   $years[$i];
					   $sql		= "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped' OR o.status ='Refunded') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND YEAR(o.shipped_date)='".$y."'";								
				       $sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

					   $res		=	$this->_db->fetchAll($sql);

					   $salerows	=	count($res);

					   $weekrows	+=	$salerows;					

					   if($salerows>0){

							foreach($res as $row){ 
								$sales		=	$row['ordertotal'];	
								$taxes		=	$row['order_vat_tax'];
								$discount	=	0;
								if($row['status']=='Shipped'){
								   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
								   if(count($selShip)>0){
									  $shipping	=	$selShip['shipping'];
									  $discount	=	$selShip['discount'];
								   }					  
								}else if($row['status']=='Refunded'){
								   $shipping	=	0;						   					  
								}
								$netsales	=	($sales-$taxes)+$discount;						
								$salestot	+=	$sales;
								$taxtot		+=	$taxes;						
								$nettot		+=	$netsales; 
								$shiptot	+=	$shipping;
								$disctot	+=	$discount;
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td class='values'><?=number_format($shipping,'2','.','');?></td>
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($discount,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						} 
					}
				?>
				<tr>
					<td colspan='3'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($shiptot,'2','.','');?></b></td>
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($disctot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
			<?php	} ?>
			</div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
	<?php
		  }
	}else if($data['report_type']=='SalesByProduct'){

	?>
	  <div id="article_tabs">
			<ul>
			  <li><a href="#tproducts">Products</a></li>			 
			</ul> 
			<div id="tproducts">	
			<?php  	////////////////////////////// For sales ////////////////////////////////////////////////////////////
						$sqlpro			=  "SELECT sum( d.quantity ) qty,count(o.order_id) as ordcnt,d.product_id,p.product_name,p.product_code,p.style,p.size,pc.procat_title,o.order_id FROM `3bit_order_details` d, `3bit_products` p, `3bit_orders` o,3bit_product_category pc WHERE p.product_id = d.product_id AND o.order_id = d.order_id AND pc.procat_id=p.procat_id AND (o.status ='Shipped') " ;

						if(trim($end_date) != "" && trim($start_date) != ""){	 	
							$monSArr	=	explode("/",$start_date);
							$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
							$monEArr	=	explode("/",$end_date);
							$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
							$sqlpro		.=  " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
						}	

						$sqlpro			.=	" GROUP BY p.product_id ORDER BY qty DESC , p.product_name ASC";



						if($data['display_type']=='TOP 5')
							$sqlpro		.=	" LIMIT 0,5";
						else if($data['display_type']=='TOP 10')
							$sqlpro		.=	" LIMIT 0,10";
						else if($data['display_type']=='TOP 15')
							$sqlpro		.=	" LIMIT 0,15";
						
						//echo $sqlpro;

						$res	=	$this->_db->fetchAll($sqlpro);

						if(count($res)>0){
							$sumqty		=	0;
							$sumcnt		=	0;
							$sumuktot	=	0;
							$sumukvat	=	0;
					?><h4><?=$data['display_type']?> Products</h4>
					<table width="100%" cellspacing="0" cellpadding="0" class="display">
					<thead>
					<tr>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Type</th>
						<th>SKU</th>
						<th>Total Quantity</th>
						<th>Number Of Orders</th>
						<th>Taxes</th>
						<th>Net Sales</th>
					</tr>
					</thead>
					<?php foreach($res as $row){
							$varid		=	$row['variation_id'];
							$type		=	$row['procat_title'];
							
							$sumqty		+=	$row['qty'];
							$sumcnt		+=	$row['ordcnt'];
							
							$item_model	=	'';
							
							if($row['style'] != '')
							  $item_model		=	"&nbsp;(".stripslashes($row['style']).")";
							else if($row['size'] != '')
							  $item_model		=	"&nbsp;(".stripslashes($row['size']).")";
							
						
							$sel		=	"SELECT o.order_id,o.order_vat_tax,d.* FROM 3bit_orders o,3bit_order_details d WHERE o.order_id=d.order_id AND d.product_id='".$row['product_id']."' AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."' AND (o.status ='Shipped') ";

							if($data['display_type']=='TOP 5')
								$sel		.=	" LIMIT 0,5";
							else if($data['display_type']=='TOP 10')
								$sel		.=	" LIMIT 0,10";
							else if($data['display_type']=='TOP 15')
								$sel		.=	" LIMIT 0,15";

							$selOrder	=	$this->_db->fetchAll($sel);

							$numrows	=	count($selOrder);

							$totvat		=	0;

							$totprice	=	0;

							if($numrows>0){
								
								foreach($selOrder as $result){

									$ordid		=	$result['order_id'];

									$vattax		=	$result['order_vat_tax'];

									$quanty		=	$result['quantity'];	

									$price		=	$result['price'];

									$line_price	=	$quanty*$price;

									if($vattax>0){

										$vat	=	$this->findProductvat($result['product_id']);

										$totvat		+=	$quanty*$vat;								
									}
									$totprice	+=	$line_price;														
								}
							}
											
							$currency	=	"&pound;";
							$sumukvat		+=	$totvat;
							$sumuktot		+=  $totprice;
							

						?>
						<tr>
							<td><?php echo $row['product_id'];?></td>
							<td><?php echo $row['product_name'].$item_model;?></td>
							<td><?php echo $type;?></td>
							<td><?php echo $row['product_code'];?></td>
							<td><?php echo $row['qty'];?></td>
							<td><?php echo $row['ordcnt'];?></td>
							<td align="right"><?=$currency?><?php echo number_format($totvat,'2','.','');?></td>
							<td align="right"><?=$currency?><?php echo number_format($totprice,'2','.','');?></td>
						</tr>
					<?php	} ?>
						<tr>
							<td colspan='4'>&nbsp;<b>Totals</b></td>
							<td><B><?php echo $sumqty;?></B></td>
							<td><B><?php echo $sumcnt;?></B></td>
							<td align="right"><B>&pound;<?php echo number_format($sumukvat,2,'.','');?></B></td>
							<td align="right"><B>&pound;<?php echo number_format($sumuktot,2,'.','');?></B></td>
						</tr>	
					</table>
			<?php }		?>				
			</div>			
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div> 
	<?php }else if($data['report_type']=='SalesByPaymentType'){	 ?>
		<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			</ul> 
			<div id="tsales">			
	   <?php
			$sqlpro		=	"SELECT payment_method,count(order_id) as cnt,sum(ordertotal)as sales FROM 3bit_orders WHERE (status = 'Shipped' OR status = 'Refunded') ";

			if(trim($end_date) != "" && trim($start_date) != ""){	 	
				$monSArr	=	explode("/",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("/",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
				$sqlpro		.=  " AND DATE(shipped_date)  <= '".$newEDate."' AND DATE(shipped_date) >= '".$newSDate."'  "; 
			}
			
			$sqlpro		.=  " GROUP BY payment_method ORDER BY cnt DESC";

			$res		=	$this->_db->fetchAll($sqlpro);

			if(count($res)>0){
				$cnttot	=	0;
				$saletot =	0;

		?> <table width="100%" cellspacing="0" cellpadding="0" class="display">
			<thead>
			<tr>
				<th>Payment Type</th>			
				<th>Number Of Orders</th>
				<th>Gross Sales</th>
			</tr>
			</thead>
			<?php foreach($res as $row){
					  $cnttot	+=	$row['cnt'];
					  $saletot	+=	$row['sales']; 
			?>
			 <tr>
				  <td><?=$row['payment_method']?></td>
				  <td><?=$row['cnt']?></td>
				  <td>&pound;<?=number_format($row['sales'],'2','.','')?></td>
			 </tr>
			<?php } ?>	   
			<tr>
				<td><b>Totals</b></td>
				<td><b><?=$cnttot?></b></td>
				<td><b>&pound;<?=number_format($saletot,'2','.','')?></b></td>
			</tr>
		 </table>
		<?php } ?>
		</div>	
		  <p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	 </div>		
	<?php }else if($data['report_type']=='SalesTaxByDate'){

		 if($data['display_type']=='Daily'){
	?>
		  <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			</ul> 
			<div id="tsales">
				<?php	$sql = "SELECT count(o.order_id )as ordcnt,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate,sum(o.ordertotal)as totals,sum(o.order_vat_tax)as taxes  FROM `3bit_orders` o WHERE (o.status ='Shipped' OR o.status ='Refunded') ";

				// Concate with query //

				if(trim($end_date) != "" && trim($start_date) != ""){	 	
					$monSArr	=	explode("/",$start_date);
					$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
					$monEArr	=	explode("/",$end_date);
					$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
					$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
				}		

				$sql	.=	" GROUP BY DATE_FORMAT(o.shipped_date,'%d/%m/%Y') ORDER BY o.shipped_date ASC ";	

				$res	=	$this->_db->fetchAll($sql);

				if(count($res)>0){				
					$daytotal	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;

				?>
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
					<tr>
						<th>Day</th>
						<th>Number Of Orders</th>			
						<th>Net Sales</th>									
						<th>Country Taxes</th>				
						<th>Gross Sales</th>		
					</tr>
				 </thead>
				<?php foreach($res as $row){ 							
							$discount	=	0;
							$sales		=	$row['totals'];
							$taxes		=	$row['taxes']; 
							$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE DATE_FORMAT(o.shipped_date,'%d/%m/%Y')='".$row['odate']."' AND o.order_id=od.order_id AND (o.status ='Shipped' OR o.status ='Refunded') GROUP BY o.order_id");				

							if(count($selOrders)>0){ 
								foreach($selOrders as $rw){									
									$discount	+=	$rw['discount'];
								}
							}
							$netsale	=	($sales-$taxes)+$discount;

							$salestot	+=	$sales;
							$taxtot		+=	$taxes;
							$disctot	+=	$discount;
							$nettot		+=	$netsale;  
							if($taxes>0){
								$daytotal	+=	$row['ordcnt'];	
				?>						 
					<tr>
						<td><?php echo $row['odate'];?></td>
						<td><?php echo $row['ordcnt'];?></td>
						<td>&pound;<?php echo number_format($netsale,'2','.','')?></td>
						<td>&pound;<?php echo number_format($taxes,'2','.','')?></td>
						<td>&pound;<?php echo number_format($sales,'2','.','')?></td> 											
					</tr>
						
				<?php 		}
						}
				?>	
				 <tr>
					<td><b>Totals</b></td>
					<td><b><?php echo $daytotal;?></b></td>			
					<td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>
					<td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>
				</tr>
			</table>
		<?php	}	 ?>
			</div>			
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
		 <?php }else if($data['display_type']=='Weekly'){
		?>
		 <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			</ul> 
			<div id="tsales">
			<?php
				$sdate	=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate	=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom = strtotime($sdate, 0);
				$dateto  = strtotime($edate, 0);

			    $datearr=	$this->get_days ($sdate,$edate);				
				$cntday	=	count($datearr);
				if($cntday>0){
					$weektot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Week Starting Date</th>
					<th>Number Of Orders</th>
					<th>Net Sales</th>			
					<th>Country Taxes</th>
					<th>Gross Sales</th>
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp	=	explode("/",$datearr[$i]);
						$m		=	$exp[1];
						$d		=	$exp[0];
						$y		=	$exp[2];
						$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

						 $sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt,sum(ordertotal)as totals,sum(order_vat_tax)as taxes FROM 3bit_orders WHERE WEEK(shipped_date,1)='".$g."' AND YEAR(shipped_date)='".$y."' AND (status ='Shipped' OR status ='Refunded') ");			
						 if(count($sql)>0){ 
							$ordcnt		=	$sql['ordcnt'];
							$discount	=	0;
							$sales		=	$sql['totals'];
							$taxes		=	$sql['taxes'];  							
							$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE WEEK(o.shipped_date,1)='".$g."' AND YEAR(o.shipped_date)='".$y."' AND (o.status ='Shipped' OR o.status ='Refunded') AND o.order_id=od.order_id GROUP BY o.order_id");	
							if(count($selOrders)>0){ 
								foreach($selOrders as $rw){									
									$discount	+=	$rw['discount'];
								}
							}
							$netsale	=	($sales-$taxes)+$discount;										
							$salestot	+=	$sales;
							$taxtot		+=	$taxes;
							$disctot	+=	$discount;
							$nettot		+=	$netsale;

							if($ordcnt>0){	 
								if($taxes>0){
									$weektot	+=	$ordcnt;	
				?>
					<tr>
						 <td><?php echo $datearr[$i]?></td>
						 <td><?php echo $ordcnt;?></td>
						 <td>&pound;<?php echo number_format($netsale,'2','.','')?></td>
						 <td>&pound;<?php echo number_format($taxes,'2','.','')?></td>
						 <td>&pound;<?php echo number_format($sales,'2','.','')?></td>
					</tr>
				<?php		}
						  }
						} 
					  }
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $weektot;?></b></td>
						 <td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>	
						 <td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>			
						 <td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>								
					</tr>	
				 </table>
				<?php } ?>
			</div>			
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
		  </div> 
	<?php  }else if($data['display_type']=='Monthly'){ ?>
		  <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			</ul> 
			<div id="tsales">
				<?php
					$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
					$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
					$datefrom	=	strtotime($sdate, 0);
					$dateto		=	strtotime($edate, 0);
					$months		=	$this->get_months($sdate,$edate);
					$cntday		=	count($months);			
					if($cntday>0){
						$monthtot	=	0;
						$salestot	=	0;
						$taxtot		=	0;
						$disctot	=	0;
						$nettot		=	0;
				?>
					 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
					 <thead>
					<tr>
						<th>Month</th>
						<th>Number Of Orders</th>
						<th>Net Sales</th>			
						<th>Country Taxes</th>		
						<th>Gross Sales</th>
					</tr>
					</thead>
				<?php
						for($i=0;$i<$cntday;$i++){
							$exp		=	explode(" ",$months[$i]);
							$m			=	$exp[0];
							$y			=   $exp[2];
							$dispmon	=   $exp[1]." ".$exp[2];								

							$sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt,sum(ordertotal)as totals,sum(order_vat_tax)as taxes FROM 3bit_orders WHERE MONTH(shipped_date)='".$m."' AND YEAR(shipped_date)='".$y."' AND (status ='Shipped' OR status ='Refunded')  ");
							if(count($sql)>0){	
								$ordcnt		=	$sql['ordcnt'];								 
								$discount	=	0;
								$sales		=	$sql['totals'];		
								$taxes		=	$sql['taxes'];	
								
								$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE MONTH(o.shipped_date)='".$m."' AND YEAR(o.shipped_date)='".$y."' AND o.order_id=od.order_id AND (o.status ='Shipped' OR o.status ='Refunded') GROUP BY o.order_id");
								if(count($selOrders)>0){ 
									foreach($selOrders as $rw){										
										$discount	+=	$rw['discount'];
									}
								}
								$netsale	=	($sales-$taxes)+$discount;
								$salestot	+=	$sales;
								$taxtot		+=	$taxes;
								$disctot	+=	$discount;
								$nettot		+=	$netsale;
								 if($ordcnt>0){
									 if($taxes>0){
										 $monthtot	+=	$ordcnt;
					?>
						<tr>
							 <td><?php echo $dispmon?></td>
							 <td><?php echo $ordcnt;?></td>
							 <td>&pound;<?php echo number_format($netsale,'2','.','')?></td>
							 <td>&pound;<?php echo number_format($taxes,'2','.','')?></td>
							 <td>&pound;<?php echo number_format($sales,'2','.','')?></td>
						</tr>
					<?php			}	
								}
							}	
						}
					?>
						<tr>
							 <td><b>Totals</b></td>
							 <td><b><?php echo $monthtot;?></b></td>
							 <td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
							 <td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
							 <td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>
						</tr>	
						</table>
					<?php  } ?>
					</div>
					<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16"	border="0" /><b>Export</b></a></p>
			  </div>
	<?php  }else if($data['display_type']=='Yearly'){?>
		  <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			</ul> 
			<div id="tsales">
				<?php			  			
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$years		=	$this->get_years($sdate,$edate);
				$cntday		=	count($years);
				if($cntday>0){
					$yeartot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Year</th>
					<th>Number Of Orders</th>
					<th>Net Sales</th>			
					<th>Country Taxes</th>
					<th>Gross Sales</th>
				</tr>
				</thead>
			<?php
				for($i=0;$i<$cntday;$i++){					
					$y			=   $years[$i];
				    $sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt,sum(ordertotal)as totals,sum(order_vat_tax)as taxes FROM 3bit_orders WHERE YEAR(shipped_date)='".$y."' AND (status ='Shipped' OR status ='Refunded') ");
					if(count($sql)>0){	
							$ordcnt		=	$sql['ordcnt'];								 
							$discount	=	0;
							$sales		=	$sql['totals'];		
							$taxes		=	$sql['taxes'];	
							
							$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE YEAR(shipped_date)='".$y."' AND o.order_id=od.order_id AND (o.status ='Shipped' OR o.status ='Refunded') GROUP BY o.order_id");				
							if(count($selOrders)>0){ 																												
								foreach($selOrders as $rw){																						
									$discount	+=	$rw['discount'];
								}
							}
							$netsale	=	($sales-$taxes)+$discount;							
							 if($ordcnt>0){
								 if($taxes>0){
									$yeartot	+=	$ordcnt;
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;
									$disctot	+=	$discount;
									$nettot		+=	$netsale;
				?>
					<tr>
							 <td><?php echo $years[$i]?></td>
							 <td><?php echo $ordcnt;?></td>
							 <td>&pound;<?php echo number_format($netsale,'2','.','')?></td>
							 <td>&pound;<?php echo number_format($taxes,'2','.','')?></td>
							 <td>&pound;<?php echo number_format($sales,'2','.','')?></td>
						</tr>
					<?php			}	
								}
							}	
						}
					?>
						<tr>
							 <td><b>Totals</b></td>
							 <td><b><?php echo $yeartot;?></b></td>
							 <td><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
							 <td><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
							 <td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>
						</tr>	
						</table>
					<?php  } ?>
					 </div>										
					<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
			  </div>
			<?php
			 }
			}else if($data['report_type']=='ShippingByDate'){
				
				if($data['display_type']=='Daily'){
		?>
		<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
				 <?php 	   //////////// For UK SALES ///////////////////////
					$sql = "SELECT count(o.order_id )as ordcnt,DATE_FORMAT(o.shipped_date,'%m/%d/%Y')as odate,sum(abs(o.ordertotal))as totals,sum(o.order_vat_tax)as taxes  FROM `3bit_orders` o WHERE (o.status ='Shipped') ";

					// Concate with query //

					if(trim($end_date) != "" && trim($start_date) != ""){	 	
						$monSArr	=	explode("/",$start_date);
						$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
						$monEArr	=	explode("/",$end_date);
						$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
						$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
					}		

					$sql	.=	" GROUP BY DATE_FORMAT(o.shipped_date,'%m/%d/%Y') ORDER BY o.shipped_date ASC ";	

					$res	=	$this->_db->fetchAll($sql);

					if(count($res)>0){				
						$daytotal	=	0;
						$salestot	=	0;
						$taxtot		=	0;
						$disctot	=	0;
						$nettot		=	0;

				?>
					<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
						<thead>
						<tr>
							<th>Day</th>
							<th>Number Of Orders</th>			
							<th>Gross Sales</th>										
						</tr>
						</thead>
						<?php foreach($res as $row){ 
									$daytotal	+=	$row['ordcnt'];	
									$discount	=	0;
									$sales		=	$row['totals'];
									$taxes		=	$row['taxes'];
									
									
									$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE DATE_FORMAT(o.shipped_date,'%m/%d/%Y')='".$row['odate']."' AND o.order_id=od.order_id GROUP BY o.order_id");	
									if(count($selOrders)>0){ 						
										foreach($selOrders as $rw){											
											$discount	+=	$rw['discount'];
										}
									}
									$netsale	=	($sales-$taxes)+$discount;
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;
									$disctot	+=	$discount;
									$nettot		+=	$netsale;
						?>

								<tr>
									<td><?php echo $row['odate'];?></td>
									<td><?php echo $row['ordcnt'];?></td>
									<td>&pound;<?php echo number_format($sales,'2','.','');?></td>								
								</tr>
				
						<?php }	?>	
						 <tr>
							<td><b>Totals</b></td>
							<td><b><?php echo $daytotal;?></b></td>			
							<td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>											
						</tr>
					</table>
			<?php	}?>
			  </div>
			  <div id='tdetails'>  
			   <?php $sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,o.courier_id,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country ";

					// Concate with query //

					if(trim($end_date) != "" && trim($start_date) != ""){	 	
						$monSArr	=	explode("/",$start_date);
						$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
						$monEArr	=	explode("/",$end_date);
						$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
						$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
					}		

					$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

					$res		=	$this->_db->fetchAll($sql);

					$salerows	=	count($res);

					if($salerows>0){
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
				?>
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Order Id</th>
					<th>Invoice Number</th>
					<th>Customer Name</th>
					<th>Shipped Date</th>
					<th>Courier</th>
					<th>Shipping Country</th>					
					<th>Net Sales</th>
					<th>Taxes</th>			
					<th>Gross Sales</th>		
				</tr>
				</thead>
				<?php foreach($res as $row){ 
						$sales		=	$row['ordertotal'];	
						$taxes		=	$row['order_vat_tax'];
						$discount	=	0;
						if($row['status']=='Shipped'){
						   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
						   if(count($selShip)>0){
							  $shipping	=	$selShip['shipping'];
							  $discount	=	$selShip['discount'];
						   }					  
						}
						$couriername	=	'';
						$selCTit		=	$this->_db->fetchRow("SELECT courier_name,courier_url FROM courier WHERE courier_id='".$row['courier_id']."'");			
						if(count($selCTit)>0){
							$couriername	=	$selCTit['courier_name'];
							$courierurl		=	$selCTit['courier_url'];
						}
						$netsales	=	($sales-$taxes)+$discount;						
						$salestot	+=	$sales;
						$taxtot		+=	$taxes;						
						$nettot		+=	$netsales; 					
				?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=$row['odate']?></td>
					<td><?=$couriername?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php }	?>
				 <tr>
					<td colspan='5'><b>Totals</b></td>
					<td><b><?php echo $salerows;?></b></td>						
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
				<?php } ?>
			  </div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>

	  <?php }else if($data['display_type']=='Weekly'){ ?>
	  <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
				<?php 
				$sdate	=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate	=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom = strtotime($sdate, 0);
				$dateto  = strtotime($edate, 0);

			    $datearr=	$this->get_days ($sdate,$edate);
				$cntday	=	count($datearr);
				if($cntday>0){
					$weektot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Week Starting Date</th>
					<th>Number Of Orders</th>
					<th>Gross Sales</th>								
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp	=	explode("/",$datearr[$i]);
						$m		=	$exp[1];
						$d		=	$exp[0];
						$y		=	$exp[2];
						$g	=	date("W", mktime(0, 0, 0, $m, $d, $y));

						 $sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt,sum(abs(ordertotal))as totals,sum(order_vat_tax)as taxes FROM 3bit_orders WHERE WEEK(shipped_date,1)='".$g."' AND YEAR(shipped_date)='".$y."' AND (status ='Shipped') ");

						 if(count($sql)>0){ 
							$ordcnt		=	$sql['ordcnt'];
							$discount	=	0;
							$sales		=	$sql['totals'];
							$taxes		=	$sql['taxes'];							
							
							$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE WEEK(o.shipped_date,1)='".$g."' AND o.order_id=od.order_id GROUP BY o.order_id ");				
							if(count($selOrders)>0){ 																							
								foreach($selOrders as $rw){																							
									$discount	+=	$rw['discount'];
								}
							}
							$netsale	=	($sales-$taxes)+$discount;
							$weektot	+=	$ordcnt;				
							$salestot	+=	$sales;
							$taxtot		+=	$taxes;
							$disctot	+=	$discount;
							$nettot		+=	$netsale;

							if($ordcnt>0){

				?>
					<tr>
						 <td><?php echo $datearr[$i]?></td>
						 <td><?php echo $ordcnt;?></td>
						 <td>&pound;<?php echo number_format($sales,'2','.','');?></td>						
					</tr>
				<?php		}
						} 
					  }
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $weektot;?></b></td>
						<td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>									
					</tr>	
				 </table>
				<?php } ?>
				</div>
				<div id='tdetails'>
				 <?php if($cntday>0){ 
						$sales		=	0;
						$taxes		=	0;
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
						$shiptot	=	0;					
				 ?>
				   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				   <thead>
					<tr>
						<th>Order Id</th>
						<th>Invoice Number</th>
						<th>Customer Name</th>
						<th>Shipped Date</th>
						<th>Courier</th>
						<th>Shipping Country</th>					
						<th>Net Sales</th>
						<th>Taxes</th>			
						<th>Gross Sales</th>	
					</tr>
				  </thead>
				  <?php for($i=0;$i<$cntday;$i++){
							$exp	=	explode("/",$datearr[$i]);
							$m		=	$exp[1];
							$d		=	$exp[0];
							$y		=	$exp[2];
							$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

							$sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,o.courier_id,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND WEEK(o.shipped_date,1)='".$g."' AND YEAR(o.shipped_date)='".$y."'";								

							$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

							$res		=	$this->_db->fetchAll($sql);

							$salerows	=	count($res);

							$weekrows	+=	$salerows;					

							if($salerows>0){

								foreach($res as $row){
									$sales		=	$row['ordertotal'];	
									$taxes		=	$row['order_vat_tax'];
									$discount	=	0;
									if($row['status']=='Shipped'){
									   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
									   if(count($selShip)>0){
										  $shipping	=	$selShip['shipping'];
										  $discount	=	$selShip['discount'];
									   }					  
									}
									$couriername	=	'';
									$selCTit		=	$this->_db->fetchRow("SELECT courier_name,courier_url FROM courier WHERE courier_id='".$row['courier_id']."'");	
									if(count($selCTit)>0){
										$couriername	=	$selCTit['courier_name'];
										$courierurl		=	$selCTit['courier_url'];
									}

									$netsales	=	($sales-$taxes)+$discount;						
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;						
									$nettot		+=	$netsales; 
									$shiptot	+=	$shipping;
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=$row['odate']?></td>
					<td><?=$couriername?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>					
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						}				   
					}
				  ?>
				  <tr>
					<td colspan='5'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
				 <?php } ?>
			  </div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
	  <?php }else if($data['display_type']=='Monthly'){ ?>
	  <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
			<?php
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$months		=	$this->get_months($sdate,$edate);
				$cntday		=	count($months);			
				if($cntday>0){
					$monthtot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Month</th>
					<th>Number Of Orders</th>
					<th>Gross Sales</th>						
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp		=	explode(" ",$months[$i]);
						$m			=	$exp[0];
						$y			=   $exp[2];
						$dispmon	=   $exp[1]." ".$exp[2];								

						$sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt,sum(abs(ordertotal))as totals,sum(order_vat_tax)as taxes FROM 3bit_orders WHERE MONTH(shipped_date)='".$m."' AND YEAR(shipped_date)='".$y."' AND (status ='Shipped')");
						if(count($sql)>0){	
							 $ordcnt	=	$sql['ordcnt'];
							 $monthtot	+=	$ordcnt;
							 $discount	=	0;
							$sales		=	$sql['totals'];
							$taxes		=	$sql['taxes'];
							
							
							$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE MONTH(o.shipped_date)='".$m."' AND YEAR(o.shipped_date)='".$y."' AND o.order_id=od.order_id GROUP BY o.order_id");				

							if(count($selOrders)>0){ 
								foreach($selOrders as $rw){									
									$discount	+=	$rw['discount'];
								}
							}
							$netsale	=	($sales-$taxes)+$discount;
							$salestot	+=	$sales;
							$taxtot		+=	$taxes;
							$disctot	+=	$discount;
							$nettot		+=	$netsale;
							 if($ordcnt>0){
				?>
					<tr>
						 <td><?php echo $dispmon?></td>
						 <td><?php echo $ordcnt;?></td>
						 <td>&pound;<?php echo number_format($sales,'2','.','');?></td>						
					</tr>
				<?php		}
						}

					}
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b><?php echo $monthtot;?></b></td>
						 <td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>									 						
					</tr>	
					</table>
				<?php } ?>
			</div>	
			<div id='tdetails'>
			  <?php if($cntday>0){ 
						$sales		=	0;
						$taxes		=	0;
						$salestot	=	0;
						$taxtot		=	0;						
						$nettot		=	0; 
						$shiptot	=	0;
					
				 ?>
				   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				   <thead>
					<tr>
						<th>Order Id</th>
						<th>Invoice Number</th>
						<th>Customer Name</th>
						<th>Shipped Date</th>
						<th>Courier</th>
						<th>Shipping Country</th>					
						<th>Net Sales</th>
						<th>Taxes</th>			
						<th>Gross Sales</th>	
					</tr>
					</thead>
				  <?php for($i=0;$i<$cntday;$i++){
						    $exp		=	explode(" ",$months[$i]);
							$m			=	$exp[0];
							$y			=   $exp[2];
							$dispmon	=   $exp[1]." ".$exp[2]; 

							$sql = "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,o.courier_id,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND MONTH(o.shipped_date)='".$m."' AND YEAR(o.shipped_date)='".$y."'";								

							$sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

							$res		=	$this->_db->fetchAll($sql);

							$salerows	=	count($res);

							$weekrows	+=	$salerows;					

							if($salerows>0){

								foreach($res as $row){ 
									$sales		=	$row['ordertotal'];	
									$taxes		=	$row['order_vat_tax'];
									$discount	=	0;
									if($row['status']=='Shipped'){
									   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
									   if(count($selShip)>0){
										  $shipping	=	$selShip['shipping'];
										  $discount	=	$selShip['discount'];
									   }					  
									}
									$couriername	=	'';
									$selCTit		=	$this->_db->fetchRow("SELECT courier_name,courier_url FROM courier WHERE courier_id='".$row['courier_id']."'");	
									if(count($selCTit)>0){
										$couriername	=	$selCTit['courier_name'];
										$courierurl		=	$selCTit['courier_url'];
									}
									$netsales	=	($sales-$taxes)+$discount;						
									$salestot	+=	$sales;
									$taxtot		+=	$taxes;						
									$nettot		+=	$netsales; 
									$shiptot	+=	$shipping;
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=$row['odate']?></td>
					<td><?=$couriername?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>					
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						}

					}
				?>
				<tr>
					<td colspan='5'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>						
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
			  <?php } ?>
			</div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>	   
	<?php }else if($data['display_type']=='Yearly'){	?>
	 <div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
			<?php
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$years		=	$this->get_years($sdate,$edate);
				$cntday		=	count($years);
				if($cntday>0){
					$yeartot	=	0;
					$salestot	=	0;
					$taxtot		=	0;
					$disctot	=	0;
					$nettot		=	0;
			?>
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Month</th>
					<th>Number Of Orders</th>
					<th>Gross Sales</th>						
				</tr>
				</thead>
			<?php
				for($i=0;$i<$cntday;$i++){					
					$y			=   $years[$i];
				    $sql		=	$this->_db->fetchRow("SELECT count(order_id)as ordcnt,sum(abs(ordertotal))as totals,sum(order_vat_tax)as taxes FROM 3bit_orders WHERE YEAR(shipped_date)='".$y."' AND (status ='Shipped') ");
					if(count($sql)>0){	
							$ordcnt		=	$sql['ordcnt'];
							$yeartot	+=	$ordcnt;
							$discount	=	0;
							$sales		=	$sql['totals'];
							$taxes		=	$sql['taxes'];
							
							
							$selOrders	=	$this->_db->fetchAll("SELECT o.order_id ,o.status,od.discount FROM 3bit_orders o,3bit_order_details od WHERE YEAR(shipped_date)='".$y."' AND o.order_id=od.order_id GROUP BY o.order_id");
							if(count($selOrders)>0){ 
								foreach($selOrders as $rw){
									$discount	+=	$rw['discount'];
								}
							}
							$netsale	=	($sales-$taxes)+$discount;
							$salestot	+=	$sales;
							$taxtot		+=	$taxes;
							$disctot	+=	$discount;
							$nettot		+=	$netsale;
							 if($ordcnt>0){
				?>
					<tr>
						 <td><?php echo $y?></td>
						 <td><?php echo $ordcnt;?></td>
						  <td>&pound;<?php echo number_format($sales,'2','.','');?></td>							 
					</tr>
				<?php	}	
					
					}
				}
			?>
				<tr>
					 <td><b>Totals</b></td>
					 <td><b><?php echo $yeartot;?></b></td>
					 <td><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>							
				</tr>	
				</table>
			<?php }  ?>
			</div>	
			<div id="tdetails">
			<?php if($cntday>0){
					$sales		=	0;
					$taxes		=	0;
					$salestot	=	0;
					$taxtot		=	0;						
					$nettot		=	0; 
					$shiptot	=	0;
				
			 ?>
			   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
			   <thead>
				<tr>
					<th>Order Id</th>
					<th>Invoice Number</th>
					<th>Customer Name</th>
					<th>Shipped Date</th>
					<th>Courier</th>
					<th>Shipping Country</th>					
					<th>Net Sales</th>
					<th>Taxes</th>			
					<th>Gross Sales</th>		
				</tr>
				</thead>
			  <?php for($i=0;$i<$cntday;$i++){
					   $y		=   $years[$i];
					   $sql		= "SELECT o.order_id,o.invoice_number,o.ordertotal,o.order_vat_tax,o.status,o.courier_id,concat(c.fname, ' ',c.lname)as Name,a.shipping_country,ct.country,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_customers` c, `3bit_customers_address` a,`3bit_country` ct WHERE (o.status ='Shipped') AND c.customer_id = o.customer_id AND o.address_id=a.address_id AND ct.id=a.shipping_country AND YEAR(o.shipped_date)='".$y."'";								
				      $sql		.=	" GROUP BY o.order_id ORDER BY o.invoice_number ASC ";	

					   $res		=	$this->_db->fetchAll($sql);

					   $salerows	=	count($res);

					   $weekrows	+=	$salerows;					

					   if($salerows>0){

							foreach($res as $row){ 
								$sales		=	$row['ordertotal'];	
								$taxes		=	$row['order_vat_tax'];
								$discount	=	0;
								if($row['status']=='Shipped'){
								   $selShip	=	$this->_db->fetchRow("SELECT shipping,discount FROM 3bit_order_details WHERE order_id='".$row['order_id']."' GROUP BY order_id");
								   if(count($selShip)>0){
									  $shipping	=	$selShip['shipping'];
									  $discount	=	$selShip['discount'];
								   }					  
								}
								$couriername	=	'';
								$selCTit		=	$this->_db->fetchRow("SELECT courier_name,courier_url FROM courier WHERE courier_id='".$row['courier_id']."'");	
								if(count($selCTit)>0){
									$couriername	=	$selCTit['courier_name'];
									$courierurl		=	$selCTit['courier_url'];
								}
								$netsales	=	($sales-$taxes)+$discount;						
								$salestot	+=	$sales;
								$taxtot		+=	$taxes;						
								$nettot		+=	$netsales; 
								$shiptot	+=	$shipping;
				 ?>
				<tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['invoice_number']?></td>
					<td><?=$row['Name']?></td>
					<td><?=$row['odate']?></td>
					<td><?=$couriername?></td>
					<td><?=ucwords(strtolower($row['country']))?></td>					
					<td class='values'><?=number_format($netsales,'2','.','');?></td>
					<td class='values'><?=number_format($taxes,'2','.','');?></td>
					<td class='values'><?=number_format($sales,'2','.','');?></td>
				</tr>
				<?php		}		
						} 
					}
				?>
				<tr>
					<td colspan='5'><b>Totals</b></td>
					<td><b><?php echo $weekrows;?></b></td>						
					<td class='values'><b>&pound;<?php echo number_format($nettot,'2','.','');?></b></td>			
					<td class='values'><b>&pound;<?php echo number_format($taxtot,'2','.','');?></b></td>	
					<td class='values'><b>&pound;<?php echo number_format($salestot,'2','.','');?></b></td>			
				</tr>
				</table>
			<?php	} ?>
			</div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
	<?php
		  }	 
	}else if($data['report_type']=='Deposits'){
		 $start_date	=	 '01/01/2010';
		 $end_date		=	 date("m/d/Y", mktime(0, 0, 0, $emon, $eday, $eyer));
		 $totdate		=	 $this->getdates($start_date,$end_date);
		 $numdates		=	 count($totdate);	
	?>
		<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
			<?php if($numdates>0){
					 $grand_total=	0;
					 $grand_sales=	0;
			 ?>
			   <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
			   <thead>
				<tr>
					<th>Date</th>
					<th>Deposits</th>
					<!--th>Sales</th -->						
				</tr>
				</thead>
			<?php	for($i=0;$i<$numdates;$i++){
						 $newdate	=	$totdate[$i];
						 $total		=	0;
						 $accept	=	0;
						 $sales		=	0;

						 $selDep	=	$this->_db->fetchAll("SELECT *,DATE_FORMAT(action_date,'%d/%m/%Y %H:%i:%s') as odate,DATE_FORMAT(action_date,'%d/%m/%Y') as chkdate FROM 3bit_order_actions WHERE DATE_FORMAT(action_date,'%d/%m/%Y')='".$newdate."' AND status ='1' ORDER BY action_date ASC"); 
						 if(count($selDep)>0){
							foreach($selDep as $res){ 
								$status	= $res['action'];
								$accept	= 0;
								if($status =='New'){
									$selAccept	=	$this->_db->fetchAll("SELECT action_id FROM 3bit_order_actions WHERE order_id='".$res['order_id']."' AND action='Accepted' AND DATE_FORMAT(action_date,'%d/%m/%Y')='".$res['chkdate']."'");
									$accept	=	count($selAccept);			
								}
								if($status =='Accepted'){
									$selAccept	=	$this->_db->fetchAll("SELECT action_id FROM 3bit_order_actions WHERE order_id='".$res['order_id']."' AND action='Deposit Balance Pay' AND DATE_FORMAT(action_date,'%d/%m/%Y')='".$res['chkdate']."'");
									$accept	=	count($selAccept); 								
								}
								$amount_paid	=	$res['amount_paid'];
								if($status	==	'Shipped' || $status == 'Archived' || $status == 'Rejected'){
									$amount_paid	=	0-$res['amount_paid'];
								}
								if($accept==0){
								  $total	+=	$amount_paid;
								}
							}
						 }
						 $exp	=	explode("/",$newdate);
						 $sale_date	=	$exp[2]."-".$exp[1]."-".$exp[0];

						 $selSales		=	$this->_db->fetchRow("SELECT sum(o.ordertotal)as totals FROM `3bit_orders` o WHERE (o.status ='Shipped' OR o.status ='Refunded') AND DATE(o.shipped_date) = '".$sale_date."' GROUP BY DATE_FORMAT(o.shipped_date,'%d/%m/%Y') ORDER BY o.shipped_date ASC ");
						  if(count($selSales)>0){
							 $sales		=	$selSales['totals'];			
						  }
						 $grand_total	+=	$total;
						 $grand_sales	+=	$sales;
						 if($total!='0'){
				?>
					 <tr>
						<td><?=$newdate?></td>
						<td class="values">&pound;<?=number_format($total,2,'.','')?></td>
						<!-- td class="values"><?=number_format($sales,2,'.','')?></td -->
					</tr>													  						
		 	   <?php  	}
						}	?>
				   <tr>
						<td><b>Total</b></td>
						<td class="values"><b>&pound;<?=number_format($grand_total,2,'.','')?></b></td>
						<!-- td class="values"><b><?=number_format($grand_sales,2,'.','')?></b></td -->
					 </tr>
			   </table>
			<?php  } ?>
			</div>	   			
			<div id="tdetails">
			<?php 
				  $current_date	=	 date("d/m/Y", mktime(0, 0, 0, $emon, $eday, $eyer));		 
				  $selDeposit	=	$this->_db->fetchAll("SELECT *,DATE_FORMAT(action_date,'%d/%m/%Y %H:%i:%s') as odate,DATE_FORMAT(action_date,'%d/%m/%Y') as chkdate FROM 3bit_order_actions WHERE DATE_FORMAT(action_date,'%d/%m/%Y')='".$current_date."' AND status ='1' ORDER BY action_date ASC");  		
				  if(count($selDeposit)>0){
					  $k		=	0;
					  $total	=	0;																																	
		    ?>
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Order Id</th>
					<th>Date</th>
					<th>Name</th>
					<th>Delivery Country</th>						
					<th>Invoice Number</th>						
					<th>Status</th>
					<th>Total</th>
				</tr>	
				</thead>
			<?php foreach($selDeposit as $row){ 
						$status	= $row['action'];
						$approve= 0;
						$attach	= 0;
						$invoice_number	=	'';
						$k++;
						if($status =='New'){
							$selApprove	=	$this->_db->fetchAll("SELECT action_id FROM 3bit_order_actions WHERE order_id='".$row['order_id']."' AND action='Accepted' AND DATE_FORMAT(action_date,'%d/%m/%Y')='".$row['chkdate']."'");
							$approve	=	count($selApprove);			
						}		
						if($status =='Accepted'){
							$selApprove	=	$this->_db->fetchAll("SELECT action_id FROM 3bit_order_actions WHERE order_id='".$row['order_id']."' AND action='Deposit Balance Pay' AND DATE_FORMAT(action_date,'%d/%m/%Y')='".$row['chkdate']."'");
							$approve	=	count($selApprove); 
							$selApprove	=	$this->_db->fetchAll("SELECT action_id FROM 3bit_order_actions WHERE order_id='".$row['order_id']."' AND action='New' AND DATE_FORMAT(action_date,'%d/%m/%Y')='".$row['chkdate']."'");
							$attach	=	count($selApprove);				
						}
						$amount_paid	=	$row['amount_paid'];
						if($status	==	'Shipped' || $status == 'Archived' || $status == 'Rejected'){
							$amount_paid	=	0-$row['amount_paid'];
						}
						if($approve==0){
							  if($attach=='1' && $status=='Accepted')
									$status		=	'New,'.$status;
							  $total	+=	$amount_paid;

							  if($status=='Shipped' || $status=='Refunded'){
								$selInv	=	$this->_db->fetchRow("SELECT invoice_number FROM 3bit_orders WHERE order_id='".$row['order_id']."'");
								if(count($selInv)>0){
								  $invoice_number	= $selInv['invoice_number'];
								}
							 }
							 $selname	=	$this->_db->fetchRow("SELECT cn.country,concat(c.fname, ' ',c.lname)as Name FROM 3bit_orders o,3bit_customers c,3bit_customers_address a, 3bit_country cn WHERE o.order_id='".$row['order_id']."' AND o.address_id=a.address_id AND o.customer_id=c.customer_id AND c.customer_id=a.customer_id AND cn.id=a.shipping_country ");
							 if(count($selname)>0){
								$cust_name	=	$selname['Name'];
								$shipcountry=	$selname['country'];

							 }
			?>
				 <tr>
					<td><?=$row['order_id']?></td>
					<td><?=$row['odate']?></td>
					<td><?=$cust_name?></td>
					<td><?=$shipcountry?></td>
					<td><?=$invoice_number?></td>
					<td><?=$status?></td>
					<td><?=$amount_paid?></td>
				 </tr>
			<?php	} 
				}
			?>
				<tr>
					<td colspan='5'><b>Totals</b></td>
					<td><b><?=$k?></b></td>
					<td><b><?=number_format($total,'2','.','')?></b></td>
				</tr>
				</table>
			<?php }	?>
			</div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
	  </div>
<?php 	}else if($data['report_type']=='TotalOrdersByDate'){

		  if($data['display_type']=='Daily'){

			$sql = "SELECT DATE_FORMAT(a.action_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_order_actions` a WHERE (a.action='New' OR a.action='Refunded') AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ";

			// Concate with query //

			if(trim($end_date) != "" && trim($start_date) != ""){	 	
				$monSArr	=	explode("/",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("/",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
				$sql .= " AND DATE(a.action_date)  <= '".$newEDate."' AND DATE(a.action_date) >= '".$newSDate."'  "; 
			}		

			$sql	.=	" GROUP BY DATE_FORMAT(a.action_date,'%d/%m/%Y') ORDER BY a.action_date ASC";	

			$res	=	$this->_db->fetchAll($sql);

			if(count($res)>0){				
				$daytotal	=	0;


		?>
		<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
			<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Day</th>
					<th>Order Total</th>			
				</tr>
				</thead>

		<?php foreach($res as $row){ 					
					$selRef	=	$this->_db->fetchAll("SELECT sum(amount_paid) as total FROM 3bit_order_actions WHERE DATE_FORMAT(action_date,'%d/%m/%Y') = '".$row['odate']."' AND action='Refunded' ");	
					if(count($selRef)>0){
						$ref_total	=	$selRef[0]['total'];
					}

					$selSales		=	$this->_db->fetchAll("SELECT sum(a.amount_paid) as total FROM 3bit_order_actions a,3bit_orders o WHERE DATE_FORMAT(a.action_date,'%d/%m/%Y') = '".$row['odate']."' AND a.action='New' AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ");
					if(count($selSales)>0){
						$act_total	 =	 $selSales[0]['total'];
					}
				
					$sales_tot		=	$act_total+$ref_total;
					$daytotal		+=	$sales_tot;	
						
		?>

				<tr>
					<td><?php echo $row['odate'];?></td>
					<td>&pound;<?php echo $sales_tot;?></td>			
				</tr>
				
		<?php 	}?>	
				 <tr>
					<td><b>Totals</b></td>
					<td><b>&pound;<?php echo $daytotal;?></b></td>			
				</tr>				
			</table>
			</div>
			<div id="tdetails">
			<?php $sql = "SELECT DATE_FORMAT(a.action_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_order_actions` a WHERE (a.action='New' OR a.action='Refunded') AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ";

			// Concate with query //

			if(trim($end_date) != "" && trim($start_date) != ""){	 	
				$monSArr	=	explode("/",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("/",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
				$sql .= " AND DATE(a.action_date)  <= '".$newEDate."' AND DATE(a.action_date) >= '".$newSDate."'  "; 
			}		

			$sql	.=	" GROUP BY DATE_FORMAT(a.action_date,'%d/%m/%Y') ORDER BY a.action_date ASC";	

			$res	=	$this->_db->fetchAll($sql);

			if(count($res)>0){
			?>
				<table width='100%' cellpadding="0" cellspacing="0" border="0" class="display">
				<thead>
				<tr>				
					<th height="20">Order No</th>					
					<th>Customer Name</th>
					<th>OrderDate</th>
					<th>Status</th>
					<th>OrderTotal</th>					
				</tr>
				</thead>
			<?php
				foreach($res as $row){
					$sql	= "SELECT oa.*,o.customer_id,o.order_id,concat(c.fname, ' ',c.lname)as Name FROM 3bit_order_actions oa,3bit_orders o,3bit_customers c WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='New' OR oa.action='Refunded') AND (o.status !='Rejected' AND o.status !='Archived') AND o.payment_status != 'Declined (General).' AND DATE_FORMAT(oa.action_date,'%d/%m/%Y')='".$row['odate']."' ORDER BY oa.action_date DESC";				   

					$result		=	$this->_db->fetchAll($sql);
					$salrows	=	count($result);	
					if($salrows>0){  
						$srows	+=	$salrows;
						foreach($result as $row){ 
							$astat	=	$row['action'];
							$total_sales	+=	$row['amount_paid'];
				?>
					 <tr>
						 <td><?=$row['order_id']?></td>
						 <td><?=$row['Name']?></td>
						 <td><?=$row['action_date']?></td>
						 <td><?=$row['action']?></td>
						 <td>&pound;<?=$row['amount_paid']?></td>						
					 </tr>
				 <?php } 
					}				
				}
			?>
			  <tr>
				  <td colspan="8">Total Sales(<?=$srows?>)</td>
			  </tr>
			  <tr>
				  <td colspan="8">Total Value Of Sales &pound;<?php echo number_format($total_sales,2,'.','');?></td>
			  </tr>			
			</table> 
			<?php
			}
			?>			
			</div>
			<p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
		   </div>		  
		<?php	}
		  }else if($data['display_type']=='Weekly'){
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
			    $datearr	=	$this->get_days ($sdate,$edate);
				$cntday		=	count($datearr);
				if($cntday>0){
					$weektot	=	0;
			?>
			<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
				 <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				 <thead>
				<tr>
					<th>Week Starting Date</th>
					<th>Orders Total</th>			
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp	=	explode("/",$datearr[$i]);						

						$m		=	$exp[1];
						$d		=	$exp[0];
						$y		=	$exp[2];
						$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));

						$selRef	=	$this->_db->fetchAll("SELECT sum(amount_paid) as total FROM 3bit_order_actions WHERE WEEK(action_date,1)='".$g."' AND YEAR(action_date)='".$y."' AND action='Refunded' ");	
						if(count($selRef)>0){
							$ref_total	=	$selRef[0]['total'];
						}

						$selSales		=	$this->_db->fetchAll("SELECT sum(a.amount_paid) as total FROM 3bit_order_actions a,3bit_orders o WHERE WEEK(action_date,1)='".$g."' AND YEAR(action_date)='".$y."' AND a.action='New' AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ");
						if(count($selSales)>0){
							$act_total	 =	 $selSales[0]['total'];
						}
					
						$sales_tot		=	$act_total+$ref_total;						
						$weektot		+=	$sales_tot;							 
						if($sales_tot>0){

				?>
					<tr>
						 <td><?php echo $datearr[$i]?></td>
						 <td>&pound;<?php echo  number_format($sales_tot,2,'.','');?></td>
					</tr>
				<?php	} 
					  }
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b>&pound;<?php echo $weektot;?></b></td>
					</tr>					
				 </table>
				 </div>
				 <div id="tdetails">
				 <?php $sql = "SELECT DATE_FORMAT(a.action_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_order_actions` a WHERE (a.action='New' OR a.action='Refunded') AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ";

				// Concate with query //

				if(trim($end_date) != "" && trim($start_date) != ""){	 	
					$monSArr	=	explode("/",$start_date);
					$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
					$monEArr	=	explode("/",$end_date);
					$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
					$sql .= " AND DATE(a.action_date)  <= '".$newEDate."' AND DATE(a.action_date) >= '".$newSDate."'  "; 
				}		

				$sql	.=	" GROUP BY DATE_FORMAT(a.action_date,'%d/%m/%Y') ORDER BY a.action_date ASC";	

				$res	=	$this->_db->fetchAll($sql);

				if(count($res)>0){
				?>
					<table width='100%' cellpadding="0" cellspacing="0" border="0" class="display">		
					<thead>
					<tr>				
						<th height="20">Order No</th>					
						<th>Customer Name</th>
						<th>OrderDate</th>
						<th>Status</th>
						<th>OrderTotal</th>					
					</tr>
					</thead>
				<?php
					foreach($res as $row){
						$sql	= "SELECT oa.*,o.customer_id,o.order_id,concat(c.fname, ' ',c.lname)as Name FROM 3bit_order_actions oa,3bit_orders o,3bit_customers c WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='New' OR oa.action='Refunded') AND (o.status !='Rejected' AND o.status !='Archived') AND o.payment_status != 'Declined (General).' AND DATE_FORMAT(oa.action_date,'%d/%m/%Y')='".$row['odate']."' ORDER BY oa.action_date DESC";				   

						$result		=	$this->_db->fetchAll($sql);
						$salrows	=	count($result);	
						if($salrows>0){  
							$srows	+=	$salrows;
							foreach($result as $row){ 							
								$astat	=	$row['action'];
							

							$total_sales	+=	$row['amount_paid'];
					?>
						 <tr>
							 <td><?=$row['order_id']?></td>
							 <td><?=$row['Name']?></td>
							 <td><?=$row['action_date']?></td>
							 <td><?=$row['action']?></td>
							 <td>&pound;<?=$row['amount_paid']?></td>						
						 </tr>
					 <?php } 
						}					
					}
				?>
				<tr>
				  <td colspan="8">Total Sales(<?=$srows?>)</td>
				</tr>
				<tr>
				  <td colspan="8">Total Value Of Sales &pound;<?php echo number_format($total_sales,2,'.','');?></td>
				</tr>				
				</table> 
				<?php
				}
				?>				
			   </div>
			   <p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
			</div>
		<?php
			}
		  }else if($data['display_type']=='Monthly'){
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$months		=	$this->get_months($sdate,$edate);
				$cntday		=	count($months);			
				if($cntday>0){
					$monthtot	=	0;
			?>
			<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Month</th>
					<th>Orders Total</th>			
				</tr>
				</thead>
			<?php
					for($i=0;$i<$cntday;$i++){
						$exp		=	explode(" ",$months[$i]);
						$m			=	$exp[0];
						$y			=   $exp[2];
						$dispmon	=   $exp[1]." ".$exp[2];
						
						$selRef	=	$this->_db->fetchAll("SELECT sum(amount_paid) as total FROM 3bit_order_actions WHERE MONTH(action_date)='".$m."' AND YEAR(action_date)='".$y."' AND action='Refunded' ");	
						if(count($selRef)>0){
							$ref_total	=	$selRef[0]['total'];
						}

						$selSales		=	$this->_db->fetchAll("SELECT sum(a.amount_paid) as total FROM 3bit_order_actions a,3bit_orders o WHERE MONTH(action_date)='".$m."' AND YEAR(action_date)='".$y."' AND a.action='New' AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ");
						if(count($selSales)>0){
							$act_total	 =	 $selSales[0]['total'];
						}
					
						$sales_tot		=	$act_total+$ref_total;						
						$monthtot		+=	$sales_tot;	
					    if($monthtot>0){
				?>
					<tr>
						 <td><?php echo $dispmon?></td>
						 <td>&pound;<?php echo $sales_tot;?></td>
					</tr>
				<?php	}					
					}
				?>
					<tr>
						 <td><b>Totals</b></td>
						 <td><b>&pound;<?php echo $monthtot;?></b></td>
					</tr>					
					</table>
				</div>
				<div id="tdetails">
				<?php $sql = "SELECT DATE_FORMAT(a.action_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_order_actions` a WHERE (a.action='New' OR a.action='Refunded') AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ";

				// Concate with query //

				if(trim($end_date) != "" && trim($start_date) != ""){	 	
					$monSArr	=	explode("/",$start_date);
					$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
					$monEArr	=	explode("/",$end_date);
					$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
					$sql .= " AND DATE(a.action_date)  <= '".$newEDate."' AND DATE(a.action_date) >= '".$newSDate."'  "; 
				}		

				$sql	.=	" GROUP BY DATE_FORMAT(a.action_date,'%d/%m/%Y') ORDER BY a.action_date ASC";	

				$res	=	$this->_db->fetchAll($sql);

				if(count($res)>0){
				?>
					<table width='100%' cellpadding="0" cellspacing="0" border="0" class="display">
					<thead>
					<tr>				
						<th height="20">Order No</th>					
						<th>Customer Name</th>
						<th>OrderDate</th>
						<th>Status</th>
						<th>OrderTotal</th>					
					</tr>					
					</thead>
				<?php
					foreach($res as $row){
						$sql	= "SELECT oa.*,o.customer_id,o.order_id,concat(c.fname, ' ',c.lname)as Name FROM 3bit_order_actions oa,3bit_orders o,3bit_customers c WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='New' OR oa.action='Refunded') AND (o.status !='Rejected' AND o.status !='Archived') AND o.payment_status != 'Declined (General).' AND DATE_FORMAT(oa.action_date,'%d/%m/%Y')='".$row['odate']."' ORDER BY oa.action_date DESC";				   

						$result		=	$this->_db->fetchAll($sql);
						$salrows	=	count($result);	
						if($salrows>0){  
							$srows	+=	$salrows;
							foreach($result as $row){ 							
								$astat	=	$row['action'];
							

							$total_sales	+=	$row['amount_paid'];
					?>
						 <tr>
							 <td><?=$row['order_id']?></td>
							 <td><?=$row['Name']?></td>
							 <td><?=$row['action_date']?></td>
							 <td><?=$row['action']?></td>
							 <td>&pound;<?=$row['amount_paid']?></td>						
						 </tr>
					 <?php } 
						}					
					}
				?>				
				  <tr>
					  <td colspan="8">Total Sales(<?=$srows?>)</td>
				  </tr>
				  <tr>
					  <td colspan="8">Total Value Of Sales &pound;<?php echo number_format($total_sales,2,'.','');?></td>
				  </tr>
				</table> 
				<?php
				}
				?>
				</div>
			   <p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
			</div>
		<?php	}
		  }else if($data['display_type']=='Yearly'){			  			
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$years		=	$this->get_years($sdate,$edate);
				$cntday		=	count($years);
				if($cntday>0){
					$yeartot	=	0;
			?>
			<div id="article_tabs">
			<ul>
			  <li><a href="#tsales">Sales</a></li>
			  <li><a href="#tdetails">Details</a></li>
			</ul> 
			<div id="tsales">
				<table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Year</th>
					<th>Orders Total</th>			
				</tr>
				</thead>
			<?php
				for($i=0;$i<$cntday;$i++){					
					$y			=   $years[$i];
					$selRef		=	$this->_db->fetchAll("SELECT sum(amount_paid) as total FROM 3bit_order_actions WHERE YEAR(action_date)='".$y."' AND action='Refunded' ");	
					if(count($selRef)>0){
						$ref_total	=	$selRef[0]['total'];
					}

					$selSales		=	$this->_db->fetchAll("SELECT sum(a.amount_paid) as total FROM 3bit_order_actions a,3bit_orders o WHERE YEAR(action_date)='".$y."' AND a.action='New' AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ");
					if(count($selSales)>0){
						$act_total	 =	 $selSales[0]['total'];
					}
				
					$sales_tot		=	$act_total+$ref_total;						
					$yeartot		+=	$sales_tot;	
				    if($yeartot>0){
				?>
					<tr>
						 <td><?php echo $y?></td>
						 <td>&pound;<?php echo $sales_tot;?></td>
					</tr>
				<?php }
				}
			?>
				<tr>
					 <td><b>Totals</b></td>
					 <td><b>&pound;<?php echo $yeartot;?></b></td>
				</tr>				
				</table>
				</div>
				<div id="tdetails">
				<?php $sql = "SELECT DATE_FORMAT(a.action_date,'%d/%m/%Y')as odate FROM `3bit_orders` o,`3bit_order_actions` a WHERE (a.action='New' OR a.action='Refunded') AND o.payment_status != 'Declined (General).' AND (o.status !='Rejected' AND o.status !='Archived') AND o.order_id=a.order_id ";

				// Concate with query //

				if(trim($end_date) != "" && trim($start_date) != ""){	 	
					$monSArr	=	explode("/",$start_date);
					$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
					$monEArr	=	explode("/",$end_date);
					$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
					$sql .= " AND DATE(a.action_date)  <= '".$newEDate."' AND DATE(a.action_date) >= '".$newSDate."'  "; 
				}		

				$sql	.=	" GROUP BY DATE_FORMAT(a.action_date,'%d/%m/%Y') ORDER BY a.action_date ASC";	

				$res	=	$this->_db->fetchAll($sql);

				if(count($res)>0){
				?>
					<table width='100%' cellpadding="0" cellspacing="0" border="0" class="display">
					<thead>
					<tr>				
						<th height="20">Order No</th>					
						<th>Customer Name</th>
						<th>OrderDate</th>
						<th>Status</th>
						<th>OrderTotal</th>					
					</tr>	
					</thead>
				<?php
					foreach($res as $row){
						$sql	= "SELECT oa.*,o.customer_id,o.order_id,concat(c.fname, ' ',c.lname)as Name FROM 3bit_order_actions oa,3bit_orders o,3bit_customers c WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='New' OR oa.action='Refunded') AND (o.status !='Rejected' AND o.status !='Archived') AND o.payment_status != 'Declined (General).' AND DATE_FORMAT(oa.action_date,'%d/%m/%Y')='".$row['odate']."' ORDER BY oa.action_date DESC";				   

						$result		=	$this->_db->fetchAll($sql);
						$salrows	=	count($result);	
						if($salrows>0){  
							$srows	+=	$salrows;
							foreach($result as $row){ 							
								$astat	=	$row['action'];
							

							$total_sales	+=	$row['amount_paid'];
					?>
						 <tr>
							 <td><?=$row['order_id']?></td>
							 <td><?=$row['Name']?></td>
							 <td><?=$row['action_date']?></td>
							 <td><?=$row['action']?></td>
							 <td>&pound;<?=$row['amount_paid']?></td>						
						 </tr>
					 <?php } 
						}					
					}
				?>				
				  <tr>
					  <td colspan="8">Total Sales(<?=$srows?>)</td>
				  </tr>
				  <tr>
					  <td colspan="8">Total Value Of Sales &pound;<?php echo number_format($total_sales,2,'.','');?></td>
				  </tr>				
				</table> 
				<?php
				}
				?>				
				</div>
			    <p><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></p>
			</div>
			<?php
			 }
		  }

	  }else if($data['report_type']=='AverageByDate'){

		  if($data['display_type']=='Daily'){

			$sql = "SELECT count(o.order_id )as ordcnt,DATE_FORMAT(o.shipped_date,'%d/%m/%Y')as odate,o.shipped_date FROM `3bit_orders` o WHERE (o.status ='Shipped') AND o.payment_status !='Declined (General).' ";

			// Concate with query //

			if(trim($end_date) != "" && trim($start_date) != ""){	 	
				$monSArr	=	explode("/",$start_date);
				$newSDate	=	$monSArr[2]."-".$monSArr[1]."-".$monSArr[0];
				$monEArr	=	explode("/",$end_date);
				$newEDate	=	$monEArr[2]."-".$monEArr[1]."-".$monEArr[0];	
				$sql .= " AND DATE(o.shipped_date)  <= '".$newEDate."' AND DATE(o.shipped_date) >= '".$newSDate."'  "; 
			}		

			$sql	.=	" GROUP BY DATE_FORMAT(o.shipped_date,'%d/%m/%Y') ORDER BY o.shipped_date ASC";	

			$res	=	$this->_db->fetchAll($sql);

			if(count($res)>0){				
				$daytotal	=	0;
				$average_total=	0;
				$daynums	=	0;
		?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
			  <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Day</th>
					<th>No Of Orders</th>
					<th>Average Times Of Orders (Hours)</th>			
				</tr>
				</thead>
		<?php foreach($res as $row){ 
					$daytotal	+=	$row['ordcnt'];							
					$selOrder	=	$this->_db->fetchAll("SELECT order_id FROM 3bit_orders WHERE DATE_FORMAT(shipped_date,'%d/%m/%Y')= '".$row['odate']."' AND status='Shipped' ");
					$orderid	=	array();
					if(count($selOrder)>0){
						foreach($selOrder as $ares){
							$orderid[] =	$ares['order_id'];
						}
					}
					$total_hour	=	0;
					for($i=0;$i<count($orderid);$i++){
						$new_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'New' AND order_id='".$orderid[$i]."'");
						$ship_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'Shipped' AND order_id='".$orderid[$i]."'");
						$diffhr		=	$this->getdayDiff($ship_date,$new_date);
						$total_hour	+=	$diffhr;
						//echo $orderid[$i]."===".$new_date."===".$ship_date."===".$diffhr."<br />";
					}
					if($total_hour>0){
						$average_hour	=	round($total_hour/count($orderid));	
						$daynums++;
					}

					$average_total	+=		$average_hour;	
						
		?>

				<tr>
					<td><?php echo $row['odate'];?></td>
					<td><?php echo $row['ordcnt'];?></td>	
					<td><?php echo $average_hour;?></td>
				</tr>
				
		<?php 	}?>	
				 <tr>
					<td><b>Totals</b></td>					
					<td><b><?php echo $daytotal;?></b></td>			
					<td><b><?php echo round($average_total/$daynums)?></b></td>
				</tr>
				<tr>
					<td colspan='3'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
				</tr>
			</table>
			</div>
			</div>
		<?php	}
		  }else if($data['display_type']=='Weekly'){
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
			    $datearr	=	$this->get_days($sdate,$edate);
				$cntday		=	count($datearr);
				$weeknums	=	0;
				if($cntday>0){
					$weektot	=	0;						
		?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
			  <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Week</th>
					<th>No Of Orders</th>
					<th>Average Times Of Orders (Hours)</th>			
				</tr>
				</thead>
				<?php	for($i=0;$i<$cntday;$i++){
							$exp	=	explode("/",$datearr[$i]);						
							$m		=	$exp[1];
							$d		=	$exp[0];
							$y		=	$exp[2];
							$g		=	date("W", mktime(0, 0, 0, $m, $d, $y));
							$res		=	$this->_db->fetchAll("SELECT count(o.order_id )as ordcnt,WEEK(o.shipped_date,1) as oweek,YEAR(o.shipped_date)as oyear,o.shipped_date FROM `3bit_orders` o WHERE WEEK(o.shipped_date,1)='".$g."' AND YEAR(o.shipped_date)='".$y."' AND (o.status ='Shipped') GROUP BY WEEK(o.shipped_date,1),YEAR(o.shipped_date) ORDER BY o.shipped_date ASC");
							if(count($res)>0){
								foreach($res as $row){ 
									$weektot	+=	$row['ordcnt'];		
									$selOrder	=	$this->_db->fetchAll("SELECT order_id FROM 3bit_orders WHERE WEEK(shipped_date,1)='".$row['oweek']."' AND YEAR(shipped_date)='".$row['oyear']."' AND status='Shipped' ");									
									$orderid	=	array();
									if(count($selOrder)>0){
										foreach($selOrder as $ares){
											$orderid[] =	$ares['order_id'];
										}
									}

									$total_hour	=	0;
									for($j=0;$j<count($orderid);$j++){
										$new_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'New' AND order_id='".$orderid[$j]."'");
										$ship_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'Shipped' AND order_id='".$orderid[$j]."'");
										$diffhr		=	$this->getdayDiff($ship_date,$new_date);
										$total_hour	+=	$diffhr;
										//echo $orderid[$j]."===".$new_date."===".$ship_date."===".$diffhr."<br />";
									}
									if($total_hour>0){
										$average_hour	=	round($total_hour/count($orderid));		
										$weeknums++;
									}								

									$average_total	+=		$average_hour;									
				?>
								<tr>
									<td><?php echo $datearr[$i];?></td>
									<td><?php echo $row['ordcnt'];?></td>	
									<td><?php echo $average_hour;?></td>
								</tr>
				<?php			}
							}
						}
				?>
				 <tr>
					<td><b>Totals</b></td>					
					<td><b><?php echo $weektot;?></b></td>			
					<td><b><?php echo round($average_total/$weeknums)?></b></td>
				</tr>
				<tr>
					<td colspan='3'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
				</tr>
			</table>
			</div>
			</div>

		<?php }

		 }else if($data['display_type']=='Monthly'){
			$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
			$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
			$datefrom	=	strtotime($sdate, 0);
			$dateto		=	strtotime($edate, 0);
			$months		=	$this->get_months($sdate,$edate);
			$cntday		=	count($months);			
			if($cntday>0){
				$monthtot	=	0;
				$monthnums	=	0;
			?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
			  <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Month</th>
					<th>No Of Orders</th>
					<th>Average Times Of Orders (Hours)</th>			
				</tr>
				</thead>
			<?php	for($i=0;$i<$cntday;$i++){
						$exp		=	explode(" ",$months[$i]);
						$m			=	$exp[0];
						$y			=   $exp[2];
						$dispmon	=   $exp[1]." ".$exp[2];	
						$res		=	$this->_db->fetchAll("SELECT count(o.order_id )as ordcnt,MONTH(o.shipped_date) as omonth,YEAR(o.shipped_date)as oyear,o.shipped_date FROM `3bit_orders` o WHERE MONTH(o.shipped_date)='".$m."' AND YEAR(o.shipped_date)='".$y."' AND (o.status ='Shipped') GROUP BY DATE_FORMAT(o.shipped_date,'%m/%Y') ORDER BY o.shipped_date ASC");
						if(count($res)>0){
							foreach($res as $row){ 
								$monthtot	+=	$row['ordcnt'];		
								$selOrder	=	$this->_db->fetchAll("SELECT order_id FROM 3bit_orders WHERE MONTH(shipped_date)='".$row['omonth']."' AND YEAR(shipped_date)='".$row['oyear']."' AND status='Shipped' ");									
								$orderid	=	array();
								if(count($selOrder)>0){
									foreach($selOrder as $ares){
										$orderid[] =	$ares['order_id'];
									}
								}

								$total_hour	=	0;
								for($j=0;$j<count($orderid);$j++){
									$new_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'New' AND order_id='".$orderid[$j]."'");
									$ship_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'Shipped' AND order_id='".$orderid[$j]."'");
									$diffhr		=	$this->getdayDiff($ship_date,$new_date);
									$total_hour	+=	$diffhr;
									//echo $orderid[$j]."===".$new_date."===".$ship_date."===".$diffhr."<br />";
								}
								if($total_hour>0){
									$average_hour	=	round($total_hour/count($orderid));		
									$monthnums++;
								}								

								$average_total	+=		$average_hour;	
			?>
			<tr>
									<td><?php echo $dispmon;?></td>
									<td><?php echo $row['ordcnt'];?></td>	
									<td><?php echo $average_hour;?></td>
								</tr>
			<?php
							}
						}						
					}
			?>
				 <tr>
					<td><b>Totals</b></td>					
					<td><b><?php echo $monthtot;?></b></td>			
					<td><b><?php echo round($average_total/$monthnums)?></b></td>
				</tr>
				<tr>
					<td colspan='3'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
				</tr>
			</table>
			</div>
			</div>
		<?php
			}
		 }else if($data['display_type']=='Yearly'){			  			
				$sdate		=	date("Y-m-d", mktime(0, 0, 0, $smon, $sday, $syer));
				$edate		=	date("Y-m-d", mktime(0, 0, 0, $emon, $eday, $eyer));
				$datefrom	=	strtotime($sdate, 0);
				$dateto		=	strtotime($edate, 0);
				$years		=	$this->get_years($sdate,$edate);
				$cntday		=	count($years);
				if($cntday>0){
					$yeartot	=	0;
					$yearnums	=	0;
		?>
			<div class="report_tabs">	  
			  <ul>
				<li><a href="#details">Details</a></li>
			  </ul>
			  <div id="details">
			  <table width="100%" cellspacing="0" cellpadding="0" border='0' class="display">
				<thead>
				<tr>
					<th>Year</th>
					<th>No Of Orders</th>
					<th>Average Times Of Orders (Hours)</th>			
				</tr>
				</thead>
		 <?php	for($i=0;$i<$cntday;$i++){					
					$y			=   $years[$i];
					$res		=	$this->_db->fetchAll("SELECT count(o.order_id )as ordcnt,YEAR(o.shipped_date)as oyear,o.shipped_date FROM `3bit_orders` o WHERE YEAR(o.shipped_date)='".$y."' AND (o.status ='Shipped') GROUP BY DATE_FORMAT(o.shipped_date,'%Y') ORDER BY o.shipped_date ASC");
					if(count($res)>0){
						foreach($res as $row){ 
							$yeartot	+=	$row['ordcnt'];		
							$selOrder	=	$this->_db->fetchAll("SELECT order_id FROM 3bit_orders WHERE YEAR(shipped_date)='".$row['oyear']."' AND status='Shipped' ");	
							$orderid	=	array();
							if(count($selOrder)>0){
								foreach($selOrder as $ares){
									$orderid[] =	$ares['order_id'];
								}
							}
							$total_hour	=	0;
							for($j=0;$j<count($orderid);$j++){
								$new_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'New' AND order_id='".$orderid[$j]."'");
								$ship_date	=	$this->_db->fetchOne("SELECT action_date FROM 3bit_order_actions WHERE action= 'Shipped' AND order_id='".$orderid[$j]."'");
								$diffhr		=	$this->getdayDiff($ship_date,$new_date);
								$total_hour	+=	$diffhr;
								//echo $orderid[$j]."===".$new_date."===".$ship_date."===".$diffhr."<br />";
							}
							if($total_hour>0){
								$average_hour	=	round($total_hour/count($orderid));		
								$yearnums++;
							}								

							$average_total	+=		$average_hour;	
				?>
				<tr>
					<td><?php echo $y;?></td>
					<td><?php echo $row['ordcnt'];?></td>	
					<td><?php echo $average_hour;?></td>
				</tr>
				<?php }
					}
				 }
				?>
				<tr>
					<td><b>Totals</b></td>					
					<td><b><?php echo $yeartot;?></b></td>			
					<td><b><?php echo round($average_total/$yearnums)?></b></td>
				</tr>
				<tr>
					<td colspan='3'><a class="button" href="<?=$this->_baseurl?>/reports/exportxls"><img src="<?=$this->_baseurl?>/public/images/icons/page_white_excel.png" width="16" height="16" border="0" /><b>Export</b></a></td>
				</tr>
			</table>
			</div>
			</div>
		<?php			
			}
		 }
	  }
	 return $results;
   }

   private function getdayDiff($sdate,$edate)
   {
		$cur	 	=	explode(" ",$sdate);
		$curdate 	=	explode("-",$cur[0]);
		$tme	 	=	$cur[1];
		$curtime 	=	explode(":",$tme);

		$split 		=	explode(" ",$edate);
		$date 		= 	$split[0];
		$time 		= 	$split[1];
		$datesplit 	= 	explode("-",$date);
		$timesplit 	= 	explode(":",$time);
		
		$first	 	= 	mktime($curtime[0], $curtime[1], $curtime[2], $curdate[1], $curdate[2], $curdate[0]);
		$second  	= 	mktime($timesplit[0], $timesplit[1], $timesplit[2], $datesplit[1],$datesplit[2], $datesplit[0]);

		$difference = $first - $second;
		$diffday	= floor($difference/86400);
		$diffhour	= floor(($difference % 86400) / 3600);
		$diffminute = floor((($difference - ($diffhour * 3600)) % 86400) / 60);
		$diffsec	= floor((($difference - ($diffminute * 60) - ($diffhour * 3600)) % 86400) / 1);

		$diffhr = $diffday * 24 + $diffhour;

		return $diffhr;
	}

	public function getPortalStats(){
		
		$portalstats	=	$this->_db->fetchAll("SELECT count(*) as ucount , DATE_FORMAT(created_date,'%M-%Y')as pmonth FROM 3bit_portal_users WHERE terms_conditions='1'  GROUP BY DATE_FORMAT(created_date,'%M-%Y') ORDER BY created_date ASC");	

		return $portalstats;

	}

	public function all_products_list($prodid)
	{		
		// select products ================================

		$selProd	=	$this->_db->fetchAll("SELECT * FROM 3bit_products WHERE 1 GROUP BY product_id ORDER BY product_name");

		$prodDrop	=	'';

		if(count($selProd)>0){
			$prodDrop	.=	'<optgroup label="Products">';
			foreach($selProd as $prow){
				$psel	=	'';
				$pvar=	'';
				if($prodid==$prow['product_id'])
					$psel		=	'selected';	

				if(!empty($prow['size']))
					$pvar	=		"&nbsp;(".$prow['size'].")";
				if(!empty($prow['style']))
					$pvar	=		"&nbsp;(".$prow['style'].")";

				$prodDrop	.=	'<option value="'.$prow['product_id'].'" '.$psel.'>'.$prow['product_name'].$pvar.'</option>'."\n";
			} 			
		}  
		return 	$prodDrop;

	}

	public function sales_list($data)
	{		
		if(!empty($data['txt_prod'])){			
			$prodid			=	$data['txt_prod'];		
		}

		$selEmail	=		$this->_db->fetchAll("SELECT DATE_FORMAT(o.ordered_date,'%d/%m/%Y') as odate,od.product_id,od.quantity,od.order_id,o.customer_id,c.email,concat(c.fname,' ',c.lname)as Name FROM 3bit_order_details od, 3bit_orders o,3bit_customers c WHERE o.order_id=od.order_id AND o.customer_id = c.customer_id AND od.product_id = '".$prodid."' AND o.status='Shipped' ORDER BY o.order_id");		

		if(count($selEmail)>0){			
			return $selEmail;
		}	

	}

}

?>