<?php

class Application_Model_Messages
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    

    public function getUsers()
    {
        $sql = "SELECT user_id,concat(user_firstname,' ',user_lastname)as uname FROM 3bit_users ORDER BY user_firstname";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function getFields()
	{
		$result	=	$this->_db->query("SELECT * FROM 3bit_orders, 3bit_order_details,3bit_customers,3bit_customers_address LIMIT 0,1");
		$res	=	$result->fetchAll();
		if(is_array($res[0])){
			return (array_keys($res['0'])); 
		}
	}

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_messages WHERE message_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_messages','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		if(!empty($data['message_to']))
			$to		=	'Customer';		
		else
			$to		=	'Admin';

		if(!empty($data['email_status']))
			$estatus=	1;		
		else
			$estatus=	0;

		$sql	=	"INSERT INTO 3bit_messages SET
								`message_type` 		=	'".$data['message_type']."',
								`message_to`		=	'".$to."',
								`user_id`			=	'".$data['user_id']."',
								`mail_subject`		=	'".addslashes($data['mail_subject'])."',
								`message_text` 		=	'".addslashes($data['message_text'])."',
								`text`				=	'".addslashes($data['text'])."',
								`email_status` 		=	'".$estatus."',
								`active`		 	=	'".$data['active']."',
								`created_date`		=	now(),
								`modified_date` 	=	now()";

		$query		=	$this->_db->getConnection()->query($sql);

		$message_id	=	$this->_db->lastInsertId();	
		
		$audit		=	new Application_Model_Audits();

		$audit->insert_audit('3bit_messages','add',$message_id,'success');
    }

	public function update($data)
	{
		if(!empty($data['message_to']))
			$to		=	'Customer';		
		else
			$to		=	'Admin';

		if(!empty($data['email_status']))
			$estatus=	1;		
		else
			$estatus=	0;

		$sql	=	"UPDATE 3bit_messages SET
								`message_type` 		=	'".$data['message_type']."',
								`message_to`		=	'".$to."',
								`user_id`			=	'".$data['user_id']."',
								`mail_subject`		=	'".addslashes($data['mail_subject'])."',
								`message_text` 		=	'".addslashes($data['message_text'])."',
								`text`				=	'".addslashes($data['text'])."',
								`email_status` 		=	'".$estatus."',
								`active`		 	=	'".$data['active']."',			
								`modified_date`		=	now() WHERE message_id='".$data['id']."'";
		$query	=	$this->_db->getConnection()->query($sql);

		$message_id	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_messages','update',$message_id,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_messages",'message_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_messages','delete',$id,'success');
	}
}

?>