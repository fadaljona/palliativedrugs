<?php

class Application_Model_Usertypes
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_user_types WHERE type_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_user_types','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$modules	=	'';
		if(!empty($data['module_user']))
			$modules	.=	$data['module_user'].",";
		if(!empty($data['module_pages']))
			$modules	.=	$data['module_pages'].",";
		if(!empty($data['module_formulary']))
			$modules	.=	$data['module_formulary'].",";
		if(!empty($data['module_members']))
			$modules	.=	$data['module_members'].",";
		if(!empty($data['module_news']))
			$modules	.=	$data['module_news'].",";
		if(!empty($data['module_products']))
			$modules	.=	$data['module_products'].",";
		if(!empty($data['module_customers']))
			$modules	.=	$data['module_customers'].",";	
		if(!empty($data['module_orders']))
			$modules	.=	$data['module_orders'].",";	
		if(!empty($data['module_reports']))
			$modules	.=	$data['module_reports'].",";	
		if(!empty($data['module_assets']))
			$modules	.=	$data['module_assets'].",";	
		if(!empty($data['module_sddb']))
			$modules	.=	$data['module_sddb'].",";	

		$modules	=	substr($modules,0,-1);

		$sql	=	"INSERT INTO 3bit_user_types SET
								title				=	'".addslashes($data['title'])."',
								module_permissions	=	'".$modules."',
								status				=	'".$data['status']."',
								created_date		=	now(),
								modified_date		=	now()";

		$query	=	$this->_db->query($sql);

		$typeid	=	$this->_db->lastInsertId();
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_user_types','add',$typeid,'success');
    }

	public function update($data)
	{
		$modules	=	'';
		if(!empty($data['module_user']))
			$modules	.=	$data['module_user'].",";
		if(!empty($data['module_pages']))
			$modules	.=	$data['module_pages'].",";
		if(!empty($data['module_formulary']))
			$modules	.=	$data['module_formulary'].",";
		if(!empty($data['module_members']))
			$modules	.=	$data['module_members'].",";
		if(!empty($data['module_news']))
			$modules	.=	$data['module_news'].",";
		if(!empty($data['module_products']))
			$modules	.=	$data['module_products'].",";
		if(!empty($data['module_customers']))
			$modules	.=	$data['module_customers'].",";	
		if(!empty($data['module_orders']))
			$modules	.=	$data['module_orders'].",";	
		if(!empty($data['module_reports']))
			$modules	.=	$data['module_reports'].",";	
		if(!empty($data['module_assets']))
			$modules	.=	$data['module_assets'].",";
		if(!empty($data['module_sddb']))
			$modules	.=	$data['module_sddb'].",";	
			
	
		$modules	=	substr($modules,0,-1);

		$sql	=	"UPDATE 3bit_user_types SET
								title				=	'".addslashes($data['title'])."',
								module_permissions	=	'".$modules."',
								status				=	'".$data['status']."',								
								modified_date		=	now() WHERE type_id='".$data['id']."'";
		$query	=	$this->_db->query($sql);

		$typeid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_user_types','update',$typeid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_user_types",'type_id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_user_types','delete',$id,'success');
	}
}

?>