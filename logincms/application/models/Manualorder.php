<?php

class Application_Model_Manualorder
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
		$this->_baseurl	= Zend_Registry::get("baseUrl");
    }       

	public function addtocart($data)
	{

		if(!empty($data)){

		   if(!empty($data['product_id'])){
			   $selprice		=	$this->_db->fetchAll("SELECT price,sale_price FROM 3bit_products WHERE product_id ='".$data['product_id']."'");
				if(count($selprice)>0){
					$pro_price	=	$selprice[0]['price'];
					$sale_price	=	$selprice[0]['sale_price'];
					if($sale_price>0)
						$pro_price	=$sale_price;	
				}
		   }
		   $selProd		=	$this->_db->fetchAll("SELECT cart_id FROM 3bit_cart_items WHERE cart_sessionid =	'".$data['sess_id']."' AND product_id ='".$data['product_id']."'");

			if(count($selProd)>0){
				$cart_id		=	$selProd[0]['cart_id'];
				$update_cart	=	$this->_db->query("UPDATE 3bit_cart_items SET 
													cart_sessionid		=	'".$data['sess_id']."',
													product_id			=	'".$data['product_id']."',
													membership_id		=	'".$data['membership_id']."',
													event_id			=	'".$data['event_id']."',
													quantity			=	quantity+1,
													price				=	'".$pro_price."',	
													browser				=	'".$_SERVER['HTTP_USER_AGENT']."',
													ip					=	'".$_SERVER['REMOTE_ADDR']."',
													modified_date		=	now() WHERE cart_id='".$cart_id."'");			
			}else{
				$insert_cart	=	$this->_db->query("INSERT INTO 3bit_cart_items SET 
													cart_sessionid		=	'".$data['sess_id']."',
													product_id			=	'".$data['product_id']."',
													membership_id		=	'".$data['membership_id']."',
													event_id			=	'".$data['event_id']."',
													quantity			=	'1',
													price				=	'".$pro_price."',
													browser				=	'".$_SERVER['HTTP_USER_AGENT']."',
													ip					=	'".$_SERVER['REMOTE_ADDR']."',
													created_date		=	now(),
													modified_date		=	now()");
			}

			$cart_items	=	$this->getcartitems($data['sess_id'],'');

			$cart_items	=	$cart_items."^S";
			
			die($cart_items);

		 }
	  }

	  public function updatetocart($data)
	  {	
		if(!empty($data)){
			$cartarr	=	explode(",",$data['cartid']);
			$qtyarr		=	explode(",",$data['qutys']);
			$cartcnt	=	count($cartarr)-1;
			for($k=0;$k<$cartcnt;$k++){
				$cart_id	=	$cartarr[$k];
				$qty		=	$qtyarr[$k];
				if($qty==0){
				   $update_cart	=	$this->_db->query("DELETE FROM 3bit_cart_items WHERE cart_id='".$cart_id."'");
				}
				$updatesql	=	 "UPDATE 3bit_cart_items SET quantity = '".$qty."',modified_date=now() WHERE cart_id='".$cart_id."'";

				$update_cart	=	$this->_db->query($updatesql);

			}
			$cart_items	=	$this->getcartitems($data['sessid'],'');

			$cart_items	=	$cart_items."^S";
			
			die($cart_items);
		 }					 
	  }

	  public function deletefromcart($data)
	  {
		 if(!empty($data)){

			 if(!empty($data['cartid'])){				
				$update_cart=	$this->_db->query("DELETE FROM 3bit_cart_items WHERE cart_id='".$data['cartid']."'");
			 }	
			 $cart_items	=	$this->getcartitems($data['sessid'],'');

			 $cart_items	=	$cart_items."^S";
			
			 die($cart_items);
		 }
	  }

	  private function getcartitems($sessid,$ship_country='')
	  {
		  $sess_country			=		$this->_db->fetchOne("SELECT shipping_country FROM 3bit_customers_address WHERE cart_sessionid = '".$sessid."' AND status ='current' ");	 
		  if($ship_country>0){
			 $ship_country		=		$ship_country;
		  }else if($numadd>0){
			$ship_country		=		$sess_country;
		  }else{
			$ship_country		=		232;
		  }
		  
		  $is_eu				=		$this->_db->fetchOne("SELECT is_eu FROM 3bit_country WHERE id='".$ship_country."'");			  

		  $selCart		=	$this->_db->fetchAll("SELECT ci.* FROM `3bit_cart_items` ci WHERE ci.cart_sessionid = '".$sessid."' ORDER BY ci.cart_id");	
		  $cartrows		=	count($selCart);
		  if($cartrows>0){
			$sub_total		=	0;
			$vatprice		=	0;
			$totweight		=	0;
			$returnText		=	'<h1>Your shopping cart contents</h1>
								 <form name="frmCheckout" action="" method="post"> 
								  <table cellspacing="0" border="0" width="100%" class="cart_table">
									<thead>
									  <tr>
										<th class="product-column">Description</th>
										<th class="quantity">Qty*</th>
										<th class="remove">Remove</th>
										<th class="value">Price</th>
										<th class="value">Total</th>
									  </tr>
									</thead>
									<tbody>';
			$shipping_price			=		0;
			foreach($selCart as $row){ 
				$cart_id			=		$row['cart_id'];			
				$all_cid			.=		$cart_id.",";	
				if(!empty($row['product_id'])){					
					$selProd	=	$this->_db->fetchAll("SELECT p.* FROM 3bit_products p WHERE p.product_id='".$row['product_id']."' ");		
					if(count($selProd)>0){								
						foreach($selProd as $res){						
							$item_id		=	$res['product_id'];	
							$item_name 		= 	$res['product_name'];
							$item_code		=	$res['product_code'];
							$item_qty		=	$row['quantity'];
							$variation_type	=	'';
							if($res['size'] != '') 
								$variation_type = "<span>(".$res['size'].")</span>" ;
							else if($res['style'] != '') 
								$variation_type = "<span>(".$res['style'].")</span>" ;
							$item_price		=	$res['price'];
							$line_price		=	$item_qty*$item_price;
							$sub_total		+=	$line_price;
							if($is_eu=='1')
								$item_vat	=	$this->findProductvat($item_id);
							$totweight		+=	$res['weight'];
						}
					}
				}
				$item_vat		=	$item_qty*$item_vat;			
				$vatprice		+=	$item_vat;
				$item_price		=	number_format($item_price, 2, '.', '');
				$line_price		=	number_format($line_price, 2, '.', '');
				$sub_total		=	number_format($sub_total, 2, '.', '');
				
				$returnText		.=	'<tr>
										<td class="left-align">'.$item_name.$variation_type.'</td>
										<td class="input"><input name="quantity'.$cart_id.'" id="quantity'.$cart_id.'" type="text" value="'.$item_qty.'" onkeypress="return blockNumbers(event)" size="2" /></td>
										<td class="image"><a href="javascript:void(0);"  id="remove_'.$cart_id.'" onclick=deletefromcart("'.$cart_id.'","'.$sessid.'");><img border="0" alt="Delete" title="Delete" src="'.$this->_baseurl.'/public/images/rule_component_remove.gif" /></a></td>
										<td class="value">'.$item_price.' GBP</td>
										<td class="value">'.$line_price.' GBP</td>
									  </tr>';


			}
			$basket_total	=	number_format($sub_total, 2, '.', '');			
			$item_ship		=	$this->getShippingCostbyWeight($totweight,$ship_country);
			$item_vat		=	$vatprice;		
			if($item_vat>0 && $item_ship>0){
				$shipvat		=	$item_ship-($item_ship/$this->liveRate());
				$shipvat		=	number_format($shipvat, 2, '.', '');
			}
			$vatprice		=	($vatprice+$shipvat);
			$item_vat		=	number_format($vatprice, 2, '.', '');
			$final_total	=	number_format(($basket_total+$item_ship), 2, '.', '');
			$returnText		.=	'</tbody>							
								<input type="hidden" id="cart_id" name="cart_id" value="'.$all_cid.'" />
								<tfoot>	  
								  <tr>
									<td colspan="4">Sub Total</td>
									<td class="value">'.$sub_total.' GBP</td>
								  </tr>	 
								  <tr>
									<td colspan="4">TAX (inc)</td>
									<td class="value">'.$item_vat.' GBP</td>
								  </tr>
								  <tr>
									<td colspan="4">Shipping Charge</td>
									<td class="value">'.$item_ship.' GBP</td>
								  </tr>
								  <tr>
									<td colspan="4"><b>Grand Total</b></td>
									<td class="value"><b>'.$final_total.' GBP</b></td>
								  </tr>
								</tfoot>	
							  </table>
							  <div class="cart_action_buttons">  
								  <a class="button" href="JavaScript:;" onclick=\'updatetocart(document.frmCheckout,"'.$sessid.'")\'>Update Cart</a>
								  <a class="button" href="JavaScript:;" onclick=\'cartsubmit(document.frmCheckout)\'>Continue</a>
							  </div>
							</form>';

		  }else{

				$returnText		=	'<table cellspacing="0" border="0" class="cart_table">
									<thead>
									  <tr>
										<th colspan="4">Your cart is empty!</th>
									  </tr>
									</thead>	
									</table>';
		  }

		 return $returnText;
	}

	public function get_cartitems()
	{
		  $sessid				=		$_SESSION['wlsession']['asess_id'];
		  $selAdd				=		$this->_db->fetchOne("SELECT shipping_country FROM 3bit_customers_address WHERE cart_sessionid = '".$sessid."' AND status ='current' ");	
		  $numadd				=		count($selAdd);
		  if($numadd>0){
			$ship_country		=		$selAdd;
		  }else{
			$ship_country		=		232;
		  }
		  $is_eu				=		$this->_db->fetchOne("SELECT is_eu FROM 3bit_country WHERE id='".$ship_country."'");
		 
		  $selCart				=		$this->_db->fetchAll("SELECT ci.* FROM `3bit_cart_items` ci WHERE ci.cart_sessionid = '".$sessid."' ORDER BY ci.cart_id");	
		  $cartrows					=		count($selCart);
		  if($cartrows>0){
			$sub_total		=	0;
			$vatprice		=	0;
			$returnText		=	'<h1>Your shopping cart contents</h1>
								 <form name="frmCheckout" action="" method="post"> 
								  <table cellspacing="0" border="0" width="100%" class="cart_table">
									<thead>
									  <tr>
										<th class="product-column">Description</th>
										<th class="quantity">Qty*</th>
										<th class="remove">Remove</th>
										<th class="value">Price</th>
										<th class="value">Total</th>
									  </tr>
									</thead>
									<tbody>';
			$shipping_price			=		0;
			$all_cid				=		'';
			$item_vat				=		0;
			$totweight				=		0;
			foreach($selCart as $row){ 
				$cart_id			=		$row['cart_id'];			
				$all_cid			.=		$cart_id.",";				
				$variation_type		=		'';
				if(!empty($row['product_id'])){					
					$selProd	=	$this->_db->fetchAll("SELECT p.* FROM 3bit_products p WHERE p.product_id='".$row['product_id']."' ");		
					if(count($selProd)>0){								
						foreach($selProd as $res){						
							$item_id		=	$res['product_id'];	
							$item_name 		= 	$res['product_name'];
							$item_code		=	$res['product_code'];
							$item_qty		=	$row['quantity'];	
							if($res['size'] != '') 
								$variation_type = "<span>(".$res['size'].")</span>" ;
							else if($res['style'] != '') 
								$variation_type = "<span>(".$res['style'].")</span>" ;
							$item_price		=	$res['price'];
							$line_price		=	$item_qty*$item_price;
							$sub_total		+=	$line_price;
							if($is_eu=='1')
								$item_vat	=	$this->findProductvat($item_id);
							$totweight		+=	$res['weight'];
						}
					}
				}
				$item_vat		=	$item_qty*$item_vat;			
				$vatprice		+=	$item_vat;
				$item_price		=	number_format($item_price, 2, '.', '');
				$line_price		=	number_format($line_price, 2, '.', '');
				$sub_total		=	number_format($sub_total, 2, '.', '');
				
				$returnText		.=	'<tr>
										<td class="left-align">'.$item_name.$variation_type.'</td>
										<td class="input"><input name="quantity'.$cart_id.'" id="quantity'.$cart_id.'" type="text" value="'.$item_qty.'" onkeypress="return blockNumbers(event)" size="2" /></td>
										<td class="image"><a href="javascript:void(0);"  id="remove_'.$cart_id.'" onclick=deletefromcart("'.$cart_id.'","'.$sessid.'");><img border="0" alt="Delete" title="Delete" src="'.$this->_baseurl.'/public/images/rule_component_remove.gif" /></a></td>
										<td class="value">'.$item_price.' GBP</td>
										<td class="value">'.$line_price.' GBP</td>
									  </tr>';


			}
			$basket_total	=	number_format($sub_total, 2, '.', '');			
			$item_ship		=	$this->getShippingCostbyWeight($totweight,$ship_country);
			$item_vat		=	$vatprice;
			$shipvat		=	0;
			if($item_vat>0 && $item_ship>0){
				$shipvat		=	$item_ship-($item_ship/$this->liveRate());
				$shipvat		=	number_format($shipvat, 2, '.', '');
			}
			$vatprice		=	($vatprice+$shipvat);
			$item_vat		=	number_format($vatprice, 2, '.', '');
			$final_total	=	number_format(($basket_total+$item_ship), 2, '.', '');
			$returnText		.=	'</tbody>							
								<input type="hidden" id="cart_id" name="cart_id" value="'.$all_cid.'" />
								<tfoot>	  
								  <tr>
									<td colspan="4">Sub Total</td>
									<td class="value">'.$sub_total.' GBP</td>
								  </tr>	 
								  <tr>
									<td colspan="4">TAX (inc)</td>
									<td class="value">'.$item_vat.' GBP</td>
								  </tr>
								  <tr>
									<td colspan="4">Shipping Charge</td>
									<td class="value">'.$item_ship.' GBP</td>
								  </tr>
								  <tr>
									<td colspan="4"><b>Grand Total</b></td>
									<td class="value"><b>'.$final_total.' GBP</b></td>
								  </tr>
								</tfoot>	
							  </table>
							  <div class="cart_action_buttons">  
								  <a class="button update-cart" href="JavaScript:;" onclick=\'updatetocart(document.frmCheckout,"'.$sessid.'")\'>Update Cart</a>
								  <a class="button" href="JavaScript:;" onclick=\'cartsubmit(document.frmCheckout)\'>Continue</a>
							  </div>
							</form>';

		  }else{

				$returnText		=	'<table cellspacing="0" border="0" class="cart_table">
									<thead>
									  <tr>
										<th colspan="4">Your cart is empty!</th>
									  </tr>
									</thead>	
									</table>';
		  }


		  return $returnText;

	}

	public function cart_summary()
	{
		  $sessid				=		$_SESSION['wlsession']['asess_id'];
		  $selAdd				=		$this->_db->fetchAll("SELECT shipping_country FROM 3bit_customers_address WHERE cart_sessionid = '".$sessid."' AND status ='current' ");	
		  $numadd				=		count($selAdd);
		  $cms_customer			=		'';
		  if(!empty($_SESSION['wlsession']['cms_customer'])){
			$selAdd					=		$this->_db->fetchAll("SELECT shipping_country FROM 3bit_customers_address WHERE customer_id = '".$_SESSION['wlsession']['cms_customer']."' ORDER BY modified_date DESC LIMIT 0,1");
			if(count($selAdd)>0){
					$ship_country			=		$selAdd[0]['shipping_country'];
					$cms_customer			=		$_SESSION['wlsession']['cms_customer'];
			}
		  }else if($numadd>0){
			$ship_country			=		$selAdd[0]['shipping_country'];
		  }else{
			$ship_country			=		232;
		  }
		  $is_eu					=		$this->_db->fetchOne("SELECT is_eu FROM 3bit_country WHERE id='".$ship_country."'");
		  $selCart					=		$this->_db->fetchAll("SELECT ci.* FROM `3bit_cart_items` ci WHERE ci.cart_sessionid = '".$sessid."' ORDER BY ci.cart_id"); 
		  $cartrows					=		count($selCart);
		  if($cartrows>0){
			$sub_total		=	0;
			$vatprice		=	0;
			$returnText		=	'<h1>Your shopping cart contents</h1>
								 <form name="frmSummary" action="" method="post">
								 <input type="hidden" name="sessid" value="'.$_SESSION['wlsession']['asess_id'].'">
								  <input type="hidden" name="customer_id" value="'.$cms_customer.'">
								  <table cellspacing="0" border="0" width="100%" class="cart_table">
									<thead>
									  <tr>
										<th class="product-column">Description</th>
										<th class="quantity">Qty*</th>										
										<th class="value">Price</th>
										<th class="value">Total</th>
									  </tr>
									</thead>
									<tbody>';
			$shipping_price			=		0;
			$all_cid				=		'';
			$item_vat				=		0;
			$totweight				=		0;
			foreach($selCart as $row){ 
				$cart_id			=		$row['cart_id'];			
				$all_cid			.=		$cart_id.",";	
				$variation_type		=		'';
				if(!empty($row['product_id'])){						
					$selProd	=	$this->_db->fetchAll("SELECT p.* FROM 3bit_products p WHERE p.product_id='".$row['product_id']."' ");					
					if(count($selProd)>0){								
						foreach($selProd as $res){						
							$item_id		=	$res['product_id'];	
							$item_name 		= 	$res['product_name'];
							$item_code		=	$res['product_code'];
							$item_qty		=	$row['quantity'];	
							if($res['size'] != '') 
								$variation_type = "<span>(".$res['size'].")</span>" ;
							if($res['style'] != '') 
								$variation_type = "<span>(".$res['style'].")</span>" ;
							$item_price		=	$res['price'];
							$line_price		=	$item_qty*$item_price;
							$sub_total		+=	$line_price;
							if($is_eu=='1')
								$item_vat	=	$this->findProductvat($item_id);
							$totweight		+=	$res['weight'];
						}
					}
				}
				$item_vat		=	$item_qty*$item_vat;			
				$vatprice		+=	$item_vat;
				$item_price		=	number_format($item_price, 2, '.', '');
				$line_price		=	number_format($line_price, 2, '.', '');
				$sub_total		=	number_format($sub_total, 2, '.', '');
				
				$returnText		.=	'<tr>
										<td class="left-align">'.$item_name.$variation_type.'</td>
										<td class="input">'.$item_qty.'</td>										
										<td class="value">'.$item_price.' GBP</td>
										<td class="value">'.$line_price.' GBP</td>
									  </tr>';


			}
			$basket_total	=	number_format($sub_total, 2, '.', '');			
			$item_ship		=	$this->getShippingCostbyWeight($totweight,$ship_country);
			$item_vat		=	$vatprice;
			$shipvat		=	0;
			if($item_vat>0 && $item_ship>0){
				$shipvat		=	$item_ship-($item_ship/$this->liveRate());
				$shipvat		=	number_format($shipvat, 2, '.', '');
			}
			$vatprice		=	($vatprice+$shipvat);
			$item_vat		=	number_format($vatprice, 2, '.', '');
			$final_total	=	number_format(($basket_total+$item_ship), 2, '.', '');
			$returnText		.=	'</tbody>							
								<input type="hidden" id="cart_id" name="cart_id" value="'.$all_cid.'" />
								<tfoot>	  
								  <tr>
									<td colspan="3">Sub Total</td>
									<td class="value">'.$sub_total.' GBP</td>
								  </tr>	 
								  <tr>
									<td colspan="3">TAX (inc)</td>
									<td class="value">'.$item_vat.' GBP</td>
								  </tr>
								  <tr>
									<td colspan="3">Shipping Charge</td>
									<td class="value">'.$item_ship.' GBP</td>
								  </tr>
								  <tr>
									<td colspan="3"><b>Grand Total</b></td>
									<td class="value"><b>'.$final_total.' GBP</b></td>
								  </tr>
								</tfoot>	
							  </table>
							  <div class="cart_action_buttons">  								  
								  <a class="button" href="'.$this->_baseurl.'/manualorder" >Back</a>
							  </div>
							</form>';

		  }else{

				$returnText		=	'<table cellspacing="0" border="0" class="cart_table">
									<thead>
									  <tr>
										<th colspan="4">Your cart is empty!</th>
									  </tr>
									</thead>	
									</table>';
		  }


		  return $returnText;

	}

	public function manualcustomer($data)
	{
		/*$_SESSION['wlsession']['cms_customer']	=	$data['customer_id'];
		$selAdd						=	$this->_db->fetchAll("SELECT address_id FROM 3bit_customers_address WHERE customer_id='".$_SESSION['wlsession']['cms_customer']."' ORDER BY modified_date DESC LIMIT 0,1");
		if(count($selAdd)>0){
			$_SESSION['wlsession']['cms_custaddr']=	$selAdd[0]['address_id'];

		}
		die($data['customer_id']);*/

		$selUser		=	$this->_db->fetchAll("SELECT * FROM new_users WHERE id='".$data['customer_id']."'");

		if(count($selUser)>0){

			foreach($selUser as $brow){

				$brow		=	array_map("stripslashes",$brow);

				$btitle		=	$brow['title'];

				$bfirst		=	$brow['FirstName'];

				$blast		=	$brow['LastName'];

				$bemail		=	$brow['email'];

				$baddress1	=	$brow['addr1'];

				$baddress2	=	$brow['addr2'];

				$bcity		=	$brow['TownOrCity'];

				$bstate		=	$brow['StateOrCounty'];

				$bzip		=	$brow['PostalCode'];

				$bphone		=	$brow['phone'];

				$country_org=	$this->_db->fetchOne("SELECT id FROM 3bit_country WHERE LOWER(country) ='".$brow['Country']."'");

				if($country_org){
					$bcountry	=	$country_org;					
				}
			}

			$chkCustomer	=	$this->_db->fetchAll("SELECT customer_id,address_id FROM 3bit_customers_address WHERE cart_sessionid ='".$data['sessid']."'");

			if(count($chkCustomer)==0){

				$insertQry		=	"INSERT INTO 3bit_customers SET
													title		=	'".addslashes($btitle)."',
													fname		=	'".addslashes($bfirst)."',
													lname		=	'".addslashes($blast)."',
													email		=	'".addslashes($bemail)."',
													address1	=	'".addslashes($baddress1)."',
													address2	=	'".addslashes($baddress2)."',
													city		=	'".addslashes($bcity)."',
													state		=	'".addslashes($bstate)."',
													county		=	'',
													country		=	'".addslashes($bcountry)."',
													postcode	=	'".addslashes($bzip)."',
													phone		=	'".addslashes($bphone)."',
													password	=	'',											
													browser		=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
													ip			=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
													created_date=	now(),
													modified_date=	now() ";
				if($this->_db->query($insertQry)){

					$customer_id	=	$this->_db->lastInsertId();

					$insertAddress	=	"INSERT INTO 3bit_customers_address SET
												customer_id		=	'".$customer_id."',
												cart_sessionid	=	'".$data['sessid']."',
												email			=	'".addslashes($bemail)."',
												phone			=	'".addslashes($bphone)."',
												billing_title	=	'".addslashes($btitle)."',
												billing_fname	=	'".addslashes($bfirst)."',
												billing_lname	=	'".addslashes($blast)."',
												billing_address1=	'".addslashes($baddress1)."',
												billing_address2=	'".addslashes($baddress2)."',
												billing_city	=	'".addslashes($bcity)."',
												billing_state	=	'".addslashes($bstate)."',
												billing_country	=	'".addslashes($bcountry)."',
												billing_postcode=	'".addslashes($bzip)."',
												billing_phone	=	'".addslashes($bphone)."',																						
												shipping_title	=	'".addslashes($btitle)."',
												shipping_fname	=	'".addslashes($bfirst)."',
												shipping_lname	=	'".addslashes($blast)."',
												shipping_address1=	'".addslashes($baddress1)."',
												shipping_address2=	'".addslashes($baddress2)."',
												shipping_city	=	'".addslashes($bcity)."',
												shipping_state	=	'".addslashes($bstate)."',
												shipping_country=	'".addslashes($bcountry)."',
												shipping_postcode=	'".addslashes($bzip)."',
												shipping_phone	=	'".addslashes($bphone)."',
												is_diffdeliver	=	'0',
												created_date	=	now(),
												modified_date	=	now()" ;

					}
					$this->_db->query($insertAddress);
					$address_id		=	$this->_db->lastInsertId();	
					
			}else{
					$customer_id	=	$chkCustomer[0]['customer_id'];
					$address_id		=	$chkCustomer[0]['address_id'];
					$updateQry		=	"UPDATE 3bit_customers SET
												title			=	'".addslashes($btitle)."',
												fname			=	'".addslashes($bfirst)."',
												lname			=	'".addslashes($blast)."',
												email			=	'".addslashes($bemail)."',
												address1		=	'".addslashes($baddress1)."',
												address2		=	'".addslashes($baddress2)."',
												city			=	'".addslashes($bcity)."',
												state			=	'".addslashes($bstate)."',
												county			=	'',
												country			=	'".addslashes($bcountry)."',
												postcode		=	'".addslashes($bzip)."',
												phone			=	'".addslashes($bphone)."',
												password		=	'',							
												browser			=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
												ip				=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
												modified_date=	now() WHERE customer_id ='".$customer_id."'";

				if($this->_db->query($updateQry)){
				
					$updateAddress	=	"UPDATE 3bit_customers_address SET
												customer_id		=	'".$customer_id."',
												cart_sessionid	=	'".$data['sessid']."',
												email					=	'".addslashes($bemail)."',
												phone					=	'".addslashes($bphone)."',
												billing_title	=	'".addslashes($btitle)."',
												billing_fname	=	'".addslashes($bfirst)."',
												billing_lname	=	'".addslashes($blast)."',
												billing_address1=	'".addslashes($baddress1)."',
												billing_address2=	'".addslashes($baddress2)."',
												billing_city	=	'".addslashes($bcity)."',
												billing_state	=	'".addslashes($bstate)."',
												billing_country	=	'".addslashes($bcountry)."',
												billing_postcode=	'".addslashes($bzip)."',
												billing_phone	=	'".addslashes($bphone)."',																						
												shipping_title	=	'".addslashes($btitle)."',
												shipping_fname	=	'".addslashes($bfirst)."',
												shipping_lname	=	'".addslashes($blast)."',
												shipping_address1=	'".addslashes($baddress1)."',
												shipping_address2=	'".addslashes($baddress2)."',
												shipping_city	=	'".addslashes($bcity)."',
												shipping_state	=	'".addslashes($bstate)."',
												shipping_country=	'".addslashes($bcountry)."',
												shipping_postcode=	'".addslashes($bzip)."',
												shipping_phone	=	'".addslashes($bphone)."',
												is_diffdeliver	=	'0',												
												modified_date	=	now() WHERE address_id = '".$address_id."' AND cart_sessionid	= '".$data['sessid']."' ";
					$this->_db->query($updateAddress);								
				}					
			}
			$_SESSION['wlsession']['cms_customer'] =	$customer_id;
			$_SESSION['wlsession']['cms_custaddr'] =	$address_id;
			$_SESSION['wlsession']['cms_frontuser']=	$data['customer_id'];
		?>					
			<div id="article_tabs">
			<ul>
			    <li><a href="#tbilling">Billing Details</a></li>				
			    <li><a href="#tshipping">Shipping Details</a></li>				
			</ul>
			<form action="#" class="appnitro" enctype="multipart/form-data" method="post" name="newscustomer" id="newscustomer">
				<div class="form_description">		
					<p class="error"></p>
				</div>	
				<input type="hidden" name="sessid" id="sessid" value="<?=$data['sessid']?>" />
				<div id="tbilling">
					<p>Please enter billing address below.</p>
					<ul>
						<li id='biname'>
							<label class="description" for="bname">Name<span class="required">*</span></label>
							<span><input id="btitle" name= "btitle" class="element text" maxlength="4" size="4" value="<?=$btitle?>" tabindex="1"/>		
							<label>Title</label>         
							</span><span>
							<input id="bfirst" name= "bfirst" class="element text" maxlength="255" tabindex='2' size="8" value="<?=$bfirst?>"/>
							<label>First</label>
							</span> <span>
							<input id="blast" name= "blast" class="element text" maxlength="255" tabindex='3' size="14" value="<?=$blast?>"/>
							<label>Last</label>
							</span> 
						 </li>
						 <li id="biemail">
							<label class="description" for="bemail">Email<span class="required">*</span></label>
							<div>
							  <input id="bemail" name="bemail" class="element text medium" type="text" maxlength="255" value="<?=$bemail?>" tabindex="4"/>
							</div>
						  </li>	
							<li id="bicvno" >
							<label class="description" for="add">Company</label>
							<div class="left">
							  <input id="bcompany" name="bcompany" class="element text medium" value="" type="text" tabindex="7">
							  <label for="bcompany">Company</label>
							</div>
							<div class="right">
							  <input id="vat_number" name="vat_number" class="element text medium" value="" type="text" tabindex="8">
							  <label for="vat_number">Vat Number</label>
							</div>
						  </li>
						  <li id="biaddr" >
							<label class="description" for="add">Address<span class="required">*</span></label>
							<div>
							  <input id="baddress1" name="baddress1" class="element text large" value="<?=$baddress1?>" type="text" tabindex="5">
							  <label for="addr1">Street Address</label>
							</div>
							<div>
							  <input id="baddress2" name="baddress2" class="element text large" value="<?=$baddress2?>" type="text" tabindex="6">
							  <label for="addr2">Address Line 2</label>
							</div>
							<div class="left">
							  <input id="bcity" name="bcity" class="element text medium" value="<?=$bcity?>" type="text" tabindex="7">
							  <label for="city">City</label>
							</div>
							<div class="right">
							  <input id="bstate" name="bstate" class="element text medium" value="<?=$bstate?>" type="text" tabindex="8">
							  <label for="state">County / State / Province / Region</label>
							</div>
							<div class="left">
							  <input id="bzipcode" name="bzipcode" class="element text medium" maxlength="15" value="<?=$bzip?>" type="text" tabindex="9">
							  <label for="pcode">Postcode</label>
							</div>
							<div class="right">
							  <?php
									$selcntry		= "SELECT * FROM 3bit_country WHERE iso_number != '' ORDER BY country";			
									$db_query		= $this->_db->fetchAll($selcntry);				
									echo '<select id="bcountry" name="bcountry" class="element select medium" tabindex="10">
																  <option value="0">-Select country-</option>';
										foreach($db_query as $db_rows){
											if($db_rows['id'] == $bcountry)
												echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
											else
												echo "<option value=\"".$db_rows['id']."\" >".ucfirst(strtolower($db_rows['country']))."</option> ";						
										}
									echo "</select>";
								  ?>		 
							  <label for="country">Country</label>
							</div>	
						  </li>
						  <li id="bitel" >
							<label class="description" for="mobile">Contact Telephone<span class="required">*</span></label>
							<div>
							  
							  <input id="bphone" name="bphone" class="element text medium" type="text" maxlength="255" value="<?=$bphone?>"  tabindex="11" /><label for="inc">please include any dialing codes in case we need to contact you regarding your order</label>
							</div>
						 </li>
					</ul>
				</div>			
				<div id="tshipping">
					<p>Please enter delivery address below</p>
					 <ul>
						  <li id='shname'>
							<label class="description" for="dname">Name<span class="required">*</span></label>
							<span><input id="dtitle" name= "dtitle" class="element text" maxlength="4" size="4" value="<?=$btitle?>" tabindex="13"/>		
							<label>Title</label>         
							</span><span>
							<input id="dfirst" name= "dfirst" class="element text" maxlength="255" tabindex='14' size="8" value="<?=$bfirst?>"/>
							<label>First</label>
							</span> <span>
							<input id="dlast" name= "dlast" class="element text" maxlength="255" tabindex='15' size="14" value="<?=$blast?>"/>
							<label>Last</label>
							</span> 
						 </li>
						 <li id="shaddr" >
							<label class="description" for="add">Address<span class="required">*</span></label>
							<div>
							  <input id="daddress1" name="daddress1" class="element text large" value="<?=$baddress1?>" type="text" tabindex="16">
							  <label for="addr1">Street Address</label>
							</div>
							<div>
							  <input id="daddress2" name="daddress2" class="element text large" value="<?=$baddress2?>" type="text" tabindex="17">
							  <label for="addr2">Address Line 2</label>
							</div>
							<div class="left">
							  <input id="dcity" name="dcity" class="element text medium" value="<?=$bcity?>" type="text" tabindex="18">
							  <label for="city">City</label>
							</div>
							<div class="right">
							  <input id="dstate" name="dstate" class="element text medium" value="<?=$bstate?>" type="text" tabindex="19">
							  <label for="state">County / State / Province / Region</label>
							</div>
							<div class="left">
							  <input id="dzipcode" name="dzipcode" class="element text medium" maxlength="15" value="<?=$bzip?>" type="text" tabindex="20">
							  <label for="pcode">Postcode</label>
							</div>
							<div class="right">
							  <?php
									$selcntry		= "SELECT * FROM 3bit_country WHERE iso_number != '' ORDER BY country";			
									$db_query		= $this->_db->fetchAll($selcntry);
									
									echo '<select id="dcountry" name="dcountry" class="element select medium" tabindex="21">
																  <option value="0">-Select country-</option>';
										foreach($db_query as $db_rows){
											if($db_rows['id'] == $bcountry)
												echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
											else
												echo "<option value=\"".$db_rows['id']."\" >".ucfirst(strtolower($db_rows['country']))."</option> ";						
										}
									echo "</select>";
								  ?>		 
							  <label for="country">Country</label>
							</div>	
						  </li>
						  <li id="shtel" >
							<label class="description" for="mobile">Contact Telephone<span class="required">*</span></label>
							<div>
							  
							  <input id="dphone" name="dphone" class="element text medium" type="text" maxlength="255" value="<?=$bphone?>"  tabindex="22" />
							<label for="inc">please include any dialing codes in case we need to contact you regarding your order</label>
							</div>
						 </li>          
						</ul>
				</div>
				<ul>
				  <li><a class="button" id="saveForm" type="button" onclick="validate_customer();">Submit</a> <a class="button" id="saveForm" type="button" onclick="addnew_customer(2);">Cancel</a></li>				 
				</ul>
			</form>
		</div>
		<?php }
		  die();
	}

	public function manualextcustomer($data)
	{
		$selUser		=	$this->_db->fetchAll("SELECT * FROM 3bit_customers WHERE customer_id='".$data['customer_id']."'");

		if(count($selUser)>0){

			foreach($selUser as $brow){

				$brow		=	array_map("stripslashes",$brow);

				$btitle		=	$brow['title'];

				$bfirst		=	$brow['fname'];

				$blast		=	$brow['lname'];

				$bemail		=	$brow['email'];

				$baddress1	=	$brow['address1'];

				$baddress2	=	$brow['address2'];

				$bcity		=	$brow['city'];

				$bstate		=	$brow['state'];

				$bzip		=	$brow['postcode'];

				$bphone		=	$brow['phone'];

				$country_org=	$this->_db->fetchOne("SELECT id FROM 3bit_country WHERE id ='".$brow['country']."'");

				if($country_org){
					$bcountry	=	$country_org;					
				}
			}

			$customer_id	=	$data['customer_id'];

			$chkCustomer	=	$this->_db->fetchAll("SELECT customer_id,address_id FROM 3bit_customers_address WHERE cart_sessionid ='".$data['sessid']."'");

			if(count($chkCustomer)==0){				

					$insertAddress	=	"INSERT INTO 3bit_customers_address SET
												customer_id		=	'".$data['customer_id']."',
												cart_sessionid	=	'".$data['sessid']."',
												email			=	'".addslashes($bemail)."',
												phone			=	'".addslashes($bphone)."',
												billing_title	=	'".addslashes($btitle)."',
												billing_fname	=	'".addslashes($bfirst)."',
												billing_lname	=	'".addslashes($blast)."',
												billing_address1=	'".addslashes($baddress1)."',
												billing_address2=	'".addslashes($baddress2)."',
												billing_city	=	'".addslashes($bcity)."',
												billing_state	=	'".addslashes($bstate)."',
												billing_country	=	'".$bcountry."',
												billing_postcode=	'".addslashes($bzip)."',
												billing_phone	=	'".addslashes($bphone)."',																						
												shipping_title	=	'".addslashes($btitle)."',
												shipping_fname	=	'".addslashes($bfirst)."',
												shipping_lname	=	'".addslashes($blast)."',
												shipping_address1=	'".addslashes($baddress1)."',
												shipping_address2=	'".addslashes($baddress2)."',
												shipping_city	=	'".addslashes($bcity)."',
												shipping_state	=	'".addslashes($bstate)."',
												shipping_country=	'".addslashes($bcountry)."',
												shipping_postcode=	'".addslashes($bzip)."',
												shipping_phone	=	'".addslashes($bphone)."',
												is_diffdeliver	=	'0',
												created_date	=	now(),
												modified_date	=	now()" ;
												
					$this->_db->query($insertAddress);

					$address_id		=	$this->_db->lastInsertId();	
					
			}else{
					$customer_id	=	$chkCustomer[0]['customer_id'];
					$address_id		=	$chkCustomer[0]['address_id'];		
				
					$updateAddress	=	"UPDATE 3bit_customers_address SET
												customer_id		=	'".$customer_id."',
												cart_sessionid	=	'".$data['sessid']."',
												email			=	'".addslashes($bemail)."',
												phone			=	'".addslashes($bphone)."',
												billing_title	=	'".addslashes($btitle)."',
												billing_fname	=	'".addslashes($bfirst)."',
												billing_lname	=	'".addslashes($blast)."',
												billing_address1=	'".addslashes($baddress1)."',
												billing_address2=	'".addslashes($baddress2)."',
												billing_city	=	'".addslashes($bcity)."',
												billing_state	=	'".addslashes($bstate)."',
												billing_country	=	'".addslashes($bcountry)."',
												billing_postcode=	'".addslashes($bzip)."',
												billing_phone	=	'".addslashes($bphone)."',																						
												shipping_title	=	'".addslashes($btitle)."',
												shipping_fname	=	'".addslashes($bfirst)."',
												shipping_lname	=	'".addslashes($blast)."',
												shipping_address1=	'".addslashes($baddress1)."',
												shipping_address2=	'".addslashes($baddress2)."',
												shipping_city	=	'".addslashes($bcity)."',
												shipping_state	=	'".addslashes($bstate)."',
												shipping_country=	'".addslashes($bcountry)."',
												shipping_postcode=	'".addslashes($bzip)."',
												shipping_phone	=	'".addslashes($bphone)."',
												is_diffdeliver	=	'0',												
												modified_date	=	now() WHERE address_id = '".$address_id."' AND cart_sessionid	= '".$data['sessid']."' ";
					$this->_db->query($updateAddress);																	
			}
			$_SESSION['wlsession']['cms_customer'] =	$customer_id;
			$_SESSION['wlsession']['cms_custaddr'] =	$address_id;
			$_SESSION['wlsession']['cms_frontuser']=	'';
			unset($_SESSION['wlsession']['cms_frontuser']);
		?>					
			<div id="article_tabs">
			<ul>
			    <li><a href="#tbilling">Billing Details</a></li>				
			    <li><a href="#tshipping">Shipping Details</a></li>				
			</ul>
			<form action="#" class="appnitro" enctype="multipart/form-data" method="post" name="newscustomer" id="newscustomer">
				<div class="form_description">		
					<p class="error"></p>
				</div>	
				<input type="hidden" name="sessid" id="sessid" value="<?=$data['sessid']?>" />
				<div id="tbilling">
					<p>Please enter billing address below.</p>
					<ul>
						<li id='biname'>
							<label class="description" for="bname">Name<span class="required">*</span></label>
							<span><input id="btitle" name= "btitle" class="element text" maxlength="4" size="4" value="<?=$btitle?>" tabindex="1"/>		
							<label>Title</label>         
							</span><span>
							<input id="bfirst" name= "bfirst" class="element text" maxlength="255" tabindex='2' size="8" value="<?=$bfirst?>"/>
							<label>First</label>
							</span> <span>
							<input id="blast" name= "blast" class="element text" maxlength="255" tabindex='3' size="14" value="<?=$blast?>"/>
							<label>Last</label>
							</span> 
						 </li>
						 <li id="biemail">
							<label class="description" for="bemail">Email<span class="required">*</span></label>
							<div>
							  <input id="bemail" name="bemail" class="element text medium" type="text" maxlength="255" value="<?=$bemail?>" tabindex="4"/>
							</div>
						  </li>	
							<li id="bicvno" >
							<label class="description" for="add">Company</label>
							<div class="left">
							  <input id="bcompany" name="bcompany" class="element text medium" value="" type="text" tabindex="7">
							  <label for="bcompany">Company</label>
							</div>
							<div class="right">
							  <input id="vat_number" name="vat_number" class="element text medium" value="" type="text" tabindex="8">
							  <label for="vat_number">Vat Number</label>
							</div>
						  </li>
						  <li id="biaddr" >
							<label class="description" for="add">Address<span class="required">*</span></label>
							<div>
							  <input id="baddress1" name="baddress1" class="element text large" value="<?=$baddress1?>" type="text" tabindex="5">
							  <label for="addr1">Street Address</label>
							</div>
							<div>
							  <input id="baddress2" name="baddress2" class="element text large" value="<?=$baddress2?>" type="text" tabindex="6">
							  <label for="addr2">Address Line 2</label>
							</div>
							<div class="left">
							  <input id="bcity" name="bcity" class="element text medium" value="<?=$bcity?>" type="text" tabindex="7">
							  <label for="city">City</label>
							</div>
							<div class="right">
							  <input id="bstate" name="bstate" class="element text medium" value="<?=$bstate?>" type="text" tabindex="8">
							  <label for="state">County / State / Province / Region</label>
							</div>
							<div class="left">
							  <input id="bzipcode" name="bzipcode" class="element text medium" maxlength="15" value="<?=$bzip?>" type="text" tabindex="9">
							  <label for="pcode">Postcode</label>
							</div>
							<div class="right">
							  <?php
									$selcntry		= "SELECT * FROM 3bit_country WHERE iso_number != '' ORDER BY country";			
									$db_query		= $this->_db->fetchAll($selcntry);				
									echo '<select id="bcountry" name="bcountry" class="element select medium" tabindex="10">
																  <option value="0">-Select country-</option>';
										foreach($db_query as $db_rows){
											if($db_rows['id'] == $bcountry)
												echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
											else
												echo "<option value=\"".$db_rows['id']."\" >".ucfirst(strtolower($db_rows['country']))."</option> ";						
										}
									echo "</select>";
								  ?>		 
							  <label for="country">Country</label>
							</div>	
						  </li>
						  <li id="bitel" >
							<label class="description" for="mobile">Contact Telephone<span class="required">*</span></label>
							<div>
							  
							  <input id="bphone" name="bphone" class="element text medium" type="text" maxlength="255" value="<?=$bphone?>"  tabindex="11" /><label for="inc">please include any dialing codes in case we need to contact you regarding your order</label>
							</div>
						 </li>
					</ul>
				</div>			
				<div id="tshipping">
					<p>Please enter delivery address below</p>
					 <ul>
						  <li id='shname'>
							<label class="description" for="dname">Name<span class="required">*</span></label>
							<span><input id="dtitle" name= "dtitle" class="element text" maxlength="4" size="4" value="<?=$btitle?>" tabindex="13"/>		
							<label>Title</label>         
							</span><span>
							<input id="dfirst" name= "dfirst" class="element text" maxlength="255" tabindex='14' size="8" value="<?=$bfirst?>"/>
							<label>First</label>
							</span> <span>
							<input id="dlast" name= "dlast" class="element text" maxlength="255" tabindex='15' size="14" value="<?=$blast?>"/>
							<label>Last</label>
							</span> 
						 </li>
						 <li id="shaddr" >
							<label class="description" for="add">Address<span class="required">*</span></label>
							<div>
							  <input id="daddress1" name="daddress1" class="element text large" value="<?=$baddress1?>" type="text" tabindex="16">
							  <label for="addr1">Street Address</label>
							</div>
							<div>
							  <input id="daddress2" name="daddress2" class="element text large" value="<?=$baddress2?>" type="text" tabindex="17">
							  <label for="addr2">Address Line 2</label>
							</div>
							<div class="left">
							  <input id="dcity" name="dcity" class="element text medium" value="<?=$bcity?>" type="text" tabindex="18">
							  <label for="city">City</label>
							</div>
							<div class="right">
							  <input id="dstate" name="dstate" class="element text medium" value="<?=$bstate?>" type="text" tabindex="19">
							  <label for="state">County / State / Province / Region</label>
							</div>
							<div class="left">
							  <input id="dzipcode" name="dzipcode" class="element text medium" maxlength="15" value="<?=$bzip?>" type="text" tabindex="20">
							  <label for="pcode">Postcode</label>
							</div>
							<div class="right">
							  <?php
									$selcntry		= "SELECT * FROM 3bit_country WHERE iso_number != '' ORDER BY country";			
									$db_query		= $this->_db->fetchAll($selcntry);
									
									echo '<select id="dcountry" name="dcountry" class="element select medium" tabindex="21">
																  <option value="0">-Select country-</option>';
										foreach($db_query as $db_rows){
											if($db_rows['id'] == $bcountry)
												echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
											else
												echo "<option value=\"".$db_rows['id']."\" >".ucfirst(strtolower($db_rows['country']))."</option> ";						
										}
									echo "</select>";
								  ?>		 
							  <label for="country">Country</label>
							</div>	
						  </li>
						  <li id="shtel" >
							<label class="description" for="mobile">Contact Telephone<span class="required">*</span></label>
							<div>
							  
							  <input id="dphone" name="dphone" class="element text medium" type="text" maxlength="255" value="<?=$bphone?>"  tabindex="22" />
							<label for="inc">please include any dialing codes in case we need to contact you regarding your order</label>
							</div>
						 </li>          
						</ul>
				</div>
				<ul>
				  <li><a class="button" id="saveForm" type="button" onclick="validate_customer();">Submit</a> <a class="button" id="saveForm" type="button" onclick="addnew_customer(2);">Cancel</a></li>				 
				</ul>
			</form>
		</div>
		<?php }
		  die();
	}


	public function registercustomer($data)
	{ 

	  if($data['sessid']<>""){

		 $data			=	array_map("addslashes",$data);

		$chkCustomer	=	$this->_db->fetchAll("SELECT customer_id,address_id FROM 3bit_customers_address WHERE cart_sessionid ='".$data['sessid']."'");

		if(count($chkCustomer)>0){

			$customer_id	=	$chkCustomer[0]['customer_id'];

			$address_id		=	$chkCustomer[0]['address_id'];

			$updateQry		=	"UPDATE 3bit_customers SET
												title		=	'".addslashes($data['btitle'])."',
												fname		=	'".addslashes($data['bfirst'])."',
												lname		=	'".addslashes($data['blast'])."',
												email		=	'".addslashes($data['bemail'])."',
												address1	=	'".addslashes($data['baddress1'])."',
												address2	=	'".addslashes($data['baddress2'])."',
												city		=	'".addslashes($data['bcity'])."',
												state		=	'".addslashes($data['bstate'])."',
												county		=	'',
												country		=	'".addslashes($data['bcountry'])."',
												postcode	=	'".addslashes($data['bzipcode'])."',
												phone		=	'".addslashes($data['bphone'])."',
												password	=	'',												
												browser		=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
												ip			=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
												modified_date=	now() WHERE customer_id ='".$customer_id."'";

			if($this->_db->query($updateQry)){
				
				$updateAddress	=	"UPDATE 3bit_customers_address SET
											customer_id		=	'".$customer_id."',
											cart_sessionid	=	'".$data['sessid']."',
											email			=	'".addslashes($data['bemail'])."',
											company		=	 '".addslashes($data['bcompany'])."',
											vat_number=	'".addslashes($data['vat_number'])."',
											phone			=	'".addslashes($data['bphone'])."',
											billing_title	=	'".addslashes($data['btitle'])."',
											billing_fname	=	'".addslashes($data['bfirst'])."',
											billing_lname	=	'".addslashes($data['blast'])."',
											billing_address1=	'".addslashes($data['baddress1'])."',
											billing_address2=	'".addslashes($data['baddress2'])."',
											billing_city	=	'".addslashes($data['bcity'])."',
											billing_state	=	'".addslashes($data['bstate'])."',
											billing_country	=	'".addslashes($data['bcountry'])."',
											billing_postcode=	'".addslashes($data['bzipcode'])."',
											billing_phone	=	'".addslashes($data['bphone'])."',												
											modified_date	=	now(),";

				if(!empty($data['dfirst'])&& !empty($data['dlast'])){

					$updateAddress	.=	"shipping_title		=	'".addslashes($data['dtitle'])."',
										 shipping_fname		=	'".addslashes($data['dfirst'])."',
										 shipping_lname		=	'".addslashes($data['dlast'])."',
										 shipping_address1	=	'".addslashes($data['daddress1'])."',
										 shipping_address2	=	'".addslashes($data['daddress2'])."',
										 shipping_city		=	'".addslashes($data['dcity'])."',
										 shipping_state		=	'".addslashes($data['dstate'])."',
										 shipping_country	=	'".addslashes($data['dcountry'])."',
										 shipping_postcode	=	'".addslashes($data['dzipcode'])."',
										 shipping_phone		=	'".addslashes($data['dphone'])."',
										 is_diffdeliver		=	'1' ";

				}else{

					$updateAddress	.=	"shipping_title		=	'".addslashes($data['btitle'])."',
										 shipping_fname		=	'".addslashes($data['bfirst'])."',
										 shipping_lname		=	'".addslashes($data['blast'])."',
										 shipping_address1	=	'".addslashes($data['baddress1'])."',
										 shipping_address2	=	'".addslashes($data['baddress2'])."',
										 shipping_city		=	'".addslashes($data['bcity'])."',
										 shipping_state		=	'".addslashes($data['bstate'])."',
										 shipping_country	=	'".addslashes($data['bcountry'])."',
										 shipping_postcode	=	'".addslashes($data['bzipcode'])."',
										 shipping_phone		=	'".addslashes($data['bphone'])."',
										 is_diffdeliver		=	'0' " ;


				}

				$updateAddress	.=		" WHERE address_id = '".$address_id."' AND cart_sessionid	= '".$data['sessid']."' ";

				$this->_db->query($updateAddress);
				if(!empty($data['dfirst'])&& !empty($data['dlast'])){
					$country_id	=	$data['dcountry'];
				}else{
					$country_id	=	$data['bcountry'];
				}				
			}

			$_SESSION['wlsession']['cms_customer']=	$customer_id;
			$_SESSION['wlsession']['cms_custaddr']=	$address_id;
			
			die("S");

		}else{

			$insertQry		=	"INSERT INTO 3bit_customers SET
												title		=	'".addslashes($data['btitle'])."',
												fname		=	'".addslashes($data['bfirst'])."',
												lname		=	'".addslashes($data['blast'])."',
												email		=	'".addslashes($data['bemail'])."',
												address1	=	'".addslashes($data['baddress1'])."',
												address2	=	'".addslashes($data['baddress2'])."',
												city		=	'".addslashes($data['bcity'])."',
												state		=	'".addslashes($data['bstate'])."',
												county		=	'',
												country		=	'".addslashes($data['bcountry'])."',
												postcode	=	'".addslashes($data['bzipcode'])."',
												phone		=	'".addslashes($data['bphone'])."',
												password	=	'',											
												browser		=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
												ip			=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
												created_date=	now(),
												modified_date=	now() ";
			if($this->_db->query($insertQry)){

				$customer_id	=	$this->_db->lastInsertId();

				$insertAddress	=	"INSERT INTO 3bit_customers_address SET
											customer_id		=	'".$customer_id."',
											cart_sessionid	=	'".$data['sessid']."',
											email			=	'".addslashes($data['bemail'])."',
											company		=	 '".addslashes($data['bcompany'])."',
											vat_number=	'".addslashes($data['vat_number'])."',
											phone			=	'".addslashes($data['bphone'])."',
											billing_title	=	'".addslashes($data['btitle'])."',
											billing_fname	=	'".addslashes($data['bfirst'])."',
											billing_lname	=	'".addslashes($data['blast'])."',
											billing_address1=	'".addslashes($data['baddress1'])."',
											billing_address2=	'".addslashes($data['baddress2'])."',
											billing_city	=	'".addslashes($data['bcity'])."',
											billing_state	=	'".addslashes($data['bstate'])."',
											billing_country	=	'".addslashes($data['bcountry'])."',
											billing_postcode=	'".addslashes($data['bzipcode'])."',
											billing_phone	=	'".addslashes($data['bphone'])."',									
											created_date	=	now(),
											modified_date	=	now(),";

				if(!empty($data['dfirst'])&& !empty($data['dlast'])){

					$insertAddress	.=	"shipping_title		=	'".addslashes($data['dtitle'])."',
										 shipping_fname		=	'".addslashes($data['dfirst'])."',
										 shipping_lname		=	'".addslashes($data['dlast'])."',
										 shipping_address1	=	'".addslashes($data['daddress1'])."',
										 shipping_address2	=	'".addslashes($data['daddress2'])."',
										 shipping_city		=	'".addslashes($data['dcity'])."',
										 shipping_state		=	'".addslashes($data['dstate'])."',
										 shipping_country	=	'".addslashes($data['dcountry'])."',
										 shipping_postcode	=	'".addslashes($data['dzipcode'])."',
										 shipping_phone		=	'".addslashes($data['dphone'])."',
										 is_diffdeliver		=	'1'" ;

				}else{

					$insertAddress	.=	"shipping_title		=	'".addslashes($data['btitle'])."',
										 shipping_fname		=	'".addslashes($data['bfirst'])."',
										 shipping_lname		=	'".addslashes($data['blast'])."',
										 shipping_address1	=	'".addslashes($data['baddress1'])."',
										 shipping_address2	=	'".addslashes($data['baddress2'])."',
										 shipping_city		=	'".addslashes($data['bcity'])."',
										 shipping_state		=	'".addslashes($data['bstate'])."',
										 shipping_country	=	'".addslashes($data['bcountry'])."',
										 shipping_postcode	=	'".addslashes($data['bzipcode'])."',
										 shipping_phone		=	'".addslashes($data['bphone'])."',
										 is_diffdeliver		=	'0'" ;

				}
				$this->_db->query($insertAddress);
				$address_id		=	$this->_db->lastInsertId();
				if(!empty($data['dfirst'])&& !empty($data['dlast'])){
					$country_id	=	$data['dcountry'];
				}else{
					$country_id	=	$data['bcountry'];
				}		

				$_SESSION['wlsession']['cms_customer']=	$customer_id;
				$_SESSION['wlsession']['cms_custaddr']=	$address_id;
			}
			$_SESSION['wlsession']['cms_frontuser']	 = '';
			unset($_SESSION['wlsession']['cms_frontuser']);
			die("S");
		 }
	   }	  
	 }


	 public function cart_confirm()
	{
		  $sessid					=		$_SESSION['wlsession']['asess_id'];
		  $selAdd					=		$this->_db->fetchAll("SELECT shipping_country,company,vat_number FROM 3bit_customers_address WHERE cart_sessionid = '".$sessid."' AND status ='current' ");	 
		  $numadd					=		count($selAdd);
		  $cms_customer				=		'';
		  $orgName					=		'';
		  if(!empty($_SESSION['wlsession']['cms_customer'])){
			$selAdd					=		$this->_db->fetchALl("SELECT shipping_country,company,vat_number FROM 3bit_customers_address WHERE customer_id = '".$_SESSION['wlsession']['cms_customer']."' ORDER BY modified_date DESC LIMIT 0,1");
			if(count($selAdd)>0){
					$ship_country			=		$selAdd[0]['shipping_country'];
					$cms_customer			=		$_SESSION['wlsession']['cms_customer'];
			}
		  }else if($numadd>0){
			$ship_country			=		$selAdd[0]['shipping_country'];
		  }else{
			$ship_country			=		232;
		  }

			$company					=		stripslashes($selAdd[0]['company']);
			$vat_number				=		stripslashes($selAdd[0]['vat_number']);

		  $is_eu					=		$this->_db->fetchOne("SELECT is_eu FROM 3bit_country WHERE id='".$ship_country."'");	
		  
		  if(!empty($_SESSION['wlsession']['cms_frontuser'])){		  
			  $orgName					=		$this->_db->fetchOne("SELECT organisation FROM new_users WHERE id='".$_SESSION['wlsession']['cms_frontuser']."'");
		  }

		  $selCart					=		$this->_db->fetchAll("SELECT ci.* FROM `3bit_cart_items` ci WHERE ci.cart_sessionid = '".$sessid."' ORDER BY ci.cart_id");	
		  $cartrows					=		count($selCart);
		  if($cartrows>0){
			$sub_total		=	0;
			$vatprice		=	0;
			$returnText		=	'<h1>Your shopping cart contents</h1>
								  <table cellspacing="0" border="0" width="100%" class="cart_table">
									<thead>
									  <tr>
										<th class="product-column">Description</th>
										<th class="quantity">Qty*</th>										
										<th class="value">Price</th>
										<th class="value">Total</th>
									  </tr>
									</thead>
									<tbody>';
			$shipping_price			=		0;
			$all_cid				=		'';
			$totweight				=		0;
			$licenced				=		"Y";
			$bundlesarr			=		array("14","17","18");
			foreach($selCart as $row){ 
				$cart_id			=		$row['cart_id'];			
				$all_cid			.=		$cart_id.",";
				$item_vat			=		'';
				$variation_type		=		'';
				$disc_vat			=		0;
				if(!empty($row['product_id'])){					
					$selProd	=	$this->_db->fetchAll("SELECT p.* FROM 3bit_products p WHERE p.product_id='".$row['product_id']."' ");		
					if(count($selProd)>0){								
						foreach($selProd as $res){						
							$item_id		=	$res['product_id'];	
							$item_name 		= 	$res['product_name'];
							$item_code		=	$res['product_code'];
							$item_qty		=	$row['quantity'];	
							if($res['size'] != '') 
								$variation_type = "<span>(".$res['size'].")</span>" ;
							else if($res['style'] != '') 
								$variation_type = "<span>(".$res['style'].")</span>" ;
							$item_price		=	$res['price'];
							$line_price		=	$item_qty*$item_price;
							$sub_total		+=	$line_price;
							$totweight		+=	$res['weight'];
							if($is_eu=='1'){
								if(in_array($item_id,$bundlesarr)){
									$item_vat	=	$this->findBundlesVat($item_id,$ship_country);
								}else{
									if($res['vat_moss'])
									$item_vat	=	$this->findProductvatmoss($item_id,$ship_country);
									else
									$item_vat	=	$this->findProductvat($item_id);
								}		

								// For the bundles update ==============
								$proCat						=	$res['procat_id'];
								$expProCat				=	explode(",",$proCat);
								if(in_array("2",$expProCat)){
									$disc_vat	=	$this->findDiscountVat($ship_country);
								}
								// Ends here ===========================

							}
							//	$item_vat	=	$this->findProductvat($item_id);
							
						}
					}
					// Checking for ebook ===============================
					$selPDF			=	$this->_db->fetchAll("SELECT p.product_id,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='4' AND p.product_id='".$row['product_id']."'");
					$numPDF			=	count($selPDF);					

					// Checking for combo product ==============================================================================================
					$selCombo		=	$this->_db->fetchAll("SELECT p.*,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='2' AND p.product_id='".$row['product_id']."'");
					$numCombo		=	count($selCombo);	
					if($numCombo>0){
						if($row['product_id']!='14' && $row['product_id']!='15'){
							$numPDF	=	$numCombo;
						}
					}

					if($numPDF>0 && $row['quantity']>1){						
						$licenced	=	"N";						
					}
				}
				$item_vat		=	$item_qty*$item_vat;			
				$disc_vat		=	$item_qty*$disc_vat;
			  $vatprice		+=	($item_vat-$disc_vat);
				$item_price		=	number_format($item_price, 2, '.', '');
				$line_price		=	number_format($line_price, 2, '.', '');
				$sub_total		=	number_format($sub_total, 2, '.', '');
				
				$returnText		.=	'<tr>
										<td class="left-align">'.$item_name.$variation_type.'</td>
										<td class="input">'.$item_qty.'</td>										
										<td class="value">'.$item_price.' GBP</td>
										<td class="value">'.$line_price.' GBP</td>
									  </tr>';


			}
			$basket_total	=	number_format($sub_total, 2, '.', '');					
			$item_ship		=	$this->getShippingCostbyWeight($totweight,$ship_country);
			$item_vat		=	$vatprice;
			$shipvat		=	0;
			if($item_vat>0 && $item_ship>0){
				$shipvat		=	$item_ship-($item_ship/$this->liveRate());
				$shipvat		=	number_format($shipvat, 2, '.', '');
			}
			$vatprice		=	($vatprice+$shipvat);
			if($company)
				$vatprice		=	0;
			$item_vat		=	number_format($vatprice, 2, '.', '');
			$final_total	=	number_format(($basket_total+$item_ship), 2, '.', '');
			$returnText		.=	'</tbody>							
								<input type="hidden" id="cart_id" name="cart_id" value="'.$all_cid.'" />
								<tfoot>	  
								  <tr>
									<td colspan="3">Sub Total</td>
									<td class="value"><input type="hidden" name="uptot" id="uptot" value="'.$sub_total.'">'.$sub_total.' GBP</td>
								  </tr>	 
								  <tr>
									<td colspan="3">TAX (inc)</td>
									<td class="value"><input type="hidden" name="upvat" id="upvat" value="'.$item_vat.'"><span id="upvats">'.$item_vat.'</span> GBP</td>
								  </tr>
								   <tr>
									<td colspan="3">Discount</td>
									<td class="value"><input type="text" name="txtDiscount" id="txtDiscount" style="width:50px;" onkeypress="return blockNumbers(event)" onblur="updateTotal(this.value,$(\'#txtShip\').val(),$(\'#upvat\').val(),$(\'#uptot\').val(),\''.$is_eu.'\')"> GBP</td>
								  </tr>
								  <tr>
									<td colspan="3">Shipping Charge</td>
									<td class="value"><input type="text" name="txtShip" id="txtShip" style="width:50px;" onkeypress="return blockNumbers(event)" onblur="updateTotal($(\'#txtDiscount\').val(),this.value,$(\'#upvat\').val(),$(\'#uptot\').val(),\''.$is_eu.'\')" value="'.$item_ship.'"> GBP</td>
								  </tr>
								  <tr>
									<td colspan="3"><b>Grand Total</b></td>
									<td class="value"><b><span id="uptots">'.$final_total.'</span> GBP</b></td>
								   </tr>';
			 if($licenced=='N'){
				$returnText		.=	'<tr>
									<td colspan="3"><b>Licensed To</b></td>
									<td class="value"><input type="text" name="txtLicense" id="txtLicense" style="width:150px;" value="'.$orgName.'" ></td>
								  </tr>';
			 }

				$returnText		.=	'<tr>
									<td colspan="3"><b>Transaction Id</b></td>
									<td class="value"><input type="text" name="txtTxnid" id="txtTxnid" style="width:150px;" value="" ></td>
								  </tr>
								  </tfoot>	
							  </table>';
		  }else{
				$returnText		=	'<table cellspacing="0" border="0" class="cart_table">
									<thead>
									  <tr>
										<th colspan="4">Your cart is empty!</th>
									  </tr>
									</thead>	
									</table>';
		  }
		  return $returnText;
	}


	public function customer_details()
	{
		$selDetails	=	$this->_db->fetchAll("SELECT * FROM 3bit_customers_address WHERE address_id = '".$_SESSION['wlsession']['cms_custaddr']."' AND customer_id='".$_SESSION['wlsession']['cms_customer']."' ");
	 
		  if(count($selDetails)>0){	  	
			foreach($selDetails as $row){
				$email		=	stripslashes($row['email']);
				$bill_fname	=	stripslashes($row['billing_fname']);
				$bill_lname	=	stripslashes($row['billing_lname']);
				$bill_name	=	ucfirst($bill_fname)." ".ucfirst($bill_lname);
				$bill_add1	=	stripslashes(ucfirst($row['billing_address1']));
				$bill_add2	=	stripslashes(ucfirst($row['billing_address2']));
				$bill_city	=	stripslashes(ucfirst($row['billing_city']));
				$bill_county=	stripslashes(ucfirst($row['billing_county']));
				$bill_state =   stripslashes(ucfirst($row['billing_state']));
				$cntry_id	=	stripslashes($row['billing_country']);
				$bill_country=	$this->_db->fetchOne("SELECT `country` FROM  3bit_country WHERE id='".$cntry_id."' ");
				$bill_pcode	=	stripslashes($row['billing_postcode']);
				$bill_phone	=	stripslashes($row['billing_phone']);
				
				$ship_fname	=	stripslashes($row['shipping_fname']);
				$ship_lname	=	stripslashes($row['shipping_lname']);
				$ship_name	=	ucfirst($ship_fname)." ".ucfirst($ship_lname);
				$ship_add1	=	stripslashes(ucfirst($row['shipping_address1']));
				$ship_add2	=	stripslashes(ucfirst($row['shipping_address2']));
				$ship_city	=	stripslashes(ucfirst($row['shipping_city']));
				$ship_county=	stripslashes(ucfirst($row['shipping_county']));
				$ship_state =   stripslashes(ucfirst($row['shipping_state']));
				$conty_id	=	stripslashes($row['shipping_country']);
				$ship_country=	$this->_db->fetchOne("SELECT `country` FROM  3bit_country WHERE id='".$conty_id."' ");
				$ship_pcode	=	stripslashes($row['shipping_postcode']);
				$ship_phone	=	stripslashes($row['shipping_phone']);
				$customer_id=	stripslashes($row['customer_id']);
			}	  
		  }

		  $returnText		=	'<table cellspacing="0" border="0" width="100%" class="cart_address">
								   <thead>
									 <tr>
									   <th>Invoice Address Details</th>
									   <th>Delivery Address Details</th>									   
									 </tr>
								   </thead>
								   <tr>
									  <td><span id="invadd">'.$bill_name.'( '.stripslashes($email).')<br />
									   '.$bill_add1.'<br />
									   '.$bill_add2.'<br />
									   '.$bill_city.'<br />
									   '.$bill_state.'<br />
									   '.$bill_pcode.'<br />
									   '.$bill_country.'<br />
									  <br />
									  Tel: '.$bill_phone.'</span>
									  </td>		
									  <td><span id="deladd">'.$ship_name.'( '.stripslashes($email).')<br />
									   '.$ship_add1.'<br />
									   '.$ship_add2.'<br />
									   '.$ship_city.'<br />
									   '.$ship_state.'<br />
									   '.$ship_pcode.'<br />
									   '.$ship_country.'<br />
									   <br />
									   Tel: '.$ship_phone.'</span>
									   </td>									  
									</tr>
								</table>';
		return $returnText;
	}

	public function checkout($data)
	{		  
		$sessid			=	$data['sessid'];
		$customer_id	=	$data['customer_id'];
		$address_id		=	$data['address_id'];
		$frontuser_id	=	$data['frontuser_id'];		
		$txn_discount	=	$data['txtDiscount'];
		$item_ship		=	$data['txtShip'];
		$orders			=	new Application_Model_Orders();
		
		if($address_id<>""){	
			$selCust		=	$this->_db->fetchAll("SELECT * FROM 3bit_customers_address WHERE customer_id = '".$customer_id."' AND address_id='".$address_id."'");		
			if(count($selCust)>0){					
				foreach($selCust as $res){
					$address_id	=	$res['address_id'];
					$phone		=	$res['phone'];
					$company		=	stripslashes($res['company']);
					$vat_number	=	stripslashes($res['vat_number']);
					$bill_fname	=	stripslashes($res['billing_fname']);
					$bill_lname	=	stripslashes($res['billing_lname']);
					$bill_name	=	ucfirst($bill_fname)." ".ucfirst($bill_lname);
					$bill_add1	=	stripslashes(ucfirst($res['billing_address1']));
					$bill_add2	=	stripslashes(ucfirst($res['billing_address2']));
					$bill_city	=	stripslashes(ucfirst($res['billing_city']));
					$bill_state	=	stripslashes(ucfirst($res['billing_state']));
					$cntry_id	=	stripslashes($res['billing_country']);
					$selCnt		=	$this->_db->fetchAll("SELECT `country`,`iso_number` FROM  3bit_country WHERE id='".$cntry_id."' ");
					if(count($selCnt)>0){
						$bill_country	=	$selCnt[0]['country'];	
						$bill_number	=	$selCnt[0]['iso_number'];			
					}
					$bill_pcode	=	stripslashes($res['billing_postcode']);					
					$ship_fname	=	stripslashes($res['shipping_fname']);
					$ship_lname	=	stripslashes($res['shipping_lname']);
					$ship_name	=	ucfirst($ship_fname)." ".ucfirst($ship_lname);
					$ship_add1	=	stripslashes(ucfirst($res['shipping_address1']));
					$ship_add2	=	stripslashes(ucfirst($res['shipping_address2']));
					$ship_city	=	stripslashes(ucfirst($res['shipping_city']));
					$ship_state	=	stripslashes(ucfirst($res['shipping_state']));
					$conty_id	=	stripslashes($res['shipping_country']);
					$selCntry		=	$this->_db->fetchAll("SELECT `country`,`iso_number` FROM  3bit_country WHERE id='".$conty_id."' ");
					if(count($selCntry)>0){
						$ship_country	=	$selCntry[0]['country'];
						$ship_number	=	$selCntry[0]['iso_number'];	
					}
					$ship_pcode	=	stripslashes($res['shipping_postcode']);
				}
			}
		}

		$selCart	=	$this->_db->fetchAll("SELECT ci.* FROM `3bit_cart_items` ci WHERE ci.cart_sessionid = '".$sessid."' ORDER BY ci.cart_id");	
		$cartrows	=	count($selCart);

		if($cartrows>0){

			$query	=	$this->_db->query("INSERT INTO 3bit_orders SET
												cart_sessionid		=	'".$sessid."',
												customer_id			=	'".$customer_id."',
												address_id			=	'".$address_id."',											
												currency			=	'GBP',
												status				=	'New',												
												live_vat_rate		=	'".$this->configRate()."',
												ordered_date		=	now(),																							
												payment_status		=	'Awaiting Payment',
												payment_method		=	'Pro-Forma',
												ccerrcode			=	'1',
												user_id				=	'".$_SESSION['wlsession']['user_id']."',
												frontuser_id		=	'".$frontuser_id."',
												licensed_to			=	'".$data['txtLicense']."',
												order_number		=	'".$data['txtTxnid']."',
												created_date		=	now(),
												modified_date		=	now()");

			$order_id	=	$this->_db->lastInsertId();			
			$sub_total	=	0;
			$vatdisc	=	0;
			$selEU			=	$this->_db->fetchAll("SELECT c.shipping_country,cn.is_eu FROM 3bit_customers_address c, 3bit_country cn WHERE c.shipping_country=cn.id AND cn.is_eu='1' AND c.address_id = '".$address_id."'");
			$is_eu			=	count($selEU);
			$shipping_price	=	0;
			$vatprice		=	0;
			$prd_details	=	'';
			$ord_details	=	'';
			$item_vat		=   0;
			$totweight		=	0;
			$bundlesarr			=	array("14","17","18");
			foreach($selCart as $row){
				$cart_id			=		$row['cart_id'];	
				$discount			=		$row['discount'];
				if(!empty($row['product_id'])){					
					$selProd	=	$this->_db->fetchAll("SELECT p.* FROM 3bit_products p WHERE p.product_id='".$row['product_id']."' ");		
					if(count($selProd)>0){								
						foreach($selProd as $res){						
							$item_id			=	$res['product_id'];	
							$item_name 		= $res['product_name'];
							$item_code		=	$res['product_code'];
							$item_qty			=	$row['quantity'];
							$vat_id				=	$res['vat_id'];
							if($res['size'] != '')
								$item_name .= "-".$res['size'];
							else if($res['style'] != '')
								$item_name .= "-".$res['style'];
							$item_price		=	$res['price'];	
							$line_price		=	$item_qty*$item_price;
							$sub_total		+=	$line_price;
							if($is_eu=='1'){
								if(in_array($item_id,$bundlesarr)){
									$item_vat	=	$this->findBundlesVat($item_id,$conty_id);
								}else{
									if($res['vat_moss'])
									$item_vat	=	$this->findProductvatmoss($item_id,$conty_id);
									else
									$item_vat	=	$this->findProductvat($item_id);
								}		

								// For the bundles update ==============
								$proCat						=	$res['procat_id'];
								$expProCat				=	explode(",",$proCat);
								if(in_array("2",$expProCat)){
									$disc_vat	=	$this->findDiscountVat($conty_id);
								}
								// Ends here ===========================

								//$item_vat		=	$this->findProductvat($item_id);
							}
							$totweight		+=	$res['weight'];
						}
					}
				}
				$item_vat			=	$item_qty*$item_vat;			
				$disc_vat			=	$item_qty*$disc_vat;			
				$vatprice			+=($item_vat-$disc_vat);
				$item_price		=	number_format($item_price, 2, '.', '');
				$line_price		=	number_format($line_price, 2, '.', '');
				$sub_total		=	number_format($sub_total, 2, '.', '');	

																																						
				$prd_details	.=	$item_qty."&nbsp;x&nbsp;".$item_code."&nbsp;(".$item_name.")&nbsp;&pound;".$line_price."<br>";
				$ord_details	.=	$item_qty." x ".$item_code." (".$item_name.") GBP ".$line_price."\n";

				$insDet			=	$this->_db->query("INSERT INTO 3bit_order_details SET
															order_id		=	'".$order_id."',
															cart_id			=	'".$cart_id."',
															cart_sessionid	=	'".$sessid."',
															product_id		=	'".$row['product_id']."',															
															quantity		=	'".$item_qty."',
															price			=	'".$item_price."',
															vat_id			=	'".$vat_id."',
															browser			=	'".$_SERVER['HTTP_USER_AGENT']."',
															ip				=	'".$_SERVER['REMOTE_ADDR']."',
															created_date	=	now(),
															modified_date	=	now()");
				
				if(!empty($row['product_id'])){			
					
					//$this->_db->query("UPDATE 3bit_products SET available_stock = available_stock-".$item_qty." WHERE product_id = '".$item_id."'");	
					
					// For membership subscription =====================================================================================

					$selSub		=	$this->_db->fetchAll("SELECT p.*,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='3' AND p.product_id='".$row['product_id']."'");
					foreach($selSub as $srow){
						//GET CURRENT SUBSCRIPTION IF ANY
						$selSub			=	"SELECT end_date FROM 3bit_subscribers WHERE user_id = '".$frontuser_id."'  AND end_date > NOW() ORDER BY end_date DESC LIMIT 0,1";
						$fecselSub		=	$this->_db->fetchRow($selSub);						
						if($fecselSub['end_date']!=""){
							$current_end_date	=	date ("Y-m-d",strtotime('+1 day',strtotime( $fecselSub['end_date'])));
							$next_month			=	date ("Y-m-d",strtotime('+1 months',strtotime( $fecselSub['end_date'])));
							$next_year			=	date ("Y-m-d",strtotime('+1 years',strtotime( $fecselSub['end_date'])));
						}else{
							$current_end_date	=	date("Y-m-d");
							$next_month			=	date("Y-m-d",strtotime("+1 months"));
							$next_year			=	date("Y-m-d",strtotime("+1 years"));
						}

						$current_date	=	date("Y-m-d");

						if($srow['style']=='Daily'){											
							$subSQL		=	"INSERT INTO 3bit_subscribers SET 
														user_id		=	'".$frontuser_id."',
														start_date	=	'".$current_date."',
														end_date	=	'".$current_end_date."',
														duration	=	'1',
														order_id	=	'".$order_id."',
														status		=	'0',
														created_date=	now(),
														modified_date=	now()";
						}elseif($srow['style']=='Monthly'){
							$subSQL		=	"INSERT INTO 3bit_subscribers SET 
														user_id		=	'".$frontuser_id."',
														start_date	=	'".$current_date."',
														end_date	=	'".$next_month."',
														duration	=	'30',
														order_id	=	'".$order_id."',
														status		=	'0',
														created_date=	now(),
														modified_date=	now()";
							
						}elseif($srow['style']=='Yearly'){
							$subSQL		=	"INSERT INTO 3bit_subscribers SET 
														user_id		=	'".$frontuser_id."',
														start_date	=	'".$current_date."',
														end_date	=	'".$next_year."',
														duration	=	'365',
														order_id	=	'".$order_id."',
														status		=	'0',
														created_date=	now(),
														modified_date=	now()";
							
						}elseif($srow['style']=='(Bundle)'){
							$subSQL		=	"INSERT INTO 3bit_subscribers SET 
														user_id		=	'".$frontuser_id."',
														start_date	=	'".$current_date."',
														end_date	=	'".$next_year."',
														duration	=	'365',
														order_id	=	'".$order_id."',
														status		=	'0',
														created_date=	now(),
														modified_date=	now()";
							
						}
						$this->_db->query($subSQL);	
					} // For membership subscription =====================================================================================

					// Checking for combo ===============================
					$selCombo			=	$this->_db->fetchAll("SELECT p.product_id,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='2' AND p.product_id='".$row['product_id']."'");

					$numCombo	=	count($selCombo);	

					if($numCombo>0){	
						
						//GET CURRENT SUBSCRIPTION IF ANY
						$selSub			=	"SELECT end_date FROM 3bit_subscribers WHERE user_id = '".$frontuser_id."'  AND end_date > NOW() ORDER BY end_date DESC LIMIT 0,1";
						$fecselSub		=	$this->_db->fetchRow($selSub);						
						if($fecselSub['end_date']!=""){
							$next_year			=	date ("Y-m-d",strtotime('+1 years',strtotime( $fecselSub['end_date'])));
						}else{
							$next_year			=	date("Y-m-d",strtotime("+1 years"));
						}

						$current_date	=	date("Y-m-d");

						$this->_db->query("INSERT INTO 3bit_subscribers SET 
														user_id		=	'".$frontuser_id."',
														start_date	=	'".$current_date."',
														end_date	=	'".$next_year."',
														duration	=	'365',
														order_id	=	'".$order_id."',
														status		=	'0',
														created_date=	now(),
														modified_date=	now()");													
					}
				}
			}
			$final_total	=	number_format($sub_total, 2, '.', '');
			$item_vat		=	$vatprice;
			//$item_ship		=	$this->getShippingCostbyWeight($totweight,$conty_id);
			$shipvat		=	0;
			$discvat		=	0;
			if($item_vat>0 && $item_ship>0){
				$shipvat	=	$item_ship-($item_ship/$this->liveRate());
				$shipvat	=	number_format($shipvat, 2, '.', '');
			}
			
			if($item_vat>0 && $txn_discount>0){
				$discvat	=	$txn_discount-($txn_discount/$this->liveRate());
				$discvat	=	number_format($discvat, 2, '.', '');
			}
			$vatprice		=	($vatprice-$discvat);
			if($company)
				$vatprice	=	0;	

			if($txn_discount>0){
				$final_total=	$final_total-$txn_discount;
			}
			if($final_total==0){
				$vatprice=	0;
			}

			$final_total	=	number_format(($final_total+$item_ship), 2, '.', '');

			$vatprice		=	($vatprice+$shipvat);

			$item_vat		=	number_format($vatprice, 2, '.', '');	

			$this->_db->query("UPDATE 3bit_orders SET ordertotal = '".$final_total."',order_shipping = '".$item_ship."',order_vat_tax= '".$item_vat."' WHERE order_id ='".$order_id."' ");

			$this->_db->query("UPDATE 3bit_order_details SET shipping = '".$item_ship."',discount='".$txn_discount."' WHERE order_id ='".$order_id."' ");
			
			$this->_db->query("INSERT INTO 3bit_order_actions SET 
										order_id			=	'".$order_id."',
										action				=	'New',
										action_date			=	now(),
										payment_method		=	'Manual Order',								
										amount_paid			=	'".$final_total."',										
										status				=	'1',
										created_date		=	now(),
										modified_date		=	now()");
			$this->_db->query("DELETE FROM 3bit_cart_items WHERE cart_sessionid = '".$sessid."' ");

			$selCname		=	$this->_db->fetchAll("SELECT fname,lname,email FROM 3bit_customers WHERE customer_id = '".$customer_id."'");
			if(count($selCname)>0){
				$custfname	=	$selCname[0]['fname'];
				$custlname	=	$selCname[0]['lname'];
				$custemail	=	$selCname[0]['email'];
				$dispname	=	$custfname." ".$custlname;
			}


			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$order_id."' rel='facebox'>order #".$order_id."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$_SESSION['wlsession']['user_id']."' rel='facebox'>".$_SESSION['wlsession']['fullname']."</a>";

			$customerurl	=	"<a href='".$this->_baseurl."/orders/customerhistory/id/".$customer_id."' rel='facebox'>".$dispname."</a>";
			
			$this->_db->query("INSERT INTO `3bit_order_history` SET order_id='".$order_id."',customer_id='".$customer_id."',status	=	'".addslashes($adminurl)." placed a new ".addslashes($orderurl)." on <span class=\"dashdate\">".date('Y/m/d H:i:s')."</span> for ".addslashes($customerurl)."',user_id='".$_SESSION['wlsession']['user_id']."',history_date=now()");


			$message_type	=	'New Order';			

			$orders->sendMails($order_id,$message_type,'0','Customer');

			$orders->sendMails($order_id,$message_type,'0','Admin');

		}	
		$_SESSION['wlsession']['cms_customer']='';
		$_SESSION['wlsession']['cms_custaddr']='';
		$_SESSION['wlsession']['cms_frontuser']='';
		$_SESSION['wlsession']['asess_id']='';
		unset($_SESSION['wlsession']['cms_customer']);
		unset($_SESSION['wlsession']['cms_custaddr']);
		unset($_SESSION['wlsession']['cms_frontuser']);
		unset($_SESSION['wlsession']['asess_id']);
		
	}		

	// Function to update total in confrm order page //

	public function updatetotal($data)
	{
		$txn_discount	=	$data['discount'];
		$txn_ship		=	$data['ship'];
		$vatprice		=	$data['vat'];
		$sub_total	=	$data['total'];
		$iseu				= $data['iseu'];
		$shipvat		=	0;
		$discvat		=	0;
		if($txn_discount>0 && $iseu){
				$discvat	=	$txn_discount-($txn_discount/$this->liveRate());
				$discvat	=	number_format($discvat, 2, '.', '');
				$vatprice	=	($vatprice-$discvat);	
		}
		$final_total	=	($sub_total-$txn_discount);
		if($final_total==0){
			$vatprice=	0;
		}
		if($txn_ship>0 && $iseu){
				$shipvat	=	$txn_ship-($txn_ship/$this->liveRate());
				$shipvat	=	number_format($shipvat, 2, '.', '');
		}		
		$final_total	=	$final_total+$txn_ship;
		//echo $final_total."+".$vatprice."+".$shipvat."<br />";
		$vatprice		=	($vatprice+$shipvat);
		$vatprice		=	number_format($vatprice, 2, '.', '');
		$final_total	=	number_format($final_total, 2, '.', '');
		
		die($discvat."^".$vatprice."^".$final_total);
	}

	public function findBundlesVat($bundleid,$country){

			if($bundleid==14)			
				$prodarr	=	array("10","23");
			elseif($bundleid==17)			
				$prodarr	=	array("10","24");
			elseif($bundleid==18)			
				$prodarr	=	array("10","23","24");
			
			$item_vat		=	0;

			for($i=0;$i<count($prodarr);$i++){
				$selProd		=		$this->_db->fetchRow("SELECT p.vat_moss FROM 3bit_products p WHERE p.product_id='".$prodarr[$i]."' ");	
				$vat_moss		=		$selProd['vat_moss'];
				if($vat_moss)
					$item_vat		+=		$this->findProductvatmoss($prodarr[$i],$country);
				else
					$item_vat		+=		$this->findProductvat($prodarr[$i]);
			}

			return $item_vat;

	}

	public function findDiscountVat($country){
		$flag			=		true;
		$selCVat	=		$this->_db->fetchRow("SELECT vat_id FROM `3bit_vatmoss` WHERE country_id='".$country."'");	
		$cartrows	=		count($selCVat);
		if($cartrows>0){
			$vatid		=	$selCVat['vat_id'];
		}

		if(!empty($vatid)){

				$selVat		=	$this->_db->fetchRow("SELECT vat_rate FROM 3bit_product_vats WHERE vat_id='".$vatid."'");

				if(count($selVat)>0){
					$vatamt			= 	$selVat['vat_rate'];
				}
				$vatper				=		($vatamt/100);		

				$pricewovat		=		10/(1+$vatper);

				$vatprice			=		number_format((10-$pricewovat), 2, '.', '');
			}else{

				$vatprice		=		number_format((10-(10/$this->liveRate())), 2, '.', '');
			}

			return $vatprice;

	}

	public function findProductvatmoss($productid,$country){
			$flag			=		true;
			$selCVat	=		$this->_db->fetchRow("SELECT vat_id FROM `3bit_vatmoss` WHERE country_id='".$country."'");	
			$cartrows	=		count($selCVat);
			if($cartrows>0){
				$vatid		=	$selCVat['vat_id'];
			}
			
			if(!empty($vatid)){

				$selVat		=	$this->_db->fetchRow("SELECT vat_rate FROM 3bit_product_vats WHERE vat_id='".$vatid."'");

				if(count($selVat)>0){
					$vatamt			= 	$selVat['vat_rate'];
				}
				$vatper			=	($vatamt/100);

				$selProd		=	$this->_db->fetchRow("SELECT price,vat_id FROM 3bit_products WHERE product_id='".$productid."'");

				if(count($selProd)>0){

					$price_site	=	$selProd['price'];
				}

				$pricewovat		=	$price_site/(1+$vatper);

				$vatprice		=	($price_site-$pricewovat);
			}else{

				$vatprice		=		$this->findProductvat($productid);
			}

			return $vatprice;
	}

	 // Function to calculate VAT for a individual product 

	public function findProductvat($productid){

		$selProd	=	$this->_db->fetchAll("SELECT price,vat_id FROM 3bit_products WHERE product_id='".$productid."'");

		if(count($selProd)>0){

			foreach($selProd as $srow){

				$price_site	=	$srow['price'];			

				$vatid		=	$srow['vat_id'];

				$selVat		=	$this->_db->fetchRow("SELECT vat_rate FROM 3bit_product_vats WHERE vat_id='".$vatid."'");

				if(count($selVat)>0){
					$vatamt			= 	$selVat['vat_rate'];
				}
				$vatper			=	($vatamt/100);

				$pricewovat		=	$price_site/(1+$vatper);

				$vatprice		=	($price_site-$pricewovat);
						
			}		
		}	 
		return $vatprice;
	}

	// Function to get price with vat amount for product

	public function findProductprice($productid){
		
		$selPrice	=	$this->_db->fetchAll("SELECT price,sale_price FROM 3bit_products WHERE product_id='".$productid."'");

		if(count($selPrice)>0){
			
			foreach($selPrice as $row){
				
				$product_price	=	$row['price'];	
				
				$sale_price		=	$row['sale_price'];

				if($sale_price>0)
					 $product_price	=	$sale_price;

				$product_price	=	number_format($product_price, 2, '.', '');

			}
		}
		return $product_price;
	}

	private function getShippingCostbyPrice($cart_total,$cntry_id)
	{		

		$cntry_id	=	trim($cntry_id);
		$is_eu		=	$this->_db->fetchOne("SELECT is_eu FROM 3bit_country WHERE id	=	'".$cntry_id."'");
		if($cart_total>0){
			if($is_eu=='1')
			{
				//Order Value under 10GBP � 1.50GBP
				if($cart_total<=10)
					$shipping	= 1.50;
				else if(($cart_total>=11) && ($cart_total<=30)) //Order Value between 11GBP and 30GBP � 3.00GBP
					$shipping	= 3.00;
				else if(($cart_total>=31) && ($cart_total<=50)) //Order Value between 31GBP and 50GBP � 4.00GBP
					$shipping	= 4.00;
				else if(($cart_total>=51) && ($cart_total<=75)) //Order Value between 51GBP and 75GBP � 5.00GBP
					$shipping	= 5.00;
				else if($cart_total>75) //Order Value over 75GBP - Free
					$shipping	= 0.00;
		
			}else{
				//Order Value under 10GBP � 1.50GBP
				if($cart_total<=10)
					$shipping	= 5.00;
				else if(($cart_total>=11) && ( $cart_total<=30)) //Order Value between 11GBP and 30GBP � 3.00GBP
					$shipping	= 6.50;
				else if(($cart_total>=31) && ($cart_total<=50)) //Order Value between 31GBP and 50GBP � 4.00GBP
					$shipping	= 7.50;
				else if(($cart_total>=51) && ($cart_total<=75)) //Order Value between 51GBP and 75GBP � 5.00GBP
					$shipping	= 8.50;
				else if($cart_total>75) //Order Value over 75GBP - Free
					$shipping	= 0.00;
		
			}
		}
		$shipping = number_format($shipping, 2, '.', '');

		return $shipping;
	}

	// Function to calculate the shipping cost by weight -----------------------------------

	private function getShippingCostbyWeight($weight,$country_id){
		$cntry_id	=	trim($country_id);

		$selZone	=	$this->_db->fetchOne("SELECT zone_id FROM 3bit_country WHERE id	=	'".$cntry_id."'");

		if($selZone){
			$zoneid	=	$selZone;
		}
		
		$gms		=	ceil($weight/1000);
		$gmsthou	=	$gms*1000;
		
		$shipperkg	=	0;

		if($zoneid==1){	
			$selZS		=	"SELECT `zone1_charge` as charge, `weight` , `ship_id` , MIN( ABS( `weight` -".$gmsthou." ) ) AS minvalue FROM 3bit_shipping GROUP BY `weight` ORDER BY minvalue LIMIT 1";				
		}else if($zoneid==2){
			$selZS		=	"SELECT `zone2_charge` as charge, `weight` , `ship_id` , MIN( ABS( `weight` -".$gmsthou." ) ) AS minvalue FROM 3bit_shipping GROUP BY `weight` ORDER BY minvalue LIMIT 1";
		}else if($zoneid==3){
			$selZS		=	"SELECT `zone3_charge` as charge, `weight` , `ship_id` , MIN( ABS( `weight` -".$gmsthou." ) ) AS minvalue FROM 3bit_shipping GROUP BY `weight` ORDER BY minvalue LIMIT 1";
		}else if($zoneid==4){
			$selZS		=	"SELECT `zone4_charge` as charge, `weight` , `ship_id` , MIN( ABS( `weight` -".$gmsthou." ) ) AS minvalue FROM 3bit_shipping GROUP BY `weight` ORDER BY minvalue LIMIT 1";
		}else if($zoneid==5){
			$selZS		=	"SELECT `zone5_charge` as charge, `weight` , `ship_id` , MIN( ABS( `weight` -".$gmsthou." ) ) AS minvalue FROM 3bit_shipping GROUP BY `weight` ORDER BY minvalue LIMIT 1";
		}

		if(!empty($selZS)){
			$zsQry	=	$this->_db->fetchAll($selZS);
			if($zsQry){
				$shipperkg	=	$zsQry[0]['charge'];
			}
		}		

		$shipperkg = number_format($shipperkg, 2, '.', '');

		return $shipperkg;
	}

	private function liveRate()
	{
		$selVat		=	$this->_db->fetchAll("SELECT vat_rate FROM 3bit_product_vats WHERE is_live='1' LIMIT 0,1 ");

		if(count($selVat)>0){
			$vatamt		= 	$selVat[0]['vat_rate'];
		}								   
		$vatper			=	1+($vatamt/100);

		$vatper			=	number_format($vatper,3,'.','');

		return $vatper;
	}

	private function configRate()
	{
		$selVat		=	$this->_db->fetchAll("SELECT vat_id FROM 3bit_product_vats WHERE is_live='1' LIMIT 0,1 ");
		if(count($selVat)>0){
			$vatamt		= 	$selVat[0]['vat_id'];
		}	
		return $vatamt;	
	}

	private function shippingExist($product_id){

		$selShip	=	$this->_db->fetchAll("SELECT `shipping_type` FROM `3bit_products` WHERE  product_id='".$product_id."'");

		if(count($selShip)>0){
			$ship_exist	=	$selShip[0]['shipping_type'];
		}

		return $ship_exist;
	}


}

?>