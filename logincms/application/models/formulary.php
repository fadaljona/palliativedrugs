<?php header( 'Content-Type:text/html; charset=UTF-8' );

class Application_Model_Formulary
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }     
	
	public function getLevels()
    {
        $sql = "SELECT formulary_level FROM 3bit_formulary WHERE 1 AND formulary_level !='' GROUP BY formulary_level ";
        if ($result = $this->_db->query($sql)) {
            $data	= $result->fetchAll();
			foreach($data as $dat){		
				$data1[]=	$dat['formulary_level'];
			}					
			natcasesort($data1);			
			return ($data1);
        }
    }

	public function add($data){
		$form_tags	=	'';
		$keys = array_keys($data);
		foreach($keys as $key ){
			if(strstr($key,'as_values'))
				$form_tags=$data[$key];						
		}

		if(!empty($data['quick_practice_guide']))
			$quick_practice_guide	=	$data['quick_practice_guide'];
		else
			$quick_practice_guide	=	0;
		
		$level		=		$this->findformularylevel($data['formulary_parent_id']);	 
		$order		=		$this->findformularyorder($level);

		$blocks = '';

		if(!empty($data['pre_text_article']))
			$blocks	.=	'pre_text_article#';
		if(!empty($data['class']))
			$blocks	.=	'class#';
		if(!empty($data['indication']))
			$blocks	.=	'indication#';
		if(!empty($data['pharmacology']))
			$blocks	.=	'pharmacology#';
		if(!empty($data['cautions']))
			$blocks	.=	'cautions#';
		if(!empty($data['drug_interactions']))
			$blocks	.=	'drug_interactions#';
		if(!empty($data['undesirable_effects']))
			$blocks	.=	'undesirable_effects#';
		if(!empty($data['dose_and_use']))
			$blocks	.=	'dose_and_use#';
		if(!empty($data['overdose']))
			$blocks	.=	'overdose#';
		if(!empty($data['death_rattle']))
			$blocks	.=	'death_rattle#';
		if(!empty($data['supply']))
			$blocks	.=	'supply#';
		if(!empty($data['references']))
			$blocks	.=	'references#';
		if(!empty($data['post_text_article']))
			$blocks	.=	'post_text_article#';
		
		$query	=	$this->_db->getConnection()->query("INSERT INTO 3bit_formulary SET 
									`language_id`		=	'".$data['language_id']."',
									`formulary_title`	=	'".trim((addslashes($data['formulary_title'])))."',
									`alias`				=	'".trim((addslashes($data['alias'])))."',
									`formulary_type`	=	'".trim(addslashes($data['formulary_type']))."',
									`formulary_parent_id`=	'".trim($data['formulary_parent_id'])."',
									`quick_practice_guide`= '".$quick_practice_guide."',
									`formulary_order`	=	'".$order."',
									`bnf_reference`		=	'".trim(addslashes($data['bnf_reference']))."',									
									`formulary_level`		=	'".trim(addslashes($data['formulary_level']))."',									
									`level`				=	'".$level."',
									`blocks`			=	'".$blocks."',
									`publish_status`	=	'draft',
									`formulary_tags`	=	'".$form_tags."',
									`status`			=	'1',	
									`created_date`		=	now(),
									`modified_date`		=	now()");

		$formulary_id	=	$this->_db->lastInsertId();

		$new_version	=	$this->findTempVersion($formulary_id);

		// To add blocks to temp table ===========================================================================
		if(!empty($blocks))
			$blocksarr		=	explode("#",substr($blocks,0,-1));

	    if(!empty($blocksarr)){
		for($i=0;$i<count($blocksarr);$i++){
			$dont	=	0;
			$btype	=  $blocksarr[$i];
			switch($btype){
				case "pre_text_article":
					  $block_tit=	$data['intro_title'];
					  $content1	=	$data['intro_text'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "class":
					  $block_tit=	$data['class_title'];
					  $content1	=	$data['class_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "indication":
					  $block_tit=	$data['indication_title'];
					  $content1	=	$data['indication_content'];
					  $content2	=	$data['contra_content'];
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='' && $content2 =='')
						   $dont	=	1;
				break;
				case "pharmacology":
					  $block_tit=	$data['pharma_title'];
					  $content1	=	$data['pharma_content'];
					  $content2	=	'';
					  $bio		=	'';//$data['bio_availability'];
					  $onset	=	'';//$data['onset_of_action'];
					  $timepeak	=	'';//$data['plasma_concentration'];
					  $plasma	=	'';//$data['plasma_halflife'];
					  $duration	=	'';//$data['duration_of_action'];
					  if($content1=='' && $bio =='' && $onset =='' && $plasma =='' && $duration =='')
						   $dont = 1;
				break;
				case "cautions":
					  $block_tit=	$data['cautions_title'];
					  $content1	=	$data['cautions_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "drug_interactions":
					  $block_tit=	$data['dinteract_title'];
					  $content1	=	$data['dinteract_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "undesirable_effects":
					  $block_tit=	$data['undesirable_title'];
					  $content1	=	$data['undesirable_effect'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "dose_and_use":
					  $block_tit=	$data['dose_title'];
					  $content1	=	$data['dose_use'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "overdose":
					  $block_tit=	$data['over_title'];
					  $content1	=	$data['over_dose'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break; 
				case "death_rattle":
					  $block_tit=	$data['death_title'];
					  $content1	=	$data['death_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "supply":
					  $block_tit=	$data['supply_title'];
					  $content1	=	$data['supply_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "references":
					  $block_tit=	'';
					  $content1	=	$data['refer_text'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "post_text_article":
					  $block_tit=	$data['post_title'];
					  $content1	=	$data['post_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
			}
			
			if($dont != 1){
				$add_query	=		$this->_db->getConnection()->query("INSERT INTO 3bit_tmp_formulary_blocks SET 
											`formulary_id`		=	'".trim(addslashes($formulary_id))."',
											`sess_id`			=	'".trim(addslashes($_SESSION['wlsession']['asess_id']))."',
											`user_id`			=	'".trim($_SESSION['wlsession']['user_id'])."',											
											`block_type`		=	'".$btype."',
											`block_title`		=	'".trim((addslashes($block_tit)))."',
											`block_content1`	=	'".trim((addslashes($content1)))."',
											`block_content2`	=	'".trim((addslashes($content2)))."',
											`bio_availability`	=	'".trim((addslashes($bio)))."',
											`onset_of_action`	=	'".trim((addslashes($onset)))."',
											`time_to_peak`		=	'".trim((addslashes($timepeak)))."',
											`plasma_halflife`	=	'".trim((addslashes($plasma)))."',
											`duration_of_action`=	'".trim((addslashes($duration)))."',	
											`version`			=	'".$new_version."',
											`status`			=	'1',
											`created_date`		=	now(),
											`modified_date`		=	now()");			
			}
		  }	 
		}	

		$sublib_qry		 =  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$formulary_id."'");
		
		if(count($sublib_qry)>0){				
			$version	=	$sublib_qry[0]['max_version'];
			$arrDecimal	=	explode('.', $version);
			$major		=	$arrDecimal[0];
			$minor		=	'0.'.$arrDecimal[1];
			$this->_db->query("UPDATE 3bit_formulary SET major_version = '".$major."',minor_version = '".$minor."' WHERE formulary_id = '".$formulary_id."'");
		}
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_formulary','add',$formulary_id,'success');
    }

	public function read($id){
	      $data			=	$this->_db->fetchAll("SELECT * FROM `3bit_formulary` WHERE formulary_id ='".$id."'");

		  $lat_version	=	0;
		  	 
		  $blocks	=	explode("#",substr($data[0]['blocks'],0,-1));	  
		 
		  /////////////////////////// Selecting blocks content for assigned blocks //////////////////////////////////////////////////

		  for($k=0;$k<count($blocks);$k++){

		   $block_type		=	$blocks[$k];

		   $res_query		=	$this->_db->fetchAll("SELECT * FROM 3bit_tmp_formulary_blocks WHERE `block_type` = '".$block_type."' AND `formulary_id` = '".stripslashes($data[0]['formulary_id'])."' ORDER BY version DESC LIMIT 0,1 ");  

		   if(count($res_query)>0){

			   foreach($res_query as $brow){
	
				   $data[$block_type]['block_title']		=	stripslashes($brow['block_title']);

				   $data[$block_type]['block_content1']		=	stripslashes($brow['block_content1']);

				   $data[$block_type]['block_content2']		=	stripslashes($brow['block_content2']);

				   $data[$block_type]['bio_availability']	=	stripslashes($brow['bio_availability']);

				   $data[$block_type]['onset_of_action']	=	stripslashes($brow['onset_of_action']);

				   $data[$block_type]['time_to_peak']		=	stripslashes($brow['time_to_peak']);

				   $data[$block_type]['plasma_halflife']	=	stripslashes($brow['plasma_halflife']);

				   $data[$block_type]['duration_of_action']	=	stripslashes($brow['duration_of_action']);
			   }
		   }
		} 		
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_formulary','view',$id,'success');
		return $data;
	}


	public function update($data){			

		$form_tags	=	'';
		$keys = array_keys($data);
		foreach($keys as $key ){
			if(strstr($key,'as_values'))
				$form_tags=$data[$key];						
		}	

		if(!empty($data['quick_practice_guide']))
			$quick_practice_guide	=	$data['quick_practice_guide'];
		else
			$quick_practice_guide	=	0;	
		
		$level		=		$this->findformularylevel($data['formulary_parent_id']);
		
		$blocks = '';

		if(!empty($data['pre_text_article']))
			$blocks	.=	'pre_text_article#';
		if(!empty($data['class']))
			$blocks	.=	'class#';
		if(!empty($data['indication']))
			$blocks	.=	'indication#';
		if(!empty($data['pharmacology']))
			$blocks	.=	'pharmacology#';
		if(!empty($data['cautions']))
			$blocks	.=	'cautions#';
		if(!empty($data['drug_interactions']))
			$blocks	.=	'drug_interactions#';
		if(!empty($data['undesirable_effects']))
			$blocks	.=	'undesirable_effects#';
		if(!empty($data['dose_and_use']))
			$blocks	.=	'dose_and_use#';
		if(!empty($data['overdose']))
			$blocks	.=	'overdose#';
		if(!empty($data['death_rattle']))
			$blocks	.=	'death_rattle#';
		if(!empty($data['supply']))
			$blocks	.=	'supply#';
		if(!empty($data['references']))
			$blocks	.=	'references#';
		if(!empty($data['post_text_article']))
			$blocks	.=	'post_text_article#';		
	
		$query		=		$this->_db->getConnection()->query("UPDATE 3bit_formulary SET 
									`language_id`		=	'".$data['language_id']."',
									`formulary_title`	=	'".trim((addslashes($data['formulary_title'])))."',
									`alias`				=	'".trim((addslashes($data['alias'])))."',
									`formulary_type`	=	'".trim(addslashes($data['formulary_type']))."',
									`formulary_parent_id`=	'".trim($data['formulary_parent_id'])."',
									`quick_practice_guide`= '".$quick_practice_guide."',
									`bnf_reference`		=	'".trim(addslashes($data['bnf_reference']))."',	
									`formulary_level`		=	'".trim(addslashes($data['formulary_level']))."',
									`level`				=	'".$level."',
									`blocks`			=	'".$blocks."',									
									`formulary_tags`	=	'".$form_tags."',															
									`modified_date`		=	now() WHERE formulary_id='".$data['id']."'");		

		$formulary_id		=	$data['id'];
	
		if(!empty($blocks))
			$blocksarr		=	explode("#",substr($blocks,0,-1));

		$check_query	= $this->_db->fetchAll("SELECT * FROM 3bit_tmp_formulary_blocks WHERE sess_id='".$_SESSION['wlsession']['asess_id']."' AND user_id='".$_SESSION['user_id']."' AND `formulary_id` = '".stripslashes($data['id'])."'");

		$new_version	= $this->findTempVersion($data['id']);

		if(count($check_query)>0){
		  if(!empty($blocksarr)){

		   for($i=0;$i<count($blocksarr);$i++){
			$dont	=	0;
			$btype	=  $blocksarr[$i];		

			switch($btype){
				case "pre_text_article":
					  $block_tit=	$data['intro_title'];
					  $content1	=	$data['intro_text'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "class":
					  $block_tit=	$data['class_title'];
					  $content1	=	$data['class_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "indication":
					  $block_tit=	$data['indication_title'];
					  $content1	=	$data['indication_content'];
					  $content2	=	$data['contra_content'];
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='' && $content2 =='')
						   $dont	=	1;
				break;
				case "pharmacology":
					  $block_tit=	$data['pharma_title'];
					  $content1	=	$data['pharma_content'];
					  $content2	=	'';
					   $bio		=	'';//$data['bio_availability'];
					  $onset	=	'';//$data['onset_of_action'];
					  $timepeak	=	'';//$data['plasma_concentration'];
					  $plasma	=	'';//$data['plasma_halflife'];
					  $duration	=	'';//$data['duration_of_action'];
					  if($content1=='' && $bio =='' && $onset =='' && $plasma =='' && $duration =='')
						   $dont = 1;
				break;
				case "cautions":
					  $block_tit=	$data['cautions_title'];
					  $content1	=	$data['cautions_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "drug_interactions":
					  $block_tit=	$data['dinteract_title'];
					  $content1	=	$data['dinteract_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "undesirable_effects":
					  $block_tit=	$data['undesirable_title'];
					  $content1	=	$data['undesirable_effect'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "dose_and_use":
					  $block_tit=	$data['dose_title'];
					  $content1	=	$data['dose_use'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "overdose":
					  $block_tit=	$data['over_title'];
					  $content1	=	$data['over_dose'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break; 
				case "death_rattle":
					  $block_tit=	$data['death_title'];
					  $content1	=	$data['death_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "supply":
					  $block_tit=	$data['supply_title'];
					  $content1	=	$data['supply_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "references":
					  $block_tit=	'';
					  $content1	=	$data['refer_text'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "post_text_article":
					  $block_tit=	$data['post_title'];
					  $content1	=	$data['post_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
			}

			$selblock	=		$this->_db->fetchAll("SELECT block_id,version FROM 3bit_tmp_formulary_blocks WHERE block_type='".$btype."' AND `formulary_id`='".trim(addslashes($data['id']))."' AND `sess_id`='".trim(addslashes($_SESSION['wlsession']['asess_id']))."' AND `user_id`	='".trim($_SESSION['wlsession']['user_id'])."'");
			
			if(count($selblock)>0){				

				$new_version	=	$selblock[0]['version'];

				$add_query		=	$this->_db->getConnection()->query("UPDATE 3bit_tmp_formulary_blocks SET 
											`formulary_id`		=	'".trim(addslashes($data['id']))."',
											`sess_id`			=	'".trim(addslashes($_SESSION['wlsession']['asess_id']))."',
											`user_id`			=	'".trim($_SESSION['wlsession']['user_id'])."',											
											`block_type`		=	'".$btype."',
											`block_title`		=	'".trim((addslashes($block_tit)))."',
											`block_content1`	=	'".trim((addslashes($content1)))."',
											`block_content2`	=	'".trim((addslashes($content2)))."',
											`bio_availability`	=	'".trim((addslashes($bio)))."',
											`onset_of_action`	=	'".trim((addslashes($onset)))."',
											`time_to_peak`		=	'".trim((addslashes($timepeak)))."',
											`plasma_halflife`	=	'".trim((addslashes($plasma)))."',
											`duration_of_action`=	'".trim((addslashes($duration)))."',											
											`status`			=	'1',									
											`modified_date`		=	now() WHERE block_type ='".$btype."' AND `formulary_id`='".trim(addslashes($data['id']))."' AND `sess_id`=	'".trim(addslashes($_SESSION['wlsession']['asess_id']))."' AND `user_id`	='".trim($_SESSION['wlsession']['user_id'])."'");				
			}else{
				if($dont !=1){
					$add_query	=	$this->_db->getConnection()->query("INSERT INTO 3bit_tmp_formulary_blocks SET 
												`formulary_id`		=	'".trim(addslashes($data['id']))."',
												`sess_id`			=	'".trim(addslashes($_SESSION['wlsession']['asess_id']))."',
												`user_id`			=	'".trim($_SESSION['wlsession']['user_id'])."',												
												`block_type`		=	'".$btype."',
												`block_title`		=	'".trim((addslashes($block_tit)))."',
												`block_content1`	=	'".trim((addslashes($content1)))."',
												`block_content2`	=	'".trim((addslashes($content2)))."',
												`bio_availability`	=	'".trim((addslashes($bio)))."',
												`onset_of_action`	=	'".trim((addslashes($onset)))."',
												`time_to_peak`		=	'".trim((addslashes($timepeak)))."',
												`plasma_halflife`	=	'".trim((addslashes($plasma)))."',
												`duration_of_action`=	'".trim((addslashes($duration)))."',	
												`version`			=	'".$new_version."',
												`status`			=	'1',
												`created_date`		=	now(),
												`modified_date`		=	now()"); 
				}
			  }													  
		   }		
		}

		}else{
		   if(!empty($blocksarr)){
			for($i=0;$i<count($blocksarr);$i++){
			$dont	=	0;
			$btype	=  $blocksarr[$i];
			switch($btype){
				case "pre_text_article":
					  $block_tit=	$data['intro_title'];
					  $content1	=	$data['intro_text'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "class":
					  $block_tit=	$data['class_title'];
					  $content1	=	$data['class_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "indication":
					  $block_tit=	$data['indication_title'];
					  $content1	=	$data['indication_content'];
					  $content2	=	$data['contra_content'];
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='' && $content2 =='')
						   $dont	=	1;
				break;
				case "pharmacology":
					  $block_tit=	$data['pharma_title'];
					  $content1	=	$data['pharma_content'];
					  $content2	=	'';
					  $bio		=	'';//$data['bio_availability'];
					  $onset	=	'';//$data['onset_of_action'];
					  $timepeak	=	'';//$data['plasma_concentration'];
					  $plasma	=	'';//$data['plasma_halflife'];
					  $duration	=	'';//$data['duration_of_action'];
					  if($content1=='' && $bio =='' && $onset =='' && $plasma =='' && $duration =='')
						   $dont = 1;
				break;
				case "cautions":
					  $block_tit=	$data['cautions_title'];
					  $content1	=	$data['cautions_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "drug_interactions":
					  $block_tit=	$data['dinteract_title'];
					  $content1	=	$data['dinteract_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "undesirable_effects":
					  $block_tit=	$data['undesirable_title'];
					  $content1	=	$data['undesirable_effect'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "dose_and_use":
					  $block_tit=	$data['dose_title'];
					  $content1	=	$data['dose_use'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "overdose":
					  $block_tit=	$data['over_title'];
					  $content1	=	$data['over_dose'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break; 
				case "death_rattle":
					  $block_tit=	$data['death_title'];
					  $content1	=	$data['death_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "supply":
					  $block_tit=	$data['supply_title'];
					  $content1	=	$data['supply_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "references":
					  $block_tit=	'';
					  $content1	=	$data['refer_text'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
				case "post_text_article":
					  $block_tit=	$data['post_title'];
					  $content1	=	$data['post_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
				break;
			}
			
			if($dont != 1){
				$add_query	=		$this->_db->getConnection()->query("INSERT INTO 3bit_tmp_formulary_blocks SET 
											`formulary_id`		=	'".trim(addslashes($data['id']))."',
											`sess_id`			=	'".trim(addslashes($_SESSION['wlsession']['asess_id']))."',
											`user_id`			=	'".trim($_SESSION['wlsession']['user_id'])."',											
											`block_type`		=	'".$btype."',
											`block_title`		=	'".trim((addslashes($block_tit)))."',
											`block_content1`	=	'".trim((addslashes($content1)))."',
											`block_content2`	=	'".trim((addslashes($content2)))."',
											`bio_availability`	=	'".trim((addslashes($bio)))."',
											`onset_of_action`	=	'".trim((addslashes($onset)))."',
											`time_to_peak`		=	'".trim((addslashes($timepeak)))."',
											`plasma_halflife`	=	'".trim((addslashes($plasma)))."',
											`duration_of_action`=	'".trim((addslashes($duration)))."',	
											`version`			=	'".$new_version."',
											`status`			=	'1',
											`created_date`		=	now(),
											`modified_date`		=	now()");
			}
		}
	  }
	}

	$sublib_qry		 =  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$data['id']."'");
		
	if(count($sublib_qry)>0){				
		$version	=	$sublib_qry[0]['max_version'];
		$arrDecimal	=	explode('.', $version);
		$major		=	$arrDecimal[0];
		$minor		=	'0.'.$arrDecimal[1];
		$this->_db->query("UPDATE 3bit_formulary SET major_version = '".$major."',minor_version = '".$minor."' WHERE formulary_id = '".$data['id']."'");
	}
	$audit	=	new Application_Model_Audits();
	$audit->insert_audit('3bit_formulary','update',$data['id'],'success');

   }


	public function save_version($data){

		if(!empty($data['quick_practice_guide']))
			$quick_practice_guide	=	$data['quick_practice_guide'];
		else
			$quick_practice_guide	=	0;		
		
		$level		=		$this->findformularylevel($data['formulary_parent_id']);
		
		$blocks = '';

		if(!empty($data['pre_text_article']))
			$blocks	.=	'pre_text_article#';
		if(!empty($data['class']))
			$blocks	.=	'class#';
		if(!empty($data['indication']))
			$blocks	.=	'indication#';
		if(!empty($data['pharmacology']))
			$blocks	.=	'pharmacology#';
		if(!empty($data['cautions']))
			$blocks	.=	'cautions#';
		if(!empty($data['drug_interactions']))
			$blocks	.=	'drug_interactions#';
		if(!empty($data['undesirable_effects']))
			$blocks	.=	'undesirable_effects#';
		if(!empty($data['dose_and_use']))
			$blocks	.=	'dose_and_use#';
		if(!empty($data['overdose']))
			$blocks	.=	'overdose#';
		if(!empty($data['death_rattle']))
			$blocks	.=	'death_rattle#';
		if(!empty($data['supply']))
			$blocks	.=	'supply#';
		if(!empty($data['references']))
			$blocks	.=	'references#';
		if(!empty($data['post_text_article']))
			$blocks	.=	'post_text_article#';	
	
		$query		=		$this->_db->getConnection()->query("UPDATE 3bit_formulary SET 
									`language_id`		=	'".$data['language_id']."',
									`formulary_title`	=	'".trim((addslashes($data['formulary_title'])))."',
									`alias`				=	'".trim((addslashes($data['alias'])))."',
									`formulary_type`	=	'".trim(addslashes($data['formulary_type']))."',
									`formulary_parent_id`=	'".trim($data['formulary_parent_id'])."',
									`quick_practice_guide`= '".$quick_practice_guide."',
									`bnf_reference`		=	'".trim(addslashes($data['bnf_reference']))."',	
									`formulary_level`		=	'".trim(addslashes($data['formulary_level']))."',
									`level`				=	'".$level."',
									`blocks`			=	'".$blocks."',									
									`status`			=	'1',									
									`modified_date`		=	now() WHERE formulary_id='".$data['id']."'");		

		$formulary_id		=	$data['id'];
	
		if(!empty($blocks))
			$blocksarr		=	explode("#",substr($blocks,0,-1));		

		$new_version	= $this->findTempVersion($data['id']);
		
		if(!empty($blocksarr)){
			for($i=0;$i<count($blocksarr);$i++){
				$dont	=	0;
				$btype	=  $blocksarr[$i];
				switch($btype){
					case "pre_text_article":
						  $block_tit=	$data['intro_title'];
						  $content1	=	$data['intro_text'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "class":
						  $block_tit=	$data['class_title'];
						  $content1	=	$data['class_content'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "indication":
						  $block_tit=	$data['indication_title'];
						  $content1	=	$data['indication_content'];
						  $content2	=	$data['contra_content'];
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='' && $content2 =='')
							   $dont	=	1;
					break;
					case "pharmacology":
						  $block_tit=	$data['pharma_title'];
						  $content1	=	$data['pharma_content'];
						  $content2	=	'';
						  $bio		=	'';//$data['bio_availability'];
						  $onset	=	'';//$data['onset_of_action'];
						  $timepeak	=	'';//$data['plasma_concentration'];
						  $plasma	=	'';//$data['plasma_halflife'];
						  $duration	=	'';//$data['duration_of_action'];
						  if($content1=='' && $bio =='' && $onset =='' && $plasma =='' && $duration =='')
							   $dont = 1;
					break;
					case "cautions":
						  $block_tit=	$data['cautions_title'];
						  $content1	=	$data['cautions_content'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "drug_interactions":
					  $block_tit=	$data['dinteract_title'];
					  $content1	=	$data['dinteract_content'];
					  $content2	=	'';
					  $bio		=	'';
					  $onset	=	'';
					  $timepeak	=	'';
					  $plasma	=	'';
					  $duration	=	'';
					  if($content1=='')
						  $dont	=	1;
					break;
					case "undesirable_effects":
						  $block_tit=	$data['undesirable_title'];
						  $content1	=	$data['undesirable_effect'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "dose_and_use":
						  $block_tit=	$data['dose_title'];
						  $content1	=	$data['dose_use'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "overdose":
						  $block_tit=	$data['over_title'];
						  $content1	=	$data['over_dose'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break; 
					case "death_rattle":
						  $block_tit=	$data['death_title'];
						  $content1	=	$data['death_content'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "supply":
						  $block_tit=	$data['supply_title'];
						  $content1	=	$data['supply_content'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "references":
						  $block_tit=	'';
						  $content1	=	$data['refer_text'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
					case "post_text_article":
						  $block_tit=	$data['post_title'];
						  $content1	=	$data['post_content'];
						  $content2	=	'';
						  $bio		=	'';
						  $onset	=	'';
						  $timepeak	=	'';
						  $plasma	=	'';
						  $duration	=	'';
						  if($content1=='')
							  $dont	=	1;
					break;
				}
			
				if($dont != 1){					  
					$add_query	=		$this->_db->getConnection()->query("INSERT INTO 3bit_tmp_formulary_blocks SET 
												`formulary_id`		=	'".trim(addslashes($data['id']))."',
												`sess_id`			=	'".trim(addslashes($_SESSION['wlsession']['asess_id']))."',
												`user_id`			=	'".trim($_SESSION['wlsession']['user_id'])."',											
												`block_type`		=	'".$btype."',
												`block_title`		=	'".trim((addslashes($block_tit)))."',
												`block_content1`	=	'".trim((addslashes($content1)))."',
												`block_content2`	=	'".trim((addslashes($content2)))."',
												`bio_availability`	=	'".trim((addslashes($bio)))."',
												`onset_of_action`	=	'".trim((addslashes($onset)))."',
												`time_to_peak`		=	'".trim((addslashes($timepeak)))."',
												`plasma_halflife`	=	'".trim((addslashes($plasma)))."',
												`duration_of_action`=	'".trim((addslashes($duration)))."',	
												`version`			=	'".$new_version."',
												`status`			=	'1',
												`created_date`		=	now(),
												`modified_date`		=	now()");
				}
			}
		}	
		$sublib_qry		 =  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$data['id']."'");
		
		if(count($sublib_qry)>0){				
			$version	=	$sublib_qry[0]['max_version'];
			$arrDecimal	=	explode('.', $version);
			$major		=	$arrDecimal[0];
			$minor		=	'0.'.$arrDecimal[1];
			$this->_db->query("UPDATE 3bit_formulary SET minor_version = '".$minor."' WHERE formulary_id = '".$data['id']."'");
		}
		
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_formulary','update',$data['id'],'success');
    }

	public function delete($id)
	{
		$this->_db->delete("3bit_formulary",'formulary_id='.$id); 			
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_formulary','delete',$id,'success');
	}


	public function findcatlevel($parent_id)
	{

		$sublib_qry  = 	$this->_db->fetchAll("SELECT cat_id,cat_level from `3bit_categories` where cat_id='$parent_id'");

		$numrows 	 = 	count($sublib_qry);
		if ($numrows == 0){	
			$level	= 1;
		}else{
			$sublib_id	 = $sublib_qry[0]['cat_id'];

			$cat_level	 = $sublib_qry[0]['cat_level'];

			$level		 = $cat_level + 1;
		}
		return $level; 
	}

	public function findcatorder($cat_level)
	{

		$sublib_qry  = $this->_db->fetchAll("SELECT max(cat_order) as max_order from `3bit_categories` where cat_level='$cat_level'");		
		
		$numrows	 = count($sublib_qry);
		if ($numrows == 0){	
			$new_order	 = 1;
		}else{
			$new_order	 = $sublib_qry[0]['max_order'] + 1;
		}

		return $new_order;

	}


	public function findformularylevel($parent_id)
	{

		$sublib_qry  = 	$this->_db->fetchAll("SELECT formulary_id,level from `3bit_formulary` where formulary_id='$parent_id'");

		$numrows 	 = 	count($sublib_qry);
		if ($numrows == 0){	
			$level	= 1;
		}else{
			$sublib_id	 = $sublib_qry[0]['formulary_id'];

			$cat_level	 = $sublib_qry[0]['level'];

			$level		 = $cat_level + 1;
		}
		return $level; 
	}

	public function findformularyorder($cat_level)
	{

		$sublib_qry  = $this->_db->fetchAll("SELECT max(formulary_order) as max_order from `3bit_formulary` where level='$cat_level'");		
		
		$numrows	 = count($sublib_qry);
		if ($numrows == 0){	
			$new_order	 = 1;
		}else{
			$new_order	 = $sublib_qry[0]['max_order'] + 1;
		}

		return $new_order;

	}

	public function parent_list($passdir,$i,$selected)
	{
	
		$i.="---";
			
		$sublib_qry  = $this->_db->fetchAll("SELECT formulary_id,formulary_parent_id,formulary_title from `3bit_formulary` where formulary_parent_id='$passdir'");
			
		$numrows	 = count($sublib_qry);

		$second = '';
			
		if ($numrows > 0){ 			
			foreach($sublib_qry as $sublib_values)	{				
				$sublib_id					= $sublib_values["formulary_id"];			
				$subparent_id				= $sublib_values["formulary_parent_id"];				
				$sublib_name				= $sublib_values["formulary_title"];	  				
				if ($selected == $sublib_id){								  						
					$sel ="selected";										  					
				}else{														  				
					$sel ="";												  					
				}															  				
				$second.= "<option value='$sublib_id' $sel>$i$sublib_name</option>";									
					
				$second.= $this->parent_list($sublib_id,$i,$selected);//recursive call to the function				
			}
			return $second;
		}else{			   		
			return $second;
		}
	}

	public function list_temp($selected)
	{
	  $res		=	 $this->_db->fetchAll("SELECT * FROM `3bit_article_template` WHERE temp_status= '1'");
	  $numrows	=	count($res);																					  
	  $temp		=	'';																									  
	  foreach($res as $temps){																						  
		  $temp_id		=	$temps['temp_id'];																		  
		  $temp_name	=	$temps['temp_filename'];																	  
		  if ($selected == $temp_id){								  						
			$sel ="selected";										  					
		  }else{														  				
			$sel ="";												  					
		  }																											  
		  $temp	.= "<option value='$temp_id' $sel>$temp_name</option>";												  
	  }		  
	  return $temp;																									  
	}

	public function list_art($selected)
	{
	  $res		=	 $this->_db->fetchAll("SELECT * FROM `3bit_articles` WHERE status= '1'");
	  $numrows	=	count($res);																					  
	  $art = '';																									  
	  foreach($res as $arts){																						  
		  $art_id		=	$arts['article_id'];																		  
		  $art_name		=	$arts['article_title'];																	  
		  if ($selected == $art_id){								  						
			$sel ="selected";										  					
		  }else{														  				
			$sel ="";												  					
		  }																											  
		  $art	.= "<option value='$art_id' $sel>$art_name</option>";												  
	  }																												  
	  return $art;																									  
	}	

	public function list_lang($selected)
	{
	  $res		=	 $this->_db->fetchAll("SELECT * FROM `3bit_language` WHERE status= '1'");
	  $numrows	=	count($res);																					  
	  $lang = '';																									  
	  foreach($res as $arts){																						  
		  $lang_id			=	$arts['language_id'];																		  
		  $lang_name		=	$arts['language_name']." [".$arts['language_code']."]";																	  
		  if ($selected == $lang_id){								  						
			$sel ="selected";										  					
		  }else{														  				
			$sel ="";												  					
		  }																											  
		  $lang	.= "<option value='$lang_id' $sel>$lang_name</option>";												  
	  }																												  
	  return $lang;																									  
	}	


	public function findTempVersion($formulary_id)
	{

		$sublib_qry  = $this->_db->fetchAll("SELECT max(version) as max_version from `3bit_tmp_formulary_blocks` where `formulary_id`='$formulary_id'");
		if(count($sublib_qry)>0){
			$new_version = $sublib_qry[0]['max_version'] + 0.01;
		}else{
			$new_version = 0.01;
		}
		
		return $new_version;  
	}	

	// To remove special chars in string ==========================================================

	function seems_utf8($str) {
		$length = strlen($str);
		for ($i=0; $i < $length; $i++) {
			$c = ord($str[$i]);
			if ($c < 0x80) $n = 0; # 0bbbbbbb
			elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
			elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
			elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
			elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
			elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
			else return false; # Does not match any model
			for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
				if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
					return false;
			}
		}
		return true;
	}

	function utf8_uri_encode( $utf8_string, $length = 0 ) {
		$unicode = '';
		$values = array();
		$num_octets = 1;
		$unicode_length = 0;
		$string_length = strlen( $utf8_string );
		for ($i = 0; $i < $string_length; $i++ ) {

			$value = ord( $utf8_string[ $i ] );

			if ( $value < 128 ) {
				if ( $length && ( $unicode_length >= $length ) )
					break;
				$unicode .= chr($value);
				$unicode_length++;
			} else {
				if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

				$values[] = $value;

				if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
					break;
				if ( count( $values ) == $num_octets ) {
					if ($num_octets == 3) {
						$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
						$unicode_length += 9;
					} else {
						$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
						$unicode_length += 6;
					}

					$values = array();
					$num_octets = 1;
				}
			}
		}
		return $unicode;
	}

	public function remExtra($str)
	{
		$string	 	= 	strtolower($str);
		$special 	=	array('&amp;','&nbsp;',';','`','<','>','!','&','*',',','/','~','@','#','$','%','^','(',')','+','=','_','�',':','"',"'",'[',']','{','}'); 
		$dash		=	array('----','---','--');
		$spaceStr	=	str_replace($special,' ',$string);
		$dashStr	=	str_replace(' ','-',$spaceStr);
		$dashStr	=	str_replace('?','',$dashStr);
		$outStr		=	str_replace($dash,'-',$dashStr);
		return $outStr;
	}

	/************ Function to remove special char and replace with dash **************/

	public function remSpecial($title) {	
		$title = strip_tags($title);
		// Preserve escaped octets.
		$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
		// Remove percent signs that are not part of an octet.
		$title = str_replace('%', '', $title);
		// Restore octets.
		$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

		if ($this->seems_utf8($title)) {
			if (function_exists('mb_strtolower')) {
				$title = mb_strtolower($title, 'UTF-8');
			}
			$title = $this->utf8_uri_encode($title, 200);
		}

		$title = strtolower($title);
		$title = preg_replace('/&.+?;/', '', $title); // kill entities
		$title = str_replace('.', '-', $title);
		$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
		$title = preg_replace('/\s+/', '-', $title);
		$title = preg_replace('|-+|', '-', $title);
		$title = trim($title, '-');
		return $title;
	}	

	public function genFootNumbers($matches)
	{
		global $uniquearr;	
		$pos=(array_search($matches[0],$uniquearr))+1;
		return "<sup id='up".$pos."'><a href='#".$pos."'>$pos</a></sup>";	
	}


	//*********************   Function to get parsed contents ***************************//
	public function getParsed($data,$preview='0')
	{
		global $matchCount,	$uniquearr,$lastuniquearr;
		$data		=	$this->parsePage($data,$preview);
		$matchCount	=	preg_match_all('/(\{.*?\})/',$data,$matches);
		$uniquearr	=	array_values(array_unique($matches[0]));
		if($matchCount>0){		
			$replacement1= preg_replace_callback('/(\{.*?\})/',array($this,'genFootNumbers'),$data);
			$replacement1=preg_replace('/<\/sup><sup(.*?)>/','</sup><sup$1>,',$replacement1);
		}
		if($matchCount<=0){
			$replacement1	=	$data;
		}else{
			for($k=0;$k<count($uniquearr);$k++)
				$lastuniquearr[]	=	$uniquearr[$k];
		}  
		$parsedcontent	=	$replacement1; 
		return $parsedcontent;
	} 
	//********************** Function to parse the snippets in the content ***************//

	public function parsePage($content,$preview)
	{	
		$matchCount	=	preg_match_all('/\[(\[.*?\])\]/', $content, $matches);
		
		if($matchCount==0)
			return $content; 
		
		foreach($matches[0] as $value){ 		
			$value1=preg_replace("/\[\[/","/\[\[",$value);
			$value1 =preg_replace("/\]\]/","\]\]/",$value1);
			if($preview=='1')
				$snippets[$value1] = '<span class="snippet" id="'.$value.'"></span>'.$this->getTables($value);
			else
				$snippets[$value1] = $this->getTables($value);
		}
		$parsedContent	=	preg_replace(array_keys($snippets),array_values($snippets),$content);
		
		return $parsedContent;
	}

	//**************************** Get Snippet text by snippet name **************************//

	public function getTables($name)
	{
		$name =str_replace("[[","",$name);
		$name =str_replace("]]","",$name);

		$idplusversion	=	explode("#",str_replace("snippet-id","",$name));	

		$snippetid		=	$idplusversion[0];

		$version		=	$idplusversion[1];

		$sql			=	"SELECT long_text,snippet_title FROM 3bit_formulary_snippets WHERE snippet_id='".$snippetid."' AND version= '".$version."' ";	

		$snippets		=	$this->_db->fetchAll($sql);

		$numsnippets	=	count($snippets);
		
		if($numsnippets<>0){
			foreach($snippets as $resR){
				$long_text	=	stripslashes($resR['long_text']);
				/*$content = preg_replace('/<p[^>]*>/', '', $long_text); // Remove the start <p> or <p attr="">
				$content = preg_replace('/<\/p>/', '<br />', $content); // Replace the end*/
				return (stripslashes($long_text));
			}
		}else{ 
			$sql			=	"SELECT long_text,snippet_title FROM 3bit_formulary_snippet_versions WHERE snippet_id='".$snippetid."' AND version= '".$version."' ";	

			$snippets		=	$this->_db->fetchAll($sql);

			$numsnippets	=	count($snippets);
			
			if($numsnippets<>0){
				foreach($snippets as $resR){
					$long_text	=	stripslashes($resR['long_text']);
					return (stripslashes($long_text));
				}
			}
		}
	}


	////////////////////////////////// For preview section ////////////////////////////////////////////////////////


	public function updatetextarea($data)
	{	
		if($data['block_type']!='indications' && $data['block_type']!='contraindications')
			$block_type	=	str_replace("-","_",$data['block_type']);
		else
			$block_type	=	'indication';
		
		$selBlock		=	$this->_db->fetchAll("SELECT ".$data['content_type']." FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$data['formulary']."' AND version='".$data['version']."' AND block_type='".$block_type."'");
		if(count($selBlock)>0){
			  $content	=	$selBlock[0][$data['content_type']];
			  $content	=	stripslashes($content);
		}	
		
		die($content."^"."S");
	}


	public function findsnippetversion($snippetid)
	{

		$sublib_qry  = $this->_db->fetchAll("SELECT max(`version`) as max_version from `3bit_formulary_snippet_versions` where snippet_id='$snippetid'");		
		
		$numrows	 = count($sublib_qry);
		if ($numrows == 0){	
			$new_order	 = 0.01;
		}else{
			$new_order	 = $sublib_qry[0]['max_version']+ 0.01;
		}

		return $new_order;

	}


	public function updateblocks($data)
	{
		if($data['block_type']!='indications' && $data['block_type']!='contraindications')
				$block_type	=	str_replace("-","_",$data['block_type']);
			else
				$block_type	=	'indication';

		if(trim($data['snippetid'])==''){
			$upBlock		=	$this->_db->getConnection()->query("UPDATE 3bit_tmp_formulary_blocks SET ".$data['content_type']." = '".addslashes($data['bcontent'])."' WHERE formulary_id='".$data['formulary']."' AND version='".$data['version']."' AND block_type='".$block_type."'");
			if($upBlock){
				die("S");
			}
		}else{

			$name =str_replace("[[","",$data['snippetid']);
			$name =str_replace("]]","",$name);	

			$idplusversion	=	explode("#",str_replace("snippet-id","",$name));	

			$snippetid		=	$idplusversion[0];

			$version		=	$idplusversion[1];

			$sql			=	"SELECT language_id,snippet_title,snippet_number,snippet_type,long_text FROM 3bit_formulary_snippets WHERE snippet_id='".$snippetid."' AND version= '".$version."' ";	

			$snippets		=	$this->_db->fetchAll($sql);

			$numsnippets	=	count($snippets);
			
			if($numsnippets<>0){
				foreach($snippets as $resR){
					$long_text	=	stripslashes($resR['long_text']);
					$newversion	=	$this->findsnippetversion($snippetid);
					if(trim($data['bcontent'])!=trim($long_text)){
						$query1		=	$this->_db->getConnection()->query("INSERT INTO 3bit_formulary_snippet_versions SET
										snippet_id			=	'".$snippetid."',
										language_id			=	'".addslashes($resR['language_id'])."',
										snippet_title		=	'".addslashes($data['snippettit'])."',
										snippet_number		=	'".addslashes($data['snippetnum'])."',
										snippet_type		=	'".addslashes($resR['snippet_type'])."',
										long_text			=	'".addslashes($data['bcontent'])."',
										version				=	'".$newversion."',
										status				=	'1',
										created_date		=	now(),
										modified_date		=	now()");

						$upSnippet		=	$this->_db->query("UPDATE 3bit_formulary_snippets SET snippet_title='".addslashes($data['snippettit'])."',snippet_number='".addslashes($data['snippetnum'])."',long_text = '".addslashes($data['bcontent'])."',version='".$newversion."' WHERE snippet_id='".$snippetid."'");

						$newsnippetag	=	'[[snippet-id'.$snippetid.'#'.$newversion.']]';

						$selBlock		=	$this->_db->fetchAll("SELECT block_content1,block_content2 FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$data['formulary']."' AND version='".$data['version']."' AND block_type='".$block_type."'");

						if(count($selBlock)>0){
							$block_content1	=	$selBlock[0]['block_content1'];
							$block_content2	=	$selBlock[0]['block_content2'];
							$newblock1		=	str_replace($data['snippetid'],$newsnippetag,stripslashes($block_content1));
							$newblock2		=	str_replace($data['snippetid'],$newsnippetag,stripslashes($block_content2));
							$this->_db->getConnection()->query("UPDATE 3bit_tmp_formulary_blocks SET block_content1 = '".addslashes($newblock1)."',block_content2='".addslashes($newblock2)."' WHERE formulary_id='".$data['formulary']."' AND version='".$data['version']."' AND block_type='".$block_type."'");
						}																												  
					} 

					die("S");
				}
			}else{ 
				$sql			=	"SELECT language_id,snippet_title,snippet_number,snippet_type,long_text FROM 3bit_formulary_snippet_versions WHERE snippet_id='".$snippetid."' AND version= '".$version."' ";	

				$snippets		=	$this->_db->fetchAll($sql);

				$numsnippets	=	count($snippets);
				
				if($numsnippets<>0){
					foreach($snippets as $resR){
						$long_text	=	stripslashes($resR['long_text']);
						$newversion	=	$this->findsnippetversion($snippetid);
						if(trim($data['bcontent'])!=trim($long_text)){
							$query1		=	$this->_db->getConnection()->query("INSERT INTO 3bit_formulary_snippet_versions SET
										snippet_id			=	'".$snippetid."',
										language_id			=	'".addslashes($resR['language_id'])."',
										snippet_title		=	'".addslashes($data['snippettit'])."',
										snippet_number		=	'".addslashes($data['snippetnum'])."',
										snippet_type		=	'".addslashes($resR['snippet_type'])."',
										long_text			=	'".addslashes($data['bcontent'])."',
										version				=	'".$newversion."',
										status				=	'1',
										created_date		=	now(),
										modified_date		=	now()");

							$upSnippet		=	$this->_db->getConnection()->query("UPDATE 3bit_formulary_snippets SET snippet_title='".addslashes($data['snippettit'])."',snippet_number='".addslashes($data['snippetnum'])."',long_text = '".addslashes($data['bcontent'])."',version='".$newversion."' WHERE snippet_id='".$snippetid."'");
							
							$newsnippetag	=	'[[snippet-id'.$snippetid.'#'.$newversion.']]';

							$selBlock		=	$this->_db->fetchAll("SELECT block_content1,block_content2 FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$data['formulary']."' AND version='".$data['version']."' AND block_type='".$block_type."'");

							if(count($selBlock)>0){
								$block_content1	=	$selBlock[0]['block_content1'];
								$block_content2	=	$selBlock[0]['block_content2'];
								$newblock1		=	str_replace($data['snippetid'],$newsnippetag,stripslashes($block_content1));
								$newblock2		=	str_replace($data['snippetid'],$newsnippetag,stripslashes($block_content2));
								$this->_db->getConnection()->query("UPDATE 3bit_tmp_formulary_blocks SET block_content1 = '".addslashes($newblock1)."',block_content2='".addslashes($newblock2)."' WHERE formulary_id='".$data['formulary']."' AND version='".$data['version']."' AND block_type='".$block_type."'");
							}	
						}
						die("S");
					}
				}
			}		
		}
	}

	public function updatesnippetarea($data)
	{
		$name =str_replace("[[","",$data['snippetid']);
		$name =str_replace("]]","",$name);							
		$idplusversion	=	explode("#",str_replace("snippet-id","",$name));	
		$snippetid		=	$idplusversion[0];									
		$version		=	$idplusversion[1];

		$sql			=	"SELECT long_text,snippet_title,snippet_number FROM 3bit_formulary_snippets WHERE snippet_id='".$snippetid."' AND version = '".$version."'";	
		$snippets		=	$this->_db->fetchAll($sql);

		$numsnippets	=	count($snippets);					
		if($numsnippets<>0){
			foreach($snippets as $resR){
				$snippet_content	=	stripslashes($resR['long_text']);
				$snippet_content	.=	"^".stripslashes($resR['snippet_title']);
				$snippet_content	.=	"^".stripslashes($resR['snippet_number']);
			}
		}else{
			$sql			=	"SELECT long_text,snippet_title,snippet_number FROM 3bit_formulary_snippet_versions WHERE snippet_id='".$snippetid."' AND version= '".$version."' ";	
			$snippets		=	$this->_db->fetchAll($sql);																												
			$numsnippets	=	count($snippets);																											
			if($numsnippets<>0){
				foreach($snippets as $resR){
					$snippet_content	=	stripslashes($resR['long_text']);
					$snippet_content	.=	"^".stripslashes($resR['snippet_title']);
					$snippet_content	.=	"^".stripslashes($resR['snippet_number']);
				}
			}
		}
		die($snippet_content."^"."S");
	}

	public function addaudit($data)
	{
		//print_r($data);
		$farticle_id	=		1;
		$selArticle		=		$this->_db->fetchAll("SELECT MAX(farticle_id)as maxid FROM 3bit_formulary_articles WHERE 1");
		if(count($selArticle)>0){
			$farticle_id		=		$selArticle[0]['maxid']+1;
		}
		if(!empty($data['date_string']))
			$date_string	=	addslashes($data['date_string']);
		else{
			if($data['scope']=='New')
				$date_string		=		'Added '.date("M Y");
			elseif($data['scope']=='Update')
				$date_string		=		'Updated '.date("M Y");
			elseif($data['scope']=='Minor Update')
				$date_string		=		'Updated (minor change) '.date("M Y");
			elseif($data['scope']=='Correction')
				$date_string		=		'Updated (correction) '.date("M Y");
			elseif($data['scope']=='Technical'){
				$selString			=		$this->_db->fetchAll("SELECT date_string FROM 3bit_formulary_scope_audit WHERE formulary_id='".$data['formulary']."' ORDER BY created_date DESC LIMIT 0,1");
				if(count($selString)>0){
					$old_string		=		$selString[0]['date_string'];
				}if(empty($old_string)){
					$selString		=		$this->_db->fetchAll("SELECT DATE_FORMAT(modified_date,'%M %Y')as added_date,DATE_FORMAT(modified_date,'%Y-%m-%d')as cdate FROM 3bit_formulary_articles WHERE formulary_id='".$data['formulary']."' ORDER BY created_date DESC LIMIT 0,1");
					if(count($selString)>0){
							$added_date	=	stripslashes($selString[0]['added_date']);
							$cdate		=	stripslashes($selString[0]['cdate']);
							$edate		=	date("Y-m-d", mktime(0, 0, 0, 1, 1, 2012));
							if(strtotime($cdate)>strtotime($edate)){
								$old_string		=	"Updated ".$added_date;
							}else{
								$old_string		=	"Added ".$added_date;
							}					
					}						
				}
				$date_string		=	$old_string;
			}
		}
		$this->_db->query("INSERT INTO 3bit_formulary_scope_audit SET farticle_id	=	'".$farticle_id."',formulary_id='".$data['formulary']."',scope_type='".$data['scope']."',notes='".addslashes($data['notes'])."',date_string='".$date_string."',created_date=now(),modified_date=now() ");
		die("S");

	}

	public function oldaudit($data)
	{
		$old_string		=		'';
		$selString			=		$this->_db->fetchAll("SELECT date_string FROM 3bit_formulary_scope_audit WHERE formulary_id='".$data['formulary']."' ORDER BY created_date DESC LIMIT 0,1");
		if(count($selString)>0){
			$old_string		=		$selString[0]['date_string'];
		}if(empty($old_string)){
			$selString		=		$this->_db->fetchAll("SELECT DATE_FORMAT(modified_date,'%M %Y')as added_date,DATE_FORMAT(modified_date,'%Y-%m-%d')as cdate FROM 3bit_formulary_articles WHERE formulary_id='".$data['formulary']."' ORDER BY created_date DESC LIMIT 0,1");
			if(count($selString)>0){
					$added_date	=	stripslashes($selString[0]['added_date']);
					$cdate		=	stripslashes($selString[0]['cdate']);
					$edate		=	date("Y-m-d", mktime(0, 0, 0, 1, 1, 2012));
					if(strtotime($cdate)>strtotime($edate)){
						$old_string		=	"Updated ".$added_date;
					}else{
						$old_string		=	"Added ".$added_date;
					}					
			}		
		}	
		die($old_string);
	}

	public function updatestatus($data)
	{	
		
		if(!empty($data['formulary'])){
			$formulary_id	=	$data['formulary']; 
			$major_version	=	0;
			$selPub		=	    $this->_db->fetchAll("SELECT publish_status,formulary_title FROM 3bit_formulary WHERE formulary_id='".$formulary_id."'");
			if(count($selPub)>0){
				$pub_status	=	$selPub[0]['publish_status'];
				$form_title	=	$selPub[0]['formulary_title'];
				if($pub_status !=$data['status']){
					if($data['status']=='final'){								 
						 $major_version	=	$this->make_majorversion($formulary_id,$data['status']);			
					} 
					if(!empty($_SESSION['wlsession']['user_id']))
						$logged_user = $_SESSION['wlsession']['user_id'];
					$admin_name	=	$_SESSION['wlsession']['fullname'];

					$history	=	$admin_name.' updated formulary ID# '.$formulary_id.' '.$form_title.' publish status to '.$data['status'].' <span class="dashdate">'.date('Y/m/d H:i:s').'</span>';

					 $this->_db->query("INSERT INTO 3bit_audit_history SET
										module				=	'formulary',
										action				=	'update',
										record_id			=	'".$formulary_id."',
										action_status		=	'success',
										user_id				=	'".addslashes($logged_user)."',
										ip_address			=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
										browser				=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
										action_date			=	now(),
										audit_history		=	'".addslashes($history)."',
										status				=	'1',
										created_date		=	now(),
										modified_date		=	now()");


				}
			}
			if($data['status']!='final')		
				$this->_db->query("UPDATE 3bit_formulary SET publish_status = '".$data['status']."' WHERE formulary_id= '".$data['formulary']."' ");	

			die($major_version."^"."S");
			exit;
		}
	}

	public function majorversion($formulary_id)
	{
	   $sublib_val  =   $this->_db->fetchAll("SELECT max(version) as max_version from `3bit_tmp_formulary_blocks` where `formulary_id`='$formulary_id'");

	   $number		=   $sublib_val[0]['max_version'];		
	   $exp			=	explode(".",$number);
	   if($exp['0']>0){
		   $number	=	number_format(($exp['0']+1),'2','.','');
	   }else{
			$number	=	number_format(1,'2','.','');
	   }
	   return $number;
	}

	public function make_majorversion($formulary_id,$publish_status)
	{

	   $sublib_val	=	$this->_db->fetchAll("SELECT max(version) as max_version from `3bit_tmp_formulary_blocks` where `formulary_id`='".$formulary_id."'");

	   $version		=	$sublib_val[0]['max_version']; 

	   $selBlock	=	$this->_db->fetchAll("SELECT * FROM `3bit_tmp_formulary_blocks` where `formulary_id`='".$formulary_id."' AND version = '".$version."' ");

	   if(count($selBlock)>0){
		   $major_ver		=	$this->majorversion($formulary_id);
		   foreach($selBlock as $row){			
				$this->_db->getConnection()->query("INSERT INTO `3bit_tmp_formulary_blocks` SET 
										formulary_id		=	'".$formulary_id."',
										sess_id				=	'".trim(addslashes($_SESSION['wlsession']['asess_id']))."',
										user_id				=	'".trim($_SESSION['wlsession']["user_id"])."',
										block_type			=	'".$row['block_type']."',
										block_title			=	'".addslashes(stripslashes($row['block_title']))."',
										block_content1		=	'".addslashes(stripslashes($row['block_content1']))."',
										block_content2		=	'".addslashes(stripslashes($row['block_content2']))."',
										version				=	'".$major_ver."',
										status				=	'1',
										created_date		=	now(),
										modified_date		=	now()");
		   }
		   $this->make_approved($formulary_id,$major_ver,$publish_status);

		   $version	=	$major_ver;
	   }			   
	   return $version;
	}

	function make_approved($formulary_id,$major_ver,$publish_status){

	   global $block_title;

	   global $matchCount,	$uniquearr,$lastuniquearr;	   

	   $sublib_val	=	$this->_db->fetchAll("SELECT max(version) as max_version from `3bit_tmp_formulary_blocks` where `formulary_id`='".$formulary_id."'");

	   $version		=	$sublib_val[0]['max_version']; 

	   $block_title[1] =		array(
									 "pre_text_article"		=>	"pre_text_article",
									 "class"				=>	"Class:",
									 "indication"			=>	"Indication:",
									 "pharmacology"			=>	"Pharmacology",
									 "cautions"				=>	"Cautions",
									 "drug_interactions"	=>	"Drug Interactions",
									 "undesirable_effects"	=>	"Undesirable Effects",
									 "dose_and_use"			=>	"Dose And Use",
									 "overdose"				=>	"Overdose",
									 "death_rattle"			=>	"Death Rattle",
									 "supply"				=>	"Supply",
									 "post_text_article"	=>	"post_text_article",
									 "Bio Availability"		=>  "Bio Availability",
									 "Onset of Action"		=>  "Onset of Action",
									 "Time to peak plasma concentration"=>"Time to peak plasma concentration",
									 "Plasma Halflife"		=>  "Plasma Halflife",
									 "Duration of Action"	=>	"Duration of Action",
									 "Contra Indications"	=>  "Contra-indications:"

								);

	    $block_title[2] =		array(
									 "pre_text_article"		=>	"Vor Text lesen",
									 "class"				=>	"Stoffgruppe:",
									 "indication"			=>	"Indikationen:",
									 "pharmacology"			=>	"Pharmakologie",
									 "cautions"				=>	"Verwarnungen",
									 "drug_interactions"	=>	"Arzneimittelinteraktionen",
									 "undesirable_effects"	=>	"unerw�nschte Wirkungen",
									 "dose_and_use"			=>	"Dosierung und Anwendung",
									 "overdose"				=>	"�berdosierung",
									 "death_rattle"			=>	"R�cheln",
									 "supply"				=>	"beliefern",
									 "post_text_article"	=>	"nachgestellten Text lesen",
									 "Bio Availability"		=>  "Bio-Verf�gbarkeit",
									 "Onset of Action"		=>  "Wirkungseintritt",
									 "Time to peak plasma concentration"=>"Time to Peak-Plasmakonzentration",
									 "Plasma Halflife"		=>  "Plasma Halflife",
									 "Duration of Action"	=>	"Dauer der Aktion",
									 "Contra Indications"	=>  "Kontraindikationen:" );


		$block_array	=	array('pre_text_article','class','indication','pharmacology','cautions','drug_interactions','undesirable_effects','dose_and_use','overdose','death_rattle','supply','post_text_article');

		$temp			=	0;

		$returnTxt		=	'';

		 for($m=0;$m<count($block_array);$m++){

			$selBlocks	=	$this->_db->fetchAll("SELECT fb.*,f.formulary_title,f.bnf_reference,f.language_id,f.quick_practice_guide FROM 3bit_tmp_formulary_blocks fb,3bit_formulary f WHERE f.formulary_id ='".$formulary_id."' AND fb.version='".$major_ver."' AND f.formulary_id=fb.formulary_id AND fb.block_type= '".$block_array[$m]."' GROUP BY fb.version");	
			  if(count($selBlocks)>0){
				  $i=0;					  
				  foreach($selBlocks as $brow){
					  $block_id	=	$brow['block_id'];			  
						  if($temp ==0){
							if(!empty($brow['bnf_reference'])){
									$returnTxt.=	'<div id="monographtitle">
														<h1>'.html_entity_decode(stripslashes($brow['formulary_title'])).'</h1><h2>'.$brow['bnf_reference'].'</h2>
													</div>';			
							  }else{
									$qpdclass	=	'';
									if(!empty($brow['quick_practice_guide']))
										$qpdclass	=	' qpd';
									$returnTxt.=	'<div id="monographtitle" class="wide'.$qpdclass.'">
														<h1>'.html_entity_decode(stripslashes($brow['formulary_title'])).'</h1>
													</div>';	
							  }					 				  
						  }
						  $temp		=   $i+1;

						  $block_type	=	stripslashes($brow['block_type']);
					  
						  if($block_type=='generic_supply')
							  $block_type	=	'supply';			  

						  if($brow['block_content1'] != ''){

							  if($brow['block_type'] !='indication')
								  $divid	=	$this->remExtra($brow['block_type']);
							  else
								  $divid	=	'indications';

							$returnTxt	.=	'<div id="'.$divid.'">';

							if($block_type != 'post_text_article' && $block_type != 'pre_text_article'){
								if(!empty($brow['block_title']))
								$returnTxt	.=	'<H2>'.($brow['block_title']).'</H2>';							
							}   

							if($brow['block_content1'] != ''){
								$decoded	=	stripslashes($brow['block_content1']);
								$returnTxt	.=	$decoded;
							}
							if($block_type == 'indication'){
							   $contra_indications	= stripslashes($brow['block_content2']);	
							   if($contra_indications !=''){
									$returnTxt	.=	'<div class="greybox"><H2>'.$block_title[$brow['language_id']]['Contra Indications'].'</H2>';
									$returnTxt	.=	$contra_indications.'</div>';
								}

							}
							$returnTxt	.=	'</div>';					
						  }
					 $i++;
				  }					  
			  }		  
		  }
		  if($returnTxt ==''){
			  $returnTxt	=	'No sections are added!';	
		  }else{
			  $returnTxt	=	$this->getParsed($returnTxt,'1');
		  }

		  if(is_array($lastuniquearr)){	

				$lastuniquearr	=	array_values(array_unique($lastuniquearr)); 

				$pubid	=	implode(",",$lastuniquearr);

				$pubid	=	preg_replace(array('/\{/','/\}/'),array('',''),$pubid);
					
				$parr	=	explode(",",$pubid);

				$cnt	=	count($parr);

				if($cnt>0){			

					 $returnTxt	.= "<div id='references'><ol>";
				
					for($m=0;$m<$cnt;$m++){			
						$selLat	=	$this->_db->fetchAll("SELECT * FROM endnote_refs WHERE id='".$parr[$m]."' ORDER BY id ");

						if(count($selLat)>0){

							foreach($selLat as $row){

								$dispauth	=	'';
						
								$auth		=	array();
								
								$dispedit	=	'';

								$editors	=	array();

								$auth		=	explode("\r",$row['author']);						

								$authcount	=	count($auth);

								if($authcount>0){
									if($authcount>2){			
										$dispauth	=	$auth[0]." et al";
									}else if($authcount==2){
										$dispauth	=	implode(",",$auth);
										$dispauth	=	substr($dispauth,0,-1);
										$dispauth	=	$dispauth;
									}else{
									   $dispauth	=	$auth[0];
									}
								}else{
									$dispauth	=	$row['author'];
								}			

								if($editcount>0){
									if($editcount>2){
										$dispedit	= "In : ".$editors[0]." et al. (eds)&nbsp;";	
									}else if($editcount==2){
										$dispedit	= "In : ".$editors[0].",".$editors[1]." (eds) &nbsp;";
									}else{
										$dispedit	= "In : ".$editors[0]." (eds) &nbsp;";
									}
								}
							
								if($content !=''){
									$returnTxt	.= "<li><a name='".($m+1)."' ></a>".$content."</li>";
								}else{
									if($row['date']!='')
										$datetext	=	$row['year']."  ".$row['date'];
									else
										$datetext	=	$row['year'];
									
									if($row['pages']!=''){
										$returnTxt		.=	"<li id='".($m+1)."'><strong><a href='#up".($m+1)."'>^</a></strong> ".$dispauth."&nbsp;(".$datetext.")&nbsp;".stripslashes($row['title']);

										if($dispedit!=''){	
											$returnTxt		.=	"&nbsp;".$dispedit;
										}
										if($row['secondary_title']!=''){	
											$returnTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
										}
										if($row['publisher']!=''){
											 $returnTxt		.=	",&nbsp;".$row['publisher'];
										}
										if($row['place_published']!=''){

											$returnTxt		.= ",".$row['place_published'];
										}
										if($row['volume']!='' && $row['pages']!=''){
										   $returnTxt		.= "&nbsp;<b>".$row['volume']."</b>:&nbsp;".$row['pages'];
										}else if($row['volume']=='' && $row['pages']!=''){
										   $returnTxt		.= "&nbsp;pp.&nbsp;".$row['pages'];
										}

										$returnTxt		.= "</li>";

									}else{
										$returnTxt		.=	"<li id='".($m+1)."'><strong><a href='#up".($m+1)."'>^</a></strong> ".$dispauth."&nbsp;(".$datetext.")&nbsp;".stripslashes($row['title']);
										if($dispedit!=''){	
											$returnTxt		.=	"&nbsp;".$dispedit;
										}
										if($row['secondary_title']!=''){	
											$returnTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
										}
										if($row['edition']!=''){   
										   $returnTxt		.=	"&nbsp;(".$row['edition']."e).";
										}
										if($row['publisher']!=''){
											 $returnTxt		.=	"&nbsp;".$row['publisher'];
										}
										if($row['place_published']!=''){

											$returnTxt		.= ",".$row['place_published'];
										}
										$returnTxt		.= "</li>";
									}

								}

							}	
						}
					}
					 $returnTxt	.= "</ol></div>";
				}

			  }else{
				$selRefs	=	$this->_db->fetchAll("SELECT fb.*,f.formulary_title,f.bnf_reference,f.language_id FROM 3bit_tmp_formulary_blocks fb,3bit_formulary f WHERE f.formulary_id ='".$formulary_id."' AND fb.version='".$major_ver."' AND f.formulary_id=fb.formulary_id AND fb.block_type= 'references' GROUP BY fb.version");
				if(count($selRefs)>0){

				 $returnTxt	.= "<div id='references'>";

					foreach($selRefs as $arefs){

						if($arefs['block_content1'] != ''){

								$decoded	=	(stripslashes($arefs['block_content1']));

								$returnTxt	.=	$decoded;

						}
					}
				 $returnTxt	.= "</div>";			 
				}
		     }		  			 

			 $this->_db->getConnection()->query("INSERT INTO 3bit_formulary_articles  SET
																formulary_id	=	'".$formulary_id."',
																language_id		=	(SELECT language_id FROM 3bit_formulary WHERE formulary_id='".$formulary_id."'),
																content			=	'".addslashes($returnTxt)."',
																version			=	'".$major_ver."',
																publish_status	=	'".$publish_status."',
																status			=	'1',
																created_date	=	now(),
																modified_date	=	now()");
																
			$farticleid		=	$this->_db->lastInsertId();
				

			$this->_db->query("UPDATE 3bit_formulary SET publish_status = 'draft',farticle_id='".$farticleid."',major_version='".$major_ver."',minor_version='0.00' WHERE formulary_id = '".$formulary_id."' ");	

			$this->makeminor_archive($formulary_id,$major_ver);			
			
			return;	
	}	
	function makeminor_archive($formulary_id,$major_ver){

		$selMinor	=	$this->_db->fetchAll("SELECT fb.*,f.formulary_title,f.bnf_reference,f.language_id FROM 3bit_tmp_formulary_blocks fb,3bit_formulary f WHERE f.formulary_id ='".$formulary_id."' AND fb.version < '".$major_ver."' AND f.formulary_id=fb.formulary_id ");

		if(count($selMinor)>0){

			foreach($selMinor as $mins){
				
				$this->_db->getConnection()->query("INSERT INTO 3bit_tmp_formulary_blocks_archive  SET
																parent_block_id	=	'".$mins['block_id']."',
																formulary_id	=	'".$mins['formulary_id']."',
																sess_id			=	'".$mins['sess_id']."',
																user_id			=	'".$mins['user_id']."',
																block_type		=	'".$mins['block_type']."',
																block_title		=	'".$mins['block_title']."',
																block_content1	=	'".$mins['block_content1']."',
																block_content2	=	'".$mins['block_content2']."',
																bio_availability=	'".$mins['bio_availability']."',
																onset_of_action	=	'".$mins['onset_of_action']."',
																time_to_peak	=	'".$mins['time_to_peak']."',
																plasma_halflife	=	'".$mins['plasma_halflife']."',
																duration_of_action=	'".$mins['duration_of_action']."',
																version			=	'".$mins['version']."',
																status			=	'".$mins['status']."',
																created_date	=	'".$mins['created_date']."',
																modified_date	=	'".$mins['modified_date']."',
																archived_date	=	now()");
				

			}

		}	

		$this->_db->query("DELETE FROM 3bit_tmp_formulary_blocks WHERE formulary_id ='".$formulary_id."' AND version< '".$major_ver."'");
	}


	function findFormversion($formulary_id){	
	
		$new_version	=	0;
		
		$sublib_qry		=	$this->_db->fetchRow("SELECT MAX(version) as max_version,COUNT(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$formulary_id."'");

		if(count($sublib_qry)>0){
			$count		=	$sublib_qry['cnt'];
			if($count>0 && $count!=NULL){
				$new_version =	$sublib_qry['max_version'];
			}
		}
		return $new_version;

	}

	function formularyNav($parentid,$type,$language_id,$language_code){
		$navCnt = 1;
		$navCntApp = 1;
		$selMonograph		=	$this->_db->fetchAll("SELECT f.* FROM 3bit_formulary f WHERE f.formulary_parent_id='".$parentid."' AND f.formulary_type = '".$type."' AND f.status = '1' AND f.language_id='".$language_id."' ORDER BY f.formulary_order ASC");
		$nummono			=	count($selMonograph);

		if($nummono>0){
			
			$returnNav		.=	'';

			foreach($selMonograph as $mrow){
				
				$ftitle		=	stripslashes($mrow['formulary_title']);
				$fversion	=	$this->findFormversion($mrow['formulary_id']);

				if(trim($mrow['alias'])!='')
					$ftitle	=	stripslashes($mrow['alias']);

				$furl		=	stripslashes($mrow['formulary_title']);

				if($fversion>0)
					$furl		=	'<a href="/formulary/'.$language_code.'/'.$this->remSpecial($ftitle).'.html">'.stripslashes(str_replace("Quick Practice Guide: ","",$mrow['formulary_title'])).'</a>';
				
				$selSec			=	$this->_db->fetchAll("SELECT f.* FROM 3bit_formulary f WHERE f.formulary_parent_id='".$mrow['formulary_id']."' AND f.formulary_type = '".$type."'  AND f.status = '1' AND f.language_id='".$language_id."' GROUP BY f.formulary_id ORDER BY f.formulary_order ASC");

				$numsec			=	count($selSec);

				if($numsec>0 || $fversion){
					if ($type=="Appendicies"){
						$returnNav	.=	'<li>A'.$navCntApp.'&nbsp;&nbsp;'.$furl;
						$navCntApp++;
					}else{
						$returnNav	.=	'<li>'.$navCnt.'&nbsp;&nbsp;'.$furl;
						$navCnt++;
					}
					

				if($numsec>0){

					$returnNav		.=	'<ul>';
					foreach($selSec as $srow)
					{	
						$stitle		=	stripslashes($srow['formulary_title']);
						$sversion	=	$this->findFormversion($srow['formulary_id']);

						if(trim($srow['alias'])!='')
							$stitle		=	stripslashes($srow['alias']);

						$surl		=	stripslashes($srow['formulary_title']);

						if($sversion>0)
							$surl		=	'<a href="/formulary/'.$language_code.'/'.$this->remSpecial($stitle).'.html">'.stripslashes(str_replace("Quick Practice Guide: ","",$srow['formulary_title'])).'</a>';	

							$returnNav	.=	'<li>'.$surl;

						$selThird			=	$this->_db->fetchAll("SELECT f.*,a.formulary_id FROM 3bit_formulary f,3bit_formulary_articles a WHERE f.formulary_id=a.formulary_id AND f.formulary_parent_id='".$srow['formulary_id']."' AND f.formulary_type = '".$type."'  AND f.status = '1' AND f.language_id='".$language_id."' GROUP BY f.formulary_id ORDER BY f.formulary_order ASC");

						$numthi				=	count($selThird);

						if($numthi>0){
						  $returnNav		.=	'<ul>';
						  foreach($selThird as $trow){				
								$ttitle		=	stripslashes($trow['formulary_title']);
								$tversion	=	$this->findFormversion($trow['formulary_id']);
								if(trim($trow['alias'])!='')
									$ttitle		=	stripslashes($trow['alias']);
								$turl		=	stripslashes($trow['formulary_title']);
								if($tversion>0)
								$turl		=	'<a href="/formulary/'.$language_code.'/'.$this->remSpecial($ttitle).'.html">'.stripslashes(str_replace("Quick Practice Guide: ","",$trow['formulary_title'])).'</a>';
								$returnNav	.=	'<li>'.$turl.'</li>'."\n";
						  }
						  $returnNav		.=	'</ul>';						
						}
						 $returnNav	.=	'</li>'."\n";
				   }
				   $returnNav		.=	'</ul>';
				   }

				   $returnNav	.=	'</li>'."\n";
				}			
			}			
			$returnNav	.=	'';
		} 
		return $returnNav;
	}

	function prelimsNav($parentid,$type,$language_id,$language_code){	
	    
		$selMonograph		=	$this->_db->fetchAll("SELECT f.* FROM 3bit_formulary f WHERE f.formulary_parent_id='".$parentid."' AND f.formulary_type = '".$type."' AND f.status = '1' AND f.language_id='".$language_id."' ORDER BY f.formulary_order ASC");
		$nummono			=	count($selMonograph);
		if($nummono>0){
			$returnNav		.=	'<div class="TOC">';
			foreach($selMonograph as $mrow){				
				$ftitle		=	stripslashes($mrow['formulary_title']);
				$fversion	=	$this->findFormversion($mrow['formulary_id']);
				if(trim($mrow['alias'])!='')
				   $ftitle		=	stripslashes($mrow['alias']);
				$furl		=	stripslashes($mrow['formulary_title']);
				if($fversion>0)
					$furl	=	'<a href="/formulary/'.$language_code.'/'.$this->remSpecial($ftitle).'.html">'.stripslashes($mrow['formulary_title']).'</a><br />';		
				if(!empty($fversion))
					$returnNav	.=	$furl;

				$selSec			=	$this->_db->fetchAll("SELECT f.* FROM 3bit_formulary f WHERE 1 AND f.formulary_parent_id='".$mrow['formulary_id']."' AND f.formulary_type = '".$type."'  AND f.status = '1' AND f.language_id='".$language_id."' GROUP BY f.formulary_id ORDER BY f.formulary_order ASC");
				$numsec			=	count($selSec);
				if($numsec>0){			   
				   foreach($selSec as $srow){				
						$stitle		=	stripslashes($srow['formulary_title']);
						$sversion	=	$this->findFormversion($srow['formulary_id']);
						if(trim($srow['alias'])!='')
							$stitle		=	stripslashes($srow['alias']);
						$surl		=	stripslashes($srow['formulary_title']);
						if($sversion>0)
							$surl		=	'<a href="/formulary/'.$language_code.'/'.$this->remSpecial($stitle).'.html">'.stripslashes($srow['formulary_title']).'</a><br />';	
						$returnNav	.=	$surl;
						$selThird			=	$this->_db->fetchAll("SELECT f.* FROM 3bit_formulary f WHERE 1 AND f.formulary_parent_id='".$srow['formulary_id']."' AND f.formulary_type = '".$type."'  AND f.status = '1' AND f.language_id='".$language_id."' GROUP BY f.formulary_id ORDER BY f.formulary_order ASC");
						$numthi				=	count($selThird);
						if($numthi>0){					  
						  foreach($selThird as $trow){				
								$ttitle		=	stripslashes($trow['formulary_title']);
								$tversion	=	$this->findFormversion($trow['formulary_id']);
								if(trim($trow['alias'])!='')
									$ttitle		=	stripslashes($trow['alias']);
								$turl		=	stripslashes($trow['formulary_title']);
								if($tversion>0)
								$turl		=	'<a href="/formulary/'.$language_code.'/'.$this->remSpecial($ttitle).'.html">'.stripslashes($trow['formulary_title']).'</a><br />';
								$returnNav	.=	$turl."\n";
						  }					  					
						}
						$returnNav	.=	"\n";
				   }
				}
				$returnNav	.=	"\n";
			}			
			$returnNav	.=	'</div>';
		} 
		return $returnNav;
	}

	function qpdNav($language_id,$language_code){	    
		$selMonograph		=	$this->_db->fetchAll("SELECT f.* FROM 3bit_formulary f WHERE f.status = '1' AND f.language_id='".$language_id."' AND f.quick_practice_guide='1' ORDER BY f.qpd_order ASC");
		// ORDER BY f.formulary_order ASC
		$nummono			=	count($selMonograph);
		if($nummono>0){
			$returnNav		.=	'';
			foreach($selMonograph as $mrow){				
				$ftitle		=	stripslashes($mrow['formulary_title']);
				$fversion	=	$this->findFormversion($mrow['formulary_id']);
				if(trim($mrow['alias'])!='')
				   $ftitle	=	stripslashes($mrow['alias']);
				$furl		=	stripslashes($mrow['formulary_title']);
				if($fversion>0)
					$furl		=	'<a href="/formulary/'.$language_code.'/'.$this->remSpecial($ftitle).'.html">'.stripslashes($mrow['formulary_title']).'</a>';		

				$returnNav	.=	'<li>'.$furl.'</li>'."\n";
			}					
		} 
		return $returnNav;
	}

	public function getaudits($id)
	{

			$selAudits	=		$this->_db->fetchAll("SELECT a.*,DATE_FORMAT(a.created_date,'%M %D %Y')as cdate,f.formulary_title,af.version FROM 3bit_formulary_scope_audit a,3bit_formulary f,3bit_formulary_articles af WHERE a.formulary_id='".$id."' AND a.formulary_id=f.formulary_id AND a.farticle_id=af.farticle_id ");

			return $selAudits;

	}

}

?>