<?php

class Application_Model_Groups
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
      $sql = "SELECT a.id, a.email, a.phone, a.FirstName, a.LastName, DATE_FORMAT( sg.start_date, '%d-%m-%Y' ) AS g_start_date, sg.organisation, sg.group_id, sg.order_id, sg.no_of_subscription
FROM new_users a, 3bit_subscribers_groups sg
WHERE sg.group_id = '".$id."'
AND sg.user_id = a.id";

        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('new_users','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		//echo "<pre>";
		//print_r($_POST);
		//exit;

		$arrStartDate			= explode('-',$data['g_start_date']);
		$startDate				= $arrStartDate[2].'-'.$arrStartDate[1].'-'.$arrStartDate[0];

		$subscribers			= "";

		for($i=0;$i<$data['no_of_subscription'];$i++){
			if($data['txtEmail_'.$i] != ''){
				$subscribers		.= $data['txtEmail_'.$i].", ";
			}
		}

		$subscribers = substr_replace( $subscribers, "", -2 );

		// Insert in to groups
		 $sql	=	"INSERT INTO `3bit_subscribers_groups` SET	
							organisation		=	'".addslashes($data['org_name'])."',
							start_date			=	'".$startDate."',
							user_id				=	'".$data['userid']."',
							order_id			=	'".$data['orderid']."',
							no_of_subscription	=	'".$data['no_of_subscription']."',
							subscribers			=	'".addslashes($subscribers)."',
							status				=	'1',
							date_created		=	'".date('Y-m-d')."'" ; 

		$query		=	$this->_db->query($sql);

		$groupid	=	$this->_db->lastInsertId();

		for($i=0;$i<$data['no_of_subscription'];$i++){

			$arrSubStartDate			= explode('-',$data['start_date_'.$i]);
			$subStartDate				= $arrSubStartDate[2].'-'.$arrSubStartDate[1].'-'.$arrSubStartDate[0];

			if($data['txtEmail_'.$i] != ''){
				
				// Insert in to subscribers
				if($data['hidSubscibtionId_'.$i]!=''){
					$sql	=	"UPDATE `3bit_subscribers` SET	
							start_date		=	'".$subStartDate."' ,
							end_date		=	DATE_ADD('".$subStartDate."', INTERVAL '".$data['duration']."' DAY) ,
							duration		=	'".$data['duration']."',
							group_id		=	'".$groupid."' ,
							user_id			=	'".$data['hidUserId_'.$i]."' 
							WHERE subscriber_id =	'".$data['hidSubscibtionId_'.$i]."'" ;
				}else{
					$sql	=	"INSERT INTO `3bit_subscribers` SET	
							user_id			=	'".$data['hidUserId_'.$i]."',
							start_date		=	'".$subStartDate."',
							end_date		=	DATE_ADD('".$subStartDate."', INTERVAL '".$data['duration']."' DAY) ,
							duration		=	'".$data['duration']."',
							group_id		=	'".$groupid."',
							subscribtion_user =	'".$data['userid']."',
							order_id		=	'".$data['orderid']."',
							status			=	'1',
							created_date	=	'".date('Y-m-d')."'" ;
				}
				$query		=	$this->_db->query($sql);
			}
		}
		$audit				=	new Application_Model_Audits();
		$audit->insert_audit('new_users','add',$memid,'success');
	}

	public function update($data)
	{
		//echo "<pre>";
		//print_r($_POST);
		//exit;

		$arrStartDate			= explode('-',$data['g_start_date']);
		$startDate				= $arrStartDate[2].'-'.$arrStartDate[1].'-'.$arrStartDate[0];

		$subscribers			= "";

		for($i=0;$i<$data['no_of_subscription'];$i++){
			if($data['txtEmail_'.$i] != ''){
				$subscribers		.= $data['txtEmail_'.$i].", ";
			}
		}

		$subscribers = substr_replace( $subscribers, "", -2 );

		// ----- update in to `3bit_subscribers_groups` ----------------------------------

		$sql	=	"UPDATE `3bit_subscribers_groups` SET	
							organisation		=	'".addslashes($data['org_name'])."',
							start_date			=	'".$startDate."',
							user_id				=	'".$data['userid']."',
							order_id			=	'".$data['orderid']."',
							no_of_subscription	=	'".$data['no_of_subscription']."',
							subscribers			=	'".addslashes($subscribers)."' 
							WHERE group_id = '".$data['group_id']."'" ; 

		$query		=	$this->_db->query($sql);

		for($i=0;$i<$data['no_of_subscription'];$i++){

			$arrSubStartDate			= explode('-',$data['start_date_'.$i]);
			$subStartDate				= $arrSubStartDate[2].'-'.$arrSubStartDate[1].'-'.$arrSubStartDate[0];

			if($data['hidSubscibtionId_'.$i] == '' && $data['txtEmail_'.$i] != '' ){

				//----- Insert in `3bit_subscribers`------------- --------------

				$sql	=	"INSERT INTO `3bit_subscribers` SET	
							user_id			=	'".$data['hidUserId_'.$i]."',
							start_date		=	'".$subStartDate."',
							end_date		=	DATE_ADD('".$subStartDate."', INTERVAL '".$data['duration']."' DAY) ,
							duration		=	'".$data['duration']."',
							group_id		=	'".$data['group_id']."',
							subscribtion_user =	'".$data['userid']."',
							order_id		=	'".$data['orderid']."',
							status			=	'1',
							created_date	=	'".date('Y-m-d')."'" ;

				$query	=	$this->_db->query($sql);
			}else{
				if($data['txtEmail_'.$i] != ''){
				
				//----- Update in `3bit_subscribers`------------- --------------

				$sql	=	"UPDATE `3bit_subscribers` SET	
							start_date		=	'".$subStartDate."' ,
							end_date		=	DATE_ADD('".$subStartDate."', INTERVAL '".$data['duration']."' DAY) ,
							duration		=	'".$data['duration']."',
							user_id =	'".$data['hidUserId_'.$i]."'
							WHERE subscriber_id =	'".$data['hidSubscibtionId_'.$i]."'" ;

				$query	=	$this->_db->query($sql);
				}
			}
		}

		//exit;
	}

	public function delete($id){

		$this->_db->delete("new_users",'id='.$id); 
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('new_users','delete',$id,'success');
	}

	public function view($id)
    {
        $sql			= "SELECT a.id, a.title, a.FirstName, a.LastName , a.email ,DATE_FORMAT(s.start_date, '%D-%M-%Y') as start_date, DATE_FORMAT(s.end_date, '%D-%M-%Y') as end_date, s.duration FROM new_users a, 3bit_subscribers s WHERE a.id = s.user_id AND id='".$id."' ORDER BY s.created_date DESC ";
        if ($result		= $this->_db->query($sql)) {
			$audit		=	new Application_Model_Audits();
			//$audit->insert_audit('new_users','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function subscriberorderdetails($orderid)
	{
		$sQuery			= $this->_db->fetchRow("SELECT u.id, u.FirstName, u.LastName, u.email, u.phone, od.quantity, DATE_FORMAT(s.start_date, '%d-%m-%Y') as start_date, s.end_date, s.duration, s.order_id , s.subscriber_id  
		FROM new_users u, 3bit_order_details od, 3bit_subscribers s
		WHERE s.order_id = '".$orderid."'
		AND s.order_id = od.order_id
		AND s.user_id = u.id  AND od.product_id  IN (8,9,10,11,12,13,14,15,18) GROUP by s.order_id");

		return $sQuery;

	}

	public function get_all_groups()
    {
		$sql = "SELECT group_id FROM 3bit_subscribers_groups WHERE 1";
        if ($result = $this->_db->query($sql)) {
            $res	=  $result->fetchAll();

			$arrSubscribers	= array();

			foreach($res AS $key => $val){
				$arrSubscribers[]	= $val['group_id'];

			}

			return $arrSubscribers;
        }         
    }

	public function getgroupsusersdetails($group_id)
	{

		$result		= $this->_db->query("SELECT u.id 	, sg.group_id 	, u.email 	, u.FirstName 	, u.LastName, DATE_FORMAT(s.start_date, '%d-%m-%Y') as start_date, s.subscriber_id, s.subscribtion_user FROM new_users u, 3bit_subscribers_groups sg, 3bit_subscribers s WHERE sg.group_id = '".$group_id."' AND sg.group_id = s.group_id AND s.user_id = u.id ORDER BY s.subscriber_id ASC");
		
		//echo "SELECT u.id 	, sg.group_id 	, u.email 	, u.FirstName 	, u.LastName, DATE_FORMAT(s.start_date, '%d-%m-%Y') as start_date, s.subscriber_id, s.subscribtion_user FROM new_users u, 3bit_subscribers_groups sg, 3bit_subscribers s WHERE sg.group_id = '".$group_id."' AND sg.group_id = s.group_id AND s.user_id = u.id ORDER BY s.subscriber_id ASC";

		$res	=  $result->fetchAll();

		return $res;
	}




}
?>