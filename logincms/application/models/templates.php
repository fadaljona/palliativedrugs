<?php

class Application_Model_Templates
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }   
	
	 public function getUsers()
    {
        $sql = "SELECT user_id,concat(user_firstname,' ',user_lastname)as uname FROM 3bit_users ORDER BY user_firstname";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }
   
	public function read($id)
    {
        $sql = "SELECT at.*,m.* FROM 3bit_article_template at LEFT JOIN 3bit_messages m ON at.message_id=m.message_id WHERE temp_id='".$id."' ";
        if ($result =	$this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_article_template','view',$id,'success');
            $data	=	$result->fetchAll();
			$image1				=	'';
			$temp_thumbs		= 	$data[0]['temp_thumbs'];
			if($temp_thumbs<>"" && $temp_thumbs <>'0'){
				$imgsql			=	$this->_db->fetchRow("SELECT asset_name FROM 3bit_assets WHERE asset_id='".$temp_thumbs."'");						
				if($imgsql)
					$image1		=	$imgsql['asset_name'];
			}
			$data[0]['temp_thumbs_img']		=	$image1;
			return $data;
        }
    }

	public function add($data)
    {	
		if(!empty($data['custom_template']))
			$custom	=	1;
		else
			$custom	=	0;

		if(!empty($data['template_thumbsid']))
			$temp_thumbs	=	$data['template_thumbsid'];
		else
			$temp_thumbs	=	0;

		$message_id	=	0;		

		// Insert into messages table for message template 

		if($data['temp_type']=='message'){

			if(!empty($data['message_to']))
				$to		=	'Customer';		
			else
				$to		=	'Admin';

			if(!empty($data['email_status']))
				$estatus=	1;		
			else
				$estatus=	0;

			for($i=0;$i<count($data["user_id"]);$i++){
				$userid			.= $data["user_id"][$i].",";
			}

			$sql	=	"INSERT INTO 3bit_messages SET
									`message_type` 		=	'".$data['message_type']."',
									`message_to`		=	'".$to."',
									`user_id`			=	'".$userid."',
									`mail_subject`		=	'".addslashes($data['mail_subject'])."',
									`message_text` 		=	'".addslashes($data['message_text'])."',
									`text`				=	'".addslashes($data['text'])."',
									`email_status` 		=	'".$estatus."',
									`active`		 	=	'".$data['active']."',
									`created_date`		=	now(),
									`modified_date` 	=	now()";

			$query		=	$this->_db->getConnection()->query($sql);

			$message_id	=	$this->_db->lastInsertId();	

		} // Ends here

		$sql	=	"INSERT INTO 3bit_article_template SET
								temp_filename		=	'".addslashes($data['temp_filename'])."',
								temp_content		=	'".addslashes($data['temp_content'] )."',
								temp_type			=	'".addslashes($data['temp_type'] )."',
								custom_template		=	'".$custom."',
								temp_thumbs			=	'".$temp_thumbs."',
								message_id			=	'".$message_id."',
								temp_status			=	'".$data['temp_status']."',
								created_date		=	now(),
								modified_date		=	now()";

		$query	=	$this->_db->getConnection()->query($sql);

		$tempid	=	$this->_db->lastInsertId();	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_article_template','add',$tempid,'success');
    }

	public function update($data)
	{
		if(!empty($data['custom_template']))
			$custom	=	1;
		else
			$custom	=	0;

		// Insert into messages table for message template 

		if($data['temp_type']=='message'){

			if(!empty($data['message_to']))
				$to		=	'Customer';		
			else
				$to		=	'Admin';

			if(!empty($data['email_status']))
				$estatus=	1;		
			else
				$estatus=	0;

			for($i=0;$i<count($data["user_id"]);$i++){
				$userid			.= $data["user_id"][$i].",";
			}

			if(!empty($data['message_id'])){

				$sql	=	"UPDATE 3bit_messages SET
								`message_type` 		=	'".$data['message_type']."',
								`message_to`		=	'".$to."',
								`user_id`			=	'".$userid."',
								`mail_subject`		=	'".addslashes($data['mail_subject'])."',
								`message_text` 		=	'".addslashes($data['message_text'])."',
								`text`				=	'".addslashes($data['text'])."',
								`email_status` 		=	'".$estatus."',
								`active`		 	=	'".$data['active']."',			
								`modified_date`		=	now() WHERE message_id='".$data['message_id']."'";

				$query	=	$this->_db->getConnection()->query($sql);

				$message_id	=	$data['message_id'];

			}else{

				$sql	=	"INSERT INTO 3bit_messages SET
										`message_type` 		=	'".$data['message_type']."',
										`message_to`		=	'".$to."',
										`user_id`			=	'".$userid."',
										`mail_subject`		=	'".addslashes($data['mail_subject'])."',
										`message_text` 		=	'".addslashes($data['message_text'])."',
										`text`				=	'".addslashes($data['text'])."',
										`email_status` 		=	'".$estatus."',
										`active`		 	=	'".$data['active']."',
										`created_date`		=	now(),
										`modified_date` 	=	now()";

				$query		=	$this->_db->getConnection()->query($sql);

				$message_id	=	$this->_db->lastInsertId();	
			}

		} // Ends here


		$sql	=	"UPDATE 3bit_article_template SET
								temp_filename		=	'".addslashes($data['temp_filename'])."',
								temp_content		=	'".addslashes($data['temp_content'] )."',
								temp_type			=	'".addslashes($data['temp_type'] )."',
								custom_template		=	'".$custom."',
								temp_thumbs			=	'".$data['template_thumbsid']."',
								message_id			=	'".$message_id."',
								temp_status			=	'".$data['temp_status']."',								
								modified_date		=	now() WHERE temp_id='".$data['id']."'";
		$query	=	$this->_db->getConnection()->query($sql);

		$tempid	=	$data['id'];

		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_article_template','update',$tempid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_article_template",'temp_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_article_template','delete',$id,'success');
	}

	public function getFields(){

		$returnfields	=	'';

		$sql = $this->_db->fetchAll("SELECT * FROM 3bit_fieldsets WHERE 1 AND status='1' ");

		foreach($sql as $res){

			$fsql	=	$this->_db->fetchAll("SELECT * FROM 3bit_fields WHERE 1 AND fieldset_id='".$res['fieldset_id']."' AND fieldstatus='1' ");

			if(count($fsql)>0){

				$returnfields	.=	'<div>
										<h3><a href="#">'.stripslashes($res['title']).'</a></h3>
										<ul>';
				foreach($fsql as $fres){
					$returnfields	.=	'<li><a href="javascript:void(0);" onclick=\'wraptext(document.addtemp.temp_content,"'.stripslashes($fres['fieldvar']).'","");\'>'.stripslashes($fres['fieldvar']).'</a></li>';
				}

				$returnfields	.=		'</ul>
									</div>';
			}
		}		
		return $returnfields;
	}
}

?>