<?php

class Application_Model_Couriers
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }       

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_courier WHERE courier_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_courier','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$sql	=	"INSERT INTO 3bit_courier SET
								courier_name			=	'".addslashes($data['courier_name'])."',
								courier_url				=	'".addslashes($data['courier_url'])."',
								courier_status			=	'".$data['courier_status']."',
								created_date			=	now(),
								modified_date			=	now()";

		$query		=	$this->_db->query($sql);

		$courierid	=	$this->_db->lastInsertId();	
		
		$audit		=	new Application_Model_Audits();

		$audit->insert_audit('3bit_courier','add',$courierid,'success');
    }

	public function update($data)
	{
		$sql	=	"UPDATE 3bit_courier SET
								courier_name			=	'".addslashes($data['courier_name'])."',
								courier_url				=	'".addslashes($data['courier_url'])."',
								courier_status			=	'".$data['courier_status']."',
								modified_date			=	now() WHERE courier_id='".$data['id']."'";
		$query		=	$this->_db->query($sql);

		$courierid	=	$data['id'];

		$audit		=	new Application_Model_Audits();

		$audit->insert_audit('3bit_courier','update',$courierid,'success');
    }

	public function delete($id){

		$this->_db->delete("3bit_courier",'courier_id='.$id); 	
		
		$audit	=	new Application_Model_Audits();

		$audit->insert_audit('3bit_courier','delete',$id,'success');
	}
}

?>