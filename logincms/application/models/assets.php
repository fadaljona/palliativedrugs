<?php

class Application_Model_Assets
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    }    

    public function getAssettypes()
    {
        $sql = "SELECT asset_category_id,asset_category_title FROM 3bit_asset_categories ORDER BY asset_category_title";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function getAssettype($id)
    {
        $sql = "SELECT asset_category_type FROM 3bit_asset_categories where asset_category_id='".$id."'";
        if ($result = $this->_db->fetchOne($sql)) {
            return $result;
        }
    }

	public function read($id)
    {
        $sql = "SELECT * FROM 3bit_assets WHERE asset_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$audit	=	new Application_Model_Audits();
			$audit->insert_audit('3bit_assets','view',$id,'success');
            return $result->fetchAll();
        }
    }

	public function add($data)
    {	
		$suggests	=	'';
		$keys		=	array_keys($data);
		foreach($keys as $key ){
			if(strstr($key,'as_values'))
				$suggests =$data[$key];
			
		}
		$tags					=	$data['keywords'];
		$tottags				=	$suggests.$tags;
		
		$upName					=	time()."_".strtolower(str_replace(" ","-",$_FILES['asset_name']['name']));
		$assettype				=	$this->getAssettype($data['cat_id']);
		$mainDestFile			=	"../../assets/".$assettype."/".$data['cat_id']."/".$upName;
		$thumbDestFile			=	"../../assets/".$assettype."/".$data['cat_id']."/".$upName;
		$name					=	$_FILES['asset_name']['name'];
		$type_ext				=	substr(strrchr($name, "."), 1);
		$type					=	$_FILES['asset_name']['type'];		

		$uploadDone				=	move_uploaded_file($_FILES['asset_name']['tmp_name'], $mainDestFile);	
		$imgsize				= 	filesize($mainDestFile);
		$assetize				=	getimagesize($mainDestFile);
		if($uploadDone){
			if($type_ext == 'pdf' || $type_ext == 'doc' || $type_ext =='octet-stream'){
				  $query	=	$this->_db->query("INSERT INTO 3bit_assets SET
										asset_title			=	'".addslashes($data['asset_title'])."',
										asset_name			=	'".addslashes($upName)."',
										asset_thumb			=	'".addslashes($upName)."',
										asset_category_id	=	'".addslashes($data['cat_id'])."',
										asset_alt			=	'".addslashes($data['asset_alt'])."',
										asset_type			=	'".addslashes($type_ext)."',										
										asset_size			=	'".$imgsize."',
										keywords			=	'".$tottags."',
										status				=	'".$data['status']."',
										created_date		=	now(),
										modified_date		=	now()");

			}else  if ($type_ext == 'jpeg' || $type_ext == 'gif' || $type_ext == 'png' || $type_ext == 'jpg' || $type_ext == 'JPG'  ){

				exec("/usr/bin/convert -geometry \"100"."x"."$assetize[1]\" -quality \"90\" \"$mainDestFile\" \"$thumbDestFile\"");

				$query	=	$this->_db->query("INSERT INTO 3bit_assets SET
										asset_title			=	'".addslashes($data['asset_title'])."',
										asset_name			=	'".addslashes($upName)."',
										asset_thumb			=	'".addslashes($upName)."',
										asset_category_id	=	'".addslashes($data['cat_id'])."',
										asset_alt			=	'".addslashes($data['asset_alt'])."',
										asset_type			=	'".addslashes($type_ext)."',
										asset_height		=	'".addslashes($assetize[1])."',
										asset_width			=	'".addslashes($assetize[0])."',
										asset_size			=	'".$imgsize."',
										keywords			=	'".$tottags."',
										status				=	'".$data['status']."',
										created_date		=	now(),
										modified_date		=	now()");
			}
		}
		$assetid	=	$this->_db->lastInsertId();	
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_assets','add',$assetid,'success');
    }

	public function update($data)
	{
		$suggests = '';
		$keys = array_keys($data);
		foreach($keys as $key ){
			if(strstr($key,'as_values'))
				$suggests =$data[$key];
			
		}
		$tags	=	$data['keywords'];
		$tottags=	$suggests.$tags;

		if(!empty($_FILES['asset_name']['tmp_name'])){
			$upName					=	time()."_".strtolower(str_replace(" ","-",$_FILES['asset_name']['name']));
			$assettype				=	$this->getAssettype($data['cat_id']);
			$mainDestFile			=	"../../assets/".$assettype."/".$data['cat_id']."/".$upName;
			$thumbDestFile			=	"../../assets/".$assettype."/".$data['cat_id']."/".$upName;
			$name					=	$_FILES['asset_name']['name'];
			$type_ext				=	substr(strrchr($name, "."), 1);
			$type					=	$_FILES['asset_name']['type'];		

			$uploadDone				=	move_uploaded_file($_FILES['asset_name']['tmp_name'], $mainDestFile);
			
			$imgsize				= 	filesize($mainDestFile);

			$assetize				=	getimagesize($mainDestFile);
			if($uploadDone){
				if($type_ext == 'pdf' || $type_ext == 'doc' || $type_ext =='octet-stream'){
					  $this->_db->query("UPDATE 3bit_assets SET
											asset_title			=	'".addslashes($data['asset_title'])."',
											asset_name			=	'".addslashes($upName)."',
											asset_thumb			=	'".addslashes($upName)."',
											asset_category_id	=	'".addslashes($data['cat_id'])."',
											asset_alt			=	'".addslashes($data['asset_alt'])."',
											asset_type			=	'".addslashes($type_ext)."',										
											asset_size			=	'".$imgsize."',
											keywords			=	'".$tottags."',
											status				=	'".$data['status']."',								
											modified_date		=	now() WHERE asset_id='".$data['id']."'");

				}else  if ($type_ext == 'jpeg' || $type_ext == 'gif' || $type_ext == 'png' || $type_ext == 'jpg' || $type_ext == 'JPG'  ){

					exec("/usr/bin/convert -geometry \"100"."x"."$assetize[1]\" -quality \"90\" \"$mainDestFile\" \"$thumbDestFile\"");

					$this->_db->query("UPDATE 3bit_assets SET
											asset_title			=	'".addslashes($data['asset_title'])."',
											asset_name			=	'".addslashes($upName)."',
											asset_thumb			=	'".addslashes($upName)."',
											asset_category_id	=	'".addslashes($data['cat_id'])."',
											asset_alt			=	'".addslashes($data['asset_alt'])."',
											asset_type			=	'".addslashes($type_ext)."',
											asset_height		=	'".addslashes($assetize[1])."',
											asset_width			=	'".addslashes($assetize[0])."',
											asset_size			=	'".$imgsize."',
											keywords			=	'".$tottags."',
											status				=	'".$data['status']."',								
											modified_date		=	now() WHERE asset_id='".$data['id']."'");
				}
			}
		}else{
			$this->_db->query("UPDATE 3bit_assets SET
											asset_title			=	'".addslashes($data['asset_title'])."',										
											asset_alt			=	'".addslashes($data['asset_alt'])."',
											keywords			=	'".$tottags."',
											status				=	'".$data['status']."',								
											modified_date		=	now() WHERE asset_id='".$data['id']."'");

		}
		$assetid	=	$data['id'];
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_assets','update',$assetid,'success');		
    }

	public function delete($id){
		$sql = "SELECT a.*,ac.* FROM 3bit_assets a,3bit_asset_categories ac WHERE a.asset_category_id=ac.asset_category_id AND a.asset_id='".$id."' ";
        if ($result = $this->_db->query($sql)) {
			$row	=	$result->fetchAll();			
			$imgurl	=	'assets/'.$row[0]['asset_category_type'].'/'.$row[0]['asset_category_id'].'/'.$row[0]['asset_name'];
			@unlink($imgurl);
			if($row[0]['asset_category_type']=='images'){
				$thumburl	=	'assets/thumbs/'.$row[0]['asset_category_id'].'/'.$row[0]['asset_name'];
				@unlink($thumburl);
			}
        }
		$this->_db->delete("3bit_assets",'asset_id='.$id); 	
		$audit	=	new Application_Model_Audits();
		$audit->insert_audit('3bit_assets','delete',$id,'success');
	}
}

?>