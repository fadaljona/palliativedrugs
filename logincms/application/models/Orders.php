<?php

class Application_Model_Orders
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
		$this->_baseurl	= Zend_Registry::get("baseUrl");
    }    

    public function browse()
    {	   	

		$bar = new Application_Model_Barchart();
		$selMax	=	$this->_db->fetchRow("SELECT count( * ) AS cnt FROM 3bit_orders GROUP BY date_format( ordered_date, '%b %Y' ) ORDER BY cnt DESC LIMIT 0 , 1 ");		
		if($selMax){
			$maxVal	=	$selMax['cnt'];
			$bar->setMaxPoint($maxVal);
		}

		$total_size = 0;
		$result		= '';

		$selMon	=	$this->_db->fetchAll("select date_format(ordered_date,'%b %Y')as disdate,month(ordered_date) as dismon,year(ordered_date) as disyear from 3bit_orders where 1 group by disdate order by ordered_date desc ");

		if($selMon){			
			foreach($selMon as $row){	
				$disdate 	= 	$row['disdate'];
				$dismon		=	$row['dismon'];
				$disyear	=	$row['disyear'];
				$selCnt		=	$this->_db->fetchRow("select count(*)as cnt from 3bit_orders where date_format(ordered_date,'%b %Y') = '".$disdate."'");
				$count		=	$selCnt['cnt'];
				$selOrder	=	$this->_db->fetchRow("SELECT a.order_id FROM 3bit_order_actions a,3bit_orders o WHERE month(a.action_date)= '$dismon'  AND year(a.action_date)='$disyear' AND (a.action='New' OR a.action='Refunded') AND o.payment_status != 'Declined (General).' AND o.order_id=a.order_id ");	
				$count		=	count($selOrder);	

				$bar->setCurrPoint($count);
				$result	.=	'<tr>
								<td><b><a href="'.$this->_baseurl.'/orders/dashboard">'.$row['disdate'].'</a></b></td>	
								<td><a href="#">'.$count.'</a></td>
								<td valign="baseline">'.$bar->getBar().'</td>
							</tr>';
			}
		}

		return $result;		
    }

	public function order_history()
	{
	  $result	= $this->_db->fetchAll('SELECT *, DATE_FORMAT(history_date,"%D %M")as amonth FROM 3bit_order_history WHERE 1 ORDER BY history_date DESC ');		

	  return $result;	 

	}

	public function orders_list($sortname,$sortorder,$type)
    {
		$sql		=	"SELECT o.order_id,concat(c.fname, ' ',c.lname)as Name, o.ordered_date,o.invoice_number,o.shipped_date, o.ordertotal,o.status FROM 3bit_orders AS o INNER JOIN 3bit_customers AS c ON o.customer_id = c.customer_id WHERE  o.ccerrcode !='' ";     

		if($type !='Cancelled')
			$sql	.=	" AND o.status = '".$type."'";
		else
			$sql	.=	" AND (o.status = 'Rejected' OR o.status = 'Archived') ";

		if($sortname !='fname')
			$sql	.= " GROUP BY o.order_id ORDER BY o.".$sortname." ".$sortorder." ";
		else
			$sql	.= " GROUP BY o.order_id ORDER BY c.".$sortname." ".$sortorder." ";

		$result	=	$this->_db->fetchAll($sql);

		return $result;

	}

	public function getYears()
	{
		return $this->_db->fetchAll('SELECT year(ordered_date) as yer,year(current_date)as cyer  FROM 3bit_orders WHERE 1 GROUP BY year(ordered_date) ORDER BY  ordered_date DESC');
	}

	public function order_count()
	{
		$data	=	$this->_db->fetchAll("SELECT count(*) as count,o.status FROM 3bit_orders o,3bit_customers c WHERE o.customer_id=c.customer_id AND o.ccerrcode !='' GROUP BY o.status ORDER BY o.ordered_date DESC");
		return $data;
		
	}

	public function order_overview()
	{
	 
		$d				=	date("d");
		$m				=	date("m");
		$y				=	date("Y");
		$tstamp			=	mktime(0,0,0,$m,$d,$y);
		$Tdate 			= 	getdate($tstamp);
		$weekday		=	$Tdate["wday"];
		$min			=	$weekday;	
		$tfday			=	date("d-m-Y", mktime(0, 0, 0, $m, $d-$min, $y));
		$dA				=	explode("-",$tfday);
		$d1				=	$dA[0];
		$m1				=	$dA[1];
		$y1				=	$dA[2];
		$lfday			=	date("d-m-Y", mktime(0, 0, 0, $m1, $d1-7, $y));
		$nfday			=	date("d-m-Y", mktime(0, 0, 0, $m1, $d1+7, $y));

		$return_text	=	'<fieldset class="current_week">
								<legend>ORDERS  THIS WEEK</legend>
								<table border="0" width="100%" cellpadding="0" cellspacing="0" class="display">
								 <thead>
								  <tr>
									<th>Sunday</th>
									<th>Monday</th>
									<th>Tuesday</th>
									<th>Wednesday</th>
									<th>Thursday</th>
									<th>Friday</th>
									<th>Saturday</th>        
								  </tr>
								  </thead>
								  <tr>';
		for($i=0;$i<=6;$i++){
			$day	=	date("jS M Y", mktime(0, 0, 0, $m1, $d1+$i, $y1));
			$mday	=	date("d-m-Y", mktime(0, 0, 0, $m1, $d1+$i, $y1));
			$end	=	explode("-",$mday);
			$mdate 	=	$end[2]."-".$end[1]."-".$end[0];
			$date	=	$day;

			$return_text	.=	'<td>'.$date.'<ul>';

			$selship		=	$this->_db->fetchAll("SELECT a.order_id as cnt,a.order_id FROM 3bit_order_actions a,3bit_orders o WHERE DATE_FORMAT(a.action_date,'%Y-%m-%d')= '$mdate' AND (a.action='New' OR a.action='Refunded') AND o.payment_status != 'Declined (General).' AND o.order_id=a.order_id");
			$srows			=	count($selship);
			if($selship){			
				$ref_total = 0;
				$act_total = 0;
				$sales_tot = 0;

				$selRef	=	$this->_db->fetchRow("SELECT sum(amount_paid) as total FROM 3bit_order_actions WHERE DATE_FORMAT(action_date,'%Y-%m-%d')= '$mdate' AND action='Refunded' ");

				if($selRef){
					$ref_total	=	$selRef['total'];
				}

				$selSales		=	$this->_db->fetchRow("SELECT sum(a.amount_paid) as total FROM 3bit_order_actions a,3bit_orders o WHERE DATE_FORMAT(a.action_date,'%Y-%m-%d')= '$mdate' AND (a.action='New') AND o.payment_status != 'Declined (General).' AND o.order_id=a.order_id ");					

				if($selSales){
					$act_total	 =	 $selSales['total'];
				}
				$sales_tot		=	$act_total+$ref_total;

				if($sales_tot<>0){
					$return_text	.=	'<li>Sales&nbsp;('.$srows.')<br><h3>&pound;'.number_format($sales_tot,2,'.','').'</h3></li>';
				}
			}

			$return_text	.=	'</ul></td>';

		}
		$return_text	.=	'</tr>
							</table>
						</fieldset>
						<fieldset>
							<legend>ORDERS  THIS MONTH</legend>
							<table border="0" width="100%" cellpadding="0" cellspacing="0" class="display">
							   <thead>
							  <tr>
								<th>'.date("F-Y", mktime(0, 0, 0, $m, $d, $y)).'</th>
							  </tr>
							  </thead>
							  <tr>
								<td>
									<ul>';

		$result		=		$this->_db->fetchAll("SELECT a.order_id as cnt FROM 3bit_order_actions a,3bit_orders o WHERE month(a.action_date)='$m' AND year(a.action_date)= '$y'  AND (a.action='New' OR a.action='Refunded') AND o.payment_status !='Declined (General).' AND a.order_id=o.order_id ");	
		
		if($result)  {
			$sales_tot	=	0;
			$ref_tot	=	0;
			$act_tot	=	0;
			$srows		=	count($result);

			$selRef	=	$this->_db->fetchRow("SELECT sum(amount_paid) as total FROM 3bit_order_actions WHERE month(action_date)= '$m' AND year(action_date) = '$y' AND (action='Refunded') ");												
			if($selRef){
				$ref_tot	=	$selRef['total'];
			}
			
			$selSales		=	$this->_db->fetchRow("SELECT sum(a.amount_paid) as total FROM 3bit_order_actions a,3bit_orders o WHERE month(a.action_date)= '$m' AND year(a.action_date)='$y' AND (a.action='New') AND a.order_id=o.order_id AND o.payment_status != 'Declined (General).' ");		
			if($selSales){
				$act_tot	 =	 $selSales['total'];
			}

			$sales_tot		=	$act_tot+$ref_tot;

			if($sales_tot<>0){
				$return_text	.=	'<li>Sales&nbsp;('.$srows.')<br /><h3>&pound;'.number_format($sales_tot,2,'.','').'</h3></li>';
			}
		}

		 $return_text	.=	'</ul>
							</td>
						 </tr>
					</table>
				</fieldset>';
		return $return_text;

  }

  public function dash_orders()
  {

	  $ord_sql =  $this->_db->fetchAll("SELECT o.order_id, concat(c.fname, ' ',c.lname)as Name, o.ordered_date , o.ordertotal,o.status,o.payment_status,nu.organisation FROM 3bit_orders AS o INNER JOIN 3bit_customers AS c ON o.customer_id = c.customer_id LEFT JOIN new_users nu ON o.frontuser_id=nu.id WHERE o.status='New' AND o.ccerrcode !='' AND o.payment_method='World Pay' ORDER BY o.ordered_date DESC");		
	  $numrows	=	count($ord_sql);  
	  if($numrows>0){
		  $total_new	=	0;
		  $return_text	=	'<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display">
							<thead>
							<tr>				
								<th height="20" class="helpHed">Order No</th>					
								<th>Customer Name</th>
								<th>Organisation</th>
								<th>OrderDate</th>
								<th>Status</th>
								<th>OrderTotal</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>';
		 foreach($ord_sql as $row){
			 $total_new	+=	$row['ordertotal'];
			 $return_text	.=	'<tr>
									 <td>'.$row['order_id'].'</td>
									 <td>'.$row['Name'].'</td>
									 <td>'.$row['organisation'].'</td>
									 <td>'.$row['ordered_date'].'</td>
									 <td>'.$row['status'].'</td>
									 <td>&pound;'.$row['ordertotal'].'</td>
									 <td><a class="button" href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'"><img src="'.$this->_baseurl.'/public/images/icons/page_white_text.png" alt="View Order" title="View Order" />View</a>
									 </td>
								 </tr>'; 
		 }

		  $return_text	.=	'</tbody>
							<tfoot>
								  <tr>
									  <td colspan="8">Total New Orders('.$numrows.')</td>
								  </tr>
								  <tr>
									  <td colspan="8">Total Value Of Orders &pound;'.number_format($total_new,2,'.','').'</td>
								  </tr>
							</tfoot>
							</table>';
		  return $return_text;
	  }  

  }
  
   public function proforma_dash_orders()
  {

	  $ord_sql =  $this->_db->fetchAll("SELECT o.order_id, concat(c.fname, ' ',c.lname)as Name, o.ordered_date , o.ordertotal,o.status,o.payment_status,nu.organisation FROM 3bit_orders AS o INNER JOIN 3bit_customers AS c ON o.customer_id = c.customer_id LEFT JOIN new_users nu ON o.frontuser_id=nu.id WHERE (o.status='' OR o.status='New') AND o.payment_method='Pro-Forma' ORDER BY o.ordered_date DESC");		
	  $numrows	=	count($ord_sql);  
	  if($numrows>0){
		  $total_new	=	0;
		  $return_text	=	'<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display">
							<thead>
							<tr>				
								<th height="20" class="helpHed">Order No</th>					
								<th>Customer Name</th>
								<th>Organisation</th>
								<th>OrderDate</th>
								<th>Status</th>
								<th>OrderTotal</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>';
		 foreach($ord_sql as $row){
			 $total_new	+=	$row['ordertotal'];
			 $return_text	.=	'<tr>
									 <td>'.$row['order_id'].'</td>
									 <td>'.$row['Name'].'</td>
									 <td>'.$row['organisation'].'</td>
									 <td>'.$row['ordered_date'].'</td>
									 <td>'.$row['status'].'</td>
									 <td>&pound;'.$row['ordertotal'].'</td>
									 <td><a class="button" href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'"><img src="'.$this->_baseurl.'/public/images/icons/page_white_text.png" alt="View Order" title="View Order" />View</a>
									 </td>
								 </tr>'; 
		 }

		  $return_text	.=	'</tbody>
							<tfoot>
								  <tr>
									  <td colspan="8">Total New Orders('.$numrows.')</td>
								  </tr>
								  <tr>
									  <td colspan="8">Total Value Of Orders &pound;'.number_format($total_new,2,'.','').'</td>
								  </tr>
							</tfoot>
							</table>';
		  return $return_text;
	  }  

  }

  public function dash_awaiting()
  {

	  $ord_sql =  $this->_db->fetchAll("SELECT o.order_id, concat(c.fname, ' ',c.lname)as Name, o.ordered_date , o.ordertotal,o.status,o.payment_status,nu.organisation FROM 3bit_orders AS o INNER JOIN 3bit_customers AS c ON o.customer_id = c.customer_id LEFT JOIN new_users nu ON o.frontuser_id=nu.id WHERE o.status='Accepted' AND o.ccerrcode !='' ORDER BY o.ordered_date DESC");		
	  $numrows	=	count($ord_sql);  
	  if($numrows>0){
		  $total_await	=	0;

		  $return_text	=	'<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display">
							<thead>
							<tr>				
								<th height="20" class="helpHed">Order No</th>					
								<th>Customer Name</th>
								<th>Organisation</th>
								<th>OrderDate</th>
								<th>Status</th>
								<th>OrderTotal</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>';
		 foreach($ord_sql as $row){
			 $total_await	+=	$row['ordertotal'];
			 $return_text	.=	'<tr>
									 <td>'.$row['order_id'].'</td>
									 <td>'.$row['Name'].'</td>
									 <td>'.$row['organisation'].'</td>
									 <td>'.$row['ordered_date'].'</td>
									 <td>'.$row['status'].'</td>
									 <td>&pound;'.$row['ordertotal'].'</td>
									 <td><a class="button" href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'"><img src="'.$this->_baseurl.'/public/images/icons/page_white_text.png" alt="View Order" title="View Order" />View</a>
									 </td>
								 </tr>'; 
		 }

		  $return_text	.=	'</tbody>
							<tfoot>
								  <tr>
									  <td colspan="8">Total Orders Awiting to Dispatch('.$numrows.')</td>
								  </tr>
								  <tr>
									  <td colspan="8">Total Value Of Orders &pound;'.number_format($total_await,2,'.','').'</td>
								  </tr>
							</tfoot>
							</table>';
		  return $return_text;
	  }
  }

  public function dash_dispatched()
  {

	  $ord_sql =  $this->_db->fetchAll("SELECT o.order_id,o.invoice_number,concat(c.fname, ' ',c.lname)as Name, o.ordered_date , o.ordertotal , o.status,o.payment_status,o.courier_id,o.tracking_info,nu.organisation FROM 3bit_orders AS o INNER JOIN 3bit_customers AS c ON o.customer_id = c.customer_id LEFT JOIN new_users nu ON o.frontuser_id=nu.id WHERE o.status='Shipped' AND o.ccerrcode !='' AND  DATE_FORMAT(o.shipped_date,'%Y-%m-%d')= '".date("Y-m-d")."' ORDER BY o.shipped_date DESC");		
	  $numrows	=	count($ord_sql);  
	  if($numrows>0){
		  $total_today	=	0;
		  $return_text	=	'<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display">
							<thead>
							<tr>				
								<th height="20" class="helpHed">Order No</th>					
								<th>Customer Name</th>
								<th>Organisation</th>
								<th>OrderDate</th>
								<th>Status</th>
								<th>OrderTotal</th>
								<th>Courier</th>
								<th>Tracking Info</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>';
		 foreach($ord_sql as $row){
			 $total_today	+=	$row['ordertotal'];
			 $couriername	=	'N/A';
			 if($row['courier_id']=='0')
				 $status	=	'Completed';
			 else
				 $status	=	$row['status'];

			 $selCTit		=	$this->_db->fetchRow("SELECT courier_name,courier_url FROM 3bit_courier WHERE courier_id='".$row['courier_id']."'");	
			 if($selCTit){
				$couriername	=	$selCTit['courier_name'];
				$courierurl		=	$selCTit['courier_url'];
			 }

			 $return_text	.=	'<tr>
									 <td>'.$row['order_id'].'/'.$row['invoice_number'].'</td>
									 <td>'.$row['Name'].'</td>
									 <td>'.$row['organisation'].'</td>
									 <td>'.$row['ordered_date'].'</td>
									 <td>'.$status.'</td>
									 <td>&pound;'.$row['ordertotal'].'</td>
									 <td>'.$couriername.'</td>
									 <td>'.stripslashes($row['tracking_info']).'</td>
									 <td><a class="button" href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'"><img src="'.$this->_baseurl.'/public/images/icons/page_white_text.png" alt="View Order" title="View Order" />View</a>
									 </td>
								 </tr>'; 
		 }

		  $return_text	.=	'</tbody>
							<tfoot>
								  <tr>
									  <td colspan="8">Total Orders Dispatched ('.$numrows.')</td>
								  </tr>
								  <tr>
									  <td colspan="8">Total Value Of Orders &pound;'.number_format($total_today,2,'.','').'</td>
								  </tr>
							</tfoot>
							</table>';
		  return $return_text;
	  }
  }

  public function dash_sales()
  {	   
	  $ord_sql =  $this->_db->fetchAll("SELECT oa.*,o.customer_id,o.order_id,concat(c.fname, ' ',c.lname)as Name,o.status,nu.organisation FROM 3bit_order_actions oa,3bit_customers c,3bit_orders o LEFT JOIN new_users nu ON o.frontuser_id=nu.id WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='New' OR oa.action='Refunded') AND  DATE_FORMAT(oa.action_date,'%Y-%m-%d')= '".date("Y-m-d")."' ORDER BY oa.action_date DESC");		
	  $numrows	=	count($ord_sql);  
	  if($numrows>0){
		  $total_sales	=	0;
		  $return_text	=	'<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display">
							<thead>
							<tr>				
								<th height="20" class="helpHed">Order No</th>					
								<th>Customer Name</th>
								<th>Organisation</th>
								<th>OrderDate</th>
								<th>Status</th>
								<th>OrderTotal</th>							
								<th>Action</th>
							</tr>
							</thead>
							<tbody>';
		 foreach($ord_sql as $row){
			 $total_sales	+=	$row['amount_paid'];
			 $return_text	.=	'<tr>
									 <td>'.$row['order_id'].'</td>
									 <td>'.$row['Name'].'</td>
									 <td>'.$row['organisation'].'</td>
									 <td>'.$row['action_date'].'</td>
									 <td>'.$row['status'].'</td>
									 <td>&pound;'.$row['amount_paid'].'</td>									 
									 <td><a class="button" href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'"><img src="'.$this->_baseurl.'/public/images/icons/page_white_text.png" alt="View Order" title="View Order" />View</a>
									 </td>
								 </tr>'; 
		 }

		  $return_text	.=	'</tbody>
							<tfoot>
								  <tr>
									  <td colspan="8">Total Sales ('.$numrows.')</td>
								  </tr>
								  <tr>
									  <td colspan="8">Total Value Of Sales &pound;'.number_format($total_sales,2,'.','').'</td>
								  </tr>
							</tfoot>
							</table>';
		  return $return_text;
	  }
  }

  public function dash_refunds()
  {	   
	  $ord_sql =  $this->_db->fetchAll("SELECT oa.*,o.customer_id,o.order_id,o.invoice_number,concat(c.fname, ' ',c.lname)as Name,nu.organisation FROM 3bit_order_actions oa,3bit_customers c,3bit_orders o LEFT JOIN new_users nu ON o.frontuser_id=nu.id WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='Refunded') AND  DATE_FORMAT(oa.action_date,'%Y-%m-%d')= '".date("Y-m-d")."' AND (o.status !='Archived' AND o.status !='Rejected' AND o.status != 'Shipped') ORDER BY oa.action_date DESC");		
	  $numrows	=	count($ord_sql);  
	  if($numrows>0){
		  $total_refunds=	0;
		  $return_text	=	'<table width="100%" cellpadding="0" cellspacing="0" border="0" class="display">
							<thead>
							<tr>				
								<th height="20" class="helpHed">Order No</th>					
								<th>Customer Name</th>
								<th>Organisation</th>
								<th>OrderDate</th>
								<th>Status</th>
								<th>OrderTotal</th>							
								<th>Action</th>
							</tr>
							</thead>
							<tbody>';
		 foreach($ord_sql as $row){
			 $total_refunds	+=	$row['amount_paid'];
			 $return_text	.=	'<tr>
									 <td>'.$row['order_id'].'</td>
									 <td>'.$row['Name'].'</td>
									 <td>'.$row['organisation'].'</td>
									 <td>'.$row['action_date'].'</td>
									 <td>'.$row['action'].'</td>
									 <td>&pound;'.$row['amount_paid'].'</td>									 
									 <td><a class="button" href="'.$this->_baseurl.'/orders/view/id/'.$row['order_id'].'"><img src="'.$this->_baseurl.'/public/images/icons/page_white_text.png" alt="View Order" title="View Order" />View</a>
									 </td>
								 </tr>'; 
		 }

		  $return_text	.=	'</tbody>
							<tfoot>
								  <tr>
									  <td colspan="8">Total Refunds ('.$numrows.')</td>
								  </tr>
								  <tr>
									  <td colspan="8">Total Value Of Sales &pound;'.number_format($total_refunds,2,'.','').'</td>
								  </tr>
							</tfoot>
							</table>';
		  return $return_text;
	  }
  }

  public function getDetails($id)
  {
		$data	=	$this->_db->fetchRow("SELECT o.*,o.status as ostatus,DATE_FORMAT(o.ordered_date,'%d-%m-%Y')as dat, DATE_FORMAT(o.ordered_date,'%h:%i %p')as tim ,u.customer_id,u.email as cemail,ca.*,c.* FROM 3bit_orders o,3bit_customers u,country c,3bit_customers_address ca WHERE o.customer_id=u.customer_id AND o.order_id='".$id."' AND ca.address_id = o.address_id and c.id=u.country order by o.order_id DESC");

		$details = '';

		if($data){

			$details =	'<table width="100%" border="0" cellspacing="0" cellpadding="0" id="customer">
							<thead>';
			if($data['ostatus']=='Shipped'){

				$details	.=	'<tr> 
                					<th colspan="2"><h3>Invoice Number&nbsp;&nbsp;:&nbsp;&nbsp;'.$data['invoice_number'].'</h3></th>
             					</tr>';
			}
			$details	.=	'	</thead>
								<tbody>
								<tr>
									<td><h3>Invoice Address:</h3> <a href="#" onclick=\'javascript:editdetails("'.$this->_baseurl.'/orders/editinvoice/id/'.$data['order_id'].'");\'>(Edit)<br></a> 
									'.ucfirst($data['billing_fname'])."&nbsp;".ucfirst($data['billing_lname']).'<br>';
			if($data['company'] != "") 
					$details	.=	$data['company']."<br>";

			if($data['billing_address1'] != "") 
					$details	.=	$data['billing_address1']."<br>";

			if($data['billing_address2'] != "") 
				$details	.=	$data['billing_address2']."<br>";
			
			if($data['billing_city'] != "") 
				$details	.=	$data['billing_city']."<br>";
			
			if($data['billing_country'] != ""){
				$selCntry	=	$this->_db->fetchRow("SELECT country FROM 3bit_country WHERE id='".$data['billing_country']."'");
				if($selCntry){											
					$billcountry	=	$selCntry['country'];
					$details	.= $billcountry."<br>";
				}				
			}

			if($data['billing_postcode']!="")
				$details	.= "(".$data['billing_postcode'].")<br>";

			if($data['billing_phone'] != "") 
				$details	.= "<b>Telephone:</b>".$data['billing_phone']."<br>"; 

			if($data['cemail'] != "") 
				$details	.= "<b>Email:</b>".$data['cemail']; 
			
				$details	.=  '</td>								  
								  <td><h3>Delivery Address:</h3> <a href="#" onclick=\'javascript:editdetails("'.$this->_baseurl.'/orders/editdelivery/id/'.$data['order_id'].'");\'>(Edit)<br></a> '.ucfirst($data['shipping_fname'])."&nbsp;".ucfirst($data['shipping_lname'])."<br>";

			if($data['shipping_address1'] != "") 
				$details	.= $data['shipping_address1']."<br>";
			if($data['shipping_address2'] != "") 
				$details	.= $data['shipping_address2']."<br>";					
			if($data['shipping_city'] != "") 
				$details	.= $data['shipping_city']."<br>";
			if($data['shipping_country'] != "") {
					$selCntry	=	$this->_db->fetchRow("SELECT country FROM 3bit_country WHERE id='".$data['shipping_country']."'");
					if($selCntry){											
						$shipcountry	=	$selCntry['country'];
						$details	.= $shipcountry."<br>";
					}
				  }
									
			if($data['shipping_postcode']!="")
				$details	.= "(".$data['shipping_postcode'].")<br>";
			if($data['shipping_phone'] != "") 
				$details	.= "<b>Telephone:</b>".$data['shipping_phone']; 

				$details	.=	'</td>
								</tr>
								</tbody>
								</table>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" id="orderdetails">
								 	<thead>
									<tr> 
										<td><h3>Order Details:</h3></td>
										              
									</tr>
									</thead>
									<tbody>
									<tr> 
										<td>
										Order Date&nbsp;:&nbsp;'.$data['dat'].'<br>
										Order Time&nbsp;:&nbsp;'.$data['tim'].'<br />
										Order Number&nbsp;:&nbsp;'.$id.'<br />';
					if(!empty($data['user_id'])){
						$details	.=	'Order Ref&nbsp;:&nbsp;'.$data["order_number"].'<br />';
					}else{
						$details	.=	'Transaction Id&nbsp;:&nbsp;'.$data["order_number"].'<br />';
					}
					$details	.=	'	Status&nbsp;:&nbsp;'.$data["payment_status"].'<br />
										Payment Method&nbsp;:&nbsp;'.$data["payment_method"].'
										';
									if($data['vat_number']!=''){
										$details	.=	'<br />VAT No&nbsp;:&nbsp;'.$data["vat_number"];
									}
										$details	.=	'
										</td>																
									</tr>
									</tbody>
								</table>
								<table width="100%" border="0" id="orderinfo" class="display">
									<col id="description" />
									<col id="sku" />
									<col id="qty" />
									<col id="price" />
									<col id="total" />
								 	<thead>
									<tr class="invheaders"> 
										<th class="txtar">Description</th>
										<th>Product Code</th>
										<th>Quantity</th>
										<th>Unit Price</th>             
										<th>Total</th>                
									</tr>
									</thead>';

				if($data['ostatus']=='Refunded')
					$res_order 		= $this->_db->fetchAll("SELECT * FROM 3bit_credit_details WHERE order_id='".$data['order_id']."'");
				else								
					$res_order 		= $this->_db->fetchAll("SELECT * FROM 3bit_order_details WHERE order_id='".$data['order_id']."'");

				$numrows 			= count($res_order);
				$sub_total			= 0;
				$shipping			= 0;
				$discount			= 0;


				foreach($res_order as $rows_order){
					$product_name_var	=	'';
					if(!empty($rows_order['product_id'])){					
						$selProd	=	$this->_db->fetchAll("SELECT p.* FROM 3bit_products p WHERE p.product_id='".$rows_order['product_id']."' ");		
						if(count($selProd)>0){								
							foreach($selProd as $res){						
								$product_id		=	$res['product_id'];	
								$product_name 	= 	$res['product_name'];
								$product_code	=	$res['product_code'];
								$product_qty	=	$rows_order['quantity'];	
								if($res['size'] != '') 
									$product_name_var = "( ".$res['size']." )" ;
								else if($res['style'] != '') 
									$product_name_var = "( ".$res['style']." )" ;	
								$customer_id		= $data['customer_id'];											
							}
						}
					}

					// updated on 24/04/2009 

					$product_price	=	$rows_order['price'];
					$product_price	=	number_format($product_price, 2, '.', '');	
					$line_price		=	$rows_order['quantity']*$product_price;
					$line_price		=	number_format($line_price, 2, '.', '');	
					$sub_total		+=	$line_price;
					$sub_total		=	number_format($sub_total, 2, '.', '');
					$vatprice		=	$data['order_vat_tax']; 

					$details		.=	'<tr> 
											<td class="txtar">'.$product_name.$product_name_var.'</td>
											<td>&nbsp;'.$product_code.'</td>
											<td>'.$rows_order['quantity'].'</td>
											<td>&pound;'.$product_price.'</td>                
											<td class="totalprice">&pound;'.$line_price.'</td>                
										</tr>';
					if(!empty($rows_order['shipping']))
						$shipping		=	$rows_order['shipping'];
					if(!empty($rows_order['discount']))
						$discount		=	$rows_order['discount'];

					if(!empty($rows_order['refund_amount']))
						$refund_amount	=	$rows_order['refund_amount'];
					if(!empty($rows_order['restocking_fee']))
						$restocking_fee	=	$rows_order['restocking_fee'];
					if(!empty($rows_order['total_refund']))
						$total_refund	=	$rows_order['total_refund'];
					

				}
				$item_vat			=	$data['order_vat_tax'];
				$total 				=	$data['ordertotal'];
				$disp_total			=	number_format($sub_total, 2, '.', '');

			if($data['ostatus']=='Refunded'){
				$details			.=	'<tfoot><tr class="invtotals"> 
											<td colspan="4"><b>Refund Amount :</b></td>
											<td >&pound;'.$refund_amount.'</td>                          
										 </tr>
										 <tr class="invtotals"> 
											<td colspan="4"><b>Restocking Fee :</b></td>
											<td >&pound;'.$restocking_fee.'</td>                          
										 </tr>
										 <tr class="invtotals"> 
											<td colspan="4"><b>Total Refund :</b></td>
											<td>&pound;'.$total_refund.'</td>                          
										 </tr></tfoot></table>';
			}else{
				$details			.=	'<tfoot><tr class="invtotals"> 
											<td colspan="4"><b>Sub Total :</b></td>
											<td >&pound;'.$disp_total.'</td>                          
										 </tr>
										 <tr class="invtotals"> 
											<td colspan="4"><b>Vat :</b></td>
											<td >&pound;'.$item_vat.'</td>                          
										 </tr>
										 <tr class="invtotals"> 
											<td colspan="4"><b>Discount :</b></td>
											<td>&pound;'.$discount.'</td>                          
										 </tr>
										 <tr class="invtotals">
											<td colspan="4"><b>Shipping Charge :</b></td>
											<td>&pound;'.$shipping.'</td>                          
										 </tr>
										 <tr class="invtotals">
											<td colspan="4"><b>Total Cost :</b></td>
											<td>&pound;'.number_format($total, 2, '.', '').'</td> 
										 </tr></tfoot>
										</table>';
			}						
			if($data['ostatus']=='Shipped'){
				$details			.=	'<div id="orderactions">
											<a class="button"  onclick=\'javascript:preview_order("'.$this->_baseurl.'/orders/print/id/'.$data['order_id'].'");\'><img src="'.$this->_baseurl.'/public/images/icons/printer.png" alt="Print" title="Print"/>Print</a>	
											<a class="button" onclick=\'javascript:preview_order("'.$this->_baseurl.'/orders/refund/id/'.$data['order_id'].'");\'><img src="'.$this->_baseurl.'/public/images/icons/accept.png" alt="Refund" title="Refund"/>Refund</a>
										</div>';
		   } else { 
			    if($data['ostatus']=='Refunded'){
					$details		.=	'<div id="orderactions">
												<a class="button"  onclick=\'javascript:preview_order("'.$this->_baseurl.'/orders/refundprint/id/'.$data['order_id'].'");\'><img src="'.$this->_baseurl.'/public/images/icons/printer.png" alt="Print" title="Print"/>Print</a>
													';	
				}else{
				   $details			.=	'<div id="orderactions">
											<a class="button"  onclick=\'javascript:preview_order("'.$this->_baseurl.'/orders/print/id/'.$data['order_id'].'");\'><img src="'.$this->_baseurl.'/public/images/icons/printer.png" alt="Print" title="Print"/>Print</a>
													';	

				}
				if($data['ostatus']=='New' || $data['ostatus']=='Accepted'){
						$details	.=	'<a class="button negative" onclick=\'return orderprocess(document.vieworder,"cancel_order","'.$data['ostatus'].'");\' ><img src="'.$this->_baseurl.'/public/images/icons/cross.png" alt="Cancel" title="Cancel"/>Cancel Order</a>
												<a class="button negative"  onclick=\'return orderprocess(document.vieworder,"reject_order","'.$data['ostatus'].'");\' ><img src="'.$this->_baseurl.'/public/images/icons/cross.png"  alt="Reject" title="Reject"/>Reject Order</a>';												
			   }if($data['ostatus']=='New'){
						$details	.=	' <a class="button positive" onclick=\'return orderprocess(document.vieworder,"approve_order","'.$data['ostatus'].'");\'" ><img src="'.$this->_baseurl.'/public/images/icons/accept.png" alt="Approve"/>Approve Order</a>';
			   }
			   if($data['ostatus']=='New' || $data['ostatus']=='Accepted'){
						$details	.=	'&nbsp;<a class="button positive" onclick=\'return orderprocess(document.vieworder,"complete_order","'.$data['ostatus'].'");\' ><img src="'.$this->_baseurl.'/public/images/icons/accept.png" alt="Dispatch" title="Dispatch"/>Complete Order</a>';
			   }
				$details	.=	'</div>';
			}	
			
			 $details			.=	'<span id="history_list'.$data['order_id'].'">										
										<table cellpadding="0" cellspacing="0" width="100%" border="0" id="ordernotes" class="display">
											<thead>
											
												<tr>
													<td colspan="3">History</td>
												</tr>
											</thead>
											<tbody>
											<tr>		
												<td colspan="3">
													<input type="text" id="txtHis'.$data['order_id'].'" name="txtHis'.$data['order_id'].'" style="width:300px;"  /><a class="button" onclick=\'add_history('.$data['order_id'].',document.getElementById("txtHis'.$data['order_id'].'").value,"'.$_SESSION['wlsession']['fullname'].'","'.$_SESSION['wlsession']['user_id'].'")\'><img src="'.$this->_baseurl.'/public/images/icons/comment_add.png" alt="Add History" title="Add History"/>Add History</a>
												</td>
											</tr>';
		  $selHis	=	$this->_db->fetchAll("SELECT *,date_format(history_date,'%D %b %Y') as hdate,date_format(history_date,'%H:%i')as htime FROM  3bit_order_history WHERE (order_id = '".$id."' or order_id=(select order_id from 3bit_credit_details WHERE refund_id='".$data['order_id']."' GROUP BY refund_id))");				
			if(count($selHis)>0){
				$k	=	0;
				foreach($selHis as $row){
					$k++;
					if($k%2 == 0)
						$bgcolor = "#EDEDED";
					else
						$bgcolor = "#FFFFFF";								
			
					$details	.=	'<tr bgcolor="'.$bgcolor.'">
										<td>'.$row['hdate'].'&nbsp;@&nbsp;'.$row['htime'].'</td>
										<td colspan="2">'.$row['status'].'</td>
									</tr>';
			
				}	
			}
			 $details	.=	'		</tbody>
								</table>
							</span>';
			
			if($data['ostatus']=='Accepted'){ 

				$details	.=	'<fieldset>
									<legend>Dispatch This Order</legend>
										<table cellpadding="0" cellspacing="0" border="0" width="100%" id="orderdispatch">
											<tr>
												<td>Courier Name</td>
												<td>:</td>
												<td>
													<select id="selcourier" name="selcourier">';
													$selC	=	$this->_db->fetchAll("select * from 3bit_courier where courier_status = '1'");
													if($selC){
														foreach($selC as $res){
														$details	.=	'<option value="'.$res['courier_id'].'">'.$res['courier_name'].'</option>';
														}
													}
													$details	.=	'</select>
												</td>
											</tr>									
									<tr>
										<td>Tracking Info</td>
										<td>:</td>
										<td><input type="text" name="txt_info" /></td>
									</tr>
									<tr>
										<td colspan="3">
											<a class="button positive" onclick=\'return orderprocess(document.vieworder,"ship_order","'.$data['ostatus'].'");\' ><img src="'.$this->_baseurl.'/public/images/icons/accept.png" alt="Dispatch" title="Dispatch"/>Dispatch Order</a>
										</td>
									</tr>
								</table>
							</fieldset>';


			}
		}	  
		return $details;
	}

	public function orderprocess($data)
	{		
		$order_process	=	$_POST['order_process'];
		$order_id		=	$_POST['order_id'];
		$old_status		=	$_POST['old_status'];

		if(trim($order_process)=='cancel_order'){
			$orderstatus	=	"Cancelled";				

			$sql = "UPDATE `3bit_orders` SET status ='Archived' WHERE order_id = '".$order_id."'";
			$this->_db->query($sql);

			$sqlUpdate	= "UPDATE 3bit_subscribers SET status = '0' WHERE order_id = '".$order_id."'";	
			$this->_db->query($sqlUpdate);

			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$order_id."' rel='facebox'>order #".$order_id."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$_SESSION['wlsession']['user_id']."' rel='facebox'>".$_SESSION['wlsession']['fullname']."</a>";
				
			$this->_db->query("INSERT INTO `3bit_order_history` SET order_id='".$order_id."',status	=	'".addslashes($adminurl)." cancelled ".addslashes($orderurl)." <span class=\'dashdate\'>".date('Y/m/d H:i:s')."</span>',user_id='".$_SESSION['wlsession']['user_id']."',history_date=now()");

			$this->_db->query("INSERT INTO 3bit_order_actions SET 
									order_id		=	'".$order_id."',
									action			=	'Archived',
									action_date		=	now(),
									payment_method	=	'',
									amount_paid		=	(SELECT ordertotal FROM 3bit_orders WHERE order_id='".$order_id."' ),
									status			=	'1',
									created_date	=	now(),
									modified_date	=	now()");
			if($old_status=="New")
				$message_type	=	'New Order Canceled';
			else if($old_status=="Accepted")
				$message_type	=	'Accepted Order Cancelled';	

			$this->sendMails($order_id,$message_type,'0','Customer');
			
	   }else if(trim($order_process)=='reject_order'){

			$orderstatus	=	"Cancelled";

			$sql = "UPDATE `3bit_orders` SET status = 'Rejected' WHERE order_id = '".$order_id."'";	

			$this->_db->query($sql);

			$sqlUpdate	= "UPDATE 3bit_subscribers SET status = '0' WHERE order_id = '".$order_id."'";	
			$this->_db->query($sqlUpdate);


			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$order_id."' rel='facebox'>order #".$order_id."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$_SESSION['wlsession']['user_id']."' rel='facebox'>".$_SESSION['wlsession']['fullname']."</a>";
				
			$this->_db->query("INSERT INTO `3bit_order_history` SET order_id='".$order_id."',status	=	'".addslashes($adminurl)." rejects ".addslashes($orderurl)." <span class=\'dashdate\'>".date('Y/m/d H:i:s')."</span>',user_id='".$_SESSION['wlsession']['user_id']."',history_date=now()");

			$this->_db->query("INSERT INTO 3bit_order_actions SET 
									order_id		=	'".$order_id."',
									action			=	'Rejected',
									action_date		=	now(),
									payment_method	=	'',
									amount_paid		=	(SELECT ordertotal FROM 3bit_orders WHERE order_id='".$order_id."' ),
									status			=	'1',
									created_date	=	now(),
									modified_date	=	now()");

			if($old_status=="New")
				$message_type	=	'New Order Rejected';
			else if($old_status=="Accepted")
				$message_type	=	'Accepted Order Rejected';

			$this->sendMails($order_id,$message_type,'0','Customer');

	   }else if(trim($order_process)=='approve_order'){
			$orderstatus	=	"Accepted";

			$sql	= "UPDATE `3bit_orders` SET status = 'Accepted' WHERE order_id = '".$order_id."'";	
			$this->_db->query($sql);

			//---------- Add order in group table -----------------------------------------------------

			

			$sqlROW		= $this->_db->fetchRow("SELECT od.product_id, o.frontuser_id, u.organisation, SUM(od.quantity) AS no_subscribtion, u.id  FROM 3bit_orders o, 3bit_order_details od, new_users u WHERE o.order_id = '".$order_id."' AND o.order_id = od.order_id  AND od.product_id  IN (8,9,10,11,12,13,14,15) AND o.frontuser_id = u.id GROUP BY  o.order_id");

			if($sqlROW){
				if($sqlROW['no_subscribtion']>1){
				$this->_db->query("INSERT INTO 3bit_subscribers_groups SET 
									organisation		=	'".addslashes($sqlROW['organisation'])."',
									start_date			=	now(),
									user_id		=	'".$sqlROW['frontuser_id']."',
									order_id		=	'".$order_id."',
									no_of_subscription	=	'".$sqlROW['no_subscribtion']."',
									subscribers			=	'',
									status	=	'1',
									date_created	=	now()");

				$group_id			= $this->_db->lastInsertId();

				$this->_db->query("UPDATE 3bit_subscribers SET group_id = '".$group_id."', subscribtion_user = '".$sqlROW['frontuser_id']."' WHERE order_id = '".$order_id."' ");	
				}
			}

			//----------------------------- End -------------------------------------------------------
			
			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$order_id."' rel='facebox'>order #".$order_id."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$_SESSION['wlsession']['user_id']."' rel='facebox'>".$_SESSION['wlsession']['fullname']."</a>";

			$this->_db->query("INSERT INTO `3bit_order_history` SET order_id='".$order_id."',status	=	'".addslashes($adminurl)." approves ".addslashes($orderurl)." <span class=\'dashdate\'>".date('Y/m/d H:i:s')."</span>',user_id='".$_SESSION['wlsession']['user_id']."',history_date=now()");

			$this->_db->query("INSERT INTO 3bit_order_actions SET 
									order_id		=	'".$order_id."',
									action			=	'Accepted',
									action_date		=	now(),
									payment_method	=	'',
									amount_paid		=	(SELECT ordertotal FROM 3bit_orders WHERE order_id='".$order_id."' ),
									status			=	'1',
									created_date	=	now(),
									modified_date	=	now()");

			
			$chkProformaOrder		=	$this->_db->fetchRow("SELECT payment_method,frontuser_id FROM 3bit_orders WHERE order_id = '".$order_id."'");

			if($chkProformaOrder['payment_method']=="Pro-Forma"){

				$sql	= "UPDATE `3bit_orders` SET payment_status = 'Success' WHERE order_id = '".$order_id."'";	
				$this->_db->query($sql);

				$sqlUpdate	= "UPDATE 3bit_subscribers SET status = '1' WHERE order_id = '".$order_id."'";	
				$this->_db->query($sqlUpdate);

				$fecgetOrderDetails	=	$this->_db->fetchAll("SELECT product_id FROM 3bit_order_details WHERE order_id = '".$order_id."'");
				if(count($fecgetOrderDetails)>0){
					foreach($fecgetOrderDetails as $row){ 
						// Checking for subscription ==========================
						$res		=	$this->_db->fetchAll("SELECT p.*,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='3' AND p.product_id='".$row['product_id']."'");
						if(count($res)>0){
							$isOrderSubscription	=	"Y";
							$message_type	=	'Subscription Activated';
							$this->sendMails($order_id,$message_type,'0','Customer');							
						}

						// Checking for ebook ===============================
						$selPDF			=	$this->_db->fetchAll("SELECT p.*,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='4' AND p.product_id='".$row['product_id']."'");
	  				    $numPDF			=	count($selPDF);
						if($numPDF>0){
								
							$pdfSQL		=	"INSERT INTO 3bit_pdf_download SET 
																	order_id	=	'".$order_id."',
																	ordered_date=	now(),
																	frontuser_id=	'".$chkProformaOrder['frontuser_id']."',
																	downloaded	=	'0',
																	created_date=	now(),
																	modified_date=	now()";
							 $this->_db->query($pdfSQL);

							 //$this->updatePDF($chkProformaOrder['frontuser_id']);

							 $message_type	=	'Ebook Download';
							 
							 $this->sendMails($order_id,$message_type,'0','Customer');
						}

						// Checking for combo ===============================
						$selCombo			=	$this->_db->fetchAll("SELECT p.*,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='2' AND p.product_id='".$prow['product_id']."'");

						$numCombo	=	count($selCombo);	

						if($numCombo>0){

							if($prow['product_id']!='14' && $prow['product_id']!='15'){

								$pdfSQL		=	"INSERT INTO 3bit_pdf_download SET 
														order_id	=	'".$order_id."',
														ordered_date=	now(),
														frontuser_id=	'".$chkProformaOrder['frontuser_id']."',
														downloaded	=	'0',
														created_date=	now(),
														modified_date=	now()";

								 $this->_db->query($pdfSQL);

								 //$this->updatePDF($chkProformaOrder['frontuser_id']);

								 $message_type	=	'Combo Package';
							 
								 $this->sendMails($order_id,$message_type,'0','Customer');
							}else{

								 $message_type	=	'Combo Package(S & B)';
							 
								 $this->sendMails($order_id,$message_type,'0','Customer');	
							}	
						}
					}
				}
				
			}	

			$message_type	=	'New Order Accepted';
			$this->sendMails($order_id,$message_type,'0','Customer');

	   }else if(trim($order_process)=='ship_order'){

			$orderstatus	=	"Shipped";

			$invoice_number	=	$this->findmaxinvoice();

			$sql			=	"UPDATE 3bit_orders SET 
									status			=	'Shipped',
									invoice_number	=	'".$invoice_number."',
									shipped_date	=	now(),
									courier_id		=	'".$_POST['selcourier']."',
									tracking_info	=	'".$_POST['txt_info']."' WHERE order_id	=	'".$order_id."'";

			$this->_db->query($sql);

			$couriername	=	'';
			$courierurl		=	'';

			$selCTit		=	$this->_db->fetchRow("SELECT courier_name,courier_url FROM 3bit_courier WHERE courier_id='".$_POST['selcourier']."'");		
			if($selCTit){
				$couriername	=	$selCTit['courier_name'];
				$courierurl		=	$selCTit['courier_url'];
			}

			
			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$order_id."' rel='facebox'>order #".$order_id."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$_SESSION['wlsession']['user_id']."' rel='facebox'>".$_SESSION['wlsession']['fullname']."</a>";

			$this->_db->query("INSERT INTO `3bit_order_history` SET order_id='".$order_id."',status	=	'".addslashes($adminurl)." has dispatched ".addslashes($orderurl)." via ".$couriername." with   ".$_POST['txt_info']." <span class=\'dashdate\'>".date('Y/m/d H:i:s')."</span>',user_id='".$_SESSION['wlsession']['user_id']."',history_date=now()");


			$this->_db->query("INSERT INTO 3bit_order_actions SET 
									order_id		=	'".$order_id."',
									action			=	'Shipped',
									action_date		=	now(),
									payment_method	=	'',
									amount_paid		=	(SELECT ordertotal FROM 3bit_orders WHERE order_id='".$order_id."' ),
									status			=	'1',
									created_date	=	now(),
									modified_date	=	now()");
			$message_type	=	'Accepted Order Shipped';

			$this->sendMails($order_id,$message_type,'0','Customer');

	   }else if(trim($order_process)=='complete_order'){
			$orderstatus	=	"Shipped";

			$invoice_number	=	$this->findmaxinvoice();

			$sql			=	"UPDATE 3bit_orders SET 
									status			=	'Shipped',
									invoice_number	=	'".$invoice_number."',
									shipped_date	=	now() WHERE order_id	=	'".$order_id."'";

			$this->_db->query($sql);
			
			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$order_id."' rel='facebox'>order #".$order_id."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$_SESSION['wlsession']['user_id']."' rel='facebox'>".$_SESSION['wlsession']['fullname']."</a>";

			$this->_db->query("INSERT INTO `3bit_order_history` SET order_id='".$order_id."',status	=	'".addslashes($adminurl)." has completed ".addslashes($orderurl)." <span class=\'dashdate\'>".date('Y/m/d H:i:s')."</span>',user_id='".$_SESSION['wlsession']['user_id']."',history_date=now()");	

			$this->_db->query("INSERT INTO 3bit_order_actions SET 
									order_id		=	'".$order_id."',
									action			=	'Shipped',
									action_date		=	now(),
									payment_method	=	'',
									amount_paid		=	(SELECT ordertotal FROM 3bit_orders WHERE order_id='".$order_id."' ),
									status			=	'1',
									created_date	=	now(),
									modified_date	=	now()");

			$message_type	=	'Accepted Order Shipped';

			//$this->sendMails($order_id,$message_type,'0','Customer');
	   }

	   return $orderstatus;

	}

	public function findmaxinvoice(){
		$sublib_qry  = $this->_db->fetchRow("SELECT max(invoice_number) as max_invoice FROM `3bit_orders` WHERE 1");				
		if (!$sublib_qry){	
			$new_invoice	 = 1;
		}else{
			$new_invoice	 = $sublib_qry['max_invoice'] + 1;
		}
		return $new_invoice;
	}

	public function configRate(){
		$selVat		=	$this->_db->fetchRow("SELECT vat_id FROM product_vats WHERE is_live='1' LIMIT 0,1 ");

		if($selVat){
			$vatamt		= 	$selVat['vat_id'];
		}	
		return $vatamt;	
	}

	public function sendMails($order_id,$message_type,$preview,$towhom)
	{

		$data	=	$this->_db->fetchRow("SELECT o.*,o.status as ostatus,DATE_FORMAT(o.ordered_date,'%d-%m-%Y')as dat, DATE_FORMAT(o.ordered_date,'%h:%i %p')as tim ,u.customer_id,u.email as cemail,u.fname,u.lname,ca.*,c.* FROM 3bit_orders o,3bit_customers u,country c,3bit_customers_address ca WHERE o.customer_id=u.customer_id AND o.order_id='".$order_id."' AND ca.address_id = o.address_id and c.id=u.country order by o.order_id DESC");

		if($data){
			$ostatus		=	$data['ostatus'];

			// To get customer details --------------------------------------------------
			$customer_fname	=	ucfirst($data['fname']);
			$customer_lname	=	ucfirst($data['lname']);
			$customer_email	=	$data['cemail'];


			// To get billing details ---------------------------------------------------
			$billing_title	=	$data['billing_title'];
			$billing_fname	=	ucfirst($data['billing_fname']);
			$billing_lname	=	ucfirst($data['billing_lname']);
			$billing_addr1	=	stripslashes($data['billing_address1']);
			$billing_addr2	=	stripslashes($data['billing_address2']);
			$billing_city	=	stripslashes($data['billing_city']);
			$billing_state	=	stripslashes($data['billing_state']);
			if($data['billing_country'] != "") {
				$selCntry	=	$this->_db->fetchRow("SELECT country FROM 3bit_country WHERE id='".$data['billing_country']."'");
				if($selCntry){											
					$billing_country	=	$selCntry['country'];
				
				}
			}
			$billing_postcode=	$data['billing_postcode'];
			$billing_phone	=	$data['billing_phone'];
			$billing_email	=	$data['cemail'];

			// To get shipping details ------------------------------------------------
			$shipping_title	=	$data['shipping_title'];
			$shipping_fname	=	ucfirst($data['shipping_fname']);
			$shipping_lname	=	ucfirst($data['shipping_lname']);
			$shipping_addr1	=	stripslashes($data['shipping_address1']);
			$shipping_addr2	=	stripslashes($data['shipping_address2']);
			$shipping_city	=	stripslashes($data['shipping_city']);
			$shipping_state	=	stripslashes($data['shipping_state']);
			if($data['shipping_country'] != "") {
				$selCntry	=	$this->_db->fetchRow("SELECT country FROM 3bit_country WHERE id='".$data['shipping_country']."'");
				if($selCntry){											
					$shipping_country	=	$selCntry['country'];
				
				}
			}
			$shipping_postcode=	$data['shipping_postcode'];
			$shipping_phone	=	$data['shipping_phone'];	

			$ordered_date	=	$data['dat'];
			$ordered_time	=	$data['tim'];
			$invoice_number	=	$data['invoice_number'];
			$payment_method	=	$data['payment_method'];
			$payment_status	=	$data['payment_status'];
			$order_number	=	$data['order_number'];
			$order_vat		=	"&pound;".$data['order_vat_tax'];
			$ordertotal 	=	"&pound;".$data['ordertotal'];
			$tracking_info	=	$data['tracking_info'];

			$couriername	=	'';
			$courierurl		=	'';
			$courier_url	=	'';

			$selCTit		=	$this->_db->fetchAll("SELECT courier_name,courier_url FROM 3bit_courier WHERE courier_id='".$data['courier_id']."'");	
			if(count($selCTit)){
				$couriername	=	$selCTit[0]['courier_name'];
				$courierurl		=	$selCTit[0]['courier_url'];
			}

			
			// To get order details ----------------------------------------------------
			$res_order 		= $this->_db->fetchAll("SELECT * FROM 3bit_order_details WHERE order_id='".$order_id."'");
			$numrows 		= count($res_order);
			$sub_total		= 0;
			$ord_details	= '';
			$txt_details	= '';

			foreach($res_order as $rows_order){
				$product_name_var	=	'';
				if(!empty($rows_order['product_id'])){					
					$selProd	=	$this->_db->fetchAll("SELECT p.* FROM 3bit_products p WHERE p.product_id='".$rows_order['product_id']."' ");		
					if($selProd){								
						foreach($selProd as $res){						
							$product_id		=	$res['product_id'];	
							$product_name 	= 	$res['product_name'];
							$product_code	=	$res['product_code'];
							$product_qty	=	$rows_order['quantity'];	
							if($res['size'] != '') 
								$product_name_var = "( ".$res['size']." )" ;						  
							$customer_id		= $data['customer_id'];											
						}
						
						// Update the stock if the status changing to rejected or cancelled --------------

						if($preview=='0' && ($ostatus =='Rejected' || $ostatus== 'Archived')){
						  $this->_db->query("UPDATE 3bit_products SET available_stock= available_stock+".$product_qty."  WHERE product_id='".$product_id."'");
						}else if($preview=='0' && $ostatus =='Shipped'){ 
						  $this->_db->query("UPDATE 3bit_products SET site_stock = site_stock-".$product_qty." WHERE product_id = '".$product_id."'");
						}
					}
				}
				$product_price	=	$rows_order['price'];
				$product_price	=	number_format($product_price, 2, '.', '');	
				$line_price		=	$rows_order['quantity']*$product_price;
				$line_price		=	number_format($line_price, 2, '.', '');	
				$sub_total		+=	$line_price;
				$sub_total		=	number_format($sub_total, 2, '.', '');	
				
				//Concatinate the order details --------------------------------
				$ord_details	.=	$product_qty."&nbsp;x&nbsp;".$product_code."&nbsp;(".$product_name.$product_name_var.")&nbsp;&pound;".$line_price."<br>";
				$txt_details	.=	$product_qty." x ".$product_code." (".$product_name.$product_name_var.") GBP ".$line_price."\n";
				
				$shipping		=	"&pound;".$rows_order['shipping'];
				$discount		=	"&pound;".$rows_order['discount'];					
			}			
			$disp_total			=	number_format($sub_total, 2, '.', '');

			if($message_type=='Ebook Download' || $message_type=='Combo Package'){

				$selPDF			=	$this->_db->fetchRow("SELECT order_id,frontuser_id,downloaded FROM 3bit_pdf_download WHERE order_id='".$order_id."'");

				if(count($selPDF)>0){
					$base64		=	base64_encode($selPDF['order_id']."##".$selPDF['frontuser_id']);
					$link		=	'http://'.$_SERVER['HTTP_HOST'].'/download-pdf/'.$base64.'.html';
					$link_here	=	'<a href="'.$link.'">'.$link.'</a>';

				}

			}

		    if($towhom=='Customer'){
				// Send Email To Customer ----------------------------------
				
				$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
				$res				= 	$this->_db->fetchAll($sql_select);

			}else if($towhom=='Admin'){
				// Send Email To Customer ----------------------------------				
				$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
				$res				= 	$this->_db->fetchAll($sql_select); 
			}
			
			if(count($res)>0){
				foreach($res as $row){
					$body			= 	stripslashes($row['message_text']);
					$text			= 	stripslashes($row['text']);
					$subject		= 	stripslashes($row['mail_subject']);	
						
					$subtag			= 	str_replace("{order_id}",$order_id, $subject);
					$subtag			= 	str_replace("{ordered_date}",$ordered_date, $subtag);	
					$subtag			= 	str_replace("{fname}",$customer_fname, $subtag);
					$subtag			= 	str_replace("{lname}",$customer_lname, $subtag);
				
					if($body !=''){
						$bodytag		= 	str_replace("{fname}",$customer_fname, $body);
						$bodytag		= 	str_replace("{lname}",$customer_lname, $bodytag);
						$bodytag		= 	str_replace("{email}",$customer_email, $bodytag);
						$bodytag		= 	str_replace("{order_id}",$order_id, $bodytag);
						$bodytag		= 	str_replace("{invoice_number}",$invoice_number, $bodytag);
						$bodytag		= 	str_replace("{order_number}",$order_number, $bodytag);
						$bodytag		= 	str_replace("{ordered_date}",$ordered_date, $bodytag);
						$bodytag		= 	str_replace("{ordertotal}",$ordertotal, $bodytag);
						$bodytag		= 	str_replace("{order_details}",$ord_details, $bodytag);
						$bodytag		= 	str_replace("{payment_method}",$payment_method, $bodytag);
						$bodytag		= 	str_replace("{order_vat_tax}",$order_vat, $bodytag);
						$bodytag		= 	str_replace("{shipping}",$shipping, $bodytag);
						$bodytag		= 	str_replace("{discount}",$discount, $bodytag);
						$bodytag		= 	str_replace("{courier_id}",$couriername, $bodytag);
						if($courierurl<>""){
							$courier_url	=	'<a href="'.$courierurl.'" target="_blank">'.$courierurl.'</a>';
						}
						$bodytag		= 	str_replace("{courier_url}",$courier_url, $bodytag);
						$bodytag		= 	str_replace("{tracking_info}",$tracking_info, $bodytag);

						$bodytag		= 	str_replace("{billing_title}",$billing_title, $bodytag);
						$bodytag		= 	str_replace("{billing_fname}",$billing_fname, $bodytag);
						$bodytag		= 	str_replace("{billing_lname}",$billing_lname, $bodytag);
						$bodytag		= 	str_replace("{billing_address1}",$billing_addr1, $bodytag);
						$bodytag		= 	str_replace("{billing_address2}",$billing_addr2, $bodytag);
						$bodytag		= 	str_replace("{billing_city}",$billing_city, $bodytag);
						$bodytag		= 	str_replace("{billing_state}",$billing_state, $bodytag);
						$bodytag		= 	str_replace("{billing_country}",$billing_country, $bodytag);
						$bodytag		= 	str_replace("{billing_postcode}",$billing_postcode, $bodytag);
						$bodytag		= 	str_replace("{billing_phone}",$billing_phone, $bodytag);

						$bodytag		= 	str_replace("{shipping_title}",$shipping_title, $bodytag);
						$bodytag		= 	str_replace("{shipping_fname}",$shipping_fname, $bodytag);
						$bodytag		= 	str_replace("{shipping_lname}",$shipping_lname, $bodytag);
						$bodytag		= 	str_replace("{shipping_address1}",$shipping_addr1, $bodytag);
						$bodytag		= 	str_replace("{shipping_address2}",$shipping_addr2, $bodytag);
						$bodytag		= 	str_replace("{shipping_city}",$shipping_city, $bodytag);
						$bodytag		= 	str_replace("{shipping_state}",$shipping_state, $bodytag);
						$bodytag		= 	str_replace("{shipping_country}",$shipping_country, $bodytag);
						$bodytag		= 	str_replace("{shipping_postcode}",$shipping_postcode, $bodytag);
						$bodytag		= 	str_replace("{shipping_phone}",$shipping_phone, $bodytag);
						$bodytag		= 	str_replace("{LINK HERE}",$link_here, $bodytag);
						$bodytag		=	nl2br($bodytag);
					}
					if($text !=''){
						$texttag		= 	str_replace("{fname}",$customer_fname, $text);
						$texttag		= 	str_replace("{lname}",$customer_lname, $texttag);
						$texttag		= 	str_replace("{email}",$customer_email, $texttag);
						$texttag		= 	str_replace("{order_id}",$order_id, $texttag);
						$texttag		= 	str_replace("{order_number}",$order_number, $texttag);
						$texttag		= 	str_replace("{invoice_number}",$invoice_number, $texttag);
						$texttag		= 	str_replace("{ordered_date}",$ordered_date, $texttag);
						$texttag		= 	str_replace("{ordertotal}",str_replace("&pound;","GBP ",$ordertotal), $texttag);
						$texttag		= 	str_replace("{order_details}",$txt_details, $texttag);
						$texttag		= 	str_replace("{payment_method}",$payment_method, $texttag);
						$texttag		= 	str_replace("{order_vat_tax}",str_replace("&pound;","GBP ",$order_vat), $texttag);
						$texttag		= 	str_replace("{shipping}",str_replace("&pound;","GBP ",$shipping), $texttag);
						$texttag		= 	str_replace("{discount}",str_replace("&pound;","GBP ",$discount), $texttag);
						$texttag		= 	str_replace("{courier_id}",$couriername, $texttag);
						$texttag		= 	str_replace("{courier_url}",$courier_url, $texttag);
						$texttag		= 	str_replace("{tracking_info}",$tracking_info, $texttag);

						$texttag		= 	str_replace("{billing_title}",$billing_title, $texttag);
						$texttag		= 	str_replace("{billing_fname}",$billing_fname, $texttag);
						$texttag		= 	str_replace("{billing_lname}",$billing_lname, $texttag);
						$texttag		= 	str_replace("{billing_address1}",$billing_addr1, $texttag);
						$texttag		= 	str_replace("{billing_address2}",$billing_addr2, $texttag);
						$texttag		= 	str_replace("{billing_city}",$billing_city, $texttag);
						$texttag		= 	str_replace("{billing_state}",$billing_state, $texttag);
						$texttag		= 	str_replace("{billing_country}",$billing_country, $texttag);
						$texttag		= 	str_replace("{billing_postcode}",$billing_postcode, $texttag);
						$texttag		= 	str_replace("{billing_phone}",$billing_phone, $texttag);

						$texttag		= 	str_replace("{shipping_title}",$shipping_title, $texttag);
						$texttag		= 	str_replace("{shipping_fname}",$shipping_fname, $texttag);
						$texttag		= 	str_replace("{shipping_lname}",$shipping_lname, $texttag);
						$texttag		= 	str_replace("{shipping_address1}",$shipping_addr1, $texttag);
						$texttag		= 	str_replace("{shipping_address2}",$shipping_addr2, $texttag);
						$texttag		= 	str_replace("{shipping_city}",$shipping_city, $texttag);
						$texttag		= 	str_replace("{shipping_state}",$shipping_state, $texttag);
						$texttag		= 	str_replace("{shipping_country}",$shipping_country, $texttag);
						$texttag		= 	str_replace("{shipping_postcode}",$shipping_postcode, $texttag);
						$texttag		= 	str_replace("{shipping_phone}",$shipping_phone, $texttag);
						$texttag		= 	str_replace("{LINK HERE}",$link_here, $texttag);
					}
					/* OLD CODE
					if($body !='' || $text !=''){

						$headers  = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						if($towhom=='Admin'){
							$userArr		=	array();
							$pos			=	strpos($row['user_id'], ',');
							if($pos>0){
								$userArr	=	explode(",",substr($row['user_id'],0,-1));	
							}else{
								$userArr[0]	=	$row['user_id'];
							}	
							$cnt			=	count($userArr);
							for($k=0;$k<$cnt;$k++){
								$user_id		=	$this->_db->fetchRow("SELECT `user_email` FROM 3bit_users WHERE user_id ='".$userArr[$k]."' ");
								if($user_id){									
									$send_email	=	$user_id['user_email'];
									mail($send_email,$subtag,$bodytag,$headers);
								}
							}
						}else{
							$send_email	=	$customer_email;
							mail($send_email,$subtag,$bodytag,$headers);
						}						
					}
					*/
					if($body !='' || $text !=''){
						if($towhom=='Admin'){
							$userArr		=	array();
							$pos			=	strpos($row['user_id'], ',');
							if($pos>0){
								$userArr	=	explode(",",substr($row['user_id'],0,-1));	
							}else{
								$userArr[0]	=	$row['user_id'];
							}	
							$cnt			=	count($userArr);
							for($k=0;$k<$cnt;$k++){
								$user_id		=	$this->_db->fetchAll("SELECT `user_email` FROM 3bit_users WHERE user_id ='".$userArr[$k]."' ");
								if(count($user_id)>0){									
									$send_email[]	=	$user_id[0]['user_email'];
								}
							}
						}else{
							$send_email	=	array('0'=>$customer_email);
						}

						require_once $_SERVER['DOCUMENT_ROOT'].'/logincms/ext/Swift/lib/swift_required.php';
						//SEND MAIL USING SWIFT LIBRARY
						$message = Swift_Message::newInstance()
						->setCharset('iso-8859-2')
						//Give the message a subject
						->setSubject($subtag)
						//Set the From address with an associative array
						->setFrom(array('hq@palliativedrugs.com' => 'Palliative Drugs'))
						//Set the To addresses with an associative array
						->setTo($send_email)
						//Set the bcc address
						->setBcc(array('jat@3bit.co.uk'))
						//Give it a body
						->setBody($texttag)
						//And optionally an alternative body
						->addPart($bodytag, 'text/html');
						$transport = Swift_MailTransport::newInstance();
						$mailer = Swift_Mailer::newInstance($transport);
						$result = $mailer->send($message);
					}
				}
			}
		}
	}


	public function adminhistory($id)
	{
		$userid	 =	$id;

		$selMax		=	$this->_db->fetchRow("SELECT date(max(history_date))as hdate FROM 3bit_order_history WHERE 1 ");
		if($selMax){	
			$maxdate	=	$selMax['hdate'];
		}

		$selHis =  $this->_db->fetchAll("SELECT *,date_format(history_date,'%D %M')as hisdate FROM 3bit_order_history WHERE user_id =	'".$userid."' AND ((history_date BETWEEN  date_sub( '".$maxdate."' , INTERVAL 7 DAY ) AND '".$maxdate."' ) OR date_format(history_date,'%Y-%m-%d') = '".$maxdate."') ORDER BY history_date DESC LIMIT 0,10");
		
		
		$selAd	=	$this->_db->fetchRow("SELECT concat(user_firstname,' ',user_lastname)as name FROM 3bit_users WHERE user_id ='".$userid."'");
		if($selAd){
			$username	=	$selAd['name'];
		}
		
		if($selHis){		
			$hisList	=	'<h3>Recent Activity: '.ucfirst($username).'</h3>';			
			$hisList	.=	'<table cellpadding="0" cellspacing="0" width="100%" class="display">
							<tr>
								<th>Date</th>
								<th>Status</th>								
							</tr>';
			$i=0;			
			foreach($selHis as $row){
				$i++;
    		 	if($i%2 == 0)
    				$bgcolor = "#EDEDED";
				else
			 		$bgcolor = "#FFFFFF";
				
				$hisList	.=	'<tr bgcolor="'.$bgcolor.'">
									<td>'.$row['hisdate'].'</td>
									<td>'.stripslashes($row['status']).'</td>									
								 </tr>';			
			}
			$hisList	.=	'</table>';
		}
		return $hisList;
	}

	public function orderhistory($id)
	{
		$user_name	= $_SESSION['wlsession']['fullname'];	
		
		$sql			=	"SELECT o.*,o.status as ostatus,u.customer_id,u.email as cemail,ca.*,c.* FROM 3bit_orders o,3bit_customers u,3bit_country c,3bit_customers_address ca WHERE o.customer_id=u.customer_id AND o.order_id='".$id."' AND ca.address_id = o.address_id AND c.id=u.country ORDER BY o.order_id DESC";		

		$printid 		=	$id;
		$rows 			=	$this->_db->fetchRow($sql);
		$customer_id	=	$rows['customer_id'];	
		$address_id		=	$rows['address_id'];	
		$invoice_number	=	$rows['invoice_number'];
		$status			=	$rows['ostatus'];
		$datearray		=	explode(" ", $rows["ordered_date"]);
		$dot			=	explode("-",$datearray[0]);
		$dat 			=	$dot[2]."-".$dot[1]."-".$dot[0];
		$tot			=	explode(":",$datearray[1]);
		$tim 			=	$tot[0]." ".$tot[1];

		$selCustAdd		=	$this->_db->fetchRow("SELECT a.`address_id`,a.`customer_id`,a.shipping_country,c.is_eu FROM 3bit_customers_address a, 3bit_country c WHERE a.customer_id ='".$customer_id."' AND  a.address_id='".$address_id."' AND a.shipping_country=c.id ");	
		
		if($selCustAdd){


		$history	=	'<table width="100%" border="0" cellspacing="0" cellpadding="0" id="customer">
						  <tbody>';

		if($status == 'Shipped'){
			$history	.=	'<tr>
							  <td colspan="2"><h3>Invoice</h3><b>Invoice Number:</b> '.$invoice_number.'</td>
							</tr>';           
		 }
		$history	.=		'<tr>
							  <td><h4>Invoice To:</h4>
							  '.ucfirst($rows['billing_fname'])."&nbsp;".ucfirst($rows['billing_lname'])."<br>";
								 if($rows['billing_address1'] != "") 
									 $history	.=	$rows['billing_address1']."<br>";
								 if($rows['billing_address2'] != "") 
									 $history	.=	$rows['billing_address2']."<br>";
								 if($rows['billing_city'] != "") 
									 $history	.=	$rows['billing_city']."<br>";
								 if($rows['billing_country'] != "") 
									 $history	.=	$rows['country']."<br>";
								 if($rows['billing_postcode']!="")
									 $history	.=	"(".$rows['billing_postcode'].")<br>";
								 if($rows['billing_phone'] != "") 
									 $history	.=	"<b>Telephone:</b>".$rows['billing_phone']."<br>"; 
								 if($rows['cemail'] != "") 
									 $history	.=	"<b>Email:</b>".$rows['cemail']."<br>"; 	
								
			$history	.=	'</td><td><h4>Delivery To:</h4>
							'.ucfirst($rows['shipping_fname'])."&nbsp;".ucfirst($rows['shipping_lname'])."<br>";
								if($rows['shipping_address1'] != "") 
									 $history	.=	$rows['shipping_address1']."<br>";
								 if($rows['shipping_address2'] != "") 
									 $history	.=	$rows['shipping_address2']."<br>";
								 if($rows['shipping_city'] != "") 
									 $history	.=	$rows['shipping_city']."<br>";
								 if($rows['shipping_country'] != ""){
									 $selCntry	=	$this->_db->fetchRow("SELECT country FROM 3bit_country WHERE id='".$rows['shipping_country']."'");
									if($selCntry){											
										$shipcountry=	$selCntry['country'];
										$history	.=	$shipcountry."<br>";
									}
								 }
								 if($rows['shipping_postcode']!="")
									 $history	.=	"(".$rows['shipping_postcode'].")<br>";
								 if($rows['shipping_phone'] != "") 
									 $history	.=	"<b>Telephone:</b>".$rows['shipping_phone'].'<br> 	
							  <td>
							  </td>
						</tbody>
						</table>';		
		 $history	.=	'<table width="100%" border="0" cellspacing="0" cellpadding="0" id="orderdetails">
							<thead>
							<tr> 
								<td><h3>Order Details:</h3></td>
											  
							</tr>
							</thead>
							<tbody>
							<tr> 
								<td valign="top">
									<b>Invoice Date:</b>'.$dat.'<br/>
									<b>Payment Method:</b>'.$rows["payment_method"].'<br/>
									<b>Order Number:</b>'.$rows["order_id"].'<br/>';
		if(trim($rows["special_instruction"])<>""){
			$history	.=			'<b>Any Special Instructions for delivery:</b>'.stripslashes($rows["special_instruction"]).'<br/>';
		}
		$history	.=			'</td>							 
								 <td valign="top">
									<b>Payment Status:</b>'.$rows["payment_status"].'<br/>
									<b>Currency:</b> GBP<br/>
								 </td>
							</tr>
							</tbody>
						</table>
						<table width="100%" border="0" class="display">									
							<thead>
							<tr class="invheaders"> 
								<th class="txtar">Description</th>
								<th>Quantity</th>
								<th>Unit Price</th>             
								<th>Total</th>                
							</tr>
							</thead>							
								';
							
		$sql_order 		= "SELECT * FROM 3bit_order_details WHERE order_id='".$rows['order_id']."'";
		$res_order  	= $this->_db->fetchAll($sql_order);		
		$sub_total		= 0;
		foreach($res_order as $rows_order){
			$product_name_var =	'';
			if(!empty($rows_order['product_id'])){
				$selProd	=	$this->_db->fetchAll("select p.* from 3bit_products p WHERE p.product_id='".$rows_order['product_id']."'  ");							
				if($selProd){																				
					foreach($selProd as $res){
						$product_id		=	$res['product_id'];	
						$product_name 	= 	$res['product_name'];
						$product_code	=	$res['product_code'];
						$product_qty	=	$rows_order['quantity'];										
						if($res['size'] != '') 
						  $product_name_var = $res['size'];									
						else if($res['style'] != '') 
						  $product_name_var = $res['style'];									
					}
				}
			}

			// updated on 18/11/2010
			$product_price	=	$rows_order['price'];
			$product_price	=	number_format($product_price, 2, '.', '');	
			$line_price		=	$rows_order['quantity']*$product_price;
			$line_price		=	number_format($line_price, 2, '.', '');	
			$sub_total		+=	$line_price;
			$sub_total		=	number_format($sub_total, 2, '.', '');
			$vatprice		=	$rows['order_vat_tax'];
			$history	.=	'<tr>
							  <td>'.$product_name;
							  if($product_name_var != "") 
								  $history	.= " ( ".$product_name_var." ) ";
			$history	.=	' </td>
							  <td align="center">'.$rows_order['quantity'].'</td>
							  <td align="right">&pound;'.$product_price.'</td>
							  <td align="right">&pound;';
							  if($line_price != "") 
								  $history	.=	$line_price;
			$history	.=	'</td>
							</tr>';
		}
		$db_query_intl 	= "SELECT * FROM 3bit_order_details where order_id='".$rows['order_id']."' ";			
		$db_result_intl = $this->_db->fetchAll($db_query_intl);
		foreach($db_result_intl as $dsdet){
			$discount	= $dsdet['discount'];
			$shipping	= $dsdet['shipping'];	
		}
		$total 			= $rows['ordertotal'];
		$discount		= number_format($discount, 2, '.', '');	
		$shipping 		= number_format($shipping, 2, '.', '');
		$item_vat		= $rows['order_vat_tax'];
		$item_vat		= number_format($item_vat, 2, '.', '');
		$disp_total		= number_format($sub_total, 2, '.', '');
		$sub_total		= ($sub_total+$shipping)-$discount;		
		$sub_total		= number_format($sub_total, 2, '.', '');
									                   
		$history	.=	'  <tr>
							  <td></td>
							  <td colspan="3"><hr size="1"/></td>
							</tr>
							<tr>
							  <td></td>
							  <td colspan="2"><b>Subtotal</b></td>
							  <td align="right">&pound;'.$disp_total.'</td>
							</tr>
							<tr>
							  <td></td>
							  <td colspan="2"><b>Shipping</b></td>
							  <td align="right">&pound;'.$shipping.'</td>
							</tr>
							<tr>
							  <td></td>
							  <td colspan="2"><b>Tax (Inc)</b></td>
							  <td align="right">&pound;'.$item_vat.'</td>
							</tr>
							<tr>
							  <td></td>
							  <td colspan="2"><b>Discount</b></td>
							  <td align="right">&pound;'.$discount.'</td>
							</tr>
							<tr>
							  <td/>
							  <td colspan="3"><hr size="1"/></td>
							</tr>
							<tr>
							  <td/>
							  <td colspan="2"><b>Total</b></td>
							  <td align="right">&pound;'.number_format($total, 2, '.', '').'</td>
							</tr>					
						  </tbody>
						</table>		  
						<table cellpadding="0" cellspacing="0" width="100%" border="0" class="display">
						<thead>
						<tr>
							<td colspan="3"><b>History</b></td>
						</tr>
						</thead>
						<tbody>';
		$selHis	=	$this->_db->fetchAll("SELECT *,date_format(history_date,'%D %b %Y') as hdate,date_format(history_date,'%H:%i')as htime FROM  3bit_order_history WHERE (order_id = '".$id."' or order_id=(select order_id from 3bit_credit_details WHERE refund_id='".$id."' GROUP BY refund_id))");
		if($selHis){		
			$k	=	0;			
			foreach($selHis as $row){			
				$k++;
				if($k%2 == 0)
					$bgcolor = "#EDEDED";
				else
					$bgcolor = "#FFFFFF";								
					
				$history	.=	'<tr bgcolor="'.$bgcolor.'">
							<td>'.$row['hdate'].'&nbsp;@&nbsp;'.$row['htime'].'</td>
							<td colspan="2">'.$row['status'].'</td>
						<tr>';
		 	}	
		}
		
		$history	.=	'</tbody></table>';	  
		 }
		return $history;
	}

	public function add_history($data){
		$orderid	=	$data['orderid'];		
		$value		=	$data['txtval'];
		$susername	=	$data['susername'];
		$suserid	=	$data['suserid'];

		if($value <>""){

			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$orderid."' rel='facebox'>order #".$orderid."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$suserid."' rel='facebox'>".$susername."</a>";

			$text	=	"".addslashes($adminurl)." added a note to ".addslashes($orderurl)." -";

			$text	.=	$value;
		
			$this->_db->query("INSERT INTO 3bit_order_history SET order_id='".$orderid."',status = '".$text."' ,user_id='".$_SESSION['wlsession']['user_id']."',history_date=now() ");
			
			$retList	=	'<table cellpadding="0" cellspacing="0" width="100%" border="0" class="display">
								<thead>
								<tr>
									<td colspan="3"><b>History</b></td>
								</tr>
								</thead>
								<tbody>
								<tr>		
									<td colspan="3"><input type="text" name="txtHis'.$orderid.'" id="txtHis'.$orderid.'"  style="width:300px;"  />&nbsp;<a class="button" onclick=\'add_history("'.$orderid.'",document.getElementById("txtHis'.$orderid.'").value,"'.$susername.'","'.$suserid.'")\'><img src="'.$this->_baseurl.'/public/images/icons/comment_add.png" alt="Add History" title="Add History"/>Add History</a></td>
								</tr>';
				 
					$selHis	=	$this->_db->fetchAll("SELECT *,date_format(history_date,'%D %b %Y') as hdate,date_format(history_date,'%H:%i')as htime FROM  3bit_order_history WHERE (order_id = '".$orderid."' or order_id=(select order_id from 3bit_credit_details WHERE refund_id='".$orderid."' GROUP BY refund_id))");
					if($selHis){
						$i	=	0;					
						
						foreach($selHis as $row){								
							$i++;
							if($i%2 == 0)
								$bgcolor = "#EDEDED";
							else
								$bgcolor = "#FFFFFF";
				
							$retList	.=	'<tr bgcolor="'.$bgcolor.'">
										<td>'.$row['hdate'].'&nbsp;@&nbsp;'.$row['htime'].'</td>
										<td colspan="2">'.$row['status'].'</td>
									<tr>';
						}	
					}

			$retList	.=	'</tbody></table>';		
			die($retList."^"."S");
		}else{
			die($retList."^"."F");
		}

	}
	public function customerhistory($custid)
	{
		$selName =  $this->_db->fetchRow("SELECT fname,lname FROM 3bit_customers WHERE customer_id =	'".$custid."'");
		
		if($selName){
		
			$fname	=	$selName['fname'];
			
			$lname	=	$selName['lname'];
			
		}

		$selMax		=	$this->_db->fetchRow("SELECT date(max(history_date))as odate FROM 3bit_order_history WHERE 1 ");

		if($selMax){
	
			$maxdate	=	$selMax['odate'];

		}

		$hisList	=	'';

		$selHis	=	$this->_db->fetchAll("SELECT * FROM 3bit_orders WHERE customer_id ='".$custid."' AND ((ordered_date BETWEEN  date_sub( '".$maxdate."' , INTERVAL 7 DAY ) AND '".$maxdate."' ) OR date_format(ordered_date,'%Y-%m-%d') = '".$maxdate."')");
		
		if($selHis){
		
			$hisList	=	'<h3>Customer History for '.ucfirst($fname).'&nbsp;'.ucfirst($lname).'</h3>';
			
			$hisList	.=	'<table cellpadding="0" cellspacing="0" width="100%" class="display">
							<tr>
								<th>Order Number</th>
								<th>Status</th>
								<th>Total Amount</th>								
								<th>Action</th>
							</tr>';
				$i=0;
			
			foreach($selHis as $row){
				$i++;
    		 	if($i%2 == 0)
    				$bgcolor = "#EDEDED";
				else
			 		$bgcolor = "#FFFFFF";
				
				$hisList	.=	'<tr bgcolor="'.$bgcolor.'">
									<td>'.$row['order_id'].'</td>
									<td>'.$row['status'].'</td>
									<td>'.$row['ordertotal'].'</td>
									<td><a href="'.$this->_baseurl.'/orders/orderhistory/id/'.$row['order_id'].'" rel="facebox">View</a></td>
								 </tr>';
			
			}
			$hisList	.=	'<tr>		
								<td colspan="3">
									<input type="text" id="txtHis" name="txtHis" style="width:300px;"/>
								</td>
								<td>
									<input type="button" name="add" value="Add" onclick=\'addCHistory("'.$custid.'",document.getElementById("txtHis").value)\'/>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<fieldset>
										<legend>Customer Notes</legend>
											<span id="cust_history'.$custid.'"><table width="100%">
										';
			$selChis	=	$this->_db->fetchAll("SELECT *,date_format(history_date,'%D %b %Y') as hdate,date_format(history_date,'%H:%i')as htime FROM 3bit_order_history WHERE customer_id ='".$custid."' AND order_id='0' order by history_date DESC");	
				

			if($selChis){
				$i	=	0;									
				foreach($selChis as $row){								
					$i++;
					if($i%2 == 0)
						$bgcolor = "#EDEDED";
					else
						$bgcolor = "#FFFFFF";
				
					$hisList	.=	'<tr bgcolor="'.$bgcolor.'">
										<td>'.$row['hdate'].'&nbsp;@&nbsp;'.$row['htime'].'</td>
										<td colspan="2">'.$row['status'].'</td>
									<tr>';
				 }	
			}
			$hisList	.=	'			</table></span>
									</fieldset>
								<td>
							</tr>	
						</table>';

		}

		return $hisList;

	}

	// Function to calculate VAT for a individual product 

	public function findProductvat($productid){

		$selProd	=	$this->_db->fetchAll("SELECT price,vat_id FROM 3bit_products WHERE product_id='".$productid."'");

		if(count($selProd)>0){

			foreach($selProd as $srow){

				$price_site	=	$srow['price'];			

				$vatid		=	$srow['vat_id'];

				$selVat		=	$this->_db->fetchRow("SELECT vat_rate FROM 3bit_product_vats WHERE vat_id='".$vatid."'");

				if(count($selVat)>0){
					$vatamt			= 	$selVat['vat_rate'];
				}
				$vatper			=	($vatamt/100);

				$pricewovat		=	$price_site/(1+$vatper);

				$vatprice		=	($price_site-$pricewovat);
						
			}		
		}	 
		return $vatprice;
	}

	// Function to get price with vat amount for product

	public function findProductprice($productid){
		
		$selPrice	=	$this->_db->fetchAll("SELECT price,sale_price FROM 3bit_products WHERE product_id='".$productid."'");

		if(count($selPrice)>0){
			
			foreach($selPrice as $row){
				
				$product_price	=	$row['price'];	
				
				$sale_price		=	$row['sale_price'];

				if($sale_price>0)
					 $product_price	=	$sale_price;

				$product_price	=	number_format($product_price, 2, '.', '');

			}
		}
		return $product_price;
	}

	public function updatePDF($frontuserid)
	{
		$selName		=	$this->_db->fetchRow("SELECT CONCAT(FirstName,' ',LastName)as name FROM new_users WHERE id='".$frontuserid."'");

		if(count($selName)>0){

			$doc_root	=	$_SERVER['DOCUMENT_ROOT'];

			include("$doc_root/mpdf/mpdf.php");	

			$mpdf		=	new mPDF('c','A4'); 

			$mpdf->SetImportUse();
	
			// forces no subsetting - otherwise the inserted characters may not be contained in a subset font
			$mpdf->percentSubset = 0;   
	
			$mpdf->debug = true;
			
			$mpdf->encrypted = true;
			$mpdf->useRC128encryption = 1;
			$mpdf->uniqid = 'fca9946d632b798439df8b446c745470' ;
			$mpdf->Ovalue = '�Qp��Q��ʩ� xH� j*+�ܻ(ْJ��' ;
			$mpdf->encryption_key = '����^��,k)�' ;
			$mpdf->Uvalue = 'ymq&/zP�<��Z�' ;
			$mpdf->Pvalue = '4294964448' ;

			
			// forces no subsetting - otherwise the inserted characters may not be contained in a subset font
			$mpdf->percentSubset = 0;   

			$name			=	stripslashes($selName['name']);
		
			$search			=	array('Licensed to Jatinder Hanspal');

			$replacement	=	array("Licensed to ".$name);

			$mpdf->OverWrite($doc_root.'/formulary-pdfs/PCF4+.pdf', $search, $replacement, 'F', $doc_root.'/formulary-pdfs/PCF4+_SEP_2012_'.$frontuserid.'.pdf' ) ;
		}
	}

	public function resetdownload(){

			$sql = "UPDATE `3bit_pdf_download` SET ordered_date =now(),downloaded_count=0 WHERE download_id = '".$_POST['downloadid']."'";
			$this->_db->query($sql);
			die("S");

	}

	public function resendlink(){

		$flag	=	"F";

		$data	=	$this->_db->fetchRow("SELECT o.order_id,o.status as ostatus,DATE_FORMAT(o.ordered_date,'%d-%m-%Y')as dat, DATE_FORMAT(o.ordered_date,'%h:%i %p')as tim ,u.customer_id,u.email as cemail,u.fname,u.lname,d.frontuser_id FROM 3bit_orders o,3bit_customers u,country c,3bit_customers_address ca,3bit_pdf_download d WHERE o.customer_id=u.customer_id AND o.order_id=d.order_id AND d.download_id='".$_POST['downloadid']."' AND ca.address_id = o.address_id and c.id=u.country order by o.order_id DESC");

		if($data){
			$order_id				=		$data['order_id'];
			$ostatus				=		$data['ostatus'];
			$order_date			=		$data['dat'];
			// To get customer details --------------------------------------------------
			$customer_fname	=		ucfirst($data['fname']);
			$customer_lname	=		ucfirst($data['lname']);
			$customer_email	=		$data['cemail'];
			$frontuser_id		=		$data['frontuser_id'];
			$base64					=		base64_encode($order_id."##".$frontuser_id);
			$pdflink				=		"http://".$_SERVER['HTTP_HOST']."/download-pdf/".$base64.'.html';
			
			//SEND CONFIRMATION MAIL TO USER
			$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='Ebook Download' AND message_to = 'Customer' AND active='1' and email_status = '1'";
			$res						= 	$this->_db->fetchAll($sql_select);
			if(count($res)>0){
				foreach($res as $row1){				
						$body					= 	stripslashes($row1['message_text']);	
						$text					=		stripslashes($row1['text']);	
						$subject			=		stripslashes($row1['mail_subject']);
						$subject			= 	str_replace("{ordered_date}",$order_date, $subject);

						$bodytag			= 	str_replace("{fname}",$customer_fname, $body);
						$bodytag			= 	str_replace("{lname}",$customer_lname, $bodytag);
						$bodytag			= 	str_replace("{order_id}",$order_id, $bodytag);
						$bodytag			= 	str_replace("{ordered_date}",$order_date, $bodytag);			
						$bodytag			= 	str_replace("{LINK HERE}",$pdflink, $bodytag);
						$bodytag			=		nl2br($bodytag);

						$texttag			= 	str_replace("{fname}",$cust_fname, $text);
						$texttag			= 	str_replace("{lname}",$cust_lname, $texttag);
						$texttag			= 	str_replace("{order_id}",$order_id, $texttag);
						$texttag			= 	str_replace("{ordered_date}",$order_date, $texttag);				
						$texttag			= 	str_replace("{LINK HERE}",$pdflink, $texttag);
						
						require_once $_SERVER['DOCUMENT_ROOT'].'/logincms/ext/Swift/lib/swift_required.php';

						//SEND MAIL USING SWIFT LIBRARY
						$message = Swift_Message::newInstance()
						->setCharset('iso-8859-2')
						//Give the message a subject
						->setSubject($subject)
						//Set the From address with an associative array
						->setFrom(array('hq@palliativedrugs.com' => 'Palliative Drugs'))
						//Set the To addresses with an associative array
						->setTo(array($customer_email))
						//Give it a body
						->setBody($texttag)
						//And optionally an alternative body
						->addPart($bodytag, 'text/html');
						$transport = Swift_MailTransport::newInstance();
						$mailer = Swift_Mailer::newInstance($transport);
						if($mailer->send($message)){
							$flag	=	"S";
						}
				}
			}
		}

		die($flag);

	}
	
	public function addorderaudit()
	{
			$orderid		=	$_POST['orderid'];

			$suserid		=	$_SESSION['wlsession']['user_id'];
			
			$susername	=	$_SESSION['wlsession']['fullname'];

			$orderurl		=	"<a href='".$this->_baseurl."/orders/orderhistory/id/".$orderid."' rel='facebox'>order #".$orderid."</a>";

			$adminurl		=	"<a href='".$this->_baseurl."/orders/adminhistory/id/".$suserid."' rel='facebox'>".$susername."</a>";

			$text				=	"".addslashes($adminurl)." prints invoice ".addslashes($orderurl).' <span class="dashdate">'.date('Y/m/d H:i:s').'</span>';			
		
			$insert			=	$this->_db->query("INSERT INTO 3bit_order_history SET order_id='".$orderid."',status = '".$text."' ,user_id='".$_SESSION['wlsession']['user_id']."',history_date=now() ");

			if($insert)
				die("S");
			else
				die("F");
	}

	public function getAddress($type,$order_id)
	{

		if($type=='shipping'){
				$data	=		$this->_db->fetchRow("SELECT o.order_id,o.order_number,ca.address_id,ca.email,ca.shipping_title as title,ca.shipping_fname as fname,ca.shipping_lname as lname,ca.shipping_address1 as address1,ca.shipping_address2 as address2,ca.shipping_city as city,ca.shipping_state as state,ca.shipping_county as county,ca.shipping_country as country,ca.shipping_postcode as postcode,ca.shipping_phone as phone FROM 3bit_orders o,3bit_country c,3bit_customers_address ca WHERE o.order_id='".$order_id."' AND ca.address_id = o.address_id and c.id=ca.shipping_country order by o.order_id DESC");
		}else if($type=='billing'){
				$data	=		$this->_db->fetchRow("SELECT o.order_id,o.order_number,ca.address_id,ca.company,ca.email,ca.billing_title as title,ca.billing_fname as fname,ca.billing_lname as lname,ca.billing_address1 as address1,ca.billing_address2 as address2,ca.billing_city as city,ca.billing_state as state,ca.billing_county as county,ca.billing_country as country,ca.billing_postcode as postcode,ca.billing_phone as phone FROM 3bit_orders o,3bit_country c,3bit_customers_address ca WHERE o.order_id='".$order_id."' AND ca.address_id = o.address_id and c.id=ca.billing_country order by o.order_id DESC");
		}
		
		return $data;
	}

	public function getCountries()
    {
        $sql = "SELECT id,country FROM 3bit_country";
        if ($result = $this->_db->query($sql)) {
            return $result->fetchAll();
        }
    }

	public function updateaddress()
	{			
			if($_POST['type']=='shipping'){
				$this->_db->query("UPDATE 3bit_customers_address SET shipping_title		=	'".addslashes($_POST['title'])."',
																														 shipping_fname		=	'".addslashes($_POST['fname'])."',
																														 shipping_lname		=	'".addslashes($_POST['lname'])."',
																														 shipping_address1=	'".addslashes($_POST['address1'])."',
																														 shipping_address2=	'".addslashes($_POST['address2'])."',
																														 shipping_city		=	'".addslashes($_POST['city'])."',
																														 shipping_state		=	'".addslashes($_POST['state'])."',
																														 shipping_county	=	'".addslashes($_POST['county'])."',
																														 shipping_country	=	'".addslashes($_POST['country'])."',
																														 shipping_postcode=	'".addslashes($_POST['postcode'])."',
																														 shipping_phone		=	'".addslashes($_POST['phone'])."',
																														 modified_date		=	now() WHERE address_id='".$_POST['address_id']."'");
			}else if($_POST['type']=='billing'){
				$this->_db->query("UPDATE 3bit_orders SET order_number='".$_POST['order_number']."' WHERE order_id='".$_POST['order_id']."'");
				$this->_db->query("UPDATE 3bit_customers_address SET company					=	'".addslashes($_POST['company'])."',
																														 billing_title		=	'".addslashes($_POST['title'])."',
																														 billing_fname		=	'".addslashes($_POST['fname'])."',
																														 billing_lname		=	'".addslashes($_POST['lname'])."',
																														 billing_address1	=	'".addslashes($_POST['address1'])."',
																														 billing_address2	=	'".addslashes($_POST['address2'])."',
																														 billing_city			=	'".addslashes($_POST['city'])."',
																														 billing_state		=	'".addslashes($_POST['state'])."',
																														 billing_county		=	'".addslashes($_POST['county'])."',
																														 billing_country	=	'".addslashes($_POST['country'])."',
																														 billing_postcode	=	'".addslashes($_POST['postcode'])."',
																														 billing_phone		=	'".addslashes($_POST['phone'])."',
																														 modified_date		=	now() WHERE address_id='".$_POST['address_id']."'");
			}
	}

}

?>