<?php

class Application_Model_Audits
{
	private $_db;    

    public function __construct()
    {
        $this->_db = Zend_Registry::get("db");
    } 
	
	public function insert_audit($module, $action, $record_id, $action_status)
    { 

		if(!empty($_SESSION['wlsession']['user_id']))
			$logged_user = $_SESSION['wlsession']['user_id'];
		else
			$logged_user = '';

		$admin_name	=	$_SESSION['wlsession']['fullname'];

		if($action=='update' || $action=='delete' || $action=='archive' || $action=='restore')
			$process=substr($action,0,-1);
		else if($action=='active' || $action=='inactive')
			$process=substr($action,0,-1).'at';
		else
			$process=$action;
		
		if(is_numeric($record_id)){

			$his_module	=	str_replace("3bit_","",$module);

			switch($his_module){
				case "users":
					$fieldname	=	"concat(user_firstname,' ',user_lastname)";
					$pkey		=	"user_id";
				break;		
				case "user_types":				
					$fieldname	=	"title"; 
					$pkey		=	"type_id";
				break;
				case "categories":
					$fieldname	=	"cat_title";
					$pkey		=	"cat_id";
				break;
				case "articles":
					$fieldname	=	"article_title";
					$pkey		=	"article_id";
				break;
				case "article_template":
					$fieldname	=	"temp_filename";
					$pkey		=	"temp_id";
				break;	
				case "snippets":
					$fieldname	=	"snippet_title";
					$pkey		=	"snippet_id";
				break;	
				case "language":
					$fieldname	=	"language_name";
					$pkey		=	"language_id";
				break;	
				case "datasource":
					$fieldname	=	"title";
					$pkey		=	"datasource_id";
				break;
				case "fieldsets":
					$fieldname	=	"title";
					$pkey		=	"fieldset_id";
				break;
				case "formulary":
					$fieldname	=	"formulary_title";
					$pkey		=	"formulary_id";
				break;
				case "drugs":
					$fieldname	=	"drug_name";
					$pkey		=	"drug_id";
				break;
				case "formulary_snippets":
					$fieldname	=	"snippet_title";
					$pkey		=	"snippet_id";
				break;
				case "endnote_refs":
					$fieldname	=	"title"; 
					$pkey		=	"id";
				break;
				case "new_users":
					$fieldname	=	"concat(FirstName,' ',LastName)";
					$pkey		=	"id";
				break;
				case "news":
					$fieldname	=	"title"; 
					$pkey		=	"news_id";
				break;
				case "news_types":
					$fieldname	=	"type_name";
					$pkey		=	"typeid";
				break;
				case "assets":
					$fieldname	=	"asset_title";
					$pkey		=	"asset_id";
				break;
				case "asset_categories":
					$fieldname	=	"asset_category_title";
					$pkey		=	"asset_category_id";
				break;
				case "courier":
					$fieldname	=	"courier_name";
					$pkey		=	"courier_id";
				break;
				case "product_vats":
					$fieldname	=	"vat_code";
					$pkey		=	"vat_id";
				break;
				case "country":
					$fieldname	=	"country";
					$pkey		=	"id";
				break;
				case "messages":
					$fieldname	=	"message_type";
					$pkey		=	"message_id";
				break;
				case "shipping":
					$fieldname	=	"weight";
					$pkey		=	"ship_id";
				break;
				case "product_category":
					$fieldname	=	"procat_title";
					$pkey		=	"procat_id";
				break;
				case "products":
					$fieldname	=	"product_name";
					$pkey		=	"product_id";
				break;
				case "customers":
					$fieldname	=	"concat(fname,' ',lname)";
					$pkey		=	"customer_id";
				break;
				case "sddb":
					$fieldname	=	"drug1_name";
					$pkey		=	"id";
				break;
				case "login":
					$fieldname	=	'';
					$pkey		=	'';
				break;
			}

			$module_title=	'';

			$module_title	=	$this->_db->fetchOne("SELECT $fieldname as fname FROM  $module WHERE $pkey = '".$record_id."'");		

			$history	=	$admin_name.' '.$process.'ed '.$his_module.' ID# '.$record_id.' '.$module_title.' <span class="dashdate">'.date('Y/m/d H:i:s').'</span>';		

			$this->_db->query("INSERT INTO 3bit_audit_history SET
									module				=	'".addslashes($module)."',
									action				=	'".addslashes($action)."',
									record_id			=	'".addslashes($record_id)."',
									action_status		=	'".addslashes($action_status)."',
									user_id				=	'".addslashes($logged_user)."',
									ip_address			=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
									browser				=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
									action_date			=	now(),
									audit_history		=	'".addslashes($history)."',
									status				=	'1',
									created_date		=	now(),
									modified_date		=	now()");

		}		
    } 	

	public function check_permission($module)
    {
		$getmodules	=	$this->_db->fetchRow("SELECT u.type_id,ut.module_permissions FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id AND u.user_id ='".$_SESSION['wlsession']['user_id']."'");
		if(count($getmodules)>0){
			$modules	=	explode(",",$getmodules['module_permissions']);
		}		
		//print_r($modules);
		if(in_array($module,$modules))
			return 1;
		else
			return 0;
	}


	/************ Function to remove special char and replace with dash **************/

	public function remSpecial($title) {	
		$title = strip_tags($title);
		// Preserve escaped octets.
		$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
		// Remove percent signs that are not part of an octet.
		$title = str_replace('%', '', $title);
		// Restore octets.
		$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

		if ($this->seems_utf8($title)) {
			if (function_exists('mb_strtolower')) {
				$title = mb_strtolower($title, 'UTF-8');
			}
			$title = $this->utf8_uri_encode($title, 200);
		}

		$title = strtolower($title);
		$title = preg_replace('/&.+?;/', '', $title); // kill entities
		$title = str_replace('.', '-', $title);
		$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
		$title = preg_replace('/\s+/', '-', $title);
		$title = preg_replace('|-+|', '-', $title);
		$title = trim($title, '-');
		return $title;
	}

	public function seems_utf8($str) {
		$length = strlen($str);
		for ($i=0; $i < $length; $i++) {
			$c = ord($str[$i]);
			if ($c < 0x80) $n = 0; # 0bbbbbbb
			elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
			elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
			elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
			elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
			elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
			else return false; # Does not match any model
			for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
				if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
					return false;
			}
		}
		return true;
	}

	public function utf8_uri_encode( $utf8_string, $length = 0 ) {
		$unicode = '';
		$values = array();
		$num_octets = 1;
		$unicode_length = 0;
		$string_length = strlen( $utf8_string );
		for ($i = 0; $i < $string_length; $i++ ) {

			$value = ord( $utf8_string[ $i ] );

			if ( $value < 128 ) {
				if ( $length && ( $unicode_length >= $length ) )
					break;
				$unicode .= chr($value);
				$unicode_length++;
			} else {
				if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

				$values[] = $value;

				if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
					break;
				if ( count( $values ) == $num_octets ) {
					if ($num_octets == 3) {
						$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
						$unicode_length += 9;
					} else {
						$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
						$unicode_length += 6;
					}

					$values = array();
					$num_octets = 1;
				}
			}
		}
		return $unicode;
	}

}

?>