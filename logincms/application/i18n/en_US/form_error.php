<?php defined('SYSPATH') or die('No direct script access.');

$lang = array
(
    'first_name' => array
    (
        'required' => 'Please enter first name.',               
        'default' => 'Invalid name.',
    ),

    'last_name' => array
    (
        'required' => 'Please enter last name.',      
        'default' => 'Invalid name.',
    ),

    'email' => array
    (
        'required' => 'Please enter email.',        
        'default' => 'Email is invalid.',
    ),
	'username' => array
    (
        'required' => 'Please enter username.',        
        'default' => 'Username is invalid.',
    ), 'password' => array
    (
        'required' => 'Please enter password.',        
        'default' => 'Password is invalid.',
    ),'level_id' => array
    (
        'required' => 'Please select level.',        
        'default' => 'Level is invalid.',
    ),'dept_id' => array
    (
        'required' => 'Please select department.',        
        'default' => 'Level is invalid.',
    ),
);
