<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

	protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
		$view->setEscape('stripslashes');
        $view->doctype('XHTML1_STRICT');
    }
	
	/**
     * Dbconnection
     */
	protected function _initDb()
	{
		if ($this->hasPluginResource("db")) {
		  $dbResource = $this->getPluginResource("db");
		  $db = $dbResource->getDbAdapter();
		  Zend_Registry::set("db", $db);
		}
	}

	/**
     * Init session
     */
	protected function _initUserSession()
	{		
		if(!empty($_POST["wlsession"]))
			$_COOKIE['PHPSESSID']	=  $_POST["wlsession"];	
		$ns	=	new Zend_Session_Namespace('wlsession');		
		Zend_Registry::set("ns", $ns);		
	}

    protected function checkSession()
    {
		$ns	=	Zend_Registry::get("ns");
        if (empty($ns->username)) {
            $this->_response->setRedirect('index')->sendResponse();
	    exit;
        }
    }

	protected function _initNavigation()
	{
		$this->bootstrap('layout');
		$layout		= $this->getResource('layout');
		$view		= $layout->getView();
		$navigation = new Zend_Navigation($this->getOption('navigation'));	
		
		$view->navigation($navigation);

		/*** To get base url for zend ***/
		$fc			= Zend_Controller_Front::getInstance();
		Zend_Registry::set("baseUrl", $fc->getBaseUrl());
		$baseurl	=	Zend_Registry::get("baseUrl");

		$db			=	Zend_Registry::get("db");

		$getmodules	=	$db->fetchRow("SELECT u.type_id,ut.module_permissions FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id AND u.user_id ='".$_SESSION['wlsession']['user_id']."'");

		if(count($getmodules)>0){
			$modules=	explode(",",$getmodules['module_permissions']);
		}

		$topnav		=	 array('Home'=> $baseurl.'/home');
		if(count($modules)>0){
			for($i=0;$i<count($modules);$i++){
				if($modules[$i]=='users'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/user.png\' alt=\'Users\' />Users'] =	$baseurl.'/'.$modules[$i];				
				}if($modules[$i]=='pages'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/document-text-image.png\' alt=\'Pages\' />Pages']	=	$baseurl.'/'.$modules[$i];
				}if($modules[$i]=='formulary'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/application-table.png\' alt=\'Formulary\' />Formulary'] =	$baseurl.'/'.$modules[$i];
				}if($modules[$i]=='members'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/users.png\' alt=\'Members\' />Members']	=	$baseurl.'/'.$modules[$i];
				}if($modules[$i]=='news'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/newspaper.png\' alt=\'News\' />News']		=	$baseurl.'/'.$modules[$i];
				}if($modules[$i]=='products'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/cart.png\' alt=\'Products\' />Products'] =	$baseurl.'/'.$modules[$i];			
				}if($modules[$i]=='customers'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/group.png\' alt=\'Customers\' />Customers'] =	$baseurl.'/'.$modules[$i];	
				}if($modules[$i]=='orders'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/creditcards.png\' alt=\'Orders\' />Orders'] =	$baseurl.'/'.$modules[$i];		
				}if($modules[$i]=='reports'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/report.png\' alt=\'Reports\' />Reports'] =	$baseurl.'/'.$modules[$i];						
				}if($modules[$i]=='assets'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/photos.png\' alt=\'Assets\' />Assets'] =	$baseurl.'/'.$modules[$i];				
				}if($modules[$i]=='sddb'){
					$topnav['<img src=\''.$baseurl.'/public/images/icons/page_white_database.png\' alt=\'Syringe Driver Database\' />Syringe Driver Database'] =	$baseurl.'/'.$modules[$i];				
				}
			}
		}else{
		   $topnav		=	 array('Home'=> $baseurl.'/home');
		}			

		$urlaction		=	'';

		if(trim($_SERVER['REQUEST_URI'])==$baseurl.'/home'){
			$urlvar		= 'home';

	    }else if(!empty($_SERVER['REQUEST_URI'])){
			$pathinfo	= $_SERVER['REQUEST_URI'];
			$exp		= explode("/",$pathinfo);					
			$urlvar		= $exp[2];
			if(!empty($exp[3]))
				$urlaction	= $exp[3];					
		}	

		$orders	=	$db->fetchAll("SELECT count(*) as count,o.status FROM 3bit_orders o,3bit_customers c WHERE o.customer_id=c.customer_id AND o.ccerrcode !='' GROUP BY o.status ORDER BY o.ordered_date DESC");
		$new_count	=	0;
		$acc_count	=	0;
		$dis_count	=	0;
		$ref_count	=	0;
		$can_count	=	0;
		if($orders){
			foreach($orders as $cnt){
				if(trim($cnt['status'])=='New')
					$new_count	= $cnt['count'];
				if(trim($cnt['status'])=='Accepted')
					$acc_count	= $cnt['count'];
				if(trim($cnt['status'])=='Shipped')
					$dis_count	= $cnt['count'];
				if(trim($cnt['status'])=='Refunded')
					$ref_count	= $cnt['count'];
				if(trim($cnt['status'])=='Rejected' || trim($cnt['status'])=='Archived')
					$can_count	+= $cnt['count'];
			}
		}	

		switch($urlvar){
			case "home":
				 $leftnav = 'Logged in user details.';
			break;
			case "users":
			case "usertypes":
				$leftnav = array
				(
					'Users'				=> "",
					'List Users'		=> $baseurl."/users",            
					'Add User'			=> $baseurl."/users/add",
					'User Types'		=> "",
					'List Types'		=> $baseurl."/usertypes",            
					'Add Type'			=> $baseurl."/usertypes/add"											          
				);
			break;
			case "pages":
			case "articles":
			case "templates":
			case "snippets":
			case "language":
			case "fieldsets":
			case "datasources":	
				$leftnav = array
				(
					'Pages'				=> "",
					'List Pages'		=> $baseurl."/pages",            
					'Add Page'			=> $baseurl."/pages/add", 
					'Articles'			=> "",           
					'List Articles'		=> $baseurl."/articles",            
					'Add Article'		=> $baseurl."/articles/add",
					'Templates'			=> "",
					'List Templates'	=> $baseurl."/templates",            
					'Add Template'		=> $baseurl."/templates/add",
					'Snippets'			=> "",
					'List Snippets'		=> $baseurl."/snippets",            
					'Add Snippet'		=> $baseurl."/snippets/add",
					'Languages'			=> "",
					'List Language'		=> $baseurl."/language",            
					'Add language'		=> $baseurl."/language/add",
					'Fieldsets'			=> "",           
					'List Fieldsets'	=> $baseurl."/fieldsets",            
					'Add Fieldset'		=> $baseurl."/fieldsets/add",
					'Datasources'		=> "",           
					'List Datasources'	=> $baseurl."/datasources",            
					'Add Datasource'	=> $baseurl."/datasources/add"	
				);
			break;
			case "formulary":
			case "pubmed":
			case "figures":	
			case "drugs":
				 $leftnav = array
				(
					'Formulary'			=> "",
					'List Formulary'	=> $baseurl."/formulary",            
					'Add Formulary'		=> $baseurl."/formulary/add",
					'List Links'		=> $baseurl."/formulary/links",               
					'Pubmed References'	=> "",
					'List Pubmed Reference'	=> $baseurl."/pubmed",            
					'Add Pubmed Reference'	=> $baseurl."/pubmed/pubmedrefadd", 
					'Add Manual Reference'	=> $baseurl."/pubmed/manualrefadd",
					'Figures'			=> "",
					'List Figures'		=> $baseurl."/figures",            
					'Add Figures'		=> $baseurl."/figures/add",
					'Supply Drugs'		=> "",           
					'List Supply Drugs'	=> $baseurl."/drugs",            
					'Add Supply Drugs'	=> $baseurl."/drugs/add"
				);
			break;
			case "members":
			case "rules":
			case "groups":
			case "invites":
				$leftnav = array
				(
					'Members'			=> "",
					'List Members'		=> $baseurl."/members",            
					'Add Members'		=> $baseurl."/members/add",            						   
					'Members Statistics'=> $baseurl."/members/statistics",      
				
					'Groups'			=> "",
					'List Groups'		=> $baseurl."/groups/",            
					'Add Groups'		=> $baseurl."/groups/add", 
					'Survey Invites'	=> "",
					'List Survey Invites'=> $baseurl."/invites",
					'Send Survey Invite'=> $baseurl."/invites/send"           						  

					/*'Rules'				=> "",
					'List Rules'		=> $baseurl."/rules",            
					'Add Rule'			=> $baseurl."/rules/add",
					'Survey Groups'		=> "",
					'List Survey Groups'=> $baseurl."/groups",
					'Add Survey Group'	=> $baseurl."/groups/add",
					'Survey Invites'	=> "",
					'List Survey Invites'=> $baseurl."/invites",
					'Send Survey Invite'=> $baseurl."/invites/send",
					'Survey Responses'	=> $baseurl."/invites/responses",*/
				);
			break; 
			case "news":
			case "newsarchives":
			case "newstypes":
				$leftnav = array
				(
					'News'				=> "",
					'List News'			=> $baseurl."/news",            
					'Add News'			=> $baseurl."/news/add",            										        
				);
			break;	
			case "products":
			case "productcat":
			case "stocks":
			case "shippings":
			case "messages":
			case "countries":
			case "vatrates":
			case "couriers":
				$leftnav = array
				(
					'Products'			=> "",
					'List Products'		=> $baseurl."/products",            
					'Add General Product'=> $baseurl."/products/generaladd",            					
					'Product Categories'=> "",
					'List Categories'	=> $baseurl."/productcat",            
					'Add Category'		=> $baseurl."/productcat/add",
					'Stocks'			=> "",
					'List Stocks'		=> $baseurl."/stocks",      
					'Shippings'			=> "",
					'List Shippings'	=> $baseurl."/shippings",      
					'Add Shipping'		=> $baseurl."/shippings/add",
					'Messages'			=> "",
					'List Message'		=> $baseurl."/messages",      
					'Add Message'		=> $baseurl."/messages/add",
					'Countries'			=> "",
					'List Countries'	=> $baseurl."/countries",      
					'Add Country'		=> $baseurl."/countries/add",
					'Vat Rates'			=> "",
					'List Vats'			=> $baseurl."/vatrates",      
					'Add Vat'			=> $baseurl."/vatrates/add",
					'Couriers'			=> "",
					'List Courier'		=> $baseurl."/couriers",      
					'Add Courier'		=> $baseurl."/couriers/add"					
				);
			break; 	
			case "customers":
				$leftnav = array
				(
					'Customers'			=> "",
					'List Customers'	=> $baseurl."/customers",            
				);
			break; 
			case "orders":
			case "manualorder":
				$leftnav = array
				(
					'Orders'							=> "",
					'New Orders ('.$new_count.')'		=> $baseurl."/orders/list/type/New",            
					'Accepted Orders ('.$acc_count.')'	=> $baseurl."/orders/list/type/Accepted",            
					'Dispatched Orders ('.$dis_count.')'=> $baseurl."/orders/list/type/Shipped",            
					'Refund Orders ('.$ref_count.')'	=> $baseurl."/orders/list/type/Refunded",            
					'Cancelled Orders ('.$can_count.')'	=> $baseurl."/orders/list/type/Cancelled",
					'Manage Downloads'				=> $baseurl."/orders/downloads",
					'Manual Order'						=> "",
					'Place New Order'					=> $baseurl."/manualorder"
				);
			break; 
			case "reports":
			case "searchorders":
			case "salesanalytics":
			case "portalstats":
			case "productsales":
				$leftnav = array
				(
					'Reports'			=> "",
					'Dashboard'			=> $baseurl."/reports",            
					'Search Orders'		=> $baseurl."/reports/searchorders",            
					'Sales Analytics'	=> $baseurl."/reports/salesanalytics", 
					'Portal Stats'		=> $baseurl."/reports/portalstats",   
					'Product Sales'		=> $baseurl."/reports/productsales",               
				);
			break; 
			case "assets":
			case "assetcat":
				$leftnav = array
				(
					'Assets'			=> "",
					'List Assets'		=> $baseurl."/assets",            
					'Add Asset'			=> $baseurl."/assets/add",            
					'Asset Categories'  => "",
					'List Categories'	=> $baseurl."/assetcat",            
					'Add Category'		=> $baseurl."/assetcat/add"                  
				);
			break; 		
			case "products":
			case "productcat":
			case "stocks":
			case "shippings":
			case "messages":
			case "countries":
			case "vatrates":
			case "couriers":
				$leftnav = array
				(
					'Products'			=> "",
					'List Products'		=> $baseurl."/products",            
					'Add General Product'=> $baseurl."/products/generaladd",            
					'Add Clothing Product'=> $baseurl."/products/clothingadd",          
					'Product Categories'=> "",
					'List Categories'	=> $baseurl."/productcat",            
					'Add Category'		=> $baseurl."/productcat/add",
					'Stocks'			=> "",
					'List Stocks'		=> $baseurl."/stocks",      
					'Shippings'			=> "",
					'List Shippings'	=> $baseurl."/shippings",      
					'Add Shipping'		=> $baseurl."/shippings/add",
					'Messages'			=> "",
					'List Message'		=> $baseurl."/messages",      
					'Add Message'		=> $baseurl."/messages/add",
					'Countries'			=> "",
					'List Countries'	=> $baseurl."/countries",      
					'Add Country'		=> $baseurl."/countries/add",
					'Vat Rates'			=> "",
					'List Vats'			=> $baseurl."/vatrates",      
					'Add Vat'			=> $baseurl."/vatrates/add",
					'Couriers'			=> "",
					'List Courier'		=> $baseurl."/couriers",      
					'Add Courier'		=> $baseurl."/couriers/add"					
				);
			break; 	
			case "customers":
				$leftnav = array
				(
					'Customers'			=> "",
					'List Customers'	=> $baseurl."/customers",            
				);
			break; 
			case "orders":
				$leftnav = array
				(
					'Orders'							=> "",
					'New Orders ('.$new_count.')'		=> $baseurl."/orders/list/type/New",            
					'Accepted Orders ('.$acc_count.')'	=> $baseurl."/orders/list/type/Accepted",            
					'Dispatched Orders ('.$dis_count.')'=> $baseurl."/orders/list/type/Shipped",            
					'Refund Orders ('.$ref_count.')'	=> $baseurl."/orders/list/type/Refunded",            
					'Cancelled Orders ('.$can_count.')'	=> $baseurl."/orders/list/type/Cancelled",            
				);
			break; 
			case "reports":
			case "searchorders":
			case "salesanalytics":
				$leftnav = array
				(
					'Reports'			=> "",
					'Dashboard'			=> $baseurl."/reports",            
					'Search Orders'		=> $baseurl."/reports/searchorders",            
					'Sales Analytics'	=> $baseurl."/reports/salesanalytics",            
				);
			break; 
			case "links":
			case "groups":
				$leftnav = array
				(
					'Links'			=> "",
					'List Links'	=> $baseurl."/links",            
					'Add Link'		=> $baseurl."/links/add",            
					'Groups'		=> "",
					'List Groups'	=> $baseurl."/groups",            
					'Add Group'		=> $baseurl."/groups/add",            
				);
			break; 	
			case "sddb":
				$leftnav = array
				(
					'SDDB'	=> "",
					'List Entries'				=> $baseurl."/sddb",       
					'Archived Entries'			=> $baseurl."/sddb/archives",       
					'Search Entry'				=> $baseurl."/sddb/search",
					'Create New Entry'			=> $baseurl."/sddb/add",            
					'Browse Index'				=> $baseurl."/sddb/browse",					
				);
			break;		
			default:
			   $leftnav = 'Logged in user details.';
			break;
		}

		//echo "<br>***".$urlvar."***";
		
		$view->assign('topnav',$topnav);
		$view->assign('leftnav',$leftnav);
		$view->assign('urlvar',$urlvar);
		$view->assign('urlaction',$urlaction);
		$view->assign('baseurl',$baseurl);
	}


}

?>