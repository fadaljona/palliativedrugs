<?php 
class MessagesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Messages');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('products');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/messages');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Message');	
		$messages	=	new Application_Model_Messages();
		$users		=	$messages->getUsers();
		$fields		=	$messages->getFields();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'				=>'',
						'message_type'		=> '',
						'message_to'		=> '',
						'user_id'			=> '',
						'mail_subject'		=> '',
						'message_text'		=> '',
						'text'				=> '',
						'email_status'		=> '',
						'active'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$messagedata	= $messages->read($id);
			$form = array
				(
						'id'				=> $messagedata[0]['message_id'],
						'message_type'		=> $messagedata[0]['message_type'],
						'message_to'		=> $messagedata[0]['message_to'],
						'user_id'			=> $messagedata[0]['user_id'],
						'mail_subject'		=> $messagedata[0]['mail_subject'],
						'message_text'		=> $messagedata[0]['message_text'],
						'text'				=> $messagedata[0]['text'],
						'email_status'		=> $messagedata[0]['email_status'],
						'active'			=> $messagedata[0]['active'],
				); 	 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);			
            $this->view->assign('users',$users);			
            $this->view->assign('fields',$fields);			
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$messages->update($form);
			else
				$messages->add($form);
			$this->_redirect('/messages');
       } 		
    }
	
	public function deleteAction()
	{
	    $messages	=	new Application_Model_Messages();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$messages->delete($id);
		$this->_redirect('/messages');
	}
}
