<?php 
class NewsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: News');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('news');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/news');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit News');	
		$news		=	new Application_Model_News();
		$select		=	new Application_Model_Selectpattern();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'title'				=> '',
						'alias_title'		=> '',
						'type'				=> '',
						'category'			=> '',
						'news_date'			=> '',
						'short_text'		=> '',
						'content'			=> '',
						'author'			=> '',
						'news_tags'			=> '',
						'related_news'		=> '',
						'related_pages'		=> '',
						'span_news'			=> '',
						'span_pages'		=> '',
						'url'				=> '',
						'link'				=> '',	
						'news_download'		=> '',
						'span_download'		=> '',
						'link_text'			=> '',						
						'status'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$newsdata	= $news->read($id);
			$form = array
				(
						'id'				=> $newsdata[0]['news_id'],
						'title'				=> $newsdata[0]['title'],
						'alias_title'		=> $newsdata[0]['alias_title'],
						'type'				=> $newsdata[0]['type'],
						'category'			=> $newsdata[0]['category'],
						'news_date'			=> $newsdata[0]['news_date'],
						'short_text'		=> $newsdata[0]['short_text'],
						'content'			=> $newsdata[0]['content'],
						'author'			=> $newsdata[0]['author'],
						'news_tags'			=> $newsdata[0]['news_tags'],
						'related_news'		=> $newsdata[0]['related_news'],
						'related_pages'		=> $newsdata[0]['related_pages'],
						'span_news'			=> $newsdata[0]['span_news'],
						'span_pages'		=> $newsdata[0]['span_pages'],
						'url'				=> $newsdata[0]['url'],
						'link'				=> $newsdata[0]['link'],
						'news_download'		=> $newsdata[0]['news_download'],
						'span_download'		=> $newsdata[0]['span_download'],
						'link_text'			=> $newsdata[0]['link_text'],
						'status'			=> $newsdata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
			$this->view->assign('select',$select);
        }else{  
			$form	=	array_map("trim",$_POST);			
			if($form['id']!='')
				$news->update($form);
			else
				$news->add($form);
			$this->_redirect('/news');
       } 		
    }
	
	public function deleteAction()
	{
	    $news		=	new Application_Model_News();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$news->delete($id);
		$this->_redirect('/news');
	}

	public function relatednewsAction()
	{
	   $this->_helper->layout->disableLayout();
	}
}
