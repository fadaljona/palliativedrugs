<?php 
class ProductsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Products');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('products');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/products');
	    }
    }	

	public function generaladdAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit General Product');	
		$products	=	new Application_Model_Products();
		$select		=	new Application_Model_Selectpattern();				
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$prodcats	=	$products->getProductcats('0');				
				$form = array
				(
						'id'				=> '',
						'procat_id'			=> '',
						'temp_id'			=> '',
						'product_name'		=> '',
						'product_shortname'	=> '',
						'short_text'		=> '',
						'description'		=> '',
						'testimonials'		=> '',
						'style_list'		=> '',
						'product_image'		=> '',
						'product_thumb'		=> '',						
						'vat_id'			=> '',
						'variation_exist'	=> '',
						'shipping_type'		=> '',
						'featured'			=> '',	
						'external_product'	=> '',
						'status'			=> '',						
						'product_image_img'	=> '',
						'product_thumb_img'	=> '',
						'meta_title'		=> '',
						'meta_desc'			=> '',
						'meta_keywords'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$productdata	= $products->generalread($id);
			$prodcats		= $products->getProductcats($productdata[0]['procat_id']);
			$style_list		= $products->style_list($id);
			$form = array
				(
						'id'				=> $productdata[0]['product_id'],
						'procat_id'			=> $productdata[0]['procat_id'],
						'temp_id'			=> $productdata[0]['temp_id'],
						'product_name'		=> $productdata[0]['product_name'],
						'product_shortname'	=> $productdata[0]['product_shortname'],
						'short_text'		=> $productdata[0]['short_text'],
						'description'		=> $productdata[0]['description'],
						'testimonials'		=> $productdata[0]['testimonials'],
						'style_list'		=> $style_list,
						'product_image'		=> $productdata[0]['product_image'],
						'product_thumb'		=> $productdata[0]['product_thumb'],
						'vat_id'			=> $productdata[0]['vat_id'],
						'variation_exist'	=> $productdata[0]['variation_exist'],
						'shipping_type'		=> $productdata[0]['shipping_type'],
						'featured'			=> $productdata[0]['featured'],
						'external_product'	=> $productdata[0]['external_product'],
						'status'			=> $productdata[0]['status'],
						'product_image_img'	=> $productdata[0]['product_image_img'],
						'product_thumb_img'	=> $productdata[0]['product_thumb_img'],
						'meta_title'		=> $productdata[0]['meta_title'],
						'meta_desc'			=> $productdata[0]['meta_desc'],
						'meta_keywords'		=> $productdata[0]['meta_keywords'],
				); 	
			

		}
		if (!$_POST){
			$templates	=	$products->getTemplates();
			$vats		=	$products->getVats();
            $this->view->assign('form',$form);
            $this->view->assign('select',$select);
            $this->view->assign('prodcats',$prodcats);
            $this->view->assign('templates',$templates);
            $this->view->assign('vats',$vats);
        }else{			
			$form	=	$_POST;
			if($form['id']!='')
				$products->generalupdate($form);
			else
				$products->generaladd($form);
			$this->_redirect('/products');
       } 		
    }	
	public function deleteAction()
	{
	    $products	=	new Application_Model_Products();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$products->delete($id);
		$this->_redirect('/products');
	}
}
