<?php 
class GroupsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);
    }

	public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Groups');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('groups');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/groups');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Members');	
		$groups		=	new Application_Model_Groups();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");

		$this->view->assign('groups',$groups);
		if(!$id && $_POST['orderid']==''){
			//echo "Hhere";
			$form = array
			(
				'id'				=>'',
				'action'				=>'',
			); 	
		}else if(!$id && $_POST['orderid']!=''){
			
			$form = array
			(
				'id'				=>'',
				'action'				=>'addprocess',
			); 	

		}else if($id && !$_POST){
			//echo "There";
			$memdata	= $groups->read($id);
			$form = array
				(
						'userid'				=> $memdata[0]['id'],
						'duration'				=> $memdata[0]['duration'],
						'action'				=>'editprocess',
						'contact_email'			=> $memdata[0]['email'],
						'contact_telephone'		=> $memdata[0]['phone'],
						'FirstName'			=> $memdata[0]['FirstName'],
						'LastName'			=> $memdata[0]['LastName'],
						'group_id'			=> $memdata[0]['group_id'],
						'org_name'		=> $memdata[0]['organisation'],
						'g_start_date'		=> $memdata[0]['g_start_date'],
						'orderid'			=> $memdata[0]['order_id'],
						'no_of_subscription'=> $memdata[0]['no_of_subscription'],
				); 	
		}

		if (!$_POST){
			//echo "Yhere";
			$this->view->assign('form',$form);
			$memdata	= $groups->subscriberorderdetails($memdata[0]['order_id']);
			$this->view->assign('memdata',$memdata);

		}else{
			//echo "Shere";
			if($_POST['orderid']!='' && $_POST['action']=='changestatus'){
				
				$this->view->assign('form',$_POST);
				$memdata	= $groups->subscriberorderdetails($_POST['orderid']);
				$this->view->assign('memdata',$memdata);
			}else{

				$form	=	array_map("trim",$_POST);

				if($_POST['action']=='editprocess')
					$groups->update($form);
				else if($_POST['action']=='addprocess')
					$groups->add($form);

				$this->_redirect('/groups');
			}
		}
	}

}
