<?php defined('SYSPATH') or die('No direct script access.');



class Login_Controller extends Controller { 

	public function __construct() { 

		parent::__construct(); // Load sessions, to support logins 

		$this->session = Session::instance(); 

		if(Auth::instance()->logged_in()) { 

			$this->user = $_SESSION['username']; 

		} 

	} 

	

	public function index() { 

		$view = new View('login'); 

		$view->render(true); 

	} 

	

	public function auth() { 

		//print Kohana::debug($this->input->post()); 

		$post = Validation::factory($_POST) ->pre_filter('trim')->add_rules('username', 'required') ->add_rules('password', 'required'); 

		if($post->validate()) { 

			// start session and redirect page 

			$this->db	=	Database::instance();

			$form		=	$this->input->post();

			$resuser	=	$this->db->query("SELECT * FROM 3bit_users WHERE user_username = '".addslashes($form['username'])."' AND user_password='".addslashes($form['password'])."' AND user_firstname='".addslashes($form['first_name'])."' AND user_lastname='".addslashes($form['last_name'])."' ")->result_array(FALSE);

			if(count($resuser)>0){

				$this->session->set(array('user_id' =>$resuser[0]['user_id'], 'username' => $resuser[0]['user_username'],'fullname'=>$resuser[0]['user_firstname']." ".$resuser[0]['user_lastname']));

				$past_url	=	$this->session->get('past_url');				
				if(!empty($past_url)){
					$pos	=	strpos($past_url,'version_preview');
					if ($pos === false) {
						url::redirect(str_replace("/logincms/index.php","",$past_url)); 
					}else{
						header("location:$past_url");
					}
				}else{
					url::redirect('/home'); 
				}

			}else{

				url::redirect('/'); 	

			}									

		}else{

			// error message to login 

			url::redirect('/'); 

		} 

	} 

	public function logout() { 

		$destroy	=	$this->session->destroy();

		url::redirect('/'); 		

	} 

} 

    

?>