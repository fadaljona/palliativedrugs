<?php 
class AssetsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Assets');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('assets');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/assets');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Assets');	
		$assets		=	new Application_Model_Assets();
		$assettypes	=	$assets->getAssettypes();
		$this->view->assign('assettypes',$assettypes);
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'asset_title'		=> '',
						'asset_name'		=> '',
						'asset_alt'			=> '',
						'asset_height'		=> '',
						'asset_width'		=> '',
						'asset_size'		=> '',
						'asset_category_id' => '',
						'keywords'			=> '',						
						'status'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$assetdata	= $assets->read($id);
			$form = array
				(
						'id'				=> $assetdata[0]['asset_id'],
						'asset_title'		=> $assetdata[0]['asset_title'],
						'asset_name'		=> $assetdata[0]['asset_name'],
						'asset_alt'			=> $assetdata[0]['asset_alt'],
						'asset_height'		=> $assetdata[0]['asset_height'],
						'asset_width'		=> $assetdata[0]['asset_width'],
						'asset_size'		=> $assetdata[0]['asset_size'],
						'asset_category_id'	=> $assetdata[0]['asset_category_id'],
						'keywords'			=> $assetdata[0]['keywords'],
						'status'			=> $assetdata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$assets->update($form);
			else
				$assets->add($form);
			$this->_redirect('/assets');
       } 		
    }
	
	public function deleteAction()
	{
	    $users		=	new Application_Model_Assets();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$users->delete($id);
		$this->_redirect('/assets');
	}

	public function downloadAction()
	{
	    $assets		=	new Application_Model_Assets();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$this->view->assign('id',$id);
		$this->_helper->layout->disableLayout();
	}
}
