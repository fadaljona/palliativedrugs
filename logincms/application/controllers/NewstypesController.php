<?php 
class NewstypesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: News Types');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('news');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/newstypes');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit News Types');	
		$types		=	new Application_Model_Newstypes();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'type_name'		=> '',
						'note'			=> '',
						'status'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$typedata	= $types->read($id);
			$form = array
				(
						'id'			=> $typedata[0]['typeid'],
						'type_name'		=> $typedata[0]['type_name'],
						'note'			=> $typedata[0]['note'],
						'status'		=> $typedata[0]['status'],						
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$types->update($form);
			else
				$types->add($form);
			$this->_redirect('/newstypes');
       } 		
    }
	
	public function deleteAction()
	{
	    $types		=	new Application_Model_Newstypes();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$types->delete($id);
		$this->_redirect('/newstypes');
	}
}
