<?php 
class UsertypesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
       $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: User Types');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('users');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/usertypes');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit User Types');	
		$types		=	new Application_Model_Usertypes();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'title'			=> '',
						'module_permissions'=>'',
						'status'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$typedata	= $types->read($id);
			$form = array
				(
						'id'			=> $typedata[0]['type_id'],
						'title'			=> $typedata[0]['title'],
						'module_permissions' => $typedata[0]['module_permissions'],
						'status'		=> $typedata[0]['status'],						
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$types->update($form);
			else
				$types->add($form);
			$this->_redirect('/usertypes');
       } 		
    }
	
	public function deleteAction()
	{
	    $types		=	new Application_Model_Usertypes();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$types->delete($id);
		$this->_redirect('/usertypes');
	}
}
