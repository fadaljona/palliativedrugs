<?php 
class MembersController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);
    }

	public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Members');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('members');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/members');
	    }
    }	

	public function groupsAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Members Statistics');
		$members	=	new Application_Model_Members();
		$roles		=	$members->active_roles();
		$speciality	=	$members->active_speciality();
		$this->view->assign('roles',$roles);
		$this->view->assign('speciality',$speciality);
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Members');	
		$members	=	new Application_Model_Members();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$roles		=	$members->active_roles();
		$speciality	=	$members->active_speciality();
		$sub_spec	=	$members->active_sub_spec();
		$country	=	$members->active_country();
		$place		=	$members->active_place();
		$post		=	$members->active_post();
		$this->view->assign('roles',$roles);
		$this->view->assign('speciality',$speciality);
		$this->view->assign('sub_spec',$sub_spec);
		$this->view->assign('country',$country);
		$this->view->assign('place',$place);
		$this->view->assign('post',$post);
		if(!$id){
				$form = array
				(
						'id'				=>'',
						'username'			=> '',
						'email'				=> '',
						'pwd'				=> '',
						'FirstName'			=> '',
						'LastName'			=> '',
						'title'				=> '',
						'role'				=> '',
						'prof_reg_num'		=> '',
						'nurse_prescriber'	=> '',
						'speciality'		=> '',
						'sub_specialty'		=> '',
						'organisation'		=> '',
						'addr1'				=> '',
						'addr2'				=> '',
						'addr3'				=> '',
						'addr4'				=> '',
						'TownOrCity'		=> '',
						'StateOrCounty'		=> '',
						'PostalCode'		=> '',
						'Country'			=> '',
						'phone'				=> '',
						'main_workplace'	=> '',
						'current_post'		=> '',
						'years_in_post'		=> '',
						'patient_care_time'	=> '',
						'gender'			=> '',
						'year_of_birth'		=> '',
						'lead'				=> '',
						'accept_site_emails'=> '',
						'accept_bb_emails'	=> '',
						'UserType'			=> '',
						'DateRegistered'	=> '',
						'RegistrationChecked'=>'',
						'vetted'			=> '',
						'previous_visit'	=> '',
						'current_visit'		=> '',
						'user_confirmed'	=> '', 
						'active'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$memdata	= $members->read($id);
			$form = array
				(
						'id'				=> $memdata[0]['id'],
						'username'			=> $memdata[0]['username'],
						'email'				=> $memdata[0]['email'],
						'pwd'				=> $memdata[0]['pwd'],
						'FirstName'			=> $memdata[0]['FirstName'],
						'LastName'			=> $memdata[0]['LastName'],
						'title'				=> $memdata[0]['title'],
						'role'				=> $memdata[0]['role'],
						'prof_reg_num'		=> $memdata[0]['prof_reg_num'],
						'nurse_prescriber'	=> $memdata[0]['nurse_prescriber'],
						'speciality'		=> $memdata[0]['speciality'],
						'sub_specialty'		=> $memdata[0]['sub_specialty'],
						'organisation'		=> $memdata[0]['organisation'],
						'addr1'				=> $memdata[0]['addr1'],
						'addr2'				=> $memdata[0]['addr2'],
						'addr3'				=> $memdata[0]['addr3'],
						'addr4'				=> $memdata[0]['addr4'],
						'TownOrCity'		=> $memdata[0]['TownOrCity'],
						'StateOrCounty'		=> $memdata[0]['StateOrCounty'],
						'PostalCode'		=> $memdata[0]['PostalCode'],
						'Country'			=> $memdata[0]['Country'],
						'phone'				=> $memdata[0]['phone'],
						'main_workplace'	=> $memdata[0]['main_workplace'],
						'current_post'		=> $memdata[0]['current_post'],
						'years_in_post'		=> $memdata[0]['years_in_post'],
						'patient_care_time'	=> $memdata[0]['patient_care_time'],
						'gender'			=> $memdata[0]['gender'],
						'year_of_birth'		=> $memdata[0]['year_of_birth'],
						'lead'				=> $memdata[0]['lead'],
						'accept_site_emails'=> $memdata[0]['accept_site_emails'],
						'accept_bb_emails'	=> $memdata[0]['accept_bb_emails'],
						'UserType'			=> $memdata[0]['UserType'],
						'DateRegistered'	=> $memdata[0]['DateRegistered'],
						'RegistrationChecked'=>$memdata[0]['RegistrationChecked'],
						'vetted'			=> $memdata[0]['vetted'],
						'previous_visit'	=> $memdata[0]['previous_visit'],
						'current_visit'		=> $memdata[0]['current_visit'],
						'user_confirmed'	=> $memdata[0]['user_confirmed'],
						'active'			=> $memdata[0]['active'],
				); 	
		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$members->update($form);
			else
				$members->add($form);
			$this->_redirect('/members');
       } 		
    }

	public function createsubscriberAction()
	{
		$this->view->assign('title','Palliative Drugs CMS :: Create/Update Subscriber');	
		$members	=	new Application_Model_Members();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$subid 		=	$members->getsubscribtion($id);
		$action		=	$request->getParam("saction");
		$formaction		=	$request->getParam("formaction");

		if($id && !$_POST){
			if(!$action){
			$form = array
				(
						'id'	=> $id,
						'subid'	=> $subid ,
				); 	
			}else if($action == 'updatesubscriber'){

				$memdata	= $members->readsubscribtion($subid);
				$arrStartDate			= explode('-',$memdata[0]['start_date']);
							$startDate				= $arrStartDate[2].'-'.$arrStartDate[1].'-'.$arrStartDate[0];
							$arrEndDate				= explode('-',$memdata[0]['end_date']);
							$endDate				= $arrEndDate[2].'-'.$arrEndDate[1].'-'.$arrEndDate[0];
				$form = array
				(
						

						'id'	=> $id,
						'subid'	=> $subid ,
						'start_date'	=> $startDate,
						'end_date'	=> $endDate,
						'duration'	=> $memdata[0]['duration'],
						'status'	=> $memdata[0]['status'],
						'action'	=> $action,
						'formaction'	=> $formaction,
				);
			}
		}

		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  

			$form	=	array_map("trim",$_POST);

			if($form['action']=='')
				$members->createsubscriber($form);
			else
				$members->updatesubscriber($form);
			$this->_redirect('/members');
		}
	}

	public function viewAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: View Members');	
		$members	=	new Application_Model_Members();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");

		if(!$id){
				$form = array
				(
						'id'				=> '',
						'title'				=> '',
						'FirstName'			=> '',
						'LastName'			=> '',
						'email'				=> '',
						'start_date'		=> '',
						'end_date'			=> '',
						'duration'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$memdata	= $members->view($id);
			$details	= $members->getSubcribtionDetails($id);
			$form = array
				(
						'id'				=> $memdata[0]['id'],
						'title'				=> $memdata[0]['title'],
						'FirstName'			=> $memdata[0]['FirstName'],
						'LastName'			=> $memdata[0]['LastName'],
						'email'			=> $memdata[0]['email'],
						'start_date'		=> $memdata[0]['start_date'],
						'end_date'			=> $memdata[0]['end_date'],
						'duration'			=> $memdata[0]['duration'],
				); 	
		}
		if (!$_POST){
            $this->view->assign('form',$form);
			$this->view->assign('details',$details);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$members->update($form);
			else
				$members->add($form);
			$this->_redirect('/members');
       } 		
    }
	
	public function deleteAction()
	{
	    $members	=	new Application_Model_Members();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$members->delete($id);
		$this->_redirect('/members');
	}

	public function quickviewAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Quick View');	
		$this->_baseurl	=	Zend_Registry::get("baseUrl");
		$members	=	new Application_Model_Members();	
		$roles		=	$members->active_roles();
		$speciality	=	$members->active_speciality();
		$sub_spec	=	$members->active_sub_spec();
		$country	=	$members->active_country();
		$place		=	$members->active_place();
		$post		=	$members->active_post();
		$this->view->assign('roles',$roles);
		$this->view->assign('speciality',$speciality);
		$this->view->assign('sub_spec',$sub_spec);
		$this->view->assign('country',$country);
		$this->view->assign('place',$place);
		$this->view->assign('post',$post);
		$request	=	$this->getRequest();		
		

		$id 		=	explode("&",$request->getParam("id"));		
		
		if(count($id)){
			$id 		=	$id[0];	
		}else{
			$id 		=	$request->getParam("id");	
		}

		$flag 		=	$request->getParam("flag");	

		$pervnextid	=	$members->prevnext_member($id);
		
		$this->_helper->layout->disableLayout();

		if(!$id){
				$form = array
				(
						'id'				=>'',
						'username'			=> '',
						'email'				=> '',
						'pwd'				=> '',
						'FirstName'			=> '',
						'LastName'			=> '',
						'title'				=> '',
						'role'				=> '',
						'prof_reg_num'		=> '',
						'nurse_prescriber'	=> '',
						'speciality'		=> '',
						'sub_specialty'		=> '',
						'organisation'		=> '',
						'addr1'				=> '',
						'addr2'				=> '',
						'addr3'				=> '',
						'addr4'				=> '',
						'TownOrCity'		=> '',
						'StateOrCounty'		=> '',
						'PostalCode'		=> '',
						'Country'			=> '',
						'phone'				=> '',
						'main_workplace'	=> '',
						'current_post'		=> '',
						'years_in_post'		=> '',
						'patient_care_time'	=> '',
						'gender'			=> '',
						'year_of_birth'		=> '',
						'lead'				=> '',
						'accept_site_emails'=> '',
						'accept_bb_emails'	=> '',
						'UserType'			=> '',
						'DateRegistered'	=> '',
						'RegistrationChecked'=>'',
						'vetted'			=> '',
						'previous_visit'	=> '',
						'current_visit'		=> '',
						'user_confirmed'	=> '', 
						'active'			=> '',
				); 	
			
		}else if($id && !$_POST){

			$memdata	= $members->quickview($id);

			$form = array
				(
					'id'				=> $memdata[0]['id'],
					'previd'			=> $pervnextid[0],
					'nextid'			=> $pervnextid[1],
					'baseurl'			=> $this->_baseurl,
					'username'			=> $memdata[0]['username'],
					'email'				=> $memdata[0]['email'],
					'pwd'				=> $memdata[0]['pwd'],
					'FirstName'			=> $memdata[0]['FirstName'],
					'LastName'			=> $memdata[0]['LastName'],
					'title'				=> $memdata[0]['title'],
					'role'				=> $memdata[0]['role'],
					'prof_reg_num'		=> $memdata[0]['prof_reg_num'],
					'nurse_prescriber'	=> $memdata[0]['nurse_prescriber'],
					'speciality'		=> $memdata[0]['speciality'],
					'sub_specialty'		=> $memdata[0]['sub_specialty'],
					'organisation'		=> $memdata[0]['organisation'],
					'addr1'				=> $memdata[0]['addr1'],
					'addr2'				=> $memdata[0]['addr2'],
					'addr3'				=> $memdata[0]['addr3'],
					'addr4'				=> $memdata[0]['addr4'],
					'TownOrCity'		=> $memdata[0]['TownOrCity'],
					'StateOrCounty'		=> $memdata[0]['StateOrCounty'],
					'PostalCode'		=> $memdata[0]['PostalCode'],
					'Country'			=> $memdata[0]['Country'],
					'phone'				=> $memdata[0]['phone'],
					'main_workplace'	=> $memdata[0]['main_workplace'],
					'current_post'		=> $memdata[0]['current_post'],
					'years_in_post'		=> $memdata[0]['years_in_post'],
					'patient_care_time'	=> $memdata[0]['patient_care_time'],
					'gender'			=> $memdata[0]['gender'],
					'year_of_birth'		=> $memdata[0]['year_of_birth'],
					'lead'				=> $memdata[0]['lead'],
					'accept_site_emails'=> $memdata[0]['accept_site_emails'],
					'accept_bb_emails'	=> $memdata[0]['accept_bb_emails'],
					'UserType'			=> $memdata[0]['UserType'],
					'DateRegistered'	=> $memdata[0]['DateRegistered'],
					'RegistrationChecked'=>$memdata[0]['RegistrationChecked'],
					'vetted'			=> $memdata[0]['vetted'],
					'previous_visit'	=> $memdata[0]['previous_visit'],
					'current_visit'		=> $memdata[0]['current_visit'],
					'user_confirmed'	=> $memdata[0]['user_confirmed'],
					'active'			=> $memdata[0]['active'],
					'flag'			=> $flag,
				); 	
		}

		
		 $this->view->assign('baseurl',$this->_baseurl);

		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);

			if($form['id']!=''){
				$members->updatequickview($form);
				$this->_redirect('/members/quickview/id/'.$pervnextid[1]);
			}

			//	$this->_helper->layout->disableLayout();
		}
		
	}

	public function statisticsAction()
	{
		$this->view->assign('title','Palliative Drugs CMS :: Members Statistics');
		$members	=	new Application_Model_Members();
		$roles		=	$members->active_roles();
		$speciality	=	$members->active_speciality();
		$this->view->assign('roles',$roles);
		$this->view->assign('speciality',$speciality);
    }
}
