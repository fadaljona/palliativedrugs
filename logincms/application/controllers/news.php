<?php
class News_Controller extends Website_Controller
{
	public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: News';
        $this->template->content = new View('news/news');
    }

	// Function to add/update news to the db //

	public function news_add($id='')
	{
	    $this->news							= new News_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit News';
        $this->template->content			= new View('news/news_add');		
        $this->template->content->errors	= array();		
		$this->session = Session::instance();
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'title'				=> '',
						'alias_title'		=> '',
						'type'				=> '',
						'category'			=> '',
						'news_date'			=> '',
						'short_text'		=> '',
						'content'			=> '',
						'author'			=> '',
						'news_tags'			=> '',
						'related_news'		=> '',
						'related_pages'		=> '',
						'span_news'			=> '',
						'span_pages'		=> '',
						'url'				=> '',
						'link'				=> '',	
						'news_download'		=> '',
						'span_download'		=> '',
						'link_text'			=> '',						
						'status'			=> '',
				); 	
			
		}else{
			$newsata	= $this->news->read($id);			
			$form = array
				(
						'id'				=> $newsata[0]->news_id,
						'title'				=> $newsata[0]->title,
						'alias_title'		=> $newsata[0]->alias_title,
						'type'				=> $newsata[0]->type,
						'category'			=> $newsata[0]->category,
						'news_date'			=> $newsata[0]->news_date,
						'short_text'		=> $newsata[0]->short_text,
						'content'			=> $newsata[0]->content,
						'author'			=> $newsata[0]->author,
						'news_tags'			=> $newsata[0]->news_tags,
						'related_news'		=> $newsata[0]->related_news,
						'related_pages'		=> $newsata[0]->related_pages,
						'span_news'			=> $newsata[0]->span_news,
						'span_pages'		=> $newsata[0]->span_pages,
						'url'				=> $newsata[0]->url,
						'link'				=> $newsata[0]->link,
						'news_download'		=> $newsata[0]->news_download,
						'span_download'		=> $newsata[0]->span_download,
						'link_text'			=> $newsata[0]->link_text,
						'status'			=> $newsata[0]->status,
				); 						
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('title','required');
            //$post->add_rules('long_text', 'required');
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{					
				if($form['id']!='')
					$this->news->update($form);
				else
					$this->news->add($form);
				url::redirect('/news');
            }
        } 
	}

	// Function to delete news from db //

	public function news_delete($id='')
	{
	    $this->news = new News_Model;
		$this->news->delete($id);
		url::redirect('/news');
	}
	
}

?>