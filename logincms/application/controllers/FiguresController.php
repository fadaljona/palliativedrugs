<?php 
class FiguresController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Formulary Snippets');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('formulary');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/formulary snippets');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Formulary Snippets');	
		$snippets	=	new Application_Model_Figures();
		$pages		=	new Application_Model_Pages();
		$select		=	new Application_Model_Selectpattern();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$langs		=	$pages->getLanguages();
		$this->view->assign('langs',$langs);
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'snippet_title'		=> '',
						'snippet_number'	=> '',
						'snippet_type'		=> '',
						'long_text'			=> '',
						'status'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$sdata	= $snippets->read($id);
			$form = array
				(
						'id'				=> $sdata[0]['snippet_id'],						
						'snippet_title'		=> $sdata[0]['snippet_title'],
						'snippet_number'	=> $sdata[0]['snippet_number'],
						'snippet_type'		=> $sdata[0]['snippet_type'],
						'long_text'			=> $sdata[0]['long_text'],						
						'status'			=> $sdata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
			$this->view->assign('select',$select);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$snippets->update($form);
			else
				$snippets->add($form);
			$this->_redirect('/figures');
       } 		
    }
	
	public function deleteAction()
	{
	    $snippets	=	new Application_Model_Snippets();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$snippets->delete($id);
		$this->_redirect('/figures');
	}
}
