<?php 
class FieldsetsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Fieldsets');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('pages');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/fieldsets');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Fieldset');	
		$field		=	new Application_Model_Fieldsets();
		$datasource	=	$field->getDatasource();
		$this->view->assign('datasource',$datasource);
		$groups	=	$field->getGroups();
		$this->view->assign('groups',$groups);
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'title'			=> '',
						'group_type'	=> '',
						'description'	=> '',
						'datasource_id'	=> '',
						'advance_sql'	=> '',
						'usage'			=> '',						
						'status'		=> '',
						'fields'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$fielddata	= $field->read($id);
			$form = array
				(
						'id'			=> $fielddata[0]['fieldset_id'],
						'title'			=> $fielddata[0]['title'],
						'group_type'	=> $fielddata[0]['group_type'],
						'description'	=> $fielddata[0]['description'],
						'datasource_id'	=> $fielddata[0]['datasource_id'],
						'advance_sql'	=> $fielddata[0]['advance_sql'],
						'usage'			=> $fielddata[0]['usage'],											
						'status'		=> $fielddata[0]['status'],
						'fields'		=> $field->getfields($id),
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);			
        }else{  
			$form	=	$_POST;
			if($form['id']!='')
				$field->update($form);
			else
				$field->add($form);
			$this->_redirect('/fieldsets');
       } 		
    }
	
	public function deleteAction()
	{
	    $field		=	new Application_Model_Fieldsets();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$field->delete($id);
		$this->_redirect('/fieldsets');
	}

	public function checksqlAction()
	{
		$field		=	new Application_Model_Fieldsets();
		$form		=	array_map("trim",$_POST);
		$field->checksql($form);
	}
}
