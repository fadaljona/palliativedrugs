<?php
class Assetcat_Controller extends Website_Controller
{	

    public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Asset categories';
        $this->template->content = new View('assets/assetcat');
    }

	// Function to add/update users to the db //

	public function assetcat_add($id='')
	{
	    $this->assetcats					= new Assetcat_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Asset Categories';
        $this->template->content			= new View('assets/assetcat_add');		
        $this->template->content->errors	= array();

		if(!$id){
				$form = array
				(
						'id'						=> '',
						'asset_category_title'		=> '',
						'asset_category_type'		=> '',
						'asset_category_description'=> '',
						'status'					=> '',
				); 	
			
		}else{
			$userdata	= $this->assetcats->read($id);	
			$form = array
				(
						'id'						=> $userdata[0]->asset_category_id,
						'asset_category_title'		=> $userdata[0]->asset_category_title,
						'asset_category_type'		=> $userdata[0]->asset_category_type,
						'asset_category_description'=> $userdata[0]->asset_category_description,						
						'status'					=> $userdata[0]->status,
				); 	
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('asset_category_title','required');           
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->assetcats->update($form);
				else
					$this->assetcats->add($form);
				url::redirect('/assetcat');
            }
        } 
	}

	// Function to delete user from db //

	public function assetcat_delete($id='')
	{
	    $this->assetcats	= new Assetcat_Model;
		$this->assetcats->delete($id);
		url::redirect('/assetcat');
	}
}

?>