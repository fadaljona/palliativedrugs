<?php 
class UsersController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Users');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('users');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/users');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Users');	
		$users		=	new Application_Model_Users();
		$usertypes	=	$users->getUsertypes();
		$this->view->assign('usertypes',$usertypes);
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'user_firstname'=> '',
						'user_lastname'	=> '',
						'user_email'	=> '',
						'user_username'	=> '',
						'user_password'	=> '',
						'type_id'		=> '',
						'status'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$userdata	= $users->read($id);
			$form = array
				(
						'id'			=> $userdata[0]['user_id'],
						'user_firstname'=> $userdata[0]['user_firstname'],
						'user_lastname' => $userdata[0]['user_lastname'],
						'user_email'	=> $userdata[0]['user_email'],
						'user_username'	=> $userdata[0]['user_username'],
						'user_password' => $userdata[0]['user_password'],
						'type_id'		=> $userdata[0]['type_id'],						
						'status'		=> $userdata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$users->update($form);
			else
				$users->add($form);
			$this->_redirect('/users');
       } 		
    }
	
	public function deleteAction()
	{
	    $users		=	new Application_Model_Users();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$users->delete($id);
		$this->_redirect('/users');
	}
}
