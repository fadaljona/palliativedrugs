<?php
class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
		$this->view->assign('past_url',$ns->past_url);	
	}

    public function indexAction()
    {
        // action body
		$ns	=	Zend_Registry::get("ns");		
		$this->_baseurl	=	Zend_Registry::get("baseUrl");
		if (!empty($ns->username)) {
			if(empty($ns->past_url)){
				$this->_response->setRedirect($this->_baseurl.'/home')->sendResponse();
				exit;
			}
        }
    }

	public function validateAction()
	{	
		$db			= Zend_Registry::get("db");		
	
		$select		= "SELECT u.*,ut.title FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id AND user_firstname='".$_POST['fname']."' AND user_lastname = '".$_POST['lname']."'";

		$row		= $db->fetchRow($select);

		$message	= 'S';
		
		$ns			=	Zend_Registry::get("ns");		

		if(!($row)){
			$message =  "IN";
		}else if($row){
		   $user_id		=   $row['user_id'];
		   $email	 	=	$row['user_email'];
		   $username	=	$row['user_username'];
		   $fullname	=	$row['user_firstname']." ".$row['user_lastname'];	
		   $utype		=   $row['title'];
		   $chkUname	=	"SELECT u.*,ut.title FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id AND user_username='".$_POST['uname']."' AND user_firstname='".$_POST['fname']."' AND user_lastname = '".$_POST['lname']."'" ;
		   $row1		=	$db->fetchRow($chkUname);		  
			if(!($row1)){			
				$message =  "IU";			
			}else if($row1){
			   $user_id =  	$row1['user_id'];
			   $email	=	$row1['user_email'];
			   $username=	$row1['user_username'];
			   $fullname=	$row1['user_firstname']." ".$row1['user_lastname'];	
			   $utype	 =  $row1['title'];
			   $chkPwd	=	"SELECT u.*,ut.title FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id AND user_username='".$_POST['uname']."' AND user_firstname='".$_POST['fname']."' AND user_lastname = '".$_POST['lname']."' AND user_password='".base64_encode($_POST['pwd'])."'" ;
			   $row2	=	$db->fetchRow($chkPwd);
			   if(!($row2)){				
					$message =  "IP";			
			   }else{
					$user_id =  $row2['user_id'];
					$email	 =	$row2['user_email'];
					$username=	$row2['user_username'];
					$fullname=	$row2['user_firstname']." ".$row2['user_lastname'];	
					$utype	 =  $row2['title'];
			   }
			} 
		}
		if($message =='S'){
			$history	=	$_POST['fname']." ".$_POST['lname'].' login <span class="dashdate">'.date('Y/m/d H:i:s').'</span>';
			$db->query("INSERT INTO 3bit_audit_history SET
									module				=	'login',
									action				=	'login',
									record_id			=	'".addslashes($user_id)."',
									action_status		=	'success',
									user_id				=	'".addslashes($user_id)."',
									ip_address			=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
									browser				=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
									action_date			=	now(),
									audit_history		=	'".addslashes($history)."',
									status				=	'1',
									created_date		=	now(),
									modified_date		=	now()");		
		}else{
			$history	=	'Failed Login attempt using username <span class="admin">'.$_POST['uname'].'</span> via IP '.$_SERVER['REMOTE_ADDR'].' <span class="dashdate">'.date('Y/m/d H:i:s').'</span>';
			$db->query("INSERT INTO 3bit_audit_history SET
									module				=	'login',
									action				=	'login',
									record_id			=	'".addslashes($user_id)."',
									action_status		=	'failure',
									user_id				=	'".addslashes($user_id)."',
									ip_address			=	'".addslashes($_SERVER['REMOTE_ADDR'])."',
									browser				=	'".addslashes($_SERVER['HTTP_USER_AGENT'])."',
									action_date			=	now(),
									audit_history		=	'".addslashes($history)."',
									status				=	'1',
									created_date		=	now(),
									modified_date		=	now()");
			

		}
		$ns->user_id	=	$user_id;
		$ns->username	=	$username;
		$ns->fullname	=	$fullname;
		$ns->email		=	$email;
		$ns->utype		=	$utype;
		die($message);   		
	}
	
	public function logoutAction()
    {
       $ns			=	Zend_Registry::get("ns");
	   $ns->unsetAll();
	   $this->_helper->redirector('index');
    }
}

