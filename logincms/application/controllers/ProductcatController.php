<?php 
class ProductcatController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Product Categories');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('products');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/product category');
	    }
		$pcat		=	new Application_Model_Productcat();
		$pcats		=	$pcat->procat_list('0','1');
		$this->view->assign('pcats',$pcats);
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Category');	
		$pcats		=	new Application_Model_Productcat();				
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$list_procat= $pcats->list_procat('0','','0');
				$form = array
				(
						'id'			=> '',
						'procat_title'  => '',
						'procat_desc'	=> '',
						'form_type'		=> '',
						'status'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$cdata		= $pcats->read($id);
			$list_procat= $pcats->list_procat('0','',$cdata[0]['procat_parent_id']);
			$form = array
				(
						'id'			=> $cdata[0]['procat_id'],
						'procat_title'  => $cdata[0]['procat_title'],
						'procat_desc'	=> $cdata[0]['procat_desc'],
						'form_type'		=> $cdata[0]['form_type'],
						'status'		=> $cdata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
            $this->view->assign('list_procat',$list_procat);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$pcats->update($form);
			else
				$pcats->add($form);
			$this->_redirect('/productcat');
       } 		
    }
	
	public function deleteAction()
	{
	    $pcats		=	new Application_Model_Productcat();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$pcats->delete($id);
		$this->_redirect('/productcat');
	}
}
