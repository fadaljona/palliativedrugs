<?php
class Rules_Controller extends Website_Controller
{

    public function index()
    {
        $this->template->title = 'Rules::Paliative Drugs';
        $this->template->content = new View('pages/rules');
		
		// Instantiate the model
        $rules = new Rules_Model;  

        // The result set is assigned to a variable called $products in the view
        $this->template->content->rules = $rules->browse();


    }

}

?>