<?php 
class CouriersController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Couriers');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('products');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/courier');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Courier');	
		$couriers	=	new Application_Model_Couriers();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'courier_name'		=> '',
						'courier_url'		=> '',						
						'courier_status'	=> '',
				); 	
			
		}else if($id && !$_POST){
			$cdata	= $couriers->read($id);
			$form = array
				(
						'id'				=> $cdata[0]['courier_id'],
						'courier_name'		=> $cdata[0]['courier_name'],
						'courier_url'		=> $cdata[0]['courier_url'],
						'courier_status'	=> $cdata[0]['courier_status']
				); 	 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);			
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$couriers->update($form);
			else
				$couriers->add($form);
			$this->_redirect('/couriers');
       } 		
    }
	
	public function deleteAction()
	{
	    $couriers	=	new Application_Model_Couriers();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$couriers->delete($id);
		$this->_redirect('/couriers');
	}
}
