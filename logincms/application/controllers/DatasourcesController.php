<?php 
class DatasourcesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
       $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Datasources');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('pages');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/datasources');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Datasources');	
		$datas		=	new Application_Model_Datasources();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'title'			=> '',
						'content'		=> '',
						'status'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$typedata	= $datas->read($id);
			$form = array
				(
						'id'			=> $typedata[0]['datasource_id'],
						'title'			=> $typedata[0]['title'],
						'content'		=> $typedata[0]['content'],
						'status'		=> $typedata[0]['status'],						
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$datas->update($form);
			else
				$datas->add($form);
			$this->_redirect('/datasources');
       } 		
    }
	
	public function deleteAction()
	{
	    $datas		=	new Application_Model_Datasources();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$datas->delete($id);
		$this->_redirect('/datasources');
	}
}
