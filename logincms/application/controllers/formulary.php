<?php
class Formulary_Controller extends Website_Controller
{
	public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Formulary';
        $this->template->content = new View('formulary/formulary');
		
		// Instantiate the model
        $formulary = new Formulary_Model;  
		
        $this->template->content->formulary_list = $formulary->formulary_list('0','1');
		$this->template->content->levels		 = $formulary->active_levels();
    }
	
	// Function to add/update formulary to the db //

	public function formulary_add($id='')
	{
		$this->session = Session::instance();
		$better_token = md5(uniqid(rand(), true));
		if(empty($_SESSION['asess_id'])){
		  $_SESSION['asess_id'] = $better_token;
		}
	    $this->formulary					= new Formulary_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Formulary';
        $this->template->content			= new View('formulary/formulary_add');		
        $this->template->content->errors	= array();	
		$this->template->content->levels	= $this->formulary->active_levels();
		if(!$id){
				$this->template->content->parent_list= $this->formulary->parent_list('0','','0');
				$this->template->content->list_lang	= $this->formulary->list_lang('0');	
				$form = array
				(
						'id'				=> '',
						'language_id'		=> '',
						'formulary_title'	=> '',
						'alias'				=> '',
						'formulary_type'	=> '',
						'formulary_parent_id'=> '',
						'url'				=> '',
						'bnf_reference'		=> '',					
						'formulary_level'	=> '',					
						'country'			=> '',
						'blocks'			=> '',
						'publish_status'	=> '',
						'formulary_tags'	=> '',
						'status'			=> '',
						'pre_text_article'	=> '',	
						'class'				=> '',
						'indication'		=> '',
						'pharmacology'		=> '',
						'cautions'			=> '',
						'undesireable_effects'=> '',
						'dose_and_use'		=> '',
						'overdose'			=> '',
						'death_rattle'		=> '',
						'supply'			=> '',
						'post_text_article'	=> '', 
						
				); 	
			
		}else{
			$formdata		= $this->formulary->read($id);	
			$this->template->content->parent_list= $this->formulary->parent_list('0','',$formdata[0]->formulary_parent_id);
			$this->template->content->list_lang	= $this->formulary->list_lang($formdata[0]->language_id);
			$block1 ='';$block2='';	$block3='';	$block4 =''; $block5='';$block6='';	$block7 ='';$block8='';$block9='';$block10 =''; $block11='';	
			if(!empty($formdata['pre_text_article']))
				$block1	=	$formdata['pre_text_article'];
			if(!empty($formdata['class']))
				$block2	=	$formdata['class'];
			if(!empty($formdata['indication']))
				$block3	=	$formdata['indication'];
			if(!empty($formdata['pharmacology']))
				$block4	=	$formdata['pharmacology'];
			if(!empty($formdata['cautions']))
				$block5	=	$formdata['cautions'];
			if(!empty($formdata['undesireable_effects']))
				$block6	=	$formdata['undesireable_effects'];
			if(!empty($formdata['dose_and_use']))
				$block7	=	$formdata['dose_and_use'];
			if(!empty($formdata['overdose']))
				$block8	=	$formdata['overdose'];
			if(!empty($formdata['death_rattle']))
				$block9	=	$formdata['death_rattle'];
			if(!empty($formdata['supply']))
				$block10=	$formdata['supply'];
			if(!empty($formdata['post_text_article']))
				$block11=	$formdata['post_text_article'];		
			
			$form = array
				(
						'id'				=> $formdata[0]->formulary_id,
						'language_id'		=> $formdata[0]->language_id,
						'formulary_title'	=> $formdata[0]->formulary_title,
						'alias'				=> $formdata[0]->alias,
						'formulary_type'	=> $formdata[0]->formulary_type,
						'formulary_parent_id'=>$formdata[0]->formulary_parent_id,
						'url'				=> $formdata[0]->url,
						'bnf_reference'		=> $formdata[0]->bnf_reference,
						'formulary_level'	=> $formdata[0]->formulary_level,			
						'country'			=> $formdata[0]->country,
						'blocks'			=> $formdata[0]->blocks,
						'publish_status'	=> $formdata[0]->publish_status,
						'formulary_tags'	=> $formdata[0]->formulary_tags,	
						'status'			=> $formdata[0]->status,
						'pre_text_article'	=> $block1,	
						'class'				=> $block2,
						'indication'		=> $block3,
						'pharmacology'		=> $block4,
						'cautions'			=> $block5,
						'undesireable_effects'=> $block6,
						'dose_and_use'		=> $block7,
						'overdose'			=> $block8,
						'death_rattle'		=> $block9,
						'supply'			=> $block10,
						'post_text_article'	=> $block11, 
				); 						
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('formulary_title','required');
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!=''){
					if($form['action']=='duplicate')
						$this->formulary->add($form);
					else if($form['action']=='save_version')
						$this->formulary->save_version($form);
					else
						$this->formulary->update($form);
				}
				else
					$this->formulary->add($form);
				url::redirect('/formulary');
            }
        } 
	}

	// Function to delete formulary from db //

	public function formulary_delete($id='')
	{
	    $this->formulary = new Formulary_Model;
		$this->formulary->delete($id);
		url::redirect('/formulary');
	}	
}

?>