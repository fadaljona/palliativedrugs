<?php 
class ArticlesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Articles');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('pages');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/articles');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Articles');	
		$articles	=	new Application_Model_Articles();
		$select		=	new Application_Model_Selectpattern();		
		$snippets	=	$articles->getSnippets();
		$this->view->assign('snippets',$snippets);
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'article_title'		=> '',
						'owner'				=> '',
						'user_id'			=> '',
						'article_date'		=> '',
						'article_sap'		=> '',
						'article_enddate'	=> '',
						'article_eap'		=> '',
						'short_text'		=> '',
						'long_text'			=> '',
						'header_image'		=> '',
						'article_image'		=> '',
						'header_imageid'	=> '',
						'article_imageid'	=> '',
						'header_image_img'	=> '',
						'article_image_img'	=> '',
						'image_text'		=> '',
						'meta_title'		=> '',
						'meta_desc'			=> '',
						'meta_keywords'		=> '',
						'status'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$adata	= $articles->read($id);
			$form = array
				(
						'id'				=> $adata[0]['article_id'],
						'article_title'		=> $adata[0]['article_title'],
						'owner'				=> $adata[0]['owner'],
						'article_date'		=> $adata[0]['article_date'],
						'article_sap'		=> $adata[0]['article_sap'],
						'article_enddate'	=> $adata[0]['article_enddate'],
						'article_eap'		=> $adata[0]['article_eap'],
						'short_text'		=> $adata[0]['short_text'],
						'long_text'			=> $adata[0]['long_text'],
						'header_image'		=> $adata[0]['header_image'],
						'article_image'		=> $adata[0]['article_image'],
						'header_imageid'	=> $adata[0]['header_image'],
						'article_imageid'	=> $adata[0]['article_image'],
						'header_image_img'  => $adata[0]['header_image_img'],
						'article_image_img'	=> $adata[0]['article_image_img'],
						'image_text'		=> $adata[0]['image_text'],
						'meta_title'		=> $adata[0]['meta_title'],
						'meta_desc'			=> $adata[0]['meta_desc'],
						'meta_keywords'		=> $adata[0]['meta_keywords'],
						'status'			=> $adata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
			$this->view->assign('select',$select);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$articles->update($form);
			else
				$articles->add($form);
			$this->_redirect('/articles');
       } 		
    }
	
	public function deleteAction()
	{
	    $articles	=	new Application_Model_Articles();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$articles->delete($id);
		$this->_redirect('/articles');
	}
}
