<?php
class Drugs_Controller extends Website_Controller
{
	public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Supply Drugs';
        $this->template->content = new View('formulary/drugs');		
    }
	
	// Function to add/update drug to the db //

	public function drug_add($id='')
	{
	    $this->drugs						= new Drugs_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Drug';
        $this->template->content			= new View('formulary/drug_add');		
        $this->template->content->errors	= array();
		$this->template->content->parent_form	=$this->drugs->parent_formulary();
		if(!$id){																  					
				$form = array
				(
						'id'				=>'',
						'formulary_id'		=> '',
						'drug_name'			=> '',
						'brand'				=> '',
						'manufacture'		=> '',
						'manufacture_url'	=> '',
						'strength'			=> '',
						'formulation'		=> '',
						'packsize'			=> '',
						'cost_quoted'		=> '',
						'nof_doses'			=> '',
						'cost_per_udose'	=> '',
						'nof_dose_pday'		=> '',
						'cost_28days'		=> '',
						'comments'			=> '',
						'status'			=> '',
				); 	
			
		}else{
			$userdata	= $this->drugs->read($id);	
			$form = array
				(
						'id'				=> $userdata[0]->drug_id,
						'formulary_id'		=> $userdata[0]->formulary_id,
						'drug_name'			=> $userdata[0]->drug_name,
						'brand'				=> $userdata[0]->brand,
						'manufacture'		=> $userdata[0]->manufacture,
						'manufacture_url'	=> $userdata[0]->manufacture_url,
						'strength'			=> $userdata[0]->strength,
						'formulation'		=> $userdata[0]->formulation,
						'packsize'			=> $userdata[0]->packsize,						
						'cost_quoted'		=> $userdata[0]->cost_quoted,
						'nof_doses'			=> $userdata[0]->nof_doses,
						'cost_per_udose'	=> $userdata[0]->cost_per_udose,
						'nof_dose_pday'		=> $userdata[0]->nof_dose_pday,
						'cost_28days'		=> $userdata[0]->cost_28days,
						'comments'			=> $userdata[0]->comments,
						'status'			=> $userdata[0]->status,
				); 	
		} 			
		if (!$_POST){
			$this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('drug_name','required');
            $post->add_rules('cost_quoted','required');
            $post->add_rules('nof_dose_pday','required');
		    $form = $post->as_array();
            if(!$post->validate()){            
				$this->template->content->form = $form;
				$this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!=''){
					if($form['action']=='duplicate')
						$this->drugs->add($form);
					else
						$this->drugs->update($form);
				}else
					$this->drugs->add($form);			
				url::redirect('/drugs');
            }
        } 
	}


	// Function to delete drugs from db //

	public function drug_delete($id='')
	{
	    $this->drugs = new Drugs_Model;
		$this->drugs->delete($id);
		url::redirect('/drugs');
	}	
}

?>