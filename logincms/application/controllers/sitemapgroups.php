<?php
class Sitemapgroups_Controller extends Website_Controller
{
	private $sitemap;  

    public function index()
    {
        url::redirect('sitemapgroups/page/1');
    }

	public function page($page_no,$sortname='smgroup_order',$sortorder='asc')
    {		
		$this->_page($page_no, $sortname, $sortorder);
    }

	
	public function group_add($id='')
	{
	    $this->sitemapgroups				= new Sitemapgroups_Model;
		$this->template->title				= 'NCCC CMS :: Add/Edit Sitemap - Groups';
        $this->template->content			= new View('sitemap/group_add');		
		$this->template->content->errors	= array();

		if(!$id){
				$form = array
				(
						'id'			=>'',
						'smgroup_title' =>'',
						'status'		=>'',
				); 	
			
		}else{
			$userdata	= $this->sitemapgroups->read($id);	
			$form = array
				(
						'id'			=> $userdata[0]->smgroup_id,
						'smgroup_title' => $userdata[0]->smgroup_title,
						'status'		=> $userdata[0]->smgroup_status,
				); 	
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{		
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('title','required');
            $post->add_rules('status', 'required');
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->sitemapgroups->update($form);
				else
					$this->sitemapgroups->add($form);
				url::redirect('/sitemapgroups');
            }
        } 
	}

	// Function to delete group from db //

	public function group_delete($id='')
	{
	    $this->sitemapgroups = new Sitemapgroups_Model;
		$this->sitemapgroups->delete($id);
		url::redirect('/sitemapgroups');
	}

	// private functions //

	private function _page($page_no,$sortname,$sortorder)
    {	
		$this->template->title = 'NCCC CMS :: Sitemap - Groups';
        $this->template->content = new View('sitemap/groups');
		if($sortorder=='asc')
			$revorder	=	'desc';
		else
			$revorder	=	'asc';

		$this->template->content->sortorder	 = $revorder;
		$this->template->content->page_no	 = $page_no;
		
		// Instantiate the model
        $this->sitemapgroups = new sitemapgroups_Model;
		
		$this->session = Session::instance();
		

		if(!empty($_POST))
			$_SESSION	=	$_POST;
		else
			$_SESSION	=	$_SESSION;		
		
		$paging = new Pagination(array
        (
           'total_items'	=> $this->sitemapgroups->count_links($_SESSION),
		   'items_per_page' => 15,
        ));

		// Render the page links
        $this->template->content->pagination = $paging->render('digg'); 	
		
		$this->template->content->search = $_SESSION; 

        // The result set is assigned to a variable called $users in the view
        $this->template->content->sitemapgroups = $this->sitemapgroups->browse($paging->items_per_page, $paging->sql_offset,$sortname,$sortorder, $_SESSION);

	}
}

?>