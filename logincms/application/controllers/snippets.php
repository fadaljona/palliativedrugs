<?php
class Snippets_Controller extends Website_Controller
{
	public function index()
    {
       $this->template->title = 'Palliative Drugs CMS :: Snippets';
       $this->template->content = new View('formulary/snippets'); 
    }	

	// Function to add/update users to the db //

	public function snippet_add($id='')
	{
	    $this->snippet						= new Snippets_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Snippets';
        $this->template->content			= new View('formulary/snippet_add');		
        $this->template->content->errors	= array();		
		if(!$id){
			    $this->template->content->list_lang	= $this->snippet->list_lang('0');	
				$form = array
				(
						'id'				=> '',
						'language_id'		=> '',
						'snippet_title'		=> '',
						'snippet_number'	=> '',
						'snippet_type'		=> '',
						'long_text'			=> '',
						'status'			=> '',
				); 	
			
		}else{
			$tempdata		= $this->snippet->read($id);
			$this->template->content->list_lang	= $this->snippet->list_lang($tempdata[0]->language_id);	
			$form = array
				(
						'id'				=> $tempdata[0]->snippet_id,
						'language_id'		=> $tempdata[0]->language_id,
						'snippet_title'		=> $tempdata[0]->snippet_title,
						'snippet_number'	=> $tempdata[0]->snippet_number,
						'snippet_type'		=> $tempdata[0]->snippet_type,
						'long_text'			=> $tempdata[0]->long_text,						
						'status'			=> $tempdata[0]->status,
				); 			
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('snippet_title','required');                              
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->snippet->update($form);
				else
					$this->snippet->add($form);
				url::redirect('/snippets');
            }
        } 
	}

	// Function to delete user from db //

	public function snippet_delete($id='')
	{
	    $this->snippet	= new Snippets_Model;
		$this->snippet->delete($id);
		url::redirect('/snippets');
	}	
}

?>