<?php 
class ManualorderController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$better_token = md5(uniqid(rand(), true));
		if(empty($ns->asess_id)){
		  $ns->asess_id = $better_token;
		}
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Manual Order');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('orders');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/manualorders');
	    }			
		$mo			=	new Application_Model_Manualorder();
		$cart_items =	$mo->get_cartitems();
		$this->view->assign('cart_items',$cart_items);
    }		

	public function addtocartAction()
	{
		$mo		=	new Application_Model_Manualorder();

		$mo->addtocart($_POST);

	}

	public function updatetocartAction()
	{
		$mo		=	new Application_Model_Manualorder();

		$mo->updatetocart($_POST);

	}

	public function deletefromcartAction()
	{
		$mo		=	new Application_Model_Manualorder();

		$mo->deletefromcart($_POST);

	}

	public function addcustomerAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add Customer');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('orders');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/manualorders');
	    }		
		$mo			=	new Application_Model_Manualorder();
		$cart_items =	$mo->cart_summary();
		$this->view->assign('cart_items',$cart_items);
    }	

	public function manualcustomerAction()
    {
		$mo		=	new Application_Model_Manualorder();

		$mo->manualcustomer($_POST);
    }	

	public function manualextcustomerAction()
    {
		$mo		=	new Application_Model_Manualorder();

		$mo->manualextcustomer($_POST);
    }	

	public function registercustomerAction()
    {
		$mo		=	new Application_Model_Manualorder();

		$mo->registercustomer($_POST);
    }	

	public function confirmorderAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Confirm Order');

		$mo			=	new Application_Model_Manualorder();

		$cart_items =	$mo->cart_confirm();

		$customer_details = $mo->customer_details();

		$this->view->assign('cart_items',$cart_items);

		$this->view->assign('customer_details',$customer_details);
    }
	
	public function checkoutAction()
    {
		$mo			=	new Application_Model_Manualorder();

		$mo->checkout($_POST);
		
		$this->_response->setRedirect($this->_baseurl.'/orders/list/type/New')->sendResponse();
    }
	
	public function updatetotalAction()
	{
		$mo			=	new Application_Model_Manualorder();

		$mo->updatetotal($_POST);
	}
}
