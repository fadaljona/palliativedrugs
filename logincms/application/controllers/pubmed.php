<?php
class Pubmed_Controller extends Website_Controller
{
	public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Pubmeds';
        $this->template->content = new View('formulary/pubmed');		
    }
	
	// Function to add/update pubmed to the db //

	public function pubmedref_add($id='')
	{
	    $this->pubmed					= new Pubmed_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Pubmed';
        $this->template->content			= new View('formulary/pubmedref_add');		
        $this->template->content->errors	= array();			
		$this->session = Session::instance();		
		if (!$_POST){

        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('pmid','required');
		    $form = $post->as_array();
            if(!$post->validate()){            
				$this->template->content->form = $form;
				$this->template->content->errors = $post->errors('form_error');
            }else{
				$this->pubmed->add($form);
				url::redirect('/pubmed');
            }
        } 
	}


	// Function to add/update pubmed to the db //

	public function manualref_add($id='')
	{
	    $this->pubmed						= new Pubmed_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Pubmed';
        $this->template->content			= new View('formulary/manualref_add');		
        $this->template->content->errors	= array();			
		if(!$id){
				$form = array
				(
						'id'			=>'',
						'pub_date'		=> '',
						'last_editor'	=> '',
						'title'			=> '',
						'full_journalname'=> '',
						'volume'		=> '',
						'issue'			=> '',
						'pages'			=> '',
						'content'		=> '',
				); 	
			
		}else{
			$userdata	= $this->pubmed->read($id);	
			$form = array
				(
						'id'			=> $userdata[0]->pubmed_id,
						'pub_date'		=> $userdata[0]->pub_date,
						'last_editor'	=> $userdata[0]->last_editor,
						'title'			=> $userdata[0]->title,
						'full_journalname'=> $userdata[0]->full_journalname,
						'volume'		=> $userdata[0]->volume,
						'issue'			=> $userdata[0]->issue,
						'pages'			=> $userdata[0]->pages,						
						'content'		=> $userdata[0]->content,
				); 	
		} 			
		if (!$_POST){
			$this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('pub_date','required');
            $post->add_rules('title','required');
            $post->add_rules('full_journalname','required');
		    $form = $post->as_array();
            if(!$post->validate()){            
				$this->template->content->form = $form;
				$this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->pubmed->manual_update($form);
				else
					$this->pubmed->manual_add($form);			
				url::redirect('/pubmed');
            }
        } 
	}


	// Function to delete formulary from db //

	public function formulary_delete($id='')
	{
	    $this->formulary = new Formulary_Model;
		$this->formulary->delete($id);
		url::redirect('/formulary');
	}	
}

?>