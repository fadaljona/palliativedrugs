<?php defined('SYSPATH') or die('No direct script access.');

class Website_Controller extends Template_Controller {

	public function __construct()
    {
        parent::__construct();

		$this->session = Session::instance();

		$session_user	=	$this->session->get('user_id');
	
		if(empty($session_user)){

		   $this->session->set(array('past_url'=>$_SERVER['REQUEST_URI']));
		   url::redirect('/'); 	
		   exit;
		}	

        
		if($_SERVER['REQUEST_URI']=='/logincms/'){
			$process	=	'home';
		}else{
			$pathinfo	=	explode("/",$_SERVER['PATH_INFO']);
			$process	=	$pathinfo[1];
		}

		switch($process){
			case "home":
				 $this->template->leftnav = 'Logged in user details.';
			break;
			case "users":
			case "usertypes":
				$this->template->leftnav = array
				(
					'Users'				=> "",
					'List Users'		=> "users",            
					'Add User'			=> "users/user_add",
					'User Types'		=> "",
					'List Types'		=> "usertypes",            
					'Add Type'			=> "usertypes/usertype_add"										          
				);
			break;
			case "pages":
			case "articles":
			case "templates":							
			case "language":			
				$this->template->leftnav = array
				(
					'Pages'				=> "",
					'List Pages'		=> "pages",            
					'Add Page'			=> "pages/page_add", 
					'Articles'			=> "",           
					'List Articles'		=> "articles",            
					'Add Article'		=> "articles/article_add",
					'Templates'			=> "",
					'List Templates'	=> "templates",            
					'Add Template'		=> "templates/temp_add",						
					'Languages'			=> "",
					'List Language'		=> "language",            
					'Add language'		=> "language/language_add"
				);
			break;
			case "formulary":
			case "pubmed":
			case "snippets":
			case "drugs":
				 $this->template->leftnav = array
				(
					'Formulary'		=> "",
					'List Formulary'	=> "formulary",            
					'Add Formulary'		=> "formulary/formulary_add",            
					'Pubmed References'	=> "",
					'List Pubmed Reference'	=> "pubmed",            
					'Add Pubmed Reference'	=> "pubmed/pubmedref_add", 
					'Add Manual Reference'	=> "pubmed/manualref_add",
					'Snippets'			=> "",
					'List Snippets'		=> "snippets",            
					'Add Snippet'		=> "snippets/snippet_add",
					'Supply Drugs'		=> "",           
					'List Supply Drugs'	=> "drugs",            
					'Add Supply Drugs'	=> "drugs/drug_add"
				);
			break;
			case "members":
				$this->template->leftnav = array
				(
					'Members'			=> "",
					'List Members'		=> "members",            
					'Add Members'			=> "members/member_add",            						   
				);
			break; 
			case "news":			
				$this->template->leftnav = array
				(
					'News'				=> "",
					'List News'			=> "news",            
					'Add News'			=> "news/news_add",            
				);
			break;
			case "assets":
			case "assetcat":
				$this->template->leftnav = array
				(
					'Assets'			=> "",
					'List Assets'		=> "assets",            
					'Add Asset'			=> "assets/asset_add",            
					'Asset Categories'  => "",
					'List Categories'	=> "assetcat",            
					'Add Category'		=> "assetcat/assetcat_add"            
				);
			break; 										
			default:
			   $this->template->leftnav = 'Logged in user details.';
			break;
		}

		

		// Add the following line.
		// By adding this we are making the database object available  
        // to all controllers that extend Website_Controller
        $this->db = Database::instance();

		$modules	=	'';

		$getmodules	=	$this->db->query("SELECT u.type_id,ut.module_permissions FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id AND u.user_id ='".$session_user."'")->result_array(FALSE);
		if(count($getmodules)>0){
			$modules	=	explode(",",$getmodules[0]['module_permissions']);
		}	

		$links		=	 array('Home'=> 'home');
		if(count($modules)>1){
			for($i=0;$i<count($modules);$i++){
				if($modules[$i]=='users'){
					$links['<img src=\'/logincms/assets/images/icons/user-silhouette.png\' alt=\'Users\' />Users'] =	$modules[$i];				
				}if($modules[$i]=='pages'){
					$links['<img src=\'/logincms/assets/images/icons/document-text-image.png\' alt=\'Pages\' />Pages'] =	$modules[$i];				
				}if($modules[$i]=='formulary'){
					$links['<img src=\'/logincms/assets/images/icons/application-table.png\' alt=\'Formulary\' />Formulary'] =	$modules[$i];				
				}if($modules[$i]=='members'){
					$links['<img src=\'/logincms/assets/images/icons/users.png\' alt=\'Members\' />Members'] =	$modules[$i];				
				}if($modules[$i]=='news'){
					$links['<img src=\'/logincms/assets/images/icons/newspaper.png\' alt=\'News\' />News'] =	$modules[$i];				
				}if($modules[$i]=='assets'){
					$links['<img src=\'/logincms/assets/images/icons/photo-album.png\' alt=\'Assets\' />Assets'] =	$modules[$i];				
				}
			}
		}else{
		   $links		=	  array('Home'=> 'home');
		}
		$this->template->links = $links;   
        
    }	  
}
