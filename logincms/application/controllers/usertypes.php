<?php
class Usertypes_Controller extends Website_Controller
{													 
    public function index()
    {
        $this->template->title = 'Palliative Drugs :: Usertypes';
        $this->template->content = new View('users/usertypes');
    }

	// Function to add/update users to the db //

	public function usertype_add($id='')
	{
	    $this->types						= new Usertypes_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit User Type';
        $this->template->content			= new View('users/usertype_add');		       
		$this->template->content->errors	= array();

		if(!$id){
				$form = array
				(
						'id'			=> '',
						'title'			=> '',
						'module_permissions'=>'',
						'status'		=> '',
				); 	
			
		}else{
			$userdata	= $this->types->read($id);	
			$form = array
				(
						'id'			=> $userdata[0]->type_id,
						'title'			=> $userdata[0]->title,	
						'module_permissions'=> $userdata[0]->module_permissions,						
						'status'		=> $userdata[0]->status,
				); 	
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('title','required');        
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->types->update($form);
				else
					$this->types->add($form);
				url::redirect('/usertypes');
            }
        } 
	}

	// Function to delete user from db //

	public function usertype_delete($id='')
	{
	    $this->types = new Usertypes_Model;
		$this->types->delete($id);
		url::redirect('/usertypes');
	}

}

?>