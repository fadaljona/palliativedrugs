<?php
class Language_Controller extends Website_Controller
{	
    public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Languages';
        $this->template->content = new View('pages/language');
    }

	// Function to add/update language to the db //

	public function language_add($id='')
	{
	    $this->lang							= new Language_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Language';
        $this->template->content			= new View('pages/language_add');		
        $this->template->content->errors	= array();
		$this->session = Session::instance();
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'language_name'		=> '',
						'language_code'		=> '',
						'is_default'		=> '',
				); 	
			
		}else{
			$langdata	= $this->lang->read($id);	
			$form = array
				(
						'id'				=> $langdata[0]->language_id,
						'language_name'		=> $langdata[0]->language_name,
						'language_code'		=> $langdata[0]->language_code,
						'is_default'		=> $langdata[0]->is_default,
				); 						
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('language_name','required');
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->lang->update($form);
				else
					$this->lang->add($form);
				url::redirect('/language');
            }
        } 
	}

	// Function to delete language from db //

	public function language_delete($id='')
	{
	    $this->lang		= new Language_Model;
		$this->lang->delete($id);
		url::redirect('/language');
	}	
}

?>