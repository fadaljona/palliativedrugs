<?php
class HomeController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Dashboard');	
		$db			= Zend_Registry::get("db");		
		$select		= 'SELECT audit_id,action_status,audit_history,module, DATE_FORMAT(action_date,"%D %M")as amonth FROM 3bit_audit_history WHERE 1 ORDER BY action_date DESC'; 
		$row		= $db->fetchAll($select);
		$this->view->assign('audits',$row);
		$request	=	$this->getRequest();	
		$get_action	=	$request->getParam("details");
		$this->view->assign('getaction',$get_action);	
		$denied_module	=	$request->getParam("denied");
		$this->view->assign('denied_module',$denied_module);
    }	
	
}
