<?php 
class OrdersController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Orders Dashboard');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('orders');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/orders');
	    }
		$orders				= new Application_Model_Orders();	
        $history			= $orders->order_history();
		$order_overview		= $orders->order_overview();
		$dash_orders		= $orders->dash_orders();
		$proforma_dash_orders	= $orders->proforma_dash_orders();
		$dash_awaiting		= $orders->dash_awaiting();
		$dash_dispatched	= $orders->dash_dispatched();
		$dash_sales			= $orders->dash_sales();
		$dash_refunds		= $orders->dash_refunds();		
		$this->view->assign('history',$history);	
		$this->view->assign('order_overview',$order_overview);	
		$this->view->assign('dash_orders',$dash_orders);	
		$this->view->assign('proforma_dash_orders',$proforma_dash_orders);
		$this->view->assign('dash_awaiting',$dash_awaiting);	
		$this->view->assign('dash_dispatched',$dash_dispatched);	
		$this->view->assign('dash_sales',$dash_sales);	
		$this->view->assign('dash_refunds',$dash_refunds);	
    }	

	public function listAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Orders List');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$type 		=	$request->getParam("type");	
		$years		=	$orders->getYears();
		$this->view->assign('type',$type);	
		$this->view->assign('years',$years);	
    }

	public function viewAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: View Order');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");	
		$details	=	$orders->getDetails($id);			
		$this->view->assign('details',$details);
		$this->view->assign('id',$id);
		if(!empty($_POST)){
			$newstatus = $orders->orderprocess($_POST);
			$this->_redirect('/orders/list/type/'.$newstatus);
		}	
    }

	public function printAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Print Order');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");			
		$this->view->assign('id',$id);	
		if(!empty($_POST)){
			$this->view->assign('data',$_POST);		
		}
		$this->_helper->layout->disableLayout();		
    }	

		public function editdeliveryAction()
    {
			$this->view->assign('title','Palliative Drugs CMS :: Edit Delivery');	
			$orders		=	new Application_Model_Orders();		
			$request	=	$this->getRequest();		
			$id 			=	$request->getParam("id");			
			$this->view->assign('id',$id);
			$details	=	$orders->getAddress('shipping',$id);
			$this->view->assign('details',$details);
			$this->view->assign('type','shipping');
			$country	=	$orders->getCountries();
			$this->view->assign('country',$country);
			if(!empty($_POST)){
				$orders->updateaddress();
				$this->_redirect('/orders/editdelivery/id/'.$id);
			}
			$this->_helper->layout->disableLayout();		
    }	

	public function editinvoiceAction()
    {
			$this->view->assign('title','Palliative Drugs CMS :: Edit Invoice');	
			$orders		=	new Application_Model_Orders();		
			$request	=	$this->getRequest();		
			$id 			=	$request->getParam("id");			
			$this->view->assign('id',$id);
			$details	=	$orders->getAddress('billing',$id);
			$this->view->assign('details',$details);
			$this->view->assign('type','billing');
			$country	=	$orders->getCountries();
			$this->view->assign('country',$country);
			if(!empty($_POST)){
				$orders->updateaddress();
				$this->_redirect('/orders/editinvoice/id/'.$id);
			}
			$this->_helper->layout->disableLayout();		
    }	


	public function refundprintAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Print Order');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");			
		$this->view->assign('id',$id);			
		$this->_helper->layout->disableLayout();		
    }	

	public function refundAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Refund Order');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");			
		$this->view->assign('id',$id);	
		if(!empty($_POST)){
			$this->view->assign('data',$_POST);		
			$this->view->assign('orders',$orders);		
		}
		$this->_helper->layout->disableLayout();		
    }	

	public function adminhistoryAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Admin History');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");			
		$history	=	$orders->adminhistory($id);
		$this->view->assign('history',$history);
		$this->_helper->layout->disableLayout();
    }

	public function orderhistoryAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Order History');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");			
		$history	=	$orders->orderhistory($id);
		$this->view->assign('history',$history);
		$this->_helper->layout->disableLayout();
    }

	public function customerhistoryAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Customer History');	
		$orders		=	new Application_Model_Orders();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");			
		$history	=	$orders->customerhistory($id);
		$this->view->assign('history',$history);
		$this->_helper->layout->disableLayout();
    }

	public function addhistoryAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add History');	
		$orders		=	new Application_Model_Orders();		
		$orders->add_history($_POST);
    }

	public function addchistoryAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add History');	
		$orders		=	new Application_Model_Orders();		
		$orders->add_chistory($_POST);
    }

	public function downloadsAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Manage Downloads');	
	}

	public function resetdownloadAction()
  {
		$orders		=	new Application_Model_Orders();		
		$orders->resetdownload();
	}

	public function resendlinkAction()
  {
		$orders		=	new Application_Model_Orders();		
		$orders->resendlink();
	}

	public function orderauditAction()
	 {
			$orders		=	new Application_Model_Orders();		
			$orders->addorderaudit();
	 }

	public function generateproformacsvAction()
    {
			$this->view->assign('title','Palliative Drugs CMS :: Proforma Orders');	
			$orders		=	new Application_Model_Orders();	
    }	
	
}
