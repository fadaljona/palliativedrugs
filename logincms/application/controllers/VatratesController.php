<?php 
class VatratesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Vat Rates');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('products');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/vatrates');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Vat');	
		$vats		=	new Application_Model_Vatrates();			
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'vat_code'		=> '',
						'vat_rate'		=> '',
						'is_live'		=> '',
						'status'		=> '',
				); 	
			
		}else if($id && !$_POST){
			$vdata	= $vats->read($id);
			$form = array
				(
						'id'			=> $vdata[0]['vat_id'],
						'vat_code'		=> $vdata[0]['vat_code'],
						'vat_rate'		=> $vdata[0]['vat_rate'],
						'is_live'		=> $vdata[0]['is_live'],							
						'status'		=> $vdata[0]['status'],
				); 	 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);			
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$vats->update($form);
			else
				$vats->add($form);
			$this->_redirect('/vatrates');
       } 		
    }
	
	public function deleteAction()
	{
	    $vats		=	new Application_Model_Vatrates();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$vats->delete($id);
		$this->_redirect('/vatrates');
	}
}
