<?php 
class ReportsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Sales Reports');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('reports');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/reports');
	    }
		$reps			= new Application_Model_Reports; 
        $products		= $reps->top_products();       
        $lproducts		= $reps->top_lproducts();
		$this->view->assign('products',$products);
		$this->view->assign('lproducts',$lproducts);
    }	
	
	public function searchordersAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Search Orders');	
		$reps			= new Application_Model_Reports;   		
		if (!$_POST){
			$ayear			= $reps->acc_year('0');
			$this->view->assign('ayear',$ayear);
			$form = array
			(
					'txt_cust'	=>'',
					'selayear'	=> '',
					'txt_start' => '',
					'txt_end'	=> '',
					'selstatus'	=> '',				
			); 
            $this->view->assign('form',$form);
            $this->view->assign("orders",'');
        }else{			           
		    $form	=	array_map("trim",$_POST);
			$ayear	=	$reps->acc_year($form['selayear']);
			$this->view->assign('ayear',$ayear);
			$this->view->assign('form',$form);
			$this->view->assign("orders",$reps->order_list($form));
		}
    }	
	
	public function salesanalyticsAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Sales Analytics');	
		$reps			= new Application_Model_Reports;   		
		if (!$_POST){
			$ayear			= $reps->acc_year('0');
			$this->view->assign('ayear',$ayear);
			$form = array
			(
					'report_type'	=>'',
					'display_type'	=> '',
					'quick_date'	=> '',
					'start_date'	=> '',
					'end_date'		=> '',				
			); 
            $this->view->assign('form',$form);
			$this->view->assign('results','');
        }else{			           
		    $form	=	array_map("trim",$_POST);			
			$results=	$reps->getresults($form);
			$this->view->assign('form',$form);
			$this->view->assign('results',$results);
		}
    }		

	public function generatecsvAction()
    {
		$this->view->assign('title','BTOI CMS :: Sales Analytics');	
		$reps			= new Application_Model_Reports;   		
		
    }	
	
	public function exportxlsAction()
    {
		$this->view->assign('title','BTOI CMS :: Sales Export');	
		$reps			= new Application_Model_Reports;		
    }

		public function portalstatsAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Portal Stats');	
		$reps			= new Application_Model_Reports;
		$portalstats=	$reps->getPortalStats();
		$this->view->assign('portalstats',$portalstats);
    }	

	public function productsalesAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Product Sales');	
		$reps			= new Application_Model_Reports;			 		
		if (!$_POST){
			$products		=	$reps->all_products_list(0);
			$this->view->assign('products',$products);
		}else{
			$products		=	$reps->all_products_list($_POST['txt_prod']);
			$orders			= $reps->sales_list($_POST);
			$this->view->assign('products',$products);
			$this->view->assign('orders',$orders);
		}
    }	
}
