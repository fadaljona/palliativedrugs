<?php 
class AssetcatController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Asset Categories');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('assets');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/assetcategory');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Asset Category');	
		$cats		=	new Application_Model_Assetcat();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'						=> '',
						'asset_category_title'		=> '',
						'asset_category_type'		=> '',
						'asset_category_description'=> '',
						'status'					=> '',
				); 	
			
		}else if($id && !$_POST){
			$catdata	= $cats->read($id);
			$form = array
				(
						'id'						=> $catdata[0]['asset_category_id'],
						'asset_category_title'		=> $catdata[0]['asset_category_title'],
						'asset_category_type'		=> $catdata[0]['asset_category_type'],
						'asset_category_description'=> $catdata[0]['asset_category_description'],						
						'status'					=> $catdata[0]['status'],					
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$cats->update($form);
			else
				$cats->add($form);
			$this->_redirect('/assetcat');
       } 		
    }
	
	public function deleteAction()
	{
	    $cats		=	new Application_Model_Assetcat();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$cats->delete($id);
		$this->_redirect('/assetcat');
	}
}
