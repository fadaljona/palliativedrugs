<?php

class Users_Controller extends Website_Controller

{

	private $users;  



    public function index()

    {

        $this->template->title = 'Palliative Drugs :: Users';

        $this->template->content = new View('users/users');

    }



	// Function to add paging with the users listing //



	public function page($page_no,$sortname='user_firstname',$sortorder='asc')

    {		

		$this->_page($page_no, $sortname, $sortorder);

    }



	// Function to add/update users to the db //



	public function user_add($id='')

	{

	    $this->users						= new Users_Model;

		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit User';

        $this->template->content			= new View('users/user_add');		

        $this->template->content->types		= $this->users->active_types();		

		$this->template->content->errors	= array();



		if(!$id){

				$form = array

				(

						'id'			=>'',

						'user_firstname'=> '',

						'user_lastname'	=> '',

						'user_email'	=> '',

						'user_username'	=> '',

						'user_password'	=> '',

						'type_id'		=> '',

						'status'		=> '',

				); 	

			

		}else{

			$userdata	= $this->users->read($id);	

			$form = array

				(

						'id'			=>	$userdata[0]->user_id,

						'user_firstname'=> $userdata[0]->user_firstname,

						'user_lastname' => $userdata[0]->user_lastname,

						'user_email'	=> $userdata[0]->user_email,

						'user_username'	=> $userdata[0]->user_username,

						'user_password' => $userdata[0]->user_password,

						'type_id'		=> $userdata[0]->type_id,						

						'status'		=> $userdata[0]->status,

				); 	

		} 	



		if (!$_POST){

            $this->template->content->form = $form;

        }else{

            $post = new Validation($_POST);

            $post->pre_filter('trim', TRUE);

            $post->add_rules('user_firstname','required');

            $post->add_rules('user_lastname', 'required');

            $post->add_rules('user_email', 'required','email');

			$post->add_rules('user_username', 'required');

            $post->add_rules('user_password','required');          

          	$post->add_rules('type_id','required');

		    $form = $post->as_array();

            if(!$post->validate()){            

             $this->template->content->form = $form;

             $this->template->content->errors = $post->errors('form_error');

            }else{

				if($form['id']!='')

					$this->users->update($form);

				else

					$this->users->add($form);

				url::redirect('/users');

            }

        } 

	}



	// Function to delete user from db //



	public function user_delete($id='')

	{

	    $this->users = new Users_Model;

		$this->users->delete($id);

		url::redirect('/users');

	}

}



?>