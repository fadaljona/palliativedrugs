<?php
class Pages_Controller extends Website_Controller
{

    public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Pages';
        $this->template->content = new View('pages/pages');
		
		// Instantiate the model
        $pages = new Pages_Model;  

        // The result set is assigned to a variable called $pages in the view
        $this->template->content->global_nav = $pages->cat_list('0','1','Global Nav');
        $this->template->content->sglobal_nav = $pages->cat_list('0','1','Secondary Global Nav');
        $this->template->content->footer_nav = $pages->cat_list('0','1','Footer Nav');
        $this->template->content->article_nav = $pages->cat_list('0','1','Article Nav'); 
    }

	// Function to add/update page to the db //

	public function page_add($id=''){
	    $this->page							= new Pages_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Pages';
        $this->template->content			= new View('pages/page_add');		
        $this->template->content->errors	= array();	
		$this->template->content->types		= $this->page->active_types();	
		$this->template->content->snippets	= $this->page->active_snippets();	
		if(!$id){
				$this->template->content->list_cat	= $this->page->list_cat('0','','0');
				$this->template->content->list_temp	= $this->page->list_temp('0');
				$this->template->content->list_art	= $this->page->list_art('0');				
				$this->template->content->list_lang	= $this->page->list_lang('0');				
				$form = array
				(
						'id'				=> '',
						'cat_title'			=> '',
						'menu_title'		=> '',
						'cat_type'			=> '',
						'cat_parent_id'		=> '',
						'cat_url'			=> '',						
						'temp_id'			=> '',
						'article_id'		=> '',
						'ext_url'			=> '',
						'menu_item'			=> '',
						'temp_filename'		=> '',
						'article_title'		=> '',						
						'start_date'		=> '',
					    'end_date'			=> '',
						'login_required'	=> '',
						'type_id'			=> '',
						'css_class'			=> '',	
						'cat_schedule'		=> '',
						'status'			=> '',						
						'shour'				=> '',
						'smin'				=> '',
						'sform'				=> '',
						'ehour'				=> '',
						'emin'				=> '',
						'eform'				=> '',
				); 	
			
		}else{
			$pagedata			= $this->page->read($id);			
			$this->template->content->list_cat	= $this->page->list_cat('0','',$pagedata[0]->cat_parent_id);
			$this->template->content->list_temp	= $this->page->list_temp($pagedata[0]->temp_id);
			$this->template->content->list_art	= $this->page->list_art($pagedata[0]->article_id);	
			$this->template->content->list_lang	= $this->page->list_lang($pagedata[0]->language_id);	
			$form = array
				(
						'id'				=> $pagedata[0]->cat_id,
						'cat_title'			=> $pagedata[0]->cat_title,
						'menu_title'		=> $pagedata[0]->menu_title,
						'cat_type'			=> $pagedata[0]->cat_type,
						'cat_parent_id'		=> $pagedata[0]->cat_parent_id,
						'cat_url'			=> $pagedata[0]->cat_url,
						'temp_id'			=> $pagedata[0]->temp_id,
						'article_id'		=> $pagedata[0]->article_id,
						'ext_url'			=> $pagedata[0]->ext_url,
						'menu_item'			=> $pagedata[0]->menu_item,
						'temp_filename'		=> $pagedata[0]->temp_filename,
						'article_title'		=> $pagedata[0]->article_title,
						'start_date'		=> $pagedata[0]->start_date,
					    'end_date'			=> $pagedata[0]->end_date,
						'login_required'	=> $pagedata[0]->login_required,						
						'type_id'			=> $pagedata[0]->type_id,
						'css_class'			=> $pagedata[0]->css_class,	
						'cat_schedule'		=> $pagedata[0]->cat_schedule,
						'status'			=> $pagedata[0]->status,						
						'shour'				=> $pagedata[0]->shour,
						'smin'				=> $pagedata[0]->smin,
						'sform'				=> $pagedata[0]->sform,
						'ehour'				=> $pagedata[0]->ehour,
						'emin'				=> $pagedata[0]->emin,
						'eform'				=> $pagedata[0]->eform,
				); 			
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('cat_title','required');                              
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->page->update($form);
				else
					$this->page->add($form);
				url::redirect('/pages');
            }
        } 
	}

	// Function to delete page from db //

	public function page_delete($id='')
	{
	    $this->pages = new Pages_Model;
		$this->pages->delete($id);
		url::redirect('/pages');
	}

}

?>