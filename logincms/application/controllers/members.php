<?php
class Members_Controller extends Website_Controller
{
	public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Members';
        $this->template->content = new View('members/members');		
    }
	
	// Function to add/update drug to the db //

	public function member_add($id='')
	{
	    $this->member						= new Members_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Member';
        $this->template->content			= new View('members/member_add');		
        $this->template->content->errors	= array();
		$this->template->content->roles		= $this->member->active_roles();
		$this->template->content->speciality= $this->member->active_speciality();
		$this->template->content->sub_spec  = $this->member->active_sub_spec();
		$this->template->content->country   = $this->member->active_country();
		$this->template->content->place		= $this->member->active_place();
		$this->template->content->post		= $this->member->active_post();
		if(!$id){																  					
				$form = array
				(
						'id'				=>'',
						'username'			=> '',
						'email'				=> '',
						'pwd'				=> '',
						'FirstName'			=> '',
						'LastName'			=> '',
						'title'				=> '',
						'role'				=> '',
						'prof_reg_num'		=> '',
						'nurse_prescriber'	=> '',
						'speciality'		=> '',
						'sub_speciality'	=> '',
						'organisation'		=> '',
						'addr1'				=> '',
						'addr2'				=> '',
						'addr3'				=> '',
						'addr4'				=> '',
						'TownOrCity'		=> '',
						'StateOrCounty'		=> '',
						'PostalCode'		=> '',
						'Country'			=> '',
						'phone'				=> '',
						'main_workplace'	=> '',
						'current_post'		=> '',
						'years_in_post'		=> '',
						'patient_care_time'	=> '',
						'gender'			=> '',
						'year_of_birth'		=> '',
						'lead'				=> '',
						'accept_site_emails'=> '',
						'accept_bb_emails'	=> '',
						'UserType'			=> '',
						'DateRegistered'	=> '',
						'RegistrationChecked'=>'',
						'vetted'			=> '',
						'previous_visit'	=> '',
						'current_visit'		=> '',
						'user_confirmed'	=> '', 						
						'active'			=> '',
				); 	
			
		}else{
			$memdata	= $this->member->read($id);	
			$form = array
				(
						'id'				=> $memdata[0]->id,
						'username'			=> $memdata[0]->username,
						'email'				=> $memdata[0]->email,
						'pwd'				=> $memdata[0]->pwd,
						'FirstName'			=> $memdata[0]->FirstName,
						'LastName'			=> $memdata[0]->LastName,
						'title'				=> $memdata[0]->title,
						'role'				=> $memdata[0]->role,
						'prof_reg_num'		=> $memdata[0]->prof_reg_num,
						'nurse_prescriber'	=> $memdata[0]->nurse_prescriber,
						'speciality'		=> $memdata[0]->speciality,
						'sub_speciality'	=> $memdata[0]->sub_specialty,
						'organisation'		=> $memdata[0]->organisation,
						'addr1'				=> $memdata[0]->addr1,
						'addr2'				=> $memdata[0]->addr2,
						'addr3'				=> $memdata[0]->addr3,
						'addr4'				=> $memdata[0]->addr4,
						'TownOrCity'		=> $memdata[0]->TownOrCity,
						'StateOrCounty'		=> $memdata[0]->StateOrCounty,
						'PostalCode'		=> $memdata[0]->PostalCode,
						'Country'			=> $memdata[0]->Country,
						'phone'				=> $memdata[0]->phone,
						'main_workplace'	=> $memdata[0]->main_workplace,
						'current_post'		=> $memdata[0]->current_post,
						'years_in_post'		=> $memdata[0]->years_in_post,
						'patient_care_time'	=> $memdata[0]->patient_care_time,
						'gender'			=> $memdata[0]->gender,
						'year_of_birth'		=> $memdata[0]->year_of_birth,
						'lead'				=> $memdata[0]->lead,
						'accept_site_emails'=> $memdata[0]->accept_site_emails,
						'accept_bb_emails'	=> $memdata[0]->accept_bb_emails,
						'UserType'			=> $memdata[0]->UserType,
						'DateRegistered'	=> $memdata[0]->DateRegistered,
						'RegistrationChecked'=>$memdata[0]->RegistrationChecked,
						'vetted'			=> $memdata[0]->vetted,
						'previous_visit'	=> $memdata[0]->previous_visit,
						'current_visit'		=> $memdata[0]->current_visit,
						'user_confirmed'	=> $memdata[0]->user_confirmed, 						
						'active'			=> $memdata[0]->active,
				); 	
		} 			
		if (!$_POST){
			$this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('FirstName','required');
            $post->add_rules('LastName','required');
            $post->add_rules('role','required');
            $post->add_rules('prof_reg_num','required');
            $post->add_rules('speciality','required');
            $post->add_rules('sub_speciality','required');
            $post->add_rules('organisation','required');
            $post->add_rules('addr1','required');
            $post->add_rules('PostalCode','required');
            $post->add_rules('phone','required');
            $post->add_rules('main_workplace','required');
            $post->add_rules('current_post','required');
            $post->add_rules('username','required');
            $post->add_rules('email','required');
		    $form = $post->as_array();
            if(!$post->validate()){            
				$this->template->content->form = $form;
				$this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')					
					$this->member->update($form);
				else
					$this->member->add($form);			
				url::redirect('/members');
            }
        } 
	}


	// Function to delete members from db //

	public function member_delete($id='')
	{
	    $this->member = new Members_Model;
		$this->member->delete($id);
		url::redirect('/members');
	}	
}

?>