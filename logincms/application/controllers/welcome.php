<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html lang='en-US' xml:lang='en-US' xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta content='text/html; charset=iso-8859-1' http-equiv='Content-type' />
<title>NCCC LOGIN</title>
<link href="https://beta.gist.com/stylesheets/site_v2.css?1268792730" media="screen" rel="stylesheet" type="text/css" />
<link href="https://beta.gist.com/stylesheets/v2/pages/onboard_page.css?1268792730" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
<div class='onboard_page' id='page'>
	<div class='login_page preserve_links'>	
		<div class='frame'>
			<div class='frame_top'>
				<div class='corners_left'></div>
					<div class='inner'>
						<h2>Sign in</h2>
					</div>
				<div class='corners_right'></div>
		  </div>
		  <div class='frame_content'>
				<div class='flash_boxes'>
					<p class='error' style='display:none'>
						Email address or password incorrect or account not activated
					</p>
				</div>
				<form action="/sessions" class="onboard_form" method="post">
					<div class='field text_field'>
						<label for="email">User Name</label>
						<input class="autotab behavior" id="username" name="username" type="text" value="" />
					</div>
					<div class='field text_field'>
						<label for="password">Password</label>
						<input id="password" name="password" type="password" value="" />
					</div>			
					<div class='actions'>
							<div class="right gistsubmit"><input name="commit" type="submit" value="Sign In" /><span></span></div>
							<div class="clear"></div>
					</div>
				</form>
		</div>
	</div>
	<div id='footer'>
	&copy; 2007-2010 Gist
	</div>
	</div>
</div>
</body>
</html>
