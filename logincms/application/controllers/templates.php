<?php
class Templates_Controller extends Website_Controller
{
	public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Templates';
        $this->template->content = new View('pages/templates');
    }

	// Function to add/update users to the db //

	public function temp_add($id='')
	{
	    $this->temps						= new Templates_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Article Templates';
        $this->template->content			= new View('pages/temp_add');		
        $this->template->content->errors	= array();

		if(!$id){
				$form = array
				(
						'id'				=> '',
						'temp_filename'		=> '',
						'temp_content'		=> '',
						'custom_template'	=> '',
						'temp_thumbs'		=> '',
						'temp_thumbs_img'	=> '',
						'temp_status'		=> '',
				); 	
			
		}else{
			$tempdata		= $this->temps->read($id);	
			$form = array
				(
						'id'				=> $tempdata[0]->temp_id,
						'temp_filename'		=> $tempdata[0]->temp_filename,
						'temp_content'		=> $tempdata[0]->temp_content,
						'custom_template'	=> $tempdata[0]->custom_template,
						'temp_thumbs'		=> $tempdata[0]->temp_thumbs,
						'temp_thumbs_img'	=> $tempdata[0]->temp_thumbs_img,
						'temp_status'		=> $tempdata[0]->temp_status,
				); 			
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('temp_filename','required');                              
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->temps->update($form);
				else
					$this->temps->add($form);
				url::redirect('/templates');
            }
        } 
	}

	// Function to delete user from db //

	public function temp_delete($id='')
	{
	    $this->temps	= new Templates_Model;
		$this->temps->delete($id);
		url::redirect('/templates');
	}	
}

?>