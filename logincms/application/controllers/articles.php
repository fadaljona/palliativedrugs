<?php
class Articles_Controller extends Website_Controller
{
	private $articles;  

    public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Articles';
        $this->template->content = new View('pages/articles');
    }

	// Function to add/update articles to the db //

	public function article_add($id='')
	{
	    $this->articles						= new Articles_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Article';
        $this->template->content			= new View('pages/article_add');		
        $this->template->content->errors	= array();
		$this->template->content->snippets	= $this->articles->active_snippets();	
		$this->session = Session::instance();
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'article_title'		=> '',
						'owner'				=> '',
						'user_id'			=> '',
						'article_date'		=> '',
						'article_sap'		=> '',
						'article_enddate'	=> '',
						'article_eap'		=> '',
						'short_text'		=> '',
						'long_text'			=> '',
						'header_image'		=> '',
						'article_image'		=> '',
						'header_imageid'	=> '',
						'article_imageid'	=> '',
						'header_image_img'	=> '',
						'article_image_img'	=> '',
						'image_text'		=> '',
						'meta_title'		=> '',
						'meta_desc'			=> '',
						'meta_keywords'		=> '',
						'status'			=> '',
				); 	
			
		}else{
			$userdata	= $this->articles->read($id);	
			$form = array
				(
						'id'				=> $userdata[0]->article_id,
						'article_title'		=> $userdata[0]->article_title,
						'owner'				=> $userdata[0]->owner,
						'article_date'		=> $userdata[0]->article_date,
						'article_sap'		=> $userdata[0]->article_sap,
						'article_enddate'	=> $userdata[0]->article_enddate,
						'article_eap'		=> $userdata[0]->article_eap,
						'short_text'		=> $userdata[0]->short_text,
						'long_text'			=> $userdata[0]->long_text,
						'header_image'		=> $userdata[0]->header_image,
						'article_image'		=> $userdata[0]->article_image,
						'header_imageid'	=> $userdata[0]->header_image,
						'article_imageid'	=> $userdata[0]->article_image,
						'header_image_img'  => $userdata[0]->header_image_img,
						'article_image_img'	=> $userdata[0]->article_image_img,
						'image_text'		=> $userdata[0]->image_text,
						'meta_title'		=> $userdata[0]->meta_title,
						'meta_desc'			=> $userdata[0]->meta_desc,
						'meta_keywords'		=> $userdata[0]->meta_keywords,
						'status'			=> $userdata[0]->status,
				); 						
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('article_title','required');
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->articles->update($form);
				else
					$this->articles->add($form);
				url::redirect('/articles');
            }
        } 
	}

	// Function to delete user from db //

	public function article_delete($id='')
	{
	    $this->articles = new Articles_Model;
		$this->articles->delete($id);
		url::redirect('/articles');
	}	
}

?>