<?php
class Assets_Controller extends Website_Controller
{	
    public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Assets';
        $this->template->content = new View('assets/assets');
    }
	
	// Function to add/update users to the db //

	public function asset_add($id='')
	{
	    $this->assets						= new Assets_Model;
		$this->template->title				= 'Palliative Drugs CMS :: Add/Edit Assets';
        $this->template->content			= new View('assets/asset_add');		
        $this->template->content->assetcats	= $this->assets->active_cats();		
		$this->template->content->errors	= array();

		if(!$id){
				$form = array
				(
						'id'				=> '',
						'asset_title'		=> '',
						'asset_name'		=> '',
						'asset_alt'			=> '',
						'asset_height'		=> '',
						'asset_width'		=> '',
						'asset_size'		=> '',
						'asset_category_id' => '',						
						'status'			=> '',
				); 	
			
		}else{
			$userdata	= $this->assets->read($id);			
			$form = array
				(
						'id'				=> $userdata[0]->asset_id,
						'asset_title'		=> $userdata[0]->asset_title,
						'asset_name'		=> $userdata[0]->asset_name,
						'asset_alt'			=> $userdata[0]->asset_alt,
						'asset_height'		=> $userdata[0]->asset_height,
						'asset_width'		=> $userdata[0]->asset_width,
						'asset_size'		=> $userdata[0]->asset_size,
						'asset_category_id'	=> $userdata[0]->asset_category_id,
						'status'			=> $userdata[0]->status,
				); 	
		} 
		
		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('asset_title','required');
            $post->add_rules('asset_alt', 'required');           
		    $form = $post->as_array();
			$form['asset_name'] = $_FILES['asset_name'];
		    if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->assets->update($form);
				else
					$this->assets->add($form);
				url::redirect('/assets');
            }
        } 
	}

	// Function to delete user from db //

	public function asset_delete($id='')
	{
	    $this->assets = new Assets_Model;
		$this->assets->delete($id);
		url::redirect('/assets');
	}	
}

?>