<?php 
class ShippingsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Shippings');		
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('products');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/shipping');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Shipping');	
		$ships		=	new Application_Model_Shippings();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=>'',
						'weight'		=> '',
						'zone1_charge'	=> '',
						'zone2_charge'	=> '',
						'zone3_charge'	=> '',
				); 	
			
		}else if($id && !$_POST){
			$shipdata	= $ships->read($id);
			$form = array
				(
						'id'			=> $shipdata[0]['ship_id'],
						'weight'		=> $shipdata[0]['weight'],
						'zone1_charge'  => $shipdata[0]['zone1_charge'],
						'zone2_charge'	=> $shipdata[0]['zone2_charge'],
						'zone3_charge'	=> $shipdata[0]['zone3_charge'],		
				); 	 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);			
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$ships->update($form);
			else
				$ships->add($form);
			$this->_redirect('/shippings');
       } 		
    }
	
	public function deleteAction()
	{
	    $ships		=	new Application_Model_Shippings();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$ships->delete($id);
		$this->_redirect('/shippings');
	}
}
