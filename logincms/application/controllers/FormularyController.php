<?php 
class FormularyController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns			=	Zend_Registry::get("ns");		
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
       /* if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }*/
		$better_token = md5(uniqid(rand(), true));
		if(empty($ns->asess_id)){
		  $ns->asess_id = $better_token;
		}
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Formulary');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('formulary');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/formulary');
	    }
		$formulary	=	new Application_Model_Formulary();
		$levels		=   $formulary->getLevels();	
		$this->view->assign('levels',$levels);
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Formulary');	
		$formulary	=	new Application_Model_Formulary();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$levels		=   $formulary->getLevels();	
		if(!$id){
				$parent_list= $formulary->parent_list('0','','0');
				$list_lang	= $formulary->list_lang('0');	
				$form = array
				(
						'id'				=> '',
						'language_id'		=> '',
						'formulary_title'	=> '',
						'alias'				=> '',
						'formulary_type'	=> '',
						'formulary_parent_id'=> '',
						'quick_practice_guide'=>'',
						'url'				=> '',
						'bnf_reference'		=> '',					
						'formulary_level'	=> '',					
						'country'			=> '',
						'blocks'			=> '',
						'publish_status'	=> '',
						'formulary_tags'	=> '',
						'status'			=> '',
						'pre_text_article'	=> '',	
						'class'				=> '',
						'indication'		=> '',
						'pharmacology'		=> '',
						'cautions'			=> '',
						'drug_interactions'	=> '',
						'undesirable_effects'=> '',
						'dose_and_use'		=> '',
						'overdose'			=> '',
						'death_rattle'		=> '',
						'supply'			=> '',
						'references'		=> '',
						'post_text_article'	=> '', 
				); 	
			
		}else if($id && !$_POST){
			$formdata	= $formulary->read($id);
			$parent_list= $formulary->parent_list('0','',$formdata[0]['formulary_parent_id']);
			$list_lang	= $formulary->list_lang($formdata[0]['language_id']);
			$block1 ='';$block2='';	$block3='';	$block4 =''; $block5='';$block6='';	$block7 ='';$block8='';$block9='';$block10 =''; $block11=''; $block12='';	
			if(!empty($formdata['pre_text_article']))
				$block1	=	$formdata['pre_text_article'];
			if(!empty($formdata['class']))
				$block2	=	$formdata['class'];
			if(!empty($formdata['indication']))
				$block3	=	$formdata['indication'];
			if(!empty($formdata['pharmacology']))
				$block4	=	$formdata['pharmacology'];
			if(!empty($formdata['cautions']))
				$block5	=	$formdata['cautions'];
			if(!empty($formdata['drug_interactions']))
				$block6	=	$formdata['drug_interactions'];
			if(!empty($formdata['undesirable_effects']))
				$block7	=	$formdata['undesirable_effects'];
			if(!empty($formdata['dose_and_use']))
				$block8	=	$formdata['dose_and_use'];
			if(!empty($formdata['overdose']))
				$block9	=	$formdata['overdose'];
			if(!empty($formdata['death_rattle']))
				$block10	=	$formdata['death_rattle'];
			if(!empty($formdata['supply']))
				$block11=	$formdata['supply'];
			if(!empty($formdata['references']))
				$block12=	$formdata['references'];
			if(!empty($formdata['post_text_article']))
				$block13=	$formdata['post_text_article'];
			
			$form = array
				(
						'id'				=> $formdata[0]['formulary_id'],
						'language_id'		=> $formdata[0]['language_id'],
						'formulary_title'	=> $formdata[0]['formulary_title'],
						'alias'				=> $formdata[0]['alias'],
						'formulary_type'	=> $formdata[0]['formulary_type'],
						'formulary_parent_id'=>$formdata[0]['formulary_parent_id'],
						'quick_practice_guide'=>$formdata[0]['quick_practice_guide'],
						'url'				=> $formdata[0]['url'],
						'bnf_reference'		=> $formdata[0]['bnf_reference'],
						'formulary_level'	=> $formdata[0]['formulary_level'],			
						'country'			=> $formdata[0]['country'],
						'blocks'			=> $formdata[0]['blocks'],
						'publish_status'	=> $formdata[0]['publish_status'],
						'formulary_tags'	=> $formdata[0]['formulary_tags'],	
						'status'			=> $formdata[0]['status'],						
						'pre_text_article'	=> $block1,	
						'class'				=> $block2,
						'indication'		=> $block3,
						'pharmacology'		=> $block4,
						'cautions'			=> $block5,
						'drug_interactions'	=> $block6,
						'undesirable_effects'=> $block7,
						'dose_and_use'		=> $block8,
						'overdose'			=> $block9,
						'death_rattle'		=> $block10,
						'supply'			=> $block11,
						'references'		=> $block12,
						'post_text_article'	=> $block13, 
				); 		
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
            $this->view->assign('list_lang',$list_lang);
            $this->view->assign('parent_list',$parent_list);
			$this->view->assign('levels',$levels);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!=''){
				if($form['action']=='duplicate')
					$formulary->add($form);
				else if($form['action']=='save_version')
					$formulary->save_version($form);
				else
					$formulary->update($form);
			}else
				$formulary->add($form);
			$this->_redirect('/formulary');
       } 		
    }
	
	public function deleteAction()
	{
	    $formulary	=	new Application_Model_Formulary();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$formulary->delete($id);
		$this->_redirect('/formulary');
	}

	public function previewAction()
	{
	    $formulary	=	new Application_Model_Formulary();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$version	=	$request->getParam("version");	
		$edit		=	$request->getParam("edit");	
		$this->view->assign('title','Palliative Drugs CMS :: Preview Formulary');	
		$this->view->assign('id',$id);	
		$this->view->assign('version',$version);	
		$this->view->assign('edit',$edit);	
		$this->_helper->layout->disableLayout();
	}

	public function updatetextareaAction(){	
		$formulary	=	new Application_Model_Formulary();
		$form		=	array_map("trim",$_POST);
		$formulary->updatetextarea($form);
	}

	public function updateblocksAction(){	
		$formulary	=	new Application_Model_Formulary();
		$form		=	array_map("trim",$_POST);
		$formulary->updateblocks($form);
	}

	public function updatesnippetareaAction(){	
		$formulary	=	new Application_Model_Formulary();
		$form		=	array_map("trim",$_POST);
		$formulary->updatesnippetarea($form);
	}

	public function updatestatusAction(){	
		$formulary	=	new Application_Model_Formulary();
		$form		=	array_map("trim",$_POST);
		$formulary->updatestatus($form);
	}

	public function exportpdfAction(){	
		$formulary	=	new Application_Model_Formulary();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$version	=	$request->getParam("version");	
		$this->view->assign('id',$id);	
		$this->view->assign('version',$version);
		$this->_helper->layout->disableLayout();
	}

	public function webpreviewAction()
	{
	    $formulary	=	new Application_Model_Formulary();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$version	=	$request->getParam("v");	
		$this->view->assign('title','Palliative Drugs CMS :: Preview Formulary');	
		$this->view->assign('id',$id);	
		$this->view->assign('version',$version);			
		$this->_helper->layout->disableLayout();
	}

	public function linksAction()
	{
	    $formulary	=	new Application_Model_Formulary();
		$request	=	$this->getRequest();	
		$id 		=	$request->getParam("id");
		$version	=	$request->getParam("v");	
		$this->view->assign('title','Palliative Drugs CMS :: Formulary Links');	
		$this->view->assign('id',$id);	
		$this->view->assign('version',$version);			
		//$this->_helper->layout->disableLayout();
	}

	public function auditAction()
	{
		$formulary	=	new Application_Model_Formulary();
		$request		=	$this->getRequest();	
		$id 				=	$request->getParam("id");
		$this->view->assign('title','Palliative Drugs CMS :: Formulary Audits');	
		$this->view->assign('id',$id);	
		$this->_helper->layout->disableLayout();
	}

	public function addauditAction()
	{
		$formulary	=	new Application_Model_Formulary();
		$formulary->addaudit($_POST);		
	}

	public function oldauditAction()
	{
		$formulary	=	new Application_Model_Formulary();
		$formulary->oldaudit($_POST);		
	}

	public function scopeauditAction()
	{
		$formulary	=	new Application_Model_Formulary();
		$request		=	$this->getRequest();	
		$id 				=	$request->getParam("id");
		$audits			=	$formulary->getaudits($id);	
		$this->view->assign('title','Palliative Drugs CMS :: Formulary Audits');		
		$this->view->assign('formulary_title',$audits[0]['formulary_title']);
		$this->view->assign('audits',$audits);
	}
	
	
}
