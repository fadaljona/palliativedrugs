<?php 
class PagesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Pages');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('pages');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/pages');
	    }
		$pages			=	new Application_Model_Pages();
		$global_nav		=	$pages->cat_list('0','1','Global Nav');
        $sglobal_nav	=	$pages->cat_list('0','1','Secondary Global Nav');
        $footer_nav		=	$pages->cat_list('0','1','Footer Nav');
        $other_nav		=	$pages->cat_list('0','1','Other Nav');        
		$this->view->assign('global_nav',$global_nav);
		$this->view->assign('sglobal_nav',$sglobal_nav);
		$this->view->assign('footer_nav',$footer_nav);
		$this->view->assign('other_nav',$other_nav);
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Pages');	
		$pages		=	new Application_Model_Pages();
		$articles	=	new Application_Model_Articles();
		$users		=	new Application_Model_Users();
		$select		=	new Application_Model_Selectpattern();
		$page_list	=	$pages->getPagelist('0','','0');
		$snippets	=	$articles->getSnippets();
		$types		=	$pages->getUsertypes();
		$langs		=	$pages->getLanguages();

		$this->view->assign('page_list',$page_list);
		$this->view->assign('snippets',$snippets);
		$this->view->assign('types',$types);
		$this->view->assign('langs',$langs);

		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'language_id'		=> '',
						'cat_title'			=> '',
						'menu_title'		=> '',
						'cat_type'			=> '',
						'cat_parent_id'		=> '',
						'cat_url'			=> '',						
						'temp_id'			=> '',
						'article_id'		=> '',
						'ext_url'			=> '',
						'menu_item'			=> '',
						'temp_filename'		=> '',
						'article_title'		=> '',						
						'start_date'		=> '',
					    'end_date'			=> '',
						'login_required'	=> '',						
						'type_id'			=> '',
						'css_class'			=> '',	
						'cat_schedule'		=> '',
						'status'			=> '',						
						'shour'				=> '',
						'smin'				=> '',
						'sform'				=> '',
						'ehour'				=> '',
						'emin'				=> '',
						'eform'				=> '',
				); 	
			
		}else if($id && !$_POST){
			$pagedata	= $pages->read($id);
			$form = array
				(
						'id'				=> $pagedata[0]['cat_id'],
						'language_id'		=> $pagedata[0]['language_id'],
						'cat_title'			=> $pagedata[0]['cat_title'],
						'menu_title'		=> $pagedata[0]['menu_title'],
						'cat_type'			=> $pagedata[0]['cat_type'],
						'cat_parent_id'		=> $pagedata[0]['cat_parent_id'],
						'cat_url'			=> $pagedata[0]['cat_url'],
						'temp_id'			=> $pagedata[0]['temp_id'],
						'article_id'		=> $pagedata[0]['article_id'],
						'ext_url'			=> $pagedata[0]['ext_url'],
						'menu_item'			=> $pagedata[0]['menu_item'],
						'temp_filename'		=> $pagedata[0]['temp_filename'],
						'article_title'		=> $pagedata[0]['article_title'],
						'start_date'		=> $pagedata[0]['start_date'],
					    'end_date'			=> $pagedata[0]['end_date'],
						'login_required'	=> $pagedata[0]['login_required'],
						'type_id'			=> $pagedata[0]['type_id'],
						'css_class'			=> $pagedata[0]['css_class'],	
						'cat_schedule'		=> $pagedata[0]['cat_schedule'],
						'status'			=> $pagedata[0]['status'],						
						'shour'				=> $pagedata[0]['shour'],
						'smin'				=> $pagedata[0]['smin'],
						'sform'				=> $pagedata[0]['sform'],
						'ehour'				=> $pagedata[0]['ehour'],
						'emin'				=> $pagedata[0]['emin'],
						'eform'				=> $pagedata[0]['eform'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
			$this->view->assign('select',$select);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$pages->update($form);
			else
				$pages->add($form);
			$this->_redirect('/pages');
       } 		
    }
	
	public function deleteAction()
	{
	    $users		=	new Application_Model_Assets();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$users->delete($id);
		$this->_redirect('/assets');
	}
	
	public function catorderAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Order Pages');
		$pages		=	new Application_Model_Pages();	
		$request	=	$this->getRequest();		
		$typeid		=	$request->getParam("typeid");
		$this->view->assign('typeid',$typeid);		
    }	
}
