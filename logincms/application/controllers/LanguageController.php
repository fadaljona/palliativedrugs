<?php 
class LanguageController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Languages');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('pages');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/languages');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Language');	
		$langs		=	new Application_Model_Language();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'language_name'	=> '',
						'language_code'	=> '',
						'is_default'	=> '',
						'status'		=> '',
				); 	
			
		}else if($id && !$_POST){			
			$langdata	= $langs->read($id);
			$form = array
				(
						'id'			=> $langdata[0]['language_id'],
						'language_name'	=> $langdata[0]['language_name'],
						'language_code' => $langdata[0]['language_code'],
						'is_default'	=> $langdata[0]['is_default'],
						'status'		=> $langdata[0]['status'],						
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$langs->update($form);
			else
				$langs->add($form);
			$this->_redirect('/language');
       } 		
    }
	
	public function deleteAction()
	{
	    $types		=	new Application_Model_Usertypes();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$types->delete($id);
		$this->_redirect('/language');
	}
}
