<?php 
class CountriesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Countries');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('products');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/country');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Country');	
		$country	=	new Application_Model_Countries();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=>'',
						'zone_id'		=> '',
						'iso_code'		=> '',
						'country'		=> '',
						'is_eu'			=> '',
						'is_gc'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$countrydata	= $country->read($id);
			$form = array
				(
						'id'			=> $countrydata[0]['id'],
						'zone_id'		=> $countrydata[0]['zone_id'],
						'iso_code'		=> $countrydata[0]['iso_code'],
						'country'		=> $countrydata[0]['country'],
						'is_eu'			=> $countrydata[0]['is_eu'],
						'is_gc'			=> $countrydata[0]['is_gc'],
				); 	 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);			
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$country->update($form);
			else
				$country->add($form);
			$this->_redirect('/countries');
       } 		
    }
	
	public function deleteAction()
	{
	    $country	=	new Application_Model_Countries();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$country->delete($id);
		$this->_redirect('/countries');
	}
}
