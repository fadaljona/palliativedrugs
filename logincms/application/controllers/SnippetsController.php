<?php 
class SnippetsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
       $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Snippets');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('pages');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/snippets');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Snippets');	
		$snippets	=	new Application_Model_Snippets();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$fields		=	$snippets->getFields();
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'snippet_title'		=> '',
						'long_text'			=> '',
						'status'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$sdata	= $snippets->read($id);
			$form = array
				(
						'id'				=> $sdata[0]['snippet_id'],
						'snippet_title'		=> $sdata[0]['snippet_title'],
						'long_text'			=> $sdata[0]['long_text'],						
						'status'			=> $sdata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
            $this->view->assign('fields',$fields);
        }else{  			
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$snippets->update($form);
			else
				$snippets->add($form);
			$this->_redirect('/snippets');
       } 		
    }
	
	public function deleteAction()
	{
	    $snippets	=	new Application_Model_Snippets();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$snippets->delete($id);
		$this->_redirect('/snippets');
	}
}
