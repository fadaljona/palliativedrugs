<?php 
class DrugsController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Supply Drugs');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('formulary');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/drugs');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Supply Drugs');	
		$drugs		=	new Application_Model_Drugs();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$parent_form=	$drugs->parent_formulary();
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'formulary_id'		=> '',
						'drug_name'			=> '',
						'brand'				=> '',
						'manufacture'		=> '',
						'manufacture_url'	=> '',
						'strength'			=> '',
						'formulation'		=> '',
						'packsize'			=> '',
						'cost_quoted'		=> '',
						'nof_doses'			=> '',
						'cost_per_udose'	=> '',
						'nof_dose_pday'		=> '',
						'cost_28days'		=> '',
						'comments'			=> '',
						'status'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$drugdata	= $drugs->read($id);
			$form = array
				(
						'id'				=> $drugdata[0]['drug_id'],
						'formulary_id'		=> $drugdata[0]['formulary_id'],
						'drug_name'			=> $drugdata[0]['drug_name'],
						'brand'				=> $drugdata[0]['brand'],
						'manufacture'		=> $drugdata[0]['manufacture'],
						'manufacture_url'	=> $drugdata[0]['manufacture_url'],
						'strength'			=> $drugdata[0]['strength'],
						'formulation'		=> $drugdata[0]['formulation'],
						'packsize'			=> $drugdata[0]['packsize'],						
						'cost_quoted'		=> $drugdata[0]['cost_quoted'],
						'nof_doses'			=> $drugdata[0]['nof_doses'],
						'cost_per_udose'	=> $drugdata[0]['cost_per_udose'],
						'nof_dose_pday'		=> $drugdata[0]['nof_dose_pday'],
						'cost_28days'		=> $drugdata[0]['cost_28days'],
						'comments'			=> $drugdata[0]['comments'],
						'status'			=> $drugdata[0]['status'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
            $this->view->assign('parent_form',$parent_form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$drugs->update($form);
			else
				$drugs->add($form);
			$this->_redirect('/drugs');
       } 		
    }
	
	public function deleteAction()
	{
	    $drugs		=	new Application_Model_Drugs();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$drugs->delete($id);
		$this->_redirect('/drugs');
	}
}
