<?php header( 'Content-Type:text/html; charset=UTF-8' );
class CommonController extends Zend_Controller_Action
{
	private $_db;  
	private $_ns;

	public function init()
    {
        /* Initialize action controller here */
		$this->_ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($this->_ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	        exit;
        }	
		$this->_db = Zend_Registry::get("db");
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$this->_ns->fullname);		
    }  
	
	// Action for the listings in all modules ==========================================================

	public function listingAction()
	{
		$case	  =	stripslashes($_REQUEST['filter_type']);

		switch($case){
		   case "users":
			   $this->users_list();
		   break;
		   case "user_types":
			   $this->usertype_list();
		   break;	
		   case "articles":
			   $this->article_list();
		   break;
		   case "article_template":
			   $this->article_template();
		   break;
		   case "snippets":
			   $this->snippet_list();
		   break;
		   case "language":
			   $this->language_list();
		   break;
		   case "fieldset":
			   $this->fieldset_list();
		   break;
		   case "dsource":
			   $this->dsource_list();
		   break;
		   case "formulary":
			   $this->formulary_list();
		   break;
		   case "linkformulary":
			   $this->linkformulary_list();
		   break;
		   case "pubmeds":
			   $this->pubmed_list();
		   break;
		   case "supply":
			   $this->supply_list();
		   break;
		   case "tables":
			   $this->table_list();
		   break;
		   case "drugs":
			   $this->drug_list();
		   break;
		   case "formsnippets":
			   $this->formsnippet_list();
		   break;		   
		   case "members":
			   $this->member_list();
		   break;
		   case "groups":
			   $this->group_list();
		   break;
		   case "memberstat":
			   $this->member_stats();
		   break;
		   case "news":
			   $this->news_list();
		   break;
		   case "relatednews":
			   $this->related_news();
		   break;
		   case "relatedpages":
			   $this->related_pages();
		   break;		   
		   case "assets":
			   $this->asset_list();
		   break;
		   case "assetcats":
			   $this->assetcat_list();
		   break;		
		    case "generalproducts":
			   $this->general_products();
		   break;
		   case "clothingproducts":
			   $this->clothing_products();
		   break;
		   case "stocks":
			   $this->stock_list();
		   break;
		   case "shippings":
			   $this->shipping_list();
		   break;
		   case "messages":
			   $this->message_list();	
		   break;
		   case "countries":
			   $this->country_list();	
		   break;
		   case "vatrates":
			   $this->vat_list();
		   break;
		   case "couriers":
			   $this->courier_list();
		   break;
		   case "customers":
			   $this->customer_list();	
		   break;
		   case "orders":
			   $this->orders_list();	
		   break;
		   case "manual_products":
			   $this->manual_products();
		   break;
		   case "manual_customers":
			   $this->manual_customers();
		   break;
		}
	}

	public function reorderAction()
	{

		if(count($_POST)>0 && $_POST['module']<>""){	

			if($_POST['module']=='formulary'){

				$allarr		=	$_POST['form'];

				$arrcount	=	count($_POST['form']);

				for($i=0;$i<$arrcount;$i++){
					$formulary_id	=	$allarr[$i];				

					$this->_db->query("UPDATE 3bit_formulary SET formulary_order = '".($i+1)."' WHERE formulary_id ='".$formulary_id."'");				
				}
				die("S");
			}else if($_POST['module']=='qpd'){

				$allarr		=	$_POST['form'];

				$arrcount	=	count($_POST['form']);

				for($i=0;$i<$arrcount;$i++){
					$formulary_id	=	$allarr[$i];				

					$this->_db->query("UPDATE 3bit_formulary SET qpd_order = '".($i+1)."' WHERE formulary_id ='".$formulary_id."'");				
				}
				die("S");
			}else if($_POST['module']=='pages'){

				$allarr		=	$_POST['pages'];

				$arrcount	=	count($_POST['pages']);

				for($i=0;$i<$arrcount;$i++){
					$cat_id	=	$allarr[$i];				

					$this->_db->query("UPDATE 3bit_categories SET cat_order = '".($i+1)."' WHERE cat_id ='".$cat_id."'");				
				}
				die("S");
			}
		}else{
			die("F");
		}
	}

	// Action for the status update in all modules ==========================================================

	public function changestatusAction()
    {
		$sID			= $_POST["id"];
    
		$sQuery			= $this->_db->fetchRow("SELECT ".$_POST['f']."  FROM ".$_POST['t']." WHERE ".$_POST['idf']."='".$sID."'");		

		//$this->audit	= new Audits_Model;

		if($sQuery){
			$updateEvent	= $this->_db->query("UPDATE ".$_POST['t']." SET ".$_POST['f']." = '".$_POST['s']."' WHERE ".$_POST['idf']." = '".$sID."'");			
			
			$audit	=	new Application_Model_Audits(); 

			if($_POST['s']=='1'){
				$audit->insert_audit($_POST['t'],'active',$sID,'success');
			}else{
				$audit->insert_audit($_POST['t'],'inactive',$sID,'success');
			}

			$caseA			=	"<a onclick=\"javascript:change_status('".$sID."','".$_POST['t']."','".$_POST['f']."','0','".$_POST['idf']."');\" href='javascript:;'><font color=\"#008000\">Active</font></a>";
			$caseI			=	"<a onclick=\"javascript:change_status('".$sID."','".$_POST['t']."','".$_POST['f']."','1','".$_POST['idf']."');\" href='javascript:;'><font color=\"#ff0000\">Inactive</font></a>";

			$case			= ($_POST['s']=='1')?$caseA:$caseI;

	    }else{
            $case			= "ID $sID doesn't exist.";
		}
		
		die($case . "###" . $_POST['s']);
    }
    
	/*************************************** Suggest tags option in all modules ************************************************/

	// To find unique array ====================================================================================

	function arrayUnique($array, $preserveKeys = false){  

	     $arrayRewrite = array();  
	     $arrayHashes = array();  
	     foreach($array as $key => $item) {           
	         $hash = md5(serialize($item));  
	         if (!isset($arrayHashes[$hash])) {  
	             $arrayHashes[$hash] = $hash;  
	             if ($preserveKeys) {  
	                 $arrayRewrite[$key] = $item;  
	             } else {  
	                 $arrayRewrite[] = $item;  
	             }  
	         }  
	     }  
	     return $arrayRewrite;  
	 } 

	// For suggest keywords in assets module ====================================================================

	public function suggestAction()
    {

		$data  = array();
		if(!empty($_POST['uploadcat'])){    
			$sql_cat	=	$this->_db->fetchRow("SELECT asset_category_id,asset_category_type FROM 3bit_asset_categories WHERE asset_category_title ='".$_POST['uploadcat']."'");
			if($sql_cat){
				$catid		=	$sql_cat['asset_category_id'];
				$catname	=	$sql_cat['asset_category_type'];
			}
		}
	
		$sql		=	"SELECT distinct(keywords) FROM 3bit_assets WHERE LOWER(keywords) LIKE '%".strtolower($_POST['q'])."%' ";
	
		if(!empty($_POST['uploadcat']))
			$sql	.=	" AND asset_category_id = '".$catid."' ";
	
		//echo $sql;
		$query = $this->_db->fetchAll($sql);	
	    foreach ($query as $row) {
	      $json = array();
		  if(strstr($row['keywords'],",")){		  
			  $kword	=	explode(",",$row['keywords']);		  
		 	  for($i=0;$i<count($kword);$i++){
				  if($kword[$i]!=''){
			    	  $json['value'] = trim($kword[$i]);
					  $data[] = $json;
				  }
			  }
		  }else{		  
			  $json['value'] = trim($row['keywords']);
			  $data[] = $json;
		  }
	      
	   }   
	   $data	=	$this->arrayUnique($data);  
	   header("Content-type: application/json");
	   die(json_encode($data));
    }

	// For suggest keywords in formulary module ======================================================================

	public function formularysuggestAction()
	{
		$data  =	array();    	
		$sql   =	"SELECT distinct(formulary_tags) FROM 3bit_formulary WHERE LOWER(formulary_tags) LIKE '%".strtolower($_POST['q'])."%' ";
		
		$query =	$this->_db->fetchAll($sql);	
		foreach($query as $row) {
		  $json = array();
		  if(strstr($row['formulary_tags'],",")){		  
			  $kword	=	explode(",",$row['formulary_tags']);		  
			  for($i=0;$i<count($kword);$i++){
				  if($kword[$i]!=''){
					  $json['value'] = trim($kword[$i]);
					  $data[] = $json;
				  }
			  }
		  }else{		  
			  $json['value'] = trim($row['formulary_tags']);
			  $data[] = $json;
		  }
		  
	   }   
	   $data	=	$this->arrayUnique($data);  
	   header("Content-type: application/json");
	   die(json_encode($data));	
	}

	// For suggest keywords in news module ========================================================================================

	public function newssuggestAction()
	{
		$data  =	array();    	
		$sql   =	"SELECT distinct(news_tags) FROM 3bit_news WHERE LOWER(news_tags) LIKE '%".strtolower($_POST['q'])."%'  ";
		
		$query =	$this->_db->fetchAll($sql);	
		foreach($query as $row) {
		  $json = array();
		  if(strstr($row['news_tags'],",")){		  
			  $kword	=	explode(",",$row['news_tags']);		  
			  for($i=0;$i<count($kword);$i++){
				  if($kword[$i]!=''){
					  $json['value'] = trim($kword[$i]);
					  $data[] = $json;
				  }
			  }
		  }else{		  
			  $json['value'] = trim($row['news_tags']);
			  $data[] = $json;
		  }
		  
	   }   
	   $data	=	$this->arrayUnique($data);  
	   header("Content-type: application/json");
	   die(json_encode($data));	
	}


	// For suggest keywords in members module ========================================================================================

	public function membersuggestAction()
	{
		$data		=	array(); 
		$fieldname	=	$_POST['field'];
		$sql		=	"SELECT distinct(".$fieldname.") FROM new_users WHERE LOWER(".$fieldname.") LIKE '%".strtolower($_POST['q'])."%'  ";
		
		$query		=	$this->_db->fetchAll($sql);	
		foreach($query as $row) {
		  $json = array();
		  if(strstr($row[$fieldname],",")){		  
			  $kword	=	explode(",",$row[$fieldname]);		  
			  for($i=0;$i<count($kword);$i++){
				  if($kword[$i]!=''){
					  $json['value'] = trim($kword[$i]);
					  $data[] = $json;
				  }
			  }
		  }else{		  
			  $json['value'] = trim($row[$fieldname]);
			  $data[] = $json;
		  }
		  
	   }   
	   $data	=	$this->arrayUnique($data);  
	   header("Content-type: application/json");
	   die(json_encode($data));	
	}

	/*************************************** Suggest tags option in all modules ************************************************/

	/*************************************** Select pattern functions in all modules *******************************************/

	// Template for select pattern ==================================================================

	public function selectimagesAction()
   {
		$request	=	$this->getRequest();				
		$form = array
		(
				'number'	=> $request->getParam("number"),
				'form'		=> $request->getParam("form"),
				'category'	=> $request->getParam("category"),
				'uploadcat'	=> $request->getParam("uploadcat"),
				'uniqueid'	=> $request->getParam("uniqueid"),			
		); 	
		$this->view->assign('form',$form);
		$this->_helper->layout->disableLayout();
	}

	// Assets list function for select pattern ======================================================

	public function showlistAction()
	{		
		$img1		=	"";

		$search_query=	"";
		
		$getAssetID	=	explode(",",$_POST['editid']);	
		
		$showpage	=	$_POST['pagenum'];

		$uploadcat	=	$_POST['uploadcat'];

		$searchval	=	$_POST['searchval'];

		$category	=	$_POST['category'];

		$uniqid		=	$_POST['uniqueid'];

		$form		=	$_POST['form'];

		$editid		=	$_POST['editid'];

		$number		=	$_POST['number'];
		
		$cntr = 0;
		
		$chkExtImgFormat	=	array("jpg","gif","tiff","png","bmp","JPG","jpeg","JPEG","GIF","TIFF","PNG","BMP");
		
		$chkExtAssetFormat	=	array("doc","txt","pdf","xls","swf","mp3","flv","DOC","TXT","PDF","XLS","SWF","FLV","MP3");
		
		$imgExt		=	implode(',',$chkExtImgFormat);
		
		$assetExt	=	implode(',',$chkExtAssetFormat);
		
		if($uploadcat !=''){
			$sqlCat			=	$this->_db->fetchRow("SELECT asset_category_id FROM 3bit_asset_categories WHERE asset_category_title='".$uploadcat."'");					
			if($sqlCat){
				$catid		=	$sqlCat['asset_category_id'];
			}	
			
			if($uploadcat == "news-header"){		
				$sqloCat	=	$this->_db->fetchRow("SELECT asset_category_id FROM 3bit_asset_categories WHERE asset_category_title='project-image'");		
				if($sqloCat){			
					$ocatid	=	$sqloCat['asset_category_id'];
				}				
				$search_query	.=	" AND (asset_category_id = '$catid' || asset_category_id = '$ocatid')";				
			}else{				
				$search_query	.=	" AND asset_category_id = '$catid'";
			}
		}	

		if($searchval!=''){
			$search_query	.=	" AND a.keywords LIKE '%".$searchval."%'";	
		}
		$img1 = "";
				
		if($category	==	'image' || $category == 'image-upload'){
			$image_query1 = "SELECT DISTINCT a.* FROM 3bit_assets a WHERE a.asset_name != '' and FIND_IN_SET(a.asset_type,'".$imgExt."') $search_query ORDER BY a.asset_id DESC ";			
		}else if($category	==	'asset' || $category == 'asset-upload'){
			$image_query1 = "SELECT DISTINCT a.* FROM 3bit_assets a WHERE a.asset_name != '' and FIND_IN_SET(a.asset_type,'".$assetExt."') $search_query ORDER BY a.asset_id DESC ";					
		}else if($category	==	'both-upload'){
			$image_query1 = "SELECT DISTINCT a.* FROM assets a WHERE a.asset_name != '' $search_query ORDER BY a.asset_id DESC";	
		}		
		
		if($category	<>	'asset' && $category <> 'asset-upload' && $category <> 'both-upload')
			$final_query 	= 	$this->pageQueryImages($image_query1,$showpage);
		else		
			$final_query 	=	$this->pageQueryImages($image_query1,$showpage);	
		
		$image_query	 	= 	$this->_db->fetchAll($final_query);		
		
		foreach($image_query as $image_values){

			$img_id			=	$image_values['asset_id'];
			
			$img_name 		= 	$image_values['asset_name'];

			$chkExt			=	substr($img_name,-3,3);	
			
			if($image_values["asset_title"]<>"" &&$image_values["asset_alt"]<>"")
				$img_title	=	",".stripslashes($image_values["asset_title"]);
			else
				$img_title	=	stripslashes($image_values["asset_title"]);
				
			$img_alt		=	stripslashes($image_values["asset_alt"]);

			$img_type		=	$image_values['asset_type'];
			
			$catchk			=	$this->_db->fetchRow("SELECT asset_category_title,asset_category_type FROM 3bit_asset_categories WHERE asset_category_id='".$image_values['asset_category_id']."'");
			
			if($catchk){
				$catname	=	$catchk['asset_category_title'];
				$cattype	=	$catchk['asset_category_type'];
			}
						
			$chkFileName	=	$_SERVER['DOCUMENT_ROOT'].'/assets/'.$cattype.'/'.$image_values['asset_category_id']."/".$img_name;
			
			$dispPath		=	'/assets/thumbs/'.$image_values['asset_category_id']."/".$img_name;

		    $thumbName		=	$_SERVER['DOCUMENT_ROOT'].'/assets/thumbs/'.$image_values['asset_category_id']."/".$img_name;

			

			if(!file_exists($thumbName)){
				$dispPath		=	'assets/images/7/default.jpg';
			}
			
			if(file_exists($chkFileName)){			
			
				$cntr = $cntr + 1;	
			
				if($category	==	'both-upload'){ // For image+asset upload check 
			
					$checked	=	'';
			
					for($jr=0;$jr<count($getAssetID);$jr++){
			
						if($getAssetID[$jr]	==	$img_id)
							$checked	.=	' checked=checked';
						else
							$checked	.=	'';
				    }
			
					// Tocheck multiple select or single select option for assets-------------------------

					//echo $uniqid;
					
					if($number == 'single' || $uniqid=='single'){
						$img1 .="<li><input type=\"radio\" name=\"chkRad\" $checked value=\"$img_id#$img_name\">&nbsp;$img_name</li>";					
					}else{									
						$img1 .="<li><input type=\"checkbox\" name=\"chkDel[]\" $checked value=\"$img_id#$img_name\">&nbsp;$img_name</li>";						
					}	
									
				}
				
				if($chkExt	<>	'swf' && in_array($chkExt,$chkExtImgFormat) && $category	<>	'both-upload'){ 					
				
					if($uniqid == 'multiple'){ 									
						if(in_array($img_id,$getAssetID))
							$checked	=	' checked=checked';
						else
							$checked	=	'';					
						$img1 .="<li><input type=\"checkbox\" name=\"chkDel[]\" ".$checked." value=\"$img_id#$img_name\"><img src='".$dispPath."' border='0'></li>";
							 
					}else{					   	
						$img1 .="<li><img src='".$dispPath."' border='0' onclick=\"javascript:FillName('".$img_name."','".$img_id."','".$sel_header_img."','".$form."','".$number."')\" style=\"cursor:pointer;\"></li>";
							 
					}				
				
				}else if(in_array($chkExt,$chkExtAssetFormat) && $category	<>	'both-upload'){
				
					$checked	=	'';
					
					if(in_array($img_id,$getAssetID))
						$checked	=	' checked=checked';
					else
						$checked	=	'';
					
					if($number	<> 'single' && $uniqid !='single'){
						$img1 .="<li><input type=\"checkbox\" name=\"chkDel[]\" $checked value=\"$img_id#$img_name\">";
					}else{
						$img1 .="<li><input type=\"radio\" name=\"chkRad\" $checked value=\"$img_id#$img_name\">";
					}
									
					$img1 .="&nbsp;$img_name</li>";
					
				}
			}			
		}
		
		if($category	==	'asset' || $category == 'asset-upload' || $category == 'both-upload'){
		
			if($number	<> 'single' && $uniqid!='single'){
			
				$img1 .="<div class=''><input type=\"button\" name=\"Insert\" value=\"Insert\" class=\"button\" onclick=\"return insertAllChecked()\" /></div>";
			
			}else{
			
				$img1 .="<div class=''><input type=\"button\" name=\"Insert\" value=\"Insert\" class=\"button\" onclick=\"return insertChecked()\" /></div>";
			
			}	
		}else if($category == 'image-upload'){
			
			if($uniqid	== 'multiple' && $num_image <>'0'){
			
				$img1 .="<div class=''><input type=\"button\" name=\"Insert\" value=\"Insert\" class=\"button\" onclick=\"return insertAllChecked()\" /></div>";
			
			}		
		}
			
		$paging	 	=	$this->pageItImages($image_query1,$uniqid,$showpage,$form,$number,$category,$uploadcat,$editid);
		
		if($category	==	'image' || $category == 'image-upload')
			$class	=	' image';
		else
			$class	=	' list';
		
		if($img1<>""){
			$img1		=	"<form name='FrmImgList' method='POST'>
								<input type='hidden' name='number' value='".$number."'>
								<input type='hidden' name='form' value='".$form."'>								
								<input type='hidden' name='uploadcat' value='".$uploadcat."'>
								<input type='hidden' name='category' value='".$category."'> 
								<input type='hidden' name='uniqueID' value='".$uniqid."'>
								<input type='hidden' name='catID' value='".$catid."'>
								<input type='hidden' value='' name='chkRad' />
								<input type='hidden' value='' name='chkDel[]' />
								<ul class='assets".$class."'>
									$img1
								</ul>
							</form>";
							
			if($paging<>""){
				$img1	.=	"<div>								
								$paging								
							</div>";		
			}			
		}

		die($img1."^S");
	}


	// For the limit and paging function ==========================================================

	private function pageQueryImages($query,$showpage)
	{
		global $limit,$limit_page,$scroll_page,$scrollnumber_page, $link;
		$limit				=		15; 
		$scroll				=		$scroll_page; // Do you want the scroll function to be on (1 = YES, 2 = NO)
		$scrollnumber		=		$scrollnumber_page; // How many elements to the record bar are shown at a time when the scroll function is on
		$query_count		=		$query;
		$res_pagination		=		$this->_db->fetchAll($query_count);
		if($res_pagination)
			$numrows		=		count($res_pagination);
		else
			$numrows		=		0;

		if (!isset($_POST['pagenum'])) {
				if ($showpage == ''){
					$display = 1;
				}else{
					$display = $showpage;
				}
		} else {
			$display = $_POST['pagenum'];
		}
		
		$start = (($display * $limit) - $limit);
		
		$final_query = $query." LIMIT ".$start." ,".$limit;
	
		$query2 = $this->_db->fetchAll($final_query); 

		$paging = ceil ($numrows / $limit);
	
		if ($display > 1) {
			$previous = $display - 1;
		}
		if ($numrows != $limit) {
			if ($scroll == 1) {
				if ($paging > $scrollnumber) {
					$first = $display;
					$last = ($scrollnumber - 1) + $display;
				}
			} else {
				$first = 1;
				$last = $paging;
			}
			if ($last > $paging ) {
				$first = $paging - ($scrollnumber - 1);
				$last 	= $paging;
			}
		}
		if ($display < $paging) {
			$next = $display + 1;
		}
		return $final_query;
	}


	// Function to show paging with listing =======================================================
	
	private function  pageItImages($query,$uniqid,$showpage,$form,$number,$category,$uploadcat,$editid)
	{
		global $limit,$limit_page,$scroll_page,$scrollnumber_page,$link;
		$navlink			=		'';
		$limit				=		15;
		$scroll				=		$scroll_page;
		$scrollnumber		=		$scrollnumber_page;
		$query_count		=		$query;
		$res_pagination		=		$this->_db->fetchAll($query_count);
		if($res_pagination)
			$numrows		=		count($res_pagination);
		else
			$numrows		=		0;
		if (!isset ($_POST['pagenum'])) {
			if ($showpage == ''){
				$display = 1;
			}else{
				$display = $showpage;
			}
		} else {
			$display = $_POST['pagenum'];
		}

		$start 			= 	(($display * $limit) - $limit);
		$final_query 	= 	$query." LIMIT ".$start." ,".$limit;
		$query2			=	$this->_db->fetchAll($final_query); 

		$paging 		= 	ceil($numrows / $limit);

		if ($display > 1) {
				$previous = $display - 1;
		
				$navlink	.=	'<a href="#" onclick=\'javascript:showList("'.$number.'","'.$form.'","'.$category.'","'.$uploadcat.'","'.$uniqid.'","1","'.$editid.'","'.$_POST['searchval'].'");\'>&laquo; First</a> | <a href="#" onclick=\'javascript:showList("'.$number.'","'.$form.'","'.$category.'","'.$uploadcat.'","'.$uniqid.'","'.$previous.'","'.$editid.'","'.$_POST['searchval'].'");\'>< </a> |';
		 
		}
		if ($numrows != $limit) {
			if ($scroll == 1) {
				if ($paging > $scrollnumber) {
					$first = $display;
					$last = ($scrollnumber - 1) + $display;
				}
			} else {
				$first = 1;
				$last = $paging;
			}
			if ($last > $paging ) {
				$first = $paging - ($scrollnumber - 1);
				$last = $paging;
			}
			for ($i = $first;$i <= $last;$i++){
				if ($display == $i) {				
					$navlink	.=	'<b>'.$i.'</b>';
				} else { 
			 		$navlink	.=	'<a href="#" onclick=\'javascript:showList("'.$number.'","'.$form.'","'.$category.'","'.$uploadcat.'","'.$uniqid.'","'.$i.'","'.$editid.'","'.$_POST['searchval'].'");\'>&nbsp;'.$i.'&nbsp;</a>';  
				}
			}
		}
		if ($display < $paging) {
			$next = $display + 1;
			$navlink	.=	'| <a href="#" onclick=\'javascript:showList("'.$number.'","'.$form.'","'.$category.'","'.$uploadcat.'","'.$uniqid.'","'.$next.'","'.$editid.'","'.$_POST['searchval'].'");\'>></a> | <a href="#" onclick=\'javascript:showList("'.$number.'","'.$form.'","'.$category.'","'.$uploadcat.'","'.$uniqid.'","'.$paging.'","'.$editid.'","'.$_POST['searchval'].'");\' >Last &raquo;</a>';
		}
		
		return $navlink;		
	}
	
	// To upload through uploadify ================================================================

	public function swfuploadAction()
	{	
	   
	   if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"])||$_FILES["Filedata"]["error"] != 0) {
			header("HTTP/1.1 500 File Upload Error");
			if (isset($_FILES["Filedata"])) {
				die($_FILES["Filedata"]["error"]);
			}
		}else if($_FILES["Filedata"]["error"]==0){
			if($_FILES["Filedata"]["tmp_name"] <>""){
				$uploadcat		=	$_POST['uploadcat'];
				if(trim($uploadcat)<>""){					
					$sql_cat	=	$this->_db->fetchRow("SELECT asset_category_id,asset_category_type FROM 3bit_asset_categories WHERE asset_category_title ='".$uploadcat."'");
					if($sql_cat){
						$catName	=	$sql_cat['asset_category_id'];
						$catType	=	$sql_cat['asset_category_type'];
					}
				}
				$ohpath			=	$_SERVER['DOCUMENT_ROOT'].'/assets/'.$catType.'/'.$catName.'/';
				$thpath			=	$_SERVER['DOCUMENT_ROOT'].'/assets/thumbs/'.$catName.'/';
				$lrpath			=	$_SERVER['DOCUMENT_ROOT'].'/assets/lowres/'.$catName.'/';
				$imgNAME		=	explode(".",$_FILES["Filedata"]["name"]);
				$orgName		=	$this->remSpecial($imgNAME['0']);
				$type_ext		=   $imgNAME['1'];

				if($type_ext=='flv'){
					$name		=	$_FILES["Filedata"]["name"];
				}else{
					$name		=	time()."_".$orgName.".".$type_ext;
				}			
				$thumbfile		= 	$thpath.$name;
				$lowresfile		=	$lrpath.$name;
				$uploadfile		=	$ohpath.$name;

				$title 			=	$this->_ns->txtTitle;
				$alt 			=	$this->_ns->txtAlt;
				$keywords 		=	$this->_ns->as_values;
				
				if(move_uploaded_file($_FILES["Filedata"]["tmp_name"], $uploadfile)){
					chmod($uploadfile, 0777);
					$imgsize		=	filesize($uploadfile);

					if($type_ext == 'pdf'||$type_ext == 'doc'||$type_ext =='octet-stream'||$type_ext == 'swf' ||$type_ext == 'flv' ||$type_ext == 'txt'|| $type_ext == 'mp3' || $type_ext == 'mp4'){
									
						$sqlinsert	=	"INSERT into 3bit_assets (asset_title,asset_name,asset_thumb,asset_category_id,asset_alt,asset_type,asset_size,asset_owner,keywords,status,created_date,modified_date) values('".$title."','".$name."','".$name."','".$catName."','".$alt."','$type_ext','".$imgsize."','".$this->_ns->user_id."','".$keywords."','1',now(),now())";
						
						$thumbfile		=	'dont have';
						
						$this->_db->query($sqlinsert);
						
						$insert_id		=	$this->_db->lastInsertId();
						
					}else if($type_ext == 'jpeg'||$type_ext == 'gif'||$type_ext == 'png'||$type_ext == 'jpg'||$type_ext == 'JPG'){
					// if file uploaded is a gif, jpeg or png, create thumbnail						
						$getSize		=	getimagesize($uploadfile);
						$imgsize		=	filesize($uploadfile);

						if($_SERVER['HTTP_HOST']=='pd2011')
							system("convert -geometry \"100"."x"."$getSize[1]\" -quality \"100\" \"$uploadfile\" \"$thumbfile\"");								
						else	
							exec("/usr/bin/convert -geometry \"100"."x"."$getSize[1]\" -quality \"100\" \"$uploadfile\" \"$thumbfile\"");								
						
						$sqlinsert		=	"INSERT into 3bit_assets (asset_title,asset_name,asset_thumb,asset_category_id,asset_alt,asset_type,asset_height,asset_width,asset_size,asset_owner,keywords,status,created_date,modified_date) values('".$title."','".$name."','".$name."','".$catName."','".$alt."','$type_ext','".$getSize[1]."','".$getSize[0]."','".$imgsize."','".$this->_ns->user_id."','".$keywords."','1',now(),now())";

						$this->_db->query($sqlinsert);
						
						$insert_id		=	$this->_db->lastInsertId();						
					}
					$result	=	$thumbfile."~".$insert_id."^single";
					$this->_ns->txtTitle='';
					$this->_ns->txtAlt='';
					$this->_ns->as_values='';

				}
				$title 			=	$this->_ns->txtTitle;
				$alt 			=	$this->_ns->txtAlt;
				$keywords 		=	$this->_ns->as_values;
			}
		}
		die("S");
	}
	
	// To set select pattern values in session ====================================================

	public function setsessionAction()
	{			
		$keys = array_keys($_POST);
		foreach($keys as $key ){
		
			if(strstr($key,'as_values'))
				$this->_ns->as_values=$_POST[$key];
			else
				$this->_ns->$key	=$_POST[$key];
		}	
		die("S");
	}

	/*************************************** Select pattern functions end *******************************************/

	/*************************************** Listing functions for datatables in all modules ************************/	

	// For users list =============================================================================

	private function users_list()
	{
       if(count($_REQUEST)>0){			    
			
			$aColumns = array('u.user_firstname','u.user_email','ut.title','u.status','u.user_id');

			$sColumns =	array('u.user_firstname','u.user_email','ut.title','u.status','u.user_id');

			$oColumns = array('u.user_firstname','u.user_email','ut.title','u.status','u.user_id');
		
			$sIndexColumn = "user_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT concat(u.user_firstname,' ',u.user_lastname) AS Name,u.user_email,ut.title,u.status,u.user_id FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id $sWhere GROUP BY u.user_id $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);			

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_users u,3bit_user_types ut WHERE u.type_id=ut.type_id ";

			$iTotal = $this->_db->fetchOne($sQuery);
			
			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['Name']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['user_email']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['title']).'",';
				
				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['user_id']."3bit_users'><a onclick=\"change_status('".$aRow['user_id']."','3bit_users','status','0','user_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['user_id']."3bit_users'><a onclick=\"change_status('".$aRow['user_id']."','3bit_users','status','1','user_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/users/add/id/'.$aRow['user_id'];				
				$delurl		=	"'".$this->_baseurl.'/users/delete/id/'.$aRow['user_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['user_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}		
	}

	// For user types list ========================================================================

	private function usertype_list()
	{ 
	   
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('title','status','type_id');

			$sColumns =	array('title','status','type_id');

			$oColumns = array('title','status','type_id');
		
			$sIndexColumn = "type_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT title,status,type_id FROM 3bit_user_types WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_user_types ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['title']).'",';
				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['type_id']."3bit_user_types'><a onclick=\"change_status('".$aRow['type_id']."','3bit_user_types','status','0','type_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['type_id']."3bit_user_types'><a onclick=\"change_status('".$aRow['type_id']."','3bit_user_types','status','1','type_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/usertypes/add/id/'.$aRow['type_id'];				
				$delurl		=	"'".$this->_baseurl.'/usertypes/delete/id/'.$aRow['type_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['type_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}
	
	// For articles list ==========================================================================

	private function article_list()
	{ 		
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('a.article_title','a.short_text','a.long_text','a.meta_title','a.meta_desc','a.meta_keywords');

			$sColumns =	array('a.article_title','a.short_text','a.long_text','a.meta_title','a.meta_desc','a.meta_keywords');

			$oColumns = array('a.article_title','p.cat_alias','a.status','a.article_title');
		
			$sIndexColumn = "article_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT a.article_title,p.cat_alias,a.status,a.article_id FROM 3bit_articles a LEFT JOIN 3bit_categories p ON a.article_id=p.article_id WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_articles ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach( $rResult as $aRow ){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['article_title']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cat_alias']).'",';
				
				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['article_id']."3bit_articles'><a onclick=\"change_status('".$aRow['article_id']."','3bit_articles','status','0','article_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['article_id']."3bit_articles'><a onclick=\"change_status('".$aRow['article_id']."','3bit_articles','status','1','article_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/articles/add/id/'.$aRow['article_id'];				
				$delurl		=	"'".$this->_baseurl.'/articles/delete/id/'.$aRow['article_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['article_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For article template list ==================================================================

	private function article_template()
	{ 
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('temp_filename','temp_type','temp_status','temp_id');

			$sColumns =	array('temp_filename','temp_type','temp_status','temp_id');

			$oColumns = array('temp_filename','temp_type','temp_status','temp_id');
		
			$sIndexColumn = "temp_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT temp_filename,temp_type,temp_status,temp_id FROM 3bit_article_template WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_article_template ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['temp_filename']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['temp_type']).'",';
				if($aRow['temp_status']=='1')
					$dispval	=	"<span id='".$aRow['temp_id']."3bit_article_template'><a onclick=\"change_status('".$aRow['temp_id']."','3bit_article_template','temp_status','0','temp_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['temp_id']."3bit_article_template'><a onclick=\"change_status('".$aRow['temp_id']."','3bit_article_template','temp_status','1','temp_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/templates/add/id/'.$aRow['temp_id'];				
				$delurl		=	"'".$this->_baseurl.'/templates/delete/id/'.$aRow['temp_id']."'";
				$style		=	'';

				if($aRow['temp_status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['temp_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For snippet list ==============================================================================================

	private function snippet_list()
	{ 		
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('snippet_title','status','snippet_id');

			$sColumns =	array('snippet_title','status','snippet_id');

			$oColumns = array('snippet_title','status','snippet_id');
		
			$sIndexColumn = "snippet_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_snippets WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_snippets ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";			
				
				$sOutput .= '"'.str_replace('"', '\"', ($aRow['snippet_title'])).'",';

				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['snippet_id']."3bit_snippets'><a onclick=\"change_status('".$aRow['snippet_id']."','3bit_snippets','status','0','snippet_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['snippet_id']."3bit_snippets'><a onclick=\"change_status('".$aRow['snippet_id']."','3bit_snippets','status','1','snippet_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/snippets/add/id/'.$aRow['snippet_id'];				
				$delurl		=	"'".$this->_baseurl.'/snippets/delete/id/'.$aRow['snippet_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['snippet_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For language list ==========================================================================

	function language_list(){ 

	   if(count($_REQUEST)>0){	
			
			$aColumns = array('language_name','language_code','is_default','language_id');

			$sColumns =	array('language_name','language_code','is_default','language_id');

			$oColumns = array('language_name','language_code','is_default','language_id');
		
			$sIndexColumn = "language_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_language WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);		


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_language ";

			$iTotal = $this->_db->fetchOne($sQuery);		

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){

				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['language_name']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['language_code']).'",';
				if($aRow['is_default']=='1')
					$sel	=	'checked';
				else
					$sel	=	'';

				$dispval	=	"<input type='checkbox' name='is_default' id='chkdefault".$aRow['language_id']."' ".$sel." onclick=\"changeDefault('".$aRow['language_id']."','chkdefault".$aRow['language_id']."');\">";

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/language/add/id/'.$aRow['language_id'];				
				$delurl		=	"'".$this->_baseurl.'/language/delete/id/'.$aRow['language_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['temp_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}


	// For fieldset list ==============================================================================================

	private function fieldset_list()
	{ 		
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('title','description','status','fieldset_id');

			$sColumns =	array('title','description','status','fieldset_id');

			$oColumns = array('title','description','status','fieldset_id');
		
			$sIndexColumn = "fieldset_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_fieldsets WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_fieldsets ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";			
				
				$sOutput .= '"'.str_replace('"', '\"', ($aRow['title'])).'",';
				$sOutput .= '"'.str_replace('"', '\"', ($aRow['description'])).'",';

				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['fieldset_id']."3bit_fieldsets'><a onclick=\"change_status('".$aRow['fieldset_id']."','3bit_fieldsets','status','0','fieldset_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['fieldset_id']."3bit_fieldsets'><a onclick=\"change_status('".$aRow['fieldset_id']."','3bit_fieldsets','status','1','fieldset_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/fieldsets/add/id/'.$aRow['fieldset_id'];				
				$delurl		=	"'".$this->_baseurl.'/fieldsets/delete/id/'.$aRow['fieldset_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['fieldset_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For datasource list ==============================================================================================

	private function dsource_list()
	{ 		
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('title','status','datasource_id');

			$sColumns =	array('title','status','datasource_id');

			$oColumns = array('title','status','datasource_id');
		
			$sIndexColumn = "datasource_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_datasource WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_datasource ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";			
				
				$sOutput .= '"'.str_replace('"', '\"', ($aRow['title'])).'",';

				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['datasource_id']."3bit_datasource'><a onclick=\"change_status('".$aRow['datasource_id']."','3bit_datasource','status','0','datasource_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['datasource_id']."3bit_datasource'><a onclick=\"change_status('".$aRow['datasource_id']."','3bit_datasource','status','1','datasource_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/datasources/add/id/'.$aRow['datasource_id'];				
				$delurl		=	"'".$this->_baseurl.'/datasources/delete/id/'.$aRow['datasource_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['datasource_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// formulary recursive listing function =======================================================

	function make_formulary($parentid,$search){	

			$msql		=	"SELECT * FROM 3bit_formulary WHERE formulary_parent_id='$parentid' ";

			if(!empty($search))
				$msql	.=	" AND (formulary_title LIKE '%".$search."%' OR formulary_type LIKE '%".$search."%' ) ";
	
			$res		=	$this->_db->fetchAll($msql);	

			$numrows	=	count($res);
			if($numrows>0)	{				
				foreach($res as $row){
					$json		.=		"["; 
					$form_id	=		$row['formulary_id'];
					$ftitle		=		html_entity_decode(addslashes($row['formulary_title']));
					$form_level	=		stripslashes($row['level']);
					$new_ver	=       '&nbsp;';
					$preview	=		'';

					if ($form_level =='1'){				
						$tab ='';
					}else if ($form_level =='2'){
						$tab ='----- ';
					}else if ($form_level =='3'){
						$tab ='-------- ';
					}else if ($form_level =='4'){
						$tab ='----------- ';
					}else if ($form_level =='5'){				
						$tab ='-------------- ';
					}	

					$new_ver =	number_format($row['major_version']+$row['minor_version'],2);
					
					$form_title	=		$tab.$ftitle; 
					$json		.=		'"'.str_replace('"', '\"', $form_title).'",';

					$notes		=	$this->getusedHighlights($form_id,$new_ver);

					if($notes>0)
						$json .= '"'.str_replace('"', '\"', $notes).'",';
					else {
						$json .= '"'.str_replace('"', '\"', '').'",';
					}

					$usedanchors	=	$this->gettitleAnchors($form_id,$new_ver);				

					if(!empty($usedanchors))
						$json	.=		'"'.str_replace('"', '\"', $usedanchors).'",';
					else
						$json	.=		'"'.str_replace('"', '\"', '').'",';

					$snippets	=	$this->getusedSnippets($form_id,$new_ver);

					$json		.=	'"'.str_replace('"', '\"', $snippets).'",';

					if($row['formulary_type']=='Drug Monographs')
						$ftype	=	'Monographs';
					else
						$ftype	=	$row['formulary_type'];

					$json		.=		'"'.str_replace('"', '\"', $ftype).'",';


					/*$sub_qry	=  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$form_id."'");

					if(count($sub_qry)>0){
						$cint	=	$sub_qry[0]['cnt'];
						if($cint>0)
							$new_ver =	$sub_qry[0]['max_version'];
					} */

					$new_ver =	number_format($row['major_version']+$row['minor_version'],2);
					
					$json		.= '"'.str_replace('"', '\"', $new_ver).'",';							

					$json		.= '"'.str_replace('"', '\"', $row['publish_status']).'",';							

					if($row['status']=='1')
						$dispval	=	"<span id='".$row['formulary_id']."3bit_formulary'><a onclick=\"change_status('".$row['formulary_id']."','3bit_formulary','status','0','formulary_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
					else
						$dispval	=	"<span id='".$row['formulary_id']."3bit_formulary'><a onclick=\"change_status('".$row['formulary_id']."','3bit_formulary','status','1','formulary_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

					$json .= '"'.str_replace('"', '\"', $dispval).'",';			

					$editurl	=	$this->_baseurl.'/formulary/add/id/'.$row['formulary_id'];				
					$delurl		=	"'".$this->_baseurl.'/formulary/delete/id/'.$row['formulary_id']."'";
					$style		=	'';

					if($row['status']=='1')
						$style	=	"style='display:none'";
					
					if($new_ver>0){
					   $preview = '<a class=\"button\" href=\"'.$this->_baseurl.'/formulary/preview/id/'.$row['formulary_id'].'/version/'.$new_ver.'\" target=\"_self\"><img src=\"'.addslashes($this->_baseurl).'/public/images/icons/magnifier.png\" alt=\"Preview\" title=\"Preview\" border=\"0\" />Preview</a>';
					}

					$json .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$row['formulary_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>'.$preview.'",';

					$json = substr_replace( $json, "", -1 );
					$json .= "],";			
						
					 // Ends here -------------------------------			
					$json .= $this->make_formulary($form_id,$search);			
				}// while
			}

			
			return $json;
		}

	// For formulary list =========================================================================

	function formulary_list(){ 

	   if(count($_REQUEST)>0){			

			$sColumns =	array('formulary_title','formulary_type','formulary_title','status','formulary_id');

			$oColumns = array('formulary_level','formulary_title','formulary_title','formulary_title','formulary_title');
		
			$sIndexColumn = "formulary_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			/*if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} */

			$_SESSION['wlsession']['search_type']	=	'';
			$_SESSION['wlsession']['search_level']	=	'';
			$_SESSION['wlsession']['search_lang']	=	'';

			if($_REQUEST['search_type']<>"A" && $_REQUEST['search_type']<>""){
				if($_REQUEST['search_type']=='GT')
					$sWhere	.=	" AND formulary_type ='General Topics' ";				
				if($_REQUEST['search_type']=='DM')
					$sWhere	.=	" AND formulary_type ='Drug Monographs' ";				
				if($_REQUEST['search_type']=='AP')
					$sWhere	.=	" AND formulary_type ='Appendicies' ";				
				if($_REQUEST['search_type']=='PR')
					$sWhere	.=	" AND formulary_type ='Prelims' ";
				if($_REQUEST['search_type']=='QPD')
					$sWhere	.=	" AND quick_practice_guide ='1' ";

				$_SESSION['wlsession']['search_type']	=	$_REQUEST['search_type'];						
			}

			if($_REQUEST['search_level']<>"A" && $_REQUEST['search_level']<>""){
				$sWhere	.= " AND formulary_level='".$_REQUEST['search_level']."' ";  
				$_SESSION['wlsession']['search_level']	=	$_REQUEST['search_level'];										
			}

			if($_REQUEST['search_lang']<>"A" && $_REQUEST['search_lang']<>""){	
				$language_id		= $this->_db->fetchOne("SELECT language_id FROM 3bit_language WHERE language_code='".$_REQUEST['search_lang']."'");				
				$sWhere	.= " AND language_id ='".$language_id."' ";  
				$_SESSION['wlsession']['search_lang']	=	$_REQUEST['search_lang'];				
			}

			$sQuery		=	"SELECT * FROM 3bit_formulary WHERE 1 AND formulary_parent_id ='0' $sWhere $sOrder $sLimit ";

			if($_REQUEST['search_type']=='QPD')
				$sQuery		=	"SELECT * FROM 3bit_formulary WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery		= "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_formulary ";

			$iTotal		= $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach ( $rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['formulary_title']).'",';

				$new_version =	number_format($aRow['major_version']+$aRow['minor_version'],2);

				$notes	 =	$this->getusedHighlights($aRow['formulary_id'],$new_version);

				if($notes>0)
					$sOutput .= '"'.str_replace('"', '\"', $notes).'",';
				else {
					$sOutput .= '"'.str_replace('"', '\"', '').'",';
				}

				$usedanchors	=	$this->gettitleAnchors($aRow['formulary_id'],$new_version);				

				if(!empty($usedanchors))
					$sOutput	.=		'"'.str_replace('"', '\"', $usedanchors).'",';
				else
					$sOutput	.=		'"'.str_replace('"', '\"', '').'",';

				$snippets = $this->getusedSnippets($aRow['formulary_id'],$new_version);

				$sOutput .= '"'.str_replace('"', '\"', $snippets).'",';

				if($aRow['formulary_type']=='Drug Monographs')
					$type	=	'Monographs';
				else
				    $type	=	$aRow['formulary_type'];

				$sOutput .= '"'.str_replace('"', '\"', $type).'",';

				/*$sublib_qry		 =  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$aRow['formulary_id']."'");
				if(count($sublib_qry)>0){
					$cnt	=	$sublib_qry[0]['cnt'];
					if($cnt>0)
						$new_version =	$sublib_qry[0]['max_version'];
				}*/

				$sOutput .= '"'.str_replace('"', '\"', $new_version).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['publish_status']).'",';
				
				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['formulary_id']."3bit_formulary'><a onclick=\"change_status('".$aRow['formulary_id']."','3bit_formulary','status','0','formulary_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['formulary_id']."3bit_formulary'><a onclick=\"change_status('".$aRow['formulary_id']."','3bit_formulary','status','1','formulary_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/formulary/add/id/'.$aRow['formulary_id'];				
				$delurl		=	"'".$this->_baseurl.'/formulary/delete/id/'.$aRow['formulary_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'";
				
				$preview	=		'';
				if($new_version>0){
					   $preview = '<a class=\"button\" href=\"'.addslashes($this->_baseurl).'/formulary/preview/id/'.$aRow['formulary_id'].'/version/'.$new_version.'\" target=\"_self\"><img src=\"'.addslashes($this->_baseurl).'/public/images/icons/magnifier.png\" alt=\"Preview\" title=\"Preview\" border=\"0\" />Preview</a>';
				}

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['formulary_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>'.$preview.'",';

				

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";

				$sOutput .= $this->make_formulary($aRow['formulary_id'],$_REQUEST['sSearch']);
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	
	//recursive formulary list for linking ========================================================

	function make_linkformulary($parentid){	
	
		$res		=	$this->_db->fetchAll("SELECT * FROM 3bit_formulary WHERE formulary_parent_id='$parentid' ");	

		$numrows	=	count($res);
		if($numrows>0)	{
			
			foreach($res as $row){
				$json		.=	"["; 
				$form_id	=	$row['formulary_id'];
				$ftitle		=	html_entity_decode(addslashes($row['formulary_title']));
				$form_level	=	stripslashes($row['level']);
				$new_ver	=   '-';
				$preview	=	'';
				
				$sversion	=	$this->findFormversion($row['formulary_id']);
				if(trim($row['alias'])!='')
					$ftitle	=	stripslashes($row['alias']);

				$lang_code	=	$this->_db->fetchOne("SELECT language_code FROM 3bit_language WHERE language_id = '".$row['language_id']."' ");
				
				if($sversion>0)
					$surl	=	'/formulary/'.strtolower($lang_code).'/'.$this->remSpecial($ftitle).'.html';

				$ftitle		=	"<a onclick=\"assign_link('".$surl."');\" href=\"javascript:;\">".$row['formulary_title']."</a>";

				$usedanchors=	$this->getusedAnchors($row['formulary_id']);				

				if ($form_level =='1'){				
					$tab ='';
				}else if ($form_level =='2'){
					$tab ='----- ';
				}else if ($form_level =='3'){
					$tab ='-------- ';
				}else if ($form_level =='4'){
					$tab ='----------- ';
				}else if ($form_level =='5'){				
					$tab ='-------------- ';
				}	
				
				$form_title	=		$tab.$ftitle; 
				$json		.=		'"'.str_replace('"', '\"', $form_title).'",';

				if(!empty($usedanchors))
					$json	.=		'"'.str_replace('"', '\"', $usedanchors).'",';
				else
					$json	.=		'"'.str_replace('"', '\"', '').'",';

				if($row['formulary_type']=='Drug Monographs')
					$ftype	=	'Monographs';
				else
					$ftype	=	$row['formulary_type'];

				$json		.=		'"'.str_replace('"', '\"', $ftype).'",';


				$sub_qry	=  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$form_id."'");

				if(count($sub_qry)>0){
					$cint	=	$sub_qry[0]['cnt'];
					if($cint>0)
						$new_ver =	$sub_qry[0]['max_version'];
				}  
				$json		.= '"'.str_replace('"', '\"', $new_ver).'",';							

				$json		.= '"'.str_replace('"', '\"', $row['publish_status']).'",';							

				if($row['status']=='1')
					$dispval	=	"<font color=\"green\">Active</font>";
				else
					$dispval	=	"<font color=\"red\">Inactive</font>";				

				$json .= '"'.str_replace('"', '\"', $dispval).'",';	
				
				$json = substr_replace( $json, "", -1 );

				$json .= "],";			
					
				 // Ends here -------------------------------			
				$json .= $this->make_linkformulary($form_id);			
			}// while
		}

		
		return $json;
	}


	// For formulary list to link ================================================================
	function linkformulary_list_old(){ 

	   if(count($_REQUEST)>0){			

			$sColumns =	array('formulary_title','formulary_title','formulary_type','formulary_title','status','formulary_id');

			$oColumns = array('formulary_level','formulary_title','formulary_type','formulary_title','publish_status','status');
		
			$sIndexColumn = "formulary_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			if($_REQUEST['search_type']<>"A" && $_REQUEST['search_type']<>""){
				if($_REQUEST['search_type']=='GT')
					$sWhere	.=	" AND formulary_type ='General Topics' ";				
				if($_REQUEST['search_type']=='DM')
					$sWhere	.=	" AND formulary_type ='Drug Monographs' ";				
				if($_REQUEST['search_type']=='AP')
					$sWhere	.=	" AND formulary_type ='Appendicies' ";				
			}

			if($_REQUEST['search_level']<>"A" && $_REQUEST['search_level']<>""){
				if($_REQUEST['search_level']=='CH1')
					$sWhere	.= " AND formulary_level='Chapter 1' ";  		
				if($_REQUEST['search_level']=='CH2')
					$sWhere	.= " AND formulary_level='Chapter 2' ";  
				if($_REQUEST['search_level']=='CH3')
					$sWhere	.= " AND formulary_level='Chapter 3' ";  		
				if($_REQUEST['search_level']=='CH4')
					$sWhere	.= " AND formulary_level='Chapter 4' ";  		
				if($_REQUEST['search_level']=='CH5')
					$sWhere	.= " AND formulary_level='Chapter 5' ";  		
				if($_REQUEST['search_level']=='CH6')
					$sWhere	.= " AND formulary_level='Chapter 6' ";  		
				if($_REQUEST['search_level']=='CH7')
					$sWhere	.= " AND formulary_level='Chapter 7' ";  		
				if($_REQUEST['search_level']=='CH8')
					$sWhere	.= " AND formulary_level='Chapter 8' ";  		
				if($_REQUEST['search_level']=='CH9')
					$sWhere	.= " AND formulary_level='Chapter 9' ";  		
				if($_REQUEST['search_level']=='CH10')
					$sWhere	.= " AND formulary_level='Chapter 10' ";  		
				if($_REQUEST['search_level']=='CH11')
					$sWhere	.= " AND formulary_level='Chapter 11' ";  		
				if($_REQUEST['search_level']=='CH12')
					$sWhere	.= " AND formulary_level='Chapter 12' ";  		
				if($_REQUEST['search_level']=='CH13')
					$sWhere	.= " AND formulary_level='Chapter 13' ";  		
				if($_REQUEST['search_level']=='CH14')
					$sWhere	.= " AND formulary_level='Chapter 14' ";  		
				if($_REQUEST['search_level']=='CH15')
					$sWhere	.= " AND formulary_level='Chapter 15' ";  		
				if($_REQUEST['search_level']=='CH16')
					$sWhere	.= " AND formulary_level='Chapter 16' ";  		
				if($_REQUEST['search_level']=='CH17')
					$sWhere	.= " AND formulary_level='Chapter 17' ";  		
				if($_REQUEST['search_level']=='CH18')
					$sWhere	.= " AND formulary_level='Chapter 18' ";  		
				if($_REQUEST['search_level']=='CH19')
					$sWhere	.= " AND formulary_level='Chapter 19' ";  		
				if($_REQUEST['search_level']=='CH20')
					$sWhere	.= " AND formulary_level='Chapter 20' ";  		
				if($_REQUEST['search_level']=='CH21')
					$sWhere	.= " AND formulary_level='Chapter 21' ";  		
				if($_REQUEST['search_level']=='CH22')
					$sWhere	.= " AND formulary_level='Chapter 22' ";  		
				if($_REQUEST['search_level']=='CH23')
					$sWhere	.= " AND formulary_level='Chapter 23' ";  		
				if($_REQUEST['search_level']=='CH24')
					$sWhere	.= " AND formulary_level='Chapter 24' ";  		
				if($_REQUEST['search_level']=='CH25')
					$sWhere	.= " AND formulary_level='Chapter 25' ";  		
				if($_REQUEST['search_level']=='CH26')
					$sWhere	.= " AND formulary_level='Chapter 26' ";  		
				if($_REQUEST['search_level']=='AP1')
					$sWhere	.= " AND formulary_level='A1' ";  		
				if($_REQUEST['search_level']=='AP2')
					$sWhere	.= " AND formulary_level='A2' ";  		
				if($_REQUEST['search_level']=='AP3')
					$sWhere	.= " AND formulary_level='A3' ";  		
				if($_REQUEST['search_level']=='AP4')
					$sWhere	.= " AND formulary_level='A4' "; 						
			}

			if($_REQUEST['search_lang']<>"A" && $_REQUEST['search_lang']<>""){	
				$language_id	= $this->_db->fetchOne("SELECT language_id FROM 3bit_language WHERE language_code='".$_REQUEST['search_lang']."'");
				$sWhere	.= " AND language_id ='".$language_id."' ";  				
			}

			$sQuery		=	"SELECT * FROM 3bit_formulary WHERE 1 AND formulary_parent_id ='0' AND language_id = '".$_REQUEST['language_id']."' $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_formulary ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach( $rResult as $aRow){

				$sublib_qry		 =  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$aRow['formulary_id']."'");

				if(count($sublib_qry)>0){
					$cnt	=	$sublib_qry[0]['cnt'];
					if($cnt>0)
						$new_version =	$sublib_qry[0]['max_version'];
				}

				$selCode	=	$this->_db->fetchAll("SELECT language_code FROM 3bit_language WHERE language_id = '".$aRow['language_id']."' ");

				if(count($selCode)>0){
					$lang_code	=	$selCode[0]['language_code'];
				}
				
				$sOutput .= "[";			
				
				$formtitle		=	stripslashes($aRow['formulary_title']);
				
				$ftitle			=	$formtitle;

				if(trim($aRow['alias'])!='')
					$ftitle	=	stripslashes($aRow['alias']);
				
				if($new_version>0){
					$surl	  =	'/formulary/'.strtolower($lang_code).'/'.$this->remSpecial($ftitle).'.html';
					$ftitle	  =	"<a onclick=\"assign_link('".$surl."');\" href=\"javascript:;\">".$formtitle."</a>";
					$sOutput .= '"'.str_replace('"', '\"',$ftitle ).'",';
				}else{
					$sOutput .= '"'.str_replace('"', '\"',$formtitle ).'",';
				}

				$usedanchors	=	$this->getusedAnchors($aRow['formulary_id']);				

				if(!empty($usedanchors))
					$sOutput	.=		'"'.str_replace('"', '\"', $usedanchors).'",';
				else
					$sOutput	.=		'"'.str_replace('"', '\"', '').'",';
				
				if($aRow['formulary_type']=='Drug Monographs')
					$type	=	'Monographs';
				else
				    $type	=	$aRow['formulary_type'];

				$sOutput .= '"'.str_replace('"', '\"', $type).'",';

				if($new_version>0){
					$sOutput .= '"'.str_replace('"', '\"', $new_version).'",';
				}else{
					$sOutput .= '"'.str_replace('"', '\"', '-').'",';
				}

				$sOutput .= '"'.str_replace('"', '\"', $aRow['publish_status']).'",';
				
				if($aRow['status']=='1')
					$dispval	=	"<font color=\"green\">Active</font>";
				else
					$dispval	=	"<font color=\"red\">Inactive</font>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';	
				
				$sOutput = substr_replace( $sOutput, "", -1 );
				
				$sOutput .= "],";

				$sOutput .= $this->make_linkformulary($aRow['formulary_id']);
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}


		// For formulary list to link ================================================================
	function linkformulary_list(){ 

	   if(count($_REQUEST)>0){			

			$sColumns =	array('formulary_title','formulary_title','formulary_type','formulary_title','status','formulary_id');

			$oColumns = array('formulary_title','formulary_title','formulary_type','formulary_title','publish_status','status');
		
			$sIndexColumn = "formulary_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			if($_REQUEST['search_type']<>"A" && $_REQUEST['search_type']<>""){
				if($_REQUEST['search_type']=='GT')
					$sWhere	.=	" AND formulary_type ='General Topics' ";				
				if($_REQUEST['search_type']=='DM')
					$sWhere	.=	" AND formulary_type ='Drug Monographs' ";				
				if($_REQUEST['search_type']=='AP')
					$sWhere	.=	" AND formulary_type ='Appendicies' ";				
			}

			if($_REQUEST['search_level']<>"A" && $_REQUEST['search_level']<>""){
				if($_REQUEST['search_level']=='CH1')
					$sWhere	.= " AND formulary_level='Chapter 1' ";  		
				if($_REQUEST['search_level']=='CH2')
					$sWhere	.= " AND formulary_level='Chapter 2' ";  
				if($_REQUEST['search_level']=='CH3')
					$sWhere	.= " AND formulary_level='Chapter 3' ";  		
				if($_REQUEST['search_level']=='CH4')
					$sWhere	.= " AND formulary_level='Chapter 4' ";  		
				if($_REQUEST['search_level']=='CH5')
					$sWhere	.= " AND formulary_level='Chapter 5' ";  		
				if($_REQUEST['search_level']=='CH6')
					$sWhere	.= " AND formulary_level='Chapter 6' ";  		
				if($_REQUEST['search_level']=='CH7')
					$sWhere	.= " AND formulary_level='Chapter 7' ";  		
				if($_REQUEST['search_level']=='CH8')
					$sWhere	.= " AND formulary_level='Chapter 8' ";  		
				if($_REQUEST['search_level']=='CH9')
					$sWhere	.= " AND formulary_level='Chapter 9' ";  		
				if($_REQUEST['search_level']=='CH10')
					$sWhere	.= " AND formulary_level='Chapter 10' ";  		
				if($_REQUEST['search_level']=='CH11')
					$sWhere	.= " AND formulary_level='Chapter 11' ";  		
				if($_REQUEST['search_level']=='CH12')
					$sWhere	.= " AND formulary_level='Chapter 12' ";  		
				if($_REQUEST['search_level']=='CH13')
					$sWhere	.= " AND formulary_level='Chapter 13' ";  		
				if($_REQUEST['search_level']=='CH14')
					$sWhere	.= " AND formulary_level='Chapter 14' ";  		
				if($_REQUEST['search_level']=='CH15')
					$sWhere	.= " AND formulary_level='Chapter 15' ";  		
				if($_REQUEST['search_level']=='CH16')
					$sWhere	.= " AND formulary_level='Chapter 16' ";  		
				if($_REQUEST['search_level']=='CH17')
					$sWhere	.= " AND formulary_level='Chapter 17' ";  		
				if($_REQUEST['search_level']=='CH18')
					$sWhere	.= " AND formulary_level='Chapter 18' ";  		
				if($_REQUEST['search_level']=='CH19')
					$sWhere	.= " AND formulary_level='Chapter 19' ";  		
				if($_REQUEST['search_level']=='CH20')
					$sWhere	.= " AND formulary_level='Chapter 20' ";  		
				if($_REQUEST['search_level']=='CH21')
					$sWhere	.= " AND formulary_level='Chapter 21' ";  		
				if($_REQUEST['search_level']=='CH22')
					$sWhere	.= " AND formulary_level='Chapter 22' ";  		
				if($_REQUEST['search_level']=='CH23')
					$sWhere	.= " AND formulary_level='Chapter 23' ";  		
				if($_REQUEST['search_level']=='CH24')
					$sWhere	.= " AND formulary_level='Chapter 24' ";  		
				if($_REQUEST['search_level']=='CH25')
					$sWhere	.= " AND formulary_level='Chapter 25' ";  		
				if($_REQUEST['search_level']=='CH26')
					$sWhere	.= " AND formulary_level='Chapter 26' ";  		
				if($_REQUEST['search_level']=='AP1')
					$sWhere	.= " AND formulary_level='A1' ";  		
				if($_REQUEST['search_level']=='AP2')
					$sWhere	.= " AND formulary_level='A2' ";  		
				if($_REQUEST['search_level']=='AP3')
					$sWhere	.= " AND formulary_level='A3' ";  		
				if($_REQUEST['search_level']=='AP4')
					$sWhere	.= " AND formulary_level='A4' "; 						
			}

			if($_REQUEST['search_lang']<>"A" && $_REQUEST['search_lang']<>""){	
				$language_id	= $this->_db->fetchOne("SELECT language_id FROM 3bit_language WHERE language_code='".$_REQUEST['search_lang']."'");
				$sWhere	.= " AND language_id ='".$language_id."' ";  				
			}

			$sQuery		=	"SELECT * FROM 3bit_formulary WHERE 1 AND language_id = '".$_REQUEST['language_id']."' $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_formulary ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach( $rResult as $aRow){

				$sublib_qry		 =  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$aRow['formulary_id']."'");

				if(count($sublib_qry)>0){
					$cnt	=	$sublib_qry[0]['cnt'];
					if($cnt>0)
						$new_version =	$sublib_qry[0]['max_version'];
				}

				$selCode	=	$this->_db->fetchAll("SELECT language_code FROM 3bit_language WHERE language_id = '".$aRow['language_id']."' ");

				if(count($selCode)>0){
					$lang_code	=	$selCode[0]['language_code'];
				}
				
				$sOutput .= "[";			
				
				$formtitle		=	stripslashes($aRow['formulary_title']);
				
				$ftitle			=	$formtitle;

				if(trim($aRow['alias'])!='')
					$ftitle	=	stripslashes($aRow['alias']);
				
				if($new_version>0){
					$surl	  =	'/formulary/'.strtolower($lang_code).'/'.$this->remSpecial($ftitle).'.html';
					$ftitle	  =	"<a onclick=\"assign_link('".$surl."');\" href=\"javascript:;\">".$formtitle."</a> (<a onclick=\"mySimpleHtmlInserter('<strong><a href=".$surl.">".$formtitle."</a></strong>');\" style='cursor:pointer;'>Insert</a>)";
					$sOutput .= '"'.str_replace('"', '\"',$ftitle ).'",';
				}else{
					$sOutput .= '"'.str_replace('"', '\"',$formtitle ).'",';
				}

				$usedanchors	=	$this->getusedAnchors($aRow['formulary_id']);				

				if(!empty($usedanchors))
					$sOutput	.=		'"'.str_replace('"', '\"', $usedanchors).'",';
				else
					$sOutput	.=		'"'.str_replace('"', '\"', '').'",';
				
				if($aRow['formulary_type']=='Drug Monographs')
					$type	=	'Monographs';
				else
				    $type	=	$aRow['formulary_type'];

				$sOutput .= '"'.str_replace('"', '\"', $type).'",';

				if($new_version>0){
					$sOutput .= '"'.str_replace('"', '\"', $new_version).'",';
				}else{
					$sOutput .= '"'.str_replace('"', '\"', '-').'",';
				}

				$sOutput .= '"'.str_replace('"', '\"', $aRow['publish_status']).'",';
				
				if($aRow['status']=='1')
					$dispval	=	"<font color=\"green\">Active</font>";
				else
					$dispval	=	"<font color=\"red\">Inactive</font>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';	
				
				$sOutput = substr_replace( $sOutput, "", -1 );
				
				$sOutput .= "],";				
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// To get snippets assigned in formulary =================================================

	private function getusedSnippets($formulary_id,$lat_version)
	{
		$snippets	=	'';
		//$selVersion		=	$this->_db->fetchAll("SELECT version FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$formulary_id."' ORDER BY version DESC LIMIT 0,1");
		//if(!empty($selVersion)>0){
			//$lat_version	=	$selVersion[0]['version'];
		//}

		$selBlocks  =	$this->_db->fetchAll("SELECT b.* FROM  `3bit_tmp_formulary_blocks` b WHERE 1 AND b.formulary_id='".$formulary_id."' AND b.version='".$lat_version."' ORDER BY b.version DESC");	
		if(count($selBlocks)>0){
			$content	=	'';		
			foreach($selBlocks as $row){
				$content	.= stripslashes($row['block_content1']);
				$content	.= stripslashes($row['block_content2']);
				$content	.= stripslashes($row['bio_availability']);
				$content	.= stripslashes($row['onset_of_action']);
				$content	.= stripslashes($row['time_to_peak']);
				$content	.= stripslashes($row['plasma_halflife']);
				$content	.= stripslashes($row['duration_of_action']);
			}
			if($content !=''){
				$matchCount	=	preg_match_all('/\[(\[.*?\])\]/', $content, $matches);							
				if($matchCount==0)
					return $snippets; 
				$result = array_unique($matches[0]);	
				foreach($result as $value){						
					$name =str_replace("[[","",$value);
					$name =str_replace("]]","",$name);
					$idplusversion	=	explode("#",str_replace("snippet-id","",$name));				
					$snippetid		=	$idplusversion[0];
					$sversion		=	$idplusversion[1];
					$selsnippet		=	$this->_db->fetchAll("SELECT snippet_type,snippet_number FROM 3bit_formulary_snippet_versions WHERE snippet_id='".$snippetid."' AND version='".$sversion."' ");				
					if(count($selsnippet)>0){
						$sniptit	=	$selsnippet[0]['snippet_type']." ".$selsnippet[0]['snippet_number'];
						$snippets	.=	"<a href='".$this->_baseurl."/snippets/add/id/".$snippetid."'>".$sniptit."</a>".",";
					}
				}
				$snippets = substr($snippets, 0, -1 );
				if(!empty($snippets))
					$snippets =	"(".$snippets.")";
				return $snippets;
			}
		}
	}

	// To get highlights assigned in formulary =================================================

	private function getusedHighlights($formulary_id,$lat_version)
	{
		$snippets	=	'';
		//$selVersion		=	$this->_db->fetchAll("SELECT version FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$formulary_id."' ORDER BY version DESC LIMIT 0,1");
		//if(!empty($selVersion)){
			//$lat_version	=	$selVersion[0]['version'];
		//}

		$selBlocks  =	$this->_db->fetchAll("SELECT b.* FROM  `3bit_tmp_formulary_blocks` b WHERE 1 AND b.formulary_id='".$formulary_id."' AND b.version='".$lat_version."' ORDER BY b.version DESC");	
		if(count($selBlocks)>0){
			$content	=	'';		
			foreach($selBlocks as $row){
				$content	.= stripslashes($row['block_content1']);
				$content	.= stripslashes($row['block_content2']);
				$content	.= stripslashes($row['bio_availability']);
				$content	.= stripslashes($row['onset_of_action']);
				$content	.= stripslashes($row['time_to_peak']);
				$content	.= stripslashes($row['plasma_halflife']);
				$content	.= stripslashes($row['duration_of_action']);
			}
			if($content !=''){
				$matchCount	=	preg_match_all('/<span class="highlight">/', $content, $matches);							
				if($matchCount==0)
					return (0); 			
				return ($matchCount);
			}
		}
	}

	// To get anchors assigned in formulary =================================================

	private function getusedAnchors($formulary_id)
	{
		$anchors	=	'';
		$selVersion		=	$this->_db->fetchAll("SELECT version FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$formulary_id."' ORDER BY version DESC LIMIT 0,1");
		if(!empty($selVersion)>0){
			$lat_version	=	$selVersion[0]['version'];
		}

		$selForm	=	$this->_db->fetchAll("SELECT formulary_title,language_id,alias FROM 3bit_formulary WHERE formulary_id='".$formulary_id."'");
		if(!empty($selForm)){
			$ftitle		=	$selForm[0]['formulary_title'];
			$lang_id	=	$selForm[0]['language_id'];
			$atitle		=	$selForm[0]['alias'];
			if($atitle!='')
				$ftitle	=	$atitle;
		}

		$selCode	=	$this->_db->fetchAll("SELECT language_code FROM 3bit_language WHERE language_id = '".$lang_id."' ");
		if(!empty($selCode)){
			$lang_code	=	$selCode[0]['language_code'];
		}

		$mainurl	=	'/formulary/'.strtolower($lang_code).'/'.$this->remSpecial($ftitle).'.html';

		$selBlocks  =	$this->_db->fetchAll("SELECT b.* FROM  `3bit_tmp_formulary_blocks` b WHERE 1 AND b.formulary_id='".$formulary_id."' AND b.version='".$lat_version."' ORDER BY b.version DESC");	
		if(count($selBlocks)>0){
			$content	=	'';		
			foreach($selBlocks as $row){
				$content	.= stripslashes($row['block_content1']);
				$content	.= stripslashes($row['block_content2']);
				$content	.= stripslashes($row['bio_availability']);
				$content	.= stripslashes($row['onset_of_action']);
				$content	.= stripslashes($row['time_to_peak']);
				$content	.= stripslashes($row['plasma_halflife']);
				$content	.= stripslashes($row['duration_of_action']);
			}
			if($content !=''){
				$matchCount	=	preg_match_all('/<a[^>]*name="([^"]*)"[^>]*>/', $content, $matches, PREG_SET_ORDER);							
				if($matchCount==0)
					return $anchors; 			
				foreach($matches as $value){		
					$surl	  = $mainurl."#".$value[1];
					$anchors .=	" <a onclick=\"assign_link('".$surl."');\" href=\"javascript:;\">#".$value[1]."</a>,";						
				}
				$anchors = substr($anchors, 0, -1 );
				if(!empty($anchors))
					$anchors =	"(".trim($anchors).")";
				return $anchors;
			}
		}
	}

	// To get anchors assigned in formulary =================================================

	private function gettitleAnchors($formulary_id,$lat_version)
	{
		$anchors	=	'';
		//$selVersion		=	$this->_db->fetchAll("SELECT version FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$formulary_id."' ORDER BY version DESC LIMIT 0,1");
		//if(!empty($selVersion)>0){
			//$lat_version	=	$selVersion[0]['version'];
		//}

		$selForm	=	$this->_db->fetchAll("SELECT formulary_title,language_id,alias FROM 3bit_formulary WHERE formulary_id='".$formulary_id."'");
		if(!empty($selForm)){
			$ftitle		=	$selForm[0]['formulary_title'];
			$lang_id	=	$selForm[0]['language_id'];
			$atitle		=	$selForm[0]['alias'];
			if($atitle!='')
				$ftitle	=	$atitle;
		}

		$selCode	=	$this->_db->fetchAll("SELECT language_code FROM 3bit_language WHERE language_id = '".$lang_id."' ");
		if(!empty($selCode)){
			$lang_code	=	$selCode[0]['language_code'];
		}

		$mainurl	=	'/formulary/'.strtolower($lang_code).'/'.$this->remSpecial($ftitle).'.html';

		$selBlocks  =	$this->_db->fetchAll("SELECT b.* FROM  `3bit_tmp_formulary_blocks` b WHERE 1 AND b.formulary_id='".$formulary_id."' AND b.version='".$lat_version."' ORDER BY b.version DESC");	
		if(count($selBlocks)>0){
			$content	=	'';		
			foreach($selBlocks as $row){
				$content	.= stripslashes($row['block_content1']);
				$content	.= stripslashes($row['block_content2']);
				$content	.= stripslashes($row['bio_availability']);
				$content	.= stripslashes($row['onset_of_action']);
				$content	.= stripslashes($row['time_to_peak']);
				$content	.= stripslashes($row['plasma_halflife']);
				$content	.= stripslashes($row['duration_of_action']);
			}
			if($content !=''){
				$matchCount	=	preg_match_all('/<a[^>]*name="([^"]*)"[^>]*>/', $content, $matches, PREG_SET_ORDER);							
				if($matchCount==0)
					return $anchors; 			
				foreach($matches as $value){		
					$surl	  = $mainurl."#".$value[1];
					$anchors .=	" #".$value[1].",";						
				}
				$anchors = substr($anchors, 0, -1 );
				if(!empty($anchors))
					$anchors =	"(".trim($anchors).")";
				return $anchors;
			}
		}
	}

	// For pubmed list ============================================================================

	private function pubmed_list()
	{ 

	   if(count($_REQUEST)>0){	
			
			$aColumns = array('author','title','secondary_title','alternate_title','keywords','abstract','notes','author_address');

			$sColumns =	array('author','title','secondary_title','alternate_title','keywords','abstract','notes','author_address');

			$oColumns = array('id','title','author');
		
			$sIndexColumn = "id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM endnote_refs WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM endnote_refs WHERE 1 $sWhere ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			$i		  =	1;

			$newarr	  =	array();

			foreach($rResult as $aRow){	
				//echo $aRow['pages'];
				$auth		=	explode("\r",$aRow['author']);						
				$authcount	=	count($auth);
				if($authcount>0){
					if($authcount>2){			
						$dispauth	=	$auth[0]." <i>et al</i>.";
					}else if($authcount==2){
						$dispauth	=	implode(",",$auth);
						$dispauth	=	substr($dispauth,0,-1);
						$dispauth	=	$dispauth;
					}else{
					   $dispauth	=	$auth[0];
					}
				}else{
					$dispauth	=	$aRow['author'];
				}			
				
				if($content != ''){
					$returnTxt	= $content;
				}else{
					if($aRow['date']!='')
						$datetext	=	$aRow['year']."  ".$aRow['date'];
					else
						$datetext	=	$aRow['year'];

					if(substr(stripslashes($aRow['title']),-1, 1)!='.')
						$reftit		=	stripslashes($aRow['title']).".";
					else
						$reftit		=	stripslashes($aRow['title']);

					if($aRow['pages']!=''){
						$returnTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".$reftit."</i>";

						if($dispedit!=''){	
							$returnTxt		.=	"&nbsp;".$dispedit;
						}
						if($aRow['secondary_title']!=''){	
							$returnTxt		.=	"&nbsp;<i>".stripslashes($aRow['secondary_title'])."</i>";
						}
						if($aRow['publisher']!=''){
							 $returnTxt		.=	",&nbsp;".$aRow['publisher'];
						}
						if($aRow['place_published']!=''){

							$returnTxt		.= ",".$aRow['place_published'];
						}
						if($aRow['volume']!='' && $aRow['pages']!=''){
						   $returnTxt		.= "&nbsp;<b>".$aRow['volume']."</b>:&nbsp;".$aRow['pages'];
						}else if($aRow['volume']=='' && $aRow['pages']!=''){
						   $returnTxt		.= "&nbsp;pp.&nbsp;".$aRow['pages'];
						}						

					}else{
						$returnTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".$reftit."</i>";
						if($dispedit!=''){	
							$returnTxt		.=	"&nbsp;".$dispedit;
						}
						if($aRow['secondary_title']!=''){	
							$returnTxt		.=	"&nbsp;<i>".stripslashes($aRow['secondary_title'])."</i>";
						}
						if($aRow['edition']!=''){   
						   $returnTxt		.=	"&nbsp;(".$aRow['edition']."e).";
						}
						if($aRow['publisher']!=''){
							 $returnTxt		.=	"&nbsp;".$aRow['publisher'];
						}
						if($aRow['place_published']!=''){

							$returnTxt		.= ",".$aRow['place_published'];
						}
					}

					$returnTxt		.= ".";

					if($aRow['accession_number']!='' && is_numeric($aRow['accession_number'])){

						$returnTxt		.= " <a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Citation&list_uids=".$aRow['accession_number']."'>PubMed: ".$aRow['accession_number']."</a>.";
					}
				
					$pubmed_id		= $aRow['id'];		

					$newarr[$pubmed_id]	= $returnTxt;

					$i++;
				}
			}
			asort($newarr,SORT_REGULAR);			

			foreach ($newarr as $key => $value){	

				$kcode	=	$this->_db->fetchOne("SELECT accession_number FROM endnote_refs WHERE id = '".$key."' AND accession_number !=''");				
				$editlink	=	'';					

				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $key).'",';
				$sOutput .= '"'.str_replace('"', '\"', $kcode).'",';
				$sOutput .= '"'.str_replace('"', '\"', '<a href="javascript:;" onclick=insert_dialog("'.$key.'");>'.$value.'</a>').'",';
				$sOutput .= '"&nbsp;",';
				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For supply list ============================================================================
	private function supply_list()
	{ 

	   if(count($_REQUEST)>0){				

			$sColumns =	array('d.drug_name','d.manufacture','d.formulation','d.status','d.drug_id','f.formulary_title');

			$oColumns = array('d.drug_name','d.manufacture','d.formulation','d.status','d.drug_id','d.drug_name','d.drug_name','d.drug_name','d.drug_name','d.drug_name','d.drug_name');
		
			$sIndexColumn = "d.drug_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT d.* FROM 3bit_drugs d, 3bit_formulary f WHERE 1 AND d.formulary_id = f.formulary_id $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery		= "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_drugs d, 3bit_formulary f WHERE 1 AND d.formulary_id = f.formulary_id ";

			$iTotal		= $this->_db->fetchOne($sQuery);			

			/*
			 * Output
			 */
			$temp	=	'';

			$cache	=	'';

			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach ($rResult as $aRow){
				$sOutput .= "[";

				if($temp != $aRow['drug_name'] || $cache != $aRow['manufacture']){
					$temp		=	$aRow['drug_name'];
					$cache		=	$aRow['manufacture'];
					$drug_name	=	$aRow['drug_name'];
					$trclass	=	' class = "drugmain"';
				}else{
					$drug_name	= '';
					$trclass	= '';
				}
				
				$sOutput .= '"'.str_replace('"', '\"', $drug_name).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['brand']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['manufacture']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['strength']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['formulation']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['packsize']).'",';								
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cost_quoted']).'",';						
				$sOutput .= '"'.str_replace('"', '\"', $aRow['nof_doses']).'",';				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cost_per_udose']).'",';					
				$sOutput .= '"'.str_replace('"', '\"', $aRow['nof_dose_pday']).'",';					
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cost_28days']).'",';
				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For table list =============================================================================

	private function table_list()
	{ 

	   if(count($_REQUEST)>0){	
			
			$aColumns = array('snippet_type','snippet_number','snippet_title','status','snippet_id');

			$sColumns =	array('snippet_type','snippet_number','snippet_title','status','snippet_id');

			$oColumns = array('snippet_type','snippet_number','snippet_title','status','snippet_id');
		
			$sIndexColumn = "snippet_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_formulary_snippets WHERE 1 AND language_id = '".$_REQUEST['language_id']."' $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_formulary_snippets ";

			$iTotal = $this->_db->fetchOne($sQuery);		

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach ( $rResult as $aRow ){
				$sOutput .= "[";

				$sOutput .= '"'.str_replace('"', '\"', ($aRow['snippet_type'])).'",';

				$sOutput .= '"'.str_replace('"', '\"', ($aRow['snippet_number'])).'",';
				
				$sOutput .= '"'.str_replace('"', '\"', '<a href="javascript:;" onclick=mySimpleHtmlInserter("[[snippet-id'.$aRow['snippet_id'].'#'.$aRow['version'].']]");>'.$aRow['snippet_title'].'</a>').'",';
				
				if($aRow['status']=='1')
					$dispval	=	"<font color=\"green\">Active</font>";
				else
					$dispval	=	"<font color=\"red\">Inactive</font>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			
				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}


	

	// For drug list ==============================================================================

	private function drug_list()
	{
	   if(count($_REQUEST)>0){				

			$sColumns =	array('d.drug_name','d.manufacture','d.formulation','d.status','d.drug_id','f.formulary_title');

			$oColumns = array('d.drug_name','d.manufacture','d.formulation','d.status','d.drug_id','d.drug_name','d.drug_name','d.drug_name','d.drug_name','d.drug_name','d.drug_name');
		
			$sIndexColumn = "d.drug_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT d.* FROM 3bit_drugs d, 3bit_formulary f WHERE 1 AND d.formulary_id = f.formulary_id $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_drugs d, 3bit_formulary f WHERE 1 AND d.formulary_id = f.formulary_id ";

			$iTotal = $this->_db->fetchOne($sQuery);			

			/*
			 * Output
			 */
			$temp	=	'';

			$cache	=	'';

			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach ( $rResult as $aRow){
				$sOutput .= "[";

				if($temp != $aRow['drug_name'] || $cache != $aRow['manufacture']){
					$temp		=	$aRow['drug_name'];
					$cache		=	$aRow['manufacture'];
					$drug_name	=	$aRow['drug_name'];
					$trclass	=	' class = "drugmain"';
				}else{
					$drug_name	= '';
					$trclass	= '';
				}
				
				$sOutput .= '"'.str_replace('"', '\"', $drug_name).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['brand']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['manufacture']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['strength']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['formulation']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['packsize']).'",';				

				$cost_quoted =	"<input type=\"text\" size=\"5\" name=\"txtcost".$aRow['drug_id']."\" id=\"txtcost".$aRow['drug_id']."\" value=\"".$aRow['cost_quoted']."\" onkeyup=\"updateDose(this.value,$('#txtdose".$aRow['drug_id']."').val(),'".$aRow['drug_id']."')\" onkeypress=\"return blockNumbers(event)\" onblur=\"drugUpdate(this.value,$('#txtdose".$aRow['drug_id']."').val(),'".$aRow['drug_id']."')\">";
				$sOutput .= '"'.str_replace('"', '\"', $cost_quoted).'",';		

				$noof_dose	=	"<input type=\"text\" size=\"5\" name=\"txtdose".$aRow['drug_id']."\" id=\"txtdose".$aRow['drug_id']."\" value=\"".$aRow['nof_doses']."\" onkeyup=\"updateDose($('#txtcost".$aRow['drug_id']."').val(),this.value,'".$aRow['drug_id']."')\" onkeypress=\"return blockNumbers(event)\" onblur=\"drugUpdate($('#txtcost".$aRow['drug_id']."').val(),this.value,'".$aRow['drug_id']."')\" >";
				$sOutput .= '"'.str_replace('"', '\"', $noof_dose).'",';
				
				$costper_dose=	"<span id=\"cdose".$aRow['drug_id']."\">".$aRow['cost_per_udose']."</span>";
				$sOutput .= '"'.str_replace('"', '\"', $costper_dose).'",';	

				$doseper_day=	"<input type=\"text\" size=\"5\" name=\"txtpday".$aRow['drug_id']."\" id=\"txtpday".$aRow['drug_id']."\" value=\"".$aRow['nof_dose_pday']."\" onkeypress=\"return blockNumbers(event)\" onkeyup=\"updateDay(this.value,$('#cdose".$aRow['drug_id']."').html(),'".$aRow['drug_id']."');\" onblur=\"dayUpdate(this.value,$('#cdose".$aRow['drug_id']."').html(),'".$aRow['drug_id']."');\">";
				$sOutput .= '"'.str_replace('"', '\"', $doseper_day).'",';	

				$days_treat=	"<span id=\"dcal".$aRow['drug_id']."\">".$aRow['cost_28days']."</span>";
				$sOutput .= '"'.str_replace('"', '\"', $days_treat).'",';

				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['drug_id']."3bit_drugs'><a onclick=\"change_status('".$aRow['drug_id']."','3bit_drugs','status','0','drug_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['drug_id']."3bit_drugs'><a onclick=\"change_status('".$aRow['drug_id']."','3bit_drugs','status','1','drug_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/drugs/add/id/'.$aRow['drug_id'];				
				$delurl		=	"'".$this->_baseurl.'/drugs/delete/id/'.$aRow['drug_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['drug_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For snippet list ===========================================================================

	private function formsnippet_list()
	{ 		
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('snippet_number','snippet_title','status','snippet_id');

			$sColumns =	array('snippet_number','snippet_title','snippet_type','status','snippet_id');

			$oColumns = array('snippet_number','snippet_title','snippet_type','language_id','snippet_title','status','snippet_id');
		
			$sIndexColumn = "snippet_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_formulary_snippets WHERE 1 $sWhere $sOrder $sLimit ";
			

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_formulary_snippets ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";			
				
				$sOutput .= '"'.str_replace('"', '\"', ($aRow['snippet_number'])).'",';
				
				$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['snippet_title'])).'",';

				$sOutput .= '"'.str_replace('"', '\"', $aRow['snippet_type']).'",';

				$language_code		= $this->_db->fetchOne("SELECT language_code FROM 3bit_language WHERE language_id='".$aRow['language_id']."'");

				$sOutput .= '"'.str_replace('"', '\"', $language_code).'",';

				$selUsage	=	$this->_db->fetchAll("SELECT f.formulary_id, f.formulary_title,max(b.version) FROM  `3bit_tmp_formulary_blocks` b, 3bit_formulary f WHERE f.formulary_id = b.formulary_id AND (block_content1 LIKE  '%[[snippet-id".$aRow['snippet_id']."%' OR block_content2 LIKE  '%[[snippet-id".$aRow['snippet_id']."%' OR bio_availability LIKE  '%[[snippet-id".$aRow['snippet_id']."%' OR onset_of_action LIKE  '%[[snippet-id".$aRow['snippet_id']."%' OR time_to_peak LIKE  '%[[snippet-id".$aRow['snippet_id']."%' OR plasma_halflife LIKE  '%[[snippet-id".$aRow['snippet_id']."%' OR duration_of_action LIKE  '%[[snippet-id".$aRow['snippet_id']."%' ) GROUP BY b.formulary_id ORDER BY b.version DESC ");

				if(count($selUsage)>0){
					$utitle			=	$selUsage[0]['formulary_title'];
					$fordid			=	$selUsage[0]['formulary_id'];
					$usage_title	=	"<a href=\"".addslashes($this->_baseurl)."/formulary/add/id/".$fordid."\">".$utitle."</a>";
				}else{
					$usage_title	=	'';
				}
				
				$sOutput .= '"'.str_replace('"', '\"', $usage_title).'",';	

				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['snippet_id']."3bit_formulary_snippets'><a onclick=\"change_status('".$aRow['snippet_id']."','3bit_formulary_snippets','status','0','snippet_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['snippet_id']."3bit_formulary_snippets'><a onclick=\"change_status('".$aRow['snippet_id']."','3bit_formulary_snippets','status','1','snippet_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/figures/add/id/'.$aRow['snippet_id'];				
				$delurl		=	"'".$this->_baseurl.'/figures/delete/id/'.$aRow['snippet_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['snippet_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For Members list ===========================================================================

	private function member_list(){

		$members			=	new Application_Model_Members();
		$arrSubscribers		=	$members->get_all_subscribers();

		if(count($_REQUEST)>0){

			if ( $_REQUEST['search_type'] != "A" && $_REQUEST['search_type'] != "unchecked" && $_REQUEST['search_type'] != "pending" ){	

			$sColumns =	array('username','email','FirstName','LastName','title','role','prof_reg_num','nurse_prescriber','speciality','sub_specialty','organisation','addr1','addr2','addr3','addr4','TownOrCity','StateOrCounty','PostalCode','a.Country','phone','main_workplace','current_post','years_in_post','patient_care_time','gender','year_of_birth','lead','UserType');

			}else{
				$sColumns =	array('username','email','FirstName','LastName','title','role','prof_reg_num','nurse_prescriber','speciality','sub_specialty','organisation','addr1','addr2','addr3','addr4','TownOrCity','StateOrCounty','PostalCode','Country','phone','main_workplace','current_post','years_in_post','patient_care_time','gender','year_of_birth','lead','UserType');
			}

			$oColumns = array('FirstName','LastName','email','role','organisation','active');
		
			$sIndexColumn = "id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			//echo $_REQUEST['search_type'];

			if ( $_REQUEST['search_type'] != "A" && $_REQUEST['search_type'] != "unchecked" && $_REQUEST['search_type'] != "pending" && $_REQUEST['search_type'] != "hinari" ){	
				//echo $_REQUEST['search_sub'];
				switch($_REQUEST['search_sub']){

					case "subscribers_7":
						$sWhere	.=	' AND s.end_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)';
					break;

					case "subscribers_30":
						$sWhere	.=	' AND s.end_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 30 DAY)';
					break;

					case "subscribers_active":
						$sWhere	.=	' AND s.end_date > CURDATE()';
					break;

					case "hinari_sub":
						$sWhere	.=	'  AND c.hinari <> "0" AND s.end_date > CURDATE()';
					break;
				}
				
				$sQuery		=	"SELECT a.*,s.*, REPLACE(a.organisation, \"'\",\"\") as organisation FROM new_users a, 3bit_subscribers s, 3bit_country c WHERE a.id = s.user_id AND a.Country =c.country $sWhere GROUP BY s.user_id $sOrder $sLimit ";

			}else if($_REQUEST['search_type'] == "unchecked"){
				$sWhere		.=	' AND RegistrationChecked = "0"';
				$sQuery		=	"SELECT a.*, REPLACE(a.organisation, \"'\",\"\") as organisation FROM new_users a WHERE 1 $sWhere $sOrder $sLimit ";
			}else if($_REQUEST['search_type'] == "pending"){
				$sWhere		.=	' AND RegistrationChecked = "2"';
				$sQuery		=	"SELECT a.*, REPLACE(a.organisation, \"'\",\"\") as organisation FROM new_users a WHERE 1 $sWhere $sOrder $sLimit ";
			}else if($_REQUEST['search_type'] == "hinari"){

				$sWhere		.=	' AND c.hinari <> "0"';
				$sQuery		=	"SELECT a.*, REPLACE(a.organisation, \"'\",\"\") as organisation FROM new_users a, 3bit_country c WHERE  a.Country =c.country $sWhere $sOrder $sLimit ";
			}else{
				$sQuery		=	"SELECT a.*, REPLACE(a.organisation, \"'\",\"\") as organisation FROM new_users a WHERE 1 $sWhere $sOrder $sLimit ";
			}
			
			$rResult		=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery			=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
		
			if ( $_REQUEST['search_type'] != "A" && $_REQUEST['search_type'] != "unchecked" && $_REQUEST['search_type'] != "pending" && $_REQUEST['search_type'] != "hinari" ){
				switch($_REQUEST['search_sub']){
					case "subscribers_7":
						$sWhere	.=	' AND s.end_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)';
					break;

					case "subscribers_30":
						$sWhere	.=	' AND s.end_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 30 DAY)';
					break;

					case "subscribers_active":
						$sWhere	.=	' AND s.end_date > CURDATE()';
					break;

					case "hinari_sub":
						$sWhere	.=	'  AND c.hinari <> "0" AND s.end_date > CURDATE()';
					break;
				}
				//	$sWhere	.=	' AND type = "'.$_REQUEST['search_type'].'"';
				$sQuery		=	"SELECT COUNT(s.user_id) FROM new_users a, 3bit_subscribers s, 3bit_country c WHERE a.id = s.user_id AND a.Country =c.country $sWhere GROUP BY s.user_id";
				$iTotal		=	count($this->_db->fetchAll($sQuery));	
			}else if($_REQUEST['search_type'] == "hinari"){
				$sWhere		.=	' AND c.hinari <> "0"';
				$sQuery		=	"SELECT COUNT(a.id) FROM new_users a, 3bit_country c WHERE  a.Country =c.country $sWhere ";
				$iTotal		= $this->_db->fetchOne($sQuery);	
			}else{
				$sQuery		=	"SELECT COUNT(".$sIndexColumn.") FROM new_users WHERE 1 $sWhere";
				$iTotal		= $this->_db->fetchOne($sQuery);	
			}

			$_SESSION['search_type']	= $_REQUEST['search_type'];
			$_SESSION['sWhere']			= $sWhere;
			$_SESSION['sOrder']			= $sOrder;

			/*
			 * Output
			 */
			$temp	=	'';

			$cache	=	'';

			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach( $rResult as $key => $aRow){

				$sOutput1	= "[";				
				$sOutput1 .= '"'.str_replace('"', '', ($aRow['FirstName']." ".$aRow['LastName'])).'",';
				$sOutput1 .= '"'.str_replace('"', '', $aRow['email']).'",';
				$sOutput1 .= '"'.str_replace('"', '', $aRow['role']).'",';
				$sOutput1 .= '"'.str_replace('"', '', $aRow['speciality']).'",';
				$sOutput1 .= '"'.addslashes($aRow['organisation']).'",';
				//$sOutput1 .= '"ASD",';
				$sOutput .=	str_replace("'","",$sOutput1);


				$rowid		= $aRow['id'];

				if($aRow['active']=='1')
					$dispval	=	"<span id='".$rowid."new_users'><a onclick=\"change_status('".$rowid."','new_users','active','0','id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$rowid."new_users'><a onclick=\"change_status('".$rowid."','new_users','active','1','id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";

				$sOutput .= '"'.str_replace('"', '', $dispval).'",';
				//$sOutput .= '"'.$dispval.'",';

				$editurl				=	$this->_baseurl.'/members/add/id/'.$aRow['id'];
				$viewurl				=	$this->_baseurl.'/members/view/id/'.$aRow['id'];
				$createsubscriberurl	=	$this->_baseurl.'/members/createsubscriber/id/'.$aRow['id'];
				$updatesubscriberurl	=	$this->_baseurl.'/members/createsubscriber/id/'.$aRow['id'].'/saction/updatesubscriber';
				$quickviewurl	=	$this->_baseurl.'/members/quickview/id/'.$aRow['id'];
				
				$delurl		=	"'".$this->_baseurl.'/members/delete/id/'.$aRow['id']."'";
				$style		=	'';

				if($aRow['active']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a>';
				
				if ( $_REQUEST['search_type'] != "A"  && $_REQUEST['search_type'] != "unchecked" && $_REQUEST['search_type'] != "pending" && $_REQUEST['search_type'] != "hinari"){
				$sOutput .= '<a class=\"button\"  href=\"'.$viewurl.'\"><img border=\"0\" title=\"View\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/magnifier.png\"/>View<span class=\"button\"></span></a>';
				}

				if(array_search($aRow['id'],$arrSubscribers)=='' && $aRow['active']=='1'){
				$sOutput .= '<a class=\"button\"  href=\"'.$createsubscriberurl.'\"><img border=\"0\" title=\"Create subscription\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Create subscription<span class=\"button\"></span></a>';
				}else if(array_search($aRow['id'],$arrSubscribers)!='' && $aRow['active']=='1'){
				$sOutput .= '<a class=\"button\"  href=\"'.$updatesubscriberurl.'\"><img border=\"0\" title=\"Update subscription\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Update subscription<span class=\"button\"></span></a>';
				}

				$sOutput .= '<a class=\"button\"  onclick=\"javascript:quick_view(\''.$quickviewurl.'\');\" ><img border=\"0\" title=\"Quick view\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/magnifier.png\"/>Quick view<span class=\"button\"></span></a>';

				$sOutput .= '<span id = \"'.$aRow['id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";

			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }';
			die($sOutput);
		}
	}

	// For Members statistics list ================================================================

	private function member_stats()
	{ 

	   if(count($_REQUEST)>0){	
			$sIndexColumn = "id";
			/* 
			* Paging
			*/
			$sLimit = "";
			$sWhere	= "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			if(!empty($_REQUEST['search_role'])){
				$sWhere	.= " AND FIND_IN_SET(role,'".$_REQUEST['search_role']."') ";
			}if(!empty($_REQUEST['search_spec'])){
				$sWhere	.= " AND FIND_IN_SET(speciality,'".$_REQUEST['search_spec']."') ";
			}if(!empty($_REQUEST['search_country'])){
				$sWhere	.= " AND FIND_IN_SET(Country,'".$_REQUEST['search_country']."') ";
			}
	
			$sQuery		=	" SELECT DATE_FORMAT( DateRegistered,  '%Y %b' ) AS smonth, COUNT( * ) AS cnt FROM  `new_users` WHERE 1 AND active='1' $sWhere GROUP BY DATE_FORMAT( DateRegistered,  '%Y %b' ) ORDER BY DateRegistered $sLimit";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM new_users WHERE 1 AND active='1' $sWhere GROUP BY DATE_FORMAT( DateRegistered,  '%Y %b' )";

			$iTotal = count($this->_db->fetchOne($sQuery));

			/*
			 * Output
			 */
			$temp	=	'';

			$cache	=	'';

			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';
			
			$total	=	0;
			foreach ( $rResult as $aRow){
				$total	 +=	$aRow['cnt'];
				$sOutput .= "[";				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['smonth']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cnt']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $total).'",';
				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For news filters ===========================================================================

	private function news_list()
	{
	
		if(count($_REQUEST)>0){	
			
			$aColumns = array('n.title','n.news_date','n.type','n.category','n.status','n.news_id');

			$sColumns =	array('n.title','n.news_date','n.type','n.category','n.status','n.news_id');

			$oColumns = array('n.title','n.news_date','n.type','n.category','n.status','n.news_id');
		
			$sIndexColumn = "news_id";

		   /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			if ( $_REQUEST['search_type'] != "A" ){				
					$sWhere	.=	' AND type = "'.$_REQUEST['search_type'].'"';
			} 		

			$sQuery		=	"SELECT ".implode(", ", $aColumns)." FROM  3bit_news n WHERE 1 $sWhere GROUP BY n.news_id $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM   3bit_news n WHERE 1 $sWhere	";

			$iTotal = $this->_db->fetchOne( $sQuery);			

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach ( $rResult as $aRow){
				$sOutput .= "[";
				for ( $i=0 ; $i<count($aColumns)-1; $i++ ){
						$fieldarr	=	explode(".",$aColumns[$i]);
						$fieldname	=	$fieldarr[1];
						/* General output */

						$dispval	=	$aRow[$fieldname];
						if($fieldname=='status'){
							if($aRow[$fieldname]=='1')
								$dispval	=	"<span id='".$aRow['news_id']."3bit_news'><a onclick=\"change_status('".$aRow['news_id']."','3bit_news','status','0','news_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
							else
								$dispval	=	"<span id='".$aRow['news_id']."3bit_news'><a onclick=\"change_status('".$aRow['news_id']."','3bit_news','status','1','news_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";


						}
						$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';
				}

				$editurl	=	$this->_baseurl.'/news/add/id/'.$aRow['news_id'];			
				$delurl		=	"'".$this->_baseurl.'/news/delete/id/'.$aRow['news_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['news_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}


	// For news filters ===========================================================================

	private function related_news()
	{
	
		if(count($_REQUEST)>0){	
			
			$aColumns = array('n.title','n.news_date','n.type','n.category','n.status','n.news_id');

			$sColumns =	array('n.title','n.news_date','n.type','n.category','n.status','n.news_id');

			$oColumns = array('n.news_id','n.title','n.news_date','n.type','n.category','n.status');
		
			$sIndexColumn = "news_id";

		   /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			}  		

			$sQuery		=	"SELECT ".implode(", ", $aColumns)." FROM  3bit_news n WHERE 1 $sWhere GROUP BY n.news_id $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery		= "	SELECT COUNT(".$sIndexColumn.") FROM   3bit_news n WHERE 1 ";

			$iTotal		= $this->_db->fetchOne($sQuery) or die(mysql_error());

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach ($rResult as $aRow){
				$sOutput .= "[";
				$sOutput .= '"'.str_replace('"', '\"', $aRow['news_id']).'",';
				for ( $i=0 ; $i<count($aColumns)-1; $i++ ){
						$fieldarr	=	explode(".",$aColumns[$i]);
						$fieldname	=	$fieldarr[1];
						/* General output */

						$dispval	=	$aRow[$fieldname];
						if($fieldname=='status'){
							if($aRow[$fieldname]=='1')
								$dispval	=	"<font color=\"green\">Active</font>";
							else
								$dispval	=	"<font color=\"red\">Inactive</font>";


						}
						$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';
				}				
				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	
	// Function for the recursive records in pages table ==========================================

	function make_pages($parentid)
	{	
		$res		=	$this->_db->fetchAll("SELECT * FROM 3bit_categories WHERE cat_parent_id='$parentid' ");	
		$numrows	=	count($res);
		if($numrows>0)	{
			foreach($res as $row){
				$json		.=		"["; 
				$form_id	=		$row['cat_id'];
				$ftitle		=		html_entity_decode(addslashes($row['cat_title']));
				$form_level	=		stripslashes($row['cat_level']);
				$new_ver	=       '&nbsp;';
				$preview	=		'';

				if ($form_level =='1'){				
					$tab ='';
				}else if ($form_level =='2'){
					$tab ='----- ';
				}else if ($form_level =='3'){
					$tab ='-------- ';
				}else if ($form_level =='4'){
					$tab ='----------- ';
				}else if ($form_level =='5'){				
					$tab ='-------------- ';
				}	
				
				$form_title		=		$tab.$ftitle; 
				$json			.=		'"'.str_replace('"', '\"', $row['cat_id']).'",';
				$json			.=		'"'.str_replace('"', '\"', $form_title).'",';

				$language		=		$this->_db->fetchOne("SELECT language_code from `3bit_language` where `language_id`='".$row['language_id']."'");
				$json			.=		'"'.str_replace('"', '\"', $language).'",';

				$json			.=		'"'.str_replace('"', '\"', $row['cat_type']).'",';
				$json			.=		'"'.str_replace('"', '\"', $row['cat_alias']).'",';

				$template		 =		$this->_db->fetchOne("SELECT temp_filename from `3bit_article_template` where `temp_id`='".$row['temp_id']."'");
				$json			.=		'"'.str_replace('"', '\"', $template).'",';

				if($row['status']=='1')
					$dispval	=	"<font color=\"green\">Active</font>";
				else
					$dispval	=	"<font color=\"red\">Inactive</font>";				

				$json .= '"'.str_replace('"', '\"', $dispval).'",';								

				$json = substr_replace( $json, "", -1 );
				$json .= "],";			
					
				 // Ends here -------------------------------			
				$json .= $this->make_pages($form_id);			
			}// while
		}		
		return $json;
	}

	// For related pages list in news module ======================================================

	private function related_pages()
	{ 
	   if(count($_REQUEST)>0){			

			$sColumns =	array('cat_title','cat_type','language_id','cat_alias','temp_id','status','cat_id');

			$oColumns = array('cat_title','cat_title','cat_title','cat_title','cat_title','cat_title');
		
			$sIndexColumn = "cat_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 			

			$sQuery		=	"SELECT * FROM 3bit_categories WHERE 1 AND cat_parent_id ='0' $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
			$sQuery =	"	SELECT COUNT(".$sIndexColumn.") FROM 3bit_categories ";

			$iTotal =	$this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach( $rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cat_id']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cat_title']).'",';

				$language =  $this->_db->fetchOne("SELECT language_code from `3bit_language` where `language_id`='".$aRow['language_id']."'");				
				$sOutput .= '"'.str_replace('"', '\"', $language).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cat_type']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['cat_alias']).'",';

				$template =  $this->_db->fetchOne("SELECT temp_filename from `3bit_article_template` where `temp_id`='".$aRow['temp_id']."'");
				$sOutput .= '"'.str_replace('"', '\"', $template).'",';
				if($aRow['status']=='1')
					$dispval	=	"<font color=\"green\">Active</font>";
				else
					$dispval	=	"<font color=\"red\">Inactive</font>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			
				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";

				$sOutput .= $this->make_pages($aRow['cat_id']);
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}	

	// For asset list =============================================================================

	private function asset_list()
	{    

	   if(count($_REQUEST)>0){	
			
			$aColumns = array('a.asset_title','a.asset_name','c.asset_category_title','a.status','a.asset_id');

			$sColumns =	array('a.asset_title','a.asset_name','a.asset_alt','a.asset_type','a.asset_owner','c.asset_category_title');

			$oColumns = array('a.asset_title','a.asset_name','c.asset_category_title','a.status','a.asset_id');
		
			$sIndexColumn = "asset_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT a.asset_title,a.asset_name,a.asset_type,a.asset_category_id,c.asset_category_title,a.status,a.asset_id FROM 3bit_assets a,3bit_asset_categories c WHERE a.asset_category_id=c.asset_category_id $sWhere GROUP BY a.asset_id $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_assets a,3bit_asset_categories c WHERE  a.asset_category_id=c.asset_category_id ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";

				if($aRow['asset_title']!='')
					$asstitle	=	$aRow['asset_title'];
				else
					$asstitle	=	$aRow['asset_name'];

				$downlink		=	'<a href="'.$this->_baseurl.'/assets/download/id/'.$aRow['asset_id'].'">download</a>';

				$asstitle		=	$asstitle." [".$downlink."]";

				$sOutput .= '"'.str_replace('"', '\"', $asstitle).'",';

				$viewurl	=	'/assets/thumbs/'.$aRow['asset_category_id'].'/'.$aRow['asset_name'];

				$type_ext	=	strtolower($aRow['asset_type']);

				if($type_ext == 'jpeg'||$type_ext == 'gif'||$type_ext == 'png'||$type_ext == 'jpg'){
					$sOutput .= '"<img src=\"'.$viewurl.'\" width=\"100\" height=\"75\"> ",';
				}else{
					$sOutput .= '"'.str_replace('"', '\"', 'none').'",';
				}

				$sOutput .= '"'.str_replace('"', '\"', $aRow['asset_category_title']).'",';
				
				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['asset_id']."3bit_assets'><a onclick=\"change_status('".$aRow['asset_id']."','3bit_assets','status','0','asset_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['asset_id']."3bit_assets'><a onclick=\"change_status('".$aRow['asset_id']."','3bit_assets','status','1','asset_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/assets/add/id/'.$aRow['asset_id'];				
				$delurl		=	"'".$this->_baseurl.'/assets/delete/id/'.$aRow['asset_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['asset_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For asset category list ====================================================================

	private function assetcat_list()
	{
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('asset_category_title','asset_category_type','asset_category_description','status','asset_category_id');

			$sColumns =	array('asset_category_title','asset_category_type','asset_category_description','status','asset_category_id');

			$oColumns = array('asset_category_title','asset_category_type','asset_category_description','status','asset_category_id');
		
			$sIndexColumn = "asset_category_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT asset_category_title,asset_category_type,asset_category_description,status,asset_category_id FROM 3bit_asset_categories WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_asset_categories ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach ( $rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['asset_category_title']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['asset_category_type']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['asset_category_description']).'",';
				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['asset_category_id']."3bit_asset_categories'><a onclick=\"change_status('".$aRow['asset_category_id']."','3bit_asset_categories','status','0','asset_category_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['asset_category_id']."3bit_asset_categories'><a onclick=\"change_status('".$aRow['asset_category_id']."','3bit_asset_categories','status','1','asset_category_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/assetcat/add/id/'.$aRow['asset_category_id'];				
				$delurl		=	"'".$this->_baseurl.'/assetcat/delete/id/'.$aRow['asset_category_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['asset_category_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For courier list ==============================================================================================

	private function courier_list()
	{	 
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('courier_name','courier_url','courier_status','courier_id');

			$sColumns =	array('courier_name','courier_url','courier_status','courier_id');

			$oColumns = array('courier_name','courier_url','courier_status','courier_id');
		
			$sIndexColumn = "courier_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_courier WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_courier ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['courier_name']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['courier_url']).'",';
				if($aRow['courier_status']=='1')
					$dispval	=	"<span id='".$aRow['courier_id']."3bit_courier'><a onclick=\"change_status('".$aRow['courier_id']."','3bit_courier','courier_status','0','courier_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['courier_id']."3bit_courier'><a onclick=\"change_status('".$aRow['courier_id']."','3bit_courier','courier_status','1','courier_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/couriers/add/id/'.$aRow['courier_id'];				
				$delurl		=	"'".$this->_baseurl.'/couriers/delete/id/'.$aRow['courier_id']."'";
				$style		=	'';

				if($aRow['courier_status']=='1')
					$style	=	"style='display:none'"; 				

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['courier_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For vat list ==============================================================================================

	private function vat_list()
	{ 		
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('vat_code','vate_rate','is_live','status','vat_id');

			$sColumns =	array('vat_code','vate_rate','is_live','status','vat_id');

			$oColumns = array('vat_code','vate_rate','is_live','status','vat_id');
		
			$sIndexColumn = "vat_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_product_vats WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_product_vats ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['vat_code']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['vat_rate']).'",';
				$chk	 =	'';
				if($aRow['is_live']=='1'){
					$chk	=	'checked';
				}

				$chkbox	  = "<input type='checkbox' name='is_live' id='chklive".$aRow['vat_id']."' ".$chk."  onclick=\"changeLive('".$aRow['vat_id']."','chklive".$aRow['vat_id']."');\">";

				$sOutput .= '"'.str_replace('"', '\"', $chkbox).'",';		


				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['vat_id']."3bit_product_vats'><a onclick=\"change_status('".$aRow['vat_id']."','3bit_product_vats','status','0','vat_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['vat_id']."3bit_product_vats'><a onclick=\"change_status('".$aRow['vat_id']."','3bit_product_vats','status','1','vat_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl	=	$this->_baseurl.'/vatrates/add/id/'.$aRow['vat_id'];				
				$delurl		=	"'".$this->_baseurl.'/vatrates/delete/id/'.$aRow['vat_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 			

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['vat_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For country list ==============================================================================================

	private function country_list()
	{ 
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('country','iso_code','zone_id','is_eu','is_gc','status','id');

			$sColumns =	array('country','iso_code','zone_id','is_eu','is_gc','status','id');

			$oColumns = array('country','iso_code','zone_id','is_eu','is_gc','status','id');
		
			$sIndexColumn = "id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_country WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_country ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['country']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['iso_code']).'",';
				
				if($aRow['zone_id']==1)
					$zone_name	=	'Zone 1';
				if($aRow['zone_id']==2)
					$zone_name	=	'Zone 2';
				if($aRow['zone_id']==3)
					$zone_name	=	'Zone 3';
				if($aRow['zone_id']==4)
					$zone_name	=	'Zone 4';
				if($aRow['zone_id']==5)
					$zone_name	=	'Zone 5';

				$sOutput .= '"'.str_replace('"', '\"', $zone_name).'",';

				$chk	 =	'';
				if($aRow['is_eu']=='1'){
					$chk	=	'checked';
				}

				$gchk	 =	'';
				if($aRow['is_gc']=='1'){
					$gchk	=	'checked';
				}

				$chkbox	  = "<input type='checkbox' name='chkeu_".$aRow['id']."' id='chkeu_".$aRow['id']."' ".$chk."  onclick=\"changeEU('".$aRow['id']."',this);\" value='1'>";

				$sOutput .= '"'.str_replace('"', '\"', $chkbox).'",';
				
				$gchkbox	  = "<input type='checkbox' name='chkgc_".$aRow['id']."' id='chkgc_".$aRow['id']."' ".$gchk."  onclick=\"changeGC('".$aRow['id']."',this);\" value='1'>";

				$sOutput .= '"'.str_replace('"', '\"', $gchkbox).'",';

				$editurl	=	$this->_baseurl.'/countries/add/id/'.$aRow['id'];				
				$delurl		=	"'".$this->_baseurl.'/countries/delete/id/'.$aRow['id']."'";

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For messages list ==============================================================================================

	private function message_list()
	{ 

	   if(count($_REQUEST)>0){	
			
			$aColumns = array('message_type','message_to','active','message_id');

			$sColumns =	array('message_type','message_to','active','message_id');

			$oColumns = array('message_type','message_to','active','message_id');
		
			$sIndexColumn = "message_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_messages WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_messages ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['message_type']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['message_to']).'",';
				if($aRow['active']=='1')
					$dispval	=	"<span id='".$aRow['message_id']."3bit_messages'><a onclick=\"change_status('".$aRow['message_id']."','3bit_messages','active','0','message_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['message_id']."3bit_messages'><a onclick=\"change_status('".$aRow['message_id']."','3bit_messages','active','1','message_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';					

				$editurl	=	$this->_baseurl.'/messages/add/id/'.$aRow['message_id'];				
				$delurl		=	"'".$this->_baseurl.'/messages/delete/id/'.$aRow['message_id']."'";
				$style		=	'';

				if($aRow['active']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['message_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For shhippings list ==============================================================================================

	private function shipping_list()
	{ 
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('weight','zone1_charge','zone2_charge','zone3_charge','ship_id');

			$sColumns =	array('weight','zone1_charge','zone2_charge','zone3_charge','ship_id');

			$oColumns = array('weight','zone1_charge','zone2_charge','zone3_charge','ship_id');
		
			$sIndexColumn = "ship_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_shipping WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_shipping ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['weight']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['zone1_charge']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['zone2_charge']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['zone3_charge']).'",';
				
				$editurl	=	$this->_baseurl.'/shippings/add/id/'.$aRow['ship_id'];				
				$delurl		=	"'".$this->_baseurl.'/shippings/delete/id/'.$aRow['ship_id']."'";
				$style		=	'';			

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For stock list ==============================================================================================

	private function stock_list()
	{ 
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('product_name','product_id','product_code','supplier_stock','site_stock','available_stock','status','product_id');

			$sColumns =	array('product_name','product_id','product_code','supplier_stock','site_stock','available_stock','status','product_id');

			$oColumns = array('product_name','product_id','product_code','supplier_stock','site_stock','available_stock','status','product_id');
		
			$sIndexColumn = "product_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_products WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_products ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";

				$product_name	=	$aRow['product_name'];

				if($aRow['size'] !=''){
					$product_name	.=	"( ".$aRow['size']." )";
				}
				
				$sOutput .= '"'.str_replace('"', '\"', $product_name).'",';
				
				$delstk	  =	"<input type='text' name='deliv_stock".$aRow['product_id']."' id='deliv_stock".$aRow['product_id']."' onkeypress=\"return blockNumbers(event);\" size='5'>&nbsp;&nbsp;<a style=\"cursor:pointer\" onclick=\"updateDeliver('deliv_stock".$aRow['product_id']."','".$aRow['product_id']."');\"><img src=\"".addslashes($this->_baseurl)."/public/images/icons/add.png\" border='0'></a>";

				$sOutput .= '"'.str_replace('"', '\"', $delstk).'",';	

				$sOutput .= '"'.str_replace('"', '\"', $aRow['product_code']).'",';

				$supstk	  =  "<input type='text' name='stocklev".$aRow['product_id']."' id='stocklev".$aRow['product_id']."' value='".$aRow['supplier_stock']."' onkeypress=\"return blockNumbers(event);\" onkeyup=\"ctrlup(event);\" onkeydown=\"moveStock('".$aRow['product_id']."',event);\" onblur=\"updatestk(this.value,$('#sitestocklev".$aRow['product_id']."').val(),'".$aRow['product_id']."');\" size='5'>";

				$sOutput .= '"'.str_replace('"', '\"', $supstk).'",';	

				$sitestk =  "<input type='text' name='sitestocklev".$aRow['product_id']."' id='sitestocklev".$aRow['product_id']."' value='".$aRow['site_stock']."'  onkeypress=\"return blockNumbers(event);\" onkeyup=\"ctrlup(event);\" onkeydown=\"updateSiteStock('sitestocklev".$aRow['product_id']."',event);\"  onblur=\"updatesite(this.value,'".$aRow['product_id']."');\" size='5'> ";

				$sOutput .= '"'.str_replace('"', '\"', $sitestk).'",';

				$avastk	=	"<span id='avi".$aRow['product_id']."'>".$aRow['available_stock']."</span>";

				$sOutput .= '"'.str_replace('"', '\"', $avastk).'",';

				if($aRow['status']=='1')
					$dispval	=	"<span id='".$aRow['product_id']."products'><a onclick=\"change_status('".$aRow['product_id']."','products','status','0','	product_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval	=	"<span id='".$aRow['product_id']."products'><a onclick=\"change_status('".$aRow['product_id']."','products','status','1','product_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';					

				$editurl	=	$this->_baseurl.'/products/add/id/'.$aRow['product_id'];				
				$delurl		=	"'".$this->_baseurl.'/products/delete/id/'.$aRow['product_id']."'";
				
				$refbtn		=	"<a style='cursor:pointer' alt='Refresh' title='Refresh' onclick=\"refreshStock('sitestocklev".$aRow['product_id']."','".$aRow['product_id']."');\"><img src=\"".addslashes($this->_baseurl)."/public/images/icons/arrow_refresh.png\" border='0'></a>";

				$sOutput .= '"'.str_replace('"', '\"', $refbtn).'",';	

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// Function to update site,available stock =================================================

	public function updatedeliverAction()
	{
		if($_POST['deliverstk'] !=''){
			$product_id	=	$_POST['prodid'];
			$deliverstk	=	$_POST['deliverstk'];
			$updsql		=	$this->_db->query("update 3bit_products set site_stock=site_stock+".$deliverstk.",available_stock=available_stock+".$deliverstk.",modified_date=now() where product_id = '".$product_id."'");
			if($updsql){
			   $selProd			=	$this->_db->fetchRow("SELECT product_name,product_code FROM 3bit_products WHERE product_id ='".$product_id."'");
			   if($selProd){
				  $prod_name	=	$selProd['product_name'];
				  $prod_code	=	$selProd['product_code'];
			   }
			   $selStock		=	$this->_db->fetchRow("SELECT available_stock,site_stock FROM 3bit_products WHERE product_id = '".$product_id."'");
			   if($selStock){
				  $avistk		=	$selStock['available_stock'];
				  $sitestk		=	$selStock['site_stock'];
			   }

			   $adminurl		=	"<a href='".$this->_baseurl."/common/adminhistory/id/".$_SESSION['wlsession']['user_id']."' rel='facebox'>".$_SESSION['wlsession']['fullname']."</a>";

			   $this->_db->query("INSERT INTO `3bit_order_history` SET status	='".addslashes($adminurl)." has added ".$deliverstk." units to ".$prod_code." ".$prod_name." on <span class=\'dashdate\'>".date('jS M Y H:i:s')."</span><br />Stock Update: ".$sitestk." Available: ".$avistk."',user_id='".$_SESSION['wlsession']['user_id']."',history_date=now()");		   		   
			   die($avistk."^".$sitestk."^"."S");
			}else{
			  die("#"."^"."#"."^"."F");
			}
		}
	}


	// Function to refresh available stock =================================================

	public function refreshstockAction()
	{		
		$product_id	=	$_POST['prodid'];
		$sitestk	=	$_POST['sitestk'];
		$stkPrs		=	$this->_db->fetchRow("SELECT o.order_id, o.status, od. product_id , sum( od.quantity ) as pqty FROM `3bit_orders` o, 3bit_order_details od WHERE (o.status != 'Shipped' AND o.status != 'Archived' AND o.status !='Rejected') AND od.product_id = '".$product_id."' AND o.order_id = od.order_id GROUP BY od.product_id");
		if($stkPrs)															  
			$processstk	=	$stkPrs['pqty'];
 	    else
			$processstk	=	0;
		$availablestk	=	($sitestk-$processstk);
		$updsql			=	$this->_db->query("UPDATE 3bit_products SET site_stock=".$sitestk.",available_stock=".$availablestk.",modified_date=now() WHERE product_id = '".$product_id."'");
		if($updsql){
			die($availablestk."^".$sitestk."^"."S");
		}else{
		    die("#"."^"."#"."^"."F");
		}
	}


	// Function to update stock =================================================

	public function updatestockAction()
	{
		$product_id	=	$_POST['product_id'];

		$pstock		=	$_POST['stocklevel'];
		
		$psite		=	$_POST['site_stocklevel'];

		$avistk		=	$_POST['avival'];

		if($pstock<>""){
			$selStock		=	$this->_db->fetchRow("SELECT product_id,supplier_stock,site_stock FROM 3bit_products WHERE product_id = '".$product_id."'");
			if(!$selStock){		
				$this->_db->query("INSERT INTO 3bit_products SET product_id = '".$product_id."',supplier_stock='".$pstock."',created_date=now(),modified_date=now()");			
			}else{
				$bstock		=	$selStock['supplier_stock'];
				$bsstock	=	$selStock['site_stock'];			
				$this->_db->query("UPDATE 3bit_products SET supplier_stock='".$pstock."',site_stock='".$psite."',available_stock='".$avistk."',modified_date=now() WHERE product_id = '".$product_id."'");	
			}

			$selU		=	$this->_db->fetchRow("SELECT product_id,supplier_stock,site_stock FROM 3bit_products WHERE product_id = '".$product_id."'");
			if($selU){
				$stock	=	$selU['supplier_stock'];
				$sstock	=	$selU['site_stock'];
			}
			die($stock."~".$sstock."^"."S");
		}else{
			die(" "."^"."F");
		}
	}

	// Function to update sitestock =================================================

	public function updatesiteAction()
	{
	
		$product_id	=	$_POST['product_id'];

		$sitestk	=	$_POST['sitestock'];

		$avistk		=	$_POST['avival'];

		if(($avistk=='' || $avistk>0) && $sitestk>0){						  
			$selPro		=	$this->_db->fetchRow("SELECT sum(od.quantity)as qty,count(product_id)as pcnt FROM 3bit_order_details od,3bit_orders o WHERE o.order_id=od.order_id AND od.product_id='".$product_id."' AND (o.status='New' OR o.status='Accepted') ");			
			if($selPro){	  			
				$count_prod	=	$selPro['pcnt'];
				if($count_prod>0){
					 $prostk	=	$selPro['qty'];
					 $avistk	=	$sitestk-$prostk;
				}else{
					 $avistk	=	$sitestk;
				}
			}else{
				$avistk	=	$sitestk;	
			}
		}

		$updsql		=	$this->_db->query("update 3bit_products set site_stock='".$sitestk."',available_stock='".$avistk."',modified_date=now() where product_id = '".$product_id."'");
		if($updsql){
		   die($sitestk."^".$avistk."^"."S");
		}else{
		  die($sitestk."^".$avistk."^"."F");
		}
	}

	// For general products filters =============================================================================================

	private function general_products()
	{
		
		if(count($_REQUEST)>0){	

			$sColumns =	array('p.product_name','p.product_shortname','p.description','p.product_code','p.style','p.status');

			$oColumns = array('p.product_name','p.product_shortname','p.style','p.status');
		
			$sIndexColumn = "product_id";

		   /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			/* 
			 * Filtering
			 * NOTE this does not match the built-in DataTables filtering which does it
			 * word by word on any field. It's possible to do here, but concerned about efficiency
			 * on very large tables, and MySQL's regex functionality is very limited
			 */
			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 
			

			$sQuery		=	"SELECT p.*, a.asset_category_id, a.asset_name, p.price FROM 3bit_products p LEFT JOIN 3bit_assets a ON ( p.product_image = a.asset_id ) WHERE p.parent_id='0' AND p.size = '' $sWhere GROUP BY p.product_id $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";		

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_products p WHERE p.parent_id='0' AND p.size = '' ";
			
			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";

				$sOutput .= '"'.str_replace('"', '\"', '<strong>'.stripslashes($aRow['product_name']).'</strong>').'",';

				if($aRow['variation_exist']!='1')
					$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['product_code'])).'",';
				else
					$sOutput .= '"'.str_replace('"', '\"', '').'",';

				if($aRow['variation_exist']=='1')
					$dispval	=	'';
				else
					$dispval  =	"<a rel='facebox' href=\"/assets/thumbs/".$aRow['asset_category_id']."/".$aRow['asset_name']."\">View</a>";

				$sOutput .= '"'.str_replace('"', '\"', $dispval).'",';

				if($aRow['variation_exist']!='1')
					$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['price'])).'",';
				else
					$sOutput .= '"'.str_replace('"', '\"', '').'",';

				if($aRow['variation_exist']!='1'){
					if($aRow['status']=='1')
						$statval	=	"<span id='".$aRow['product_id']."3bit_products'><a onclick=\"change_status('".$aRow['product_id']."','3bit_products','status','0','product_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
					else
						$statval	=	"<span id='".$aRow['product_id']."3bit_products'><a onclick=\"change_status('".$aRow['product_id']."','3bit_products','status','1','product_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";
				}else
					 $statval	=	'';

				$sOutput .= '"'.str_replace('"', '\"', $statval).'",';				

				$editurl	=	$this->_baseurl.'/products/generaladd/id/'.$aRow['product_id'];				
				$delurl		=	"'".$this->_baseurl.'/products/delete/id/'.$aRow['product_id']."'";
				$style		=	'';

				if($aRow['status']=='1')
					$style	=	"style='display:none'"; 

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><span id = \"'.$aRow['product_id'].'\" '.$style.'><a class=\"button\" onclick=\"return open_dialog('.$delurl.');\"><img border=\"0\" title=\"Delete\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/delete.png\"/>Delete</a></span>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";

				// List of variants goes here ========================================================

				$selVar		=	$this->_db->fetchAll("SELECT p.*, a.asset_category_id, a.asset_name, p.price FROM 3bit_products p LEFT JOIN 3bit_assets a ON ( p.product_thumb = a.asset_id ) WHERE (p.parent_id='".$aRow['product_id']."' OR p.product_id='".$aRow['product_id']."') AND p.variation_exist='1' AND p.size = ''  GROUP BY p.product_id ORDER BY p.style ASC ");

				if($selVar){

					foreach($selVar as $vRow){

						$sOutput	.=		"[";

						$sOutput	.=		'"'.str_replace('"', '\"', '&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($vRow['style'])).'",';
						$sOutput	.=		'"'.str_replace('"', '\"', '&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($vRow['product_code'])).'",';

						$varval		=		"<a rel='facebox' href=\"assets/thumbs/".$vRow['asset_category_id']."/".$vRow['asset_name']."\">View</a>";

						$sOutput	.=		'"'.str_replace('"', '\"', $varval).'",';

						$sOutput	.=		'"'.str_replace('"', '\"', stripslashes($vRow['price'])).'",';

						if($vRow['status']=='1')
							$vstatval	=	"<span id='".$vRow['product_id']."3bit_products'><a onclick=\"change_status('".$vRow['product_id']."','3bit_products','status','0','product_id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
						else
							$vstatval	=	"<span id='".$vRow['product_id']."3bit_products'><a onclick=\"change_status('".$vRow['product_id']."','3bit_products','status','1','product_id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";

						$sOutput .= '"'.str_replace('"', '\"', $vstatval).'",';	
						
						$sOutput .= '"'.str_replace('"', '\"', "").'",';		

						$sOutput = substr_replace( $sOutput, "", -1 );
						
						$sOutput .= "],";

					}

				}	 
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For customers list ==============================================================================================

	private function customer_list()
	{ 		
	   if(count($_REQUEST)>0){	
			
			$aColumns = array('fname','fname','created_date','postcode','email','customer_id');

			$sColumns =	array('fname','fname','created_date','postcode','email','customer_id');

			$oColumns = array('fname','fname','created_date','postcode','email','customer_id');
		
			$sIndexColumn = "customer_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sQuery		=	"SELECT * FROM 3bit_customers WHERE 1 $sWhere $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);	


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_customers ";

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['fname']." ".$aRow['lname']).'",';

				$ordercnt	=	0;
				$lastdate	=	'';

				$selCnt		=	$this->_db->fetchRow("SELECT count(order_id) as order_cnt,ordered_date FROM 3bit_orders WHERE customer_id='".$aRow['customer_id']."' GROUP BY customer_id ORDER BY ordered_date DESC");

				if($selCnt){
				  $ordercnt		=	$selCnt['order_cnt'];
				  $lastdate		=	$selCnt['ordered_date'];
				}

				$sOutput .= '"'.str_replace('"', '\"', $ordercnt).'",';

				$sOutput .= '"'.str_replace('"', '\"', $lastdate).'",';				
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['postcode']).'",';				

				$sOutput .= '"'.str_replace('"', '\"', $aRow['email']).'",';				
				
				$editurl	=	$this->_baseurl.'/customers/add/id/'.$aRow['customer_id'];				
				$historyurl	=	$this->_baseurl.'/customers/history/id/'.$aRow['customer_id'];

				$sOutput .= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit<span class=\"button\"></span></a><a class=\"button\" href=\"'.$historyurl.'\"><img border=\"0\" title=\"History\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/magnifier.png\"/>View history</a>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For orders list ==============================================================================================

	private function orders_list()
	{
       if(count($_REQUEST)>0){			    
			
			$aColumns = array('o.order_id','c.fname','o.ordered_date','o.ordertotal','o.order_id');

			$sColumns =	array('o.order_id','c.fname','o.ordered_date','o.ordertotal','o.order_id');

			$oColumns = array('o.order_id','c.fname','o.ordered_date','o.ordertotal','o.order_id');
		
			$sIndexColumn = "user_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			if(!empty($_POST['search_year']) && $_POST['search_year']<>"A"){
				$sWhere	.=	" AND YEAR(o.ordered_date) ='".$_REQUEST['search_year']."' ";				
			}

			if(!empty($_POST['search_month']) && $_POST['search_month']<>"A"){			
				$sWhere	.= " AND DATE_FORMAT(o.ordered_date,'%c')='".$_POST['search_month']."' ";  				
			}

			if(!empty($_POST['search_stat']) && $_POST['search_stat']<>"A"){	
				if($_POST['search_stat']=='Cancelled')
					$sWhere	.=	" AND (o.status ='Rejected' OR o.status='Archived') ";				
				else
					$sWhere	.=	" AND o.status ='".$_POST['search_stat']."' ";				
			}


			$sQuery		=	"SELECT o.*,concat(c.fname,' ',c.lname)as fullname FROM 3bit_orders o,3bit_customers c WHERE o.customer_id=c.customer_id $sWhere GROUP BY o.order_id $sOrder $sLimit ";			

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);			

			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_orders o,3bit_customers c WHERE o.customer_id=c.customer_id $sWhere";

			$iTotal = $this->_db->fetchOne($sQuery);
			
			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";
				
				$sOutput .= '"'.str_replace('"', '\"', $aRow['order_id']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['fullname']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['ordered_date']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['ordertotal']).'",';								
				$viewurl	=	$this->_baseurl.'/orders/view/id/'.$aRow['order_id'];				

				$sOutput .= '"<a class=\"button\"  href=\"'.$viewurl.'\"><img border=\"0\" title=\"View\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/magnifier.png\"/>View<span class=\"button\"></span></a>",';

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}		
	}

	// For manual products filters =============================================================================================

	function manual_products(){
	
		if(count($_REQUEST)>0){	

			$sColumns =	array('p.product_name','p.product_shortname','p.description','p.product_code','p.style','p.status');

			$oColumns = array('p.product_name','p.product_shortname','p.style','p.status');
		
			$sIndexColumn = "product_id";

		   /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			/* 
			 * Filtering
			 * NOTE this does not match the built-in DataTables filtering which does it
			 * word by word on any field. It's possible to do here, but concerned about efficiency
			 * on very large tables, and MySQL's regex functionality is very limited
			 */
			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 
			

			$sQuery		=	"SELECT p.*, a.asset_category_id, a.asset_name, p.price FROM 3bit_products p LEFT JOIN 3bit_assets a ON ( p.product_thumb = a.asset_id ) WHERE p.parent_id='0' AND p.procat_id !='3,' $sWhere GROUP BY p.product_id $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.") FROM 3bit_products p WHERE p.parent_id='0' ";			

			$iTotal = $this->_db->fetchOne($sQuery);

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){
				$sOutput .= "[";

				$sOutput .= '"'.str_replace('"', '\"', '<strong>'.stripslashes($aRow['product_name']).'</strong>').'",';

				if($aRow['variation_exist']!='1')
					$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['product_code'])).'",';
				else
					$sOutput .= '"'.str_replace('"', '\"', '').'",';
			

				if($aRow['variation_exist']!='1')
					$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['price'])).'",';
				else
					$sOutput .= '"'.str_replace('"', '\"', '').'",';

				if($aRow['variation_exist']!='1'){
					if($aRow['status']=='1')
						$statval	=	"<font color=\"green\">Active</font>";
					else
						$statval	=	"<font color=\"red\">Inactive</font>";
				}else
					 $statval	=	'';

				$sOutput .= '"'.str_replace('"', '\"', $statval).'",';							

				if($aRow['variation_exist']!='1'){
					$sOutput .= '"<a class=\"button\" onclick=addmanual_cart(\"'.$aRow['product_id'].'\",\"'.$_SESSION['wlsession']['asess_id'].'\")><img border=\"0\" title=\"Add\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Add<span class=\"button\"></span></a>",';
				}else{
					$sOutput .= '"'.str_replace('"', '\"', '').'",';
				}
				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";

				// List of variants goes here ========================================================

				$selVar		=	$this->_db->fetchAll("SELECT p.*, a.asset_category_id, a.asset_name, p.price FROM 3bit_products p LEFT JOIN 3bit_assets a ON ( p.product_thumb = a.asset_id ) WHERE (p.parent_id='".$aRow['product_id']."' OR p.product_id='".$aRow['product_id']."') AND p.variation_exist='1' GROUP BY p.product_id ORDER BY p.style ASC ");

				if($selVar){

					foreach($selVar as $vRow){

						$sOutput	.=		"[";

						if($vRow['style']!=''){
							$sOutput	.=		'"'.str_replace('"', '\"', '&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($vRow['style'])).'",';
						}else if($vRow['size']!=''){
							$sOutput	.=		'"'.str_replace('"', '\"', '&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($vRow['size'])).'",';
						}
						$sOutput	.=		'"'.str_replace('"', '\"', '&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($vRow['product_code'])).'",';
						
						$sOutput	.=		'"'.str_replace('"', '\"', stripslashes($vRow['price'])).'",';

						if($vRow['status']=='1')
							$vstatval	=	"<font color=\"green\">Active</font>";
						else
							$vstatval	=	"<font color=\"red\">Inactive</font>";

						$sOutput .= '"'.str_replace('"', '\"', $vstatval).'",';	
						
						$sOutput .= '"<a class=\"button\" onclick=addmanual_cart(\"'.$vRow['product_id'].'\",\"'.$_SESSION['wlsession']['asess_id'].'\")><img border=\"0\" title=\"Add\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Add<span class=\"button\"></span></a>",';	

						$sOutput = substr_replace( $sOutput, "", -1 );
						
						$sOutput .= "],";

					}

				}	 
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}

	// For manual customers filters =============================================================================================

	function manual_customers(){
	
		if(count($_REQUEST)>0){	

			$sColumns =	array('c.fname','c.fname','o.ordered_date','c.postcode','c.email','c.fname');

			$oColumns = array('c.fname','c.fname','o.ordered_date','c.postcode','c.email','c.fname');
		
			$sIndexColumn = "c.customer_id";

		   /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			/* 
			 * Filtering
			 * NOTE this does not match the built-in DataTables filtering which does it
			 * word by word on any field. It's possible to do here, but concerned about efficiency
			 * on very large tables, and MySQL's regex functionality is very limited
			 */
			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 
			

			$sQuery		=	"SELECT c. * , DATE_FORMAT(o.ordered_date,'%d-%m-%Y %H:%i:%s')as odate, COUNT( c.customer_id ) AS cnt FROM  `3bit_customers` c, 3bit_orders o WHERE 1  AND o.customer_id = c.customer_id  $sWhere GROUP BY c.customer_id $sOrder $sLimit ";

			$rResult	=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery		=	"SELECT FOUND_ROWS()";
			
			$iFilteredTotal = $this->_db->fetchOne($sQuery);


			/* Total data set length */
			$sQuery = "	SELECT COUNT(".$sIndexColumn.")  FROM  `3bit_customers` c, 3bit_orders o WHERE 1  AND o.customer_id = c.customer_id GROUP BY c.customer_id ORDER BY o.ordered_date DESC ";

			$iTotal = count($this->_db->fetchAll($sQuery));

			/*
			 * Output
			 */
			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach($rResult as $aRow){

				$sOutput .= "[";

				$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['fname']." ".$aRow['lname'])).'",';

				$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['cnt'])).'",';

				$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['odate'])).'",';

				$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['postcode'])).'",';				

				$sOutput .= '"'.str_replace('"', '\"', stripslashes($aRow['email'])).'",';								

				$sOutput .= '"<a class=\"button\" onclick=addmanual_customer(\"'.$aRow['customer_id'].'\",\"'.$_SESSION['wlsession']['asess_id'].'\")><img border=\"0\" title=\"Add\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Select<span class=\"button\"></span></a>",';	

				$sOutput = substr_replace( $sOutput, "", -1 );
				$sOutput .= "],";
			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }'; 		
			die($sOutput);
		}	
	}


	// Function to filter the dashboard orders //

	public function filterdashAction()
	{	
		$type		=	$_POST['type'];
		$duration	=	$_POST['duration'];
		$cyear		=	date("Y");

		$cmon		=	date("n");
		if($cmon	==	12)
			$cyear		=	$cyear+1;		 

		$prev_year	=	$cyear-1;	

		$prev_date	=	$prev_year."-12-01";

		$sear_year	=	$cyear;
 
		$sear_date =   $sear_year."-11-30";

		if($type=='shipped'){
			$sql	=	"SELECT o.order_id,o.invoice_number,concat(c.fname, ' ',c.lname)as Name, o.ordered_date ,o.shipped_date, o.ordertotal ,o.status,o.payment_status,o.courier_id,o.tracking_info FROM 3bit_orders AS o INNER JOIN 3bit_customers AS c ON o.customer_id = c.customer_id WHERE o.status='Shipped' AND o.ccerrcode !=''"; 

			if($duration=='all')
			   $sql	.=	" AND date(o.shipped_date) <='".$sear_date."' AND date(o.shipped_date) >='".$prev_date."' ";
			else if($duration=='today')
			   $sql	.=	" AND  DATE_FORMAT(o.shipped_date,'%Y-%m-%d')= '".date("Y-m-d")."'";
			else if($duration=='week')
			   $sql	.=	" AND  YEARWEEK(o.shipped_date)= YEARWEEK(current_date)";
			else if($duration=='month')
			   $sql	.=	" AND  MONTH(o.shipped_date)= MONTH(current_date) AND YEAR(o.shipped_date)= YEAR(current_date)";	

			$sql	.=	" ORDER BY o.shipped_date DESC";

			
			$result	=	$this->_db->fetchAll($sql);

			$trows	=	count($result);

			$total_today=	0;
	?>
			<a style="cursor:pointer" onclick="filter_dash('all','shipped')" >All</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('today','shipped')" >Today</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('week','shipped')" >Week</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('month','shipped')" >Month</a>

	<?php   if($trows>0){	?>	
					<table width='100%' cellpadding="0" cellspacing="0" border="0" class="display">
					<thead>
					<tr>				
						<th height="20" class="helpHed">Order No</th>					
						<th>Customer Name</th>
						<th>Shipped Date</th>
						<th>Status</th>
						<th>OrderTotal</th>
						<th>Courier</th>
						<th>Tracking Info</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					 <?php foreach($result as $row){ 
							 $couriername	=	'N/A';
							 if($row['courier_id']=='0')
								 $status	=	'Completed';
							 else
								 $status	=	$row['status'];

							$selCTit		=	$this->_db->fetchRow("SELECT courier_name,courier_url FROM 3bit_courier WHERE courier_id='".$row['courier_id']."'");				if($selCTit){
								$couriername	=	$selCTit['courier_name'];
								$courierurl		=	$selCTit['courier_url'];
							}

							$total_today	+=	$row['ordertotal'];
					?>
						 <tr>
							 <td><?=$row['order_id']?>/<?=$row['invoice_number']?></td>
							 <td><?=$row['Name']?></td>
							 <td><?=$row['shipped_date']?></td>
							 <td><?=$status?></td>
							 <td>&pound;<?=$row['ordertotal']?></td>
							 <td><?=$couriername?></td>
							 <td><?=$row['tracking_info']?></td>
							 <td><a href="<?=$this->_baseurl?>/orders/view/id/<?=$row['order_id']?>" class='button'><img src="<?=$this->_baseurl?>/public/images/icons/page_white_text.png" alt="View Order" title="View Order" border='0' />View</a></td>
						 </tr>
					 <?php } ?>
					</tbody>
					<tfoot>
						  <tr>
							  <td colspan="8">Total Orders Dispatched(<?=$trows?>)</td>
						  </tr>
						  <tr>
							  <td colspan="8">Total Value Of Orders &pound;<?php echo number_format($total_today,2,'.','');?></td>
						  </tr>
					</tfoot>
					</table>

				<?php
				}

		}else if($type=='sales'){
			   $sql	= "SELECT oa.*,o.customer_id,o.order_id,concat(c.fname, ' ',c.lname)as Name,o.shipped_date,o.status FROM 3bit_order_actions oa,3bit_orders o,3bit_customers c WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='New' OR oa.action='Deposit' OR oa.action='Deposit Balance Pay' OR oa.action='Refunded') AND o.payment_status != 'Declined (General).' ";

			   if($duration=='all')
					$sql	.=	" AND date(oa.action_date) <='".$sear_date."' AND date(oa.action_date) >='".$prev_date."' ";
			   else if($duration=='today')
				   $sql	.=	" AND  DATE_FORMAT(oa.action_date,'%Y-%m-%d')= '".date("Y-m-d")."'";
				else if($duration=='week')
				   $sql	.=	" AND  YEARWEEK(oa.action_date)= YEARWEEK(current_date)";
				else if($duration=='month')
				   $sql	.=	" AND  MONTH(oa.action_date)= MONTH(current_date) AND YEAR(oa.action_date)= YEAR(current_date)";	

				$sql	.=	" ORDER BY oa.action_date DESC";

				$result	 =	$this->_db->fetchAll($sql);

				$srows	=	count($result);

				$total_sales	=	0;
		?>
			<a style="cursor:pointer" onclick="filter_dash('all','sales')" >All</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('today','sales')" >Today</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('week','sales')" >Week</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('month','sales')" >Month</a>

		<?php	if($srows>0){  ?>
					

					<table width='100%' cellpadding="0" cellspacing="0" border="0" class="display">
					<thead>
					<tr>				
						<th height="20" class="helpHed">Order No</th>					
						<th>Customer Name</th>
						<th>OrderDate</th>
						<th>Status</th>
						<th>OrderTotal</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($result as $row){ 
							if($row['status']=='Deposit')
								$astat	=	'Deposited';
							else if($row['status']=='Deposit Balance Pay')
								$astat	=	'Accepted';
							else
								$astat	=	$row['action'];
							$total_sales	+=	$row['amount_paid'];
					?>
						 <tr>
							 <td><?=$row['order_id']?></td>
							 <td><?=$row['Name']?></td>
							 <td><?=$row['action_date']?></td>
							 <td><?=$row['status']?></td>
							 <td>&pound;<?=$row['amount_paid']?></td>
							 <td><a href="<?=$this->_baseurl?>/orders/view/id/<?=$row['order_id']?>" class='button'><img src="<?=$this->_baseurl?>/public/images/icons/page_white_text.png" alt="View Order" title="View Order" border='0'/>View</a></td>
						 </tr>
					 <?php } ?>
					</tbody>
					<tfoot>
						  <tr>
							  <td colspan="8">Total Sales(<?=$srows?>)</td>
						  </tr>
						  <tr>
							  <td colspan="8">Total Value Of Sales &pound;<?php echo number_format($total_sales,2,'.','');?></td>
						  </tr>
					</tfoot>
					</table> 
				<?php	}
			   

		}else if($type=='refunds'){
			   $sql	= "SELECT oa.*,o.customer_id,o.order_id,o.invoice_number,concat(c.fname, ' ',c.lname)as Name FROM 3bit_order_actions oa,3bit_orders o,3bit_customers c WHERE oa.order_id=o.order_id AND o.customer_id=c.customer_id AND (oa.action='Refunded') AND (o.status !='Archived' AND o.status !='Rejected' AND o.status != 'Shipped') ";

			   if($duration=='all')
					$sql	.=	" AND date(oa.action_date) <='".$sear_date."' AND date(oa.action_date) >='".$prev_date."' ";
			   else if($duration=='today')
				   $sql	.=	" AND  DATE_FORMAT(oa.action_date,'%Y-%m-%d')= '".date("Y-m-d")."'";
				else if($duration=='week')
				   $sql	.=	" AND  YEARWEEK(oa.action_date)= YEARWEEK(current_date)";
				else if($duration=='month')
				   $sql	.=	" AND  MONTH(oa.action_date)= MONTH(current_date) AND YEAR(oa.action_date)= YEAR(current_date)";	

				$sql	.=	" ORDER BY oa.action_date DESC";

				$result	 =	$this->_db->fetchAll($sql);

				$rrows	=	count($result);
		?>

				<a style="cursor:pointer" onclick="filter_dash('all','refunds')" >All</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('today','refunds')" >Today</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('week','refunds')" >Week</a>&nbsp;/&nbsp;<a style="cursor:pointer" onclick="filter_dash('month','refunds')" >Month</a>
		<?php

				if($rrows>0){

					$total_refunds	=	0;
			  ?>
					

					<table width='100%' cellpadding="0" cellspacing="0" border="0" class="display">
					<thead>
					<tr>				
						<th height="20" class="helpHed">Order No</th>					
						<th>Customer Name</th>
						<th>OrderDate</th>
						<th>Status</th>
						<th>OrderTotal</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($result as $row){ 
							if($row['status']=='Deposit')
								$astat	=	'Deposited';
							else if($row['status']=='Deposit Balance Pay')
								$astat	=	'Accepted';
							else
								$astat	=	$row['action'];
							$total_refunds += $row['amount_paid']; 
					?>
						 <tr>
							 <td><?=$row['order_id']?>/<?=$row['invoice_number']?></td>
							 <td><?=$row['Name']?></td>
							 <td><?=$row['action_date']?></td>
							 <td><?=$row['action']?></td>
							 <td>&pound;<?=$row['amount_paid']?></td>
							 <td><a href="<?=$this->_baseurl?>/orders/view/id/<?=$row['order_id']?>" class='button'><img src="<?=$this->_baseurl?>/public/images/icons/page_white_text.png" alt="View Order" title="View Order" border='0'/>View</a></td>
						 </tr>
					 <?php } ?>
					</tbody>
					<tfoot>
						  <tr>
							  <td colspan="8">Total Refunds(<?=$rrows?>)</td>
						  </tr>
						  <tr>
							  <td colspan="8">Total Value Of Refunds &pound;<?php echo number_format($total_refunds,2,'.','');?></td>
						  </tr>
					</tfoot>
					</table> 
				<?php	}


		}
		die();
	}


	function dateupdateAction(){

			if($_POST['dtype'] !=''){
				$dtype	=	$_POST['dtype'];
				switch($dtype){

					case "Today":  
						$sdate		=	date("d/m/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
						$edate		=	date("d/m/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
						$retdate	=	$sdate."^".$edate;
					break;
					case "Yesterday":
						$sdate		=	date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));
						$edate		=	date("d/m/Y", mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));
						$retdate	=	$sdate."^".$edate;
					break;
					case "This Month":
						$sdate		=	date("d/m/Y", mktime(0, 0, 0, date("m"), 1, date("Y")));
						$edate		=	date("d/m/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
						$retdate	=	$sdate."^".$edate;
					break;
					case "This Year":
						$sdate		=	date("d/m/Y", mktime(0, 0, 0, 1, 1, date("Y")));
						$edate		=	date("d/m/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
						$retdate	=	$sdate."^".$edate;
					break;
				}

				die($retdate);
			}
		}	

	/*************************************** Listing function ends *******************************/

	/*************************************** Formulary related function **************************/

	//To find version of the formulary ============================================================

	function findFormversion($formulary_id){
		$new_version		=	0;
		$sublib_qry		 =  $this->_db->fetchAll("SELECT max(version) as max_version,count(block_id) as cnt from `3bit_tmp_formulary_blocks` where `formulary_id`='".$formulary_id."'");
		if(count($sublib_qry)>0){
			$count	=	$sublib_qry[0]['cnt'];
			if($count>0){
				$new_version =	$sublib_qry[0]['max_version'];	
			}
		}
		return $new_version;
	}
	
	// Function to insert pubmed id inside formulary ==============================================

	public function insertpmidAction()
	{
	   if(!empty($_POST['pmid'])){

		  $selPum	=	$this->_db->fetchAll("SELECT * FROM endnote_refs WHERE id = '".$_POST['pmid']."' ");

		  if(count($selPum)>0){

			   foreach($selPum as $aRow){	
					//echo $aRow['pages'];
					$auth		=	explode("\r",$aRow['author']);						
					$authcount	=	count($auth);
					if($authcount>0){
						if($authcount>2){			
							$dispauth	=	$auth[0]." <i>et al</i>";
						}else if($authcount==2){
							$dispauth	=	implode(",",$auth);
							$dispauth	=	substr($dispauth,0,-1);
							$dispauth	=	$dispauth;
						}else{
						   $dispauth	=	$auth[0];
						}
					}else{
						$dispauth	=	$aRow['author'];
					}			
					
					if($content != ''){
						$returnTxt	= $content;
					}else{
						if($aRow['date']!='')
							$datetext	=	$aRow['year']."  ".$aRow['date'];
						else
							$datetext	=	$aRow['year'];

						if(substr(stripslashes($aRow['title']),-1, 1)!='.')
							$reftit		=	stripslashes($aRow['title']).".";
						else
							$reftit		=	stripslashes($aRow['title']);

						if($aRow['pages']!=''){
							$returnTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".$reftit."</i>";

							if($dispedit!=''){	
								$returnTxt		.=	"&nbsp;".$dispedit;
							}
							if($aRow['secondary_title']!=''){	
								$returnTxt		.=	"&nbsp;<i>".stripslashes($aRow['secondary_title'])."</i>";
							}
							if($aRow['publisher']!=''){
								 $returnTxt		.=	",&nbsp;".$aRow['publisher'];
							}
							if($aRow['place_published']!=''){

								$returnTxt		.= ",".$aRow['place_published'];
							}
							if($aRow['volume']!='' && $aRow['pages']!=''){
							   $returnTxt		.= "&nbsp;<b>".$aRow['volume']."</b>:&nbsp;".str_replace("�","&ndash;",$aRow['pages']);
							}else if($aRow['volume']=='' && $aRow['pages']!=''){
							   $returnTxt		.= "&nbsp;pp.&nbsp;".$aRow['pages'];
							}			

						}else{
							$returnTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;".$reftit;
							if($dispedit!=''){	
								$returnTxt		.=	"&nbsp;".$dispedit;
							}
							if($aRow['secondary_title']!=''){	
								$returnTxt		.=	"&nbsp;<i>".stripslashes($aRow['secondary_title'])."</i>";
							}
							if($aRow['edition']!=''){   
							   $returnTxt		.=	"&nbsp;(".$aRow['edition']."e).";
							}
							if($aRow['publisher']!=''){
								 $returnTxt		.=	"&nbsp;".$aRow['publisher'];
							}
							if($aRow['place_published']!=''){

								$returnTxt		.= ",".$aRow['place_published'];
							}
							
						}

						$returnTxt		.= ".";

						if($aRow['accession_number']!='' && is_numeric($aRow['accession_number'])){
							$returnTxt		.= " <a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Citation&list_uids=".$aRow['accession_number']."'>PubMed: ".$aRow['accession_number']."</a>.";
						}
					}
					
					$ret_text	=	'<li><a href="javascript:;" onclick=mySimpleHtmlInserter("{'.$aRow['id'].'}");>'.$returnTxt.'&nbsp;<span>{'.$aRow['id'].'}</span></a></li>'."^"."S";
				
				} 

				die($ret_text);
			}
		}
	}

	// Function to update the reference in the side panel =========================================

	public function updatereferenceAction()
	{
		global $matchCount,	$uniquearr,$lastuniquearr;
		$lat_version	=	$this->_db->fetchOne("SELECT version FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$_POST['formulary']."' ORDER BY version DESC LIMIT 0,1");		

		$block_array	=	array('pre_text_article','class','indication','pharmacology','cautions','drug_interactions','undesirable_effects','dose_and_use','overdose','death_rattle','supply','post_text_article');
		$temp			=	0;

		$returnTxt		=	'';

		$modelform		=	new Application_Model_Formulary();

		 for($m=0;$m<count($block_array);$m++){

			$selBlocks	=	$this->_db->fetchAll("SELECT fb.*,f.formulary_title,f.bnf_reference,f.language_id FROM 3bit_tmp_formulary_blocks fb,3bit_formulary f WHERE f.formulary_id ='".$_POST['formulary']."' AND fb.version='".$lat_version."' AND f.formulary_id=fb.formulary_id AND fb.block_type= '".$block_array[$m]."' GROUP BY fb.version");

				if(count($selBlocks)>0){
					$i=0;

					foreach($selBlocks as $brow){
						$block_id	=	$brow['block_id'];

						if($temp ==0){
							$returnTxt.=	'<div id="monographtitle">
												<h1>'.html_entity_decode(stripslashes($brow['formulary_title'])).'<span>'.$brow['bnf_reference'].'</span></h1>
											</div>';					 				  
						}
						$temp			=		$i+1;

						$block_type		=	stripslashes($brow['block_type']);
						if($block_type=='generic_supply')
							  $block_type	=	'supply';			  

						  if($brow['block_content1'] != ''){

							  if($brow['block_type'] !='indication')
								  $divid	=	$modelform->remExtra($brow['block_type']);
							  else
								  $divid	=	'indications';

							$returnTxt	.=	'<div id="'.$divid.'">';
							if($block_type != 'post_text_article'){
								$returnTxt	.=	'<H2>'.($brow['block_title']).' </H2>';							
							}

							if($brow['block_content1'] != ''){
								$decoded	=	html_entity_decode(stripslashes($brow['block_content1']));
								$returnTxt	.=	$decoded;
							}
							if($block_type == 'pharmacology'){
								$bio_availability	=	html_entity_decode(stripslashes($brow['bio_availability']));
								$onset_of_action	=	html_entity_decode(stripslashes($brow['onset_of_action']));
								$time_to_peak		=   html_entity_decode(stripslashes($brow['time_to_peak']));
								$halflife			=	html_entity_decode(stripslashes($brow['plasma_halflife']));
								$duration_of_action	=	html_entity_decode(stripslashes($brow['duration_of_action']));			

								if($bio_availability !=''){
									$returnTxt	.=	$bio_availability;
								}if($onset_of_action !=''){								
									$returnTxt	.=	$onset_of_action;
								}if($time_to_peak !=''){								
									$returnTxt	.=	$time_to_peak;
								}if($halflife !=''){
									$returnTxt	.=	$halflife;
								}if($duration_of_action !=''){								
									$returnTxt	.=	$duration_of_action;
								}
							}else if($block_type == 'indication'){
							   $contra_indications	= html_entity_decode(stripslashes($brow['block_content2']));	
							   if($contra_indications !=''){								
									$returnTxt	.=	$contra_indications;
								}
							}
							$returnTxt	.=	'</div>';
						}
					 $i++;
				}
			}
		}

		if($returnTxt ==''){
			$returnTxt	=	'No sections are added!';	
		}else{
			$returnTxt	=	$this->getParsed($returnTxt);
		}

		if(is_array($lastuniquearr)){	

			$lastuniquearr	=	array_values(array_unique($lastuniquearr)); 

			$pubid	=	implode(",",$lastuniquearr);

			$pubid	=	preg_replace(array('/\{/','/\}/'),array('',''),$pubid);
				
			$parr	=	explode(",",$pubid);

			$cnt	=	count($parr);

			if($cnt>0){

				for($m=0;$m<$cnt;$m++){			
					$selLat	=	$this->_db->fetchAll("SELECT * FROM endnote_refs WHERE id='".$parr[$m]."' ORDER BY id ");

					if(count($selLat)>0){

						foreach($selLat as $row){

							$dispauth	=	'';
					
							$auth		=	array();
							
							$dispedit	=	'';

							$editors	=	array();

							$auth		=	explode("\r",$row['author']);						

							$authcount	=	count($auth);

							if($authcount>0){
								if($authcount>2){			
									$dispauth	=	$auth[0]." <i>et al</i>.";
								}else if($authcount==2){
									$dispauth	=	implode(",",$auth);
									$dispauth	=	substr($dispauth,0,-1);
									$dispauth	=	$dispauth;
								}else{
								   $dispauth	=	$auth[0];
								}
							}else{
								$dispauth	=	$row['author'];
							}			

							if($editcount>0){
								if($editcount>2){
									$dispedit	= "In : ".$editors[0]." <i>et al.</i> (eds)&nbsp;";	
								}else if($editcount==2){
									$dispedit	= "In : ".$editors[0].",".$editors[1]." (eds) &nbsp;";
								}else{
									$dispedit	= "In : ".$editors[0]." (eds) &nbsp;";
								}
							}
						
							if($content !=''){
								$referTxt	.= "<li><a href='javascript:;' onclick=mySimpleHtmlInserter('{".$row['id']."}');>".$content."&nbsp;{".$row['id']."}</a></li>";
							}else{
								if($row['date']!='')
									$datetext	=	$row['year']."  ".$row['date'];
								else
									$datetext	=	$row['year'];

								if(substr(stripslashes($row['title']),-1, 1)!='.')
									$reftit		=	stripslashes($row['title']).".";
								else
									$reftit		=	stripslashes($row['title']);

								if($row['pages']!=''){
									$refTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".$reftit."</i>";

									if($dispedit!=''){	
										$refTxt		.=	"&nbsp;".$dispedit;
									}
									if($row['secondary_title']!=''){	
										$refTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
									}
									if($row['publisher']!=''){
										 $refTxt		.=	",&nbsp;".$row['publisher'];
									}
									if($row['place_published']!=''){
										$refTxt		.= ",".$row['place_published'];
									}
									if(trim($row['volume'])!='' && trim($row['pages'])!=''){
									   $refTxt		.= "&nbsp;<b>".$row['volume']."</b>:&nbsp;".str_replace("�","&ndash;",$row['pages']);
									}else if(trim($row['volume'])=='' && trim($row['pages'])!=''){
									   $refTxt		.= "&nbsp;pp.&nbsp;".$row['pages'];
									}							

								}else{
									$refTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".$reftit."</i>";
									if($dispedit!=''){	
										$refTxt		.=	"&nbsp;".$dispedit;
									}
									if($row['secondary_title']!=''){	
										$refTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
									}
									if($row['edition']!=''){   
									   $refTxt		.=	"&nbsp;(".$row['edition']."e).";
									}
									if($row['publisher']!=''){
										 $refTxt		.=	"&nbsp;".$row['publisher'];
									}
									if($row['place_published']!=''){

										$refTxt		.= ",".$row['place_published'];
									}								
								}

								$refTxt		.= ".";

								if($row['accession_number']!='' && is_numeric($row['accession_number'])){
									$refTxt		.= " <a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Citation&list_uids=".$row['accession_number']."'>PubMed: ".$row['accession_number']."</a>.";
								}

								$referTxt	.= "<li><a href='javascript:;' onclick=mySimpleHtmlInserter('{".$row['id']."}');>".$refTxt."&nbsp;<span>{".$row['id']."}</span></a></li>";
							}

						}	
					}
				}		 
			}
		  }
		  if($referTxt !='')
			  die($referTxt."^"."S");
		  else
			  die($referTxt."^"."S");

	}

	// Function to refresh the side panel in formulary section ====================================

	public function refreshdialogAction()
	{
		global $matchCount,	$uniquearr,$lastuniquearr;
		foreach($_POST as $key => $value){
		  $alltext	.=	$value;
		}
		
		$returnTxt	=	$this->getParsed($alltext);

		if(is_array($lastuniquearr)){	

			$lastuniquearr	=	array_values(array_unique($lastuniquearr)); 

			$pubid	=	implode(",",$lastuniquearr);

			$pubid	=	preg_replace(array('/\{/','/\}/'),array('',''),$pubid);
				
			$parr	=	explode(",",$pubid);

			$cnt	=	count($parr);

			if($cnt>0){

				for($m=0;$m<$cnt;$m++){			
					$selLat	=	$this->_db->fetchAll("SELECT * FROM endnote_refs WHERE id='".$parr[$m]."' ORDER BY id ");

					if(count($selLat)>0){

						foreach($selLat as $row){

							$dispauth	=	'';
					
							$auth		=	array();
							
							$dispedit	=	'';

							$editors	=	array();

							$auth		=	explode("\r",$row['author']);						

							$authcount	=	count($auth);

							if($authcount>0){
								if($authcount>2){			
									$dispauth	=	$auth[0]." et al";
								}else if($authcount==2){
									$dispauth	=	implode(",",$auth);
									$dispauth	=	substr($dispauth,0,-1);
									$dispauth	=	$dispauth;
								}else{
								   $dispauth	=	$auth[0];
								}
							}else{
								$dispauth	=	$row['author'];
							}			

							if($editcount>0){
								if($editcount>2){
									$dispedit	= "In : ".$editors[0]." <i>et al</i>. (eds)&nbsp;";	
								}else if($editcount==2){
									$dispedit	= "In : ".$editors[0].",".$editors[1]." (eds) &nbsp;";
								}else{
									$dispedit	= "In : ".$editors[0]." (eds) &nbsp;";
								}
							}
						
							if($content !=''){
								$referTxt	.= "<li><a href='javascript:;' onclick=mySimpleHtmlInserter('{".$row['id']."}');>".$content."&nbsp;{".$row['id']."}</a></li>";
							}else{
								if($row['date']!='')
									$datetext	=	$row['year']."  ".$row['date'];
								else
									$datetext	=	$row['year'];

								if(substr(stripslashes($row['title']),-1, 1)!='.')
									$reftit		=	stripslashes($row['title']).".";
								else
									$reftit		=	stripslashes($row['title']);

								if($row['pages']!=''){
									$refTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".stripslashes($reftit)."</i>";

									if($dispedit!=''){	
										$refTxt		.=	"&nbsp;".$dispedit;
									}
									if($row['secondary_title']!=''){	
										$refTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
									}
									if($row['publisher']!=''){
										 $refTxt		.=	",&nbsp;".$row['publisher'];
									}
									if($row['place_published']!=''){
										$refTxt		.= ",".$row['place_published'];
									}
									if(trim($row['volume'])!='' && trim($row['pages'])!=''){
									   $refTxt		.= "&nbsp;<b>".$row['volume']."</b>:&nbsp;".str_replace("�","&ndash;",$row['pages']);
									}else if(trim($row['volume'])=='' && trim($row['pages'])!=''){
									   $refTxt		.= "&nbsp;pp.&nbsp;".$row['pages'];
									}							

								}else{
									$refTxt		=	$dispauth."&nbsp;(".$datetext.")&nbsp;".stripslashes($reftit);
									if($dispedit!=''){	
										$refTxt		.=	"&nbsp;".$dispedit;
									}
									if($row['secondary_title']!=''){	
										$refTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
									}
									if($row['edition']!=''){   
									   $refTxt		.=	"&nbsp;(".$row['edition']."e).";
									}
									if($row['publisher']!=''){
										 $refTxt		.=	"&nbsp;".$row['publisher'];
									}
									if($row['place_published']!=''){

										$refTxt		.= ",".$row['place_published'];
									}								
								}

								$refTxt		.= ".";

								if($row['accession_number']!='' && is_numeric($row['accession_number'])){
									$refTxt		.= " <a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Citation&list_uids=".$row['accession_number']."'>PubMed: ".$row['accession_number']."</a>.";
								}
								
								
								$referTxt	.= "<li><a href='javascript:;' onclick=mySimpleHtmlInserter('{".$row['id']."}');>".$refTxt."&nbsp;<span>{".$row['id']."}</span></a></li>";
							}

						}	
					}
				}		 
			}
		  }
		  if($referTxt !='')
			  die($referTxt."^"."S");
		  else
			  die($referTxt."^"."S");	
	}

	// Callback function for parsing ==============================================================

	function genFootNumbers($matches)
	{
		global $uniquearr;	
		$pos=(array_search($matches[0],$uniquearr))+1;
		return "<sup id='up".$pos."'><a href='#".$pos."'>$pos</a></sup>";	
	}


	//Function to get parsed contents =============================================================

	function getParsed($data,$preview='0')
	{
		global $matchCount,	$uniquearr,$lastuniquearr;
		$data		=	$this->parsePage($data,$preview);
		$matchCount	=	preg_match_all('/(\{.*?\})/',$data,$matches);
		$uniquearr	=	array_values(array_unique($matches[0]));
		if($matchCount>0){		
			$replacement1= preg_replace_callback('/(\{.*?\})/',array($this,'genFootNumbers'),$data);
			$replacement1=preg_replace('/<\/sup><sup(.*?)>/','</sup><sup$1>,',$replacement1);
		}
		if($matchCount<=0){
			$replacement1	=	$data;
		}else{
			for($k=0;$k<count($uniquearr);$k++)
				$lastuniquearr[]	=	$uniquearr[$k];
		}  
		$parsedcontent	=	$replacement1; 
		return $parsedcontent;
	} 

	//Function to parse the snippets in the content ===============================================

	function parsePage($content,$preview)
	{	
		$matchCount	=	preg_match_all('/\[(\[.*?\])\]/', $content, $matches);
		
		if($matchCount==0)
			return $content; 
		
		foreach($matches[0] as $value){ 		
			$value1=preg_replace("/\[\[/","/\[\[",$value);
			$value1 =preg_replace("/\]\]/","\]\]/",$value1);
			if($preview=='1')
				$snippets[$value1] = '<span class="snippet" id="'.$value.'"></span>'.$this->getTables($value);
			else
				$snippets[$value1] = $this->getTables($value);
		}
		$parsedContent	=	preg_replace(array_keys($snippets),array_values($snippets),$content);
		
		return $parsedContent;
	}

	// Get Snippet text by snippet name ===========================================================

	function getTables($name)
	{
		$name =str_replace("[[","",$name);
		$name =str_replace("]]","",$name);

		$idplusversion	=	explode("#",str_replace("snippet-id","",$name));	

		$snippetid		=	$idplusversion[0];

		$version		=	$idplusversion[1];

		$sql			=	"SELECT long_text,snippet_title FROM 3bit_formulary_snippets WHERE snippet_id='".$snippetid."' AND version= '".$version."' ";	

		$snippets		=	$this->_db->fetchAll($sql);

		$numsnippets	=	count($snippets);
		
		if($numsnippets<>0){
			foreach($snippets as $resR){
				$long_text	=	stripslashes($resR['long_text']);
				/*$content = preg_replace('/<p[^>]*>/', '', $long_text); // Remove the start <p> or <p attr="">
				$content = preg_replace('/<\/p>/', '<br />', $content); // Replace the end*/
				return (stripslashes($long_text));
			}
		}else{ 
			$sql			=	"SELECT long_text,snippet_title FROM 3bit_formulary_snippet_versions WHERE snippet_id='".$snippetid."' AND version= '".$version."' ";	

			$snippets		=	$this->_db->fetchAll($sql);

			$numsnippets	=	count($snippets);
			
			if($numsnippets<>0){
				foreach($snippets as $resR){
					$long_text	=	stripslashes($resR['long_text']);
					return (stripslashes($long_text));
				}
			}
		}
	}
	
	// Function to update drug ====================================================================

	public function drugupdateAction()
	{
		$cost_quoted	=	$_POST['dcost'];
		$noof_dose		=	$_POST['ddose'];
		$costper_dose	=	number_format(($cost_quoted/$noof_dose),2,'.','');
		$this->_db->query("UPDATE 3bit_drugs SET cost_quoted='".$cost_quoted."',nof_doses='".$noof_dose."',cost_per_udose='".$costper_dose."' WHERE drug_id='".$_POST['drugid']."'");
		die();
	}

	// Function to update day in drugs ============================================================

	public function dayupdateAction()
	{
		$cost_per_udose	=	$_POST['vpdose']; 	
		$sql	=	$this->_db->fetchAll("SELECT cost_quoted,nof_doses FROM 3bit_drugs WHERE drug_id='".$_POST['drugid']."'");
		if(count($sql)>0){
			 $cost_quoted   = $sql[0]['cost_quoted'];
			 $nof_doses		= $sql[0]['nof_doses'];
			 $nof_dose_pday	= ($cost_quoted/$nof_doses);
		}
		$costper_twen	=	number_format(($cost_per_udose*$nof_dose_pday*28),2,'.','');
		$this->_db->query("UPDATE 3bit_drugs SET nof_dose_pday='".$cost_per_udose."',cost_28days='".$costper_twen."' WHERE drug_id='".$_POST['drugid']."'");
		die();
	}


	/*************************************** Formulary related function ends *********************/

	/*************************************** Pages related function ******************************/
	
	// Template list for pages module =============================================================

	public function seltemplateAction()
	{
		$templist	=	'<fieldset>
							<legend>Template List</legend>
							<span id="msg" style="color:red"></span>
							<form name="ttemplatelist" method="POST">							
								<div id="templist">
								<table cellpadding="2" cellspacing="0" border="0" width="100%" class="display">';

		$sql_list	=	$this->_db->fetchAll("SELECT t.*,a.asset_name,a.asset_category_id FROM 3bit_article_template t LEFT JOIN 3bit_assets a ON t.temp_thumbs=a.asset_id ORDER BY t.created_date");
				if($sql_list){
					$i=0;
					foreach($sql_list as $lbrow){
						if($i%3==0)
							$templist	.= "<tr>";
						$i++;				
						$tempid		= 	$lbrow['temp_id'];
						$temp_name	=	$lbrow['temp_filename'];
						$assname	=	$lbrow['asset_name'];				
						$status	=	$lbrow['temp_status'];				
						$asset_cat	=	$lbrow['asset_category_id'];	
						$img_tag	=	'<img src="/assets/thumbs/'.$asset_cat.'/'.$assname.'" border="0" alt="Assign">';
						if($status==1){
							$templist	.= "<td><a id='".$tempid."' title='Click to assign' alt='Click to assign' style='cursor:pointer'	onclick='assignTemplate(".$tempid.",\"".$temp_name."\")'>".$img_tag."</a></td>";
						}else{
   							$templist	.= "<td><a id='".$tempid."' title='Inactive Template' alt='Inactive Template'>".$img_tag."</a></td>";
						}
						 if($i%3==0)
							$templist	.= "<tr>";
					}
				}			
			$templist	.= '</table>			
			</div>
		</form>
		</fieldset>';	
		
		die($templist);
	}

	// Article list for pages module ==============================================================

	public function selarticleAction()
	{
		$artlist	=	'<fieldset>
							<legend>Articles List</legend>
							<span id="msg" style="color:red"></span>
							<form name="ttemplatelist" method="POST">						
								<div id="templist">
									<table cellpadding="2" cellspacing="0" border="0" width="100%" class="display">
									<tr>
									   <th>Article Title</th>
									   <th>Date Created</th>
									   <th>Date Modified</th>
									   <th>Status</th>
									   <th>Action</th>
									</tr>';
		$sql_list	 =	$this->_db->fetchAll("SELECT *,DATE_FORMAT(created_date,'%d/%m/%Y-%k%p')as cdate,DATE_FORMAT(modified_date,'%d/%m/%Y-%k%p')as mdate FROM 3bit_articles WHERE 1 ORDER BY created_date");
				if($sql_list){
					foreach($sql_list as $lbrow){
						$article_id		= 	$lbrow['article_id'];
						$article_name	=	$lbrow['article_title'];							
						$status			=	$lbrow['status'];
						if($status=='0')
							$active		=	'<font color="red">Inactive</font>';
						else if($status=='1')
							$active		=	'<font color="green">Active</font>';
						$artlist		.=	"<tr>
											  <td>".$lbrow['article_title']."</td>
											  <td>".$lbrow['cdate']."</td>
											  <td>".$lbrow['mdate']."</td>
											  <td>".$active."</td>";
						if($status==1)
							$artlist	.=	"<td><a id='".$article_id."' title='Click to assign' alt='Click to assign' style='cursor:pointer'	onclick='assignArticle(".$article_id.",\"".$article_name."\")' class='button'>Insert</a></td></tr>";
						else
							$artlist	.=	"<td>&nbsp;</td></tr>";															
					}
				}
			$artlist	.=	'</table>			
							</div>
						</form>
					<legend>';
			die($artlist);
	}

	// Function to check URL in pages module ======================================================

	public function checkurlAction()
	{
		$page_url	=	$_POST['page_url'];

		$path_info	=	parse_url($page_url);
		
		$path	=	$path_info['path'];
		$exp	=	explode("/",ltrim($path,"/"));			
		$pcount	=	count($exp);
		if($pcount=='2'){
			$folder	=	$exp[0];
			$file	=	$exp[1];
		}else if($pcount=='1'){
			$file	=	$exp[0];
		}
		$pos	=	strpos($file,'.php');
		if($pos === false){
			  if($file=='')
				  $file	=	'index.php';
			  else
				  $file	=	$file.".php";
		} 
		if(!empty($path_info['scheme']) || !empty($path_info['host']) ){
			die('External URL');
		}else{
			$selPage	=	$this->_db->fetchAll("SELECT cat_id FROM  3bit_categories WHERE cat_url ='".trim($page_url)."' OR cat_file ='".trim($page_url)."' ");		
			if($selPage){
				if($pcount=='2'){
					$outstring =	"Warning: ".$file." inside /cricket /".$folder."/ already exists.";
				}else if($pcount=='1'){
					$outstring	=	"Warning: ".$file." inside root folter already exists.";
				}
				die($outstring);
			}else{					
				if($pcount=='2'){
					$outstring	=	"creating a new file <strong>".$file."</strong> inside /".$folder."/ folder.";
				}else if($pcount=='1'){
					$outstring	=	"creating new file <strong>".$file."</strong> inside root folter.";
				}
				
				die("$outstring");
				
			}
		}
	}

	// Function to order the pages ================================================================
	
	public function pageorderAction()
	{

		if(count($_POST)>0 && $_POST['action']<>""){	

			$allarr		=	$_POST['cat_list'];

			$arrcount	=	count($_POST['cat_list']);

			for($i=0;$i<$arrcount;$i++){

				$cat_id	=	$allarr[$i]['id'];				
				
				$this->_db->query("UPDATE 3bit_categories SET cat_order = '".($i+1)."' WHERE cat_id ='".$cat_id."'");

				$childarr	=	$allarr[$i]['children'];

				$childcnt	=	count($childarr);

				for($k=0;$k<$childcnt;$k++){

					$scat_id		=	$childarr[$k]['id'];			

					$chkProd	=	$this->_db->fetchAll("SELECT cat_id FROM 3bit_categories WHERE cat_id ='".$scat_id."' AND cat_parent_id='".$cat_id."'");

					if($chkProd){	
						$this->_db->query("UPDATE 3bit_categories SET cat_order = '".($k+1)."' WHERE cat_id ='".$scat_id."' AND cat_parent_id='".$cat_id."'");
					}else{ 
						$this->_db->query("UPDATE 3bit_categories SET cat_order = '".($k+1)."' ,cat_parent_id='".$cat_id."' WHERE cat_id ='".$scat_id."'");					
					}
				} 
			}
			die("S");
		}else{
			die("F");
		}
	}

	public function	populateparentAction()
	{

		die($this->populate_parent('0','',$_REQUEST['parent_id'],$_POST['form_level'],$_REQUEST['id']));

	}

	public function populate_parent($passdir,$i,$selected,$form_level,$language_id)
	{
		$i.="---";

		$sql	=	"SELECT formulary_id,formulary_parent_id,formulary_title from `3bit_formulary` where formulary_parent_id='$passdir' AND language_id='".$language_id."'";
		if(!empty($form_level)){
			$sql	.=	" AND formulary_level='".$form_level."' ";
		}		
		$sublib_qry  = $this->_db->fetchAll($sql);		
		$numrows	 = count($sublib_qry);

		$second = '';
			
		if ($numrows > 0){ 		
			foreach($sublib_qry as $sublib_values)	{				
				$sublib_id					= $sublib_values["formulary_id"];			
				$subparent_id				= $sublib_values["formulary_parent_id"];				
				$sublib_name				= $sublib_values["formulary_title"];	  				
				if ($selected == $sublib_id){								  						
					$sel ="selected";										  					
				}else{														  				
					$sel ="";												  					
				}		
				$second.= "<option value='$sublib_id' $sel>$i$sublib_name</option>";											
					
				$second.= $this->populate_parent($sublib_id,$i,$selected,'',$language_id);//recursive call to the function				
			}			
			return($second);
		}else{			   		
			return($second);
		}
		
	}

	public function livevatAction()
	{

		$this->_db->query("UPDATE 3bit_product_vats SET is_live =	'0'");

		$updatesql	=	$this->_db->query("UPDATE product_vats SET is_live =	'1' WHERE vat_id='".$_POST['vatid']."'");

		if($updatesql)
			die("S");
		else
			die("F");

	}

	public function updateeuAction()
	{
		if($_POST['process']=="add"){		
			$sQuery				= $this->_db->query("UPDATE country SET is_eu='1' WHERE id='".$_POST['id']."'");
			if($sQuery){
				$sInfo = "updated country table!";			
			}
		}else{
			$sQuery				= $this->_db->query("UPDATE country SET is_eu='0' WHERE id='".$_POST['id']."'");
			if($sQuery){			
				$sInfo = "updated country table!";	
			}
		}		
		die($sInfo);
	}

	public function updategcAction()
	{
		if($_POST['process']=="add"){		
			$sQuery				= $this->_db->query("UPDATE country SET is_gc='1' WHERE id='".$_POST['id']."'");
			if($sQuery){
				$sInfo = "updated country table!";			
			}
		}else{
			$sQuery				= $this->_db->query("UPDATE country SET is_gc='0' WHERE id='".$_POST['id']."'");
			if($sQuery){			
				$sInfo = "updated country table!";	
			}
		}		
		die($sInfo);
	}

	/*************************************** Pages related function ends**************************/

    /*************************************** Common functions ************************************/
	
	// Function to delete the audits activity in dashboard ========================================

	public function deleteactivityAction()
	{	
		if(!empty($_POST['aid'])){
			$sQuery				= $this->_db->query("DELETE FROM 3bit_audit_history WHERE audit_id='".$_POST['aid']."'");
			if($sQuery){			
				$sInfo = "updated audit table!";	
			}
		}		
		die($sInfo);
	}		

	// To remove special chars in string ==========================================================

	function seems_utf8($str) {
		$length = strlen($str);
		for ($i=0; $i < $length; $i++) {
			$c = ord($str[$i]);
			if ($c < 0x80) $n = 0; # 0bbbbbbb
			elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
			elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
			elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
			elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
			elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
			else return false; # Does not match any model
			for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
				if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
					return false;
			}
		}
		return true;
	}

	function utf8_uri_encode( $utf8_string, $length = 0 ) {
		$unicode = '';
		$values = array();
		$num_octets = 1;
		$unicode_length = 0;
		$string_length = strlen( $utf8_string );
		for ($i = 0; $i < $string_length; $i++ ) {

			$value = ord( $utf8_string[ $i ] );

			if ( $value < 128 ) {
				if ( $length && ( $unicode_length >= $length ) )
					break;
				$unicode .= chr($value);
				$unicode_length++;
			} else {
				if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

				$values[] = $value;

				if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
					break;
				if ( count( $values ) == $num_octets ) {
					if ($num_octets == 3) {
						$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
						$unicode_length += 9;
					} else {
						$unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
						$unicode_length += 6;
					}

					$values = array();
					$num_octets = 1;
				}
			}
		}
		return $unicode;
	}

	/************ Function to remove special char and replace with dash **************/

	public function remSpecial($title) {	
		$title = strip_tags($title);
		// Preserve escaped octets.
		$title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
		// Remove percent signs that are not part of an octet.
		$title = str_replace('%', '', $title);
		// Restore octets.
		$title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

		if ($this->seems_utf8($title)) {
			if (function_exists('mb_strtolower')) {
				$title = mb_strtolower($title, 'UTF-8');
			}
			$title = $this->utf8_uri_encode($title, 200);
		}

		$title = strtolower($title);
		$title = preg_replace('/&.+?;/', '', $title); // kill entities
		$title = str_replace('.', '-', $title);
		$title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
		$title = preg_replace('/\s+/', '-', $title);
		$title = preg_replace('|-+|', '-', $title);
		$title = trim($title, '-');
		return $title;
	}	

	// For Group list ===========================================================================

	private function group_list(){ 

		$members			=	new Application_Model_Groups();
		$arrSubscribers		=	$members->get_all_groups();

		if(count($_REQUEST)>0){

			$oColumns = array('sg.date_created','u.email','u.FirstName','sg.no_of_subscription','sg.subscribers','sg.status');
			$sColumns =	array('sg.organisation','u.FirstName','u.LastName');
		
			$sIndexColumn = "group_id";

			 /* 
			* Paging
			*/
			$sLimit = "";
			if ( isset( $_REQUEST['iDisplayStart'] ) && $_REQUEST['iDisplayLength'] != '-1' ){
				$sLimit = "LIMIT ".addslashes( $_REQUEST['iDisplayStart'] ).", ".addslashes( $_REQUEST['iDisplayLength'] );
			}

			/*
			 * Ordering
			 */
			if ( isset( $_REQUEST['iSortCol_0'] ) ){
				$sOrder = "ORDER BY  ";
				for ( $i=0 ; $i<intval( $_REQUEST['iSortingCols'] ) ; $i++ ){
					$sOrder .= $oColumns[ intval( $_REQUEST['iSortCol_'.$i] ) ]." ".addslashes( $_REQUEST['sSortDir_'.$i] ) .", ";
				}
				$sOrder = substr_replace( $sOrder, "", -2 );
			}

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			$sWhere = "";
			if ( $_REQUEST['sSearch'] != "" ){				
				for($i=0; $i<count($sColumns); $i++){
					$sWhere .= $sColumns[$i]." LIKE '%".addslashes( $_REQUEST['sSearch'] )."%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				if(!empty($sWhere))
					$sWhere	=	' AND ( '.$sWhere.' )';
			} 

			if ( $_REQUEST['search_type'] != "A" ){	
				$sQuery		=	"SELECT sg.*, u.email, u.FirstName, u.LastName FROM 3bit_subscribers_groups sg, new_users u WHERE sg.user_id = u.id $sWhere $sOrder $sLimit ";
			}else{
				$sQuery		=	"SELECT sg.*, u.email, u.FirstName, u.LastName FROM 3bit_subscribers_groups sg, new_users u WHERE 1 $sWhere $sOrder $sLimit ";
			}

			$rResult		=	$this->_db->fetchAll($sQuery);

			/* Data set length after filtering */
			$sQuery			=	"SELECT FOUND_ROWS()";

			$iFilteredTotal = $this->_db->fetchOne($sQuery);

			/* Total data set length */
		
			if ( $_REQUEST['search_type'] != "A" ){				
				//$sWhere		.=	' AND type = "'.$_REQUEST['search_type'].'"';
				$sQuery		=	"SELECT COUNT(".$sIndexColumn.") FROM 3bit_subscribers_groups sg , new_users u WHERE sg.user_id = u.id $sWhere";
				$iTotal		=	count($this->_db->fetchAll($sQuery));	

			}else{

				$sQuery		=	"SELECT COUNT(".$sIndexColumn.") FROM 3bit_subscribers_groups sg , new_users u WHERE sg.user_id = u.id $sWhere";
				$iTotal		= $this->_db->fetchOne($sQuery);	
			}

			/*
			 * Output
			 */
			$temp	=	'';

			$cache	=	'';

			$sOutput = '{';

			$sOutput .= '"sEcho": '.intval($_REQUEST['sEcho']).', ';

			$sOutput .= '"iTotalRecords": '.$iFilteredTotal.', ';

			$sOutput .= '"iTotalDisplayRecords": '.$iTotal.', ';

			$sOutput .= '"aaData": [ ';

			foreach( $rResult as $key => $aRow){

				$usedSubscription	= $this->_db->fetchRow("SELECT COUNT(subscriber_id) cnt FROM 3bit_subscribers WHERE 1 GROUP BY group_id HAVING group_id = '".$aRow['group_id']."'");

				if($usedSubscription == '')
					$usedSubscription	= '0';


				$sOutput .= "[";			
				$sOutput .= '"'.str_replace('"', '\"', $aRow['organisation']).'",';
				$sOutput .= '"'.str_replace('"', '\"', $aRow['email']).'",';
				$sOutput .= '"'.str_replace('"', '\"', ($aRow['FirstName']." ".$aRow['LastName'])).'",';
				$sOutput .= '"'.str_replace('"', '\"', $usedSubscription['cnt']."/".$aRow['no_of_subscription']).'",';
				//$sOutput .= '"'.str_replace('"', '\"', $aRow['subscribers']).'",';
				

				if($aRow['status']=='1')
					$dispval			=	"<span id='".$aRow['id']."new_users'><a onclick=\"change_status('".$aRow['id']."','new_users','active','0','id');\" href=\"javascript:;\"><font color=\"green\">Active</font></a></span>";
				else
					$dispval			=	"<span id='".$aRow['id']."new_users'><a onclick=\"change_status('".$aRow['id']."','new_users','active','1','id');\" href=\"javascript:;\"><font color=\"red\">Inactive</font></a></span>";				

				$sOutput				.= '"'.str_replace('"', '\"', $dispval).'",';			

				$editurl				=	$this->_baseurl.'/groups/edit/id/'.$aRow['group_id'];
				
				$style					=	'';

				if($aRow['active']=='1')
					$style				=	"style='display:none'"; 

				$sOutput				.= '"<a class=\"button\"  href=\"'.$editurl.'\"><img border=\"0\" title=\"Edit\" src=\"'.addslashes($this->_baseurl).'/public/images/icons/page_white_text.png\"/>Edit <span class=\"button\"></span></a>';

				$sOutput				.= '",';

				$sOutput				= substr_replace( $sOutput, "", -1 );
				$sOutput				.= "],";

			}
			$sOutput = substr_replace( $sOutput, "", -1 );
			$sOutput .= '] }';
			die($sOutput);
		}
	}

	public function memberdetailsAction()
	{
		$uid 		=	$_POST['uid'];
		$uemail 	=	$_POST['email'];
		$utype		=	$_POST['rtype'];

		if($uemail =='' && $utype == 'id'){

			$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName, u.phone, u.email , o.order_id FROM new_users u, 3bit_orders o, 3bit_order_details od, 3bit_subscribers s WHERE u.id = '".$uid."' AND u.id = o.frontuser_id AND o.order_id = od.order_id AND od.product_id  IN (8,9,10,11,12,13,14,15) AND o.order_id = s.order_id ORDER BY o.	created_date DESC");	
		}else if( $uid == '' && $utype == 'email'){

			$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName, u.phone, u.email , o.order_id FROM new_users u, 3bit_orders o, 3bit_order_details od, 3bit_subscribers s WHERE u.email = '".$uemail."' AND u.id = o.frontuser_id AND o.order_id = od.order_id AND od.product_id  IN (8,9,10,11,12,13,14,15) AND o.order_id = s.order_id ORDER BY o.	created_date DESC");	
		}else if( $uid != '' && $uemail != ''){

			$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName, u.phone, u.email , o.order_id FROM new_users u, 3bit_orders o, 3bit_order_details od, 3bit_subscribers s WHERE u.id = '".$uid."' AND u.email = '".$uemail."' AND u.id = o.frontuser_id AND o.order_id = od.order_id AND od.product_id  IN (8,9,10,11,12,13,14,15) AND o.order_id = s.order_id ORDER BY o.	created_date DESC");	
		}
		
		if($sQuery){
			$userFullNmae	= $sQuery['FirstName']." ".$sQuery['LastName'];
			$user_id		= $sQuery['id'];
			$userTelephone	= $sQuery['phone'];
			$order_id		= $sQuery['order_id'];
			$email		= $sQuery['email'];
			$response		= $userFullNmae."||".$userTelephone."||".$order_id."||".$user_id."||".$email;
		}else{
			$response		= '';
		}
		die($response);
	}

	public function groupsubscriberdetailsAction()
	{
		$uid 		=	$_POST['uid'];
		$uemail 	=	$_POST['email'];
		$utype		=	$_POST['rtype'];

		if($uemail =='' && $utype == 'id'){

			$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName, u.email FROM new_users u WHERE u.id = '".$uid."' AND u.active ='1'");	

		}else if( $uid == '' && $utype == 'email'){

			$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName, u.email FROM new_users u WHERE u.email = '".$uemail."' AND u.active ='1'");	
		}else if( $uid != '' && $uemail != ''){

			$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName, u.email FROM new_users u WHERE u.id = '".$uid."' AND u.email = '".$uemail."' AND u.active ='1'");	
		}

		//$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName FROM new_users u WHERE email = '".$emailid."' AND u.active ='1' ");	
		//$sQuery			= $this->_db->fetchRow("SELECT u.id , u.FirstName, u.LastName, u.email FROM new_users u WHERE u.id = '".$id."' AND u.active ='1' ");	
		
		if($sQuery){
			$userFirstNmae	= $sQuery['FirstName'];
			$userLastNmae	= $sQuery['LastName'];
			$user_id		= $sQuery['id'];
			$email			= $sQuery['email'];

			$response		= $userFirstNmae."||".$userLastNmae."||".$user_id."||".$email;
		}else{
			$response		= '';
		}
		die($response);
	}

	// Action for the status update in all modules ==========================================================

	public function sendsubscribtiontriggerAction()
    {
		$usersID		= $_POST["userid"];
		$groupID		= $_POST["groupid"];
		$orderID		= $_POST["order_id"];
		$towhom			= $_POST["towhom"];
    
		$message_type	=	'Subscription Added';	

		$sql			= $this->_db->fetchRow("SELECT a.id, a.title, a.FirstName, a.LastName , a.email FROM new_users a WHERE a.id ='".$usersID."' ");

			$customer_fname		=	$sql['FirstName'];
			$customer_lname		=	$sql['LastName'];
			$customer_email		=	$sql['email'];
			$customer_order_id	=	$orderID;

			if($towhom=='Customer'){
				// Send Email To Customer ----------------------------------
				$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
				$res				= 	$this->_db->fetchAll($sql_select);

			}else if($towhom=='Admin'){
				// Send Email To Customer ----------------------------------				
				$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
				$res				= 	$this->_db->fetchAll($sql_select); 
			}

			//echo "<pre>";
			//print_r($res);
			//exit;

			if(count($res)>0){
				foreach($res as $row){
					$body			= 	stripslashes($row['message_text']);
					$text			= 	stripslashes($row['text']);
					$subject		= 	stripslashes($row['mail_subject']);

					if($body !=''){
						$bodytag		= 	str_replace("{fname}",$customer_fname, $body);
						$bodytag		= 	str_replace("{lname}",$customer_lname, $bodytag);
						$bodytag		= 	str_replace("{order_number}",$customer_order_id, $bodytag);
						$bodytag		=	nl2br($bodytag);
					}

					if($text !=''){
						$texttag		= 	str_replace("{fname}",$customer_fname, $text);
						$texttag		= 	str_replace("{lname}",$customer_lname, $texttag);
						$texttag		= 	str_replace("{order_number}",$customer_order_id, $texttag);
					}

					if($body !='' || $text !=''){

						$send_email	=	array('0'=>$customer_email);

						require_once $_SERVER['DOCUMENT_ROOT'].'/logincms/ext/Swift/lib/swift_required.php';
						//SEND MAIL USING SWIFT LIBRARY
						$message = Swift_Message::newInstance()
						->setCharset('iso-8859-2')
						//Give the message a subject
						->setSubject($subject)
						//Set the From address with an associative array
						->setFrom(array('hq@palliativedrugs.com' => 'Palliative Drugs'))
						//Set the To addresses with an associative array
						->setTo($send_email)
						//Give it a body
						->setBody($texttag)
						//And optionally an alternative body
						->addPart($bodytag, 'text/html');
						$transport		= Swift_MailTransport::newInstance();
						$mailer			= Swift_Mailer::newInstance($transport);
						$result			= $mailer->send($message);
					}
				}
			}
		
		die("success");
    }

	public function sendauthorisedtriggerAction()
    {
		$usersID		= $_POST["userid"];
	    
		$message_type	=	'Corporate Subscription';	
		$towhom			= $_POST["towhom"];

		$sql			= $this->_db->fetchRow("SELECT a.id, a.title, a.FirstName, a.LastName , a.email FROM new_users a WHERE a.id ='".$usersID."' ");

		$customer_fname		=	$sql['FirstName'];
		$customer_lname		=	$sql['LastName'];
		$customer_email		=	$sql['email'];

		if($towhom=='Customer'){
			// Send Email To Customer ----------------------------------
			$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
			$res				= 	$this->_db->fetchAll($sql_select);

		}else if($towhom=='Admin'){
			// Send Email To Customer ----------------------------------				
			$sql_select			= 	"SELECT * FROM 3bit_messages WHERE message_type='".$message_type."' AND message_to ='".$towhom."' AND active ='1'";
			$res				= 	$this->_db->fetchAll($sql_select); 
		}

		if(count($res)>0){
			foreach($res as $row){
				$body			= 	stripslashes($row['message_text']);
				$text			= 	stripslashes($row['text']);
				$subject		= 	stripslashes($row['mail_subject']);

				if($body !=''){
					$bodytag		=	nl2br($body);
				}

				if($text !=''){
					$texttag		= 	$text;
				}

				if($body !='' || $text !=''){

					$send_email	=	array('0'=>$customer_email);

					require_once $_SERVER['DOCUMENT_ROOT'].'/logincms/ext/Swift/lib/swift_required.php';
					//SEND MAIL USING SWIFT LIBRARY
					$message = Swift_Message::newInstance()
					->setCharset('iso-8859-2')
					//Give the message a subject
					->setSubject($subject)
					//Set the From address with an associative array
					->setFrom(array('hq@palliativedrugs.com' => 'Palliative Drugs'))
					//Set the To addresses with an associative array
					->setTo($send_email)
					//Give it a body
					->setBody($texttag)
					//And optionally an alternative body
					->addPart($bodytag, 'text/html');
					$transport		= Swift_MailTransport::newInstance();
					$mailer			= Swift_Mailer::newInstance($transport);
					$result			= $mailer->send($message);
				}
			}
		}
	
		die("success");
    }

/*	
	public function exportcontentsAction()
	{

		
		$sourcefile		= 'http://local.pd.com/logincms/formulary/preview/id/174/version/3.00';
		$file			= 'test.doc';
		$download		= true;

		$opts = array(
		  'http'=>array(
			'method'=>"GET",
			'header'=>"Accept-language: en\r\n" .
					  "Cookie: foo=bar\r\n"
		  )
		);

		$context		= stream_context_create($opts);

		$html			= file_get_contents($sourcefile, false, $context);

		$fp				= fopen($file, 'w') or die("can't open file");
		fwrite($fp, $html);
		fclose($fp);

		$response		=  'Success';

		die($response);
		//---------------------------------------------------------------------------------------------

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://local.pd.com/logincms/'); //login URL
		curl_setopt ($ch, CURLOPT_POST, 1);

		$postData='
		first_name=asmita
		&last_name=gagade
		&username=asmita.gagade
		&password=asmita3bit';

		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt ($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		$store = curl_exec ($ch);
		
		curl_setopt($ch, CURLOPT_URL, "http://local.pd.com/logincms/formulary/preview/id/174/version/3.00");
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_REFERER, "http://local.pd.com/logincms/");
		//curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		$output = curl_exec($ch);
		
		$file			= 'test.doc';

		$fp				= fopen($file, 'w') or die("can't open file");
		fwrite($fp, $store);
		fclose($fp);
		
		$response		=  'Success';

		die($response);
	}*/

	public function exportcontentsAction()
	{
		header("Content-type: application/vnd.ms-word");
		header("Content-Disposition: attachment;Filename=document_name.doc");
		header("Content-type: text/css");
		global $block_title;

		$block_title[1] =		array(
											 "pre_text_article"		=>	"pre_text_article",
											 "class"				=>	"Class:",
											 "indication"			=>	"Indication:",
											 "pharmacology"			=>	"Pharmacology",
											 "cautions"				=>	"Cautions",
											 "drug_interactions"	=>	"Drug Interactions",
											 "undesirable_effects"	=>	"Undesirable Effects",
											 "dose_and_use"			=>	"Dose And Use",
											 "overdose"				=>	"Overdose",
											 "death_rattle"			=>	"Death Rattle",
											 "supply"				=>	"Supply",
											 "post_text_article"	=>	"post_text_article",
											 "Bio Availability"		=>  "Bio Availability",
											 "Onset of Action"		=>  "Onset of Action",
											 "Time to peak plasma concentration"=>"Time to peak plasma concentration",
											 "Plasma Halflife"		=>  "Plasma Halflife",
											 "Duration of Action"	=>	"Duration of Action",
											 "Contra Indications"	=>  "Contra-indications:"

										);

			    $block_title[2] =		array(
											 "pre_text_article"		=>	"Vor Text lesen",
											 "class"				=>	"Stoffgruppe:",
											 "indication"			=>	"Indikationen:",
											 "pharmacology"			=>	"Pharmakologie",
											 "cautions"				=>	"Verwarnungen",
											 "drug_interactions"	=>	"Arzneimittelinteraktionen",
											 "undesirable_effects"	=>	"unerw�nschte Wirkungen",
											 "dose_and_use"			=>	"Dosierung und Anwendung",
											 "overdose"				=>	"�berdosierung",
											 "death_rattle"			=>	"R�cheln",
											 "supply"				=>	"beliefern",
											 "post_text_article"	=>	"nachgestellten Text lesen",
											 "Bio Availability"		=>  "Bio-Verf�gbarkeit",
											 "Onset of Action"		=>  "Wirkungseintritt",
											 "Time to peak plasma concentration"=>"Time to Peak-Plasmakonzentration",
											 "Plasma Halflife"		=>  "Plasma Halflife",
											 "Duration of Action"	=>	"Dauer der Aktion",
											 "Contra Indications"	=>  "Kontraindikationen:" );

		$block_array	=	array('pre_text_article','class','indication','pharmacology','cautions','drug_interactions','undesirable_effects','dose_and_use','overdose','death_rattle','supply','post_text_article');

		$temp			=	0;

		$selForm		=	$this->_db->fetchAll("SELECT f.publish_status,f.formulary_title,l.language_name,f.language_id FROM 3bit_formulary f,3bit_language l WHERE f.formulary_id='".$_POST['id']."' AND f.language_id=l.language_id ");

		if(count($selForm)>0){

		$publish_status	=	$selForm[0]['publish_status'];

		$formulary_tit	=	stripslashes($selForm[0]['formulary_title']);

		$language_name	=	strtolower($selForm[0]['language_name']);

		$language_id	=	strtolower($selForm[0]['language_id']);

	}
	$modelform			=	new Application_Model_Formulary();
	$output				=	'';

	$output				.=	'<html>
			<meta http-equiv=\'Content-Type\' content=\'text/html; charset=Windows-1252\'>
		 <head>
		 <title>Formulary Preview: <?php echo $formulary_tit;?></title>
		</head>';

	$output				= '<style>a:link, #preview .container a:visited {color: red;text-decoration: none;}
								.greybox { background-color: #D9D9D9; margin-bottom: 1em; margin-left: -0.5em; margin-top: 0.5em; padding-bottom: 0.5em; padding-top: 1em; padding-bottom: 10px; }
								p.greybox { margin-bottom: 1em; margin-top: 1px; margin-left: -0.5em; padding-left: 0.5em; padding-bottom: 0.5em; padding-top: 0.5em; }
								.tablechart{font-family:Liberation Sans,Helvetica Neue,Helvetica,Arial,sans-serif;border-left: 1px solid #000000; border-top: 1px solid #000000;border-collapse: collapse;margin-bottom: 1em;margin-top: 1em;width: 80%;}
								.container table th strong { margin-right: 1em; }
								.container table.innercontent tr { border-style: none; }
								.container .innercontent thead tr td { border-bottom-width: 2px; border-bottom-style: solid; }
								p.greybox { margin-bottom: 1em; margin-top: 1px; margin-left: -0.5em; padding-left: 0.5em; padding-bottom: 0.5em; padding-top: 0.5em; }
								table.innercontent tbody tr td h5 { margin-bottom: 0; margin-top: 1em; }
								table.innercontent { font-family:Liberation Sans,Helvetica Neue,Helvetica,Arial,sans-serif; width: 100%; margin-bottom: 0; width: 80%;}
								table.innercontent.nomargin ol { margin-bottom: 0; margin-left: 1.4em; }
								table.innercontent col#key  { width: 150px;}
								#appdenx3keys tbody td img { margin-left: 0; margin-top: 5px; margin-bottom: 5px; }
								img.appendix { margin-top: 1em; margin-bottom: 1em; }
								#appdenx3keys tbody td { vertical-align: middle; border-bottom: 1px solid #ebebeb; padding: 2px; }
								.innercontent tbody tr td.greybox { padding-bottom: 10px; }
								.innercontent td.vlmiddle { vertical-align: middle; padding-top: 0; padding-bottom: 0; }
								.innercontent tbody tr.nobottompadding td { padding-bottom: 0; }
								.innercontent tbody tr.nopadding td { padding-top: 0; padding-bottom: 0; }
								.innercontent tbody tr.notoppadding td { padding-top: 0; }
								#preview #formulary.container #pre-text-article ol {margin-left: 1.8em;}
								.container table.tablechart tfoot tr, .container table.box tfoot tr {
									border-style: solid hidden hidden;
									border-top-width: 1px;
								}
								</style>';

	$output				.=	'<body class="formulary" id="preview" style="font-family:Liberation Sans,Helvetica Neue,Helvetica,Arial,sans-serif;float: left; width:760px;margin-top: 5px; margin-left: 150px;margin-right: 150px; font-size: 1em; line-height: 1.5em; ">
		<div class="outer-center" >
			<div class="middle-center">
				<div class="inner-center">
					<div class="container formulary <?=$language_name?>" id="formulary">';

		$returnTxt		=	'';

		for($m=0;$m<count($block_array);$m++){

			$selBlocks	=	$this->_db->fetchAll("SELECT fb.*,f.formulary_title,f.bnf_reference,f.language_id FROM 3bit_tmp_formulary_blocks fb,3bit_formulary f WHERE f.formulary_id ='".$_POST['id']."' AND fb.version='".$_POST['lat_version']."' AND f.formulary_id=fb.formulary_id AND fb.block_type= '".$block_array[$m]."' GROUP BY fb.version");

			if(count($selBlocks)>0){
				$i		=	0;

				foreach($selBlocks as $brow){
					$block_id	=	$brow['block_id'];
					if($temp ==0){
					if(!empty($brow['bnf_reference'])){
						$returnTxt.=	'<div >
											<h1 style="border-bottom: 3px solid black; padding-bottom: 0.5em; text-transform: uppercase; margin-left: 0; padding-left: 0; margin-bottom: 0;">'.stripslashes($brow['formulary_title']).'</h1><h2>'.$brow['bnf_reference'].'</h2>
										</div>';
					}else{
						$returnTxt.=	'<div id="monographtitle" class="wide">
											<h1 style="border-bottom: 2px solid black; padding-bottom: 0.5em; text-transform: uppercase; margin-left: 0; padding-left: 0; margin-bottom: 0;">'.stripslashes($brow['formulary_title']).'</h1>
										</div>';
					}
				}

					$temp			=   $i+1;
					$block_type		=	stripslashes($brow['block_type']);

					if($block_type=='generic_supply')
						$block_type	=	'supply';			  

					if($brow['block_content1'] != ''){
						if($brow['block_type'] !='indication')
							$divid	=	$modelform->remExtra($brow['block_type']);
						else
							$divid	=	'indications';

					$returnTxt	.=	'<div id="'.$divid.'"  alt="block_content1">';

					if($block_type != 'post_text_article' && $block_type != 'pre_text_article'){
						if(!empty($brow['block_title']))
							$returnTxt	.=	'<H2>'.($brow['block_title']).'</H2>';
					}  

					if($brow['block_content1'] != ''){
						$decoded	=	stripslashes($brow['block_content1']);
						$returnTxt	.=	$decoded;
					}

					$returnTxt	.=	'</div>';	
					
					if($brow['block_type']=='supply')
						$textareaclass	=	'mceEditor-supply';
					else 
						$textareaclass	=	'mceEditor';

					//$returnTxt	.=	'<span class="'.$divid.'" ><textarea id="textarea_'.$divid.'" rows="10" cols="130" class="'.$textareaclass.'"></textarea><div class="tinyactions"><a class="tinysave" href="javascript:;" onclick=update_block($("#id").val(),$("#version").val(),"'.$divid.'","block_content1");>Save</a>&nbsp;<a  class="tinycancel" href="javascript:;" onclick=cancel_update();>Cancel</a></div></span>';

					if($block_type == 'indication'){
					   $contra_indications	= stripslashes($brow['block_content2']);	
					   if($contra_indications !=''){
							$divid		=	"contraindications";
							$returnTxt	.=	'<div id="'.$divid.'" alt="block_content2" class="greybox"><H2>'.$block_title[$brow['language_id']]['Contra Indications'].':</H2>';
							$returnTxt	.=	$contra_indications.'</div>';
							//$returnTxt	.=	'<span class="'.$divid.'" ><textarea id="textarea_'.$divid.'" rows="10" cols="130" class="mceEditor"></textarea><div class="tinyactions"><a class="tinysave" href="javascript:;" onclick=update_block($("#id").val(),$("#version").val(),"'.$divid.'","block_content2");>Save</a>&nbsp;<a class="tinycancel" href="javascript:;" onclick=cancel_update();>Cancel</a></div></span>';
						}
					}
				}
				 $i++;
				}					  
			}
		}

		if($returnTxt ==''){
			$returnTxt	=	'No sections are added!';	
		}else{
			$returnTxt	=	$modelform->getParsed($returnTxt,'1');
		}

		 /////////////////////////////////////////////////// To display blocks ///////////////////////////////////////////////////////////////////

		 /////////////////////////////////////////////////// To display footnote ///////////////////////////////////////////////////////////////////

		global $matchCount,$uniquearr,$lastuniquearr;			

		if(is_array($lastuniquearr)){	

			$lastuniquearr	=	array_values(array_unique($lastuniquearr)); 
			$pubid	=	implode(",",$lastuniquearr);
			$pubid	=	preg_replace(array('/\{/','/\}/'),array('',''),$pubid);
			$parr	=	explode(",",$pubid);

			$cnt	=	count($parr);

			if($cnt>0){

				$returnTxt	.= "<div id='references' style='border-top: 2px solid black; padding-top: 1em; margin-top: 1em;'><ol>";
				for($m=0;$m<$cnt;$m++){			

					$selLat	=	$this->_db->fetchAll("SELECT * FROM endnote_refs WHERE id='".$parr[$m]."' ORDER BY id ");
					if(count($selLat)>0){
						foreach($selLat as $row){

							$dispauth	=	'';
							$auth		=	array();
							$dispedit	=	'';
							$editors	=	array();
							$auth		=	explode("\r",$row['author']);						
							$authcount	=	count($auth);
							if($authcount>0){
								if($authcount>2){			
									$dispauth	=	$auth[0]." <i>et al</i>.";
								}else if($authcount==2){
									$dispauth	=	implode(",",$auth);
									$dispauth	=	substr($dispauth,0,-1);
									$dispauth	=	$dispauth;
								}else{
									$dispauth	=	$auth[0];
								}
							}else{
								$dispauth	=	$row['author'];
							}			

							if($editcount>0){
								if($editcount>2){
									$dispedit	= "In : ".$editors[0]." <i>et al</i>. (eds)&nbsp;";	
								}else if($editcount==2){
									$dispedit	= "In : ".$editors[0].",".$editors[1]." (eds) &nbsp;";
								}else{
									$dispedit	= "In : ".$editors[0]." (eds) &nbsp;";
								}
							}
							if($content !=''){
								$returnTxt	.= "<li><a name='".($m+1)."' ></a>".$content."</li>";
							}else{
								if($row['date']!='')
									$datetext	=	$row['year']."  ".$row['date'];
								else
									$datetext	=	$row['year'];							
								if(substr(stripslashes($row['title']),-1, 1)!='.')
									$reftit		=	stripslashes($row['title']).".";
								else
									$reftit		=	stripslashes($row['title']);

								if($row['pages']!=''){
									$returnTxt		.=	"<li id='".($m+1)."'><strong><a href='#up".($m+1)."'>^</a></strong> ".$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".$reftit."</i>";
									if($dispedit!=''){	
										$returnTxt		.=	"&nbsp;".$dispedit;
									}

									if($row['secondary_title']!=''){	
										$returnTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
									}

									if($row['publisher']!=''){
										 $returnTxt		.=	",&nbsp;".$row['publisher'];
									}

									if($row['place_published']!=''){
										$returnTxt		.= ",".$row['place_published'];
									}

									if($row['volume']!='' && $row['pages']!=''){
										$returnTxt		.= "&nbsp;<b>".$row['volume']."</b>:&nbsp;".$row['pages'];
									}else if($row['volume']=='' && $row['pages']!=''){
										$returnTxt		.= "&nbsp;pp.&nbsp;".$row['pages'];
									}

									$returnTxt		.= ".";

									if($row['accession_number']!='' && is_numeric($row['accession_number'])){

										$returnTxt		.= " <a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Citation&list_uids=".$row['accession_number']."'>PubMed: ".$row['accession_number']."</a>.";
									}

									$returnTxt		.= "</li>";
								}else{
									$returnTxt		.=	"<li id='".($m+1)."'><strong><a href='#up".($m+1)."'>^</a></strong> ".$dispauth."&nbsp;(".$datetext.")&nbsp;<i>".$reftit."</i>";
									if($dispedit!=''){	
										$returnTxt		.=	"&nbsp;".$dispedit;
									}
									if($row['secondary_title']!=''){	
										$returnTxt		.=	"&nbsp;<i>".stripslashes($row['secondary_title'])."</i>";
									}

									if($row['edition']!=''){   
										$returnTxt		.=	"&nbsp;(".$row['edition']."e).";
									}

									if($row['publisher']!=''){
										 $returnTxt		.=	"&nbsp;".$row['publisher'];
									}

									if($row['place_published']!=''){
										$returnTxt		.= ",".$row['place_published'];
									}

									$returnTxt		.= ".";

									if($row['accession_number']!='' && is_numeric($row['accession_number'])){
										$returnTxt		.= " <a href='http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Citation&list_uids=".$row['accession_number']."'>PubMed: ".$row['accession_number']."</a>.";
									}
									$returnTxt		.= "</li>";
								}
							}
						}
					}
				}
				 $returnTxt	.= "</ol></div>";
			}
		}else{
				$selRefs	=	$this->_db->fetchAll("SELECT fb.*,f.formulary_title,f.bnf_reference,f.language_id FROM 3bit_tmp_formulary_blocks fb,3bit_formulary f WHERE f.formulary_id ='".$_POST['id']."' AND fb.version='".$_POST['lat_version']."' AND f.formulary_id=fb.formulary_id AND fb.block_type= 'references' GROUP BY fb.version");
				if(count($selRefs)>0){
				 $returnTxt	.= "<div id='references' style='border-top: 2px solid black; padding-top: 1em; margin-top: 1em;' alt='block_content1'>";
					foreach($selRefs as $arefs){
						if($arefs['block_content1'] != ''){
								$decoded	=	(stripslashes($arefs['block_content1']));
								$returnTxt	.=	$decoded;
						}
					}
				 $returnTxt	.= "</div>";
				 //$returnTxt	.=	'<span class="references" ><textarea id="textarea_references" rows="10" cols="130" class="mceEditor"></textarea><div class="tinyactions"><a  class="tinysave" href="javascript:;" onclick=update_block($("#id").val(),$("#version").val(),"references","block_content1");>Save</a>&nbsp;<a class="tinycancel" href="javascript:;" onclick=cancel_update();>Cancel</a></div></span>';
			}
		}

		$output	.= $returnTxt;
		$output	.= '</div>
				   </div>
				 <div class="middle-east">
				 </div>
			   </div>
			 </div>
		</body>
		</html>';
		$filename		= 'formulary_'.$_POST['id'].'_'.date("y-m-d-h-i").'.doc';
		$file			= $_SERVER['DOCUMENT_ROOT'].'/formulary/'.$filename;

		$fp				= fopen($file, 'w') or die("can't open file");
		fwrite($fp, str_replace('�','',$output));
		fclose($fp);

		/*

		$path			=		$_SERVER['DOCUMENT_ROOT']."/logincms/public/"; // change the path to fit your websites document structure
		echo $fullPath		=		$path.$file;

		*/

		$response		=  $filename;

		die($response);
	}
}
