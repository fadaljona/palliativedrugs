<?php 
class SddbController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
	   $this->_ns		=	Zend_Registry::get("ns");
       $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($this->_ns->username)) {
			$this->_ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Syringe Driver Database');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('sddb');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/Syringe Driver Database');
	    }
		$sddb		=	new Application_Model_Sddb();
		$diluent	=	$sddb->getDiluents();
		$this->view->assign('diluent',$diluent);
    }	

	public function listAction()
	{
		$sddb		=	new Application_Model_Sddb();
		$sddb->listings($_POST);
	}

	public function searchAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Search Entry');	
		$sddb		=	new Application_Model_Sddb();		
		$drugs		=	array("",
						"Alfentanil", 
						"Diamorphine",
						"Fentanyl",
						"Hydrocodone", 
						"Hydromorphone", 
						"Methadone", 
						"Morphine hydrochloride", 
						"Morphine sulphate",
						"Morphine tartrate",
						"Oxycodone",
						"Sufentanil", 
						"Baclofen", 
						"Bupivacaine", 
						"Clonazepam", 
						"Clonidine",
						"Cyclizine lactate",
						"Dexamethasone (sodium phosphate salt)",
						"Dexamethasone (phosphate salt)",
						"Diclofenac sodium",
						"Flunitrazepam",
						"Glycopyrronium (glycopyrrolate)",
						"Granisetron",
						"Haloperidol",
						"Hydrocortisone",
						"Hyoscine butylbromide", 
						"Hyoscine hydrobromide (scopolamine hydrobromide)",
						"Ketamine",
						"Ketorolac trometamol",
						"Levetiracetam",
						"Levobupivacaine",
						"Levomepromazine (methotrimeprazine)",
						"Lidocaine",
						"Metoclopramide",
						"Midazolam",
						"Octreotide",
						"Ondansetron",
						"Parecoxib",
						"Phenobarbital (phenobarbitone)",
						"Promethazine hydrochloride",
						"Ranitidine",
						"Tramadol");
		$diluent	=	array("Please select ...",
							"Water",
							"0.9% Saline",
							"5% Dextrose",
							"None",
							"Other");
		$time		=	array("Please select ...",
							"0h",
							"4h",
							"8h",
							"12h",
							"16h",
							"24h",
							"48h",
							"other");
		$compatibility=	array("Please select ...",
							"Appeared compatible",
							"Incompatible",
							"See Comments");

		$datatype	=	array("Observational data",
							"Chemical laboratory stability data",
							"Physical laboratory stability data"
							);

		$reaction	=	array("Please select ...",
							"No",
							"Yes",
							"Unknown");

		if(empty($_POST)){
				$form = array
				(
						'druglist1'		=> $sddb->arrayToListBox("drug1_name",$drugs,""),
						'druglist2'		=> $sddb->arrayToListBox("drug2_name",$drugs,""),
						'druglist3'		=> $sddb->arrayToListBox("drug3_name",$drugs,""),
						'druglist4'		=> $sddb->arrayToListBox("drug4_name",$drugs,""),
						'druglist5'		=> $sddb->arrayToListBox("drug5_name",$drugs,""),
						'druglist6'		=> $sddb->arrayToListBox("drug6_name",$drugs,""),
						'result'		=> ''
				); 	
				$this->view->assign('form',$form);			
		}else{
			$form	=	$_POST;			
			$result	=	$sddb->listings($form);	
			$form['result']= $result;
			$this->view->assign('form',$form);	
	    }
	}

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Create New Entry');	
		$sddb		=	new Application_Model_Sddb();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$drugs		=	array("",
						"Alfentanil", 
						"Diamorphine",
						"Fentanyl",
						"Hydrocodone", 
						"Hydromorphone", 
						"Methadone", 
						"Morphine hydrochloride", 
						"Morphine sulphate",
						"Morphine tartrate",
						"Oxycodone",
						"Sufentanil", 
						"Baclofen", 
						"Bupivacaine", 
						"Clonazepam", 
						"Clonidine",
						"Cyclizine lactate",
						"Dexamethasone (sodium phosphate salt)",
						"Dexamethasone (phosphate salt)",
						"Diclofenac sodium",
						"Flunitrazepam",
						"Glycopyrronium (glycopyrrolate)",
						"Granisetron",
						"Haloperidol",
						"Hydrocortisone",
						"Hyoscine butylbromide", 
						"Hyoscine hydrobromide (scopolamine hydrobromide)",
						"Ketamine",
						"Ketorolac trometamol",
						"Levetiracetam",
						"Levobupivacaine",
						"Levomepromazine (methotrimeprazine)",
						"Lidocaine",
						"Metoclopramide",
						"Midazolam",
						"Octreotide",
						"Ondansetron",
						"Parecoxib",
						"Phenobarbital (phenobarbitone)",
						"Promethazine hydrochloride",
						"Ranitidine",
						"Tramadol");

		$diluent	=	array("Please select ...",
							"Water",
							"0.9% Saline",
							"5% Dextrose",
							"None",
							"Other");
		$time		=	array("Please select ...",
							"0h",
							"4h",
							"8h",
							"12h",
							"16h",
							"24h",
							"48h",
							"other");
		$compatibility=	array("Please select ...",
							"Appeared compatible",
							"Incompatible",
							"See Comments");

		$datatype	=	array("Observational data",
							"Chemical laboratory stability data",
							"Physical laboratory stability data"
							);

		$reaction	=	array("Please select ...",
							"No",
							"Yes",
							"Unknown");

		if(!$id){
				$form = array
				(
						'id'			=> '',
						'unit_code'		=> '',
						'unit_ref'		=> '',	
						'drug1_name'	=> '',
						'druglist1'		=> $sddb->arrayToListBox("drug1_name",$drugs,""),
						'drug1_dose'	=> '',
						'drug2_name'	=> '',
						'druglist2'		=> $sddb->arrayToListBox("drug2_name",$drugs,""),
						'drug2_dose'	=> '',
						'drug3_name'	=> '',
						'druglist3'		=> $sddb->arrayToListBox("drug3_name",$drugs,""),
						'drug3_dose'	=> '',
						'drug4_name'	=> '',
						'druglist4'		=> $sddb->arrayToListBox("drug4_name",$drugs,""),
						'drug4_dose'	=> '',
						'drug5_name'	=> '',
						'druglist5'		=> $sddb->arrayToListBox("drug5_name",$drugs,""),
						'drug5_dose'	=> '',
						'drug6_name'	=> '',
						'druglist6'		=> $sddb->arrayToListBox("drug6_name",$drugs,""),
						'drug6_dose'	=> '',
						'diluent'		=> $sddb->arrayToListBox("diluent",$diluent,""),
						'volume'		=> '',
						'time'			=> $sddb->arrayToListBox("time",$time,""),
						'duration_other'=> '',
						'compatibility'	=> $sddb->arrayToListBox("compatibility",$compatibility,""),
						'datatype'		=> $sddb->arrayToListBox("datatype",$datatype,""),
						'reaction'		=> $sddb->arrayToListBox("reaction",$reaction,""),
						'comments'		=> '',
						'support_data'	=> '',
						'approved'		=> '',
						'addedby'		=> $this->_ns->fullname,
				); 	
			
		}else if($id && !$_POST){
			$sdata		=	$sddb->read($id);
			$form = array
				(
						'id'			=> $sdata[0]['id'],
						'unit_code'		=> $sdata[0]['unit_code'],
						'unit_ref'		=> $sdata[0]['unit_ref'],	
						'drug1_name'	=> $sdata[0]['drug1_name'],
						'druglist1'		=> $sddb->arrayToListBox("drug1_name",$drugs,$sdata[0]['drug1_name']),
						'drug1_dose'	=> $sdata[0]['drug1_dose'],
						'drug2_name'	=> $sdata[0]['drug2_name'],
						'druglist2'		=> $sddb->arrayToListBox("drug2_name",$drugs,$sdata[0]['drug2_name']),
						'drug2_dose'	=> $sdata[0]['drug2_dose'],
						'drug3_name'	=> $sdata[0]['drug3_name'],
						'druglist3'		=> $sddb->arrayToListBox("drug3_name",$drugs,$sdata[0]['drug3_name']),
						'drug3_dose'	=> $sdata[0]['drug3_dose'],
						'drug4_name'	=> $sdata[0]['drug4_name'],
						'druglist4'		=> $sddb->arrayToListBox("drug4_name",$drugs,$sdata[0]['drug4_name']),
						'drug4_dose'	=> $sdata[0]['drug4_dose'],
						'drug5_name'	=> $sdata[0]['drug5_name'],
						'druglist5'		=> $sddb->arrayToListBox("drug5_name",$drugs,$sdata[0]['drug5_name']),
						'drug5_dose'	=> $sdata[0]['drug5_dose'],
						'drug6_name'	=> $sdata[0]['drug6_name'],
						'druglist6'		=> $sddb->arrayToListBox("drug6_name",$drugs,$sdata[0]['drug6_name']),
						'drug6_dose'	=> $sdata[0]['drug6_dose'],
						'diluent'		=> $sddb->arrayToListBox("diluent",$diluent,$sdata[0]['diluent']),
						'volume'		=> $sdata[0]['volume'],
						'time'			=> $sddb->arrayToListBox("time",$time,$sdata[0]['duration']),
						'duration_other'=> $sdata[0]['duration_other'],
						'compatibility'	=> $sddb->arrayToListBox("compatibility",$compatibility,$sdata[0]['compatibility']),
						'datatype'		=> $sddb->arrayToListBox("datatype",$datatype,$sdata[0]['datatype']),
						'reaction'		=> $sddb->arrayToListBox("reaction",$reaction,$sdata[0]['site_reaction']),
						'comments'		=> $sdata[0]['comments'],
						'support_data'	=> $sdata[0]['support_data'],
						'approved'		=> $sdata[0]['approved'],
						'addedby'		=> $sdata[0]['added_by'],
						
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$sddb->update($form);
			else
				$sddb->add($form);
			$this->_redirect('/sddb');
       } 		
    }
	
	public function deleteAction()
	{
	    $sddb		=	new Application_Model_Sddb();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$sddb->delete($id);
		$this->_redirect('/sddb');
	}

	public function moveupAction()
    {		
		$sddb		=	new Application_Model_Sddb();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$mid		=	$request->getParam("mid");
		$sddb->moveup($id,$mid);
		$this->_redirect('/sddb/add/id/'.$id);
	}

	public function browseAction()
    {		
		$this->view->assign('title','Palliative Drugs CMS :: SDDB - Index');	
	}

	public function browsedrugsAction()
	{
		$sddb		=	new Application_Model_Sddb();		
		$sddb->browseDrugs($_POST);
	}

	public function archiveAction()
	{
		$sddb		=	new Application_Model_Sddb();		
		$sddb->archive($_POST);		
	}

	public function archivesAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Syringe Driver Database - Archives');
    }	

	public function archivelistAction()
	{
		$sddb		=	new Application_Model_Sddb();
		$sddb->archivelistings($_POST);
	}
}
