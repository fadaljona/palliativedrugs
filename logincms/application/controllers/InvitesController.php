<?php 
class InvitesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Survey Invites');		
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('members');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/survey invites');
	    }
    }	

	public function sendAction()
  {
		$this->view->assign('title','Palliative Drugs CMS :: Send Invites');	
		$invs			=	new Application_Model_Invites();				
		$groups		= $invs->getGroups();
		$fields		=	$invs->getUserFields();
		
		if (!$_POST){
      $this->view->assign('groups',$groups);			
      $this->view->assign('fields',$fields);			
    }else{  
			$form	=	array_map("trim",$_POST);
			$invs->sendinvites($form);
			$this->_redirect('/invites');
     } 		
  }

  	public function resendAction()
  {
		$this->view->assign('title','Palliative Drugs CMS :: Send Invites');	
		$invs			=	new Application_Model_Invites();				
		$groups		= $invs->getGroups();
		$fields		=	$invs->getUserFields();	
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if (!$_POST){
	      $this->view->assign('groups',$groups);			
	      $this->view->assign('fields',$fields);			
	      $this->view->assign('id',$id);			
	    }else{  
				$form	=	array_map("trim",$_POST);
				$invs->sendreinvites($form);
				$this->_redirect('/invites');
	     } 		
  }


}
