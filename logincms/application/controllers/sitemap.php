<?php
class Sitemap_Controller extends Website_Controller
{
	private $sitemap;  

    public function index()
    {
        url::redirect('sitemap/page/1');
    }

	// Function to add paging with the users listing //

	public function page($page_no,$sortname='smgroup_id',$sortorder='asc')
    {		
		$this->_page($page_no, $sortname, $sortorder);
    }

	// Function to add/update users to the db //

	public function sitemap_add($id='')
	{
	    $this->sitemap						= new Sitemap_Model;
		$this->template->title				= 'NCCC CMS :: Add/Edit Links';
        $this->template->content			= new View('sitemap/sitemap_add');	
		$this->template->content->groups	= $this->sitemap->active_groups();		
		$this->template->content->errors	= array(); 
		if(!$id){
				$this->template->content->list_cat	= $this->sitemap->list_cat('0','','0');
				$form = array
				(
						'id'		=>'',
						'smgroup_id'=> '',
						'title'		=> '',
						'cat_id'	=> '',
						'link'		=> '',
						'status'	=> '',
				); 	
			
		}else{
			$userdata	= $this->sitemap->read($id);
			$this->template->content->list_cat	= $this->sitemap->list_cat('0','',$userdata[0]->cat_id);
			$form = array
				(
						'id'		 =>	$userdata[0]->link_id,
						'smgroup_id' => $userdata[0]->smgroup_id,
						'title'		 => $userdata[0]->title,
						'cat_id'	 => $userdata[0]->cat_id,
						'link'		 => $userdata[0]->link,						
						'status'	 => $userdata[0]->status,
				); 	
		} 	

		if (!$_POST){
            $this->template->content->form = $form;
        }else{
            $post = new Validation($_POST);
            $post->pre_filter('trim', TRUE);
            $post->add_rules('title','required');
            $post->add_rules('smgroup_id', 'required');           
		    $form = $post->as_array();
            if(!$post->validate()){            
             $this->template->content->form = $form;
             $this->template->content->errors = $post->errors('form_error');
            }else{
				if($form['id']!='')
					$this->sitemap->update($form);
				else
					$this->sitemap->add($form);
				url::redirect('/sitemap');
            }
        } 
	}	

	// Function to delete user from db //

	public function sitemap_delete($id='')
	{
	    $this->sitemap = new Sitemap_Model;
		$this->sitemap->delete($id);
		url::redirect('/sitemap');
	}

	// private functions //

	private function _page($page_no,$sortname,$sortorder)
    {	
		$this->template->title = 'NCCC CMS :: Sitemap';
        $this->template->content = new View('sitemap/sitemap');
		if($sortorder=='asc')
			$revorder	=	'desc';
		else
			$revorder	=	'asc';

		$this->template->content->sortorder	 = $revorder;
		$this->template->content->page_no	 = $page_no;
		
		// Instantiate the model
        $this->sitemap = new Sitemap_Model;
		
		$this->session = Session::instance();
		

		if(!empty($_POST))
			$_SESSION	=	$_POST;
		else
			$_SESSION	=	$_SESSION;		
		
		
		$this->template->content->search = $_SESSION; 

		$this->template->content->temp = ''; 

        // The result set is assigned to a variable called $users in the view
        $this->template->content->sitemap = $this->sitemap->browse($sortname,$sortorder, $_SESSION);

	}
}

?>