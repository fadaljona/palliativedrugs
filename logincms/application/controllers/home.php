<?php defined('SYSPATH') or die('No direct script access.');

class Home_Controller extends Website_Controller {

    public function index()
    {
        $this->template->title = 'Palliative Drugs CMS :: Dashboard';
        $this->template->message = 'Logged in on ';       
		$this->template->content = new View('pages/home');
		$this->audits	= new Home_Model;
		$this->template->content->audits = 	$this->audits->get_audits();
    }

}
?>