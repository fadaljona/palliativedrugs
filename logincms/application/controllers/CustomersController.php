<?php 
class CustomersController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Customers');	
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('customers');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/customers');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Edit Customer');	
		$customers	=	new Application_Model_Customers();
		$country	=	$customers->getCountries();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if($id){
			$cdata	= $customers->read($id);
			$form = array
				(
						'id'			=> $cdata[0]['customer_id'],
						'title'			=> $cdata[0]['title'],
						'fname'			=> $cdata[0]['fname'],
						'lname'			=> $cdata[0]['lname'],
						'email'			=> $cdata[0]['email'],
						'address1'		=> $cdata[0]['address1'],
						'address2'		=> $cdata[0]['address2'],						
						'city'			=> $cdata[0]['city'],						
						'state'			=> $cdata[0]['state'],						
						'county'		=> $cdata[0]['county'],						
						'country'		=> $cdata[0]['country'],						
						'postcode'		=> $cdata[0]['postcode'],						
						'phone'			=> $cdata[0]['phone'],						
						'password'		=> $cdata[0]['password'],
				); 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
            $this->view->assign('country',$country);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$customers->update($form);			
			$this->_redirect('/customers');
       } 		
    }
	
	public function historyAction()
	{
		$this->view->assign('title','Palliative Drugs CMS :: View Order History');	
	    $customers	=	new Application_Model_Customers();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$history	=	$customers->history($id);
		$this->view->assign('history',$history);
	}
}
