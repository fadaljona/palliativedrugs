<?php 
class PubmedController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
        $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Pubmeds');		
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('formulary');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/pubmeds');
	    }
    }	

	public function pubmedrefaddAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Pubmed');	
		$pubmeds	=	new Application_Model_Pubmed();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=> '',
						'title'			=> '',
						'module_permissions'=>'',
						'status'		=> '',
				); 	
			
		}
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			$pubmeds->add($form);
			$this->_redirect('/pubmed');
       } 		
    }	

	public function manualrefaddAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Pubmed');	
		$pubmeds	=	new Application_Model_Pubmed();		
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		if(!$id){
				$form = array
				(
						'id'			=>'',
						'pub_date'		=> '',
						'last_editor'	=> '',
						'title'			=> '',
						'full_journalname'=> '',
						'volume'		=> '',
						'issue'			=> '',
						'pages'			=> '',
						'content'		=> '',
				); 				
		}else{
			$pdata	= $pubmeds->read($id);	
			$form = array
				(
						'id'			=> $pdata[0]['pubmed_id'],
						'pub_date'		=> $pdata[0]['pub_date'],
						'last_editor'	=> $pdata[0]['last_editor'],
						'title'			=> $pdata[0]['title'],
						'full_journalname'=> $pdata[0]['full_journalname'],
						'volume'		=> $pdata[0]['volume'],
						'issue'			=> $pdata[0]['issue'],
						'pages'			=> $pdata[0]['pages'],						
						'content'		=> $pdata[0]['content'],
				); 	
		} 	
		if (!$_POST){
            $this->view->assign('form',$form);
        }else{  
			$form	=	array_map("trim",$_POST);
			if($form['id']!='')
				$pubmeds->manual_update($form);
			else
				$pubmeds->manual_add($form);
			$this->_redirect('/pubmed');
       } 		
    }	
}
