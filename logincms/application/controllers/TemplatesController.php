<?php 
class TemplatesController extends Zend_Controller_Action
{
	public function init()
    {
        /* Initialize action controller here */
		$ns	=	Zend_Registry::get("ns");
       $this->_baseurl	=	Zend_Registry::get("baseUrl");
        if (empty($ns->username)) {
			$ns->past_url = $_SERVER['REQUEST_URI'];
            $this->_response->setRedirect($this->_baseurl.'/index')->sendResponse();
	    exit;
        }
		$this->_helper->layout->setLayout('layout1');
		$this->view->assign('fullname',$ns->fullname);		
    }

    public function indexAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Article Templates');
		$audit		=	new Application_Model_Audits();
		$access		=	$audit->check_permission('pages');
		if(empty($access)){
		   $this->_redirect('/home/index/denied/templates');
	    }
    }	

	public function addAction()
    {
		$this->view->assign('title','Palliative Drugs CMS :: Add/Edit Templates');	
		$templates	=	new Application_Model_Templates();
		$articles	=	new Application_Model_Articles();
		$select		=	new Application_Model_Selectpattern();		
		$snippets	=	$articles->getSnippets();
		$users		=	$templates->getUsers();
		$this->view->assign('snippets',$snippets);	
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$fields		=	$templates->getFields();
		if(!$id){
				$form = array
				(
						'id'				=> '',
						'temp_filename'		=> '',
						'temp_type'			=> '',
						'temp_content'		=> '',
						'custom_template'	=> '',
						'temp_thumbs'		=> '',
						'temp_thumbs_img'	=> '',
						'temp_status'		=> '',
						'message_id'		=> '',
						'message_type'		=> '',
						'message_to'		=> '',
						'user_id'			=> '',
						'mail_subject'		=> '',
						'message_text'		=> '',
						'text'				=> '',
						'email_status'		=> '',
						'active'			=> '',
				); 	
			
		}else if($id && !$_POST){
			$tempdata	= $templates->read($id);
			$form = array
				(
						'id'				=> $tempdata[0]['temp_id'],
						'temp_filename'		=> $tempdata[0]['temp_filename'],
						'temp_type'			=> $tempdata[0]['temp_type'],
						'temp_content'		=> $tempdata[0]['temp_content'],
						'custom_template'	=> $tempdata[0]['custom_template'],
						'temp_thumbs'		=> $tempdata[0]['temp_thumbs'],
						'temp_thumbs_img'	=> $tempdata[0]['temp_thumbs_img'],
						'temp_status'		=> $tempdata[0]['temp_status'],
						'message_id'		=> $tempdata[0]['message_id'],
						'message_type'		=> $tempdata[0]['message_type'],
						'message_to'		=> $tempdata[0]['message_to'],
						'user_id'			=> $tempdata[0]['user_id'],
						'mail_subject'		=> $tempdata[0]['mail_subject'],
						'message_text'		=> $tempdata[0]['message_text'],
						'text'				=> $tempdata[0]['text'],
						'email_status'		=> $tempdata[0]['email_status'],
						'active'			=> $tempdata[0]['active'],
				); 	 	
			

		}
		if (!$_POST){
            $this->view->assign('form',$form);
			$this->view->assign('select',$select);
			$this->view->assign('fields',$fields);
			$this->view->assign('users',$users);
        }else{  
			$form	=	$_POST;
			if($form['id']!='')
				$templates->update($form);
			else
				$templates->add($form);
			$this->_redirect('/templates');
       } 		
    }
	
	public function deleteAction()
	{
	    $users		=	new Application_Model_Users();
		$request	=	$this->getRequest();		
		$id 		=	$request->getParam("id");
		$users->delete($id);
		$this->_redirect('/users');
	}
}
