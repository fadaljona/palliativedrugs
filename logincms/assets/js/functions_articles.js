$(document).ready(function () {
	 articletable	=	$('#articlelist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": false,
		"bLengthChange": 100,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"articles"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Validate article ==============================================

function validate_article(){
	flag=true;
	var message		= '';
	if(tinyMCE.getInstanceById('long_text')){
		lcontent	=	escape(tinyMCE.get('long_text').getContent());
	}	
	if($('#article_title').val()==''){
	   message += '<li>Please enter title</li>';
	   flag= false;
	}	
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addarticle').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}