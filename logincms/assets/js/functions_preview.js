var outerLayout, middleLayout, innerLayout; 
var clickflag=false;
var clickstat=false;

$(document).ready(function () { 

	outerLayout = $('#container').layout({ 			
		center__paneSelector:		".outer-center" 
		,	west__paneSelector:		".outer-west" 
		,	east__paneSelector:		".outer-east" 
		,	spacing_open:			8 // ALL panes
		,	spacing_closed:			12 // ALL panes
		,	north__spacing_open:	0
		,	south__spacing_open:	0
		,	south__maxSize:			300
		, 	north__size:		"auto"	
		,	north__closable:		false		
		,	north__resizable:		false
		,	center__onresize:		"middleLayout.resizeAll" 
	
	});
	
	middleLayout = $('div.outer-center').layout({ 
		center__paneSelector:	".middle-center" 
	,	west__paneSelector:		".middle-west" 
	,	east__paneSelector:		".middle-east" 
	,	west__size:				175 
	,	east__size:				275		
	,	east__initClosed:		true 
	,	spacing_open:			10  // ALL panes
	,	spacing_closed:			10 // ALL panes
	,	center__onresize:		"innerLayout.resizeAll"
	}); 

	innerLayout = $('div.middle-center').layout({ 
		center__paneSelector:	".inner-center" 
	,	west__paneSelector:		".inner-west" 
	,	east__paneSelector:		".inner-east" 
	,	south__size: 			"275"
	,	south__initClosed:		true 
	,	spacing_open:			10  // ALL panes
	,	spacing_closed:			10  // ALL panes
	}); 

	$("#tabs").tabs();
	$("#breadCrumb3").jBreadCrumb();		
	$("#filters").accordion();
	$("#accordion").accordion({ header: "h3" });
	$("#addforms").tabs();
	$('#tabs, #listform').tabs();
	$("#pubstatus").buttonset();
	$("#editstatus").buttonset();

	$('#radio5').click(function(){
	  middleLayout.close('east');
	  innerLayout.close('south');	
	  $("#formulary textarea").parent().css({display:"none"});
	  $(".container>div:not(:first-child)").css({display:"block"});
	  $('.container').removeClass('editable');
	  $(".container>div:not(:first-child)").removeClass("edit");
	  $(".snippet").html('');
	});

	$('#radio6').click(function(){
		middleLayout.open('east');
		//innerLayout.open('south');		
		$('.container').addClass('editable');
		$(".container>div:not(:first-child)").addClass("edit");
		if(clickflag==false){
			clickflag=true;				
			initclick();
			initsnippet_click();
		}
	});		

	$("#formulary textarea").parent().css({display:"none"});

	$('input[name="radio_status"]').each(function(){
		$(this).click(function(){
			formulary_id	=	$('#id').val();
			publish_status	=	$(this).val();
			if(clickstat==false){
				clickstat=true;
				update_status(formulary_id,publish_status);
			}
			return false;
		});		
	});

	if($('#radio6').attr('checked')==true){
		$('#radio6').trigger('click');		
	};

	$('.dialog-confirm').hide();
});   

function initclick(){	
	$("#formulary .edit").one('dblclick',function(event){
			event.preventDefault();
			showArea = $(this).attr("id");	
			removeEditors();
			areas = $("#formulary textarea").parent().css({display:"none"});
			$(".container>div:not(:first-child)").css({display:"block"});
			initclick();
			$('#snippetid').val('');
			for(i=0;i<areas.length;i++){				
				if($(areas[i]).css("display")=="none"){
					edditorclass	=	$(areas[i]).attr("class");						
					if(showArea == edditorclass){						
						$('#'+showArea).hide();							
						$('.'+edditorclass).show();							
						update_textarea($('#id').val(),$('#version').val(),showArea);
					}						
				}
			} 
		});	
}

function initsnippet_click(){
	if($('.snippet')){
		$('.snippet').show();
		$('.snippet').html('<a href="javascript:;" class="snippetedit">Edit</a>');
		removeEditors();
		$('.snippetedit').click(function(event){
			event.preventDefault();
			areas = $("#formulary textarea").parent().css({display:"none"});
			$(".container>div:not(:first-child)").css({display:"block"});
			initclick();
			var snippetid	=	$(this).parent().attr('id');
			var blocktype	=	$(this).parent().parent().parent().attr('id');
			$('#'+blocktype).hide();
			$('.'+blocktype).show();
			update_snippetarea(snippetid,blocktype);
			//alert(snippetid+"========="+blocktype);
		});
	}
}

 tinyMCE.init({
			mode : "specific_textareas",
			theme : "advanced",
			editor_selector : "mceEditor",
			width:"950",	
			height:"500",
			relative_urls : true, 
			remove_script_host : false, 
			document_base_url : "http://pd2016.palliativedrugs.com/", 
			convert_urls : false,
			plugins : "safari,spellchecker,style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,pagebreak,table",
			theme_advanced_buttons1_add_before : "save,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect,emotions,iespell",
			theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
			theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
			theme_advanced_buttons3_add_before : "spellchecker,separator,styleprops,|,template,blockquote,|,insertfile,insertimage,",
			theme_advanced_buttons3_add : "tablecontrols",
			table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
			table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
			table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
			table_cell_limit : 100,
			table_row_limit : 5,
			table_col_limit : 5,
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			content_css : "/logincms/assets/css/formulary.css, /logincms/assets/css/tables.css",
			body_class : "container",
			plugin_insertdate_dateFormat : "%Y-%m-%d",
			plugin_insertdate_timeFormat : "%H:%M:%S",
			external_link_list_url : "example_data/example_link_list.js",
			external_image_list_url : "example_data/example_image_list.js",
			flash_external_list_url : "example_data/example_flash_list.js",
			template_external_list_url : "example_data/example_template_list.js",
			theme_advanced_resize_horizontal : false,
			theme_advanced_resizing : true,
			apply_source_formatting : true,
			spellchecker_languages : "+English=en"			
	});

function removeEditors(){
  var editors = document.getElementsByTagName('textarea');  
    for(var i = 0; i < editors.length; i++){    				
        var editid = editors[i].getAttribute('id');			
        if (tinyMCE.getInstanceById(editid)){
			tinyMCE.execCommand('mceFocus', false, editid);          
			tinyMCE.execCommand('mceRemoveControl', false, editid);
		}
    }
}

function delay(millis)
{
var date = new Date();
var curDate = null;

do { curDate = new Date(); }
while(curDate-date < millis);
} 

// Function to insert tags into textarea //	

function mySimpleHtmlInserter(insertHtml){
	if(tinyMCE.activeEditor){		
		  tinyMCE.getInstanceById(tinyMCE.activeEditor.id).getWin().focus();
		  tinyMCE.execCommand('mceInsertContent',false,insertHtml);
	}
}

function edit_article(block_type,formulary_id,block_id){
	$('.dialog_article').dialog('open');
}

function update_status(formulary_id,publish_status){
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "formulary=" + formulary_id+"&status="+publish_status+"&action=update_status&s=" +ran_number,
		   success: function(response){	
			 opvar	=	response.split("^");
			 if(opvar[1]=='S'){	
				 if(opvar[0]>0){
					 baseurl = '/logincms/includes/version_preview.php?formulary_id='+formulary_id+'&version='+opvar[0];
					 //window.location.href=baseurl;
					 open_dialog();
				}else{
				     window.location.href=window.location.href;
				}
			 }    
		  }
	 });
	 return false;
}

function update_textarea(formulary_id,version,block_type){
   var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "formulary=" + formulary_id+"&version="+version+"&block_type="+block_type+"&action=update_textarea&s=" +ran_number,
		   success: function(response){
			 getOP	=	response.split("^");
			 if(getOP[1]=='S'){	
				textareaid	=	'textarea_'+block_type;
				$('#'+textareaid).val(getOP[0]);					
				tinyMCE.execCommand('mceAddControl', false, textareaid);
			 }    
		  }
	 });
	 return false;
}

function update_snippetarea(snippetid,block_type){
   var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "snippetid=" + snippetid+"&block_type="+block_type+"&action=update_snippetarea&s=" +ran_number,
		   success: function(response){
			 getOP	=	response.split("^");
			 if(getOP[3]=='S'){	
				$('#snipfields').remove();				
				$('.'+block_type).prepend('<span id="snipfields">Title : <input type="text" name="snippet_title" id="snippet_title" class="element text small" value="'+getOP[1]+'" size="60"><br/>Number : <input type="text" name="snippet_number" id="snippet_number" class="element text small" value="'+getOP[2]+'" size="60"></span>');
				textareaid	=	'textarea_'+block_type;
				$('#'+textareaid).val(getOP[0]);	
				$('#snippetid').val(snippetid);
				tinyMCE.execCommand('mceAddControl', false, textareaid);								
			 }    
		  }
	 });
	 return false;
}

function update_block(formulary_id,version,block_type){
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	tinyMCE.triggerSave();
	var bcontent	=	tinyMCE.get('textarea_'+block_type).getContent();
	bcontent		=	escape(bcontent);
	bcontent		=	bcontent.replace(/\+/g, '%2B');
	var snippetid	=	$('#snippetid').val();
	var appendvar	=	'';
	if($('#snippet_title').size()){
		appendvar	+=	'&snippettit='+escape($('#snippet_title').val());
	}if($('#snippet_number').size()){
		appendvar	+=	'&snippetnum='+$('#snippet_number').val();
	}	
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "formulary=" + formulary_id+"&version="+version+"&bcontent="+bcontent+"&block_type="+block_type+"&snippetid="+snippetid+appendvar+"&action=update_block&s=" +ran_number,
		   success: function(response){			 
			 if(response=='S'){					
				baseurl	= window.location.href;
				var editexist	=	baseurl.indexOf('edit=');
				if(editexist==-1)
					baseurl = baseurl+"&edit=1";
				window.location.href=baseurl;
			 }    
		  }
	 });
	 return false;
}

function cancel_update(){
	baseurl	= window.location.href;
	var editexist	=	baseurl.indexOf('edit=');
	if(editexist==-1)
		baseurl = baseurl+"&edit=1";
	window.location.href=baseurl;
}

function open_dialog(){		
	$(".dialog-confirm").dialog({
		resizable: false,
		height:120,
		modal: true,
		buttons: {
			'Ok': function() {
				history.go(-1);
			}			
		}
	});
}