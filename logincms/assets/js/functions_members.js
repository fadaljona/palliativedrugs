$(document).ready(function () {
	TableToolsInit.sSwfPath = "/logincms/assets/swf/ZeroClipboard.swf";
	membertable	=	$('#memberlist').dataTable({
		"sDom": 'T<"clear">lfrtip',		
		"bStateSave": true,
		"bPaginate": true,
		"iDisplayLength":15,
		"sPaginationType": "full_numbers",
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"members"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});							


// Function to insert / update the member ----------------------------------------

function validate_member(action){
	flag=true;
	var message		= '';
	if($('#title').val()==''){
		message	+=	'<li>Please select title.</li>';  
		flag = false;
	}if($('#FirstName').val()==''){
		message	+=	'<li>Please enter first name.</li>';  
		flag = false;
	}if($('#LastName').val()==''){
		message	+=	'<li>Please enter last name.</li>';  
		flag = false;
	}if($('#role').val()==''){
		message	+=	'<li>Please enter role.</li>';  
		flag = false;
	}if($('#prof_reg_num').val()==''){
		message	+=	'<li>Please enter registration no.</li>';  
		flag = false;
	}if($('#speciality').val()==''){
		message	+=	'<li>Please enter speciality.</li>';  
		flag = false;
	}if($('#sub_speciality').val()==''){
		message	+=	'<li>Please enter sub speciality.</li>';  
		flag = false;
	}if($('#organisation').val()==''){
		message	+=	'<li>Please enter organisation name.</li>';  
		flag = false;
	}if($('#addr1').val()==''){
		message	+=	'<li>Please enter address.</li>';  
		flag = false;
	}if($('#PostalCode').val()==''){
		message	+=	'<li>Please enter postcode.</li>';  
		flag = false;
	}if($('#phone').val()==''){
		message	+=	'<li>Please enter phone no.</li>';  
		flag = false;
	}if($('#main_workplace').val()==''){
		message	+=	'<li>Please enter primary work place.</li>';  
		flag = false;
	}if($('#current_post').val()==''){
		message	+=	'<li>Please enter current position.</li>';  
		flag = false;
	}if($('#username').val()==''){
		message	+=	'<li>Please enter username.</li>';  
		flag = false;
	}if($('#email').val()==''){
		message	+=	'<li>Please enter email.</li>';  
		flag = false;
	}if($('#pwd').val()==''){
		message	+=	'<li>Please enter password.</li>';  
		flag = false;
	}		 
   if(flag==true){
	   $('#action').val(action);
	   $('.form_description').find('.error').html('');
	   $('#addmember').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}