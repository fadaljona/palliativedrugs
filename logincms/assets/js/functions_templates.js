$(document).ready(function () {
	 atemptable	=	$('#atemplatelist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": false,
		"bLengthChange": 100,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"article_template"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Validate template ==============================================

function validate_template(){
	flag=true;
	var message		= '';
	if($('#temp_filename').val()==''){
	   message += '<li>Please enter template name</li>';
	   flag= false;
	}
	if($('#custom_template').attr('checked')!=true && $('#temp_content').val()==''){
	   message += '<li>Please enter template content</li>';
	   flag= false;
	}
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addtemp').submit();
   }else{		   
		$('#addtemp').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}