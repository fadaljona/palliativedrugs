$(document).ready(function () {
	 articletable	=	$('#languagelist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": false,
		"bLengthChange": 100,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"language"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Change default for the language =============================================

function changeDefault(langid,chkid){
	chkobj	=	$('#'+chkid);    
	if(chkobj.attr('checked')==true){
		$('input[type=checkbox]').attr('checked',false);
		chkobj.attr('checked',true);
		var ran_unrounded	=	Math.random()*100000;
		var ran_number		=	Math.floor(ran_unrounded); 
		var action			=	'default_language';
		var url_params		=	"langid="+langid+"&action="+action+"&s="+ran_number;
		$.ajax({
		type	:	'POST',
		url		:	'/logincms/includes/orderajax.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 				
			if(msg=="S"){
				
			}else{
				alert('Please try again!');
			}
		}
	});
	}else{
		chkobj.attr('checked',true);
	}
}

// Validate article ==============================================

function validate_language(){
	flag=true;
	var message		= '';
	if($('#language_name').val()==''){
	   message += '<li>Please enter name</li>';
	   flag= false;
	}if($('#language_code').val()==''){
	   message += '<li>Please enter code</li>';
	   flag= false;
	}		
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addlanguage').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}