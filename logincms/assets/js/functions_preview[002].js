var outerLayout, middleLayout, innerLayout; 

$(document).ready(function () { 

	outerLayout = $('#container').layout({ 			
		center__paneSelector:		".outer-center" 
		,	west__paneSelector:		".outer-west" 
		,	east__paneSelector:		".outer-east" 
		,	spacing_open:			8 // ALL panes
		,	spacing_closed:			12 // ALL panes
		,	north__spacing_open:	0
		,	south__spacing_open:	0
		,	south__maxSize:			300
		, 	north__size:		"auto"	
		,	north__closable:		false		
		,	north__resizable:		false
		,	center__onresize:		"middleLayout.resizeAll" 
	
	});
	
	middleLayout = $('div.outer-center').layout({ 
		center__paneSelector:	".middle-center" 
	,	west__paneSelector:		".middle-west" 
	,	east__paneSelector:		".middle-east" 
	,	west__size:				175 
	,	east__size:				275		
	,	east__initClosed:		true 
	,	spacing_open:			10  // ALL panes
	,	spacing_closed:			10 // ALL panes
	,	center__onresize:		"innerLayout.resizeAll"
	}); 

	innerLayout = $('div.middle-center').layout({ 
		center__paneSelector:	".inner-center" 
	,	west__paneSelector:		".inner-west" 
	,	east__paneSelector:		".inner-east" 
	,	south__size: 			"275"
	,	south__initClosed:		true 
	,	spacing_open:			10  // ALL panes
	,	spacing_closed:			10  // ALL panes
	}); 

	$("#tabs").tabs();
	$("#breadCrumb3").jBreadCrumb();		
	$("#filters").accordion();
	$("#accordion").accordion({ header: "h3" });
	$("#addforms").tabs();
	$('#tabs, #listform').tabs();
	$("#pubstatus").buttonset();
	$("#editstatus").buttonset();

	$('#radio5').click(function(){
	  middleLayout.close('east');
	  innerLayout.close('south');		  
	  $('.container').removeClass('editable');
	});
	$('#radio6').click(function(){
		middleLayout.open('east');
		innerLayout.open('south');		
		$('.container').addClass('editable');
	});

	$('input[name="radio_status"]').each(function(){
		$(this).click(function(){
			formulary_id	=	$('#id').val();
			publish_status	=	$(this).val();
			update_status(formulary_id,publish_status);
			return false;
		});		
	});

});   
 
 tinyMCE.init({
			mode : "specific_textareas",
			theme : "advanced",
			editor_selector : "mceEditor",
			width:"700",	
			height:"500",
			relative_urls : true, 
			remove_script_host : false, 
			document_base_url : "http://pd2010/", 
			convert_urls : false,
			plugins : "safari,spellchecker,style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,pagebreak,table",
			theme_advanced_buttons1_add_before : "save,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect,emotions,iespell",
			theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
			theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
			theme_advanced_buttons3_add_before : "spellchecker,separator,styleprops,|,template,blockquote,|,insertfile,insertimage,",
			theme_advanced_buttons3_add : "tablecontrols",
			table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
			table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
			table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
			table_cell_limit : 100,
			table_row_limit : 5,
			table_col_limit : 5,
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			content_css : "/css/formulary.css",
			plugin_insertdate_dateFormat : "%Y-%m-%d",
			plugin_insertdate_timeFormat : "%H:%M:%S",
			external_link_list_url : "example_data/example_link_list.js",
			external_image_list_url : "example_data/example_image_list.js",
			flash_external_list_url : "example_data/example_flash_list.js",
			template_external_list_url : "example_data/example_template_list.js",
			theme_advanced_resize_horizontal : false,
			theme_advanced_resizing : true,
			apply_source_formatting : true,
			spellchecker_languages : "+English=en"		
	});

// Function to insert tags into textarea //	

function mySimpleHtmlInserter(insertHtml){
	if(tinyMCE.activeEditor){		
		  tinyMCE.getInstanceById(tinyMCE.activeEditor.id).getWin().focus();
		  tinyMCE.execCommand('mceInsertContent',false,insertHtml);
	}
}

function edit_article(block_type,formulary_id,block_id){
	$('.dialog_article').dialog('open');
}

function update_status(formulary_id,publish_status){
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "formulary=" + formulary_id+"&status="+publish_status+"&action=update_status&s=" +ran_number,
		   success: function(response){			 
			 if(response=='S'){	
				
			 }    
		  }
	 });
	 return false;
}