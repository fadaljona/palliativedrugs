/*
 * Facebox (for jQuery)
 * version: 1.2 (05/05/2008)
 * @requires jQuery v1.2 or later
 * 
 * Usage:
 *  
 *  jQuery(document).ready(function() {
 *    jQuery('a[rel*=facebox]').facebox() 
 *  })
 *
 *  <a href="#terms" rel="facebox">Terms</a>
 *    Loads the #terms div in the box
 *
 *  <a href="terms.html" rel="facebox">Terms</a>
 *    Loads the terms.html page in the box
 *
 *  <a href="terms.png" rel="facebox">Terms</a>
 *    Loads the terms.png image in the box
 *
 *
 *  You can also use it programmatically:
 * 
 *    jQuery.facebox('some html')
 *
 *  The above will open a facebox with "some html" as the content.
 *    
 *    jQuery.facebox(function($) { 
 *      $.get('blah.html', function(data) { $.facebox(data) })
 *    })
 *
 *  The above will show a loading screen before the passed function is called,
 *  allowing for a better ajaxy experience.
 *
 *  The facebox function can also display an ajax page or image:
 *  
 *    jQuery.facebox({ ajax: 'remote.html' })
 *    jQuery.facebox({ image: 'dude.jpg' })
 *
 *  Want to close the facebox?  Trigger the 'close.facebox' document event:
 *
 *    jQuery(document).trigger('close.facebox')
 *
 *  Facebox also has a bunch of other hooks:
 *
 *    loading.facebox
 *    beforeReveal.facebox
 *    reveal.facebox (aliased as 'afterReveal.facebox')
 *    init.facebox
 *
 *  Simply bind a function to any of these hooks:
 *
 *   $(document).bind('reveal.facebox', function() { ...stuff to do after the facebox and contents are revealed... })
 *
 */
 var orderIndex;
(function($) {
  $.facebox = function(data, klass) {
    $.facebox.loading()
    if (data.ajax) fillFaceboxFromAjax(data.ajax)
    else if (data.image) fillFaceboxFromImage(data.image)
    else if (data.div) fillFaceboxFromHref(data.div)
    else if ($.isFunction(data)) data.call($)
    else $.facebox.reveal(data, klass)
  }

  /*
   * Public, $.facebox methods
   */

  $.extend($.facebox, {
    settings: {
      opacity      : 0,
      overlay      : true,
      loadingImage : '/logincms/assets/images/loading.gif',
      closeImage   : '/logincms/assets/images/closelabel.gif',
      imageTypes   : [ 'png', 'jpg', 'jpeg', 'gif' ],
      faceboxHtml  : '\
    <div id="facebox" style="display:none;"> \
      <div class="popup"> \
        <table> \
          <tbody> \
            <tr> \
              <td class="tl"/><td class="b"/><td class="tr"/> \
            </tr> \
            <tr> \
              <td class="b"/> \
              <td class="body"> \
                <div class="content"> \
                </div> \
                <div class="footer"> \
                  <a href="#" class="close"> \
                    <img src="/logincms/assets/images/closelabel.gif" title="close" class="close_image" /> \
                  </a> \
                </div> \
              </td> \
              <td class="b"/> \
            </tr> \
            <tr> \
              <td class="bl"/><td class="b"/><td class="br"/> \
            </tr> \
          </tbody> \
        </table> \
      </div> \
    </div>'
    },

    loading: function() {
      init()
      if ($('#facebox .loading').length == 1) return true
      showOverlay()

      $('#facebox .content').empty()
      $('#facebox .body').children().hide().end().
        append('<div class="loading"><img src="'+$.facebox.settings.loadingImage+'"/></div>')

      /*$('#facebox').css({
        top:	getPageScroll()[1] + (getPageHeight() / 10),
        left:	385.5
      }).show()*/

	  $('#facebox').css({
		  top:    getPageScroll()[1] + (getPageHeight() / 10),
		  left:   ($(window).width() - $('#facebox').width()) / 2
	  }).show()


      $(document).bind('keydown.facebox', function(e) {
        if (e.keyCode == 27) $.facebox.close()
        return true
      })
      $(document).trigger('loading.facebox')
    },

    reveal: function(data, klass) {
	  $(document).trigger('beforeReveal.facebox')
      if (klass) $('#facebox .content').addClass(klass)
      $('#facebox .content').append(data)
      $('#facebox .loading').remove()
      $('#facebox .body').children().fadeIn('normal')
      $('#facebox').css('left', $(window).width() / 2 - ($('#facebox table').width() / 2))
      $(document).trigger('reveal.facebox').trigger('afterReveal.facebox')
    },
    close: function(data) {	  
	 	if(orderIndex>0){
		 window.location.reload(true);		 
		}else{
		   $(document).trigger('close.facebox');
	      return false;		 	
		}
    }
  })

  /*
   * Public, $.fn methods
   */

  $.fn.facebox = function(settings) {
    init(settings)

    function clickHandler() {
      $.facebox.loading(true)

      // support for rel="facebox.inline_popup" syntax, to add a class
      // also supports deprecated "facebox[.inline_popup]" syntax
      var klass = this.rel.match(/facebox\[?\.(\w+)\]?/)
      if (klass) klass = klass[1]

      fillFaceboxFromHref(this.href, klass)
      return false
    }

    return this.click(clickHandler)
  }

  /*
   * Private methods
   */

  // called one time to setup facebox on this page
  function init(settings) {
    if ($.facebox.settings.inited) return true
    else $.facebox.settings.inited = true

    $(document).trigger('init.facebox')
    makeCompatible()

    var imageTypes = $.facebox.settings.imageTypes.join('|')
    $.facebox.settings.imageTypesRegexp = new RegExp('\.' + imageTypes + '$', 'i')

    if (settings) $.extend($.facebox.settings, settings)
    $('body').append($.facebox.settings.faceboxHtml)

    var preload = [ new Image(), new Image() ]
    preload[0].src = $.facebox.settings.closeImage
    preload[1].src = $.facebox.settings.loadingImage

    $('#facebox').find('.b:first, .bl, .br, .tl, .tr').each(function() {
      preload.push(new Image())
      preload.slice(-1).src = $(this).css('background-image').replace(/url\((.+)\)/, '$1')
    })	
    $('#facebox .close').click($.facebox.close)
    $('#facebox .close_image').attr('src', $.facebox.settings.closeImage)
  }
  
  // getPageScroll() by quirksmode.com
  function getPageScroll() {
    var xScroll, yScroll;
    if (self.pageYOffset) {
      yScroll = self.pageYOffset;
      xScroll = self.pageXOffset;
    } else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
      yScroll = document.documentElement.scrollTop;
      xScroll = document.documentElement.scrollLeft;
    } else if (document.body) {// all other Explorers
      yScroll = document.body.scrollTop;
      xScroll = document.body.scrollLeft;	
    }
    return new Array(xScroll,yScroll) 
  }

  // Adapted from getPageSize() by quirksmode.com
  function getPageHeight() {
    var windowHeight
    if (self.innerHeight) {	// all except Explorer
      windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
      windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
      windowHeight = document.body.clientHeight;
    }	
    return windowHeight
  }

  // Backwards compatibility
  function makeCompatible() {
    var $s = $.facebox.settings

    $s.loadingImage = $s.loading_image || $s.loadingImage
    $s.closeImage = $s.close_image || $s.closeImage
    $s.imageTypes = $s.image_types || $s.imageTypes
    $s.faceboxHtml = $s.facebox_html || $s.faceboxHtml
  }

  // Figures out what you want to display and displays it
  // formats are:
  //     div: #id
  //   image: blah.extension
  //    ajax: anything else
  function fillFaceboxFromHref(href, klass) {
    // div
    if (href.match(/#/)) {
      var url    = window.location.href.split('#')[0]
      var target = href.replace(url,'')
      $.facebox.reveal($(target).clone().show(), klass)

    // image
    } else if (href.match($.facebox.settings.imageTypesRegexp)) {
      fillFaceboxFromImage(href, klass)
    // ajax
    } else {
      fillFaceboxFromAjax(href, klass)
    }
  }

  function fillFaceboxFromImage(href, klass) {
    var image = new Image()
    image.onload = function() {
      $.facebox.reveal('<div class="image"><img src="' + image.src + '" /></div>', klass)
    }
    image.src = href
  }

  function fillFaceboxFromAjax(href, klass) {	
	orderIndex	=	href.indexOf('cat_order');
	relatedIndex=	href.indexOf('select_related');	
	$.get(href, function(data) { $.facebox.reveal(data, klass);});	
	if(orderIndex==-1 && relatedIndex==-1){
		testvar=0;
		filname	=	href.indexOf('select_images.php');
		if(filname>0){
			getURLParameters(href); 
			var valid	=	selnumber+'id';
			getvalue	=	document.getElementById(valid).value;			
			setTimeout(
				function test(){
					$('#select_pattern').layout({ applyDefaultStyles: true,west__size:'300px',west__minSize:100,north__size:'20%' ,south__size:'auto',north__showOverflowOnHover:true,south__closable:false,south__resizable:false,south__slidable:false});
					$("#sel_tag").autoSuggest("/logincms/includes/orderajax.php", {minChars: 2,extraParams:"&search_tags=1"});

					$("#sel_kword").autoSuggest("/logincms/includes/orderajax.php", {minChars: 2,extraParams:"&search_asset=1&uploadcat="+seluploadcat,
					selectionAdded: function(elem){ 						
						var full	=elem.html().split("</a>");
						showList(selnumber,selform,selcategory,seluploadcat,seluniqueID,"1",getvalue,full[1]);
					},
					selectionRemoved: function(elem){ 
						elem.remove();
						showList(selnumber,selform,selcategory,seluploadcat,seluniqueID,"1",getvalue,"");
					},
					resultClick: function(data){ 
										
					}});
				},2000);				

			setTimeout('showList(selnumber,selform,selcategory,seluploadcat,seluniqueID,"1",getvalue,"")',2000);			
			setTimeout(
				function temp(){
					$("#uploadify").uploadify({
						'uploader'       : '/logincms/assets/js/uploadify.swf',
						'script'         : '/logincms/includes/swfupload.php',				
						'cancelImg'      : '/logincms/assets/images/cancel.png',
						'folder'         : 'uploads',
						'queueID'        : 'fileQueue',
						'auto'           : true,
						'multi'          : true,
						'scriptData'	 : {'kohanasession':sess_id,'category':selcategory,'uploadcat':seluploadcat},
						onComplete: function (evt, queueID, fileObj, response, data) {								
							$('#patternform').clearForm();
							$('.as-selection-item').remove();
							showList(selnumber,selform,selcategory,seluploadcat,seluniqueID,"1",getvalue,"");
						},onCancel: function (evt, queueID, fileObj, data){				   
						},onSelectOnce: function (evt,data) {
							$('#patternform .error').html('');
							if($('#txtTitle').val()==''){
								$("#uploadify").uploadifyClearQueue();
								$('#patternform .error').html('<ul><li>Please insert title</li></ul>');
							}else if($('#txtAlt').val()==''){
								$("#uploadify").uploadifyClearQueue();
								$('#patternform .error').html('<ul><li>Please insert alt</li></ul>');
							}else if($('.as-selections').children("li").length=='1'){
								$("#uploadify").uploadifyClearQueue();
								$('#patternform .error').html('<ul><li>Please insert tags</li></ul>');
							}			
						}				
					});
				},2000);
		}		
	}else if(orderIndex>0 && relatedIndex==-1){
		setTimeout('loadSortables()',2000);			
	}else if(relatedIndex>0){
		setTimeout(
			function temp(){
				$('#related_tabs').tabs();
				$("a.button", "#related_tabs").button();
				prelatedtable	=	$('#relatedpages').dataTable({ 		
						"bStateSave": true,
						"bPaginate": false,
						"bLengthChange": 100,
						"bFilter": true,
						"bSort": true,
						"bInfo": true,
						"bAutoWidth": false,
						"bProcessing": true,
						"bServerSide": true,
						"sAjaxSource": "/logincms/includes/datatable-ajax.php",
						"aoColumns": [ {"bVisible":    false},
							null,
							null,
							null,
							null,
							null,
							null],
						"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
							if ( jQuery.inArray(aData[0], pageSelected) != -1 )
							{
								$(nRow).addClass('row_selected');
							}
							return nRow;
						},					
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							/* Add some extra data to the sender */
							aoData.push( {"name":"filter_type","value":"relatedpages"} );
							$.ajax({
								"dataType": 'json', 
								"type": "POST", 
								"url": sSource, 
								"data": aoData, 
								"success": fnCallback
							});			
						}	   	
					});
					$('#relatedpages tbody tr').live('click', function () {
							var aData = prelatedtable.fnGetData( this );
							var iId   = aData[0];
							var pname = aData[1];
							
							if ( jQuery.inArray(iId, pageSelected) == -1 )
							{
								pageSelected[pageSelected.length++] = iId;
								pname	=	pname.replace(/-/g,"");
								pageNames[pageNames.length++]	= pname;

							}
							else
							{
								pageSelected = jQuery.grep(pageSelected, function(value) {
									return value != iId;
								} );
								pageNames = jQuery.grep(pageNames, function(value) {
									pname	=	pname.replace(/-/g,"");
									return value != pname;
								} ); 
							}
							
							$(this).toggleClass('row_selected');

						} );
				nrelatedtable	=	$('#relatednews').dataTable({ 		
					"bStateSave": true,
					"bPaginate": true,
					"iDisplayLength":15,
					"sPaginationType": "full_numbers",
					"bLengthChange": 100,
					"bFilter": true,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false,
					"bProcessing": true,
					"bServerSide": true,
					"sAjaxSource": "/logincms/includes/datatable-ajax.php",
					"iSortingCols": 6,
					"aaSorting": [ [2,'desc'] ],
					"aoColumns": [ {"bVisible":    false},
						null,
						null,
						null,
						null,
						null],
					"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
						if ( jQuery.inArray(aData[0], newsSelected) != -1 )
						{
							$(nRow).addClass('row_selected');
						}
						return nRow;
					},					
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						/* Add some extra data to the sender */
						aoData.push( {"name":"filter_type","value":"relatednews"} );
						$.ajax({
							"dataType": 'json', 
							"type": "POST", 
							"url": sSource, 
							"data": aoData, 
							"success": fnCallback
						});			
					}	   	
				});
				$('#relatednews tbody tr').live('click', function () {
						var aData = nrelatedtable.fnGetData( this );
						var iId = aData[0];
						var nname= aData[1];
						
						if ( jQuery.inArray(iId, newsSelected) == -1 )
						{
							newsSelected[newsSelected.length++] = iId;
							newsNames[newsNames.length++] = nname;
						}
						else
						{
							newsSelected = jQuery.grep(newsSelected, function(value) {
								return value != iId;
							} );
							newsNames = jQuery.grep(newsNames, function(value) {									
								return value != nname;
							} );
						}
						
						$(this).toggleClass('row_selected');

					} );
					
			},1000);		
	}
}

  function skipOverlay() {
    return $.facebox.settings.overlay == false || $.facebox.settings.opacity === null 
  }

  function showOverlay() {
    if (skipOverlay()) return

    if ($('facebox_overlay').length == 0) 
      $("body").append('<div id="facebox_overlay" class="facebox_hide"></div>')

    $('#facebox_overlay').hide().addClass("facebox_overlayBG")
      .css('opacity', $.facebox.settings.opacity)
      .click(function() { $(document).trigger('close.facebox') })
      .fadeIn(200)
    return false
  }

  function hideOverlay() {
    if (skipOverlay()) return

    $('#facebox_overlay').fadeOut(200, function(){
      $("#facebox_overlay").removeClass("facebox_overlayBG")
      $("#facebox_overlay").addClass("facebox_hide") 
      $("#facebox_overlay").remove()
    })
    
    return false
  }	 

  /*
   * Bindings
   */

  $(document).bind('close.facebox', function() {
    $(document).unbind('keydown.facebox')
    $('#facebox').fadeOut(function() {
      $('#facebox .content').removeClass().addClass('content')
      hideOverlay()
      $('#facebox .loading').remove()
    })
  })  

})(jQuery);
