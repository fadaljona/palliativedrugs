$(document).ready(function () {	 
	 update_reference();
	 formtable	=	$('#formularylist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": false,
		"sPaginationType": "full_numbers",		
		"bFilter": false,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"formulary"},{"name":"search_type","value":$('#search_type').val()} ,{"name":"search_level","value":$('#search_level').val()} ,{"name":"search_lang","value":$('#search_lang').val()}  );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});

	 pubmedtable	=	$('#pubmedlist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": true,
		"sPaginationType": "full_numbers",		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"pubmeds"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});

	supplytable	=	$('#supplylist').dataTable({ 				
		"bPaginate": true,
		"sPaginationType": "full_numbers",		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"supply"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});

	snippettable	=	$('#snippetlist').dataTable({ 			
		"bPaginate": true,
		"sPaginationType": "full_numbers",		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"tables"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});

	formularytable	=	$('#lformlist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": false,
		"sPaginationType": "full_numbers",		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"linkformulary"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});

	$("#formulary_tags").autoSuggest("/logincms/includes/orderajax.php", {minChars: 2,asHtmlID:$('#id').val(),resultsHighlight:true,preFill:$('#formulary_tags').val(),extraParams:"&action=search_ftags"});	

});

// Validate article ==============================================

function validate_formulary(action){
	flag=true;
	var message		= '';
	if($('#formulary_title').val()==''){
	   message += '<li>Please enter title</li>';
	   flag= false;
	}	
   if(flag==true){
	   $('#action').val(action);
	   $('.form_description').find('.error').html('');
	   $('#addformulary').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}

function showBlock(chkbox,divid){	
   if(chkbox.checked==true){
	  $(divid).show();
   }else{
	  $(divid).hide();
   }
}

function show_form(ftype,flevel,flang){	
	if(ftype!=0 && ftype!=''){
		acstat	=	'#fcon_'+ftype;
		$('#search_type').val(ftype);		
		$('#form_type a').removeClass('selected');		
		$(acstat).find('a').addClass('selected');		
	}
	if(flevel!=0 && flevel!=''){
		acfeat	=	'#flev_'+flevel;		
		$('#search_level').val(flevel);		
		$('#form_level a').removeClass('selected');	
		acfeat  =   acfeat.replace(" ", "_");
		$(acfeat).find('a').addClass('selected');		
	}
	if(flang!=0 && flang!=''){
		acspec	=	'#flang_'+flang;
		$('#search_lang').val(flang);		
		$('#form_lang a').removeClass('selected');		
		$(acspec).find('a').addClass('selected');		
	}	
	formtable.fnDraw();
}

function labelDE(langid){	
	if(langid==2){
		$('#bnfref').hide();
		//$('.delabel').show();
		$('.delabel').addClass("active");
		if($('#id').val()==''){
		   $('#intro_title').val('Einführung');
		   $('#class_title').val('Stoffgruppe');
		   $('#indication_title').val('Indikationen');
		   $('#pharma_title').val('Pharmakologie');
		   $('#cautions_title').val('Warnhinweise');
		   $('#undesireable_title').val('Nebenwirkungen');
		   $('#dose_title').val('Dosierung mit Art und Dauer der Anwendung');
		   $('#over_title').val('Überdosis');
		   $('#death_title').val('Röcheln');
		   $('#supply_title').val('Verfügbare Fertigarzneimittel (Auswahl)');
		   $('#post_title').val('Inhalt nach Referenzen');
		}
	}else{
		$('#bnfref').show();
		//$('.delabel').hide();
		$('.delabel').removeClass("active");
		if($('#id').val()==''){
		   $('#intro_title').val('Introduction');
		   $('#class_title').val('Class');
		   $('#indication_title').val('Indication');
		   $('#pharma_title').val('Pharmacology');
		   $('#cautions_title').val('Cautions');
		   $('#undesireable_title').val('Undesireable Effects');
		   $('#dose_title').val('Dose And Use');
		   $('#over_title').val('Overdose');
		   $('#death_title').val('Death Rattle');
		   $('#supply_title').val('Supply');
		   $('#post_title').val('Content after References');
		}
	}
}

function openWindow(url){
	window.open(url,'Preview','menubar=0, status=0, scrollbars=1, resizable=1,top=100,left=300,width=1000,height=600,location=0,directories=0,toolbar=0');
}

function insert_dialog(pmid){
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "pmid=" + escape(pmid)+"&action=insert_pmid&s=" +ran_number,
		   success: function(response){
			 var getOP	 =	response.split("^");
			 if(getOP[1]	==	'S'){		
				$('#dialog ol').append(getOP[0]);				
			 }    
		  }
	 });
}

function update_reference(){  
	if($('#id').val()=='')return;
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "formulary=" + escape($('#id').val())+"&action=update_reference&s=" +ran_number,
		   success: function(response){
			 var getOP	 =	response.split("^");
			 if(getOP[1]	==	'S'){		
				$('#dialog ol').html(getOP[0]);				
			 }    
		  }
	 }); 
}

function refresh_dialog(){	
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	tinyMCE.triggerSave();
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: $('#addformulary').serialize()+"&action=refresh_dialog&s=" +ran_number,
		   success: function(response){
			 var getOP	 =	response.split("^");
			 if(getOP[1]	==	'S'){		
				$('#dialog ol').html(getOP[0]);				
			 }    
		  }
	 }); 
}

function assign_link(linkval){
	var selectedtext	=	tinyMCE.activeEditor.selection.getContent({format : 'text'});
	tinyMCE.activeEditor.execCommand('mceReplaceContent',false,'<a href="'+linkval+'">{$selection}</a>');
}