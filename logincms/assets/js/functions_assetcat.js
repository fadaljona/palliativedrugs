$(document).ready(function () {
	 assetcattable	=	$('#assetcatlist').dataTable({ 			
		"bStateSave": true,
		"bPaginate": true,		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"sPaginationType": "full_numbers",		
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"assetcats"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Validate asset category ==============================================

function validate_assetcat(){
	flag=true;
	var message		= '';
	if($('#asset_category_title').val()==''){
	   message += '<li>Please enter title</li>';
	   flag= false;
	}
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addassetcat').submit();
   }else{		   
		$('#addassetcat').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}