// Function to validate login administrator -----------------------------------

function validate_login(){
	flag=true;
	if($('#first_name').val()=='' || $('#last_name').val()==''){
	   $('#fname').addClass('error');
	   $('#name_error').show();
	   flag=false;
	}else{
	   $('#fname').removeClass('error');
	   $('#name_error').hide();
	}
	if($('#username').val()==''){
	   $('#uname').addClass('error');
	   $('#uname_error').show();
	   flag=false;
	}else{
	   $('#uname').removeClass('error');
	   $('#uname_error').hide();
	}
	if($('#password').val()==''){
	   $('#pwd').addClass('error');
	   $('#pwd_error').show();
	   flag=false;
	}else{
	   $('#pwd').removeClass('error');
	   $('#pwd_error').hide();
	}
	if(flag==true){
	    var ran_unrounded	=	Math.random()*100000;
		var ran_number		=	Math.floor(ran_unrounded); 
		var action			=	'validate_login';
		var fname			=	$('#first_name').val();
		var lname			=	$('#last_name').val();
		var uname			=	$('#username').val();
		var pwd				=	$('#password').val();
		var url_params		=	"fname="+fname+"&lname="+lname+"&uname="+uname+"&pwd="+pwd+"&action="+action+"&s="+ran_number;
		$.ajax({
			type	:	'POST',
			url		:	'/logincms/includes/orderajax.php',
			data	:	url_params,
			error	:	function(){alert('Error loading document!');},
			success	:	function(msg){ 				
				if(msg!=""){
					if(msg=='IN'){
					    $('#fname').addClass('error');
						$('#name_error').show();
					}else if(msg=='IU'){
						$('#uname').addClass('error');
						$('#uname_error').show();
					}else if(msg=='IP'){
						$('#pwd').addClass('error');
						$('#pwd_error').show();
					}else if(msg=='S'){						
						$('#login_form').submit();
					}
				}
			}
		});
	}			   
}