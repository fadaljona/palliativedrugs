$(document).ready(function () {
	
});

// Function to insert / update the user types ----------------------------------------

function showMenuItems(show){
	if(show==0){
	  $('.menu_items').hide();
	}else{
	  $('.menu_items').show('slow');
	}
}

function showSchedule(show){
	if(show==0){
	  $('.schedule').hide();
	}else{
	  $('.schedule').show('slow');
	}
}
function showGroup(show){
	if(show==0){
	  $('.group').hide();
	}else{
	  $('.group').show('slow');
	}
}

function assignTemplate(tempid, tempname){
  $("#temp_id").val(tempid);
  $("#txt_template").val(tempname);
}

function assignArticle(articleid,article){
  $("#article_id").val(articleid);
  $("#txt_article").val(article);
}

function showArticleTabs(){
  $('#article_id').val('');
  $('#txt_article').val('');
  $('#savePage').hide();
  $('#tgeneral').show();
  $('#tcontent').show();
  $('#timages').show();
  $('#tmeta').show();
  $('#new_arttab').show();
}
function hideArticleTabs(){
  $('#savePage').show();
  $('#tgeneral').hide();
  $('#tcontent').hide();
  $('#timages').hide();
  $('#tmeta').hide();
  $('#new_arttab').hide();
}

function clearArticle(){
  $('#article_id').val('');
  $('#txt_article').val('');
}

function clearTemplate(){
  $('#temp_id').val('');
  $('#txt_template').val('');
}

function checkURL(page_url){	
	if(page_url=='')return;
	var ran_unrounded	=	Math.random()*100000;
	var ran_number		=	Math.floor(ran_unrounded); 
	var action			=	'check_url';
	var url_params		=	"page_url="+page_url+"&action="+action+"&s="+ran_number;
	$.ajax({
		type	:	'POST',
		url		:	'/logincms/includes/orderajax.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 				
			if(msg!=""){
				$('#info').html(msg);				
				if(msg.search("Warning")>=0 && $('#id').val()=='')
					$('#cat_url').val('');
			}else{
				//alert('Please try again!');
			}
		}
	});

}

function isUrl(s) {
	var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	return regexp.test(s);
}

function validate_page(){
	flag=true;	
	var message		= '';
	if($('#cat_title').val()==''){
	   message += '<li>Please enter page title</li>';
	   flag= false;
	}
	if($('#menu_item').val()=='1' && $('#menu_title').val()==''){
	   message += '<li>Please enter menu title</li>';
	   flag= false;
	}
	if($('#cat_url').val()=='' ){
	   message += '<li>Please enter url</li>';
	   flag= false;
	}
	if($('#cat_url').val()!='' && isUrl($('#cat_url').val())==false && $('#txt_template').val()==''){
	   message += '<li>Please select template</li>';
	   flag= false;
	}	
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#article_title').val('');
	   $('#header_imageid').val('');
	   $('#article_imageid').val(''); 
	   $('#addpage').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}

function validate_newarticle(){
	flag=true;	
	var message		= '';
	if($('#cat_title').val()==''){
	   message += '<li>Please enter page title</li>';
	   flag= false;
	}
	if($('#menu_item').val()=='1' && $('#menu_title').val()==''){
	   message += '<li>Please enter menu title</li>';
	   flag= false;
	}
	if($('#cat_url').val()=='' ){
	   message += '<li>Please enter url</li>';
	   flag= false;
	}	
	if(tinyMCE.getInstanceById('long_text')){
		lcontent	=	escape(tinyMCE.get('long_text').getContent());
	}	
	if($('#article_title').val()==''){
	   message += '<li>Please enter article title</li>';
	   flag= false;
	}
	if(lcontent=='' && $("#txt_template").val()=='article-template' ){
	   message += '<li>Please enter article content</li>';
	   flag= false;
	}
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addpage').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}
