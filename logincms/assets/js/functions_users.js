$(document).ready(function () {
	usertable	=	$('#userlist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": false,
		"bLengthChange": 100,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"users"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Function to insert / update the user ----------------------------------------

function validate_user(){
	flag=true;
	var message		= '';
	if($('#user_firstname').val()==''){
	   message += '<li>Please enter first name</li>';
	   flag= false;
	}if($('#user_lastname').val()==''){
	   message += '<li>Please enter last name</li>';
	   flag= false;
	}if($('#user_email').val()==''){
	   message += '<li>Please enter email</li>';
	   flag= false;
	}if($('#user_username').val()==''){
	   message += '<li>Please enter user name</li>';
	   flag= false;
	}if($('#user_password').val()==''){
	   message += '<li>Please enter password</li>';
	   flag= false;
	}if($('#type_id').val()==''){
	   message += '<li>Please select type</li>';
	   flag= false;
	}
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#adduser').submit();
   }else{		   
		$('#adduser').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}