$(document).ready(function () {
	newstable	=	$('#newslist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": true,
		"iDisplayLength":15,
		"sPaginationType": "full_numbers",
		"bLengthChange": 100,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"iSortingCols": 6,
		"aaSorting": [ [1,'desc'] ],
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"news"},{"name":"search_type","value":$('#search_type').val()} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
	
	$("#news_tags").autoSuggest("/logincms/includes/orderajax.php", {minChars: 2,asHtmlID:$('#id').val(),resultsHighlight:true,preFill:$('#news_tags').val(),extraParams:"&action=search_ntags"});	

});


function show_news(ntype){
   $('#news_type a').removeClass('selected');	
   acstat	=	'#fcon_'+ntype;
   $(acstat).find('a').addClass('selected');	
   $('#search_type').val(ntype);
   newstable.fnDraw();
}


// Function to insert / update the news ----------------------------------------

function validate_news(){	
	flag=true;
	var message		= '';
	if($('#title').val()==''){
		message	+=	'<li>Please enter title.</li>';  
		flag = false;
	}			
	if($('#news_date').val()==''){
		message	+=	'<li>Please choose date.</li>';  
		flag = false;
	}	
   if(flag==true){	   
	   $('.form_description').find('.error').html('');
	   $('#addnews').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}

function assign_related(){	
	 page_string = pageNames.toString();
	 news_string = newsNames.toString();
	 if(page_string!='')
	 $('#span_page').html("<label>Pages</label>"+page_string);
	 if(news_string!='')
	 $('#span_news').html("<label>News</label>"+news_string);
	 $('#related_pages').val(pageSelected);
	 $('#related_news').val(newsSelected);
	 $.facebox.close(); 
}
function clear_items(){
	 $('#span_page').html("");
	 $('#span_news').html("");
	 $('#related_pages').val('');
	 $('#related_news').val('');  
	 newsSelected =  [];
	 pageSelected =  [];
	 newsNames	 =	[];
	 pageNames	 =	[];
}

// Function assign image to the content

function assignImage(){
  var imgname	=	$('#news_shortimage').val();
  var insertHtml=	'<img src="/assets/images/3/'+imgname+'" />';
  tinyMCE.getInstanceById(tinyMCE.activeEditor.id).getWin().focus();
  tinyMCE.execCommand('mceInsertContent',false,insertHtml);
}