$(document).ready(function () {
	drugtable	=	$('#druglist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": true,
		"sPaginationType": "full_numbers",
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"drugs"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});


function updateDose(cost,dose,drugid){
   var vcost	=	parseFloat(cost);
   var vdose	=	parseFloat(dose);
   var uval		=	parseFloat(vcost/vdose);
   cdoseid	=	'#cdose'+drugid;   
   if(cost !='' && dose !=''){
	   $(cdoseid).html(uval.toFixed(2));
   }
}

function updateDay(pdose,uprice,drugid){
	costid		=	'#txtcost'+drugid;
	doseid		=	'#txtdose'+drugid;
	costval		=	 $(costid).val();
	doseval		=	 $(doseid).val();
	var vpdose	=	parseFloat(pdose);
	var vuprice	=	parseFloat(costval)/parseFloat(doseval);
	var vcal	=	parseFloat(vpdose*vuprice*28);
	ccal		=	'#dcal'+drugid;
	if(pdose !='' && uprice !=''){
	   $(ccal).html(vcal.toFixed(2));
    }
}

function drugUpdate(cost,dose,drugid){
   var dcost	=	parseFloat(cost);
   var ddose	=	parseFloat(dose);
   var ran_unrounded=	Math.random()*100000;
   var ran_number	=	Math.floor(ran_unrounded); 
   var posturl		=	"action=drug_update&dcost="+dcost+"&ddose="+ddose+"&drugid="+drugid+"&sid="+ran_number;
   $.ajax({
		type	:	'POST',
		url		:	'/logincms/includes/orderajax.php',	
		data	:	posturl,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){						
		}
	});
}

function dayUpdate(pdose,uprice,drugid){
   var vpdose	=	parseFloat(pdose);
   var vuprice	=	parseFloat(uprice);
   var ran_unrounded=	Math.random()*100000;
   var ran_number	=	Math.floor(ran_unrounded); 
   var posturl		=	"action=day_update&vpdose="+vpdose+"&vuprice="+vuprice+"&drugid="+drugid+"&sid="+ran_number;
   $.ajax({
		type	:	'POST',
		url		:	'/logincms/includes/orderajax.php',	
		data	:	posturl,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){	
			
		}
	});
}

// Function to insert / update the drug ----------------------------------------

function validate_drugs(action){
	flag=true;
	var message		= '';
	if($('#drug_name').val()==''){
		message	+=	'<li>Please enter drug name.</li>';  
		flag = false;
	}			
	if($('#cost_quoted').val()==''){
		message	+=	'<li>Please enter cost quoted.</li>';  
		flag = false;
	}
	if($('#nof_doses').val()==''){
		message	+=	'<li>Please enter number of doses.</li>';  
		flag = false;
	}	
   if(flag==true){
	   $('#action').val(action);
	   $('.form_description').find('.error').html('');
	   $('#adddrugs').submit();
   }else{		   
		$('#adddrugs').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}

// Function to update cost per day on change of days and cost //

function findCost(ecost,edays){
	
	if(edays!='' && edays != 0 && ecost>0){

	  var costpday	=	(ecost/edays);

	  costpday		=	roundNumber(costpday,2);

	  $('#cost_per_udose').val(costpday);
	}else{
	  $('#cost_per_udose').val('');
	}
}

// function to update cost for 28 days treatment //

function findTcost(icost,idose,idays){
	if(icost>0 && idays>0){
	   icost		=	parseFloat(icost/idose);

	   var costtw	=	 (icost*idays)*28;

	   costtw		=	parseFloat(costtw);

	   $('#cost_28days').val(costtw.toFixed(2));
	}else
		$('#cost_28days').val('');
}


