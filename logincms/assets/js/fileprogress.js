/*
	A simple class for displaying file information and progress
	Note: This is a demonstration only and not part of SWFUpload.
	Note: Some have had problems adapting this class in IE7. It may not be suitable for your application.
*/

// Constructor
// file is a SWFUpload file object
// targetID is the HTML element id attribute that the FileProgress HTML structure will be added to.
// Instantiating a new FileProgress object with an existing file will reuse/update the existing DOM elements
function FileProgress(file, targetID,serverData) {
	//alert(serverData);
	this.fileProgressID = file.id;

	this.opacity = 100;
	this.height = 0;

	this.fileProgressWrapper = document.getElementById(this.fileProgressID);
	if (!this.fileProgressWrapper) {
		this.fileProgressWrapper = document.createElement("div");
		this.fileProgressWrapper.className = "progressWrapper";
		this.fileProgressWrapper.id = this.fileProgressID;

		this.fileProgressElement = document.createElement("div");
		this.fileProgressElement.className = "progressContainer";

		var progressCancel = document.createElement("a");
		progressCancel.className = "progressCancel";
		progressCancel.href = "#";
		progressCancel.style.visibility = "hidden";
		progressCancel.appendChild(document.createTextNode(" "));

		var progressText = document.createElement("div");
		progressText.className = "progressName";
		progressText.appendChild(document.createTextNode(file.name));

		var progressBar = document.createElement("div");
		progressBar.className = "progressBarInProgress";

		var progressStatus = document.createElement("div");
		progressStatus.className = "progressBarStatus";
		progressStatus.innerHTML = "&nbsp;";

		this.fileProgressElement.appendChild(progressCancel);
		this.fileProgressElement.appendChild(progressText);
		this.fileProgressElement.appendChild(progressStatus);
		this.fileProgressElement.appendChild(progressBar);

		this.fileProgressWrapper.appendChild(this.fileProgressElement);

		document.getElementById(targetID).appendChild(this.fileProgressWrapper);
	} else {		
		this.fileProgressElement = this.fileProgressWrapper.firstChild;
		//alert(serverData);
		var uptype	=	serverData.split("^");
		if(uptype[1]=='single'){
			if(uptype[0]!=''){
				respond		=	uptype[0].split("~");
				if(respond[0]!='dont have'){
					var progressTest = document.createElement("img");
					progressTest.src = respond[0];
					progressTest.name 	= 'img'+this.fileProgressID;
					this.fileProgressElement.appendChild(progressTest);
				}
				var progressCancel = document.createElement("a");
				progressCancel.className = "progressCancel";
				progressCancel.href = "#";
				progressCancel.style.visibility = "";
				progressCancel.name='del'+respond[1];
				progressCancel.onclick	= function (){
					///////////////////////Code for the deleting Details ////////////////////////////
					var xmlHttpDelAssetDetails = false;
						/*@cc_on @*/
						/*@if (@_jscript_version >= 5)try {  
							xmlHttpDelAssetDetails = new ActiveXObject("Msxml2.XMLHTTP");
						}catch (e) {  
							try {    
								xmlHttpDelAssetDetails = new ActiveXObject("Microsoft.XMLHTTP");
							}catch (e2) {    
							   xmlHttp = false;  
							}
						}@end @*/
						if (!xmlHttpDelAssetDetails && typeof XMLHttpRequest != 'undefined') {
						  xmlHttpDelAssetDetails = new XMLHttpRequest();
						}
						valN	= 	this.name.substring(3);	
						deldiv	=	file.id;
						if ((valN == null) || (valN == "")) return;  
						var ran_unrounded=Math.random()*100000;
						var ran_number=Math.floor(ran_unrounded); 
						var url = "doBkpUpd_assets.php?delAssetID=" + escape(valN)+"&s= " +ran_number;
						xmlHttpDelAssetDetails.open("GET", url, true);  
						xmlHttpDelAssetDetails.onreadystatechange = updFnDel;
						xmlHttpDelAssetDetails.send(null);
						function updFnDel() {  
							if (xmlHttpDelAssetDetails.readyState == 4) {  
								var response = xmlHttpDelAssetDetails.responseText;
								if(response	==	'S'){
									document.getElementById(deldiv).innerHTML='';
								}else{
									alert("Unable to delete.Please try again");
								}
							}
						 }
					 }
					this.fileProgressElement.appendChild(progressCancel);
				}
			}else if(uptype[1]=='zip'){
				var zipdet	=	uptype[0];
				var idaname	=	zipdet.split("~");
				var idArr	=	idaname[0].split("$");		
				var nameArr	=	idaname[1].split("#");		
				document.getElementById(this.fileProgressID).innerHTML='';	
				for(kk=0;kk<idArr.length;kk++){
					imgn	=	nameArr[kk].split("/");
					imgName	=	imgn[4];
					this.fileProgressWrapper = document.createElement("div");
					this.fileProgressWrapper.className = "progressWrapper";
					this.fileProgressWrapper.id = idArr[kk];
			
					this.fileProgressElement = document.createElement("div");
					this.fileProgressElement.className = "progressContainer";
				
									
					var progressText = document.createElement("div");
					progressText.className = "progressName";
					progressText.appendChild(document.createTextNode(imgName));
				
					var progressTest = document.createElement("img");
					progressTest.src = nameArr[kk];
					progressTest.name 	= 'img'+idArr[kk];
					
					var progressDel = document.createElement("a");
					progressDel.className = "progressCancel";
					progressDel.href = "#";
					progressDel.style.visibility = "";
					progressDel.name='del'+idArr[kk];
					progressDel.onclick	= function ()
					{
						///////////////////////Code for the deleting Details ////////////////////////////
						var xmlHttpDelAssetDetails = false;
						/*@cc_on @*/
						/*@if (@_jscript_version >= 5)try {  
						xmlHttpDelAssetDetails = new ActiveXObject("Msxml2.XMLHTTP");
						} 
						catch (e) {  
						try {    
							xmlHttpDelAssetDetails = new ActiveXObject("Microsoft.XMLHTTP");
						}
						catch (e2) {    
						   xmlHttp = false;  
						}
						}@end @*/
						if (!xmlHttpDelAssetDetails && typeof XMLHttpRequest != 'undefined') {
							  xmlHttpDelAssetDetails = new XMLHttpRequest();
						}
						valN	= 	this.name.substring(3);	
						deldiv	=	valN;
						if ((valN == null) || (valN == "")) return;  
						 var ran_unrounded=Math.random()*100000;
						 var ran_number=Math.floor(ran_unrounded); 
						 var url = "doBkpUpd_assets.php?delAssetID=" + escape(valN)+"&s= " +ran_number;
						 xmlHttpDelAssetDetails.open("GET", url, true);  
						 xmlHttpDelAssetDetails.onreadystatechange = updFnDel;
						 xmlHttpDelAssetDetails.send(null);
						 function updFnDel() {  
							if (xmlHttpDelAssetDetails.readyState == 4) {  
								var response = xmlHttpDelAssetDetails.responseText;
								if(response	==	'S'){
									document.getElementById(deldiv).innerHTML='';
								}else{
									alert("Unable to delete.Please try again");
								}
							}
						 }
					 }
					
					this.fileProgressElement.appendChild(progressText);
					this.fileProgressElement.appendChild(progressTest);
					this.fileProgressElement.appendChild(progressDel);
					this.fileProgressWrapper.appendChild(this.fileProgressElement);	
					document.getElementById(targetID).appendChild(this.fileProgressWrapper);	
													
				}
			}
		}
	this.height = this.fileProgressWrapper.offsetHeight;
}
FileProgress.prototype.setProgress = function (percentage) {
	this.fileProgressElement.className = "progressContainer green";
	this.fileProgressElement.childNodes[3].className = "progressBarInProgress";	
	this.fileProgressElement.childNodes[3].style.width = percentage + "%";
};
FileProgress.prototype.setComplete = function () {
	this.fileProgressElement.className = "progressContainer blue";
	this.fileProgressElement.childNodes[3].className = "progressBarComplete";
	this.fileProgressElement.childNodes[3].style.width = "";

	var oSelf = this;
	/*setTimeout(function () {
		oSelf.disappear();
	}, 10000);*/
};
FileProgress.prototype.setError = function () {
	this.fileProgressElement.className = "progressContainer red";
	this.fileProgressElement.childNodes[3].className = "progressBarError";
	this.fileProgressElement.childNodes[3].style.width = "";

	var oSelf = this;
	setTimeout(function () {
		oSelf.disappear();
	}, 5000);
};
FileProgress.prototype.setCancelled = function () {
	this.fileProgressElement.className = "progressContainer";
	this.fileProgressElement.childNodes[3].className = "progressBarError";
	this.fileProgressElement.childNodes[3].style.width = "";

	var oSelf = this;
	setTimeout(function () {
		oSelf.disappear();
	}, 2000);
};
FileProgress.prototype.setStatus = function (status) {
	this.fileProgressElement.childNodes[2].innerHTML = status;
};

// Show/Hide the cancel button
FileProgress.prototype.toggleCancel = function (show, swfUploadInstance) {
	this.fileProgressElement.childNodes[0].style.visibility = show ? "visible" : "hidden";
	if (swfUploadInstance) {
		var fileID = this.fileProgressID;
		this.fileProgressElement.childNodes[0].onclick = function () {
			swfUploadInstance.cancelUpload(fileID);
			return false;
		};
	}
};

// Fades out and clips away the FileProgress box.
FileProgress.prototype.disappear = function () {

	var reduceOpacityBy = 15;
	var reduceHeightBy = 4;
	var rate = 30;	// 15 fps

	if (this.opacity > 0) {
		this.opacity -= reduceOpacityBy;
		if (this.opacity < 0) {
			this.opacity = 0;
		}

		if (this.fileProgressWrapper.filters) {
			try {
				this.fileProgressWrapper.filters.item("DXImageTransform.Microsoft.Alpha").opacity = this.opacity;
			} catch (e) {
				// If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
				this.fileProgressWrapper.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=" + this.opacity + ")";
			}
		} else {
			this.fileProgressWrapper.style.opacity = this.opacity / 100;
		}
	}

	if (this.height > 0) {
		this.height -= reduceHeightBy;
		if (this.height < 0) {
			this.height = 0;
		}

		this.fileProgressWrapper.style.height = this.height + "px";
	}

	if (this.height > 0 || this.opacity > 0) {
		var oSelf = this;
		setTimeout(function () {
			oSelf.disappear();
		}, rate);
	} else {
		this.fileProgressWrapper.style.display = "none";
	}
};