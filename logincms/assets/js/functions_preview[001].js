 $(document).ready(function () {		
		myLayout =	$('body').layout({
			applyDefaultStyles: true,
			north__size:125,
			south__initClosed:true,
			east__initClosed:true,
			closable:true,
			resizable:true,
			slidable:true});

		$('a[rel*=facebox]').facebox({
			loading_image : 'loading.gif',
			close_image   : 'closelabel.gif'
		  });

		//myLayout.addToggleBtn('#radio6', 'south');

		$('#activitytabs').tabs();		
		$(".dialog_article").dialog({autoOpen:false,width:750});
		$("#radio_status").buttonset();
		$("#radio_action").buttonset();
		$('#radio5').click(function(){
		  $.each('south,east'.split(','), function(){myLayout.close(this);});
		  $('.container').removeClass('editmode');
		});
		$('#radio6').click(function(){
		   $.each('south,east'.split(','), function(){myLayout.open(this);});
		   $('.container').addClass('editmode');
		});

});
 
 tinyMCE.init({
			mode : "specific_textareas",
			theme : "advanced",
			editor_selector : "mceEditor",
			width:"700",	
			height:"500",
			relative_urls : true, 
			remove_script_host : false, 
			document_base_url : "http://pd2010/", 
			convert_urls : false,
			plugins : "safari,spellchecker,style,layer,table,save,advhr,advimage,advlink,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,pagebreak,table",
			theme_advanced_buttons1_add_before : "save,separator",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect,emotions,iespell",
			theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,separator,forecolor,backcolor",
			theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
			theme_advanced_buttons3_add_before : "spellchecker,separator,styleprops,|,template,blockquote,|,insertfile,insertimage,",
			theme_advanced_buttons3_add : "tablecontrols",
			table_styles : "Header 1=header1;Header 2=header2;Header 3=header3",
			table_cell_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Cell=tableCel1",
			table_row_styles : "Header 1=header1;Header 2=header2;Header 3=header3;Table Row=tableRow1",
			table_cell_limit : 100,
			table_row_limit : 5,
			table_col_limit : 5,
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			content_css : "/css/formulary.css",
			plugin_insertdate_dateFormat : "%Y-%m-%d",
			plugin_insertdate_timeFormat : "%H:%M:%S",
			external_link_list_url : "example_data/example_link_list.js",
			external_image_list_url : "example_data/example_image_list.js",
			flash_external_list_url : "example_data/example_flash_list.js",
			template_external_list_url : "example_data/example_template_list.js",
			theme_advanced_resize_horizontal : false,
			theme_advanced_resizing : true,
			apply_source_formatting : true,
			spellchecker_languages : "+English=en"		
	});

// Function to insert tags into textarea //	

function mySimpleHtmlInserter(insertHtml){
	if(tinyMCE.activeEditor){		
		  tinyMCE.getInstanceById(tinyMCE.activeEditor.id).getWin().focus();
		  tinyMCE.execCommand('mceInsertContent',false,insertHtml);
	}
}

function edit_article(block_type,formulary_id,block_id){
	$('.dialog_article').dialog('open');
}

function togglepanel(action){

}