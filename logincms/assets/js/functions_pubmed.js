$(document).ready(function () {
	 pubmedtable	=	$('#pubmedlist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": true,
		"sPaginationType": "full_numbers",		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"pubmeds"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Validate pubmed ==============================================

function validate_pubmed(){
	flag=true;
	var message		= '';	
	if($('#pmid').val()==''){
	   message += '<li>Please enter code</li>';
	   flag= false;
	}	
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addpubmed').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}

/////////////////////////// Function to add author for the manual reference ////////////////////////

function addauthor(authid){	
	var authval		=	$('#'+authid).val();	
	if(authval==""){
		alert('Please enter author name!');
		$('#'+authid).focus();
	}else{
		var norows = document.getElementById('authtable').rows.length;
		var x = document.getElementById('authtable').insertRow(norows);
		var a = x.insertCell(0);
		var b = x.insertCell(1);		
		k	=	norows;
		a.innerHTML	=	"<nobr>"+"<input type='hidden' id='txtAuth_"+k+"' name='txtAuth[]' value='"+authval+"' />"+authval+"</nobr>";		
		b.innerHTML	=	"<input type='button' name='btnedit'  value='Edit' onClick='editRow(this.parentNode.parentNode.rowIndex,document.getElementById(\"txtAuth_"+k+"\").value)' class='button'><input type='button' name='btnview'  value='Delete' onClick='deleteRow(this.parentNode.parentNode.rowIndex)' class='button'>";				
		$('#'+authid).val('');
		$('#'+authid).focus();
	}
}

function deleteRow(i){	
	document.getElementById('authtable').deleteRow(i);
	var norows = document.getElementById('authtable').rows.length;
	if(norows<1){
		$('#txt_author').val('');	
		document.getElementById('sbutton').value	=	'Add';
		document.getElementById('sbutton').name		=	'Add';
		document.getElementById('sbutton').onclick	=	function (){addauthor('txt_author');};
	}
}

function editRow(rowno,authvar){
	document.getElementById('txt_author').value	=	authvar;	
	document.getElementById('sbutton').value	=	'Update';
	document.getElementById('sbutton').name		=	'Update';
	document.getElementById('sbutton').onclick	=   function (){updateRow(rowno);};	
}

function updateRow(rowno){
	var authval		=	document.getElementById('txt_author').value;	
	if(authval==""){
		alert('Please enter author name!');
		document.getElementById('txt_author').focus();
	}else{
		var x = document.getElementById('authtable').rows[rowno].cells;
		x[0].innerHTML="<nobr>"+"<input type='hidden' id='txtAuth_"+rowno+"' name='txtAuth[]' value='"+authval+"' />"+authval+"</nobr>";		
		x[1].innerHTML	=	"<input type='button' name='btnedit'  value='Edit' onClick='editRow(this.parentNode.parentNode.rowIndex,document.getElementById(\"txtAuth_"+rowno+"\").value)' class='button'><input type='button' name='btnview'  value='Delete' onClick='deleteRow(this.parentNode.parentNode.rowIndex)' class='button'>";		
		$('#txt_author').val('');	
		document.getElementById('sbutton').value	=	'Add';
		document.getElementById('sbutton').name		=	'Add';
		document.getElementById('sbutton').onclick	=	function (){addauthor('txt_author');};
	}
	
}

/////////////////////////// Function to add editor for the manual reference ////////////////////////

 function addeditor(editid){	
	var editval		=	$('#'+editid).val();	
	if(editval==""){
		alert('Please enter editor name!');
		$('#'+editid).focus();
	}else{
		var norows = document.getElementById('edittable').rows.length;
		var x = document.getElementById('edittable').insertRow(norows);
		var a = x.insertCell(0);
		var b = x.insertCell(1);		
		k	=	norows;
		a.innerHTML	=	"<nobr>"+"<input type='hidden' id='txtEdit_"+k+"' name='txtEdit[]' value='"+editval+"' />"+editval+"</nobr>";		
		b.innerHTML	=	"<input type='button' name='bnedit'  value='Edit' onClick='editERow(this.parentNode.parentNode.rowIndex,document.getElementById(\"txtEdit_"+k+"\").value)' class='button'><input type='button' name='bnview'  value='Delete' onClick='deleteERow(this.parentNode.parentNode.rowIndex)' class='button'>";				
		$('#'+editid).val('');
		$('#'+editid).focus();
	}
}

function deleteERow(i){	
	document.getElementById('edittable').deleteRow(i);
	var norows = document.getElementById('edittable').rows.length;
	if(norows<1){
		$('#txt_editor').val('');	
		document.getElementById('ebutton').value	=	'Add';
		document.getElementById('ebutton').name		=	'Add';
		document.getElementById('ebutton').onclick	=	function (){addeditor('txt_editor');};
	}
}

function editERow(rowno,editvar){
	document.getElementById('txt_editor').value	=	editvar;	
	document.getElementById('ebutton').value	=	'Update';
	document.getElementById('ebutton').name		=	'Update';
	document.getElementById('ebutton').onclick	=   function (){updateERow(rowno);};	
}

function updateERow(rowno){
	var editval		=	document.getElementById('txt_editor').value;	
	if(editval==""){
		alert('Please enter editor name!');
		document.getElementById('txt_editor').focus();
	}else{
		var x = document.getElementById('edittable').rows[rowno].cells;
		x[0].innerHTML="<nobr>"+"<input type='hidden' id='txtEdit_"+rowno+"' name='txtEdit[]' value='"+editval+"' />"+editval+"</nobr>";		
		x[1].innerHTML	=	"<input type='button' name='bnedit'  value='Edit' onClick='editERow(this.parentNode.parentNode.rowIndex,document.getElementById(\"txtEdit_"+rowno+"\").value)' class='button'><input type='button' name='bnview'  value='Delete' onClick='deleteERow(this.parentNode.parentNode.rowIndex)' class='button'>";		
		$('#txt_editor').val('');	
		document.getElementById('ebutton').value	=	'Add';
		document.getElementById('ebutton').name		=	'Add';
		document.getElementById('ebutton').onclick	=	function (){addeditor('txt_editor');};
	}
	
}

// Function to validate manualref ----------------------------------------

function validate_manualref(){	 
	flag=true;
	var message		= '';
	if(tinyMCE.getInstanceById('txtContent')){
		pcontent	=	escape(tinyMCE.get('txtContent').getContent());	
	}else{
		pcontent	=	$('#txtContent').val();
	}
	if(pcontent==''){
		if(trim($('#authtable').html())==''){		
		   message += '<li>Please enter author.</li>';
		   flag= false;
		}if($('#pub_date').val()==''){
		   message += '<li>Please enter year.</li>';
		   flag= false;
		}if($('#title').val()==''){
		   message += '<li>Please enter title.</li>';
		   flag= false;
		}if($('#full_journalname').val()==''){
		   message += '<li>Please enter journal name.</li>';
		   flag= false;
		}if($('#txt_pagesfrom').val()==''){
		   message += '<li>Please enter pages.</li>';
		   flag= false;
		}
	}
	 if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addmanual').submit();
	 }else{		   
	   $('.form_description').find('.error').html('<ul>'+message+'</ul>');
	   window.scrollTo(0,0);
	   return flag;
	 }
}