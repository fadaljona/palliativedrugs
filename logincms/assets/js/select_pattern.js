// JavaScript Document

// To order category 

var offsetYInsertDiv = -3; // Y offset for the little arrow indicating where the node should be inserted.
if(!document.all)offsetYInsertDiv = offsetYInsertDiv - 7; 	// No IE
var arrParent				= false;
var arrMoveCont				= false;
var arrMoveCounter			= -1;
var arrTarget				= false;
var arrNextSibling			= false;
var leftPosArrangableNodes	= false;
var widthArrangableNodes	= false;
var nodePositionsY			= new Array();
var nodeHeights				= new Array();
var arrInsertDiv			= false;
var insertAsFirstNode		= false;
var arrNodesDestination		= false;

// To load swf uploads 
var	testvar;
var upload1, upload2;
var selnumber;
var selform; 
var seleditId; 
var seluniqueID;
var selcategory = 'image-upload';
var seluploadcat;
var sess_id;
var getvalue;
var checkflag = "false";

	
// To save alt,title,keyword in the upload form ----------------------------
	
function save_alt(){	
	var posturl		= '&'  + $('#patternform').serialize();	
	$.ajax({
		type	:	'POST',
		url		:	'/logincms/includes/set_session.php',	
		data	:	posturl,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){
		  if(msg == 'S'){}
		}
	});	 
}

// For assets listing in the first tab -------------------------------------------------------
function showList(pnumber,pform,pcateg,puploadcat,uniid,pagenum,editid,searchval) { 
	if ((puploadcat == null) || (pcateg == "")) return;  
	var ran_unrounded=	Math.random()*100000;
	var ran_number	 =  Math.floor(ran_unrounded); 
	var url			 =  "/logincms/includes/showList.php";
	var posturl		 =	"pnumber=" + escape(pnumber)+"&pform=" +escape(pform)+"&pcateg=" +escape(pcateg)+"&puploadcat=" +escape(puploadcat)+"&puniid=" +escape(uniid)+"&pagenum=" +escape(pagenum)+"&editid=" +escape(editid)+"&searchval=" +escape(searchval)+"&s=" +ran_number;	
	$.ajax({
		type	:	'POST',
		url		:	'/logincms/includes/showList.php',	
		data	:	posturl,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){
		  if(msg != ''){
			var getOP	=	msg.split("^");	
			if(getOP[1]	==	'S'){					
				$('#'+puploadcat).html(getOP[0]);
			}else if(getOP[1]	==	'F'){
				$('#'+puploadcat).html('');
			}
		  }
		}
	});	 
}
	
/******* Function to Fill Image Name ***********/
function FillName(filename,imgid,number,formname,fname){
	var f1	=	eval("document.forms."+formname+"."+fname);
	var f2	=	eval("document.forms."+formname+"."+fname+"id");
	if(f1)
		f1.value = filename;
	if(f2)
		f2.value = imgid;
	$(document).trigger('close.facebox');
}
	
/******* Function to check all assets **********/
	
function checkAll(field) {
	if(field){
		if (checkflag == "false") {
			for (i = 0; i < field.length; i++) {
				field[i].checked = true;
			}
			checkflag = "true";
		  return "Uncheck All";
		} else {
			 for (i = 0; i < field.length; i++) {
				 field[i].checked = false;
			 }
			 checkflag = "false";
			 return "Check All";
		}
	}
 }
 
 /******* Function to insert checked all assets ********/

 function insertAllChecked(){
	var field		=	new Array();
	field			=	document.forms.FrmImgList.elements['chkDel[]'];
	var assetID		=	'';
	var assetName	=	'';
	var disAsset	=	'';
	var totVal		=	'';
	var inc			=	0;
	var frmName		=	document.forms.FrmImgList.form.value;
	var unID		=	document.forms.FrmImgList.uniqueID.value;
	var temp		=	document.forms.FrmImgList.number.value;
	upcat			=	temp+"id";
	catid			=	document.forms.FrmImgList.catID.value;
	tmpname			=	'';
	
	for (i = 0; i < field.length; i++) {
		if( field[i].checked){
			inc++;
			var g	=	field[i].value;
			totVal	=	g.split('#');
			assetID+=	totVal[0]+',';
			tmpname =   totVal[1];
			assetName+=	totVal[1]+'<br />\n';
			disAsset+=   "<img src='/assets/thumbs/"+catid+'/'+tmpname+"'>";
			
		}
	}	
	
	if(tmpname!="" && temp=='product_speclb'){
		showSpec('Bikes');
	}		
	if(unID	==	'multiple'){
	   if(temp == 'press_images'){
			var DownName		=	'asset2';
			var spanName		=	'file_names2';
	   }else{
			var DownName		=	'asset2';
			var spanName		=	temp+'2';
			assetName			=	disAsset;
	   }
	}else{
		var DownName		=	'asset'+unID;
		var spanName		=	'file_names'+unID;
	}	

	if(document.getElementById(upcat)){
		document.getElementById(upcat).value	=	assetID;
	}
	if(document.getElementById(spanName)){
		document.getElementById(spanName).innerHTML	=	assetName;
	}
	$(document).trigger('close.facebox');
}

/********** Function to insert single checked asset ***********/

function insertChecked(){
	for (var i=0; i < (document.forms.FrmImgList.chkRad.length); i++)
  	{	 		 
  		 if (document.forms.FrmImgList.chkRad[i].checked){
			  var rad_val = document.forms.FrmImgList.chkRad[i].value;
			  totVal	=	rad_val.split('#');
			  assetID	=	totVal[0];
			  assetName =	totVal[1];
			  var frmName	=	document.forms.FrmImgList.form.value;
			  var sing		=	document.forms.FrmImgList.uploadcat.value;	
			  sing		=	sing.replace('-','_');
			  sing		=	sing.replace('-','_');
			  var f1	=	document.getElementById(sing);
			  var f2	=	document.getElementById(sing+"id");				  
			  if(f1){
				f1.value	=	assetName;
			  }
			  if(f2){
				f2.value	=	assetID;
			  }
		 }
		 $(document).trigger('close.facebox');
   }
   
}
	
	
// Function to get parameters from url

function getURLParameters(url){
	var sURL = url.toString();

	if (sURL.indexOf("?") > 0){
		var arrParams = sURL.split("?");			
		var arrURLParams = arrParams[1].split("&");		
		var arrParamNames = new Array(arrURLParams.length);
		var arrParamValues = new Array(arrURLParams.length);		
		var i = 0;
		for (i=0;i<arrURLParams.length;i++){
			var sParam =  arrURLParams[i].split("=");
			arrParamNames[i] = sParam[0];
			if (sParam[1] != "")
				arrParamValues[i] = unescape(sParam[1]);
			else
				arrParamValues[i] = "";
		}
		
		for (i=0;i<arrURLParams.length;i++){
			if(arrParamNames[i]=='number'){
				selnumber=arrParamValues[i];
			}
			if(arrParamNames[i]=='form'){
				selform=arrParamValues[i]; 
			}
			if(arrParamNames[i]=='editId'){
				seleditId=arrParamValues[i]; 
			}
			if(arrParamNames[i]=='category'){
				selcategory=arrParamValues[i];
			}
			if(arrParamNames[i]=='uploadcat'){
				seluploadcat=arrParamValues[i]; 
			}
			if(arrParamNames[i]=='uniqueID'){
				seluniqueID=arrParamValues[i]; 
			}		
		}
	}	
}

 function loadSortables() {
	$("#contentLeft ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
		var pages = $("#ActionType").val();
		var order = $(this).sortable("serialize") + '&action=updateRecordsListings' + '&module='+pages; 
		$.post("/logincms/includes/order_ajax.php", order, function(theResponse){
			$("#contentRight").html(theResponse);
		}); 															 
	   }								  
	});
}

   
   $.fn.clearForm = function() {
       return this.each(function() {
   
          var type = this.type, tag = this.tagName.toLowerCase();   
          if (tag == 'form')									    
            return $(':input',this).clearForm();				    
          if (type == 'text' || type == 'password' || tag == 'textarea')   
            this.value = '';
             else if (type == 'checkbox' || type == 'radio')   
            this.checked = false;							  
          else if (tag == 'select')							
            this.selectedIndex = -1;						  
        });
  
      };

