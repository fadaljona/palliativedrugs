$(document).ready(function () {
	if($('body').layout()){
		$('body').layout({ 
			north: {
				size:	"auto",	
				spacing_open:	0,	
				closable:	false,	
				resizable:	false,
			},
			west:{
				applyDefaultStyles: true,
			},
			center: {
				applyDefaultStyles: true,
			}, 
		});
	}
	$("#breadCrumb2").jBreadCrumb({easing:'none'});
	$('.rounded').corners();
    $('.rounded').corners(); /* test for double rounding */
    $('table', $('#featureTabsContainer .tab')[0]).each(function(){$('.native').hide();});
    $('#featureTabsContainer').show(); 
	var $atabs	=	$("#article_tabs").tabs();
	$('#activitytabs, #dashtabs').tabs();
	$("a.button, #view_order").button();
	$("a.button", ".grid tbody").button();
	$("form.appnitro a").button();
	$("#ordersitemap").button();
	$("button:submit", ".tablesearch").button();
	$('.grid tbody').highlight();
	$("form.appnitro a.button").button();
	$("#usersaccordion").accordion(); 
	$('.dialog-confirm').hide();
	$("#dialog").dialog("destroy");
	$('#userslist').dataTable();
	$('.grid tbody tr').each(function(){
		var parent	=	$(this);
	    parent.find('td:lt(2)').click(function(){
			if(parent.find('td:last-child a:eq(0)').attr('href')){
				var url = parent.find('td:last-child a:eq(0)').attr('href');				
				document.location.href = url;	
			}		
	  });
	});
	
	$('.next-tab').click(function() {		
		 var i =$atabs.tabs('option', 'selected');		
		 $atabs.tabs('select', i+1);		
		 return false;
	});

});


function chkall(val){

  if(val.checked==false){
	  selectcheck(val,document.addlevel,false)
   }else if(val.checked==true){
	  selectcheck(val,document.addlevel,true)
   }else{
	  val.checked=false
   }										
}

var checkflag = "false";
 function checkAll(field) {
	 if (checkflag == "false") {
		for (i = 0; i < field.length; i++) {
			 field[i].checked = true;
		 }
		 checkflag = "true";
		 return "Uncheck All";
	 } else {
		 for (i = 0; i < field.length; i++) {
			 field[i].checked = false;
		 }
		 checkflag = "false";
		 return "Check All";
	 }
 }
function selectcheck(element,frm,action){
	name =  element.name;
	var length = name.length;

	var subs = name.substring(length-2,length);

	for (var i=0; i < frm.elements.length; i++){

		if (frm.elements[i].type == 'checkbox'){
			
			if (frm.elements[i].name.substring(frm.elements[i].name.length-2,frm.elements[i].name.length)== subs){
				frm.elements[i].checked=action;
				frm.elements[i].disabled=action;

				if(frm.elements[i].name.charAt(0)=="p")
					frm.elements[i].disabled=false;

				if(frm.elements[i].name.charAt(0)=="f")
					frm.elements[i].disabled=false;
			}
						
		 }
	 }

}

//############################################################### Common functions #####################################################//

// Function to change status //

function change_status(id,tablename,fieldname,status,idf){
  var ran_unrounded	=	Math.random()*100000;
  var ran_number	=	Math.floor(ran_unrounded); 
  var posturl		=   "process=change_status&id="+id+"&t="+tablename+"&f="+fieldname+"&s="+status+"&idf="+idf+"&sid="+ran_number;
  $.ajax({
		type	:	'POST',
		url		:	'/logincms/index.php/common',	
		data	:	posturl,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){
		  if(msg !=''){
			var divID	=	id+tablename;
			var response_text = trim(msg);
			response_text = response_text.split('###');				
			$("#"+divID).html(response_text[0]);
			if(response_text[1] == '0'){
			  $('#'+id).show() ;
			 }else{
			  $('#'+id).hide();
			 }
  		  }
  		}
	});
}

function open_dialog(url){		
	$(".dialog-confirm").dialog({
		resizable: false,
		height:120,
		modal: true,
		buttons: {
			'Cancel': function() {
				$(this).dialog('close');				
			},
			'Ok': function() {
				location.href=url;
				return true;
			}
		}
	});
}


//  Function to delete records //

function delete_record(tablename,fieldname,id) {
  var ran_unrounded	=	Math.random()*100000;
  var ran_number	=	Math.floor(ran_unrounded); 
  var posturl		= 	'process=delete_record&table='+tablename+'&fieldname='+fieldname+'&delete='+id+"&sid="+ran_number;
	$.ajax({
		type		: 'POST',
		url			: '/logincms/modules/common.php',
		data		: posturl,	
		error	:	function(){alert('Error loading document!');},
		success		: function(msg) {
			if(msg=="S"){
				switch(tablename){
					case 'users':
					   user_list();
					break;
					case 'user_types':
					   usertype_list();
					break;
					case 'categories':
					   page_list();
					break;
					case 'articles':
					   article_list();
					break;
					case 'article_template':
					   template_list();
					break;
					case 'snippets':
					   snippet_list();
					break;
					case 'fixtures':
					   fixture_list();
					break;
					case 'player_profiles':
						profile_list();
					break;
					case 'news':
						news_list();
					break;
					case 'asset_category':
						assetcat_list();
					break;
					case 'assets':
						asset_list();
					break;
				 }				
			}
		 }
	});
}

// Function to make feature for news --------------------------------------------
function makeFeature(obj,tablename,fieldname){
  var ran_unrounded	=	Math.random()*100000;
  var ran_number	=	Math.floor(ran_unrounded); 
  var id			=	obj.attr('id').replace('fea_','');  
  if(obj.attr('checked')==true){					   	
	$('.features').attr('checked',false);
	obj.attr('checked',true);    
	var posturl		=   "process=make_feature&id="+id+"&t="+tablename+"&f="+fieldname+"&sid="+ran_number;
	$.ajax({
		type	:	'POST',
		url		:	'/logincms/modules/common.php',	
		data	:	posturl,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){
		  if(msg == 'S'){}
		}
	});
  }
}

// Function to insert tags into textarea //	

function mySimpleHtmlInserter(insertHtml){	   
  tinyMCE.getInstanceById(tinyMCE.activeEditor.id).getWin().focus();
  tinyMCE.execCommand('mceInsertContent',false,insertHtml);
}

// Funtion to round the number //

function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
  var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
  return newnumber; // Output the result to the form field (change for your purposes)
}

// Function to block alphabets //

function blockNumbers(e){
	var key;
	var keychar;
	var reg;			 
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		key = e.keyCode;
	}else if(e.which) {
		// netscape
		key = e.which;
	}else {
		// no event, so pass through
		return true;
	}
	keychar = String.fromCharCode(key);
	
	if ( (key>=48 &&  key<=57) || (key==46) || (key==44)|| (key==45) || (key==34) || (key==39) || (key==32) || (key == 8) || (key == 43) || (key == 41) || (key == 40)){
		return true;
	}else{
		return false;
	}
}

///////////////   To trim the string ////////////////////
function trim(str){
	return str.replace(/^\s*|\s*$/g,"");
}

//////////// Validate email ///////////////////////
var remail=/^([_&a-zA-Z0-9-]+(\.[_&a-zA-Z0-9-]+)*@[&a-zA-Z0-9-]+\.+[&a-zA-Z0-9-]+)/;
function checkEmail(fieldvalue){
	if(remail.test(fieldvalue))
		return false;
	else
		return true;
}

function openOrderPop(url){
	window.open(url,'Order the Pages',"scrollbars=yes,resizable=1,menubar=no,width=400,height=500");
	return;
}

//############################################################### Common functions #################################################//

// Function to insert the snippet tags to template //
function insertSnippetQuery(frmname) {	
    var myQuery = frmname.txtContent;
    var myListBox = frmname.dummy;

    if(myListBox.options.length > 0) {
        var chaineAj = "";
        var NbSelect = 0;
        for(var i=0; i<myListBox.options.length; i++) {
            if (myListBox.options[i].selected){
                NbSelect++;
                if (NbSelect > 1)
                    chaineAj += ", ";
                chaineAj += "{{" + myListBox.options[i].value + "}}";
            }
        }

        //IE support
        if (document.selection) {
            myQuery.focus();
            sel = document.selection.createRange();
            sel.text = chaineAj;
            document.sqlform.insert.focus();
        }
        //MOZILLA/NETSCAPE support
        else if (frmname.txtContent.selectionStart || frmname.txtContent.selectionStart == "0") {
            var startPos = frmname.txtContent.selectionStart;
            var endPos = frmname.txtContent.selectionEnd;
            var chaineSql = frmname.txtContent.value;
	            myQuery.value = chaineSql.substring(0, startPos) + chaineAj + chaineSql.substring(endPos, chaineSql.length);
        } else {
            myQuery.value += chaineAj;
        }
    }
}

function delay(millis)
{
var date = new Date();
var curDate = null;

do { curDate = new Date(); }
while(curDate-date < millis);
} 

$.fn.clearForm = function() {
  return this.each(function() {
 var type = this.type, tag = this.tagName.toLowerCase();
 if (tag == 'form')
   return $(':input',this).clearForm();
 if (type == 'text' || type == 'password' || tag == 'textarea')
   this.value = '';
 else if (type == 'checkbox' || type == 'radio')
   this.checked = false;
 else if (tag == 'select')
   this.selectedIndex = -1;
  });
};

function ClearText(name1,name2){
	name1.value = '';
	name2.value	= '';
 }

function insertSnippet(frm){
	var w = frm.snippetid.selectedIndex;
	var selected_text = "{{"+frm.snippetid.options[w].text+"}}";	
	tinyMCE.getInstanceById(tinyMCE.activeEditor.id).getWin().focus();
	tinyMCE.execCommand('mceInsertContent',false,selected_text);
}

function insertValueQuery(frmname) {
    var myQuery = frmname.message_text;
    var myListBox = frmname.dummy;

    if(myListBox.options.length > 0) {
        var chaineAj = "";
        var NbSelect = 0;
        for(var i=0; i<myListBox.options.length; i++) {
            if (myListBox.options[i].selected){
                NbSelect++;
                if (NbSelect > 1)
                    chaineAj += ", ";
                chaineAj += "{" + myListBox.options[i].value + "}";
            }
        }

        //IE support
        if (document.selection) {
            myQuery.focus();
            sel = document.selection.createRange();
            sel.text = chaineAj;
            document.sqlform.insert.focus();
        }
        //MOZILLA/NETSCAPE support
        else if (frmname.message_text.selectionStart || frmname.message_text.selectionStart == "0") {
            var startPos = frmname.message_text.selectionStart;
            var endPos = frmname.message_text.selectionEnd;
            var chaineSql = frmname.message_text.value;
	            myQuery.value = chaineSql.substring(0, startPos) + chaineAj + chaineSql.substring(endPos, chaineSql.length);
        } else {
            myQuery.value += chaineAj;
        }
    }
}
