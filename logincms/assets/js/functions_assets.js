$(document).ready(function () {
	 assettable	=	$('#assetlist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": true,
		"sPaginationType": "full_numbers",		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"assets"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Validate assets ==============================================

function validate_asset(){
	flag=true;
	var message		= '';
	if($('#asset_title').val()==''){
	   message += '<li>Please enter title</li>';
	   flag= false;
	}
	if($('#asset_alt').val()==''){
	   message += '<li>Please enter alt</li>';
	   flag= false;
	}
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addasset').submit();
   }else{		   
		$('#addasset').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}