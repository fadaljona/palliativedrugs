// JavaScript Document

// To order category 

var offsetYInsertDiv = -3; // Y offset for the little arrow indicating where the node should be inserted.
if(!document.all)offsetYInsertDiv = offsetYInsertDiv - 7; 	// No IE
var arrParent				= false;
var arrMoveCont				= false;
var arrMoveCounter			= -1;
var arrTarget				= false;
var arrNextSibling			= false;
var leftPosArrangableNodes	= false;
var widthArrangableNodes	= false;
var nodePositionsY			= new Array();
var nodeHeights				= new Array();
var arrInsertDiv			= false;
var insertAsFirstNode		= false;
var arrNodesDestination		= false;

// To load swf uploads 
var	testvar;
var upload1, upload2;
var selnumber;
var selform; 
var seleditId; 
var seluniqueID;
var selcategory = 'image-upload';
var seluploadcat;
var sess_id;
var getvalue;
var checkflag = "false";

	function loadswf() {	
		testvar = 1;
		var ran_unrounded=Math.random()*100000;
		var ran_number=Math.floor(ran_unrounded); 
		
		if(selcategory == 'image-upload'){
			filetypes = "*.jpg;*.gif;*.png;*.tiff";
		}else if(selcategory =='asset-upload'){
			filetypes = "*.doc;*.txt;*.xls;*.pdf;*.swf;*.flv;*.mp3";
		}else{
			filetypes = "*.jpg;*.gif;*.png;*.tiff;*.doc;*.txt;*.xls;*.pdf;*.eps;*.mp3,*.mp4";
		}
		
		var settings1 = {
			flash_url : "/logincms/assets/js/swfupload.swf",
			upload_url: "../../includes/swfupload.php?category="+selcategory+"&uploadcat="+seluploadcat+"&s="+ran_number,// Relative to the SWF file
			post_params: {"kohanasession" : sess_id},
			file_size_limit : "100 MB",
			file_types : filetypes,
			file_types_description : "All Files",
			file_upload_limit : 100,
			file_queue_limit  : 0,
			custom_settings : {
				progressTarget : "fsUploadProgress1",
				cancelButtonId : "btnCancel1"
			},
			debug: false,

			// Button settings
			button_image_url: "/logincms/assets/images/button.png",	// Relative to the Flash file
			button_width: "65",
			button_height: "29",
			button_placeholder_id: "spanButtonPlaceHolder1",
			button_text: '<span class="theFont">Upload</span>',
			button_text_style: ".theFont { font-size: 16; }",
			
			// The event handler functions are defined in handlers.js
			file_queued_handler : fileQueued,
			file_queue_error_handler : fileQueueError,
			file_dialog_complete_handler : fileDialogComplete,
			upload_start_handler : uploadStart,
			upload_progress_handler : uploadProgress,
			upload_error_handler : uploadError,
			upload_success_handler : uploadSuccess,
			upload_complete_handler : uploadComplete,
			queue_complete_handler : queueComplete	// Queue plugin event
		};
		
		var settings2 = {
			flash_url : "/logincms/assets/js/swfupload.swf",
			upload_url: "../../includes/zipswfupload.php?category="+selcategory+"&uploadcat="+seluploadcat+"&s="+ran_number,// Relative to the SWF file
			post_params: {"kohanasession" : sess_id},
			file_size_limit : "100 MB",
			file_types : '*.zip',
			file_types_description : "All Files",
			file_upload_limit : 100,
			file_queue_limit  : 0,
			custom_settings : {
				progressTarget : "fsUploadProgress2",
				cancelButtonId : "btnCancel2"
			},
			debug: false,

			// Button settings
			button_image_url: "/logincms/assets/images/button.png",	// Relative to the Flash file
			button_width: "65",
			button_height: "29",
			button_placeholder_id: "spanButtonPlaceHolder2",
			button_text: '<span class="theFont">Upload</span>',
			button_text_style: ".theFont { font-size: 16; }",
			
			// The event handler functions are defined in handlers.js
			file_queued_handler : fileQueued,
			file_queue_error_handler : fileQueueError,
			file_dialog_complete_handler : fileDialogComplete,
			upload_start_handler : uploadStart,
			upload_progress_handler : uploadProgress,
			upload_error_handler : uploadError,
			upload_success_handler : uploadSuccess,
			upload_complete_handler : uploadComplete,
			queue_complete_handler : queueComplete	// Queue plugin event
		};

		upload1 = new SWFUpload(settings1);		
		upload2 = new SWFUpload(settings2);
	}
	
	// To change the tab inside prototype window -------------------------------
	
	function tab_show(div_obj){		
		var childNodeArray	=	new Array();
		childNodeArray = document.getElementById('fsUploadProgress1').getElementsByTagName("*");
		for(var j=0; j<childNodeArray.length;j++){
			if(childNodeArray[j].className=='progressWrapper'){
				childNodeArray[j].innerHTML='';
			}
		}
		var zipChildArray	=	new Array();
		zipChildArray		=	document.getElementById('fsUploadProgress2').getElementsByTagName("*");
		for(var k=0; k<zipChildArray.length;k++){
			if(zipChildArray[k].className=='progressWrapper'){
				zipChildArray[k].innerHTML='';
			}
		}
		$('#txtAlt').val('');
		$('#txtTitle').val('');
		$('#txtkeywords').val('');
		if(div_obj=='#assets'){
		   showList(selnumber,selform,selcategory,seluploadcat,seluniqueID,"1",getvalue);	
		}else{
		   if(testvar==0)
				setTimeout('loadswf()',500);	
		}
	}
	
	// To save alt,title,keyword in the upload form ----------------------------
	
	function save_alt(){
		var alt 		= $('#txtAlt').val();
		var title 		= $('#txtTitle').val();
		var keywords 	= $('#txtkeywords').val();
		var posturl		= "title="+title+"&alt="+alt+"&keywords="+keywords;
		$.ajax({
			type	:	'GET',
			url		:	'/logincms/includes/set_session.php',	
			data	:	posturl,
			error	:	function(){alert('Error loading document!');},
			success	:	function(msg){
			  if(msg == 'S'){}
			}
		});	 
	}

// For assets listing in the first tab -------------------------------------------------------
	function showList(pnumber,pform,pcateg,puploadcat,uniid,pagenum,editid) { 
		if ((puploadcat == null) || (pcateg == "")) return;  
	    var ran_unrounded=	Math.random()*100000;
		var ran_number	 =  Math.floor(ran_unrounded); 
		var url			 =  "/logincms/includes/showList.php";
		var posturl		 =	"pnumber=" + escape(pnumber)+"&pform=" +escape(pform)+"&pcateg=" +escape(pcateg)+"&puploadcat=" +escape(puploadcat)+"&puniid=" +escape(uniid)+"&pagenum=" +escape(pagenum)+"&editid=" +escape(editid)+"&s=" +ran_number;	
		$.ajax({
			type	:	'POST',
			url		:	'/logincms/includes/showList.php',	
			data	:	posturl,
			error	:	function(){alert('Error loading document!');},
			success	:	function(msg){
			  if(msg != ''){
				var getOP	=	msg.split("^");	
				if(getOP[1]	==	'S'){					
					$('#'+puploadcat).html(getOP[0]);
				}else if(getOP[1]	==	'F'){
					$('#'+puploadcat).html('');
				}
			  }
			}
		});	 
	}
	
/******* Function to Fill Image Name ***********/
	function FillName(filename,imgid,number,formname,fname){
		var f1	=	eval("document.forms."+formname+"."+fname);
		var f2	=	eval("document.forms."+formname+"."+fname+"id");
		if(f1)
			f1.value = filename;
		if(f2)
			f2.value = imgid;
		$(document).trigger('close.facebox');
	}
	
/******* Function to check all assets **********/
	
	function checkAll(field) {
 		if(field){
			if (checkflag == "false") {
				for (i = 0; i < field.length; i++) {
					field[i].checked = true;
				}
			 	checkflag = "true";
			  return "Uncheck All";
			} else {
				 for (i = 0; i < field.length; i++) {
					 field[i].checked = false;
				 }
				 checkflag = "false";
				 return "Check All";
	 		}
		}
	 }
 
	 /******* Function to insert checked all assets ********/
 
	 function insertAllChecked(){
 		var field		=	new Array();
		field			=	document.forms.FrmImgList.elements['chkDel[]'];
		var assetID		=	'';
		var assetName	=	'';
		var disAsset	=	'';
		var totVal		=	'';
		var inc			=	0;
		var frmName		=	document.forms.FrmImgList.form.value;
		var unID		=	document.forms.FrmImgList.uniqueID.value;
		var temp		=	document.forms.FrmImgList.number.value;
		upcat			=	temp+"id";
		catid			=	document.forms.FrmImgList.catID.value;
		tmpname			=	'';
		
		for (i = 0; i < field.length; i++) {
			if( field[i].checked){
				inc++;
				var g	=	field[i].value;
				totVal	=	g.split('#');
				assetID+=	totVal[0]+',';
				tmpname =   totVal[1];
				assetName+=	totVal[1]+'<br />\n';
				disAsset+=   "<img src='/assets/thumbs/"+catid+'/'+tmpname+"'>";
				
			}
		}	
		
		if(tmpname!="" && temp=='product_speclb'){
			showSpec('Bikes');
		}		
		if(unID	==	'multiple'){
		   if(temp == 'press_images'){
				var DownName		=	'asset2';
				var spanName		=	'file_names2';
		   }else{
			   	var DownName		=	'asset2';
				var spanName		=	temp+'2';
				assetName			=	disAsset;
	   	   }
		}else{
			var DownName		=	'asset'+unID;
			var spanName		=	'file_names'+unID;
		}	
	
		if(document.getElementById(upcat)){
			document.getElementById(upcat).value	=	assetID;
		}
		if(document.getElementById(spanName)){
			document.getElementById(spanName).innerHTML	=	assetName;
		}
		$(document).trigger('close.facebox');
	}

/********** Function to insert single checked asset ***********/

function insertChecked(){
	for (var i=0; i < (document.forms.FrmImgList.chkRad.length); i++)
  	{	 		 
  		 if (document.forms.FrmImgList.chkRad[i].checked){
			  var rad_val = document.forms.FrmImgList.chkRad[i].value;
			  totVal	=	rad_val.split('#');
			  assetID	=	totVal[0];
			  assetName =	totVal[1];
			  var frmName	=	document.forms.FrmImgList.form.value;
			  var sing		=	document.forms.FrmImgList.uploadcat.value;	
			  sing		=	sing.replace('-','_');
			  sing		=	sing.replace('-','_');
			  var f1	=	document.getElementById(sing);
			  var f2	=	document.getElementById(sing+"id");				  
			  if(f1){
				f1.value	=	assetName;
			  }
			  if(f2){
				f2.value	=	assetID;
			  }
		 }
		 $(document).trigger('close.facebox');
   }
   
}
	
	
	// Function to get parameters from url
	
	function getURLParameters(url){
		var sURL = url.toString();
	
		if (sURL.indexOf("?") > 0){
			var arrParams = sURL.split("?");			
			var arrURLParams = arrParams[1].split("&");		
			var arrParamNames = new Array(arrURLParams.length);
			var arrParamValues = new Array(arrURLParams.length);		
			var i = 0;
			for (i=0;i<arrURLParams.length;i++){
				var sParam =  arrURLParams[i].split("=");
				arrParamNames[i] = sParam[0];
				if (sParam[1] != "")
					arrParamValues[i] = unescape(sParam[1]);
				else
					arrParamValues[i] = "";
			}
			
			for (i=0;i<arrURLParams.length;i++){
				if(arrParamNames[i]=='number'){
					selnumber=arrParamValues[i];
				}
				if(arrParamNames[i]=='form'){
					selform=arrParamValues[i]; 
				}
				if(arrParamNames[i]=='editId'){
					seleditId=arrParamValues[i]; 
				}
				if(arrParamNames[i]=='category'){
					selcategory=arrParamValues[i];
				}
				if(arrParamNames[i]=='uploadcat'){
					seluploadcat=arrParamValues[i]; 
				}
				if(arrParamNames[i]=='uniqueID'){
					seluniqueID=arrParamValues[i]; 
				}		
			}
		}	
	}

	 function loadSortables() {
		$("#contentLeft ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var pages = $("#ActionType").val();
			var order = $(this).sortable("serialize") + '&action=updateRecordsListings' + '&module='+pages; 
			$.post("/logincms/includes/order_ajax.php", order, function(theResponse){
				$("#contentRight").html(theResponse);
			}); 															 
	       }								  
		});
	}