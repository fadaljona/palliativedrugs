$(document).ready(function () {	
	 snippettable	=	$('#snippetlist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": false,
		"bLengthChange": 100,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"snippets"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
	pubmedtable	=	$('#pubmedlist').dataTable({ 		
		"bStateSave": true,
		"bPaginate": true,
		"sPaginationType": "full_numbers",		
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/logincms/includes/datatable-ajax.php",
		"fnDrawCallback": function() {
			$("a.button", ".display tbody").button();
		},
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( {"name":"filter_type","value":"pubmeds"} );
			$.ajax({
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			});			
		}	   	
	});
});

// Validate pubmed ==============================================

function validate_snippet(){
	flag=true;
	var message		= '';	
	if($('#snippet_title').val()==''){
	   message += '<li>Please enter title</li>';
	   flag= false;
	}	
   if(flag==true){
	   $('.form_description').find('.error').html('');
	   $('#addsnippet').submit();
   }else{		   
		$('.form_description').find('.error').html('<ul>'+message+'</ul>');
		window.scrollTo(0,0);
		return flag;
   }
}

// Function assign image to the content

function assignImage(){
  var imgname	=	$('#snippet_image').val();
  var insertHtml=	'<img src="/assets/images/2/'+imgname+'" />';
  tinyMCE.getInstanceById(tinyMCE.activeEditor.id).getWin().focus();
  tinyMCE.execCommand('mceInsertContent',false,insertHtml);

}

function update_reference(){  
	if($('#id').val()=='')return;
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "formulary=" + escape($('#id').val())+"&action=update_reference&s=" +ran_number,
		   success: function(response){
			 var getOP	 =	response.split("^");
			 if(getOP[1]	==	'S'){		
				$('#dialog ol').append(getOP[0]);				
			 }    
		  }
	 }); 
}

function insert_dialog(pmid){
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: "pmid=" + escape(pmid)+"&action=insert_pmid&s=" +ran_number,
		   success: function(response){
			 var getOP	 =	response.split("^");
			 if(getOP[1]	==	'S'){		
				$('#dialog ol').append(getOP[0]);				
			 }    
		  }
	 });
}

function refresh_dialog(){	
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	tinyMCE.triggerSave();
	$.ajax({
	   type: "POST",
	   url: "/logincms/includes/orderajax.php",
	   data: $('#addsnippet').serialize()+"&action=refresh_dialog&s=" +ran_number,
		   success: function(response){
			 var getOP	 =	response.split("^");
			 if(getOP[1]	==	'S'){		
				$('#dialog ol').html(getOP[0]);				
			 }    
		  }
	 }); 
}