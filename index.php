<?php $doc_root	=	$_SERVER['DOCUMENT_ROOT'];
include("$doc_root/siteconfig/configs/config_include.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include ("$doc_root/includes/inc-header.php")?>
</head>
<body>
<?php include ("$doc_root/includes/inc-error-login.php")?>
<?php include ("$doc_root/includes/inc-user-update-expose.php")?>
<div id="container">
  <?php include ("$doc_root/includes/inc-topnav.php")?>
  <?php include ("$doc_root/includes/inc-nav.php")?>
  <?php include ("$doc_root/includes/inc-exposewindow.php")?>
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <?php include ("$doc_root/includes/inc-loginform.php")?>
<a href="https://twitter.com/palliativedrugs" class="twitter-follow-button" data-show-count="false" data-size="large"  data-lang="en">Follow @palliativedrugs</a>
            <?php include ("$doc_root/includes/inc-newsroller.php")?>
            <?php include ("$doc_root/includes/inc-germanlink.php")?>
            <?php include ("$doc_root/includes/inc-honcode.php")?>
          </div>
        </div>
      </div>
    </div>
    <div id="col02">
      <div class="bg_right">
        <div class="tr">
          <div class="bl">
            <div class="br">
              <div id="col02_1">

                <div class="pressnews">
                  <h1>Press Release</h1>
                  <h2>{{hppress.title}}</h2>
                  <h3>{{hppress.ndate}}</h3>
                  {{hppress.stext}}<a href="/press-news/{{hppress.nmonth}}/{{hppress.alias_title}}.html"> Read more</a> </div>

                <h1><strong>{{article.article_title}}</strong></h1>
                {{article.long_text}}

              </div>
              <div id="col02_2">
                <?php //  include ("$doc_root/includes/inc-hpsurvey.php")?>
<h2>Calling all members</h2>
<p>Please complete our short survey:<br />
  <br />
  <a href="survey.html">Prevention of skeletal-related events (SRE) in adults with bone metastases from solid tumours - what do you use?</a> </p>
<!--<p>Previous survey results<br />
  <a href="/latest.html#survey">click here</a></p>--> 

                <div class="latestnews">
                  <h1>Latest Additions</h1>
                  <ul>
                    {{repeat}}
                    <li><small>{{hpaddition.ndate}}</small><br />
                      {{hpaddition.title}} <a href="/latest/{{hpaddition.nmonth}}/{{hpaddition.alias_title}}.html"> Read more</a></li>
                    {{/repeat}}
                  </ul>
                </div>
                <div class="latestnews">
                  <h1>News</h1>
                  <ul>
                    {{repeat}}
                    <li><small>{{hpnews.ndate}}</small><br />
                      {{hpnews.title}} <a href="/news/{{hpnews.nmonth}}/{{hpnews.alias_title}}.html">Read more</a></li>
                    {{/repeat}}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include ("$doc_root/includes/inc-footer.php")?>
</div>
<?php include ("$doc_root/includes/inc-analytics.php")?>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</body>
</html>