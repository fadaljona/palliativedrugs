<?php $doc_root	=	$_SERVER['DOCUMENT_ROOT'];
include("$doc_root/siteconfig/configs/config_include.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<?php include ("$doc_root/includes/inc-formulary-header.php")?>
</head>
<body  id="formularyhome" >
<div id="container">
<?php include ("$doc_root/includes/inc-topnav.php")?>
<?php include ("$doc_root/includes/inc-nav.php")?>	
  <div id="contentwrapper">
    <div class="tr">
      <div class="bl">
        <div class="br">
          <div id="contentcolumn"> 
	  <?php include ("$doc_root/includes/inc-formulary-nav.php")?>
	  <?php include ("$doc_root/includes/inc-formulary-article.php")?>         
        </div>
        </div>
      </div>
    </div>
  </div>
  <?php include ("$doc_root/includes/inc-footer.php")?>
</div>
<?php include ("$doc_root/includes/inc-analytics.php")?>
</body>
</html>