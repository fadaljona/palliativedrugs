RewriteEngine on
RedirectMatch ^/$ /index.html
RewriteRule \.(css|jpe?g|gif|png|js)$ - [L] 
RewriteRule ^(shop)\/(proforma)\.html$ /shop/proforma.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/(worldpay)\.html$ /shop/worldpay.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/(cart-items)\.html$ /shop/cart-items.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/(summary)\.html$ /shop/summary.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/(checkout)\.html$ /shop/checkout.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/(books)\.html$ /shop/books.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/(onlinesubscriptions)\.html$ /shop/online-subscription.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/(combinationpackages)\.html$ /shop/combinationpackages.php?folder=$1&no404=$2&tit=nftrue [L,QSA]
RewriteRule ^(shop)\/([^/\.].*)\.html$ /shop/product-detail.php?folder=$1&ptitle=$2&tit=nftrue [L,QSA]
RewriteRule ^(formulary)\/([^/\.].*)\/([^/\.].*)\.html$ /loadpage.php?page=$1&site_language=$2&ftitle=$3&tit=nftrue [L,QSA]
RewriteRule ^(news|press-news|latest)\/([0-9]+)\/([^/\.].*)\.html$ /loadpage.php?page=$1&nyear=$2&nmonth=$3&tit=nftrue [L,QSA]
RewriteRule ^(news|press-news|latest)\/([^/\.].*)\/([^/\.].*)\.html$ /loadpage.php?page=$1&nmonth=$2&ntitle=$3&tit=nftrue [L,QSA]
RewriteRule ^(index)\.php$ loadpage.php?page=$1&tit=nftrue [L,QSA]
RewriteRule ^([^/\.].*)\.html$ loadpage.php?page=$1&tit=nftrue [L,QSA]
php_value display_errors 1
Options -Indexes