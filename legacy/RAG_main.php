<?php require_once('Connections/cnPall.php'); ?>
<?php
mysql_select_db($database_cnPall, $cnPall);
$query_rsRagSection = "SELECT id, title, active FROM rag_section WHERE active = 1 ORDER BY title ASC";
$rsRagSection = mysql_query($query_rsRagSection, $cnPall) or die(mysql_error());
$row_rsRagSection = mysql_fetch_assoc($rsRagSection);
$totalRows_rsRagSection = mysql_num_rows($rsRagSection);

$colname_rsEntries = $row_rsRagSection['id'];
if (isset($_GET['secid'])) {
  $colname_rsEntries = (get_magic_quotes_gpc()) ? $_GET['secid'] : addslashes($_GET['secid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsEntries = sprintf("SELECT id, section_id, title FROM rag_entry WHERE section_id = %s", $colname_rsEntries);
$rsEntries = mysql_query($query_rsEntries, $cnPall) or die(mysql_error());
$row_rsEntries = mysql_fetch_assoc($rsEntries);
$totalRows_rsEntries = mysql_num_rows($rsEntries);
?>
<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/RAG_func.inc.php');

session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
/*TMP
if(!$_SESSION[visitor]->RAG) {
	header("location:  /content/website/RAG_info.html");
}
$rag_vetted = dLookUp("vetted","rag_user","user_id=" . $_SESSION[visitor]->id);
if($rag_vetted == 0) {
	$msg = "<p>Your application to join the RAG panel has not yet been processed.</p>";
	$halt = TRUE;
}

if(isset($_GET['login'])) {
	$onload = "onLoad=\"parent.leftmenu.location.href='leftmenu.php';\"";
} else {
	$onload = "";
}
*/
?>
<!-- 
	<link rel="STYLESHEET" type="text/css" href="/mk/style.css">
 <div style="font-family:arial,helvetica,sans; font-size:13px;">
 -->
<link href="/css/legacy.css" rel="stylesheet" type="text/css">
<h3>Welcome to the Document library</h3>
<?
echo $msg;
if($halt==TRUE) {
	exit;
}
?>
<p>To access the documents first click on the topic of interest followed by the contents.</p><br>
<table border="0" cellspacing="0" cellpadding="4" align="center">
  <tr> 
    <th>Topic</th>
    <td>&nbsp;</td>
    <th>Contents</th>
  </tr>
  <tr> 
    <td valign="top"> <form name="frmSection">
		<select name="sectionid" size="8" id="sectionid" style="width:265px;" onchange=
      "document.location.href=document.frmSection.sectionid.options[document.frmSection.sectionid.selectedIndex].value">
		    <?php do { ?>
     <option value="RAG_main.php?secid= <?php echo $row_rsRagSection['id']; ?>"
	 <?php 
	 if ($colname_rsEntries==$row_rsRagSection['id']) {
	 	echo " selected";
	}
	 ?>
	 ><?php echo $row_rsRagSection['title']; ?></option>
  <?php } while ($row_rsRagSection = mysql_fetch_assoc($rsRagSection)); ?> 
		</select></form>
  </td>
  <td></td>
  <td align="center"><form name="frmEntry">
        <select name="entryid" size="8" id="entryid" style="width:265px;" onchange=
      "document.location.href=document.frmEntry.entryid.options[document.frmEntry.entryid.selectedIndex].value">
          <?php do {  ?>
          	<option value="RAG_entry.php?secid=<?php echo $_GET['secid']; ?>&entryid=<?php echo $row_rsEntries['id']?>"><?php echo $row_rsEntries['title']?></option>
          <?php
} while ($row_rsEntries = mysql_fetch_assoc($rsEntries));
  $rows = mysql_num_rows($rsEntries);
  if($rows > 0) {
      mysql_data_seek($rsEntries, 0);
	  $row_rsEntries = mysql_fetch_assoc($rsEntries);
  }
?>
        </select>
        <br>
<!-- <input name="btnView" type="submit" id="btnView" value="view"> -->
        <input name="secid" type="hidden" id="secid" value="<?php echo $_GET['secid']; ?>">
      </form></td>
  </tr>  
</table>
<br>
To donate guidelines, treatment protocols, patient information leaflets or audit documentation (e.g. proformas, results), please 
send them as Word or PDF documents to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.

</body>
</html>
<?php
mysql_free_result($rsRagSection);

mysql_free_result($rsEntries);
?>
</div>
