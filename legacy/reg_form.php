<? ///////////////////////////////////////////////////////////////////
// palliativedrugs.com/reg_form.php
// mkirk@aejent.co.uk, 12-04-2002
///////////////////////////////////////////////////////////////////////

include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/misc_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');

session_start();
if ($_SERVER['REQUEST_METHOD']=="POST") {
	$username 		= AddSlashes(trim($_POST["txt_username"]));
	$email 			= AddSlashes(trim($_POST["txt_email"]));
	$pwd1 			= AddSlashes(trim($_POST["txt_pwd1"]));
	$pwd2 			= AddSlashes(trim($_POST["txt_pwd2"]));
	$FirstName 		= AddSlashes(trim($_POST["txt_FirstName"]));
	$LastName 		= AddSlashes(trim($_POST["txt_LastName"]));
	$title 			= AddSlashes(trim($_POST["sel_title"]));
	$role 			= AddSlashes(trim($_POST["sel_role"]));
	$role_other		= AddSlashes(trim($_POST["txt_roleOther"]));
	$speciality 	= AddSlashes(trim($_POST["sel_speciality"]));
	$speciality_other = AddSlashes(trim($_POST["txt_specialityOther"]));
	$organisation 	= AddSlashes(trim($_POST["txt_organisation"]));
	$addr1 			= AddSlashes(trim($_POST["txt_addr1"]));
	$addr2 			= AddSlashes(trim($_POST["txt_addr2"]));
	$addr3 			= AddSlashes(trim($_POST["txt_addr3"]));
	$addr4 			= AddSlashes(trim($_POST["txt_addr4"]));	
	$TownOrCity 	= AddSlashes(trim($_POST["txt_TownOrCity"]));
	$StateOrCounty 	= AddSlashes(trim($_POST["txt_StateOrCounty"]));
	$PostalCode 	= AddSlashes(trim($_POST["txt_PostalCode"]));
	$Country 		= AddSlashes(trim($_POST["sel_Country"]));
	$phone 			= AddSlashes(trim($_POST["txt_phone"]));
	$lead 			= AddSlashes(trim($_POST["sel_lead"]));
	$lead_other		= AddSlashes(trim($_POST["txt_leadOther"]));
	$AcceptPDImails = AddSlashes(trim($_POST["txt_AcceptPDImails"]));
	$AcceptBBmails 	= AddSlashes(trim($_POST["txt_AcceptBBmails"]));
	
	$errors = array();
	
	// validate username
	if($username) {
		//if(!AlphaNumAndLenTest($username,4,50)) {
		//	$errors["username"] = "please choose a username between 4 & 50 characters, using only letters and numbers.";
		//} 	
		$uid = 	dLookUp("id", "users", "username='$username'");
		if(IsSet($uid)) {
			$errors["username"] = "that username is already taken, please choose another.";
		}
	} else {	
		$errors["username"] = "you must choose a username.";
	}
	
	//validate email 
	if($email) {
		if(!validEmail($email)) {
			$errors["email"] = "please enter a valid e-mail address.";
		}
	} else {
		$errors["email"] = "you must enter an email address.";
	}
	
	//validate password
	if($pwd1){		
	} else {
		$errors["password"] = "you must enter a password.";
	}
	if($pwd2){		
		if(!($pwd1==$pwd2)) {
			$errors["password"] = "your passwords did not match.";
		}
	} else {
		$errors["password"] = "you must enter your password twice.";
	}
	
	//validate FirstName
	if($FirstName){
		// no further validation.	
	} else {
		$errors["first name"] = "you must enter a first name.";
	}
	
	//validate LastName
	if($LastName){
		// no further validation.	
	} else {
		$errors["last name"] = "you must enter a last name.";
	}
	
	//validate org
	if($organisation){
		// no further validation.	
	} else {
		$errors["organisation"] = "please enter your organisation name.";
	}
	
	//validate addr1+2
	if($addr1){
		// no further validation.	
	} else {
		$errors["work address"] = "please enter your work address.";
	}
	
	//validate city
	if($TownOrCity){
		// no further validation.	
	} else {
		$errors["city"] = "please enter your town or city name.";
	}
	
	//validate state
	if($StateOrCounty){
		// no further validation.	
	} else {
		$errors["state/county"] = "please enter your state or county name.";
	}
	
	//validate zip
	if($PostalCode){
		// no further validation.	
	} else {
		$errors["postcode/zip"] = "please enter your postcode or zip.";
	}
	
	//validate phone number
	if($phone){
		// no further validation.	
	} else {
		$errors["phone"] = "please enter your work telephone number.";
	}
	
	// end of field validation routines.
	
	if(sizeof($errors)==0) {
		if($role=="Other (please specify)") {
			$role = $role_other;
		}
		if($speciality=="Other (please specify)") {
			$speciality = $speciality_other;			
		}
		if($lead=="Other (please specify)") {
			$lead = $lead_other;
		}		
		$sql = "INSERT INTO users"
			. " (username, email, pwd, FirstName, LastName, title,"
			. " role, speciality, organisation, addr1, addr2, addr3, addr4,"
			. " TownOrCity, StateOrCounty, PostalCode, Country, phone,"
			. " lead, AcceptPDImails, AcceptBBmails, UserType,"
			. " DateRegistered, RegistrationChecked, active)"
			. " VALUES ("
			. "'$username', '$email', '$pwd1', '$FirstName','$LastName','$title',"
			. "'$role','$speciality','$organisation','$addr1','$addr2','$addr3','$addr4',"
			. "'$TownOrCity','$StateOrCounty','$PostalCode','$Country','$phone',"
			. " '$lead',1,1,'user',"
			. " '".GetDbTimestamp()."', 0, 1)";		
		
		mysql_query($sql) or die(mysql_error() ."<hr>$sql");
		$userid= mysql_insert_id();
		
		// auto-login 
		if (!IsSet($_SESSION['visitor'])) {
			$visitor = new objVisitor;
			session_register("visitor");
		}
		$_SESSION['visitor']->id = $userid;
		$_SESSION['visitor']->acl = "user";
		$_SESSION['visitor']->logged_in = TRUE;

		// send welcome email... 
		$tags["title"] = StripSlashes($title);
		$tags["lastname"] = StripSlashes($LastName);
		$tags["username"] = StripSlashes($username);
		$tags["password"] = StripSlashes($pwd1);
		$body = BasicMailMerge("/legacy/content/snippets/welcome.txt", $tags);
		mail($email, "Welcome to palliativedrugs.com", $body, "From:hq@palliativedrugs.com");	
		
		header("Location: /legacy/content/website/reg_thankyou.html");
	}		
} // if ($_SERVER['REQUEST_METHOD']=="POST")
?>
<html>
<head>
<title>&copy; palliativedrugs.com</title>
<link REL="stylesheet" TYPE="text/css" HREF="/pdi_style.css">
<link rel="stylesheet" href="/palliative.css" type="text/css">
<style type="text/css">
body{font:normal 62.5% Arial, Helvetica, sans-serif;}
p{font:normal 0.8em Arial, Helvetica, sans-serif;}
p strong{color:#333;}
</style>
</head>
<body bgcolor="#ffffff">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
		<p>
		<?
		if ($_SERVER['REQUEST_METHOD']=="GET") {
			print "Palliativedrugs.com will use your e-mail address to deliver an "
				. "update newsletter and messages from the bulletin board on a regular "
				. "basis. Once registered, you can choose whether to receive these or "
				. "not by clicking on 'preferences' and making the appropriate selection.";
		} else {
			if(sizeof($errors)>0) {
				print "<span class=\"warning\">errors occured, please correct the following:-</span><br>";	
				foreach($errors as $key => $value)
					echo "<a href=\"#$key\"><LI>$key : $value</a><br>\n";
			}
		}
		?>
       </p>
    </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr> 
    <td> 
      <form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
        <table cellpadding="1" cellspacing="0" border="0" width="100%">
          <tr> 
            <td colspan=2 bgcolor=#CCCCCC> 
              <p><strong>Login information</strong></p>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td></tr>
          <tr> 
            <td valign="top"> 
				<p><a name="username">Username</a></p>
            </td>
            <td> 
              <p><input name="txt_username" maxlength="200" size="30" value=<?=StripSlashes($username)?>></p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="email">Email</a></p></td>
            <td> 
              <p><input name="txt_email" maxlength="200" size="30" value=<?=StripSlashes($email)?>></p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="password">Password</a></p></td>
            <td> 
              <p><input name="txt_pwd1" type="password" maxlength="80" size="30" value="<?=$pwd1?>"></p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p>Retype Password</p></td>
            <td> 
              <p><input name="txt_pwd2" type="password" maxlength="80" size="30" value="<?=$pwd2?>"></p>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan=2 bgcolor=#CCCCCC> 
              <p><strong>Contact information (all fields required)</p>
            </td>
          </tr>
          <tr><td colspan="2">&nbsp;</td>
          <tr> 
            <td valign="top"><p>Title</p></td>
            <td> 
              <p>
			  	<?
				$options=array("Professor","Doctor","Mr","Mrs","Ms","Miss","Sister");
				$default = (IsSet($title)) ? $title : "Doctor";
				arrayToListBox("sel_title",$options,$default);
				?>
               </p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="first name">First Name</a></p></td>
            <td> 
              <p><input name="txt_FirstName" maxlength="200" size=30 value="<?=StripSlashes($FirstName)?>"></p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="last name">Last Name</a></p></td>
            <td> 
              <p><input name="txt_LastName" maxlength="200" size=30 value="<?=StripSlashes($LastName)?>"></p>
            </td>
          </tr>
          <tr><td valign="top">&nbsp;</td><td>&nbsp;</td></tr>
          <tr> 
            <td valign="top"><p>Professional Role</p></td>
            <td> 
              <p> 
			  	<?
				$options=array("Doctor","Nurse","Pharmacist","Other (please specify)");
				$default = (IsSet($role)) ? $role : "Doctor";
				arrayToListBox("sel_role",$options,$default);
				?>
              </p>
            </td>
          </tr>
          <tr> 
            <td valign="top" align="right"> 
              <p>(Other)
            </p></td>
            <td> 
              <p><input size="30" name="txt_roleOther" maxlength="200" value="<?=StripSlashes($role_other)?>"></p>
            </td>
          </tr>
          <tr><td valign="top">&nbsp;</td><td>&nbsp;</td></tr>
          <tr> 
            <td valign="top"><p>Speciality</p></td>
            <td> 
              <p> 
  			  	<?
				$options=array("Palliative medicine","Oncology","General medicine","General practice 
                  (family medicine)","Anaesthesia","Other (please specify)");
				$default = (IsSet($speciality)) ? $speciality : "Palliative medicine";
				arrayToListBox("sel_speciality",$options,$default);
				?>
               </p>
            </td>
          </tr>
          <tr>
		  	<td valign="top" align="right"><p>(Other)</p></td>
            <td><p><input size="30" maxlength="200" name="txt_specialityOther" value="<?=StripSlashes($speciality_other)?>"></p></td>
          </tr>
          <tr> <td>&nbsp;</td> <td>&nbsp;</td> </tr>
		  <tr> 
            <td valign="top"><p><a name="organisation">Organisation</a></p></td>
            <td><input name="txt_organisation" maxlength="200"  size=30 value="<?=StripSlashes($organisation)?>"></td>
          </tr>
          <tr> 
		  
          <tr> 
            <td valign="top"><p><a name="work address">Work Address</a></p></td>
            <td> 
              <p>
                <input name="txt_addr1" maxlength="200"  size=30 value="<?=StripSlashes($addr1)?>"><br>
                <input name="txt_addr2" maxlength="200"  size=30 value="<?=StripSlashes($addr2)?>"><br>
                <input name="txt_addr3" maxlength="200"  size=30 value="<?=StripSlashes($addr3)?>"><br>
                <input name="txt_addr4" maxlength="200"  size=30 value="<?=StripSlashes($addr4)?>">
              </p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="city">City</a></p></td>
            <td> 
              <p><input name="txt_TownOrCity" maxlength="200"  size=30 value="<?=StripSlashes($TownOrCity)?>"></p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="state/county">State/Province/County</a></p></td>
            <td> 
              <p><input name="txt_StateOrCounty" maxlength="200"  size=30 value="<?=StripSlashes($StateOrCounty)?>"></p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="postcode/zip">Zip/Postal Code</a></p></td>
            <td> 
              <p><input name="txt_PostalCode" maxlength="200"  size=15 value="<?=StripSlashes($PostalCode)?>"></p>
            </td>
          </tr>
          <tr> 
            <td valign="top"><p><a name="country">Country</a></p></td>
            <td> 
              <p>
			  <?
				$options=array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", 
				"Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
				"Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh",
				"Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", 
				"Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", 
				"British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso",
				"Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
				"Central African Republic", "Chad", "Chile", "China", "Christmas Island", 
				"Cocos Keeling Islands", "Colombia", "Comoros", "Congo (Brazzaville)", 
				"Congo, Democratic Republic of", "Cook Islands", "Costa Rica", "Croatia Hrvatska",
				"Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
				"Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", 
				"Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", 
				"Falkland Islands, Malvinas", "Faroe Islands", "Fiji", "Finland", "France",
				"French Guiana", "French Polynesia", "French Southern Territories", "Gabon",
				"Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Great Britain (UK)",
				"Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea",
				"Guinea-Bissau", "Guyana", "Haiti", "Heard and McDonald Islands", "Honduras",
				"Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland",
				"Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati",
				"Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon",
				"Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau",
				"Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta",
				"Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico",
				"Micronesia", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique",
				"Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", 
				"New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island",
				"Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", 
				"Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", 
				"Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", 
				"S. Georgia and S. Sandwich Isls.", "Saint Kitts and Nevis", "Saint Lucia", 
				"Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", 
				"Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovak Republic", 
				"Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", 
				"St. Helena & Dependencies", "St. Pierre and Miquelon", "Sudan", "Suriname", 
				"Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syria", 
				"Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", 
				"Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", 
				"Tuvalu", "U.S. Minor Outlying Islands", "Uganda", "Ukraine", "United Arab Emirates", 
				"United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", 
				"Vatican City State", "Venezuela", "Viet Nam", "Virgin Islands (British)", 
				"Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", 
				"Yugoslavia", "Zambia", "Zimbabwe");
				$default = (IsSet($Country)) ? $Country : "United Kingdom";
				arrayToListBox("sel_Country",$options,$default);
				?>
               </p>
            </td>
          </tr>
          <tr><td valign="top">&nbsp;</td><td>&nbsp;</td></tr>
          <tr> 
            <td valign="top"><p><a name="phone">Telephone Number<br>
            Inc. International/Area code)</a></p></td>
            <td valign="top"> 
              <p><input name="txt_phone" maxlength="200"  size=30 value="<?=StripSlashes($phone)?>"></p>
            </td>
          </tr>
          <tr><td valign="top">&nbsp;</td><td>&nbsp;</td></tr>
          <tr> 
            <td valign="top"> 
              <p>Where did you first hear about us?</p>
            </td>
            <td>
			<p>
			<?
				$options=array("web search","word of mouth","book version","CD ROM version",
				"mail shot","magazine advert","conference","link from other website","Other (please specify)");
				$default = (IsSet($lead)) ? $lead : "web search";
				arrayToListBox("sel_lead",$options,$default);
			?>
            </p>
            </td>
          </tr>
          <tr> 
            <td align="right" valign="top"><p>(Other)</p></td>
            <td> 
              <p><input size="30" maxlength="200" name="txt_leadOther" value="<?=StripSlashes($lead_other)?>"></p>
            </td>
          </tr>
          <tr><td colspan="2" >&nbsp;</td></tr>
          <tr> 
            <td colspan="2" align="center"> 
              <p><input type="submit" value="Register" name="submit"></p>
            </td>
          </tr>
          <tr><td colspan="2" >&nbsp;</td></tr>
        </table>
      </form>
    </td>
  </tr>
</table>
</body>
</html>


