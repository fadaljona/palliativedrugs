<?php require_once('_include/pall_db.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

?>
<?php

if($_GET['ftitle']!='index'){
	$selFormulary	=	$pall_db->query("SELECT formulary_id,formulary_title,alias FROM 3bit_formulary WHERE 1 AND STATUS ='1'") or die(mysql_error());
	if($pall_db->getRecordCount( $selFormulary )>0){
		while($frow		=	$pall_db->getRow($selFormulary)){
			$formtitle	=	stripslashes($frow['formulary_title']);
			$faliastitle=	stripslashes($frow['alias']);			
			if((remSpecial($formtitle)==$_GET['ftitle']) || (remSpecial($faliastitle)==$_GET['ftitle'])){
				$gform_id	=	$frow['formulary_id'];
			}
		}
	}
}

$selVersion		=	$pall_db->query("SELECT version FROM 3bit_tmp_formulary_blocks WHERE formulary_id='".$gform_id."' ORDER BY version DESC LIMIT 0,1") or die(mysql_error());
if($pall_db->getRecordCount( $selVersion )>0){	
	while($frow		=	$pall_db->getRow($selVersion)){
		$lat_version	=	$frow['version'];
	}
}
?>
<div class="container">
<?php
	$selArticle		=	$pall_db->query("SELECT * FROM 3bit_formulary_articles WHERE formulary_id='".$gform_id."' ORDER BY version DESC LIMIT 0,1");
	if($pall_db->getRecordCount($selArticle)>0){	
		while($row	=	$pall_db->getRow($selArticle)){
			$content	=	stripslashes($row['content']);
			echo html_entity_decode($content);
		}
	}	
?> 
</div>