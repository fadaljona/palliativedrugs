<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php'); 
?>
<link rel="STYLESHEET" type="text/css" href="/css/legacy.css">

<Script Language="JavaScript">
function load(url) {
var load = window.open(url,'','scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,location=no,status=no');
}
</Script>
<!-- <div style="font-family:arial,helvetica,sans; font-size:13px;">  -->
<?
if ($_SERVER['REQUEST_METHOD']=='POST') {
	$drug1 = rtrim($HTTP_POST_VARS['drug1']);
	$drug2 = rtrim($HTTP_POST_VARS['drug2']);
	$drug3 = rtrim($HTTP_POST_VARS['drug3']);
	$drug4 = rtrim($HTTP_POST_VARS['drug4']);
	$drug5 = rtrim($HTTP_POST_VARS['drug5']);
	$drug6 = rtrim($HTTP_POST_VARS['drug6']);
	$exact = IsSet($HTTP_POST_VARS['exact']) ? 1 : 0;
	
	if(!$drug1 || !$drug2) {
		echo "Please choose at least two drugs, or browse the index.";
		echo "<br><br><a href=\"sddb.php\">Back to SDSD search page</a> | <a target=\"top\" href=\"/syringe-driver-database-introduction.html\">Back to SDSD home page</a>";
		exit();
	}
	
	
	$operator = "AND";
	$sql = "SELECT * FROM sddb WHERE (drug1_name='$drug1' OR drug2_name='$drug1' OR drug3_name='$drug1' OR drug4_name='$drug1' OR drug5_name='$drug1' OR drug6_name='$drug1')";
	if($drug2) {
		$sql .= " $operator (drug1_name='$drug2' OR drug2_name='$drug2' OR drug3_name='$drug2' OR drug4_name='$drug2' OR drug5_name='$drug2' OR drug6_name='$drug2')";
	}
	if($drug3) {
		$sql .= " $operator (drug1_name='$drug3' OR drug2_name='$drug3' OR drug3_name='$drug3' OR drug4_name='$drug3' OR drug5_name='$drug3' OR drug6_name='$drug3')";
	} else {
		$sql .= " $operator (drug3_name='0')";		
	}
	
	if($drug4) {
		$sql .= " $operator (drug1_name='$drug4' OR drug2_name='$drug4' OR drug3_name='$drug4' OR drug4_name='$drug4' OR drug5_name='$drug4' OR drug6_name='$drug4')";
	} else {
		$sql .= " $operator (drug4_name='0')";		
	}
	
	if($drug5) {
		$sql .= " $operator (drug1_name='$drug5' OR drug2_name='$drug5' OR drug3_name='$drug5' OR drug4_name='$drug5' OR drug5_name='$drug5' OR drug6_name='$drug5')";
	} else {
		$sql .= " $operator (drug5_name='0')";		
	}
	
	if($drug6) {
		$sql .= " $operator (drug1_name='$drug6' OR drug2_name='$drug6' OR drug3_name='$drug6' OR drug4_name='$drug6' OR drug5_name='$drug6' OR drug6_name='$drug6')";
	} else {
		$sql .= " $operator (drug6_name='0')";		
	}
	
	$sql .= " AND approved=1";
	
	echo "<a target=\"_top\" href=\"/syringe-driver-database-introduction.html\">Back to SDSD home page</a> | <a href=\"sddb.php\">new search</a><br /><br />";
	
	$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
	if(mysql_num_rows($rs)==0) {
		echo "no matching entries found.";
		echo "<!-- $sql -->";
	} else {		
		$numdrugs = 2;
		
		echo "<table cellpadding=\"1\" cellspacing=\"3\" width=\"90%\" align=\"center\">";
		while($row=mysql_fetch_object($rs)) {
			//need to know how many drugs in this combo for rowspanning
			if($row->drug6_name) {
				$numdrugs=6;
			} elseif($row->drug5_name) {
				$numdrugs=5;
			} elseif($row->drug4_name) {
				$numdrugs=4;
			} elseif($row->drug3_name) {
				$numdrugs=3;
			} else {
				$numdrugs=2;
			}		
		 ?>
			<tr><td>

			<table width="100%" bgcolor="#e0e0e0">
		<!--	<tr><td>
			<table width="100%"> -->
			<tr>
				<th rowspan="2">Drug</th>
				<th rowspan="2">Dose in syringe (mg)</th>
				<th rowspan="2">Final volume in syringe (ml)</th>
				<th rowspan="2">Concentration (mg/ml)</th>
				<th rowspan="2">Diluent</th>
				<th colspan="3">Compatibility information</th>
				<th rowspan="2">Site reaction reported</th>
				<th rowspan="2">&nbsp;</th>
			</tr>
			<tr>
				<th>Outcome</th>
				<th>Duration</th>
				<th>Data</th>
			</tr>
			<tr>
				<td><?=$row->drug1_name?></td>
				<td align="center"><?=($row->drug1_dose) ? $row->drug1_dose : '';?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=($row->volume) ? $row->volume : '';?></td>
				<td align="center"><?=($row->drug1_dose) ? @number_format($row->drug1_dose / $row->volume,2) : '';?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->diluent?></td>
				<?
					if ($row->compatibility=="Incompatible") {
						$style = "color:red;";
					} else {
						$style = "";	
					}
				?>
				<td align="center" rowspan="<?=$numdrugs?>"><span style="<?=$style?>"><?=$row->compatibility?></span></td>
				<td align="center" rowspan="<?=$numdrugs?>">
					<?
					if($row->duration=='other') {
						echo $row->duration_other;
					} else {
						echo $row->duration;
					}
					?>
				</td>				
				
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->datatype?></td> 
			<!--	<td align="center" rowspan="<?=$numdrugs?>">to be confirmed</td>-->
				
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->site_reaction?></td>
				<td align="center" rowspan="<?=$numdrugs?>">&nbsp;
				<?		
				if ($row->show_details_link==1) {
					?>  <a href="javascript:load('sddb_view.php?id=<?=$row->id?>')">details</a> <?
				}
				?>				
				</td>
			</tr>
			<tr>
				<td><?=$row->drug2_name?></td>
				<td align="center"><?=($row->drug2_dose) ? $row->drug2_dose : '';?></td>
				<td align="center"><?=($row->drug2_dose) ? @number_format($row->drug2_dose / $row->volume,2) : '';?></td>
			</tr>
			
			<? if($row->drug3_name) { ?>
			<tr>
				<td><?=$row->drug3_name?></td>
				<td align="center"><?=($row->drug3_dose) ? $row->drug3_dose : '';?></td>
				<td align="center"><?=($row->drug3_dose) ? @number_format($row->drug3_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug4_name) { ?>
			<tr>
				<td><?=$row->drug4_name?></td>
				<td align="center"><?=($row->drug4_dose) ? $row->drug4_dose : '';?></td>
				<td align="center"><?=($row->drug4_dose) ? @number_format($row->drug4_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug5_name) { ?>
			<tr>
				<td><?=$row->drug5_name?></td>
				<td align="center"><?=($row->drug5_dose) ? $row->drug5_dose : '';?></td>
				<td align="center"><?=($row->drug5_dose) ? @number_format($row->drug5_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug6_name) { ?>
			<tr>
				<td><?=$row->drug6_name?></td>
				<td align="center"><?=($row->drug6_dose) ? $row->drug6_dose : '';?></td>
				<td align="center"><?=($row->drug6_dose) ? @number_format($row->drug6_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			</table>
		
			</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
	<?	} //while
		echo "</table>";
	} //else
} else {
?>

<!-- 
<style>
  td {font-size:13px;}
</style>
 -->

<!-- <p><strong>Syringe driver survey database search page</strong></p> -->
<p><h4>Syringe driver survey database search page</h4></p>
<p>Please select the drug combination to search for below, <a href="sddb_index.php">browse the index</a>, 
 <a href="sddb_submit.php">submit an entry</a>,
 <a href="/sddbsurvey/sddb_submit.php">submit a survey entry</a>
 or <a target="_top" href="/syringe-driver-database-introduction.html">go back to SDSD home page</a></p>
<form name="frmSddb" action="sddb.php" method="POST">
<table border="0" cellspacing="0" cellpadding="4">
<tr>
	<td><p>Drug 1</p></td>
	<td>
		<?	
			$a[] = "";
			$drugs = array_merge($a,file($_SERVER['DOCUMENT_ROOT']."/legacy/content/snippets/sddb_drug_list.txt"));
			arrayToListBox("drug1",$drugs,"", $extra="");
		?>	
	</td>
</tr>
<tr>
	<td><p>Drug 2</p></td>
	<td><?arrayToListBox("drug2",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td><p>Drug 3</p></td>
	<td><?arrayToListBox("drug3",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td><p>Drug 4</p></td>
	<td><?arrayToListBox("drug4",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td><p>Drug 5</p></td>
	<td><?arrayToListBox("drug5",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td><p>Drug 6</p></td>
	<td><?arrayToListBox("drug6",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td colspan="2" align="center">
		<input type="submit" value="find">		
	</td>
</tr>
</table>
</form>
<? } //else ?> 
</div>