<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/misc_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php'); 

if (IsSet($_GET["nlid"])) {
	$nlid = $_GET["nlid"];
} else {
	$sql = "SELECT id from newsletter where live <> 0 order by id desc limit 1";
	$rs = mysql_query($sql) or die(mysql_error(). "<br>$sql");
	$row = mysql_fetch_object($rs);
	$nlid = $row->id;
	mysql_free_result($rs);
}
if ($nlid==0) {
	$sql = "SELECT id from newsletter where live <> 0 order by id desc limit 1";
	$rs = mysql_query($sql) or die(mysql_error(). "<br>$sql");
	$row = mysql_fetch_object($rs);
	$nlid = $row->id;
	mysql_free_result($rs);
}

$bShowArticles = TRUE;

$sql = "SELECT * FROM newsletter WHERE id=" .$nlid;
$rs = mysql_query($sql) or die(mysql_error(). "<br>$sql");
$newsletter = mysql_fetch_object($rs);
mysql_free_result($rs);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>&copy; palliativedrugs.com</title>
	<LINK REL="stylesheet" TYPE="text/css" HREF="/legacy/palliative.css">
	<script language=Javascript>
	<!--
	function mkOpenFeaturlessWin2(page, w, h) {
	//tested: ie5, N4.7(ignores size params), N6.2, Opera6
	//example call: <a href="javascript:mkOpenFeaturlessWin('newwin.htm',300,300);">new win</a>
	attributes ='toolbar=no,location=no,directories=no,menubar=no,scrollbars=yes,resizable=yes,'
	attributes+='width=' + w + ', height=' + h 
	new_win = window.open(page,"",attributes)
	}
	// -->
	</script>
</head>
<body bgcolor=#ffffff>
<table bgcolor="#CE0021" bordercolor="#CE0021" width="94%" align="center" cellpadding="0">
<tr><td>
	<table width="100%" cellspacing="0" cellpadding="0">
	<tr bgcolor=#CE0021>
		<td><font color="#ffffff">Newsletter</font></td>
		<td align="right">
			<font color="#ffffff"><?=$newsletter->TextDate?></font>
		</td>	
	</tr>
	</table>	
</td></tr>
</table>

<p><br>
<?=StripSlashes($newsletter->IntroText)?>
</p><br>
<hr size=1 width=94%>
<p><b>CONTENTS</b></p>
<div style="width:535" class="unnamed1">
<p>
<?
$sql = "SELECT * FROM newsarticle WHERE newsletter_id = $nlid AND live <> 0 ORDER BY ord";
$rsArticles = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_object($rsArticles)) {
	echo "<a href='newsletter.php?nlid=$nlid#art$row->id'>".trim($row->headline)."</a>";
	echo trim(str_replace(chr(13),"<br>",StripSlashes($row->abstract))) . "<br><br>";
}
echo "<hr size=1 width=94%><p>";

if ($bShowArticles==TRUE) {
	mysql_data_seek($rsArticles,0);
	while($row=mysql_fetch_object($rsArticles)) {
		echo "<a name='art$row->id'><br></a><b>$row->headline</b><br>";
		
		$s1 = str_replace("http://www.palliativedrugs.net/","/",StripSLashes($row->body));
		$s2 = str_replace("http://palliativedrugs.net/","/",$s1);
		$s3 = str_replace(chr(13),"<br>",$s2) ;
		$s4 = str_replace('/newsletter/',"/legacy/newsletter/",$s3) ;
		echo $s4 . "<br><br>";
	}
}
?>
</p>
</div>
<br>
<hr size=1 width=94%>
<p>
To unsubscribe from mailings informing you of the publication of this newsletter go to the site, enter your username and password and select 'preferences' on the left of the screen. At the bottom of the page of your registration details are two check boxes, one for bulletinboard 
messages, the other for newsletters. Click on the tick mark and then click on 'update' to save the change in details.<br>
</p> 
<p align=center>
Please note: The terms and conditions of the website apply to the content of this newsletter.</p>
</body>
</html>
