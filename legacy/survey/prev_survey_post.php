<?
require_once("$DOCUMENT_ROOT/_include/config.inc.php");
require_once("$DOCUMENT_ROOT/_include/db_func.inc.php");
require_once("$DOCUMENT_ROOT/_include/visitor.class.php");
session_start();

$sid = $_POST["hidSid"];
$q_count = $_POST["hidQcount"];

$username = dLookUp("username","users","id=$visitor->id");

for ($i=1; $i<=$q_count; $i++) {
	$qid = $_POST["hidQid".$i];
	
	if (isset($_POST["hidMoc".$i])) {
		$tmp = "";
		$count = $_POST["hidMoc".$i];
		for ($j=1; $j<=$count; $j++) {
			$fldname = "fc" . $i . '_' . $j;
			if (IsSet($_POST[$fldname])) {
					$tmp .= strval($j).", ";
				}
		}
		$fld_val = substr($tmp,0,strlen($tmp)-2);
	} elseif (isset($_POST["ff".$i])) {
		$fld_val = $_POST["ff".$i];
	} else {
		$fld_val = "0";
	}
	
	//echo "Q=$i, Qid=$qid, a=$fld_val<br>";
	
	if (strlen($fld_val) < 255) {
		if (strlen($fld_val)==0) {
			$db_val = "NONE";
		} else {
			$db_val = AddSlashes($fld_val);
		}
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, answer, username) "
			. "VALUES ($sid, $qid, '$db_val', '$username')";
	} else {
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, long_answer, username) "
			. "VALUES ($sid, $qid, '$db_val', '$username')";
	}//if strlen
	
	mysql_query($sql) or die(mysql_error() . "<hr>$sql");
	
}//for

//log user to prevent multiples.
$sql = "INSERT INTO survrespondent (survey_id, username) VALUES ($sid, '$username')";
mysql_query($sql) or die(mysql_error() . "<hr>$sql");

// get ack msg from db
$sql = "SELECT acknowledgement_msg FROM survsurvey WHERE id=$sid";
$rs = mysql_query($sql) or die(mysql_error()."<br>$sql");
$row = mysql_fetch_object($rs);
$ack_msg = $row->acknowledgement_msg;

mysql_free_result($rs);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>&copy; 2002 palliativedrugs.com</title>
	<LINK REL="stylesheet" TYPE="text/css" HREF="/palliative.css">
</head>
<body>
<img src="../newsletter/images/toplogo.gif" width="251" height="70" border="0" alt=""><br>
<br>
<p>
<?=$ack_msg?>
</p>
<br>
<br>
 
<!--- NOTE: might not always be called from newsletter, so need to accept parameter here for link-back ... --->
<div align=center>
<a href="/newsletter/index.php" target="mid">back to newsletter</a>
</div>
</body>
</html>
