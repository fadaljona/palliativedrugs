<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$sid = $_POST['hidSid'];
$q_count = $_POST['hidQcount'];

$username = dLookUp("username","new_users","id=".$_SESSION['visitor']->id);

//echo "sid=" . $sid . "<br />";
//echo "user=" . $username;
//exit();

for ($i=1; $i<=$q_count; $i++) {
	$qid = $_POST["hidQid".$i];

	if (isset($_POST["hidMoc".$i])) {
		$tmp = "";
		$count = $_POST["hidMoc".$i];
		for ($j=1; $j<=$count; $j++) {
			$fldname = "fc" . $i . '_' . $j;
			if (IsSet($_POST[$fldname])) {
					$tmp .= strval($j).", ";
				}
		}
		$fld_val = substr($tmp,0,strlen($tmp)-2);
	} elseif (isset($_POST["ff".$i])) {
		$fld_val = $_POST["ff".$i];
	} else {
		$fld_val = "0";
	}

	if (is_array($fld_val)) {
		$fld_val = implode("|",$fld_val);
	}

	//echo "Q=$i, Qid=$qid, a=$fld_val<br>";

	if (strlen($fld_val) < 255) {
		if (strlen($fld_val)==0) {
			$db_val = "NONE";
		} else {
			$db_val = $fld_val;
		}
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, answer, username) "
			. "VALUES ($sid, $qid, '$db_val', '$username')";
	} else {
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, long_answer, username) "
			. "VALUES ($sid, $qid, '$db_val', '$username')";
	}//if strlen

	mysql_query($sql) or die(mysql_error() . "<hr>$sql");
	//echo $sql."<br><br>";
}//for

//log user to prevent multiples.

if($username!='aw') {
	$sql = "INSERT INTO survrespondent (survey_id, username) VALUES (" . $sid . ", '" . $username. "')";
	mysql_query($sql) or die(mysql_error() . "<hr>$sql");
}

// get ack msg from db
$sql = "SELECT acknowledgement_msg FROM survsurvey WHERE id=$sid";
$rs = mysql_query($sql) or die(mysql_error()."<br>$sql");
$row = mysql_fetch_object($rs);
$ack_msg = $row->acknowledgement_msg;

mysql_free_result($rs);
echo "<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"/css/legacy.css\">";
echo "<p>" . $ack_msg . "</p>";
//header('Location:  /survey-acknowledgment');
exit();
?>