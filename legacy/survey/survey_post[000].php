<?
require_once("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/config.inc.php");
require_once("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/db_func.inc.php");
require_once("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/visitor.class.php");
session_start();

$sid = $HTTP_POST_VARS["hidSid"];
$q_count = $HTTP_POST_VARS["hidQcount"];

$username = dLookUp("username","users","id=".$_SESSION[visitor]->id);

for ($i=1; $i<=$q_count; $i++) {
	$qid = $HTTP_POST_VARS["hidQid".$i];

	if (isset($HTTP_POST_VARS["hidMoc".$i])) {
		$tmp = "";
		$count = $HTTP_POST_VARS["hidMoc".$i];
		for ($j=1; $j<=$count; $j++) {
			$fldname = "fc" . $i . '_' . $j;
			if (IsSet($HTTP_POST_VARS[$fldname])) {
					$tmp .= strval($j).", ";
				}
		}
		$fld_val = substr($tmp,0,strlen($tmp)-2);
	} elseif (isset($HTTP_POST_VARS["ff".$i])) {
		$fld_val = $HTTP_POST_VARS["ff".$i];
	} else {
		$fld_val = "0";
	}

	if (is_array($fld_val)) {
		$fld_val = implode("|",$fld_val);
	}

	//echo "Q=$i, Qid=$qid, a=$fld_val<br>";

	if (strlen($fld_val) < 255) {
		if (strlen($fld_val)==0) {
			$db_val = "NONE";
		} else {
			//$db_val = AddSlashes($fld_val);
			$db_val = $fld_val;
		}
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, answer, username) "
			. "VALUES ($sid, $qid, '$db_val', '$username')";
	} else {
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, long_answer, username) "
			. "VALUES ($sid, $qid, '$db_val', '$username')";
	}//if strlen

	mysql_query($sql) or die(mysql_error() . "<hr>$sql");
	//echo $sql."<br><br>";
}//for

//log user to prevent multiples.
if(!$username=='aw') {
	$sql = "INSERT INTO survrespondent (survey_id, username) VALUES ($sid, '$username')";
	mysql_query($sql) or die(mysql_error() . "<hr>$sql");
}


// get ack msg from db
$sql = "SELECT acknowledgement_msg FROM survsurvey WHERE id=$sid";
$rs = mysql_query($sql) or die(mysql_error()."<br>$sql");
$row = mysql_fetch_object($rs);
$ack_msg = $row->acknowledgement_msg;

mysql_free_result($rs);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>&copy; 2003 palliativedrugs.com</title>
	<LINK REL="stylesheet" TYPE="text/css" HREF="/palliative.css">
</head>
<body>
<img src="../newsletter/images/toplogo.gif" width="251" height="70" border="0" alt=""><br>
<br>
<p>
<?=$ack_msg?>
</p>
<br>
<br>

<!--- NOTE: might not always be called from newsletter, so need to accept parameter here for link-back ... --->
<div align=center>
<?
if ($HTTP_POST_VARS['hidRtn']=="newsletter") {
	echo "<a href=\"/newsletter/index.php\" target=\"mid\">back to newsletter</a>";
} elseif ($HTTP_POST_VARS['hidRtn']=="RAG_main") {
	echo "<a href=\"/RAG_main.php\">view main RAG page</a>";
} elseif ($HTTP_POST_VARS['hidRtn']=="home") {
	echo "<a href=\"/pdi.html\">www.palliativedrugs.com home page</a>";
}
?>
</div>
</body>
</html>