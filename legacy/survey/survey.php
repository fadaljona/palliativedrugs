<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php'); 

if (IsSet($_GET["sid"])) {
	$sid = $_GET["sid"];
} else {
	$sid = dLookUp("id","survsurvey", "is_default=1");
}

// detect refering page for return link ...
$rtn = $_SERVER['HTTP_REFERER'];
if (strpos($rtn,"sidebar.php")) {
	$return_page = "newsletter";
} elseif (strpos($rtn,"RAG_main.php")) {
	$return_page = "RAG_main";
} else {
	$return_page = "home";
}

//echo "[$sid]";

$sql = "SELECT id, title, intro, groupname FROM survsurvey WHERE id=$sid";
$rs = mysql_query($sql) or die ("survey error:<br>". mysql_error() ."<br>[$sql]");
$row_survey = mysql_fetch_object($rs);
mysql_free_result($rs);

//new bit to restrict access by group
if($row_survey->groupname) {
	if(!is_member($_SESSION['visitor']->id, $row_survey->groupname)) {	
		echo "<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"/palliative.css\">";
		echo "<p>Access to this survey is restricted.</p>";
		die();	
	}	
}
$sql = "SELECT id, type, num_rows, title_text, main_text, ord, required, continuation "
	. "FROM survquestion WHERE survey_id = $sid ORDER BY ord";
$rs = mysql_query($sql) or die ("survey error:<br>". mysql_error() ."<br>[$sql]");
?>
<!-- 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>&copy; 2003 palliativedrugs.com</title>
	<LINK REL="stylesheet" TYPE="text/css" HREF="/palliative.css">
	 -->
	<SCRIPT language=JavaScript > 
    var submitcount=0;  
    function checkSubmit() {    
    	if (submitcount == 0)  { 
			//submitcount++;
			 return true;
		} else {
		 	 alert('Your form has already been submitted, please wait for it to be processed.' );
			 submitcount--;
			  return false; 
		}
	}
    </SCRIPT>
    <!-- 
</head>
<body class="unnamed1">
 -->
<LINK REL="stylesheet" TYPE="text/css" HREF="/css/legacy.css">
<?
// see if user has voted already ...
$user = dLookUp("username","new_users","id=".$_SESSION['visitor']->id);
if (dLookUp("survey_id","survrespondent","survey_id=$sid AND username='$user'") != NULL) {
	echo "<p align=\"center\"><br><br>";
	echo "You have already completed this survey.<br><br><br>";
	echo "<a href=\"javascript:history.go(-1);\">back</a></p>";
} elseif ($sid==47) {
	echo "<p><span class=\"heading\">$row_survey->title</span><br><br>\n";
	echo str_replace(chr(13),"<br>",$row_survey->intro) . "<br><br><br>";
} elseif (mysql_num_rows($rs)==0) {
	echo "There is no survey currently active.";
} else {
	echo "<p><form action=\"/legacy/survey/survey_post.php\" method=\"post\" onSubmit=\" return checkSubmit();\">\n";
	$nSurveyId = $row_survey->id;
	
	echo "<input type=\"hidden\" name=\"hidSid\" value=\"$nSurveyId\">\n";
	
	echo "<input type=\"hidden\" name=\"hidRtn\" value=\"$return_page\">\n";

	
	echo "<span class=\"heading\">$row_survey->title</span><br><br>\n";
	echo str_replace(chr(13),"<br>",$row_survey->intro) . "<br><br><br>";
	echo "<input type=\"hidden\" name=\"hidQcount\" value=\"" . mysql_num_rows($rs) . "\">";
	$num_conts = 0;
	while ($row = mysql_fetch_object($rs)) {		//begin main questions loop	
		echo "<input type=\"hidden\" name=\"hidQid$row->ord\" value=\"$row->id\">";
		if ($row->continuation == TRUE) {
			$num_conts++;
			echo "<b>". (($row->ord) - $num_conts) . ". ". nl2br(StripSlashes($row->title_text)) ."</b><br>";
		} else {
			echo "<b>". (($row->ord) - $num_conts) . ". ". StripSlashes($row->title_text) ."</b><br>";
			
			if (($row->type == "freetext") AND (strpos($row->main_text,"|"))) {
				echo StripSlashes(substr($row->main_text,0,strpos($row->main_text,"|")));
			} else {
				echo nl2br(StripSlashes($row->main_text)) ."<br>";
			}
		}
		if ($row->type == "yes_no") {
			echo "<table>";
			echo "<tr><td>Yes</td><td><input type=\"radio\" name=\"ff$row->ord\" value=\"Yes\"></td></tr>";
			echo "<tr><td>No</td><td><input type=\"radio\" name=\"ff$row->ord\" value=\"No\"></td></tr></table><br>";			
		} elseif ($row->type == "freetext") {
			echo "<table><tr><td>";
			
			if (!strpos($row->main_text,"|")==FALSE) {
				$num_secs = substr_count($row->main_text,'|');
				$num_cols = round(60 / $num_secs);
				$secs = explode("|",$row->main_text);
				echo "<table width=100%><tr>";
				for($i = 1; $i <= $num_secs; $i++) {
					echo "<td>".StripSlashes($secs[$i])."</td>";
				}				
				echo "</tr><tr>";
				for($i = 1; $i <= $num_secs; $i++) {
					echo "<td><textarea name=\"ff$row->ord[$i]\" cols=\"$num_cols\" rows=\"$row->num_rows\"></textarea></td>";
				}
				
				
				echo "</tr></table>";
			} else {
				if ($row->num_rows == 1) {
					echo "<input type=\"text\" name=\"ff$row->ord\" value=\"\" size=\"80\">";
				} else {
					echo "<textarea name=\"ff$row->ord\" cols=\"60\" rows=\"$row->num_rows\"></textarea>";
				}
			}
			echo "</td></tr></table>";
		} elseif ($row->type == "one_of") {
			//echo "[qid=$row->id]";
			$sql = "SELECT answer, ord, is_default FROM survanswers WHERE question_id=$row->id ORDER BY ord";
			$rs_answers = mysql_query($sql)or die ("survey error:<br>". mysql_error() ."<br>[$sql]");			
			echo "<font size=-2>please select one -</font>";
			echo "<table >";
			while ($ans = mysql_fetch_object($rs_answers)) {	
				echo "<tr><td>$ans->answer</td>";
				echo "<td><input type=\"radio\" name=\"ff$row->ord\" value=\"$ans->ord\"></td></tr>";
			}
			echo "</table>";
		} elseif ($row->type == "many_of") {
			$sql = "SELECT answer, ord, is_default FROM survanswers WHERE question_id=$row->id ORDER BY ord";
			$rs_answers = mysql_query($sql);			
			echo "<font size=-2>tick all that apply -</font><table>";
			$i = 0;
			while ($ans = mysql_fetch_object($rs_answers)) {	
				echo "<tr><td>$ans->answer</td>";
				echo "<td><input type=\"checkbox\" name=\"fc".$row->ord."_".$ans->ord."\" ></td></tr>";
				$i++;
			}
			echo "<input type=\"hidden\" name=\"hidMoc$row->ord\" value=\"$i\"></table>";
		} else {
			//unkown qtype ...
			echo "\n<!-- dbt:survey - unknown question type. -->\n";
		}
		echo "<br><br>";
	} //while
	echo "<input type=\"submit\" value=\"submit\">";
	echo "<input type=\"reset\">";

	if (($_SERVER['HTTP_REFERER']) && (!strpos($_SERVER['HTTP_REFERER'],'login.php'))) {
		echo "<input type=\"button\" value=\"cancel\" onClick=\"Javascript:history.go(-1);\">";
	} else {
		echo "<input type=\"button\" value=\"Exit without submitting\" onClick=\"Javascript:window.location='/';\">";
	}
	echo "</form></p>";
}
?>		
</body>
</html>
