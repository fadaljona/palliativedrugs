<?php
require_once('_include/pall_db.inc.php');
require_once('_include/visitor.class.php');
echo "<p>";

if($_SERVER['REQUEST_METHOD']=='POST') {
	//	die ('<pre>' . print_r($_POST,TRUE) . '</pre>');
	
	$qs = (IsSet($_POST['quicksearch'])) ? TRUE : FALSE;
	
	$search_for = trim($_POST['search_for']);
	
	if(StrLen($search_for) <2 ) {
		echo "Please enter your topic into the search box";		
	} else {
	
		echo "Results in: <br />";
		if(IsSet($_POST['search_formulary']) || $qs) {
			echo "<br /><b>Formulary</b> (all)<br />";
			
			$sql = "SELECT id, pagetitle, alias, SUBSTRING(content,1,100) as trunc_content FROM modx_site_content "
					. "WHERE (pagetitle like '%" . $search_for . "%' "
					. "OR content like '%" . $search_for . "%') "
					. "AND searchable=1";
			$result = $modx->db->query($sql);
			while( $row = $modx->db->getRow( $result ) ) {
				$link = '/' . $row['alias'] . '.html';
				echo '&nbsp;&nbsp;&nbsp;<a href="' . $link . '">' . $row['pagetitle'] . '</a><br />';
				//echo $row['trunc_content'] . '<br /><br />';
			}
		}
		
		if(IsSet($_POST['search_bb']) || $qs) {
			echo "<br /><br /><b>Bulletin board</b> (up to 50 most recent)<br /><br />";
			
			/*$sql = "SELECT forum_pall.id, forum_pall.thread, subject, datestamp, SUBSTRING(body,1,160) AS tbody FROM forum_pall, forum_pall_bodies"
					. " WHERE forum_pall_bodies.thread = forum_pall.id "
					. " AND approved='Y'"
					. " AND (subject like '%" . $search_for . "%' "
					. " OR body like '%" . $search_for . "%') "
					. " ORDER BY datestamp DESC ";*/
			$sql = "SELECT message_id, thread, subject, datestamp, SUBSTRING(body,1,160) AS tbody FROM phorum_messages "
					. " WHERE 1 "
					. " AND status='2'"
					. " AND (subject like '%" . $search_for . "%' "
					. " OR body like '%" . $search_for . "%') "
					. " ORDER BY datestamp DESC ";

	//		if(($qs) || ())
					
	$sql 				.= " LIMIT 50";
			$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
			while($row=$pall_db->getRow($rs)) {
				
				//$link = "/forum-read?&f=1&i=".$row['id']."&t=".$row['thread'];
				$link = "/forum-read?1,".$row['thread'].",".$row['message_id']."#msg-".$row['message_id'];
				
				echo '<a href="'.$link.'">' . $row['subject'] . '</a> [' . substr($row['datestamp'],0,10) . ']<br />';
				echo $row['tbody'] . '...<br /><br />';
			}
		}
		
		if(IsSet($_POST['search_doclib']) || $qs) {
			echo "<br /><br /><b>Document library</b> (up to 30 most recent)<br />";
			
			$sql = "SELECT id, title, entry_id, DateAdded, SUBSTRING(notes,1,160) AS tnotes FROM rag_doc"
					. " WHERE (title like '%" . $search_for . "%' "
					. " OR notes like '%" . $search_for . "%') "
					. " ORDER BY DateAdded DESC "
					. " LIMIT 50";
			$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
			while($row=$pall_db->getRow($rs)) {
				
				$link = "/doc-library?&entryid=".$row['entry_id'];
				
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$link.'">' . $row['title'] . '</a> [' . substr($row['DateAdded'],0,10) . ']<br />';
			//	echo $row['tnotes'] . '<br /><br />';
			}
		}
		
		if(IsSet($_POST['search_news']) || $qs) {
			echo "<br /><br /><b>News</b> (up to 30 most recent)<br />";
			
			$sql = "SELECT news_id, news_date, category, title,DATE_FORMAT(news_date,'%M') as nmonth FROM 3bit_news"
					. " WHERE type='news' "
					. " AND (title like '%" . $search_for . "%' "
					. " OR alias_title like '%" . $search_for . "%' "
					. " OR content like '%" . $search_for . "%') "
					. " ORDER BY news_date DESC "
					. " LIMIT 30";
			$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
			while($row=$pall_db->getRow($rs)) {
				
				if($row['alias_title']!='')
					$link = '/news/' .remSpecial($row['nmonth']).'/'.remSpecial($row['alias_title']).'.html';
				else
					$link = '/news/' .remSpecial($row['nmonth']).'/'.remSpecial($row['title']).'.html';
				
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$link.'">' . $row['title'] . '</a> ['
				 . substr($row['news_date'],0,10) . ']'
				 . ' [' . $row['category'] . ']<br />';

			}
		}
		
		if(IsSet($_POST['search_latest']) || $qs) {
			echo "<br /><br /><b>Latest additions</b> (up to 30 most recent)<br />";
			
			$sql = "SELECT news_id, news_date, category, title,alias_title,DATE_FORMAT(news_date,'%M') as nmonth FROM 3bit_news"
					. " WHERE type='addition' "
					. " AND (title like '%" . $search_for . "%' "
					. " OR alias_title like '%" . $search_for . "%' "
					. " OR content like '%" . $search_for . "%') "
					. " ORDER BY news_date DESC "
					. " LIMIT 30";
			$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
			while($row=$pall_db->getRow($rs)) {
				
				if($row['alias_title']!='')
					$link = '/news/' .remSpecial($row['nmonth']).'/'.remSpecial($row['alias_title']).'.html';
				else
					$link = '/news/' .remSpecial($row['nmonth']).'/'.remSpecial($row['title']).'.html';
				
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$link.'">' . $row['title'] . '</a> ['
				 . substr($row['news_date'],0,10) . ']'
				 . ' [' . $row['category'] . ']<br />';			
			}
		}
	}
} // if($_SERVER['REQUEST_METHOD']=='POST')
?>
</p>
<!-- 
<p>
<br /><br />
<b>Advanced search</b>
<div style="align:center;">
<form action="advanced-search" method="POST">
<table border="0" cellpadding="8" align="center">
	<tbody>
		<tr>
			<td>&nbsp;Search for</td>
			<td><input name="search_for" size="30" type="text" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td style="font-size:1em;"><input name="words" value="all" type="radio" checked/>all words<br /><input name="words" value="any" type="radio" />any words</td>
		</tr>
		<tr>
			<td>&nbsp;Search in</td>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
				<tr><td valign="middle" style="font-size:.7em;"><input name="search_formulary" type="checkbox" checked /> Formulary</td></tr>
				<tr><td valign="middle" style="font-size:.7em;"><input name="search_bb" type="checkbox" checked /> Bulletin board</td></tr>
				<tr><td valign="middle" style="font-size:.7em;"><input name="search_doclib" type="checkbox" checked /> Document library</td></tr>
				<tr><td valign="middle" style="font-size:.7em;"><input name="search_news" type="checkbox" checked /> News</td></tr>
				<tr><td valign="middle" style="font-size:.7em;"><input name="search_latest" type="checkbox" checked /> Latest additions</td></tr>
				</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<TD>Show</TD>
			<td style="font-size:1em;">
				<input name="recent" value="recent" type="radio" checked/>most recent<br />
				<input name="recent" value="all" type="radio" />all results
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
          <input name="search" type="submit" value="Search"></td>
		</tr>
	</tbody>
</table>
</form>
</div>
</p>
 -->
 <br />
<br />
<br />
<br />
<br />
