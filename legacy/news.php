<?php
require_once('_include/pall_db.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');


session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

$since = (IsSet($_GET['since']) ? $_GET['since'] : '30');
if($since=='last') {
	$from_date = $_SESSION['previous_visit'];
} elseif($since=='all') {
	$from_date = date("Y-m-d", mktime(0,0,0,1,1,1999));
} else {
	$from_date = date( "Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$since, date("y")) );
}
?>
<p>
Show news: from last <a href="news.html?since=30">30 days</a> (default), 
<a href="news.html?since=60">60 days</a>,
<a href="news.html?since=90">90 days</a>, 
<a href="news.html?since=last">last visit</a>
or <a href="news.html?since=all">all</a>
<br />
<br />
<a href="#hot">Hot topics</a><br />
<a href="#drugs">Drugs</a><br />
<a href="#pall">palliativedrugs.com</a><br />
<a href="#other">Other news</a><br />
<a href="newsletter-archive">Newsletter archive (previous format, 2000-2008)</a><br />
<?
echo "<br /><a name=\"hot\"></a><b>Hot topics</b><br />";
$sql = "SELECT * FROM news WHERE active=1 AND type='news' AND category='hot' AND date > '" . $from_date . "' ORDER BY date DESC";
$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
while($row=$pall_db->getRow($rs)) {
	echo '<br /><a name="' . $row['id'] . '"></a>';
	echo '<i>' . $row['leader'] . ' [' . substr($row['date'],0,10) . ']</i><br />';
	echo html_entity_decode($row['content']) . '<br />';
	if(strpos($row['link'],'ttp://')!=FALSE) {
		$target = 'target="_blank" ';
	} else { $target='';}
	if($row['links']) {
		echo $row['links'] . "<br />";
	} elseif($row['link']) {
		echo "<a $target href=\"" . $row['link'] . "\">click here to view</a><br />";
	}
}

echo "<br /><a name=\"drugs\"></a><b>Drugs</b><br />";
$sql = "SELECT * FROM news WHERE active=1 AND type='news' AND category='drug' AND date > '" . $from_date . "' ORDER BY date DESC";
$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
while($row=$pall_db->getRow($rs)) {
	echo '<br /><a name="' . $row['id'] . '"></a>';
	echo '<i>' . $row['leader'] . ' [' . substr($row['date'],0,10) . ']</i><br />';
	echo html_entity_decode($row['content']) .  '<br />';
	if(strpos($row['link'],'ttp://')!=FALSE) {
		$target = 'target="_blank" ';
	} else { $target='';}
	if($row['links']) {
		echo $row['links'] . "<br />";
	} elseif($row['link']) {
		echo "<a $target href=\"" . $row['link'] . "\">click here to view</a><br />";
	}
}

echo "<br /><a name=\"pall\"></a><b>palliativedrugs.com</b><br />";
$sql = "SELECT * FROM news WHERE active=1 AND type='news' AND category='pall' AND date > '" . $from_date . "' ORDER BY date DESC";
$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
while($row=$pall_db->getRow($rs)) {
	echo '<br /><a name="' . $row['id'] . '"></a>';
	echo '<i>' . $row['leader'] . ' [' . substr($row['date'],0,10) . ']</i><br />';
	echo html_entity_decode($row['content']) . '<br />';
	if(strpos($row['link'],'ttp://')!=FALSE) {
		$target = 'target="_blank" ';
	} else { $target='';}
	if($row['links']) {
		echo $row['links'] . "<br />";
	} elseif($row['link']) {
		echo "<a $target href=\"" . $row['link'] . "\">click here to view</a><br />";
	}
}

echo "<br /><a name=\"other\"></a><b>Other news</b><br />";
$sql = "SELECT * FROM news WHERE active=1 AND type='news' AND category='other' AND date > '" . $from_date . "' ORDER BY date DESC";
$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
while($row=$pall_db->getRow($rs)) {
	echo '<br /><a name="' . $row['id'] . '"></a>';
	echo '<i>' . $row['leader'] . ' [' . substr($row['date'],0,10) . ']</i><br />';
	echo html_entity_decode($row['content']) . '<br />';
	if(strpos($row['link'],'ttp://')!=FALSE) {
		$target = 'target="_blank" ';
	} else { $target='';}
	if($row['links']) {
		echo $row['links'] . "<br />";
	} elseif($row['link']) {
		echo "<a $target href=\"" . $row['link'] . "\">click here to view</a><br />";
	}
}
echo "</p>";
?>