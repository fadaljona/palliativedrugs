<?php

function GetArrayOfMonographs() {
	$dir = opendir("../content/monographs");
	while ($file = readdir($dir)) {
		$filename = $file;
		if (strpos($filename,".html")) {
			if (strpos($filename,"PPENDIX")) {
				//$mon[] = GetAppendixName(substr($filename,0,strpos($filename,".html")));
			} else {
				$mon[] = substr($filename,0,strpos($filename,".html"));
			}
		}
	}
	sort($mon);
	return $mon;
}

function GetArrayOfAppendicies() {
	$apx[] = "Anaphylactic shock";
	$apx[] = "Synopsis of pharmacokinetic data";
	$apx[] = "Prolongation of the QT interval in palliative care";
	$apx[] = "Cytochrome P450";
	$apx[] = "Drug-induced movement disorders";
	$apx[] = "Discoloured urine";
	$apx[] = "Taking controlled drugs abroad";
	$apx[] = "Special orders and named patient supplies";
	$apx[] = "Nebulised drugs";
	$apx[] = "Administering drugs via feeding tubes";
	$apx[] = "Syringe driver compatibility charts";
	return $apx;
}

function GetAppendixName($num) {
	switch($num) {
		case "APPENDIX1":
			return ("Anaphylactic shock");
			break;
		case "APPENDIX2":
			return ("Synopsis of pharmacokinetic data");
			break;
		case "APPENDIX3":
			return ("Prolongation of the QT interval in palliative care");
			break;
		case "APPENDIX4":
			return ("Cytochrome P450");
			break;
		case "APPENDIX5":
			return ("Drug-induced movement disorders");
			break;
		case "APPENDIX6":
			return ("Discoloured urine");
			break;
		case "APPENDIX7":
			return ("Taking controlled drugs abroad");
			break;
		case "APPENDIX8":
			return ("Special orders and named patient supplies");
			break;
		case "APPENDIX9":
			return ("Nebulised drugs");
			break;
		case "APPENDIX10":
			return ("Administering drugs via feeding tubes");
			break;
		case "APPENDIX11":
			return ("Syringe driver compatibility charts");
			break;
		default:
			return("appendix not found");
			break;
	}
}

function GetArrayOfCountries() {
global $HTTP_SERVER_VARS;
	$filename = $HTTP_SERVER_VARS["DOCUMENT_ROOT"] . "/legacy/content/snippets/countries.txt";
	$countries = file($filename);
	return $countries;
}
?>