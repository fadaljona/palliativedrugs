<?php
class objUserInfo {
	var $id; 
	var $username;
	var $name;
	var $role;
	var $org;
	var $country;
	
	//constructor
	function objUserInfo($id) {
		
		if($id > 32463) {
			$table = "new_users";
		} else {
			$table = "users";
		}
		
		$sql = "SELECT username, FirstName, LastName, title, role, speciality, organisation, country,email"
				. " FROM " . $table . " WHERE id = '$id' LIMIT 2";
		
		$rs = mysql_query($sql) or die(mysql_error());
		if(!$rs or mysql_num_rows($rs)!=1) {
			$this->username = 'unknown';
			$this->name = 'unknown';
			$this->role =  'unknown';
			$this->org =  'unknown';
			$this->country =  'unknown';
			$this->email =  'unknown';
			$this->id = $id;			
			//die("invalid username passed to 'objUserInfo'");
		} else {
			$row = mysql_fetch_object($rs);
			$this->username = $row->username;
			$this->name = str_replace("*","",$row->title . " " . $row->FirstName . " " . $row->LastName);
			$this->role = $row->role;
			$this->org = $row->organisation;
			$this->country = $row->country;
			$this->email = $row->email;
			$this->id = $id;
		}
	} //function objUserInfo
}//class objUserInfo
?>