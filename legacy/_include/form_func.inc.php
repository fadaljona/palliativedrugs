<?
function arrayToListBox_old($name,$array,$selected, $extra="") {
	echo "<select name=\"$name\"";
	if (IsSet($extra))
		 echo " $extra>";
	else 
		 echo ">";
	$num_opts=count($array);
	for ($i=0; $i<$num_opts; $i++) {
		echo "<option value=\"$array[$i]\"";
		if(trim($array[$i])==trim($selected)) 
			echo " selected";
		echo ">$array[$i]</option>\n";
	}
	echo "</select>\n";
}

function arrayToListBox($name,$array,$selected, $extra="") {
	
	echo "<select name=\"$name\"";
	if (IsSet($extra)) {
		 echo " $extra>";
	} else { 
		 echo ">";
	}
	
	foreach($array as $key=>$value) {
		if(is_integer($key)) {
			echo "<option value=\"$value\"";
		} else {
			echo "<option value=\"$key\"";
		}
		if(is_integer($key)) {
			if(trim($value)==trim($selected)) 
			echo " selected";
		} else {
			if(trim($key)==trim($selected)) 
				echo " selected";
		}
		echo ">$value</option>\n";
	}
	echo "</select>\n";
}


function AlphaNumAndLenTest($string, $min_len, $max_len) {
	$regexp = "/[A-Za-z0-9]{" . $min_len .",". $max_len. "}/i";
	if (preg_match($regexp,$string))
		return TRUE;
	else
		return FALSE;
}

function validEmail($email) { 
    if (preg_match("/[a-z0-9]+([-_\.]?[a-z0-9])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}/i", $email)){ 
        return TRUE; 
    } else { 
        return FALSE; 
    } 
} 
?>
