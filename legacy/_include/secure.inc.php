<?php
function IsAllowed($acl,$allowed) {
	// tests if string ($acl) can be found in comma delimited list of strings ($allowed)
	
	$result = FALSE;
	$allowed_array = explode(',', $allowed);
	for ($i = 0; $i < count($allowed_array); $i++) {
		$this_opt = each($allowed_array);		
		if ($this_opt[1]==$acl) {
			$result = TRUE;
		}
	}
	return $result;
}

// entry point /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (IsSet($_SESSION['visitor'])) {
	if (!IsAllowed($_SESSION['visitor']->acl,$allowed)) {
		header("Location:  /access-denied.html");
		exit;
	}
} else {
	header("Location:  /access-denied.html");
	exit;	
}
?>