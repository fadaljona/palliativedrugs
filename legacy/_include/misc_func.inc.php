<?php
function BasicMailMerge($template, $tags) {
//global $DOCUMENT_ROOT;
	$result = implode("",(file($_SERVER['DOCUMENT_ROOT'] . $template)));
	foreach ($tags as $key => $value) {
		$tag = '{'.$key.'}';
		$result = str_replace($tag, $value, $result);
	}
	return $result;
}

function GetSnippet($snippet) {
	$file = $_SERVER['DOCUMENT_ROOT'] .'/legacy/content/snippets/' . $snippet;	
	$result = implode("",file($file));	
	return $result;
}
?>
