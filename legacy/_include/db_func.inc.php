<?php 
//  --------------------------------------------------------
//	name: db_func.inc.php
//	description: useful db functions
//	author: mkirk@aejent.co.uk
//	created: 23-jan-2002
// 	---------------------------------------------------------
// 	modified:13-apr-2002
//
//  ---------------------------------------------------------


// good old dLookUp function ....	
function dLookUp($field,$table,$where) {
	$sql = "SELECT $field FROM $table WHERE $where";
	$rs = mysql_query($sql); // or die ("dlookup error:<br>". mysql_error() ."<br>[$sql]");
	if (mysql_num_rows($rs)==0) {
		return NULL;
	} else {
		$row=mysql_fetch_array($rs);
		return $row[$field];
	}
}

function is_member($user, $group) {
	$found = false;
	$fn = $_SERVER['DOCUMENT_ROOT'] . '/content/groups/' . $group;	
	$group_array = file($fn);
	foreach($group_array as $id) {
//		echo $id . ", " . $user . "</br>";
		if(trim($id)==$user) { $found = true; }
	}
	return $found;	
}

function GetDbTimestamp() {
	$now = getdate();
	$mday = $now['mday'];
	$month = $now['mon'];
	$year = $now['year'];
	$hours = $now['hours'];
	$minutes = $now['minutes'];
	$seconds = $now['seconds'];
	$timestamp = "$year-$month-$mday $hours:$minutes:$seconds";
	return $timestamp;
}

function DbDateTimeToUnixTime($datetime) {
	if(!IsSet($datetime)) {
		return "error";
	} else {
		$split = explode(" ", $datetime);
		$date = explode("-", $split[0]);
		$time = explode(":", $split[1]);
		$unixtime = date("U", mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]));
		return $unixtime;
	} 
}

function FormatDbDateTime($dt) {
	if(!IsSet($dt)) {
		return "---";
	} else {
		return date("d-M-Y",DbDateTimeToUnixTime($dt));
	}
}

function selbox($name, $attr, $sql, $default, $top, $bot) {
// $name = name given to select box
// $attr = additional attributes to place in the select tag
// $sql = query used to retrieve values (fld0=value [, fld1=label])
// $default = pre-selected value
// $top = extra option to add at top (format="val|option" or just "val" if same)
// $bot = extra option to add at bottom (format as above)
	$rs=mysql_query($sql) or die("selbox error: " . mysql_error());
	$result = "<select name=\"$name\" $attr>";
	
	if (!$top=="") {
		$pos = strpos($top, "|");
		if ($pos === false) { // note: three equal signs
    		$result .= "<option value=\"$top\">$top</option>\n";
		} else {
			$arr=explode("|",$top);
			$result .= "<option value=\"$arr[0]\">$arr[1]</option>\n";
		}
	}
	while ($row = mysql_fetch_array($rs)) {
		if (!isset($row[1])) { 
			$row[1] = $row[0];
		}
		$result .= "<option value=\"$row[0]\"";
		if ($row[0] == $default) {
			$result .= " selected";
		}
		$result .= ">$row[1]</option>\n";
	}	
	if (!$bot=="") {
		$pos = strpos($bot, "|");
		if ($pos === false) { // note: three equal signs
    		$result .= "<option value=\"$bot\">$bot</option>\n";
		} else {
			$arr=explode("|",$bot);
			$result .= "<option value=\"$arr[0]\">$arr[1]</option>\n";
		}
	}
	$result .= "</select>";
	return $result;
}


