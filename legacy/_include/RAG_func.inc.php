<?
// RAG functions ...

function IsRAGuser($userid = 0) {
global $HTTP_SESSION_VARS;
// if no param passed, use current Visitor
	if ($userid==0) {
		$userid = $HTTP_SESSION_VARS[visitor]->id;
	} 	
//	echo "<br>[".$userid.",".$HTTP_SESSION_VARS[visitor]->id."]<br>\n";
	$ragusr = DLookUp("user_id","rag_user","user_id=".$userid);		
	if ($ragusr) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function MonographSurvey($monograph) {
	$surv_id = dLookup("survey_id","RAG_survey","monograph='".$monograph."'");
	return $surv_id;
}

function NumSurveyResponses($survey_id) {
	$sql = "select distinct username from survresults where survey_id=$survey_id";
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$total_votes = mysql_num_rows($rs);
	mysql_free_result($rs);
	return $total_votes;
}

function HasUserTakenSurvey($survey_id, $userid = 0) {
global $HTTP_SESSION_VARS;
	if ($userid==0) {
		$userid = $HTTP_SESSION_VARS[visitor]->id;
	}	
	$username = dLookUp("username","users","id=".$userid);
	if (dLookUp("survey_id","survrespondent","survey_id=".$survey_id." AND username='".$username."'") != NULL) {
		return TRUE;
	} else {
		return FALSE;
	}
}

// added 29-jan-04
function ragAreAssociated($survey_id, $doc_id) {
	if (dLookUp("doc_id","rag_doc_survey","rag_survey_id=".$survey_id." AND doc_id='".$doc_id."'") != NULL) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function ragGetDocSurveys($docid) {
	$result = array();
	$sql = "SELECT rag_survey_id FROM rag_doc_survey WHERE doc_id=" . $docid;
	$rs = mysql_query($sql) or die($sql);	
	if ($rs) {
		while($row = mysql_fetch_object($rs)) {
			$result[] = $row->rag_survey_id;
		}
	mysql_free_result($rs);
	} 
	return $result;
}

function ragGetSurveyDocs($survid) {
	$result = array();
	$sql = "SELECT doc_id FROM rag_doc_survey WHERE rag_survey_id=" . $survid;
	$rs = mysql_query($sql) or die($sql);	
	if ($rs) {
		while($row = mysql_fetch_object($rs)) {
			$result[] = $row->doc_id;
		}
	mysql_free_result($rs);
	} 
	return $result;
}

?>