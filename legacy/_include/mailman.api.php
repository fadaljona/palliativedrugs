<?php
// MailMan API
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/misc_func.inc.php');

function mmAddBBMsgToQueue($msg_id) {

	//avoid posting dupes on page-refresh
	if (dLookUp("mm_queued","forum_pall","id=$msg_id")==1) {
		return TRUE;
		exit;
	}

	$received = date("Y-m-d H:i:s");
	$msg_from = "bulletinboard@palliativedrugs.com";
	$msg_replyTo = "bulletinboard@palliativedrugs.com";
	$recip_list = 3; //bb msg subscribers
	
	//get msg details
	$msg_subject = dLookUp("subject","forum_pall","id=$msg_id");
	$msg_body = dLookUp("body","forum_pall_bodies","id=$msg_id");
	$author_id = dLookUp("userid","forum_pall","id=$msg_id");
	
	//create msg header from user info
	$userinfo = new objUserInfo($author_id);	
	$header = "author: $userinfo->name\n"
			. "role: $userinfo->role\n"
			. "organisation: $userinfo->org, $userinfo->country\n";

	/*$sig = "(This message has been forwarded from the bulletin board. If you wish to respond please go to http://www.palliativedrugs.com and use the bulletin board facility on the site.
	To unsubscribe from the bulletin board go to the site and log on. Select 'preferences' at the bottom of the screen. At the bottom of the page of your registration details are two check boxes, one for bulletin board messages, the other for newsletters. Click on the tick mark and then click on 'update' to save the change in details.)";*/
	
	$sig = GetSnippet("bb_sig.txt");
	
	$msg_body = "$header\n$msg_body\n\n$sig";
	
	//put in db
	$SQL = "INSERT INTO mlrmailing";
	$SQL = $SQL . " (received, msg_from, msg_replyTo, msg_subject, msg_body, recip_list)";
	$SQL = $SQL . " VALUES (";
	$SQL = $SQL . "'" . $received . "', '" . AddSlashes($msg_from) ."', '". AddSlashes($msg_replyTo) . "', '" . AddSlashes($msg_subject) . "', '" . AddSlashes($msg_body) . "', " . $recip_list . ")";
	
	/*debug 
		echo "<textarea rows=30 cols=80>$SQL</textarea>";
		exit;
	// end-debug */
	
	if (mysql_query($SQL)) {
		$sql = "UPDATE forum_pall SET mm_queued = 1 WHERE id = $msg_id";
		mysql_query($sql);
		return TRUE;
	} else {
		return FALSE;
	}
}
?>