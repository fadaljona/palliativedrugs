<?
include("$DOCUMENT_ROOT/_include/config.inc.php");
include("$DOCUMENT_ROOT/_include/db_func.inc.php");
include("$DOCUMENT_ROOT/_include/form_func.inc.php");
require_once("$DOCUMENT_ROOT/_include/visitor.class.php");
session_start();
$allowed="admin";
require_once("$DOCUMENT_ROOT/_include/secure.inc.php");

If(IsSet($_GET["sid"])) {
	$sid = $_GET["sid"];
} else {
	echo "sid parameter missing";
	exit;
}

//results output functions (by qtype)
function survresManyOf($qid, $total_votes) {
	$sql="select answer from survanswers where question_id=$qid order by ord";
	$rsans=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$num_answers = mysql_num_rows($rsans);		
	$answer_array=array();
	for($i=1;$i<=$num_answers;$i++) {
		$answer_array[$i]=0;
	}
	$sql= "select answer from survresults where question_id=$qid and answer <> '0'";
	$rsans2=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	while($rowans=mysql_fetch_object($rsans2)) {
		$tmp = explode(",",$rowans->answer);			
		for($j=0; $j<count($tmp); $j++) {
			$answer_array[$tmp[$j]] = $answer_array[$tmp[$j]] +1;
		}
	}
	echo "<table width=\"100%\" cellpadding=\"1\" cellspacing=\"0\" bordercolor=\"#c0c0c0\" border=\"1\">";
	echo "<tr><td><i>answer</i></td><td><i>votes</i></td><td><i>% of vote</i></td><td>&nbsp;</td></tr>";
	for ($i=1; $i<=$num_answers; $i++) {
		$sql="select answer from survanswers where question_id=$qid and ord=$i";
		$rstmp = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		$arow = mysql_fetch_object($rstmp);
		$pcnt = ($answer_array[$i] / $total_votes) * 100;
		$pcnt = round($pcnt);
		$bar_width = $pcnt*5;
		$ans = str_replace("(please e-mail us <a href=\"mailto:druginformation@palliativedrugs.com\">druginformation@palliativedrugs.com</a>)","",$arow->answer);
		echo "<tr>";
		echo "<td>$ans</td>";
		echo "<td align=\"center\">$answer_array[$i]</td>";
		echo "<td align=\"center\">$pcnt</td>";
		echo "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>";
		echo "</tr>";
	}
	echo "</table>";
}

function survresFreeText($qid) {
	$sql = "SELECT answer, long_answer FROM survresults WHERE question_id=$qid";
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	while($row=mysql_fetch_object($rs)) {
		$blank=TRUE;
		if (strlen(trim($row->answer)) > 0) {
			echo trim($row->answer)."<br>";
			$blank = FALSE;
		} elseif (strlen(trim($row->long_answer)) > 0) {
			echo trim($row->answer)."<br>";
			$blank = FALSE;
		}
		//if($blank==TRUE) {
		//	echo "<hr size=\"1\" with=\"70%\">";
		//}
		echo "<hr size=\"1\" with=\"70%\">";
	}
}

function survresOneOf($qtype, $qid, $total_votes) {
	echo "<table width=\"100%\" cellpadding=\"1\" cellspacing=\"0\" bordercolor=\"#c0c0c0\" border=\"1\">";
	echo "<tr><td><i>answer</i></td><td><i>votes</i></td><td><i>% of vote</i></td><td>&nbsp;</td></tr>";
	$sql = "select answer, ord from survanswers where question_id=$qid order by ord";
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	while($row=mysql_fetch_object($rs)) {
		if($qtype=="yes_no") {
			$sql_votes="select count(*) as num_votes from survresults where question_id=$qid and answer='$row->answer'";
		} else {
			$sql_votes="select count(*) as num_votes from survresults where question_id=$qid and answer='$row->ord'";
		}
		//echo "<!--qType=$qtype, SQL=$sql-->";
			
		$rs_votes=mysql_query($sql_votes) or die(mysql_error()."<hr>".$sql_votes);
		$row_votes = mysql_fetch_object($rs_votes);
		$pcnt = ($row_votes->num_votes / $total_votes) * 100;
		$pcnt = round($pcnt);
		$bar_width=$pcnt*5;
		$ans = str_replace("(please e-mail us <a href=\"mailto:druginformation@palliativedrugs.com\">druginformation@palliativedrugs.com</a>)","",$row->answer);
		echo "<tr>";
		echo "<td>$ans</td>";
		echo "<td align=\"center\">$row_votes->num_votes</td>";
		echo "<td align=\"center\">$pcnt%</td>";
		echo "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>";
		echo "</tr>";
	}
	echo "</table>";
}

/*
#replace(qryAnswers.answer,"(please e-mail us <a href=""mailto:druginformation@palliativedrugs.com"">druginformation@palliativedrugs.com</a>)","","ALL")#
*/


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>palliativedrugs.com/survey results</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size="-2">admin</font></a> / 
<a href="survAdmin.php"><font size="-2">survey admin</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<font size="+1"><b>survey results</b></font><br>
<?
$sql = "select distinct username from survresults where survey_id=$sid";
$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
$total_votes = mysql_num_rows($rs);
mysql_free_result($rs);

if ($total_votes==0) {
	$sql = "select distinct username from survrespondent where survey_id=$sid";
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$total_votes = mysql_num_rows($rs);
	mysql_free_result($rs);
}

if ($total_votes==0) {
	echo "there are no respondents to this survey yet.";
	exit;
}

$sql = "SELECT title, active, date_published, closing_date, is_default FROM survsurvey WHERE id=$sid";
$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
$row = mysql_fetch_object($rs);
?>
<table border="1" cellspacing="0" cellpadding="4">
<tr>
	<td bgcolor="#c0c0c0">title</td>
	<td bgcolor="#ededed"><?=$row->title?></td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">active</td>
	<td bgcolor="#ededed">
	<? 
	if($row->active == 1) {
		echo "yes";
	} else {
		echo "no";
	}
	?>
	</td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">published</td>
	<td bgcolor="#ededed">
	<?
	if(IsSet($row->date_published)) {
		echo FormatDbDateTime($row->date_published);
	} else {
		echo "not published";
	}	
	?>
	</td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">closing date</td>
	<td bgcolor="#ededed">
	<?
	if(IsSet($row->closing_date)) {
		echo FormatDbDateTime($row->closing_date);
	} else {
		echo "none set";
	}
	?>
	</td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">respondents</td>
	<td bgcolor="#ededed"><?=$total_votes?></td>
</tr>
</table>
<br><br>
<b>questions:</b><br>
<table border="0" cellspacing="0" cellpadding="4" width="530">
<?
$sql = "SELECT id, type, title_text, ord, active FROM survquestion WHERE survey_id=$sid order by ord";
$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
while($row=mysql_fetch_object($rs)) {
	$qid = $row->id;
	$qtype = $row->type;
	echo "<tr><td>&nbsp;</td></tr>";
	echo "<tr bgcolor=\"#c0c0c0\">";
	echo "<td align=\"left\" colspan=\"2\">$row->ord) $row->title_text</td>";
	echo "<td align=\"right\">($qtype)</td>";
	echo "</tr>";
	echo "<tr bgcolor=\"#ededed\"><td colspan=\"3\"><table border=\"0\" width=\"100%\"><tr><td>";  //<td A>
	if($qtype=="many_of") {
		survresManyOf($qid, $total_votes);	
	} elseif($qtype=="freetext") {
		survresFreeText($qid);
	} else {
		survresOneOf($qtype, $qid, $total_votes);
	}
	echo "</td></tr></table>"; //<td A>
}
echo "</td></tr><tr><td colspan=3>&nbsp;</td></tr>";
echo "</table>";

?>
</body>
</html>
