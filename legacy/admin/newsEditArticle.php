<?php
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/config.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/db_func.inc.php");
require_once("$_SERVER[DOCUMENT_ROOT]/legacy/_include/visitor.class.php");
session_name('palliativedrugs');
session_start();
$allowed="admin";

require_once("$_SERVER[DOCUMENT_ROOT]/legacy/_include/secure.inc.php");

if ($REQUEST_METHOD=="POST") {
	$artid = $_POST["hidArtId"];
	$nlid = $_POST["hidNlid"];
	if ($artid==0) {
		$sql="SELECT MAX(ord) AS new_ord FROM newsarticle WHERE newsletter_id=$nlid";
		$rs=mysql_query($sql);
		$row=mysql_fetch_object($rs);
		if (IsSet($row->new_ord)) {
			$new_ord = $row->new_ord + 1;
		} else {
			$new_ord = 1;
		}
		mysql_free_result($rs);
		$vLive = ($_POST["chkLive"]) ? 1 : 0;
		$sql = "INSERT INTO newsarticle (newsletter_id, headline, abstract, body, ord, Live)"
			. " VALUES ($nlid, "
			. " '".AddSlashes($_POST[txtHeadline])."', "
			. " '".AddSlashes($_POST[txtAbstract])."', "
			. " '".AddSlashes($_POST[txtBody])."', "
			. " $new_ord, $vLive)";
		mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		header("Location: newsEditNewsletter.php?nlid=$nlid");
	} else {
		// it's an update
		$vLive = ($_POST["chkLive"]) ? 1 : 0;	
		$sql = "UPDATE newsarticle"
			. " SET headline='".AddSlashes($_POST[txtHeadline])."',"
			. " abstract='".AddSlashes($_POST[txtAbstract])."',"
			. " body='".AddSlashes($_POST[txtBody])."',"
			. "	live=$vLive"
			. " WHERE id=$artid";
		mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		header("Location: newsEditNewsletter.php?nlid=$nlid");
	}//if($artid==0)
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size=-2>admin</font></a> / 
<a href="newsAdmin.php"><font size=-2>newsletter admin</font></a> /
<a href="javascript:history.go(-1);"><font size="-2">edit newsletter</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<?
$artid = $_GET["artid"];
if ($artid==0) {
	echo "<font size=\"+1\"><b>New Article</b></font><br>";
	$vHeadline = "";
	$vAbstract = "";
	$vBody = "";
	$vLive = FALSE;
	$vNewsletter_id = $_GET["nlid"];
	
} else {
	echo "<font size=\"+1\"><b>Edit Article</b></font><br>";
	$sql = "SELECT * FROM newsarticle WHERE id=$artid";
	$rs = mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
	if (mysql_num_rows($rs)==0) echo "no rows";
	$row = mysql_fetch_object($rs);
	$vHeadline = StripSlashes($row->headline);
	$vAbstract = StripSlashes($row->abstract);
	$vBody = StripSlashes($row->body);
	$vLive = $row->live;
	$vNewsletter_id = $row->newsletter_id;
}
?>
<br>
<form action="<?=$PHP_SELF?>" method="POST">
<input type="hidden" name="hidArtId" value="<?=$artid?>">
<input type="hidden" name="hidNlid" value="<?=$vNewsletter_id?>">
<table border="1" cellspacing="0" cellpadding="4">
<tr>
	<td>headline</td>
	<td><input type="text" name="txtHeadline" value="<?=$vHeadline?>" size="80"></td>
</tr>
<tr>
	<td>abstract</td>
	<td><textarea name="txtAbstract" rows="8" cols="80"><?=$vAbstract?></textarea></td>
</tr>
<tr>
	<td>body</td>
	<td><textarea name="txtBody" rows="40" cols="80"><?=$vBody?></textarea></td>
</tr>
<tr>
	<td>live</td>
	<td>
		<? $checked = ($vLive) ? " checked" : ""; ?>
		<input type="checkbox" name="chkLive" <?=$checked?>>
	</td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" value="save">
		<input type="button" value="cancel" onClick="Javascript:history.go(-1);">
	</td>
</tr>
</table>
</form>
</body>
</html>
