<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/app_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/form_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/RAG_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

if ($_SERVER['REQUEST_METHOD']=='POST') {
	if(IsSet($_POST['view_details'])) {
		// view details ...
		
	} elseif(IsSet($_POST['delete_group'])) {
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/legacy/content/groups/' . $_POST['group'];
		unlink($filename);
		header("Location:  groups.php");
		exit();
	} elseif(IsSet($_POST['list_emails'])) {
		$group_text = StripSlashes($_POST['group_text']);
		$id_list = str_replace("\r\n",',',rtrim($group_text));
		$sql = "select email from new_users where id in ($id_list)";
		$rs = mysql_query($sql) or die(mysql_error());
		echo "<html><body><textarea rows=\"50\" cols=\"80\">";
		while($row=mysql_fetch_object($rs)) {
			echo $row->email . "; "; 	
		}
		echo "</textarea></body></html>";
		exit();
	} elseif (IsSet($_POST['chk_survey'])) {
		$full_group = split("\r\n",$_POST['group_text']);
		$sql = "SELECT distinct new_users.id FROM survresults, new_users " 
			. " WHERE survey_id = " . $_POST['sid']
			. " AND new_users.username = survresults.username";
		$rs = mysql_query($sql) or die(mysql_error());
		while($row=mysql_fetch_object($rs)) $participants[] = $row->id; 
		mysql_free_result( $rs );
		$non_participants = array_diff($full_group,$participants);
		sort($full_group);
		sort($participants);
		sort($non_participants);
//		die ("<hr size=\"5\"><pre>" . print_r($_POST,TRUE) . "</pre>");
	} else {  // save	
		$group = $_POST['group'];
		$group_text = StripSlashes($_POST['group_text']);
		$fp = fopen($_SERVER['DOCUMENT_ROOT']."/legacy/content/groups/".$group,"w");
		fputs($fp,$group_text);
		fclose($fp);
		header("Location:  groups.php");
		exit();
	}
}
$group = $_SERVER['QUERY_STRING'];
$group_text = implode("",file($_SERVER['DOCUMENT_ROOT']."/legacy/content/groups/".$group));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>palliativedrugs.com: admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>

<? if(IsSet($_POST['chk_survey'])) : ?>
<table width="70%" align="center">
<tr><th>participants (<?=count($participants)?>)</th><th>non-participants (<?=count($non_participants)?>)</th></tr>
<tr>
	<td align="center">
		<textarea rows="30" cols="30"><? foreach ($participants as $key => $val) echo $val . "\r\n"; ?></textarea>
	</td>
	<td align="center">
		<textarea rows="30" cols="30"><? foreach ($non_participants as $key => $val) echo $val . "\r\n"; ?></textarea>
	</td>
</tr>
</table>
<? elseif(!IsSet($_POST['view_details'])) : ?>
<strong>edit group</strong> - <?=$group?><br>
<br>
<form action="groups_edit.php" method="POST">
<input type="hidden" name="group" value="<?=$group?>">
<textarea name="group_text" cols="60" rows="15"><?=$group_text?></textarea>
<br>
<input type="submit" name="save" value="save"> 
<input type="submit" name="view_details" value="view details"/>
<input type="submit" name="list_emails" value="list emails"/>
<input type="submit" name="delete_group" value="delete group"/>
<input type="button" value="cancel" onClick="Javascript:history.go(-1);">
<br/><br/>
<input type="text" name="sid" size="4"> <input type="submit" name="chk_survey" value="check against survey"> 
</form>
<?  else : ?>
group details: <?=$_POST['group']?><br>
<?
$group_text = StripSlashes($_POST['group_text']);
$id_list = str_replace("\r\n",',',rtrim($group_text));
$sql = "select id, username, title, FirstName, LastName, role, organisation from new_users where id in ($id_list)";
echo "<table width=\"100%\">";
echo "<tr><th>id</th><th>username</th><th>name</th><th>organisation</th><th>view</th></tr>";	
$rs = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_object($rs)) {
	$view_link = "/code/phpMyEdit/new_users2.php?PME_sys_fl=0&PME_sys_fm=0&PME_sys_sfn[0]=0&PME_sys_operation=PME_op_View&PME_sys_rec=".$row->id;
	echo "<tr>";
	echo "<td>" . $row->id . "</td>";
	echo "<td>" . $row->username . "</td>";
	echo "<td>" . $row->title . " " . $row->FirstName . " " . $row->LastName . "</td>";
	echo "<td>" . $row->organisation . "</td>";
	echo "<td><a href=\"".$view_link."\">view</a></td>";	
	echo "</tr>";
}
echo "</table>";
?>

<? endif; ?>
</body>
</html>
