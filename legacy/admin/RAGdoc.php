<?php require_once('../Connections/cnPall.php'); ?>
<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/_include/app_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/_include/form_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<?php require_once('../_include/RAG_func.inc.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmDocument")) {
  $updateSQL = sprintf("UPDATE rag_doc SET entry_id=%s, title=%s, notes=%s, file_location=%s, organisation=%s, contact=%s, active=%s, audit=%s, DateAdded=%s WHERE id=%s",
                       GetSQLValueString($_POST['entry_id'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['file_location'], "text"),
                       GetSQLValueString($_POST['organisation'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString(isset($_POST['active']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['audit'], "int"),
                       GetSQLValueString($_POST['DateAdded'], "date"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($updateSQL, $cnPall) or die(mysql_error());

  $updateGoTo = "RAGentry.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if (isset($_POST['docid'])) {
	$docid = $_POST['docid'];
} else {
	$docid = $_GET['docid'];
}

if ((isset($_GET['docid'])) && ($_GET['docid'] != "") && (isset($_GET['del']))) {
  $deleteSQL = sprintf("DELETE FROM rag_doc WHERE id=%s",
                       GetSQLValueString($_GET['docid'], "int"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($deleteSQL, $cnPall) or die(mysql_error());

  $deleteGoTo = "RAGentry.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmDocument")) {
  $insertSQL = sprintf("INSERT INTO rag_doc (entry_id, title, notes, file_location, organisation, contact, active, DateAdded, audit) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['entry_id'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['file_location'], "text"),
                       GetSQLValueString($_POST['organisation'], "text"),
                       GetSQLValueString($_POST['contact'], "text"),
                       GetSQLValueString(isset($_POST['active']) ? "true" : "", "defined","1","0"),
					   GetSQLValueString($_POST['DateAdded'], "date"),
                       GetSQLValueString($_POST['audit'], "int"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($insertSQL, $cnPall) or die(mysql_error());

  $insertGoTo = "RAGentry.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
   $insertGoTo .= "docid=" . mysql_insert_id();
  header(sprintf("Location: %s", $insertGoTo));
}

$colname_rsDocument = "1";
if (isset($_GET['docid'])) {
  $colname_rsDocument = (get_magic_quotes_gpc()) ? $_GET['docid'] : addslashes($_GET['docid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsDocument = sprintf("SELECT * FROM rag_doc WHERE id = '%s'", $colname_rsDocument);
$rsDocument = mysql_query($query_rsDocument, $cnPall) or die(mysql_error());
$row_rsDocument = mysql_fetch_assoc($rsDocument);
$totalRows_rsDocument = mysql_num_rows($rsDocument);
?>
<html>
<head>
<title>rag:doc/audit</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0>
<tr>
    <td >
		<a href="index.php"><font size=-2>admin</font></a>
		/ <a href="RAGadmin.php"><font size=-2>RAG admin</font></a>
		/ <a href="RAGsection.php?secid=<?php echo $_GET['secid']; ?>"><font size=-2>RAG topic</font></a>
		/  <a href="RAGentry.php?secid=<?php echo $_GET['secid']; ?>&entryid=<?php echo $_GET['entryid']; ?>"><font size=-2>RAG content</font></a>
	</td>
  </tr>
</table>
<!-- end quick-nav bar -->
<p>
  <?php if ($_GET['audit']==1) { 
  	$audit  = 1;
	echo "RAG Audit";
} else { 
	$audit = 0; 
	echo "RAG Document";
}
?>
</p>
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmDocument" id="frmDocument">
  <table border="0" align="center" cellpadding="2" cellspacing="1">
    <tr> 
      <th>title</th>
      <td><input name="title" type="text" id="title" value="<?php echo $row_rsDocument['title']; ?>" size="80"></td>
    </tr>
    <tr> 
      <th>organisation</th>
      <td><input name="organisation" type="text" id="organisation" value="<?php echo $row_rsDocument['organisation']; ?>" size="80"></td>
    </tr>
    <tr> 
      <th>contact</th>
      <td><input name="contact" type="text" id="contact" value="<?php echo $row_rsDocument['contact']; ?>" size="60"></td>
    </tr>
    <tr> 
      <th>notes</th>
      <td><textarea name="notes" cols="70" rows="6" id="notes"><?php echo $row_rsDocument['notes']; ?></textarea></td>
    </tr>
    <tr>
      <th>date</th>
      <td><input name="DateAdded" type="text" id="DateAdded" value="<?php echo $row_rsDocument['DateAdded']; ?>" size="10" maxlength="10"></td>
    </tr>
    <tr> 
      <th>filename</th>
      <td><input name="file_location" type="text" id="file_location" value="<?php echo $row_rsDocument['file_location']; ?>" size="60"></td>
    </tr>
    <tr> 
      <th>active</th>
      <td><input name="active" type="checkbox" id="active" <?php echo ($row_rsDocument['active']==1) ? " checked" : ""; ?>>
        <input name="id" type="hidden" id="id" value="<?php echo $_GET['docid']; ?>">
		<input name="audit" type="hidden"  id="audit" value="<?php echo $audit ?>">
        <input name="entry_id" type="hidden" id="entry_id" value="<?php echo $_GET['entryid']; ?>"></td>
    </tr>
    <tr> 
      <td colspan="2" align="center"> 
        <?php if ($_GET['docid']>0) { ?>
        <input name="btnUpdate" type="submit" id="btnUpdate" value="update"> 
        <?php } // show if var=val ?>
        <?php if ($_GET['docid']==0) { ?>
        <input name="btnSave" type="submit" id="btnSave" value="save"> 
        <?php } // show if var=val ?>
      </td>
    </tr>
  </table>
  <?php if ($_GET['docid']>0) { ?>
  <input type="hidden" name="MM_update" value="frmDocument">
  <?php } // show if var=val ?>
  <?php if ($_GET['docid']==0) { ?>
  <input type="hidden" name="MM_insert" value="frmDocument">
  <?php } // show if var=val ?>
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsDocument);
?>

