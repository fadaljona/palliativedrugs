<?php
require_once("$DOCUMENT_ROOT/_include/config.inc.php");
require_once("$DOCUMENT_ROOT/_include/db_func.inc.php");

function q1($ans) {
     switch ($ans) {
       case 1:
         	return "Doctor";
         break;
       case 2:
            return "Nurse";
         break;
       case 3:
            return "Pharmacist";
         break;
       case 4:
	   		return "Librarian";
		 break;
	   case 5:
            return "Other";
         break;
       default:
       		return "NA";
            break;
     }
}

function q2($ans) {
     switch ($ans) {
       case 1:
         	return "Specialist Palliative Care Unit/Hospice";
         break;
       case 2:
            return "Hospital";
         break;
       case 3:
            return "Community";
         break;
       case 4:
	   		return "Nursing Home";
		 break;
	   case 5:
            return "Other";
         break;
       default:
       		return "NA";
            break;
     }
}

$sql = "select survresults.username, role, speciality, organisation, addr1, country, "
		. "title_text as question, answer"
		. " from survresults, users, survquestion"
		. " where survresults.survey_id = 22"
		. " and survresults.username = users.username"
		. " and survquestion.id = survresults.question_id";
//        . " and country!='United Kingdom'";
$rs = mysql_query($sql) or die(mysql_error() . "<hr>$sql<hr>");

echo "user,country,question,answer<br>";

while($row=mysql_fetch_object($rs)) {
	
    $csv_row=	str_replace(","," ",$row->username) . ","
//        . str_replace(","," ",$row->speciality) . ","
//        . str_replace(","," ",$row->organisation) . ","
//		. str_replace(","," ",$row->addr1) . ","
      	. str_replace(","," ",$row->country) . ","
        . str_replace(","," ",$row->question) . ",";

    if ($row->question=="Please indicate your professional role") {
    	$prorole =  q1($row->answer);
	} elseif ($row->question=="Please indicate the clinical area you mainly work in") {
		$prorole =  q2($row->answer);
    } else {
        $prorole = $row->answer;
    }
    $csv_row .= str_replace(","," ",$prorole);

    echo $csv_row . "<br>";

}
?>
