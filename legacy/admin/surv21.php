<?php
require_once("$DOCUMENT_ROOT/_include/config.inc.php");
require_once("$DOCUMENT_ROOT/_include/db_func.inc.php");

function q1($ans) {
     switch ($ans) {
       case 1:
         	return "Doctor";
         break;
       case 2:
            return "Nurse";
         break;
       case 3:
            return "Pharmacist";
         break;
       case 4:
            return "Other";
         break;
       default:
       		return "NA";
            break;
     }
}

$sql = "select survresults.username, role, speciality, organisation, addr1, country, "
		. "title_text as question, answer"
		. " from survresults, users, survquestion"
		. " where survresults.survey_id = 21"
		. " and survresults.username = users.username"
		. " and survquestion.id = survresults.question_id"
        . " and country!='United Kingdom'";
$rs = mysql_query($sql) or die(mysql_error() . "<hr>$sql<hr>");

echo "username,role,speciality,organisation,addr1,country,question,answer<br>";

while($row=mysql_fetch_object($rs)) {
	$csv_row= $row->username . ","
    	. str_replace(","," ",$row->role) . ","
        . str_replace(","," ",$row->speciality) . ","
        . str_replace(","," ",$row->organisation) . ","
		. str_replace(","," ",$row->addr1) . ","
        . str_replace(","," ",$row->country) . ","
        . str_replace(","," ",$row->question) . ",";

    if ($row->question=="What is your professional role?") {
    	$prorole =  q1($row->answer);
    } else {
        $prorole = $row->answer;
    }
    $csv_row .= str_replace(","," ",$prorole);

    echo $csv_row . "<br>";

}
?>
