<?php
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/config.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/db_func.inc.php");
require_once("$_SERVER[DOCUMENT_ROOT]/legacy/_include/visitor.class.php");
session_name('palliativedrugs');
session_start();
$allowed="admin";

require_once("$_SERVER[DOCUMENT_ROOT]/legacy/_include/secure.inc.php");

if ($REQUEST_METHOD=="POST") {
	$nlid = $_POST["hidNlid"];
	$TextDate = $_POST["txtTextDate"];
	$IntroText = AddSlashes($_POST["txtIntroText"]);	
	if ($nlid==0) {
		$sql = "INSERT INTO newsletter (TextDate, IntroText, IssueDate, Live)"
			. " VALUES ('$TextDate', '$IntroText', '" . GetDbTimestamp() . "', 0)";
		mysql_query($sql) or die(mysql_error()."<hr>$sql");
		$new_id = mysql_insert_id();
		header("Location:  newsEditNewsletter.php?nlid=$new_id");
		//echo "new one posted";
		//echo "<a href=\"newsEditNewsletter.php?nlid=$new_id\">newsEditNewsletter.php?nlid=";
		//echo $new_id . "</a>";
		exit;
	} else {
		$vLive = ($_POST["chkLive"]) ? 1 : 0;
		$sql = "UPDATE newsletter"
			. " SET IntroText='$IntroText',"
			. "	TextDate='$TextDate',"
			. "	live=$vLive"
			. " WHERE id=$nlid";
		mysql_query($sql) or die(mysql_error."<hr>$sql");
		header("Location:  newsEditNewsletter.php?nlid=$nlid");
		//echo "updated<br><br>";
		//echo "<a href=\"newsEditNewsletter.php?nlid=$nlid\">newsEditNewsletter.php?nlid=";
		//echo $nlid . "</a>";
		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
	<script language="JavaScript">
	function DeleteArticle(id, headline, nlid)
	{
	   if (confirm("DELETE: " + headline + " ?")) {
	   		
			location.href="/admin/newsDelArticle.php?id=" + id + "&nlid=" + nlid
			return true;	
	   }
	}
	</script>	
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size=-2>admin</font></a> / 
<a href="newsAdmin.php"><font size=-2>newsletter admin</font></a>
</td></tr></table>
<!-- end quick-nav bar -->

<?
if ($_GET["nlid"] == 0) {
	echo "<font size=\"+1\"><b>new newsletter</b></font>";
	$vTextDate = "";
	$vIssueDate = "";
	$vIntroText = "";
	$vLive = FALSE;
	$nlid = $_GET["nlid"];
} else {
	echo "<font size=\"+1\"><b>edit newsletter</b></font>";
	$sql = "SELECT * FROM newsletter WHERE id=$_GET[nlid]";
	$rs = mysql_query($sql);
	$row = mysql_fetch_object($rs);
	$vTextDate = $row->TextDate;
	$vIssueDate = $row->IssueDate;
	$vIntroText = StripSlashes($row->IntroText);
	$vLive = $row->live;
	$nlid = $_GET["nlid"];
}
?>
<br>
<form action="<?=$PHP_SELF?>" method="POST">
<input type=hidden name=hidNlid value=<?=$nlid?>>
<table border="1" cellspacing="0" cellpadding="4">
<tr>
	<td>issue</td>
	<td>
		<?
		if ($vLive == TRUE) {
			echo $vTextDate;
			echo "<input type=\"hidden\" name=\"txtTextDate\" value=\"$vTextDate\">";
		} else {
			echo "<input type=\"text\" name=\"txtTextDate\" value=\"$vTextDate\" size=\"30\">";
		}
		?>
	</td>
</tr>
<tr>
	<td>intro text</td>
	<td><textarea name="txtIntroText" rows="8" cols="80"><?=$vIntroText?></textarea></td>
</tr>
<? if ($vLive == TRUE) { ?>
<tr>
	<td>live</td>
	<td>
		<input type="checkbox" name="chkLive" checked>
	</td>
</tr>
<? } ?>
<tr>
	<td colspan=2>
		<input type="submit" value="save">
		<input type=button value="cancel" onClick="Javascript:document.location.href='newsAdmin.php'">
	</td>
</tr>
</table>
</form>
<?
if ($nlid != 0) {
	echo "<b>contents:</b><br><br>";
	if ($vLive == FALSE) {
		echo "<a href=\"newsEditArticle.php?nlid=$nlid&artid=0\">new article</a><br><br>";
	}
	echo "<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\">";
	echo "<tr bgcolor=\"#c0c0c0\">";
	echo "<td>&nbsp;</td>";
	echo "<td>headline</td>";
	echo "<td>live</td>";
	echo "<td colspan=\"4\">&nbsp;</td>";
	echo "</tr>";
	
	$sql = "SELECT id, ord, headline, live FROM newsarticle WHERE newsletter_id=$nlid order by ord";
	$rs = mysql_query($sql);
	while($row=mysql_fetch_object($rs)) {
		echo "<tr>";
		echo "<td>$row->ord</td>";
		echo "<td>$row->headline</td>";
		echo "<td>";
		if ($row->live == TRUE) {
			echo "<img src=\"/images/check.gif\">";
		} else {
			echo "&nbsp;";
		}
		echo "</td>";
		echo "<td><a href=\"newsEditArticle.php?artid=$row->id\">edit</a></td>";
		echo "<td>";
		if ($row->ord == 1) {
			echo "<img src=\"/images/btnUp-disabled.gif\">";
		} else {
			echo "<a href=\"newsReorderArticle.php?nlid=$nlid&aid=$row->id&ud=up\"><img src=\"/images/btnUp.gif\" border=\"0\" alt=\"move up\"></a>";
		}
		echo "</td>";
		echo "<td>";
		if ($row->ord == mysql_num_rows($rs)) {
			echo "<img src=\"/images/btnDown-disabled.gif\">";
		} else {
			echo "<a href=\"newsReorderArticle.php?nlid=$nlid&aid=$row->id&ud=dn\"><img src=\"/images/btnDown.gif\" border=\"0\" alt=\"move down\"></a>";
		}
		echo "</td>";
		echo "<td><a href=\"Javascript:DeleteArticle($row->id,'".str_replace("'","\\'",$row->headline) ."', $nlid)\">";
		echo "<img src=\"/images/trash.gif\" border=\"0\" alt=\"delete\"></a></td>";
		echo "</tr>";
	}
	echo "</table>";
}
?>
</body>
</html>
