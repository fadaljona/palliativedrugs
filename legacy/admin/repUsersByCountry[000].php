<?php
include("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/config.inc.php");
include("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/db_func.inc.php");
include("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/form_func.inc.php");
require_once("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/visitor.class.php");
session_start();
$allowed="admin";
require_once("$HTTP_SERVER_VARS[DOCUMENT_ROOT]/_include/secure.inc.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>&copy;2002 palliativedrugs.com</title>
<link rel="STYLESHEET" type="text/css" href="/admin/admin.css">
</head>
<?
$rs = mysql_query("SELECT COUNT(*) as total_users FROM users") or die(mysql_error());
$row = mysql_fetch_object($rs);
$total_users = $row->total_users;
mysql_free_result($rs);

$rs = mysql_query("select country, count(*) as num_users from users group by country order by 2 desc") or die(mysql_error());

?>
<body>
<font size=+1>palliativedrugs.com: Users by Country</font><br>
<br>
<font size=-1>
date: <?echo Date("F d, Y")?><br>
total users: <?=$total_users?></font>
<br><br>
<table bgcolor=#f0f0f0>
<?
while($row=mysql_fetch_object($rs)) {
	$pcnt = ($row->num_users / $total_users) * 100;
	$pcnt = number_format($pcnt, 2);
	$bar_width = $pcnt * 6;
	echo "<tr>"
		. "<td>$row->country</td>"
		. "<td>$row->num_users</td>"
		. "<td>(" . round($pcnt) ."%)</td>"
		. "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>"
		. "</tr>";
}
?>
</table>
</body>
</html>
