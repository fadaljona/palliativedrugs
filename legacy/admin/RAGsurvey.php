<?php require_once('../Connections/cnPall.php'); ?>
<?php require_once('../_include/RAG_func.inc.php'); ?>
<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/_include/app_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/_include/form_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmSurvey")) {
  $updateSQL = sprintf("UPDATE rag_survey SET rag_entry_id=%s, survey_id=%s, monograph=%s, notes=%s WHERE id=%s",
                       GetSQLValueString($_POST['rag_entry_id'], "int"),
                       GetSQLValueString($_POST['survey_id'], "int"),
                       GetSQLValueString($_POST['monograph'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($updateSQL, $cnPall) or die(mysql_error());

  $updateGoTo = "RAGsurvey.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_GET['survid'])) && ($_GET['survid'] != "") && (isset($_GET['del']))) {
  $deleteSQL = sprintf("DELETE FROM rag_survey WHERE id=%s",
                       GetSQLValueString($_GET['survid'], "int"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($deleteSQL, $cnPall) or die(mysql_error());

  $deleteGoTo = "RAGentry.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $deleteGoTo .= (strpos($deleteGoTo, '?')) ? "&" : "?";
    $deleteGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $deleteGoTo));
}

//mk
if (isset($_GET[remdoc])) {
	$entry =  $_GET[entry];
	$sec =  $_GET[sec];
	$doc = $_GET[remdoc];
	$surv = $_GET[surv];
	$sql = "DELETE FROM rag_doc_survey WHERE doc_id=" . $doc . " AND rag_survey_id=" . $surv;
	mysql_select_db($database_cnPall, $cnPall);
  	mysql_query($sql, $cnPall) or die(mysql_error() . "<hr>" . $sql);
	$GoTo = "RAGsurvey.php?survid=" . $surv . "&entryid=" . $entry . "&secid=" . $sec;
 	header(sprintf("Location: %s", $GoTo));
}

if (isset($_POST['btnAdd'])) {
	$sql = "INSERT INTO rag_doc_survey (doc_id, rag_survey_id) VALUES (" . $_POST['doc'] . ", " . $_POST['survid'] . ")";
	mysql_select_db($database_cnPall, $cnPall);
  	mysql_query($sql, $cnPall) or die(mysql_error() . "<hr>" . $sql);
	
	$GoTo = "RAGsurvey.php?survid=" . $_POST[survid] . "&entryid=" . $_POST[entryid] . "&secid=" . $_POST[secid];
 	header(sprintf("Location: %s", $GoTo));	
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmSurvey")) {
  $insertSQL = sprintf("INSERT INTO rag_survey (rag_entry_id, survey_id, monograph, notes) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['rag_entry_id'], "int"),
                       GetSQLValueString($_POST['survey_id'], "int"),
                       GetSQLValueString($_POST['monograph'], "text"),
                       GetSQLValueString($_POST['notes'], "text"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($insertSQL, $cnPall) or die(mysql_error());

  $insertGoTo = "RAGsurvey.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
   $insertGoTo .= "survid=" . mysql_insert_id();
  header(sprintf("Location: %s", $insertGoTo));
}
?>
<?php
if (isset($_POST['survid'])) {
	$survid = $_POST['survid'];
} else {
	$survid = $_GET['survid'];
}
?>
<?php
mysql_select_db($database_cnPall, $cnPall);
$query_rsSurveys = "SELECT id, title FROM survsurvey";
$rsSurveys = mysql_query($query_rsSurveys, $cnPall) or die(mysql_error());
$row_rsSurveys = mysql_fetch_assoc($rsSurveys);
$totalRows_rsSurveys = mysql_num_rows($rsSurveys);

$colname_rsRagSurvey = "1";
if (isset($_GET['survid'])) {
  $colname_rsRagSurvey = (get_magic_quotes_gpc()) ? $_GET['survid'] : addslashes($_GET['survid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsRagSurvey = sprintf("SELECT * FROM rag_survey WHERE id = %s", $colname_rsRagSurvey);
$rsRagSurvey = mysql_query($query_rsRagSurvey, $cnPall) or die(mysql_error());
$row_rsRagSurvey = mysql_fetch_assoc($rsRagSurvey);
$totalRows_rsRagSurvey = mysql_num_rows($rsRagSurvey);

$colname_rsDocs = "1";
if (isset($_GET['entryid'])) {
  $colname_rsDocs = (get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsDocs = sprintf("SELECT id, title FROM rag_doc WHERE entry_id = %s", $colname_rsDocs);
$rsDocs = mysql_query($query_rsDocs, $cnPall) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);

$colname_rsAssocDocs = "1";
if (isset($_GET['survid'])) {
  $colname_rsAssocDocs = (get_magic_quotes_gpc()) ? $_GET['survid'] : addslashes($_GET['survid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsAssocDocs = sprintf("SELECT * FROM rag_doc_survey WHERE rag_survey_id = %s", $colname_rsAssocDocs);
$rsAssocDocs = mysql_query($query_rsAssocDocs, $cnPall) or die(mysql_error());
//$row_rsAssocDocs = mysql_fetch_assoc($rsAssocDocs);
$totalRows_rsAssocDocs = mysql_num_rows($rsAssocDocs);
?>
<html>
<head>
<title>rag:survey</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="admin.css" rel="stylesheet" type="text/css">
</head>

<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0>
<tr>
    <td >
		<a href="index.php"><font size=-2>admin</font></a>
		/ <a href="RAGadmin.php"><font size=-2>RAG admin</font></a>
		/ <a href="RAGsection.php?secid=<?php echo $_GET['secid']; ?>"><font size=-2>RAG topic</font></a>
		/  <a href="RAGentry.php?secid=<?php echo $_GET['secid']; ?>&entryid=<?php echo $_GET['entryid']; ?>"><font size=-2>RAG content</font></a>
	</td>
  </tr>
</table>
<!-- end quick-nav bar -->
<p>RAG Survey</p>
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmSurvey" id="frmSurvey">
  <table width="100%" border="0" cellspacing="1" cellpadding="2">
    <tr> 
      <th>RAG section / entry</th>
      <td> <?php echo dLookUp("title","rag_section","id=" . $_GET['secid']); ?><br> 
        <?php echo dLookUp("title","rag_entry","id=" . $_GET['entryid']); ?> 
      </td>
    </tr>
    <tr> 
      <th>survey</th>
      <td><select name="survey_id" id="survey_id">
          <?php
do {  
?>
          <option value="<?php echo $row_rsSurveys['id']?>"<?php if (!(strcmp($row_rsSurveys['id'], $row_rsRagSurvey['survey_id']))) {echo "SELECTED";} ?>><?php echo $row_rsSurveys['title']?></option>
          <?php
} while ($row_rsSurveys = mysql_fetch_assoc($rsSurveys));
  $rows = mysql_num_rows($rsSurveys);
  if($rows > 0) {
      mysql_data_seek($rsSurveys, 0);
	  $row_rsSurveys = mysql_fetch_assoc($rsSurveys);
  }
?>
        </select></td>
    </tr>
    <tr> 
      <th>monograph</th>
      <td> <input name="monograph" type="hidden" value=""> 
        <?
			$mons = GetArrayOfMonographs();
			$apx = GetArrayOfAppendicies();
			$both = array_merge($mons,$apx);
			array_unshift($both,"None");
			arrayToListBox("monograph",$both,$row_rsRagSurvey['monograph']);
			?>
      </td>
    </tr>
    <tr> 
      <th>notes</th>
      <td><textarea name="notes" cols="70" rows="6" id="notes"><?php echo $row_rsRagSurvey['notes']; ?></textarea></td>
    </tr>
	<tr>
		<th>allow multpiple entries</th>
		<td>
			<input type="Checkbox" name="allow_multiples" <?php if($row_rsRagSurvey['allow_multiples']) echo " checked";?>>
		</td>
	</tr>
    <tr> 
      <td colspan="2" align="center"><input name="id" type="hidden" id="id" value="<?php echo $_GET['survid']; ?>"> 
        <input name="rag_entry_id" type="hidden" id="rag_entry_id" value="<?php echo $_GET['entryid']; ?>"> 
        <?php if ($_GET['survid']>0) { ?>
        <input name="btnUpdate" type="submit" id="btnUpdate" value="update">   <input type="hidden" name="MM_update" value="frmSurvey">
        <?php } // show if var=val ?>
        <?php if ($_GET['survid']==0) { ?>
        <input name="btnSave" type="submit" id="btnSave" value="save">
        <input type="hidden" name="MM_insert" value="frmSurvey">
        <?php } // show if var=val ?>
      </td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($rsSurveys);

mysql_free_result($rsRagSurvey);

mysql_free_result($rsDocs);

mysql_free_result($rsAssocDocs);
?>
