<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');


if($_SERVER['REQUEST_METHOD']=='POST') {
	$id = $_POST['id'];
	
	if(IsSet($_POST['delete'])) {
		$sql = "DELETE FROM news WHERE id=" . $id;
		mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		header('Location:  news.php');
	}
	
	$date = $_POST['date'];
	$author = $_POST['author'];
	$type = $_POST['type'];
	$category = $_POST['category'];
	$leader = $_POST['leader'];
	$content = $_POST['content'];
	$link = $_POST['link'];
	$links = $_POST['links'];
	$approved = IsSet($_POST['approved']) ? 1 : 0;
	
	if($id==0) {
		$sql = "INSERT INTO news (active) VALUES (0)";
		mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		$id = mysql_insert_id();
	}
	$sql = "UPDATE news SET "
			. "date='" . $date . "', "
			. "author='" . $author . "', "
			. "type='" . $type . "', "
			. "category='" . $category . "', "
			. "leader='" . $leader . "', "
			. "content='" . htmlentities($content) . "', "
			. "link='" . $link . "', "
			. "links='" . $links . "', "
			. "active=$approved "
			. "WHERE id= $id";
	mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	header('Location:  news.php');
}

$id = $_GET['id'];
if($id==0) {
	$page_title = "Create new news item";
	$date = date('Y-m-d');
	$author = dLookUp('username','new_users','id='.$_SESSION['visitor']->id);
	$approve_checked = " checked";
} else {
	$page_title = "Edit news item";
	$sql = "SELECT * FROM news WHERE id=" . $id;
	$rs = mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
	//populate field vars
	if($rs) {
		$row=mysql_fetch_object($rs);
		$id = $row->id;
		$date = substr($row->date,0,10);
		$author = $row->author;
		$type = $row->type;
		$category = $row->category;
		$leader = $row->leader;
		$content = $row->content;
		$link = $row->link;
		$links = $row->links;
		$approved = $row->active;
		if($approved) {
			$approve_checked = " checked";
		} else {
			$approve_checked = "";
		}
	} else {
		die('record not found.');
	}	
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="STYLESHEET" type="text/css" href="admin.css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<b><?=$page_title;?></b>
<br /><br />
<form action="news_edit.php" method="POST">
<input type="hidden" name="id" value="<?=$id;?>">
<table>
<tr>
	<td>author</td>
	<td><input type="text" name="author" size="40" value="<?=$author ?>"></td>
</tr>
<tr>
	<td>date [YYYY-MM-DD]</td>
	<td><input type="text" name="date" size="12" value="<?=$date ?>"></td>
</tr>

<tr>
	<td>type</td>
	<td>
		<?
		$options=array("news","addition");
		$default = (IsSet($type)) ? $type : "news";
		arrayToListBox('type',$options,$default);
		?>
	</td>
</tr>

<tr>
	<td>category (temp!!!)</td>
	<td>
		<?
		$options=array("hot"=>"news_hot","drug"=>"news_drug", "pall"=>"news_pall", "other"=>"news_other",
			"doclib"=>"addition_doc-library", "survey"=>"addition_survey", "monograph"=>"addition_monograph", "other"=>"addition_other");
		$default = (IsSet($category)) ? $category : 'hot';
		arrayToListBox('category',$options,$default);
		?>
	</td>
</tr>

<!-- 
<tr>
	<td>category<br /></td>
	<td><input type="text" name="category" size="20" value="<?=$category ?>"> news = [ hot |drug | pall | other ]&nbsp;&nbsp;addition = [ doclib | survey | monograph | other ]</td>
</tr>
 -->
<tr>
	<td>leader</td>
	<td><input type="text" name="leader" size="60" value="<?=$leader ?>"></td>
</tr>
<tr>
	<td>content</td>
	<td><textarea name="content" rows="10" cols="80"><?=$content;?></textarea></td>
</tr>
<tr>
	<td>link</td>
	<td><input name="link" type="text" size="60" value="<?=$link;?>"></td>
</tr>

<tr>
	<td>links (multiple)</td>
	<td><textarea name="links" rows="6" cols="80"><?=$links;?></textarea></td>
</tr>

<tr>
	<td>approved ?</td>
	<td><input type="checkbox" name="approved"<?=$approve_checked;?>></td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" value="save">
		<?if($id != 0) {  ?>
		<input type="submit" value="delete" name="delete">
		<?} ?>
		<input type="button" value="cancel" onClick="Javascript:history.go(-1);">
	</td>
</tr>
</table>
</form>


</body>
</html>