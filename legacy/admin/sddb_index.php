<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/form_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size=-2>admin</font></a> / 
<a href="sddb.php"><font size=-2>SDdb</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<?
if(IsSet($_GET['filt'])) {
	$filter = $_GET['filt'];
} else {
	$filter = "all";	
}
$form_count=0;
?>
<b>SDDB - Index</b> 
<a href="sddb_index.php?filt=all">all</a>
 | <a href="sddb_index.php?filt=2">two drugs</a>
 | <a href="sddb_index.php?filt=3">three drugs</a>
 | <a href="sddb_index.php?filt=4">four drugs</a>
 | <a href="sddb_index.php?filt=5">five drugs</a>
 | <a href="sddb_index.php?filt=6">six drugs</a>
<br><br>

<? if($filter=="all" || $filter=="2") { ?>
<table align="center">
<tr><th colspan="3">Two Drugs</th></tr>
<tr>
	<th>drug 1</th>
	<th>drug 2</th>
	<th></th>
</tr>
<?
$sql = "select distinct drug1_name, drug2_name from sddb "
	. "where  drug3_name='0' and drug4_name='0' and drug5_name='0' and drug6_name='0' order by drug1_name, drug2_name asc ";
$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
while($row=mysql_fetch_object($rs)) {
		?>
		<tr>
		<td><?=$row->drug1_name;?></td>
		<td><?=$row->drug2_name;?></td>
		<form action="sddb.php" method="POST">
			<input type="hidden" name="drug1" value="<?=$row->drug1_name?>">
			<input type="hidden" name="drug2" value="<?=$row->drug2_name?>">
			<input type="hidden" name="exact" value="1">
		<td align="center">
			<a href="javascript:document.forms[<?=$form_count?>].submit();">view</a>
		</td>
		</form>
		<? $form_count++;?>		
		</tr>
<? }?>
</table>
<br><br>
<? }?>


<? if($filter=="all" || $filter=="3") { ?>
<table align="center">
<tr><th colspan="4">Three Drugs</th></tr>
<tr>
	<th>drug 1</th>
	<th>drug 2</th>
	<th>drug 3</th>
	<th></th>
</tr>
<?
$sql = "select distinct drug1_name, drug2_name, drug3_name from sddb where  drug3_name!='0' and drug4_name='0' and drug5_name='0' and drug6_name='0' order by drug1_name, drug2_name, drug3_name asc";
$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
while($row=mysql_fetch_object($rs)) {
		?>
		<tr>
		<td><?=$row->drug1_name;?></td>
		<td><?=$row->drug2_name;?></td>
		<td><?=$row->drug3_name;?></td>
		<form action="sddb.php" method="POST">
			<input type="hidden" name="drug1" value="<?=$row->drug1_name?>">
			<input type="hidden" name="drug2" value="<?=$row->drug2_name?>">
			<input type="hidden" name="drug3" value="<?=$row->drug3_name?>">
			<input type="hidden" name="exact" value="1">
		<td align="center">
			<a href="javascript:document.forms[<?=$form_count?>].submit();">view</a>
		</td>
		</form>
		<? $form_count++;?>	
		</tr>
<? } ?>
</table>
<br><br>
<? } ?>

<? if($filter=="all" || $filter=="4") { ?>
<table width="100%">
<tr><th colspan="5">Four Drugs</th></tr>
<tr>
	<th>drug 1</th>
	<th>drug 2</th>
	<th>drug 3</th>
	<th>drug 4</th>
	<th></th>
</tr>
<?
$sql = "select distinct drug1_name, drug2_name, drug3_name, drug4_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name='0' and drug6_name='0' order by drug1_name, drug2_name, drug3_name, drug4_name asc";
$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
while($row=mysql_fetch_object($rs)) {
		?>
		<tr>
		<td><?=$row->drug1_name?></td>
		<td><?=$row->drug2_name?></td>
		<td><?=$row->drug3_name?></td>
		<td><?=$row->drug4_name?></td>
		<form action="sddb.php" method="POST">
			<input type="hidden" name="drug1" value="<?=$row->drug1_name?>">
			<input type="hidden" name="drug2" value="<?=$row->drug2_name?>">
			<input type="hidden" name="drug3" value="<?=$row->drug3_name?>">
			<input type="hidden" name="drug4" value="<?=$row->drug4_name?>">
			<input type="hidden" name="exact" value="1">
		<td align="center">
			<a href="javascript:document.forms[<?=$form_count?>].submit();">view</a>
		</td>
		</form>
		<? $form_count++;?>	
		</tr>
<? } ?>
</table>
<br><br>
<? } ?>

<? if($filter=="all" || $filter=="5") { ?>
<table align="center">
<tr><th colspan="6">Five Drugs</th></tr>
<tr>
	<th>drug 1</th>
	<th>drug 2</th>
	<th>drug 3</th>
	<th>drug 4</th>
	<th>drug 5</th>
	<th></th>
</tr>
<?
$sql = "select distinct drug1_name, drug2_name, drug3_name, drug4_name, drug5_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name!='0' and drug6_name='0' order by drug1_name, drug2_name, drug3_name, drug4_name, drug5_name asc";
$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
while($row=mysql_fetch_object($rs)) {
		?>
		<tr>
		<td><?=$row->drug1_name?></td>
		<td><?=$row->drug2_name?></td>
		<td><?=$row->drug3_name?></td>
		<td><?=$row->drug4_name?></td>
		<td><?=$row->drug5_name?></td>
		<form action="sddb.php" method="POST">
			<input type="hidden" name="drug1" value="<?=$row->drug1_name?>">
			<input type="hidden" name="drug2" value="<?=$row->drug2_name?>">
			<input type="hidden" name="drug3" value="<?=$row->drug3_name?>">
			<input type="hidden" name="drug4" value="<?=$row->drug4_name?>">
			<input type="hidden" name="drug5" value="<?=$row->drug5_name?>">
			<input type="hidden" name="exact" value="1">
		<td align="center">
			<a href="javascript:document.forms[<?=$form_count?>].submit();">view</a>
		</td>
		</form>
		<? $form_count++;?>	
		</tr>
<? } ?>
</table>
<br><br>
<? } ?>

<? if($filter=="all" || $filter=="6") {
$sql = "select distinct drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name!='0' and drug6_name!='0' order by drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name asc";
$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
if(mysql_num_rows($rs)==0) { 
	echo "<p align=\"center\">no results for six drug combinations.</p>";
} else { ?>
<table align="center">
<tr><th colspan="7">Six Drugs</th></tr>
<tr>
	<th>drug 1</th>
	<th>drug 2</th>
	<th>drug 3</th>
	<th>drug 4</th>
	<th>drug 5</th>
	<th>drug 6</th>
	<th></th>
</tr>
<?
$sql = "select distinct drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name  from sddb where  drug3_name!='0' and drug4_name!='0' and drug5_name!='0' and drug6_name!='0' order by drug1_name, drug2_name, drug3_name, drug4_name, drug5_name, drug6_name asc";
$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
while($row=mysql_fetch_object($rs)) {
		?>
		<tr>
		<td><?=$row->drug1_name?></td>
		<td><?=$row->drug2_name?></td>
		<td><?=$row->drug3_name?></td>
		<td><?=$row->drug4_name?></td>
		<td><?=$row->drug5_name?></td>
		<td><?=$row->drug6_name?></td>
		<form action="sddb.php" method="POST">
			<input type="hidden" name="drug1" value="<?=$row->drug1_name?>">
			<input type="hidden" name="drug2" value="<?=$row->drug2_name?>">
			<input type="hidden" name="drug3" value="<?=$row->drug3_name?>">
			<input type="hidden" name="drug4" value="<?=$row->drug4_name?>">
			<input type="hidden" name="drug5" value="<?=$row->drug5_name?>">
			<input type="hidden" name="drug6" value="<?=$row->drug6_name?>">
			<input type="hidden" name="exact" value="1">
		<td align="center">
			<a href="javascript:document.forms[<?=$form_count?>].submit();">view</a>
		</td>
		</form>
		<? $form_count++;?>	
		</tr>
<? } ?>
</table>
<br>
<?
} 
}
?>
</body>
</html>