<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed='admin';
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

if ($_SERVER['REQUEST_METHOD']=='POST') {
	$sid = $_POST['hidsid'];
	$title = AddSlashes(trim($_POST['txtTitle']));
	$intro = AddSlashes(trim($_POST['txtIntro']));
	$ack_msg = AddSlashes(trim($_POST['txtAckMsg']));
	$avail = AddSlashes(trim($_POST['txtAvailTo']));
	$active =  ($_POST['chkActive']) ? 1 : 0;
	if ($sid==0) {
		//it's a new one
		$SQL = "INSERT INTO survsurvey (title, intro, acknowledgement_msg, active)"
			. " VALUES ('$title', '$intro', '$ack_msg', $active)";
		mysql_query($SQL) or die("add survey<hr>".mysql_error()."<hr>$sql");
		$new_id = mysql_insert_id();
		header("Location:  survEditSurvey.php?sid=$new_id");
		//echo "new surv posted<br>";
		//echo "<a href=\"survEditSurvey.php?sid=$new_id\">survEditSurvey.php?sid=";
		//echo $new_id . "</a>";
		exit;
	} else {
		// it's an update
		$SQL = "UPDATE survsurvey"
			. "	SET title='$title',"
			. "	intro='$intro',"
			. "	acknowledgement_msg = '$ack_msg',"
			. "	groupname = '$avail',"
			. "	active=$active"		
			. " WHERE id=$sid";
		mysql_query($SQL) or die("surv update<hr>".mysql_error()."<hr>$SQL");
		header("Location:  survEditSurvey.php?sid=$sid");
		//echo "surv updated";
		//echo "<a href=\"survEditSurvey.php?sid=$sid\">survEditSurvey.php?sid=";
		//echo $sid . "</a>";
		exit;
	}//if $sid==0
}//if ($REQUEST_METHOD=="POST")
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
	<script language="JavaScript">
	function DeleteQuestion(id, question, sid)
	{
	   if (confirm("DELETE: " + question + " ?")) {	   		
			location.href="/legacy/admin/survDelQuestion.php?qid=" + id + "&sid=" + sid
			return true;	
	   }
	}
	</script>	
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size="-2">admin</font></a> / 
<a href="survAdmin.php"><font size="-2">survey admin</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<?
$sid = $_GET["sid"];
if ($sid == 0) {
	echo "<font size=\"+1\"><b>new survey</b></font>";
	$vTitle = "";
	$vIntro = "";
	$vAckMsg = "";
	$vActive = FALSE;
	$vAvail = "";
} else {
	echo "<font size=\"+1\"><b>edit survey</b></font>";
	$sql = "SELECT * FROM survsurvey WHERE id=$sid";
	$rs = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$row = mysql_fetch_object($rs);
	$vTitle = StripSlashes($row->title);
	$vIntro = StripSlashes($row->intro);
	$vAckMsg = StripSlashes($row->acknowledgement_msg);
	$vActive = $row->active;
	$vAvail = StripSlashes($row->groupname);
	mysql_free_result($rs);
}
?>
<br>
<form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
<input type="hidden" name="hidsid" value="<?=$sid?>">
<table border="1" cellspacing="0" cellpadding="4">
<tr>
	<td>title</td>
	<td><input type="text" name="txtTitle" value="<?=$vTitle?>" size="30"></td>
</tr>
<tr>
	<td>intro text</td>
	<td><textarea name="txtIntro" rows="8" cols="80"><?=$vIntro?></textarea></td>
</tr>
<tr>
	<td>acknowledgement message</td>
	<td><textarea name="txtAckMsg" rows="4" cols="80"><?=$vAckMsg?></textarea></td>
</tr>
<? if ($vActive == TRUE) { ?>
<tr>
	<td>live</td>
	<td>
		<input type=checkbox name=chkActive checked>
	</td>
</tr>
<? } ?>
<tr>
	<td>available to</td>
	<td><input type="text" name="txtAvailTo" value="<?=$vAvail?>" size="30"></td>
</tr>
<tr>
	<td colspan=2>
	<?
	if ($sid==0) {
		echo "<input type=submit value=\"save\">";
	} else {
		echo "<input type=submit value=\"update\">";
	}
	?>
	</td>
</tr>
</table>
</form>
<?
if ($sid != 0) {
	$sql = "SELECT id, type, title_text, ord, active FROM survquestion WHERE survey_id=$sid order by ord";
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	echo "<b>questions:</b><br><br>";
	if ($vActive==FALSE) {
		echo "<a href=\"survQuestion.php?sid=$sid&&qid=0\">new question</a><br><br>";
	}
	echo "<table border=1 cellspacing=0 cellpadding=4>";
	echo "<tr bgcolor=#c0c0c0>";
	echo "<td>&nbsp;</td>";
	echo "<td>question</td>";
	echo "<td>type</td>";
	echo "<td>incl.</td>";
	echo "<td colspan=4>&nbsp;</td>	";
	echo "</tr>";
	while ($row=mysql_fetch_object($rs)) {
		echo "<tr>";
		echo "<td>$row->ord</td>";
		echo "<td>$row->title_text</td>";
		echo "<td>$row->type</td>";
		echo "<td align=\"center\">";
		if ($row->active == TRUE) {
			echo "<img src=\"/images/check.gif\">";
		} else {
			echo "&nbsp;";
		}
		echo "</td>";
		echo "<td><a href=\"survQuestion.php?qid=$row->id&sid=$sid\">edit</a></td>";
		echo "<td>";
		if ($row->ord == 1) {
			echo "<img src=\"/images/btnUp-disabled.gif\">";
		} else {
			echo "<a href=\"survReOrderQuestion.php?sid=$sid&qid=$row->id&ud=up\"><img src=\"/images/btnUp.gif\" border=0 alt=\"move up\"></a>";
		}
		echo "</td>";
		echo "<td>";
		if ($row->ord == mysql_num_rows($rs)) {
			echo "<img src=\"/images/btnDown-disabled.gif\">";
		} else {
			echo "<a href=\"survReOrderQuestion.php?sid=$sid&qid=$row->id&ud=dn\"><img src=\"/images/btnDown.gif\" border=0 alt=\"move down\"></a>";
		}
		echo "</td>";
		echo "<td><a href=\"Javascript:DeleteQuestion($row->id,'$row->title_text', $sid)\"><img src=\"/images/trash.gif\" border=0 alt=\"delete\"></a></td>";
		echo "</tr>";
	}//while ($row=mysql_fetch_object($rs)) {
	echo "</table>";
}
?>
</body>
</html>
