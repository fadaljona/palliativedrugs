<?php
include("$_SERVER[DOCUMENT_ROOT]/_include/config.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/_include/db_func.inc.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_include/visitor.class.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_include/UserInfo.class.php");
session_start();
$allowed="admin";
require_once("$_SERVER[DOCUMENT_ROOT]/_include/secure.inc.php");

if(IsSet($_GET["clr"])) {
	$sql = "update users set RegistrationChecked=1 where RegistrationChecked=0";
    mysql_query($sql) or die(mysql_error);
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>&copy;2004 palliativedrugs.com</title>
<link rel="STYLESHEET" type="text/css" href="/admin/admin.css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0>
<tr><td><a href="index.php"><font size=-2>admin</font></a></td></tr>
</table>
<!-- end quick-nav bar -->
<br>
<b>new users</b><br>
<a href="/admin/new_users.php?clr=yes">clear list</a><br><br>
<table border="1" cellspacing="0" cellpadding="4" width="100%">
<tr>
	<th>username</th>
	<th>title</th>
	<th>first name</th>
	<th>last name</th>
	<th>role</th>
	<th>country</th>
	<th>date reg'd</th>
	<th>rag sign-up</th>
</tr>
<?php
$sql = "select * from users where RegistrationChecked = 0 order by DateRegistered";
$rs = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_object($rs)) {
	$rs2=mysql_query("SELECT user_id FROM rag_user WHERE user_id=$row->id");
	if(mysql_num_rows($rs2) > 0) {
		$rag_signup="yes";
	} else {
		$rag_signup="&nbsp;";
	}
	mysql_free_result($rs2);
	echo "<tr>"
		."<td><a href=\"edit_user.php?cp=/admin/new_users.php&uid=$row->id\">$row->username</a></td>"
        ."<td>$row->title</td>"
        ."<td>$row->FirstName</td>"
        ."<td>$row->LastName</td>"
        ."<td>$row->role</td>"
        ."<td>$row->Country</td>"
        ."<td>". substr($row->DateRegistered,0,10) ."</td>"
		."<td align=center>$rag_signup</td>"
        ."</tr>\n";																	 
}
mysql_free_result($rs);
?>
</table>
<br><br>
<b>existing users new to RAG</b><br><br>
<table border="1" cellspacing="0" cellpadding="4" width="100%">
<tr>
	<th>username</th>
	<th>title</th>
	<th>first name</th>
	<th>last name</th>
	<th>role</th>
	<th>country</th>
	<th>orig. reg date</th>
</tr>
<?php
$sql = "select id, username, title, FirstName, LastName, role, Country, DateRegistered "
		. "from users, rag_user "
		. "where RegistrationChecked = 1 "
		. "and rag_user.vetted = 0 "
		. "and id = user_id";

$rs = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_object($rs)) {
	echo "<tr>"
		."<td><a href=\"edit_user.php?cp=/admin/new_users.php&uid=$row->id\">$row->username</a></td>"
        ."<td>$row->title</td>"
        ."<td>$row->FirstName</td>"
        ."<td>$row->LastName</td>"
        ."<td>$row->role</td>"
        ."<td>$row->Country</td>"
        ."<td>". substr($row->DateRegistered,0,10) ."</td>"
        ."</tr>\n";																	 
}
mysql_free_result($rs);
?>
</table>
</body>
</html>
