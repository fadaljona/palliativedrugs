<?php
require_once("../../../config.inc.php");
session_start();

// this function is used to retrieve additional form fields associated with a given db-field.
function GetAttr($field, $attr, $form) {
	if (array_key_exists($attr.$field,$form)) {
		return $form[$attr.$field];
	} else {
		return NULL;
	}
}

// parse form vars into associative array
while(list($key, $value) = each($HTTP_POST_VARS)) {
		$form[$key] = trim($value);
}

// iterate form array and build new array of included fields
$i = count($form);
for ($n = 0; $n < $i; $n++) {
	$elem=each($form);
	if (substr($elem[key],0,6)=="chkInc") {
		$fields[] = substr($elem[key],6,strlen($elem[key])-6);
	}
}

//now we can iterate the $fields array, and use GetAttr to retrive associated values...
$num_fields = count($fields);
for ($i = 0; $i < $num_fields; $i++) {
	$field = each($fields);
	print "fld=".  $field[value];
	print ", title=" . GetAttr($field[value],"txtTitle",$form) . "<br>";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<link rel="STYLESHEET" type="text/css" href="../../../dbtool.css">
	<title><? echo $HTTP_POST_VARS["txtPageTitle"]?></title>
</head>
<body>
<? echo $HTTP_POST_VARS["txtHeader"]?>
<br>
<table width="100%" >
<?echo $HTTP_POST_VARS["txtTableAttributes"]?>
<br>
<?echo $HTTP_POST_VARS["rbSort"]?><br>
<hr>
<?
$filename = "../../../" . $HTTP_POST_VARS["txtFilename"];
$fp = @fopen($filename,"w") or die("couldn't create output file.");
fwrite($fp,"<!--codegen made this!-->");

fclose($fp);
?>

</table>
</body>
</html>
