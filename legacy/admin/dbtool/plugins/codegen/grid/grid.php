<?php
require_once("../../../config.inc.php");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<link rel="STYLESHEET" type="text/css" href="../../../dbtool.css">
</head>
<body>
<table border="1" cellspacing="0" width="100%">
<tr>
	<td>&nbsp;CodeGen&nbsp;</td>
	<td>&nbsp;module:<b>grid</b></td>
	<td>&nbsp;version: <b>0.1</b></td>
	<td>&nbsp;table: <b><?=$HTTP_GET_VARS["tbl"]?></b></td>	
</tr>
</table>
<br>
<form action="grid_post.php" method="post">
<table width="100%">
<tr>
	<td width="120">output filename</td>
	<td><input type="text" name="txtFilename" size="80" value="output/untitled_grid.php"></td>
</tr>
<tr>
	<td width="120">[page title]</td>
	<td><input type="text" name="txtPageTitle" size="60"></td>
</tr>
<tr>
	<td>[style sheet]</td>
	<td><input type="text" name="txtStyleSheet" size="40"></td>
</tr>
<tr>
	<td>[header]</td>
	<td><textarea name="txtHeader" rows="3" cols="60"></textarea></td>
</tr>
<tr>
	<td>[table attributes]</td>
	<td><input type="text" name="txtTableAttributes" size="40"></td>
</tr>
<tr>
	<td>fields</td>
	<td>
		<table border="1" cellspacing="0">
		<tr>
			<th>fieldname</th>
			<th>type</th>
			<th>null</th>
			<th>show</th>
			<th>sort</th>
			<th>column<br>title</th>
		</tr>
		<?
			$rs = mysql_db_query($current_db,'SHOW FIELDS FROM ' . $tbl) or die (mysql_error());
			while ($row = mysql_fetch_array($rs)) {
				print "<tr>\n";
				print "<td>". $row["Field"] .(($row['Key'] == '') ? "" : " (PK)") .  "</td>\n"
						. "<td>" . $row["Type"] . "</td>\n"
						. "<td>" . (($row['Null'] == '') ? "No" : "Yes") . "</td>\n"						
						. "<td align=\"center\"><input type=\"Checkbox\" name=\"chkInc" . $row["Field"] ."\"></td>\n"
						. "<td><input type=\"radio\" name=\"rbSort\" value=\"" . $row["Field"]  . "\"></td>\n";
				print "<td><input type=\"text\" name=\"txtTitle".$row["Field"]. "\" size=\"15\" value=\"".str_replace("_"," ",$row["Field"])."\"></td>\n";
				print "</tr>\n";
			}
		?>
		</table>		
	</td>
</tr>
<tr>
	<td>rows per page</td>
	<td><input type="text" name="txtRowsPerPage" size="2" value="10"></td>
</tr>
<tr>
	<td align="center"><input type="submit" name="btnSubmit" value="next..."></td>
</tr>
</table>
</form>
</body>
</html>
