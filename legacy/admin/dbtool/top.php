<?php
require_once("config.inc.php");
session_start();
?>
<html>
<head>
<script language="JavaScript">
<!--
function db_change(new_db) { 
	// detect which frameset is currently loaded & respond appropriately...
 	parent.frMain.location.href = "sql.php?db=" + frmForm.selDatabase.value;
}
-->
</script>
<link rel="STYLESHEET" type="text/css" href="dbtool.css">
</head>
<body>
<form name="frmForm">
<table width="100%"><tr>
<td width="200" align="center" valign="middle"><img src="images/dbt_logo.gif" alt="dbTool" width="140" height="39" border="0"></td>
<td width="300">

	<table>
	<tr>
		<td>
			<table cellspacing="0" cellpadding="2" border="1">
			<tr><th>server</th><td><?=$db_info[host]?></td></tr>
			<tr><th>user</th><td><?=$db_info[user]?></td></tr>
			</table>
		</td>
		<td>&nbsp;</td>
		<td>
			<table cellspacing="0" cellpadding="2" border="1">
			<tr><th>database</th></tr>
			<tr><td>
				<select name="selDatabase" ONCHANGE="db_change (this.form.selDatabase)">				
				<?
				if ($db_info[onlydb] == TRUE) {
					echo "<option selected value=\"$db_info[dbname]\">$db_info[dbname]</option>\n";				
				} else {
					echo "<option></option>";
					$db_list = mysql_list_dbs($db_conn) or die (mysql_error());
					while ($row = mysql_fetch_array($db_list)) {
					    print "<option value=\"$row[Database]\" ";
						if ($row[Database]==$current_db) {
							print "selected";
						}
						print ">" . $row[Database] . "</option>\n";
					}
				}
				?>
				</select>
			</td></tr>
			</table>
		</td>
	</tr>
	</table>

</td>
<td align="center">
<? if ($limit_functions==FALSE) { ?>
<a href="sql.php" target="frMain">SQL</a> | 
<a href="code.php" target="frMain">code</a> | 
<a href="tools/index.htm" target="frMain">tools</a> |
<a href="help.htm" target="_blank">help</a>
<?}?>
&nbsp;
</td>
</tr></table>
</form>
</body>
</html>
