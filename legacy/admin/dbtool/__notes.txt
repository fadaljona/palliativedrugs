notes
=====

code wizard algo:-

	select table (or query...)
	select fields
	select page options + field options
	generate page (+custom db-include or include in-place with cut'n'paste markers)
	test/preview page
	(allow regen after tweaking options ??)
	save...
	
page types:
	grid (multi-record) (incl. new/edit/delete options) (incl. filter option for each column)
	new/edit (single-record) (include form-val: standard options (text/num/email, req, 
		size etc.) + generate code stubs (functions) for custom field validation.
	view (multi-record, sequential)
	search/search results (present results as array of structs - no formatting)
	
	
session_vars:
-------------------
		$current_db			indicates active db on current connection
	
	
	
feature wish-list
----------------------
	limit rows returned (+ paging)
	open query in new window
	load/save queries (to text files...)	