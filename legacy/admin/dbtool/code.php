<?php
require_once("config.inc.php");
session_start();

if (IsSet($HTTP_POST_VARS["btnSubmit"])) {
	// form has been submitted, so redirect to plugin, passing table-name variable ...
	$url = "./plugins/codegen/" . $HTTP_POST_VARS["rbCodeGen"] . "/".$HTTP_POST_VARS["rbCodeGen"].".php";
	 if( $fp = @fopen($url, "r") ) {
	 	fclose($fp);
		$url .= "?tbl=" . $HTTP_POST_VARS["selTable"];
		header("location: $url");
	} else {
		print "error: couldn't open plugin.";
	}
	exit;
}
?>
<html>
<head>
<link rel="STYLESHEET" type="text/css" href="dbtool.css">
</head>
<body>
code..
<?
if (!IsSet($current_db)) {
	print "please select a database first.";
} else {
?>
<form action="code.php" method="post">
<table border="1" cellspacing="0" cellpadding="6" align="center">
	<tr>
		<td valign="top">select a table:</td>
		<td>			
			<select name="selTable">
			<?
			$tables = mysql_list_tables ($current_db, $db_conn) or die (mysql_error());
			while ($row = mysql_fetch_array($tables)) {
				echo "<option value=\"" . $row[0] . "\">" . $row[0] . "</option>\n";
			}
			?>			
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top">CodeGen's:</td>
		<td>			
			<table border="0" cellpadding="0" cellspacing="0">
			<?
				//scan plugins/codegen dir for code generators...
				chdir("./plugins/codegen");
				$handle=opendir(".");
				 while ($file = readdir($handle)) {
				 	 if (($file!='..') & ($file!='.')) {
					 	if (is_dir($file)) {
							print "<tr><td valign=\"bottom\"><input type=\"radio\" name=\"rbCodeGen\" value=\"$file\"></td><td valign=\"top\"><b>$file</b></td>";
							@$fp = fopen($file . "/description.txt", "r");
							if ($fp) {
								$descrip = file($file . "/description.txt");
								print "<td valign=\"top\">&nbsp;(" . $descrip[0] . ")</td>";
								fclose($fp);
							} else {
								print "<td valign=\"top\">&nbsp;()</td>";
							}
							print "</tr>";
						} // if (is_dir($file))
					 } //  if (($file!='..') & ($file!='.')) 
				 } // while ($file = readdir($handle)) 
			?>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">			
			<input type="submit" name="btnSubmit" value="go">
		</td>
	</tr>
</table>
</form>
<?
} // (else) if (!IsSet($current_db))
?>.
</body>
</html>
