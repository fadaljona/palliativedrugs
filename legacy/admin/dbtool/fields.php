<?php
require_once("config.inc.php");
session_start();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<link rel="STYLESHEET" type="text/css" href="dbtool.css">
</head>
<body>
<?php

if (IsSet($HTTP_GET_VARS["tbl"])) {
	$tbl = $HTTP_GET_VARS["tbl"];
	print "<i>$current_db.$tbl</i><br><br>";
	
	$rs = mysql_db_query($current_db,'SHOW FIELDS FROM ' . $tbl) or die (mysql_error());
	print "<table border=1 cellspacing=0>";
	print "<tr><th><i>key</i></th><th><i>name</i></th><th><i>type</i></th><th><i>null</i></th></tr>";
	while ($row = mysql_fetch_array($rs)) {
		print "<tr>";
		print "<td>" . (($row['Key'] == '') ? "&nbsp;" : "PK") . "</td>"
				. "<td>" . $row["Field"] . "</td>"
				. "<td>" . $row["Type"] . "</td>"
				. "<td>" . (($row['Null'] == '') ? "No" : "Yes") . "</td>";
		print "</tr>";
	}
	print "</table>";
} else {
	print "<i>no table selected.</i>";
}

?>
</body>
</html>
