<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/app_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/misc_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<html>
<head>
        <title>palliativedrugs admin:reports</title>
        <link rel="STYLESHEET" type="text/css" href="admin.css">
        <script language="Javascript">

        var arrOldValues;

        function SelectAllList(CONTROL){
          for(var i = 0;i < CONTROL.length;i++){
            CONTROL.options[i].selected = true;
          }
        }

        function DeselectAllList(CONTROL){
          for(var i = 0;i < CONTROL.length;i++){
            CONTROL.options[i].selected = false;
          }
        }

        function FillListValues(CONTROL){
          var arrNewValues;
          var intNewPos;
          var strTemp = GetSelectValues(CONTROL);
          arrNewValues = strTemp.split(",");
          for(var i=0;i<arrNewValues.length-1;i++){
            if(arrNewValues[i]==1){
              intNewPos = i;
            }
          }

          for(var i=0;i<arrOldValues.length-1;i++){
            if(arrOldValues[i]==1 && i != intNewPos){
              CONTROL.options[i].selected= true;
            }
            else if(arrOldValues[i]==0 && i != intNewPos){
              CONTROL.options[i].selected= false;
            }

            if(arrOldValues[intNewPos]== 1){
              CONTROL.options[intNewPos].selected = false;
            }
            else{
              CONTROL.options[intNewPos].selected = true;
            }
          }
        }

        function GetSelectValues(CONTROL){
          var strTemp = "";
          for(var i = 0;i < CONTROL.length;i++){
            if(CONTROL.options[i].selected == true){
              strTemp += "1,";
            }
            else{
              strTemp += "0,";
            }
          }
          return strTemp;
        }

        function GetCurrentListValues(CONTROL){
          var strValues = "";
            strValues = GetSelectValues(CONTROL);
            arrOldValues = strValues.split(",")
        }
</script>

</head>
<body>
<?
if ($_SERVER['REQUEST_METHOD']=="POST") {
  $breakdown = $_POST['breakdown'];
  $all_or_rag = $_POST['users'];
  $all_countries = $_POST['allcountries'];
  $country_list = $_POST['countries'];

  if ($all_or_rag=="all") {
    $description = "showing <b>all</b> users ";
  } else {
    $description =  "showing <b>RAG</b> users ";
  }
  $description .= "by <b>$breakdown</b> from";
  if ($all_countries=="all") {
    $description .= " <b>all</b> countries.";
  } else {
    $description .= " the following countries :-<br>";
    foreach ($country_list as $c){$description .= "<i>$c</i>, ";}
  }

    $sql = "SELECT $breakdown as field, count(*) as num_users from new_users ";
    if ($all_countries!="all") {
      $sql .= "WHERE (RegistrationChecked=1) AND country IN (";
      foreach ($country_list as $c){$sql .= "'$c',";}
      $sql = substr($sql,0,strlen($sql)-1);
      $sql .= ") ";
    }
    $sql .= " GROUP BY $breakdown ORDER BY 2 DESC";
    
  $rs = mysql_query($sql) or die(mysql_error()."<hr>$sql");
  $total_users = 0;
  while($row=mysql_fetch_object($rs)) {
     $total_users =  $total_users + $row->num_users;
  }
  mysql_data_seek($rs,0);

	echo "<!-- $sql -->";

  echo "palliativedrugs.com<br>" . Date("F d, Y") . "<br><br>";
  echo $description;
  echo "<br><br>total users: $total_users<br><br>";

  echo "<table bgcolor=#f0f0f0>";

  //$rs = mysql_query($sql) or die(mysql_error()."<hr>$sql");
  while($row=mysql_fetch_object($rs)) {
  $pcnt = ($row->num_users / $total_users) * 100;
  $pcnt = number_format($pcnt, 2);
  $bar_width = $pcnt * 6;
  echo "<tr>"
    . "<td>$row->field</td>"
    . "<td>$row->num_users</td>"
    . "<td>($pcnt\%)</td>"
    . "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>"
    . "</tr>";
  }
  echo "</table>";
  echo "<br>total users: " . $total_users . "<br><br>";
 // echo "<br><a href=\"reports.php\">back</a>";
} else { ?>
<!-- start quick-nav bar -->
<table width="100%" bgcolor="#e0e0e0" border="1" cellspacing="0" cellpadding="0">
<tr><td><a href="index.php"><font size="-2">admin</font></a></td></tr>
</table>
<!-- end quick-nav bar -->
<br><b>Custom Reports :-</b><br><br>
<form action="reports.php" method="POST" name="myform">
<table width="80%" border="0">
  <tr>
    <td>
      show users by
      <select name="breakdown">
        <option value="country">Country
        <option value="role">Role
        <option value="speciality">Speciality
        <option value="lead">Lead
        <option value="prof_category">RAG Professional Category
        <option value="main_speciality">RAG Main Speciality
        <option value="main_workplace">RAG Main Workplace
        <option value="role_specific">RAG Current Post
      </select>
    </td>
    <td>
      include users from :-<br>
      &nbsp;<input type="radio" name="allcountries" value="all" checked > All countries<br>
      &nbsp;<input type="radio" name="allcountries" value="selected" > Selected countries only<br>
    </td>
  </tr>

  <tr><td colspan="2">&nbsp;</td></tr>

  <tr>
    <td valign="top">
      <!-- include in this report :-<br>
      &nbsp;<input type="radio" name="users" value="all" checked > All users<br>
      &nbsp;<input type="radio" name="users" value="rag" > RAG users only<br>  -->
      &nbsp;
    </td>
    <td>
      <?
      $countries = GetArrayOfCountries();
//      $extra = "multiple size=\"10\" style=\"width:240px\" onMouseDown=\"GetCurrentListValues(this);\" onchange=\"FillListValues(this);\"";
 		$extra = "multiple size=\"10\" style=\"width:240px\"";
      arrayToListBox("countries[]",$countries,"",$extra);
      ?>
      <br>
      <a href="javascript:SelectAllList(document.myform.elements[5]);">select all</a> |
      <a href="javascript:DeselectAllList(document.myform.elements[5]);">select none</a>
    </td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" value="generate report"></td>
  </tr>
</table>
</form>
<? } ?>
</body>
</html>
