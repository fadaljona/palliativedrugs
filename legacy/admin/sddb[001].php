<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/form_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();

// 'staff' access hack (mk:25-08-2005)
$vid = $HTTP_SESSION_VARS["visitor"]->id;
if ($vid == 18660) {
		// let this user in ...
} else {
	// do normal authentication
	$allowed="admin";
	require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size=-2>admin</font></a> / 
<?if($_SERVER['REQUEST_METHOD']=="POST"){?>
<a href="sddb.php"><font size=-2>SDdb</font></a>
<?}?>
</td></tr></table>
<!-- end quick-nav bar -->
<br><strong>Syringe Driver Database</strong><br>
<br>
<?
if ($_SERVER['REQUEST_METHOD']=="POST") {
	$drug1 = $_POST['drug1'];
	$drug2 = $_POST['drug2'];
	$drug3 = $_POST['drug3'];
	$drug4 = $_POST['drug4'];
	$drug5 = $_POST['drug5'];
	$drug6 = $_POST['drug6'];
	$exact = IsSet($_POST['exact']) ? 1 : 0;
	
	if(!$drug1) {
		echo "choose at least one drug ...";
		exit();
	}
	
	if($exact) {
		$operator = "AND";
	} else {
		$operator = "OR";
	}
	
	$sql = "SELECT * FROM sddb WHERE (drug1_name='$drug1' OR drug2_name='$drug1' OR drug3_name='$drug1' OR drug4_name='$drug1' OR drug5_name='$drug1' OR drug6_name='$drug1')";
	if($drug2) {
		$sql .= " $operator (drug1_name='$drug2' OR drug2_name='$drug2' OR drug3_name='$drug2' OR drug4_name='$drug2' OR drug5_name='$drug2' OR drug6_name='$drug2')";
	}
	if($drug3) {
		$sql .= " $operator (drug1_name='$drug3' OR drug2_name='$drug3' OR drug3_name='$drug3' OR drug4_name='$drug3' OR drug5_name='$drug3' OR drug6_name='$drug3')";
	}
	
	if($drug4) {
		$sql .= " $operator (drug1_name='$drug4' OR drug2_name='$drug4' OR drug3_name='$drug4' OR drug4_name='$drug4' OR drug5_name='$drug4' OR drug6_name='$drug4')";
	}
	
	if($drug5) {
		$sql .= " $operator (drug1_name='$drug5' OR drug2_name='$drug5' OR drug3_name='$drug5' OR drug4_name='$drug5' OR drug5_name='$drug5' OR drug6_name='$drug5')";
	}
	
	if($drug6) {
		$sql .= " $operator (drug1_name='$drug6' OR drug2_name='$drug6' OR drug3_name='$drug6' OR drug4_name='$drug6' OR drug5_name='$drug6' OR drug6_name='$drug6')";
	}
	
	$resultspage = TRUE;
}

if($HTTP_GET_VARS['ac']=='lu') {
	$sql = "SELECT * FROM sddb WHERE approved=0 ORDER BY unit_code, unit_ref";
	$resultspage = TRUE;
}

if($HTTP_GET_VARS['ac']=='la') {
	$sql = "SELECT * FROM sddb ORDER BY unit_code, unit_ref";
	$resultspage = TRUE;
}

if($resultspage) {
	$rs=mysql_query($sql) or die(mysql_error()."<hr>$sql");
	if(mysql_num_rows($rs)==0) {
		echo "no matching entries found.";
	} else {
		echo "<table cellpadding=\"1\" cellspacing=\"3\" width=\"90%\" align=\"center\">";
		while($row=mysql_fetch_object($rs)) {
		//need to know how many drugs in this combo for rowspanning
			if($row->drug6_name) {
				$numdrugs=6;
			} elseif($row->drug5_name) {
				$numdrugs=5;
			} elseif($row->drug4_name) {
				$numdrugs=4;
			} elseif($row->drug3_name) {
				$numdrugs=3;
			} else {
				$numdrugs=2;
			}		
			?>
		<tr><td>

			<table width="100%" bgcolor="#e0e0e0">
		<!--	<tr><td>
			<table width="100%"> -->
			<tr>
				<th rowspan="2">Centre code / Record num</th>
				<th rowspan="2">Drug</th>
				<th rowspan="2">Dose in syringe (mg)</th>
				<th rowspan="2">Final volume in syringe (ml)</th>
				<th rowspan="2">Concentration (mg/ml)</th>
				<th rowspan="2">Diluent</th>
				<th colspan="3">Compatibility information</th>
				<th rowspan="2">Site reaction reported</th>
				<th rowspan="2">&nbsp;</th>
			</tr>
			<tr>
				<th>Outcome</th>
				<th>Duration</th>
				<th>Data type</th>				
			</tr>
			<tr>
				<td rowspan="<?=$numdrugs?>"><?=$row->unit_code;?> / <?=$row->unit_ref;?></td>
				<td><?=$row->drug1_name?></td>
				<td align="center"><?=$row->drug1_dose?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->volume?></td>
				<td align="center"><?=@number_format($row->drug1_dose / $row->volume,2)?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->diluent?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->compatibility?></td>
				<td align="center" rowspan="<?=$numdrugs?>">
					<?
					if($row->duration=='other') {
						echo $row->duration_other;
					} else {
						echo $row->duration;
					}
					?>
				</td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->datatype?></td>				
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->site_reaction?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><a href="sddb_edit.php?id=<?=$row->id?>">edit</a></td>
			</tr>
			<tr>
				<td><?=$row->drug2_name?></td>
				<td align="center"><?=$row->drug2_dose?></td>
				<td align="center"><?=@number_format($row->drug2_dose / $row->volume,2)?></td>
			</tr>
			
			<? if($row->drug3_name) { ?>
			<tr>
				<td><?=$row->drug3_name?></td>
				<td align="center"><?=$row->drug3_dose?></td>
				<td align="center"><?=@number_format($row->drug3_dose / $row->volume,2)?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug4_name) { ?>
			<tr>
				<td><?=$row->drug4_name?></td>
				<td align="center"><?=$row->drug4_dose?></td>
				<td align="center"><?=@number_format($row->drug4_dose / $row->volume,2)?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug5_name) { ?>
			<tr>
				<td><?=$row->drug5_name?></td>
				<td align="center"><?=$row->drug5_dose?></td>
				<td align="center"><?=@number_format($row->drug5_dose / $row->volume,2)?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug6_name) { ?>
			<tr>
				<td><?=$row->drug6_name?></td>
				<td align="center"><?=$row->drug6_dose?></td>
				<td align="center"><?=@number_format($row->drug6_dose / $row->volume,2)?></td>
			</tr>
			<? } ?>			
			
			</table>
		
			</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
	<?	} //while
		echo "</table>";
	} //else
} else {
?>
<a href="sddb_edit.php?id=0">create new entry</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="sddb.php?ac=lu">list unapproved entries</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="sddb.php?ac=la">list all entries</a>&nbsp;&nbsp;|&nbsp;&nbsp;
<a href="sddb_index.php">browse index</a>

<br><br>
or find an entry to edit -<br><br>
<form action="sddb.php" method="POST">
<table border="0" cellspacing="0" cellpadding="4">
<tr>
	<td>drug 1</td>
	<td>
		<?	
			$a[] = "";
			$drugs = array_merge($a,file($_SERVER["DOCUMENT_ROOT"]."/legacy/content/snippets/sddb_drug_list.txt"));
			arrayToListBox("drug1",$drugs,"", $extra="");
		?>	
	</td>
</tr>
<tr>
	<td>drug 2</td>
	<td><?arrayToListBox("drug2",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td>drug 3</td>
	<td><?arrayToListBox("drug3",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td>drug 4</td>
	<td><?arrayToListBox("drug4",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td>drug 5</td>
	<td><?arrayToListBox("drug5",$drugs,"", $extra="");?></td>
</tr>
<tr>
	<td>drug 6</td>
	<td><?arrayToListBox("drug6",$drugs,"", $extra="");?></td>
</tr>
<tr><td colspan="2" align="center"><input type="checkbox" name="exact" value=""> exact matches only</td></tr>
<tr>
	<td colspan="2" align="center">
		<input type="submit" value="find">		
	</td>
</tr>
</table>
</form>
<?}?>
</body>
</html>
