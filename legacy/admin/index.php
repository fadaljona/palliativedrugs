<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";

require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>palliativedrugs.com: admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body style="background-image:none;">
<h1>palliativedrugs.com: website administration</h1>
<table width="100%" border="1" cellspacing="0" cellpadding="4" bordercolor="#111111">
<tr><th align="left">NEW</th></tr>
<tr>
	<td align="left">
		<li> <a href="/legacy/admin/news.php">news / additions</a> </li>
		<li> <a href="/code/phpMyEdit/new_users2.php">users</a> </li>
		<li> <a href="/legacy/admin/new_user_breakdown.php">new users breakdown</a> </li>
		<li> <a target="_blank" href="https://palliativedrugs.com:2096/">webmail login</a> </li>
	</td>
</tr>
<tr><th align="left">users</th></tr>
<tr>
		<td align="left">
				<li><a href="/legacy/admin/users.php">users</a>
				<li><a href="/legacy/admin/new_users.php">new registrations</a>
				<li><a href="/legacy/admin/repUsersByCountry.php">report - users by country</a>
				<li><a href="/legacy/admin/repUsersByRole.php">report: users - by role</a>
				<li><a href="/legacy/admin/repUsersBySpeciality.php">report - users by speciality</a>
				<li><a href="/legacy/admin/repUsersByLead.php">report - users by lead</a>
        <li><a href="/legacy/admin/reports.php">Custom user reports</a>
        <li><a href="/legacy/admin/check-emails.php">invalid e-mail addresses</a>
        <li><a href="/legacy/admin/groups.php">user groups</a>
		</td>
</tr>
<tr><th align="left">forum</th></tr>
<tr>
		<td align="left">
				<li><a href="/forum51/admin.php">forum admin menu</a>
		  <li><a href="/forum51/control.php?0,panel=messages">approve / delete new posts</a>
	</td>
</tr>
<tr><th align="left">newsletter</th></tr>
<tr>
		<td align="left">
				<li><a href="/legacy/admin/newsAdmin.php">newsletter admin</a>
		</td>
</tr>
<tr><th align="left">survey</th></tr>
<tr>
		<td align="left">
				<li><a href="/legacy/admin/survAdmin.php">survey admin</a>
				<li><a href="/legacy/admin/RAGadmin.php">RAG admin</a>
		</td>
</tr>
<tr><th align="left">SDdb</th></tr>
<tr>
	<td align="left">
		<li><a href="/legacy/admin/sddb.php">Syringe Driver Database</a></li>
		<li><a href="/sddb.php">SD-Db (user)</a></li>
	</td>	
</tr>
<tr><th align="left">misc.</th></tr>
<tr>
		<td align="left">
				<li><a href="snippets.php">edit snippets</a>
				<li><a href="/palladmin/pmail/mailman.php">mailman</a>
				<li><a href="/legacy/admin/dbtool/">dbTool (SQL)</a>
				<li><a href="http://palliativedrugs.com:2082/frontend/rvblue/stats/awstats.html" target="_blank">web stats</a> (separate window/login)
		</td>
</tr>
</table>

</body>
</html>