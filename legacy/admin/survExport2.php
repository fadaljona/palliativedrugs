<?php ob_start();
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

function one_of($qid, $ord) {
	return dLookUp("answer","survanswers","question_id=$qid and ord=$ord");
}

function many_of($qid, $ans) {
	$result="";
	$arr = explode(",",$ans);
	if($ans!="NONE") {
		for ($i=0; $i < count($arr); $i++) {
			$where = "question_id=" . $qid . " AND ord=" . $arr[$i];
			$result .= dLookUp("answer","survanswers",$where) . " | ";
		}
	} else {
		$result = '&nbsp;';
	}
	return $result;
}

$sid = $_GET["id"];
$filt = stripslashes(urldecode($_GET["filt"]));

$sql = "select survresults.username, "
		. "survquestion.id as question_id, title_text as question, type, survresults.answer"
		. " from survresults, survquestion"
		. " where survresults.survey_id =" .$sid
		. " and survquestion.id = survresults.question_id";

$rs = mysql_query($sql) or die(mysql_error() . "<hr>$sql<hr>");

echo "<!-- $sql -->";

echo "user,speciality,organisation,addr1,country,question,answer<br>";

while($row=mysql_fetch_object($rs)) {

	$dquery	=	"SELECT role, speciality, organisation, addr1, country FROM new_users WHERE username = '".$row->username."' ";

	if(trim($filt)!="like '%'")
		$sql .= " and country " . $filt;

	$selDet	=	mysql_query($dquery)or die(mysql_error());	
	$spec	=	'';
	$org	=	'';
	$add	=	'';
	$country=	'';
	if(mysql_num_rows($selDet)>0){		
		while($drow		=	mysql_fetch_array($selDet)){
			$spec	=	$drow['speciality'];
			$org	=	$drow['organisation'];
			$add	=	$drow['addr1'];
			$country=	$drow['country'];			
		}
	}
	
    $csv_row=	str_replace(","," ",$row->username) . ","
        . str_replace(","," ",$spec) . ","
        . str_replace(","," ",$org) . ","
		. str_replace(","," ",$add) . ","
      	. str_replace(","," ",$country) . ","
        . str_replace(","," ",$row->question) . ",";
		
	if ($row->type=="one_of") {
		$csv_row .= str_replace(","," ",one_of($row->question_id, $row->answer));
	} elseif ($row->type=="many_of") {
		$csv_row .= str_replace(","," ",many_of($row->question_id, $row->answer));
	} else {
		$csv_row .= str_replace(","," ",$row->answer);
	}

    echo $csv_row . "<br>";

}
?>
