<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>&copy;2001-2008 palliativedrugs.com</title>
<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<?
$rs = mysql_query("SELECT COUNT(*) as total_users FROM new_users where RegistrationChecked=1") or die(mysql_error());
$row = mysql_fetch_object($rs);
$total_users = $row->total_users;
mysql_free_result($rs);

$rs = mysql_query("select speciality, count(*) as num_users from new_users where RegistrationChecked=1 group by speciality order by 2 desc") or die(mysql_error());

?>
<body>
<font size=+1>palliativedrugs.com: Users by Speciality</font><br>
<br>
<font size=-1>
date: <?echo Date("F d, Y")?><br>
total users: <?=$total_users?></font>
<br><br>
<table bgcolor=#f0f0f0>
<?
while($row=mysql_fetch_object($rs)) {
	$pcnt = ($row->num_users / $total_users) * 100;
	$pcnt = number_format($pcnt, 2);
	$bar_width = $pcnt * 6;
	echo "<tr>"
		. "<td>$row->speciality</td>"
		. "<td>$row->num_users</td>"
		. "<td>(" . round($pcnt) ."%)</td>"
		. "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>"
		. "</tr>";
}
?>
</table>
</body>
</html>
