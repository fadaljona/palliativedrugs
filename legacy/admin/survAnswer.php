<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed='admin';
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

if ($_SERVER['REQUEST_METHOD']=='POST') {
	// form subbed, so save or update ...
	$sid = $_POST["hidSid"];
	$qid = $_POST["hidQid"];
	$aord = $_POST["hidAord"];
	$answer = $_POST["txtAnswer"] . " ";
	$default = ($_POST["chkDefault"]) ? 1 : 0;
	
	if ($aord==0) {
		//it's a new one, so determine new ord, then insert...			
		$sql = "SELECT MAX(ord)+1 AS new_ord FROM survanswers WHERE question_id=$qid";
		$rs=mysql_query($sql) or die (mysql_error()."<hr>".$sql);		
		if (mysql_num_rows($rs)==0) {
			$new_ord = 1;
		} else {			
			$row=mysql_fetch_object($rs);
			$new_ord = $row->new_ord;
		}
		if ($new_ord=="") { 
			$new_ord=1;
		}			
		$sql= "INSERT INTO survanswers"
			. "(question_id, answer, ord, is_default)"
			. " VALUES ($qid, '$answer', $new_ord, $default)";
							
		mysql_query($sql) or die("add survey<hr>".mysql_error()."<hr>$sql");
		header("Location:  survQuestion.php?sid=$sid&&qid=$qid");
		//echo "new answer posted<br>";
		//echo "<a href=\"survQuestion.php?sid=$sid&&qid=$qid\">survQuestion.php?sid=$sid&&qid=$qid </a>";
		exit;
	} else {
		// it's an update
		$sql = "UPDATE survanswers"
			. " SET question_id=$qid,"
			. "	answer='$answer',"
			. " is_default=$default"
			. " WHERE question_id=$qid AND ord=$aord";
		
		mysql_query($sql) or die("surv update<hr>".mysql_error()."<hr>$SQL");
		header("Location:  survQuestion.php?sid=$new_id&&qid=$qid");
		//echo "answer updated";
		//echo "<a href=\"survQuestion.php?sid=$sid&&qid=$qid\">survQuestion.php?sid=$sid&&qid=$qid</a>";
		exit;
	}
} else {
	$qid = $_GET['qid'];
	$aord = $_GET['aord'];
	$sid = $_GET['sid'];
	if ($aord==0) {
		$answer="";
		$ord=0;
		$default=FALSE;
		$pagetile="New Answer";
	} else {
		$sql = "SELECT * FROM survanswers WHERE question_id=$qid AND ord=$aord";
		$rs = mysql_query($sql) or die(mysql_error."<hr>".$sql);
		$row = mysql_fetch_object($rs);
		$answer=$row->answer;
		$ord=$row->ord;
		$default=$row->is_default;
		$pagetile="Edit Answer";		
	}//if ($aord==0)
}//if ($REQUEST_METHOD=="POST")
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">	
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size="-2">admin</font></a> / 
<a href="survAdmin.php"><font size="-2">survey admin</font></a> /
<a href="survEditSurvey.php?sid=<?=$sid?>"><font size="-2">edit survey</font></a> /
<a href="survQuestion.php?Qid=<?=$qid?>&&sid=<?=$sid?>"><font size=-2>edit question</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<br>
<font size=+1><b><?=$pagetitle?></b></font><br>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
<input type=hidden name=hidQid value='<?=$qid?>'>
<input type=hidden name=hidSid value='<?=$sid?>'>
<input type=hidden name=hidAord value='<?=$aord?>'>
<table border="1" cellspacing="0" cellpadding="4">
<tr>
	<td>answer</td>
	<td><textarea name="txtAnswer" rows="4" cols="80"><?=$answer?></textarea></td>
</tr>
<tr>
	<td>default</td>
	<td>
		<? $defchk = ($default==TRUE) ? " checked" : "";?>
		<input type="checkbox" name="chkDefault" <?=$defchk?>>
	</td>
</tr>
<tr>
	<td colspan=2>
		<? $btn_caption = ($aord==0) ? "save" : "update";?>
		<input type="submit" value="<?=$btn_caption?>">
	</td>
</tr>
</table>
</form>

</body>
</html>
