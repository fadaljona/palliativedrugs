<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
//require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

// was 'ac' (action) parameter supplied?
if (IsSet($_GET["ac"])) {
	if ($_GET["ac"]=="del") {
		$uid = ($_GET[uid]);
		$un = dLookUp("username","new_users","id=$uid");
		$sql = "DELETE FROM new_users WHERE id=$uid";
		mysql_query($sql) or die (mysql_error());
		echo "<b>$un</b> deleted, click <a href=\"/admin/index.php\">here</a> to continue";
		exit;
	}
}

// was form subbed ??
if ($_SERVER['REQUEST_METHOD']=="POST") {
	$uid		= trim($_POST["hid_uid"]);
	$email 		= AddSlashes(trim($_POST["txt_email"]));
	$pwd1 		= trim($_POST["txt_pwd1"]);
	$pwd2 		= trim($_POST["txt_pwd2"]);
	$FirstName 	= AddSlashes(trim($_POST["txt_FirstName"]));
	$LastName 	= AddSlashes(trim($_POST["txt_LastName"]));
	$title 		= AddSlashes(trim($_POST["sel_title"]));
	$role 		= AddSlashes(trim($_POST["txt_role"]));
	$speciality 	= AddSlashes(trim($_POST["txt_speciality"]));
	$organisation 	= AddSlashes(trim($_POST["txt_organisation"]));
	$addr1 		= AddSlashes(trim($_POST["txt_addr1"]));
	$addr2 		= AddSlashes(trim($_POST["txt_addr2"]));
	$addr3 		= AddSlashes(trim($_POST["txt_addr3"]));
	$addr4 		= AddSlashes(trim($_POST["txt_addr4"]));
	$TownOrCity 	= AddSlashes(trim($_POST["txt_TownOrCity"]));
	$StateOrCounty 	= AddSlashes(trim($_POST["txt_StateOrCounty"]));
	$PostalCode 	= AddSlashes(trim($_POST["txt_PostalCode"]));
	$Country 	= AddSlashes(trim($_POST["sel_Country"]));
	$phone 		= AddSlashes(trim($_POST["txt_phone"]));
	$AcceptPDImails = (trim($_POST["chk_AcceptPDImails"])) ? 1 : 0;
	$AcceptBBmails 	= (trim($_POST["chk_AcceptBBmails"])) ? 1 : 0;
        $UserType	= trim($_POST["sel_UserType"]);
	$lead		= AddSlashes(trim($_POST["txt_lead"]));
        $active 	= (trim($_POST["chk_active"])) ? 1 : 0;
	$vetted 	= (trim($_POST["vetted"])) ? 1 : 0;


	$errors = array();

	//validate email
	if($email) {
		//if(!validEmail($email)) {
		//	$errors["email"] = "please enter a valid e-mail address.";
		//}
	} else {
		$errors["email"] = "you must enter an email address.";
	}

	//validate password
	if($pwd2 AND $pwd1){
		if(!($pwd1==$pwd2)) {
			$errors["password"] = "your passwords did not match.";
		}
	}
	//validate FirstName
	if($FirstName){
		// no further validation.
	} else {
		$errors["first name"] = "you must enter a first name.";
	}

	//validate LastName
	if($LastName){
		// no further validation.
	} else {
		$errors["last name"] = "you must enter a last name.";
	}

	//validate org
	if($organisation){
		// no further validation.
	} else {
		$errors["organisation"] = "please enter your organisation name.";
	}

	//validate addr1+2
	if($addr1){
		// no further validation.
	} else {
		$errors["work address"] = "please enter your work address.";
	}

	//validate city
	if($TownOrCity){
		// no further validation.
	} else {
		$errors["city"] = "please enter your town or city name.";
	}

	//validate state
	if($StateOrCounty){
		// no further validation.
	} else {
		$errors["state/county"] = "please enter your state or county name.";
	}

	//validate zip
	if($PostalCode){
		// no further validation.
	} else {
		$errors["postcode/zip"] = "please enter your postcode or zip.";
	}

	//validate phone number
	if($phone){
		// no further validation.
	} else {
		$errors["phone"] = "please enter your work telephone number.";
	}

	// end of field validation routines.

	if(sizeof($errors)==0) {
		$sql = "UPDATE new_users SET "
			. "email = '$email', ";
		if (IsSet($pwd1))
			$sql .= "pwd = '$pwd1', ";
		$sql .= "FirstName = '$FirstName', "
			 . "LastName = '$LastName', "
			 . "title = '$title', "
			 . "role = '$role', "
			 . "speciality = '$speciality', "
			 . "organisation = '$organisation', "
			 . "addr1 = '$addr1', "
			 . "addr2 = '$addr2', "
			 . "addr3 = '$addr3', "
			 . "addr4 = '$addr4', "
			 . "TownOrCity = '$TownOrCity', "
			 . "StateOrCounty = '$StateOrCounty', "
			 . "PostalCode = '$PostalCode', "
			 . "Country = '$Country', "
			 . "phone = '$phone', "
			 . "lead = '$lead', "
			 . "AcceptPDImails = $AcceptPDImails, "
			 . "AcceptBBmails = $AcceptBBmails, "
			 . "UserType = '$UserType', "
			 . "active = $active, "
			 . "vetted = $vetted "
			 . "WHERE id=$uid";

		mysql_query($sql) or die(mysql_error() ."<hr>$sql");

		if($_POST["is_rag_user"]=="1") {
			//save rag fields also
			$sex				= trim($_POST["sex"]);
			$prof_category 			= AddSlashes(trim($_POST["prof_category"]));
			$main_speciality 		= AddSlashes(trim($_POST["main_speciality"]));
			$main_workplace 		= AddSlashes(trim($_POST["main_workplace"]));
			$year_of_birth 			= trim($_POST["year_of_birth"]);
			$years_qualified 		= trim($_POST["years_qualified"]);
			$years_in_current_post 	        = trim($_POST["years_in_current_post"]);
			$role_specific 			= AddSlashes(trim($_POST["role_specific"]));
			$main_area 			= AddSlashes(trim($_POST["main_area"]));

                        if(isset($_POST["subDecline"])) {
                                $rag_vetted  = 2;
                        } elseif(isset($_POST["subRequest"])) {
                                $rag_vetted  = 3;
                        } else {
                                $rag_vetted = (trim($_POST["rag_vetted"])) ? 1 : 0;
                        }

			$sql = "UPDATE rag_user SET "
				. "sex = '$sex', "
				. "prof_category ='$prof_category', "
				. "main_speciality = '$main_speciality', "
				. "main_workplace = '$main_workplace', "
				. "year_of_birth = '$year_of_birth', "
				. "years_qualified = $years_qualified, "
				. "years_in_current_post = $years_in_current_post, "
				. "role_specific = '$role_specific', "
				. "main_area = '$main_area', "
				. "vetted = $rag_vetted "
				. "WHERE user_id=$uid";
			mysql_query($sql) or die(mysql_error() ."<hr>$sql");
		}
		
		//change loc below to user page (with filter/page num intact!!)
		$loc = $_POST["calling_page"];
//		echo "<a href=\"$loc\">$loc</a>";
		header("Location:  $loc");
		exit;
	} 
} else {
	$uid = $_GET["uid"];
	if(IsSet($_GET["cp"])) {
		$calling_page = $_GET["cp"];
	} else {
		$calling_page = "users.php";
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>&copy;2002 palliativedrugs.com</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
	<script language="Javascript">
	<!--
	function DeleteUser(userid,username)
	{
	   if (confirm("DELETE: " + username + " ?")) {
	   		
			location.href="/legacy/admin/edit_user.php?ac=del&uid=" + userid;
			return true;	
	   }
	}
	// -->
	</script>
</head>
<body>
<h1>edit user</h1>
<?
if ($_SERVER['REQUEST_METHOD']=="GET") {
	$sql = "SELECT * FROM new_users WHERE id = $uid";
	$rs = mysql_query($sql);
	if($rs) {
		$row = mysql_fetch_object($rs);
		$username 		= StripSlashes($row->username);
		$email 			= StripSlashes($row->email);
		$pwd1 			= $row->pwd;	
		$FirstName 		= StripSlashes($row->FirstName);
		$LastName 		= StripSlashes($row->LastName);
		$title 			= StripSlashes($row->title);
		$role 			= StripSlashes($row->role);
		$speciality 	= StripSlashes($row->speciality);
		$organisation 	= StripSlashes($row->organisation);
		$addr1 			= StripSlashes($row->addr1);
		$addr2 			= StripSlashes($row->addr2);
		$addr3 			= StripSlashes($row->addr3);
		$addr4 			= StripSlashes($row->addr4);	
		$TownOrCity 	= StripSlashes($row->TownOrCity);
		$StateOrCounty 	= StripSlashes($row->StateOrCounty);
		$PostalCode 	= StripSlashes($row->PostalCode);
		$Country 		= StripSlashes($row->Country);
		$phone 			= StripSlashes($row->phone);
		$AcceptPDImails = $row->AcceptPDImails;
		$AcceptBBmails 	= $row->AcceptBBmails;
		$lead			= StripSlashes($row->lead);
	    $UserType 		= $row->UserType;
	    $active 		= $row->active;
		$vetted 		= $row->vetted;
	} else {
		echo "error opening user database.";
		exit;
	}
	$rs = mysql_query("SELECT * FROM rag_user WHERE user_id=$uid");
	if($rs) {
		if(mysql_num_rows($rs) > 0 ) {
			$row = mysql_fetch_object($rs);
			$sex					= $row->sex;
			$prof_category 			= StripSlashes($row->prof_category);
			$main_speciality 		= StripSlashes($row->main_speciality);
			$main_workplace 		= StripSlashes($row->main_workplace);
			$year_of_birth 			= $row->year_of_birth;
			$years_qualified 		= $row->years_qualified;
			$years_in_current_post 	        = $row->years_in_current_post;
			$role_specific 			= StripSlashes($row->role_specific);
			$main_area 			= StripSlashes($row->main_area);
                        $rag_approval_status            = StripSlashes($row->approval_status);
			$rag_vetted 			= $row->vetted;
			$is_rag_user = TRUE;
		} else {
			$is_rag_user = FALSE;
		}
	} else {
		echo "error opening user database.";
		exit;
	}
}//if ($_SERVER['REQUEST_METHOD']=="GET")

if(sizeof($errors)>0) {
	print "<span class=\"warning\">errors occured, please correct the following:-</span><br>";
	foreach($errors as $key => $value)
		//echo "<a href=\"#$key\"><LI>$key : $value</a><br>\n";
		echo "<LI>$key : $value<br>\n";
}

?>
<form action="<?=$_SERVER[PHP_SELF]?>" method="POST">
<input type="hidden" name="hid_uid" value="<?echo $uid?>">
<input type="hidden" name="calling_page" value="<?echo $calling_page?>">
<table>
<tr><td>username</td><td><b><?=$username?></b></td></tr>
<tr><td>email address</td><td><input type="text" name="txt_email" maxlength="200" size="30" value="<?=$email?>"></td></tr>
<tr><td>password</td><td><input type="password" name="txt_pwd1" maxlength="80" size="30" value=<?=$pwd1?>></td></tr>
<tr><td>re-enter password</td><td><input type="password" name="txt_pwd2"  maxlength="80" size="30" value="<?=$pwd1?>"></td></tr>
<tr><td>FirstName </td><td><input type="text" name="txt_FirstName" maxlength="200" size="30" value="<?=$FirstName?>"></td></tr>
<tr><td>LastName</td><td><input type="text" name="txt_LastName" maxlength="200" size="30" value="<?=$LastName?>"></td></tr>
<tr>
	<td>title</td>
	<td>
		<?
		$options=array("Professor","Doctor","Mr","Mrs","Ms","Miss","Sister");
		$default = (IsSet($title)) ? $title : "Doctor";
		arrayToListBox("sel_title",$options,$default);
		?>		
	</td>
</tr>
<tr>
	<td>role</td>
	<td>
		<input type="text" name="txt_role" maxlength="200" size="30" value="<?=$role?>">
	</td>
</tr>

<tr><td>speciality</td><td><input type="text" name="txt_speciality" maxlength="200" size="30" value="<?=$speciality?>"></td></tr>

<tr><td>organisation</td><td><input type="text" name="txt_organisation" maxlength="200" size="50" value="<?=$organisation?>"></td></tr>
<tr><td>addr1</td><td><input type="text" name="txt_addr1" maxlength="200" size="50" value="<?=$addr1?>"></td></tr>
<tr><td>addr2</td><td><input type="text" name="txt_addr2" maxlength="200" size="50" value="<?=$addr2?>"></td></tr>
<tr><td>addr3</td><td><input type="text" name="txt_addr3" maxlength="200" size="50" value="<?=$addr3?>"></td></tr>
<tr><td>addr4</td><td><input type="text" name="txt_addr4" maxlength="200" size="50" value="<?=$addr4?>"></td></tr>
<tr><td>TownOrCity</td><td><input type="text" name="txt_TownOrCity" maxlength="200" size="50" value="<?=$TownOrCity?>"></td></tr>
<tr><td>StateOrCounty</td><td><input type="text" name="txt_StateOrCounty" value="<?=$StateOrCounty?>"></td></tr>
<tr><td>PostalCode</td><td><input type="text" name="txt_PostalCode" value="<?=$PostalCode?>"></td></tr>
<tr>
	<td>Country</td>
	<td>
		<?
		$options=array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", 
		"Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
		"Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh",
		"Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", 
		"Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", 
		"British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso",
		"Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
		"Central African Republic", "Chad", "Chile", "China", "Christmas Island", 
		"Cocos Keeling Islands", "Colombia", "Comoros", "Congo (Brazzaville)", 
		"Congo, Democratic Republic of", "Cook Islands", "Costa Rica", "Croatia Hrvatska",
		"Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
		"Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", 
		"Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", 
		"Falkland Islands, Malvinas", "Faroe Islands", "Fiji", "Finland", "France",
		"French Guiana", "French Polynesia", "French Southern Territories", "Gabon",
		"Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Great Britain (UK)",
		"Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea",
		"Guinea-Bissau", "Guyana", "Haiti", "Heard and McDonald Islands", "Honduras",
		"Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland",
		"Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati",
		"Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon",
		"Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau",
		"Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta",
		"Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico",
		"Micronesia", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique",
		"Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", 
		"New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island",
		"Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", 
		"Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", 
		"Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", 
		"S. Georgia and S. Sandwich Isls.", "Saint Kitts and Nevis", "Saint Lucia", 
		"Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", 
		"Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovak Republic", 
		"Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", 
		"St. Helena & Dependencies", "St. Pierre and Miquelon", "Sudan", "Suriname", 
		"Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syria", 
		"Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", 
		"Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", 
		"Tuvalu", "U.S. Minor Outlying Islands", "Uganda", "Ukraine", "United Arab Emirates", 
		"United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", 
		"Vatican City State", "Venezuela", "Viet Nam", "Virgin Islands (British)", 
		"Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", 
		"Yugoslavia", "Zambia", "Zimbabwe");
		$default = (IsSet($Country)) ? $Country : "United Kingdom";
		arrayToListBox("sel_Country",$options,$default);
		?>	
	</td>
</tr>
<tr><td>phone</td><td><input type="text" name="txt_phone" value="<?=$phone?>"></td></tr>
<tr>
	<td>AcceptPDImails</td>
	<td>
		<input type="checkbox" name="chk_AcceptPDImails" <?if($AcceptPDImails) echo "checked"?>> 
	</td>
</tr>
<tr>
	<td>AcceptBBmails</td>
	<td>
		<input type="checkbox" name="chk_AcceptBBmails" <?if($AcceptBBmails) echo "checked"?>> 
	</td>
</tr>
<tr><td>lead</td><td><input type="text" name="txt_lead" value="<?=$lead?>"></td></tr>
<tr>
	<td>vetted</td>
	<td><input type="checkbox" name="vetted" <?if($vetted) echo "checked"?>> </td>
</tr>
<tr>
	<td>user type</td>
	<td>
		<?
		$options=array("user","admin");
		$default = (IsSet($UserType)) ? $UserType : "user";
		arrayToListBox("sel_UserType",$options,$default);
		?>		
	</td>
</tr>
<tr>
	<td>active</td>
	<td>
		<input type="checkbox" name="chk_active" <?if($active) echo "checked"?>> 
	</td>
</tr>
<? if($is_rag_user && ($rag_vetted!=2)) { ?>
<tr><td colspan="2" bgcolor="#C0C0C0">RAG Fields</td></tr>
<tr>
	<td>sex</td>
	<td><input type="text" size=1 name="sex" value="<?=$sex?>"></td>
</tr>
<tr>
	<td>prof_category</td>
	<td><input type="text" size=40 name="prof_category" value="<?=$prof_category?>"></td>
</tr>
<tr>
	<td>main_speciality</td>
	<td><input type="text" size=60 name="main_speciality" value="<?=$main_speciality?>"></td>
</tr>
<tr>
	<td>main_workplace</td>
	<td><input type="text" size=60 name="main_workplace" value="<?=$main_workplace?>"></td>
</tr>
<tr>
	<td>year_of_birth</td>
	<td><input type="text" size=4 name="year_of_birth" value="<?=$year_of_birth?>"></td>
</tr>
<tr>
	<td>years_qualified</td>
	<td><input type="text" size=2 name="years_qualified" value="<?=$years_qualified?>"></td>
</tr>
<tr>
	<td>years_in_current_post</td>
	<td><input type="text" size=2 name="years_in_current_post" value="<?=$years_in_current_post?>"></td>
</tr>
<tr>
	<td>current post</td>
	<td><input type="text" size=60 name="role_specific" value="<?=$role_specific?>"></td>
</tr>
<!-- <tr>
	<td>main_area</td>
	<td><input type="text" size=40 name="main_area" value="<?=$main_area?>"></td>
</tr> -->
<tr>
	<td>vetted</td>
	<td><input type="checkbox" name="rag_vetted" <?if($rag_vetted==1) echo "checked"?>> </td>
</tr>
<?if(!($rag_vetted==1)) { ?>
<tr>
        <td colspan="2" align="center">
                <input type="submit" name="subDecline" value="decline RAG application">
                <?if(!($rag_vetted==3)){?>
                <input type="submit" name="subRequest" value="set status to PENDING">
                <a href="mailto:<?=$email?>"> (e-mail this user</a>)
                <?} else {?>
                [PENDING]
                <?}?>
        </td>
</tr>
<? } ?>
<? } ?>
<tr>
	<td colspan="2" align="center">
		<input type="submit" name="subUpdate" value="update">
		<input type="Button" value="cancel" onClick="Javascript:history.go(-1);">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" value="delete this user" onClick="Javascript:DeleteUser(<?=$uid?>,'<?= str_replace("'","\'",$username)?>');">
	</td>
</tr>
</table>
<input type="hidden" name="is_rag_user" value="<?=$is_rag_user?>">
</form>
</body>
</html>