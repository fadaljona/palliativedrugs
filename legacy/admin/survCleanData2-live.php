<?php
include("$_SERVER[DOCUMENT_ROOT]/_include/config.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/_include/db_func.inc.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_include/visitor.class.php");
session_start();
$allowed="admin";
require_once("$_SERVER[DOCUMENT_ROOT]/_include/secure.inc.php");

function one_of($qid, $ord) {
	return dLookUp("answer","survanswers","question_id=$qid and ord=$ord");
}

function many_of($qid, $ans) {
	$result="";
	$arr = explode(",",$ans);
	for ($i=0; $i < count($arr); $i++) {
		$where = "question_id=" . $qid . " AND ord=" . $arr[$i];
		$result .= dLookUp("answer","survanswers",$where) . " | ";
	}
	return $result;
}

function delete_rows($keep_one) {
global $survey_id, $question_id, $answer, $ans_other, $long_answer, $username;
	$sql = "DELETE FROM survresults WHERE "
			. "survey_id=" . $survey_id
			. " AND question_id=" . $question_id
			. " AND username='" . $username . "' ";
	if($answer) { $sql .= " AND answer='" . $answer . "' "; }
	if($ans_other) { $sql .= " AND ans_other='" . $ans_other . "' "; }
	if($long_answer) { $sql .= " AND long_answer='" . $long_answer . "' "; }
			
	mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	if($keep_one==TRUE) {
		$sql = "INSERT INTO survresults VALUES ("
				. $survey_id . ", "
				. $question_id . ", "
				. "'" . $answer . "', "
				. "'" . $ans_other . "', "
				. "'" . $long_answer . "', "
				. "'" . $username . "'"
				.")";
	mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	}
}

function check_dupes() {
global $survey_id, $question_id, $answer, $ans_other, $long_answer, $username;
	$dupes = FALSE;
	$sql = "SELECT  * from survresults WHERE "
			. "survey_id=" . $survey_id
			. " AND question_id=" . $question_id
			. " AND username='" . $username . "' ";
	if($answer) { $sql .= " AND answer='" . $answer . "' "; }
	if($ans_other) { $sql .= " AND ans_other='" . $ans_other . "' "; }
	if($long_answer) { $sql .= " AND long_answer='" . $long_answer . "' "; }
	//echo "<br>function check_dupes() " . $sql . "<br>";
	$rs = mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
	if($rs) {
		//echo "<br>recordset is good (" .mysql_num_rows($rs) .")<br>";
	} else {
		//echo "<br>recordset is good<br>";
	}
	if(mysql_num_rows($rs) > 1) {
		$dupes=TRUE;
	} 
return $dupes;
}

if ($_SERVER['REQUEST_METHOD']=="POST") {
	$survey_id= $_POST["survey_id"];
    $question_id= $_POST["question_id"];
    $answer= $_POST["answer"];
    $ans_other= $_POST["ans_other"];
    $long_answer= $_POST["long_answer"];
    $username= $_POST["username"];
    $page= $_POST['page'];
	$fltUsername= $_POST['fltUsername'];

	if(IsSet($_POST["btnApplyFilter"])) {
		$page= 0;
	}
	if(IsSet($_POST["btnRemoveFilter"])) {
		$page= 0;
  		$fltUsername = "";
	}
	if(IsSet($_POST["btnNextPage"])) {
		$page = $page + 1;
	}
	if(IsSet($_POST["btnPrevPage"])) {
		$page = $page - 1;
	}
	if(IsSet($_POST["btnDeleteRow"])) {
		//echo "del row";
		if(check_dupes()==TRUE) {
			//echo " dupes";
			// if dupes show special screen - del all/keep one/cancel
			$dupes = TRUE;
		} else {
			delete_rows(FALSE);
		}
	}
	if(IsSet($_POST["btnKeepOne"])) {
		delete_rows(TRUE);	// del all but one
	}
	if(IsSet($_POST["btnKeepNone"])) {
		delete_rows(FALSE);	// dell all
	}
	if(IsSet($_POST["btnDeleteAll"])) {
		$sql = "DELETE FROM survresults WHERE "
				. "survey_id=" . $survey_id
				. " AND username='" . $fltUsername . "' ";
		mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
		$fltUsername = "";
		$page = 0;
	}
} else {
	$survey_id= $_GET["survey_id"];
    $page= 0;
  	$fltUsername = "";
}

$self = $_SERVER[PHP_SELF];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>pall:admin - clean survey data</title>
<link rel="STYLESHEET" type="text/css" href="/admin/admin.css">
</head>
<body>
<? if($dupes==TRUE) {?>
	<form action="<?=$self?>" method="post" name="frmDelete" id="frmDelete">
    <input name="survey_id" type="hidden" value="<?= $survey_id?>">
    <input name="question_id" type="hidden" value="<?= $question_id?>">
    <input name="answer" type="hidden" value="<?= $answer?>">
    <input name="ans_other" type="hidden" value="<?= $ans_other?>">
    <input name="long_answer" type="hidden" value="<?= $long_answer?>">
    <input name="username" type="hidden" value="<?= $username?>">
    <input name="page" type="hidden" value="<?= $page ?>">
	<input name="fltUsername" type="hidden" value="<?= $fltUsername ?>">
	there are exact duplicate rows, keep one ?<br><br>
	<input name="btnKeepOne" type="submit" class="small" value="keep one">
   	<input name="btnKeepNone" type="submit" class="small" value="delete all">
	<input name="btnCancel" type="button" class="small" value="cancel" onClick="Javascript:history.go(-1);">
	</form> 
<?exit; }  ?>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size="-2">admin</font></a> / 
<a href="survAdmin.php"><font size="-2">survey admin</font></a> / 
<a href="survResults.php?sid=<?=$survey_id?>"><font size="-2">survey results</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<br><b>survey: clean data</b><br><br>
<form name="frmFilter" action="<?=$self?>" method="POST">
<input type="hidden" name="survey_id" value="<?=$survey_id?>">
<input type="hidden" name="page" value="<?=$page?>">

filter by username: 
<?=selbox("fltUsername", "", "SELECT DISTINCT(username) FROM survresults WHERE survey_id=".$survey_id, $fltUsername, "| - SHOW ALL - ", "");?>
&nbsp; 
	<input type="submit" class="small_btn" name="btnApplyFilter" value="apply filter">
	<input type="submit" class="small_btn" name="btnRemoveFilter" value="remove filter">

<?if($fltUsername != "") {?>
&nbsp;&nbsp;&nbsp; <input type="submit" name="btnDeleteAll" id="btnDeleteAll" value="delete ALL rows for this user">
<?}?>
</form>
<table border="1" cellspacing="0" cellpadding="4" width="100%">
<tr>
	<th>question</th>
	<th>answer</th>
	<th>other</th>
	<th>long_ans</th>
	<th>username</th>
	<th>&nbsp;</th>
</tr>

<?php
$pagesize=50;

$sql = "select * from survresults where survey_id=" . $survey_id;
if ($fltUsername != "") {
		$sql .= " and username = '".$fltUsername."' ORDER BY question_id";
	}

if(!$page or !(is_numeric($page)) or $page < 0) 
	$page=1;

$sql .= " LIMIT " . ($pagesize*($page-1)) . ", " . ($pagesize+1);

$rs = mysql_query($sql) or die(mysql_error() . "<hr>$sql<hr>");

if (mysql_affected_rows() > $pagesize)
	$morepages=1;

while($row=mysql_fetch_object($rs)) {
	$q_type = dLookUp("type","survquestion","id=".$row->question_id);
	if ($q_type=="one_of") {
		$ans = one_of($row->question_id, $row->answer);
	} elseif ($q_type=="many_of") {
		$ans = many_of($row->question_id, $row->answer);
	} else {
		$ans = $row->answer;
	} ?>
	<tr>
		<td><b><?=StripSlashes($row->question_id);?></b> <?=dLookUp("title_text","survquestion","id=".$row->question_id);?></td>
		<td><?=StripSlashes($ans)?></td>
        <td><?=StripSlashes($row->ans_other)?></td>
        <td><?=StripSlashes($row->long_answer)?></td>
		<td><?=$row->username?></td>
        <td>
			<form action="<?=$self?>" method="post" name="frmDelete" id="frmDelete">
        	<input name="survey_id" type="hidden" value="<?= $row->survey_id?>">
        	<input name="question_id" type="hidden" value="<?= $row->question_id?>">
        	<input name="answer" type="hidden" value="<?= $row->answer?>">
        	<input name="ans_other" type="hidden" value="<?= $row->ans_other?>">
        	<input name="long_answer" type="hidden" value="<?= $row->long_answer?>">
        	<input name="username" type="hidden" value="<?= $row->username?>">
        	<input name="page" type="hidden" value="<?= $page ?>">
			<input name="fltUsername" type="hidden" value="<?= $fltUsername ?>">
        	<input name="btnDeleteRow"  type="submit" class="small_btn" value="delete row">			
      		</form> 
		</td>
	</tr>
<?}?>
</table>
<br>
<form action="<?=$self?>" method="POST">
    <input name="survey_id" type="hidden" value="<?= $survey_id?>">
    <input name="question_id" type="hidden" value="<?= $question_id?>">
    <input name="answer" type="hidden" value="<?= $answer?>">
    <input name="ans_other" type="hidden" value="<?= $ans_other?>">
    <input name="long_answer" type="hidden" value="<?= $long_answer?>">
    <input name="username" type="hidden" value="<?= $username?>">
	<input name="fltUsername" type="hidden" value="<?= $fltUsername ?>">
	<input name="page" type="hidden" value="<?= $page ?>">
<?if ($page>1) {?>
	<input name="btnPrevPage"  type="submit" class="small_btn" value="previous page">			
<? } 
if ($morepages) {?>
    <input name="btnNextPage"  type="submit" class="small_btn" value="  next page  ">			
<?}
mysql_free_result($rs);
?>
</form> 
</body>
</html>
