<?php require_once('../Connections/cnPall.php'); ?>
<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/app_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/form_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/RAG_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmEntry")) {
  $insertSQL = sprintf("INSERT INTO rag_entry (section_id, title, notes, active) VALUES (%s, %s, %s, %s)",
                       GetSQLValueString($_POST['section_id'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString(isset($_POST['active']) ? "true" : "", "defined","1","0"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($insertSQL, $cnPall) or die(mysql_error());

  $insertGoTo = "RAGentry.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
   $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
   $insertGoTo .= "entryid=" . mysql_insert_id();
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmEntry")) {
  $updateSQL = sprintf("UPDATE rag_entry SET section_id=%s, title=%s, notes=%s, active=%s WHERE id=%s",
                       GetSQLValueString($_POST['section_id'], "int"),
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString(isset($_POST['active']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($updateSQL, $cnPall) or die(mysql_error());

  $updateGoTo = "RAGentry.php?id=" . $_POST['id'];
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}
?>

<?php require_once('../_include/db_func.inc.php'); ?>
<?php
$colname_rsEntry = "1";
if (isset($_GET['entryid'])) {
  $colname_rsEntry = (get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsEntry = sprintf("SELECT * FROM rag_entry WHERE id = %s", $colname_rsEntry);
$rsEntry = mysql_query($query_rsEntry, $cnPall) or die(mysql_error());
$row_rsEntry = mysql_fetch_assoc($rsEntry);
$totalRows_rsEntry = mysql_num_rows($rsEntry);

$colname_rsDocs = "1";
if (isset($_GET['entryid'])) {
  $colname_rsDocs = (get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsDocs = sprintf("SELECT id, title, file_location, organisation, active FROM rag_doc WHERE entry_id = %s AND audit=0", $colname_rsDocs);
$rsDocs = mysql_query($query_rsDocs, $cnPall) or die(mysql_error());
$row_rsDocs = mysql_fetch_assoc($rsDocs);
$totalRows_rsDocs = mysql_num_rows($rsDocs);

$colname_rsSurveys = "1";
if (isset($_GET['entryid'])) {
  $colname_rsSurveys = (get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsSurveys = sprintf("SELECT id, survey_id, monograph FROM rag_survey WHERE rag_entry_id = %s", $colname_rsSurveys);
$rsSurveys = mysql_query($query_rsSurveys, $cnPall) or die(mysql_error());
$row_rsSurveys = mysql_fetch_assoc($rsSurveys);
$totalRows_rsSurveys = mysql_num_rows($rsSurveys);

$colname_rsAuditDocs = "1";
if (isset($_GET['entryid'])) {
  $colname_rsAuditDocs = (get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsAuditDocs = sprintf("SELECT id, title, file_location, organisation, active FROM rag_doc WHERE entry_id = '%s' AND audit=1", $colname_rsAuditDocs);
$rsAuditDocs = mysql_query($query_rsAuditDocs, $cnPall) or die(mysql_error());
$row_rsAuditDocs = mysql_fetch_assoc($rsAuditDocs);
$totalRows_rsAuditDocs = mysql_num_rows($rsAuditDocs);
?>
<html>
<head>
<title>rag:contents</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0>
<tr>
    <td >
		<a href="index.php"><font size=-2>admin</font></a>
		/ <a href="RAGadmin.php"><font size=-2>RAG admin</font></a>
		/ <a href="RAGsection.php?secid=<?php echo $_GET['secid']; ?>"><font size=-2>RAG topic</font></a>
	</td>
  </tr>
</table>
<!-- end quick-nav bar -->
<p>Rag Content</p>
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmEntry" id="frmEntry">
  <table border="0" align="center" cellpadding="2" cellspacing="1">
    <tr> 
      <th>title</th>
      <td colspan="2"><input name="title" type="text" id="title" value="<?php echo $row_rsEntry['title']; ?>" size="80" maxlength="255"></td>
    </tr>
    <tr> 
      <th>notes</th>
      <td colspan="2"><textarea name="notes" cols="70" rows="6" id="notes"><?php echo $row_rsEntry['notes']; ?></textarea></td>
    </tr>
    <tr> 
      <th>active</th>
      <td><input name="active" type="checkbox" id="active" value="checkbox" <?php echo ($row_rsEntry['active'] ==1) ?  " checked" : "" ?>> 
        <input name="id" type="hidden" id="id" value="<?php echo $_GET['entryid']; ?>"> 
        <input name="section_id" type="hidden" id="section_id" value="<?php echo $_GET['secid']; ?>"></td>
      <td align="right">
        <?php if ($_GET['entryid']==0) { ?>
        <input name="btnSave" type="submit" id="btnSave" value="save">
        <?php } // show if var=val ?>
		 <?php if ($_GET['entryid']!=0) { ?>
        <input name="btnUpdate" type="submit" id="btnUpdate" value="update">
		 <?php } // show if var=val ?>
		</td>
    </tr>
  </table>
  <?php if (0!=$_GET['entryid']) { ?>
  <input type="hidden" name="MM_update" value="frmEntry">
  <?php } // show if var=val ?>
   <?php if (0==$_GET['entryid']) { ?>
  <input type="hidden" name="MM_insert" value="frmEntry">
   <?php } // show if var=val ?>
</form>
<p>&nbsp;</p>
<?php if (0!=$_GET['entryid']) { ?>
<p>Documents (<?php echo $totalRows_rsDocs ?> ) <a href="RAGdoc.php?docid=0&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid'];?>">add</a></p>
<table width="85%" border="1" align="center" cellpadding="1" cellspacing="0">
  <tr> 
    <th>title</th>
    <th>organisation</th>
    <th>active</th>
    <th>actions</th>
  </tr>
 <?php if ($totalRows_rsDocs > 0) { // Show if recordset not empty ?>
 <?php do { ?>
  <tr> 
    <td><?php echo $row_rsDocs['title']; ?></td>
    <td><?php echo $row_rsDocs['organisation']; ?></td>
    <td align="center"><?php echo $row_rsDocs['active']; ?></td>
    <td align="center"><a href="RAGdoc.php?docid=<?php echo $row_rsDocs['id']; ?>&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid']; ?>">edit</a> 
      | <a href="RAGdoc.php?del=1&docid=<?php echo $row_rsDocs['id']; ?>&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid']; ?>">delete</a></td>
  </tr>
   <?php } while ($row_rsDocs = mysql_fetch_assoc($rsDocs)); ?>
	<?php } // Show if recordset not empty ?>
 </table>
<p>Audits (<?php echo $totalRows_rsAuditDocs ?>
) <a href="RAGdoc.php?docid=0&audit=1&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid'];?>">add</a></p>
<table width="85%" border="1" align="center" cellpadding="1" cellspacing="0">
  <tr>
    <th>title</th>
    <th>organisation</th>
    <th>active</th>
    <th>actions</th>
  </tr>
  <?php do { ?>
  <tr>
    <td><?php echo $row_rsAuditDocs['title']; ?></td>
    <td><?php echo $row_rsAuditDocs['organisation']; ?></td>
    <td><?php echo $row_rsAuditDocs['active']; ?></td>
    <td align="center"><a href="RAGdoc.php?audit=1&docid=<?php echo $row_rsAuditDocs['id']; ?>&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid']; ?>">edit</a> | <a href="RAGdoc.php?del=1&docid=<?php echo $row_rsAuditDocs['id']; ?>&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid']; ?>">delete</a></td>
  </tr>
  <?php } while ($row_rsAuditDocs = mysql_fetch_assoc($rsAuditDocs)); ?>
  
</table>
<p>&nbsp;</p>
<p>Surveys (<?php echo $totalRows_rsSurveys ?> ) <a href="RAGsurvey.php?survid=0&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid']; ?>">add</a></p>
<table width="85%" border="1" align="center" cellpadding="1" cellspacing="0">
  <tr> 
    <th>title</th>
    <th>monograph</th>
    <th>responses</th>
    <th>actions</th>
  </tr>
  <?php if ($totalRows_rsSurveys > 0) { // Show if recordset is not empty ?>
   <?php do { ?>
  <tr> 
    <td><?php echo dLookUp("title","survsurvey","id=".$row_rsSurveys['survey_id']); ?></td>
    <td><?php echo $row_rsSurveys['monograph']; ?>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">
		<a href="RAGsurvey.php?survid=<?php echo $row_rsSurveys['id'];?>&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid']; ?>">edit</a>
	|  <a href="RAGsurvey.php?del=1&survid=<?php echo $row_rsSurveys['id'];?>&entryid=<?php echo $_GET['entryid']; ?>&secid=<?php echo $_GET['secid']; ?>">delete</a>
	</td>
  </tr>
    <?php } while ($row_rsSurveys = mysql_fetch_assoc($rsSurveys)); ?>
  <?php } // Show if recordset empty ?>
</table>
<?php } // show if var=val ?>
<p>&nbsp;</p></body>
</html>
<?php
mysql_free_result($rsEntry);

mysql_free_result($rsDocs);

mysql_free_result($rsSurveys);

mysql_free_result($rsAuditDocs);
?>

