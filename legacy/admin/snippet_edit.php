<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/form_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
// do normal authentication
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<?
if ($_SERVER['REQUEST_METHOD']=='POST') {
	$snippet = $_POST['snippet'];
	$snippet_text = StripSlashes($_POST['snippet_text']);
	$fp = fopen($_SERVER['DOCUMENT_ROOT']."/legacy/content/snippets/".$snippet,"w");
	fputs($fp,$snippet_text);
	fclose($fp);
	header("Location:  snippets.php");
	exit();
}
$snippet = $_SERVER['QUERY_STRING'];
$snippet_text = implode("",file($_SERVER['DOCUMENT_ROOT']."/legacy/content/snippets/".$snippet));
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>palliativedrugs.com: admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<strong>edit snippet</strong> - <?=$snippet?><br>
<br>
<form action="snippet_edit.php" method="POST">
<input type="hidden" name="snippet" value="<?=$snippet?>">
<textarea name="snippet_text" cols="60" rows="15"><?=$snippet_text?></textarea>
<br><input type="submit" value="save"> <input type="button" value="cancel" onClick="Javascript:history.go(-1);">
</form>
</body>
</html>
