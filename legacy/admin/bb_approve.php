<?php
// includes & requires -------------------------------------
include("$DOCUMENT_ROOT/_include/config.inc.php");
include("$DOCUMENT_ROOT/_include/db_func.inc.php");
require_once("$DOCUMENT_ROOT/_include/visitor.class.php");
require_once("$DOCUMENT_ROOT/_include/UserInfo.class.php");

// security ------------------------------------------------
session_start();
$allowed="admin";
require_once("$DOCUMENT_ROOT/_include/secure.inc.php");

// this function is nicked from forumwrite.php
function insert_new_message($parent_id, $forum_id, $user_id, $subject, $body) {	
	if ($parent_id) {
		$sql = "SELECT * FROM tdmessage WHERE id=$parent_id";
		$rs = mysql_query($sql) or die("<hr>".mysql_error()."<hr>".$sql."<hr>");
		$row = mysql_fetch_array($rs);
		$level = $row["level"] + 1;
		$orderstr = bin2hex($row["orderstr"]);
		$root_id = $row["root_id"];
		if (!$root_id) $root_id = $row["id"];
	} else {
		$level = 0;
		$root_id = 0;
		$parent_id = 0;
	}
	
	if ($level <= 31) {
		$tmp = dechex(time());
		if (strlen($tmp)<8) $tmp = str_repeat(" ", 8 - strlen($tmp));
		$orderstr = "0x" . $orderstr . $tmp;
	} else {
		$level = 31;
		$orderstr = "0x" . $orderstr;	
	}
	
	$sql = "INSERT INTO tdmessage (forum_id, parent_id, root_id, user_id, subject, body, level, orderstr) " 
				. " VALUES ($forum_id, $parent_id, $root_id, $user_id, " 
				. " '" . addslashes($subject) . "',   '" . addslashes($body) . "', $level, $orderstr)";
	//echo "<hr>". $sql . "<hr>";
	//exit;
	mysql_query($sql) or die("<hr>".mysql_error()."<hr>".$sql."<hr>");
	
} // funtion insert_new_message


function delete_message($id) {
	$sql = "DELETE FROM tdunapporovedmessage WHERE id=$id";
	mysql_query($sql) or die("<hr>".mysql_error()."<hr>".$sql."<hr>");	
}

function approve_message($id) {	
	$sql = "SELECT * FROM tdunapporovedmessage WHERE id=$id";
	$rs = mysql_query($sql) or die("<hr>".mysql_error()."<hr>".$sql."<hr>");
	$row = mysql_fetch_object($rs);	
	if($row){
		//echo "P:$row->parent_id, F:$row->forum_id, U:$row->user_id, S:$row->subject, B:$row->body";	
		insert_new_message($row->parent_id, $row->forum_id, $row->user_id, $row->subject, $row->body);
		$sql = "DELETE FROM tdunapporovedmessage WHERE id=$id";
		mysql_query($sql) or die("<hr>".mysql_error()."<hr>".$sql."<hr>");	
	}
}

// - entry point -----------------------------------------------------------

if($REQUEST_METHOD=="POST") {	
	while(list($key,$var) = each($_POST))
     if(is_array($var)) {
          for($a=0;$a<count($var);$a++) {
		  	//echo $key." = ".$var[$a]."<br>\n";
		  	if($key=="delete_msg") {
				delete_message($var[$a]);
			} elseif ($key=="approve_msg") {
				approve_message($var[$a]);
			}            
		}
    }
    

	header("Location: /admin/bb_approve.php");
	exit;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>approve bb posts</title>
	<link rel="STYLESHEET" type="text/css" href="/admin/admin.css">
</head>
<body>
<p class="heading">New Topics</p>
<form action="<?=$PHP_SELF?>" method="post">
<?
$sql = "SELECT * FROM tdunapporovedmessage WHERE parent_id=0";
$rs = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_object($rs)) : ?>
	<table bgcolor="cccccc" width="90%" border="1" bordercolor="#000000" cellspacing="0">
	<tr bgcolor="#ededed">
		<td rowspan="2" width="120" valign="top"><!--user -->
		<p align="center"><i>posted by</i></p>		
		<?
		$userinfo = new objUserInfo($row->user_id);
		print htmlentities($userinfo->name) . "<br>\n";
		print htmlentities($userinfo->role) . "<br>\n";
		print htmlentities($userinfo->org) . "<br>\n";
		print htmlentities($userinfo->country) . "<br>\n";
		?><br><br>
		</td>
		<td valign="top"><i>subject:</i> <?=$row->subject?></td>
	</tr>	
	<tr>
		<td valign="top" bgcolor="#ededed"><?=$row->body?></td>
	</tr>
	<tr>
		<td colspan=2>
			<input type="checkbox" name="approve_msg[]" value="<?=$row->id?>">Approve&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="delete_msg[]" value="<?=$row->id?>">Delete
		</td>
	</tr>
	</table><br><br>
<?endwhile; ?>
<p class="heading">Replies</p>
<?
$sql = "SELECT * FROM tdunapporovedmessage WHERE parent_id!=0";
$rs = mysql_query($sql) or die(mysql_error());
while($row=mysql_fetch_object($rs)) : ?>
	<table bgcolor="cccccc" width="90%" border="1" bordercolor="#000000" cellspacing="0">
	<tr bgcolor="#ededed">
		<td rowspan="2" width="120" valign="top"><!--user -->
		<p align="center"><i>posted by</i></p>		
		<?
		$userinfo = new objUserInfo($row->user_id);
		print htmlentities($userinfo->name) . "<br>\n";
		print htmlentities($userinfo->role) . "<br>\n";
		print htmlentities($userinfo->org) . "<br>\n";
		print htmlentities($userinfo->country) . "<br>\n";
		?><br><br>
		</td>
		<td valign="top"><i>subject:</i> <?=$row->subject?></td>
	</tr>	
	<tr>
		<td valign="top" bgcolor="#ededed"><?=$row->body?></td>
	</tr>
	<tr>
		<td colspan=2>
			<input type="checkbox" name="approve_msg[]" value="<?=$row->id?>">Approve&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="delete_msg[]" value="<?=$row->id?>">Delete
		</td>
	</tr>
	</table><br><br>
<?endwhile; ?>
<input type="submit" value="submit">
</form>
</body>
</html>

