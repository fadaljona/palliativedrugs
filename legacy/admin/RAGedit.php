<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/_include/app_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/_include/form_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

if ($_SERVER['REQUEST_METHOD']=="POST") {
	$id = $_POST[id];
	$monograph = $_POST[monograph];
	$notes = $_POST[notes];
	$survey_id = $_POST[survey];
	
	if ($id !=0) {
		$sql = "DELETE FROM RAG_survey WHERE rag_id=1 AND survey_id=$id";
		mysql_query($sql) or die (mysql_error()."<hr>$sql");
	}
	
	$sql = "INSERT INTO RAG_survey VALUES (1, $survey_id, '$monograph','$notes')";
	mysql_query($sql) or die (mysql_error()."<hr>$sql");
	header("Location:  /admin/RAGadmin.php");
	
} else {
	$id = $_GET[id];
	$sql = "SELECT * FROM RAG_survey WHERE survey_id=" . $id;
	$rs = mysql_query($sql);
	if ($rs) {
		$row = mysql_fetch_object($rs);
		$monograph = $row->monograph;
		$notes = $row->notes;
		mysql_free_result($rs);
	} else {
		echo "error opening database.";
		exit;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0>
<tr><td>
<a href="index.php"><font size=-2>admin</font></a>
 / <a href="RAGadmin.php"><font size=-2>RAG admin</font></a>
</td></tr>
</table>
<!-- end quick-nav bar -->
<?
if ($id==0) {
	$title = "New RAG survey";
} else {
	$title = "Edit RAG survey";
}
?>
<h1><?=$title?></h1>
<form action="<?=$_SERVER[PHP_SELF]?>" method="POST">
<input type="hidden" name="id" value="<?=$id?>">
<table>
	<tr>
		<td>survey</td>
		<td>
			<?
			echo selbox("survey", "", "SELECT id, title FROM survsurvey", $id, "", "");
			?>
		</td>
	</tr>
	<tr>
		<td>monograph</td>
		<td>
			<?
			$mons = GetArrayOfMonographs();
			arrayToListBox("monograph",$mons,$monograph);
			?>
		</td>
	</tr>
	<tr>
		<td>notes</td>
		<td><textarea name="notes" rows="5" cols="60"><?=$notes?></textarea></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="submit" value="save">
			<input type="button" value="cancel" onClick="javascript:history.go(-1);">
		</td>
	</tr>
</table>
</form>
</body>
</html>
