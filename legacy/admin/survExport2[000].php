<?php
require_once("$DOCUMENT_ROOT/_include/config.inc.php");
require_once("$DOCUMENT_ROOT/_include/db_func.inc.php");

function one_of($qid, $ord) {
	return dLookUp("answer","survanswers","question_id=$qid and ord=$ord");
}

function many_of($qid, $ans) {
	$result="";
	$arr = explode(",",$ans);
	for ($i=0; $i < count($arr); $i++) {
		$where = "question_id=" . $qid . " AND ord=" . $arr[$i];
		$result .= dLookUp("answer","survanswers",$where) . " | ";
	}
	return $result;
}

$sid = $HTTP_GET_VARS["id"];

$sql = "select survresults.username, role, speciality, organisation, addr1, country, "
		. "survquestion.id as question_id, title_text as question, type, survresults.answer"
		. " from survresults, users, survquestion"
		. " where survresults.survey_id =" .$sid
		. " and survresults.username = users.username"
		. " and survquestion.id = survresults.question_id";

$filt = stripslashes(urldecode($HTTP_GET_VARS["filt"]));
$sql .= " and country " . $filt;

$rs = mysql_query($sql) or die(mysql_error() . "<hr>$sql<hr>");

//echo $filt."<br>";
//echo $sql."<br>";
//exit;

echo "<!-- $sql -->";

echo "user,speciality,organisation,addr1,country,question,answer<br>";

while($row=mysql_fetch_object($rs)) {
	
    $csv_row=	str_replace(","," ",$row->username) . ","
        . str_replace(","," ",$row->speciality) . ","
        . str_replace(","," ",$row->organisation) . ","
		. str_replace(","," ",$row->addr1) . ","
      	. str_replace(","," ",$row->country) . ","
        . str_replace(","," ",$row->question) . ",";
		
	if ($row->type=="one_of") {
		$csv_row .= str_replace(","," ",one_of($row->question_id, $row->answer));
	} elseif ($row->type=="many_of") {
		$csv_row .= str_replace(","," ",many_of($row->question_id, $row->answer));
	} else {
		$csv_row .= str_replace(","," ",$row->answer);
	}

    echo $csv_row . "<br>";

}
?>
