<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/legacy/_include/form_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
// 'staff' access hack (mk:25-08-2005)
$vid = $_SESSION["visitor"]->id;
if ($vid == 18660) {
		// let this user in ...
} else {
	// do normal authentication
	$allowed="admin";
	require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
}
?>
<?php

if($_SERVER['REQUEST_METHOD']=='POST') {
	$_POST			=	array_map("trim",$_POST);
	$id				=	$_POST['id'];
	$unit_code		=	$_POST['unit_code'];
	$unit_ref		=	$_POST['unit_ref'];
	$drug1_name		=	trim($_POST['drug1_name']);
	$drug1_dose		=	($_POST['drug1_dose']) ? $_POST['drug1_dose'] : 0;
	$drug2_name		=	$_POST['drug2_name'];
	$drug2_dose		=	($_POST['drug2_dose']) ? $_POST['drug2_dose'] : 0;
	$drug3_name		=	$_POST['drug3_name'];
	$drug3_dose		=	($_POST['drug3_dose']) ? $_POST['drug3_dose'] : 0;
	$drug4_name		=	$_POST['drug4_name'];
	$drug4_dose		=	($_POST['drug4_dose']) ? $_POST['drug4_dose'] : 0;
	$drug5_name		=	$_POST['drug5_name'];
	$drug5_dose		=	($_POST['drug5_dose']) ? $_POST['drug5_dose'] : 0;
	$drug6_name		=	$_POST['drug6_name'];
	$drug6_dose		=	($_POST['drug6_dose']) ? $_POST['drug6_dose'] : 0;
	$volume			=	($_POST['volume']) ? $_POST['volume'] : 0 ;
	$diluent		=	$_POST['diluent'];
	if ($diluent	==	'Please select ...') { $diluent = 'unknown'; }
		$duration	=	$_POST['duration'];
	if ($duration	==	'Please select ...') { $duration = 'unknown'; }
		$duration_other = $_POST['duration_other'];
	$compatibility	=	$_POST['compatibility'];
	if ($compatibility == 'Please select ...') { $compatibility = 'unknown'; }
		$datatype	=	$_POST['datatype'];
	if ($datatype == 'Please select ...') { $datatype = 'unknown'; }
		$site_reaction = $_POST['site_reaction'];
	if ($site_reaction == 'Please select ...') { $site_reaction = 'unknown'; }
		$comments	=	 $_POST['comments'];
	$support_data	=	$_POST['support_data'];
	
	
	$approved		    =	(isset($_POST["chkApproved"])) ? 1 : 0;	
	$show_details_link  =	(isset($_POST["chkShowDetailsLink"])) ? 1 : 0;	

	if($id==0) {
		$sql = "INSERT INTO sddb_temp (drug1_name) VALUES ('')";
		mysql_query($sql) or die(mysql_error());
		$id = mysql_insert_id();
		$added_date = GetDbTimestamp();
		$added_by = $_SESSION["visitor"]->id;
	}
	
	$sql = "UPDATE sddb_temp SET "
			. "unit_code	=	'".$unit_code."',"
			. "unit_ref		=	'".$unit_ref."', "	
			. "drug1_name	=	'".$drug1_name."', "
			. "drug1_dose	=	'".$drug1_dose."', "
			. "drug2_name	=	'".$drug2_name."', "
			. "drug2_dose	=	'".$drug2_dose."', ";
	if(!empty($drug3_name)) {
		$sql .= "drug3_name	=	'".$drug3_name."', "
			. "drug3_dose	=	'".$drug3_dose."', ";
	} else {
		$sql .= "drug3_name	=	'0', "
			. "drug3_dose	=	0, ";
	}
	if(!empty($drug4_name)) {
		$sql .= "drug4_name	=	'".$drug4_name."', "
			. "drug4_dose	=	'".$drug4_dose."', ";
	} else {
		$sql .= "drug4_name	=	'0', "
			. "drug4_dose	=	0, ";
	}
	if(!empty($drug5_name)) {
		$sql .= "drug5_name	=	'".$drug5_name."', "
			. "drug5_dose	=	'".$drug5_dose."', ";
	} else {
		$sql .= "drug5_name	=	'0', "
			. "drug5_dose	=	0, ";
	}
	if(!empty($drug6_name)) {
		$sql .= "drug6_name	=	'".$drug6_name."', "
			. "drug6_dose	=	'".$drug6_dose."', ";
	} else {
		$sql .= "drug6_name	=	'0', "
			. "drug6_dose	=	0, ";
	}
	$sql .= "volume			=	'".$volume."', "
			. "diluent		=	'".$diluent."', "
			. "duration		=	'".$duration."', "
			. "duration_other=	'".$duration_other."', "
			. "compatibility=	'".$compatibility."', "
			. "datatype		=	'".$datatype."', "
			. "site_reaction=	'".$site_reaction."', "			
			. "comments		=	'".$comments."', "
			. "support_data	=	'".$support_data."', "
			. "show_details_link=	'".$show_details_link."', ";

	if(IsSet($added_by)) {
		$sql .= "added_by	=	'".$added_by."', ";
	}
			
	if(IsSet($added_date)) {
		$sql .= "added_date	=	'".$added_date."', ";
	}
		$sql .= "approved	=	'".$approved."' ". "WHERE id='".$id."'";

	mysql_query($sql) or die(mysql_error()."<hr>$sql");

	if($approved=='1'){

		$movesql	=	"INSERT INTO sddb (unit_code,unit_ref,drug1_name,drug1_dose,drug2_name,drug2_dose,drug3_name,drug3_dose,drug4_name,drug4_dose,drug5_name,drug5_dose,drug6_name,drug6_dose,volume,diluent,duration,duration_other,compatibility,datatype,site_reaction,comments,show_details_link,support_data,added_by,added_date,approved)SELECT unit_code,unit_ref,drug1_name,drug1_dose,drug2_name,drug2_dose,drug3_name,drug3_dose,drug4_name,drug4_dose,drug5_name,drug5_dose,drug6_name,drug6_dose,volume,diluent,duration,duration_other,compatibility,datatype,site_reaction,comments,show_details_link,support_data,added_by,added_date,approved FROM sddb_temp WHERE id='".$id."'";
		if(mysql_query($movesql)){
			mysql_query("DELETE FROM sddb_temp WHERE id='".$id."'")or die(mysql_error());
		}
	}

	header("Location:  sddb.php");
	exit();

} else {
	// it's a GET request..
	$id		= $_GET['id'];
	if(IsSet($_GET['moveup'])) {
		$n = $_GET['moveup'];
		$m = $n -1;
		// select row..
		$sql = "SELECT drug".$n."_name, drug".$n."_dose, drug".$m."_name, drug".$m."_dose from sddb_temp WHERE id=$id";
		$rs = mysql_query($sql) or die(mysql_error()."<hr>$sql");
		$row = mysql_fetch_object($rs);
		
		$tmp = "drug".$n."_name";
		$upName = $row->$tmp;
		$tmp = "drug".$n."_dose";
		$upDose = $row->$tmp;
		
		$tmp = "drug".$m."_name";		
		$dnName = $row->$tmp;
		$tmp = "drug".$m."_dose";
		$dnDose = $row->$tmp;
		
		mysql_free_result($rs);
		
		$sql = "UPDATE sddb_temp SET " 
			. "drug".$n."_name = '".$dnName."', "
			. "drug".$n."_dose = '".$dnDose."', "
			. "drug".$m."_name = '".$upName."', "
			. "drug".$m."_dose = '".$upDose."' "
			. "WHERE id=$id";
		mysql_query($sql) or die(mysql_error()."<hr>$sql");
	}
	
	if(IsSet($_GET['ac'])) {
		$ac = $_GET['ac'];
		if($ac=='del') {
			$sql = "DELETE FROM sddb_temp WHERE id=" . $id;
			mysql_query($sql) or die(mysql_error()."<hr>$sql");	
			//header("Location:  sddb.php");
			//exit();
		}
	}
	
	
		if($id==0) {
			//it's a new one...
			$page_title = "SD-Db - new entry";
			$unit_code = ''; $unit_ref='';
			$drug1_name = "";	$drug1_dose =  "";	
			$drug2_name = "";	$drug2_dose =  "";	
			$drug3_name = "";	$drug3_dose =  "";	
			$drug4_name = "";	$drug4_dose =  "";	
			$drug5_name = "";	$drug5_dose =  "";	
			$drug6_name = "";	$drug6_dose =  "";	
			$volume		= "";
			$diluent	= "";
			$duration	= "";
			$duration_other = "";
			$compatibility = "";
			$datatype	= "";
			$site_reaction = "";
			$comments	= "";
			$support_data = "";
			$added_by	= "";
			$added_date = "";
			$show_details_link = 0;
			$approved	= FALSE;		
		} else {
			$page_title = "SD-Db - edit entry";
			$sql = "SELECT * FROM sddb_temp WHERE id=" . $id;
			$rs = mysql_query($sql) or die(mysql_error()."<hr>$sql");
			$row = mysql_fetch_object($rs);
			$unit_code	= $row->unit_code;
			$unit_ref	= $row->unit_ref;
			$drug1_name = $row->drug1_name;		$drug1_dose = $row->drug1_dose;		
			$drug2_name = $row->drug2_name;		$drug2_dose = $row->drug2_dose;		
			$drug3_name = $row->drug3_name;		$drug3_dose = $row->drug3_dose;		
			$drug4_name = $row->drug4_name;		$drug4_dose = $row->drug4_dose;		
			$drug5_name = $row->drug5_name;		$drug5_dose = $row->drug5_dose;		
			$drug6_name = $row->drug6_name;		$drug6_dose = $row->drug6_dose;				
			$volume		= $row->volume;
			$diluent	= $row->diluent;
			$duration	= $row->duration;
			$duration_other = $row->duration_other;
			$compatibility = $row->compatibility;
			$datatype	= $row->datatype;
			$site_reaction = $row->site_reaction;
			$comments	= $row->comments;
			$support_data = $row->support_data;
			$show_details_link = $row->show_details_link;
			$added_by	= $row->added_by;
			$added_date = $row->added_date;
			$approved	= $row->approved;		
			mysql_free_result($rs);
			$body = "onLoad=\"Javascript:update_concentrations();\"";		
		}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
	<script language="javascript">
	function update_concentrations() {
		vol = document.sddb_form.volume.value
		if(document.sddb_form.drug1_name.value != '') {
			dose = document.sddb_form.drug1_dose.value;
			conc = dose / vol;
			document.sddb_form.drug1_concentration.value = conc;
		}
		if(document.sddb_form.drug2_name.value != '') {
			dose = document.sddb_form.drug2_dose.value;
			conc = dose / vol;
			document.sddb_form.drug2_concentration.value = conc;
		}		
		if(document.sddb_form.drug3_name.value != '') {
			dose = document.sddb_form.drug3_dose.value;
			conc = dose / vol;
			document.sddb_form.drug3_concentration.value = conc;
		}
		if(document.sddb_form.drug4_name.value != '') {
			dose = document.sddb_form.drug4_dose.value;
			conc = dose / vol;
			document.sddb_form.drug4_concentration.value = conc;
		}
		if(document.sddb_form.drug5_name.value != '') {
			dose = document.sddb_form.drug5_dose.value;
			conc = dose / vol;
			document.sddb_form.drug5_concentration.value = conc;
		}
		if(document.sddb_form.drug6_name.value != '') {
			dose = document.sddb_form.drug6_dose.value;
			conc = dose / vol;
			document.sddb_form.drug6_concentration.value = conc;
		}
	}
	
	function DeleteEntry(id)	{
	   if (confirm("Are you sure you want to delete this entry ?")) {
			location.href="sddb_edit-survey.php?ac=del&id=" + id;
			return true;	
	   }
	}
	</script>
</head>
<body <?=$body?>>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size=-2>admin</font></a> / 
<a href="sddb.php"><font size=-2>SDdb</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<br><strong><?=$page_title?></strong><br>
<form name="sddb_form" action="sddb_edit-survey.php" method="POST">
<input type="hidden" name="id" value="<?=$id?>">
<table width="90%" border="0" align="center">
<tr>
	<td colspan="5" align="right">
		Center / unit code <input name="unit_code" type="text" id="unit_code" size="20" value="<?=$unit_code;?>">
		&nbsp;&nbsp;Record number <input name="unit_ref" type="text" id="unit_ref" size="10" value="<?=$unit_ref;?>">
	</td>
</tr>
  <tr>
    <td colspan="5">&nbsp;</th>
  </tr>
 <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
    	<th>Drug name </th>
    	<th>Dose (mg)</th>
      	<th>Concentration</th>
  </tr>
     <tr>
        <th>Drug 1</th>
        <td>&nbsp;</td>
        <td><?	
			$a[] = "";
			$drugs = array_merge($a,file($_SERVER["DOCUMENT_ROOT"]."/legacy/content/snippets/sddb_drug_list.txt"));
			arrayToListBox("drug1_name",$drugs,$drug1_name, $extra="");
		?>	
	 </td>
        <td><input name="drug1_dose" type="text" id="drug1_dose" size="15" value="<?=$drug1_dose;?>"></td>
        <td><input disabled name="drug1_concentration" type="text" id="drug1_concentration" style="background-color:#CCCCCC;"></td>
    </tr>
      <tr>
        <th>Drug 2 </th>
        <td>
        <?  	if ($id!=0 && ($drug2_name)) { ?>
        	<a href="sddb_edit-survey.php?moveup=2&id=<?=$id?>"><img border="0" src="/images/btnUp.gif"></a>
        <? } ?>
        </td>
        <td><? arrayToListBox("drug2_name",$drugs,$drug2_name, $extra=""); ?></td>
        <td><input name="drug2_dose" type="text" id="drug2_dose" size="15" value="<?=$drug2_dose;?>"></td>
        <td><input disabled name="drug2_concentration" type="text" id="drug2_concentration" style="background-color:#CCCCCC;"></td>
      </tr>
      <tr>
        <th>Drug 3 </th>
        <td>
        <?  	if ($id!=0 && ($drug3_name)) { ?>
        	<a href="sddb_edit-survey.php?moveup=3&id=<?=$id?>"><img border="0" src="/images/btnUp.gif"></a>
        <? } ?>
        </td>
        <td><? arrayToListBox("drug3_name",$drugs,$drug3_name, $extra=""); ?></td>
        <td><input name="drug3_dose" type="text" id="drug3_dose" size="15" value="<?=$drug3_dose;?>"></td>
        <td><input disabled name="drug3_concentration" type="text" id="drug3_concentration" style="background-color:#CCCCCC;"></td>
      </tr>
      <tr>
        <th>Drug 4 </th>
        <td>
        <?  	if ($id!=0 && ($drug4_name)) { ?>
        	<a href="sddb_edit-survey.php?moveup=4&id=<?=$id?>"><img border="0" src="/images/btnUp.gif"></a>
        <? } ?>
        </td>
        <td><? arrayToListBox("drug4_name",$drugs,$drug4_name, $extra=""); ?></td>
        <td><input name="drug4_dose" type="text" id="drug4_dose" size="15" value="<?=$drug4_dose;?>"></td>
        <td><input disabled name="drug4_concentration" type="text" id="drug4_concentration" style="background-color:#CCCCCC;"></td>
      </tr>
      <tr>
        <th>Drug 5 </th>
        <td>
        <?  	if ($id!=0 && ($drug5_name)) { ?>
        	<a href="sddb_edit-survey.php?moveup=5&id=<?=$id?>"><img border="0" src="/images/btnUp.gif"></a>
        <? } ?>
        </td>
        <td><? arrayToListBox("drug5_name",$drugs,$drug5_name, $extra=""); ?></td>
        <td><input name="drug5_dose" type="text" id="drug5_dose" size="15" value="<?=$drug5_dose;?>"></td>
        <td><input disabled name="drug5_concentration" type="text" id="drug5_concentration" style="background-color:#CCCCCC;"></td>
      </tr>
      <tr>
        <th>Drug 6 </th>
        <td>
        <?  	if ($id!=0 && ($drug6_name)) { ?>
        	<a href="sddb_edit-survey.php?moveup=6&id=<?=$id?>"><img border="0" src="/images/btnUp.gif"></a>
        <? } ?>
        </td>
        <td><? arrayToListBox("drug6_name",$drugs,$drug6_name, $extra=""); ?></td>
        <td><input name="drug6_dose" type="text" id="drug6_dose" size="15" value="<?=$drug6_dose;?>"></td>
        <td><input disabled name="drug6_concentration" type="text" id="drug6_concentration" style="background-color:#CCCCCC;"></td>
      </tr>   
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
    <tr>
    	<td colspan="3">Diluent
		<?
		$options = file($_SERVER["DOCUMENT_ROOT"]."/legacy/content/snippets/sddb_diluent_dropdown.txt");
		array_unshift($options,"Please select ...");
		$default = (IsSet($diluent)) ? $diluent : "Please select ...";
		echo "<!-- MK default=$default -->";
		
		if($default=='Saline 0.9') {
			$default ='Saline 0.9%';
		} elseif ($default=='Dextrose 5') {
			$default ='Dextrose 5%';
		} 
		
		arrayToListBox("diluent",$options,$default);
		?>	
		&nbsp;&nbsp;&nbsp;made up to final volume of 
      <input name="volume" type="text" id="volume3" size="4" value="<?=$volume;?>">	ml  </td>
      <td colspan="2" align="right"><a href="#" onClick="javascript:update_concentrations();">update concentrations</a></td>
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr>
  	<td colspan="5"><b>Actual</b> duration of infusion
			<?
		$options = file($_SERVER["DOCUMENT_ROOT"]."/legacy/content/snippets/sddb_time_dropdown.txt");
		array_unshift($options,"Please select ...");
		$default = (IsSet($duration)) ? $duration : "Please select ...";
		arrayToListBox("duration",$options,$default);
		?>
		&nbsp;&nbsp;(if 'other' please specify <input type="text" name="duration_other" size="6" value="<?=$duration_other?>">)
	</td>
  </tr>
   
   
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    </tr>
  <tr>
    <th colspan="5">Outcomes</th>
  </tr>
  <tr>
  	<td>Compatibilty outcome</td>
    <td colspan="5">
		<?
		$options = file($_SERVER["DOCUMENT_ROOT"]."/legacy/content/snippets/sddb_admin_compatibility_dropdown.txt");
		array_unshift($options,"Please select ...");
		$default = (IsSet($compatibility)) ? $compatibility : "Please select ...";
		arrayToListBox("compatibility",$options,$default);
		?>		
		if <b>incompatible</b> please state details in Comments box below.
	  </td>
  </tr>
  <tr>
  	<td>Data type</td>
	<td colspan="4">
		<?
		$options = file($_SERVER["DOCUMENT_ROOT"]."/legacy/content/snippets/sddb_admin_datatype_dropdown.txt");
		array_unshift($options,"Please select ...");
		$default = (IsSet($datatype)) ? $datatype : "Please select ...";
		arrayToListBox("datatype",$options,$default);
		?>
	</td>
  </tr>
  <tr>
  	<td>Infusion site reaction</td>
    <td colspan="4"><?
		$options = file($_SERVER["DOCUMENT_ROOT"]."/legacy/content/snippets/sddb_site_reaction_dropdown.txt");
		array_unshift($options,"Please select ...");
		$default = (IsSet($site_reaction)) ? $site_reaction : "Please select ...";
		arrayToListBox("site_reaction",$options,$default);
		?>
		if <b>yes</b> please state details in Comments box below.
		</td>
  </tr>
  <tr>
    <td>Comments</td>
    <td colspan="4"><textarea name="comments" cols="60" rows="6"><? echo $comments; ?></textarea></td>
  </tr>
  <tr>
    <td>References</td>
	<td colspan="4"><textarea name="support_data" cols="60" rows="6"><? echo $support_data; ?></textarea></td>
  </tr>
  <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">added by: 
    	<? 
    		if($id==0) { 
    			$usr = new objUserInfo($_SESSION['visitor']->id);
    			//$userinfo = new objUserInfo($_SESSION['visitor']->id);
    			echo $usr->username;
    	 	} else { 
    	 		$usr = new objUserInfo($added_by);
    	 		?>
    	 		
    	 		<?
				if($added_by > 32464) {
					$lnk = "<a href=\"/code/phpMyEdit/new_users2.php?PME_sys_fl=0&PME_sys_fm=0&PME_sys_sfn[0]=0&PME_sys_operation=PME_op_View&PME_sys_rec=".$added_by."\">" . $usr->username ."</a><br />";
				} else {
					$lnk = "<a href=\"edit_user.php?uid=".$added_by."\">".$usr->username."</a>";
				}
				echo $lnk;
    	 		?>
    	<? 	} ?>
    </td>
    <td colspan="2" align="center">
    	show 'details' link <input name="chkShowDetailsLink" type="checkbox" <? if($show_details_link==1) echo "checked";?>>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	approved <input name="chkApproved" type="checkbox" id="chkApproved" <? if($approved==1) echo "checked";?>>
    </td>
    <td colspan="2" align="left">
    	<input name="submit" type="submit" value="save">
       <input name="button" type="button" onClick="Javascript:history.go(-1);" value="cancel">
       <input type="button" value="delete" onClick="Javascript:DeleteEntry(<?=$id?>);">
     </td>
    </tr>
</table>
</form>
</body>
</html>
