<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
	<script language="JavaScript">
	function DeleteSurvey(id, title)
	{		
	   if (confirm("DELETE: " + title + " ?")) {	   		
			location.href="/legacy/admin/survAdminActions.php?ac=del&sid=" + id;
			return true;	
	   }
	}
	</script>	
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0>
<tr><td><a href="index.php"><font size=-2>admin</font></a></td></tr>
</table>
<!-- end quick-nav bar -->
<br>
<br><font size="+1"><b>Survey Admin</b></font><br><br>
<a href='survEditSurvey.php?sid=0'>create new survey</a> | <a href='survDuplicate.php'>duplicate existing</a><br><br>

<table border="1" cellspacing="0" cellpadding="4" align="center">
<tr bgcolor="#c0c0c0">
	<td><b>survey</b></td>
	<td><b>active</b></td>
	<td><b>default</b></td>
	<td><b>date published</b></td>
	<td><b>closing date</b></td>
	<td colspan="7" align="center"><b><i>actions</i></b></td>
</tr>
<?
$sql = "SELECT id, title, date_published, closing_date, is_default, active FROM survsurvey ORDER BY id DESC";
$rs = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
while($row=mysql_fetch_object($rs)) {
	if ($row->active == TRUE) {
		if ($row->is_default == TRUE) {
			echo "<tr bgcolor=\"#99ff99\">";
		} else {
			echo "<tr bgcolor=\"#ccffcc\">";
		}
	} else {
		echo "<tr>";
	}
	echo "<td>$row->title</td>";
	echo "<td align=\"center\">";
	if ($row->active == TRUE) {
		echo "<img src=\"/images/check.gif\">";
	} else {
		echo "&nbsp";
	}
	echo "</td>";
	echo "<td align=\"center\">";
	if ($row->is_default == TRUE) {
		echo "<img src=\"/images/check.gif\">";
	} else {
		echo "&nbsp;";
	}
	echo "</td>";
	echo "<td>";
	if (IsSet($row->date_published)) {
		echo FormatDbDateTime($row->date_published);
	} else {
		echo "not published";
	}
	echo "</td>";
	echo "<td>";
	if (IsSet($row->closing_date)) {
		echo "<nobr>".FormatDbDateTime($row->closing_date)."</nobr>";
	} else {
		echo "none set";
	}
	echo "</td>";
	echo "<td><a href=\"survEditSurvey.php?sid=$row->id\">edit</a></td>";
	echo "<td><a href=\"/legacy/survey/survey.php?sid=$row->id\" target=\"_new\">view</a></td>";
	echo "<td><a href=\"survResults.php?sid=$row->id\">results</a><br><a href=\"survResults2.php?sid=$row->id\">results(v2)</a></td>";
	echo "<td>";
	if ($row->active == FALSE) {
		echo "<a href=\"survAdminActions.php?ac=activate&&sid=$row->id\">activate</a>";
	} else {
		echo "<a href=\"survAdminActions.php?ac=close&&sid=$row->id\">close</a>";
	}
	echo "</td>";
	echo "<td>";
	if ($row->is_default == TRUE) {
		echo "&nbsp;";
	} else {
		echo "<a href=\"survAdminActions.php?ac=makedef&&sid=$row->id\">make default</a>";
	}
	echo "</td>";
	echo "<td><a href=survWinner.php?sid=".$row->id."><img src=\"/images/winner.gif\" border=\"0\" alt=\"pick-a-winner\"></a></td>";
	echo "<td><a href=\"Javascript:DeleteSurvey($row->id,'";
	echo str_replace("'","\\'",$row->title);
	echo "')\"><img src=\"/images/trash.gif\" border=\"0\" alt=\"delete\"></a></td>";
	echo "</tr>";
}
?>
</table>
<br><br>
</body>
</html>
