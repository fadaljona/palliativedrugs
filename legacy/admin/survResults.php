<?php
set_time_limit(120);
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

If(IsSet($_GET['sid'])) {
	$sid = $_GET['sid'];
} elseif (IsSet($_POST['sid'])) {
	$sid = $_POST['sid'];
} else {
	die('missing sid parameter.');
}

if($_SERVER['REQUEST_METHOD']=='POST') {
	$country = $_POST["filter"];
} else {
	$country = "like '%'";
} 

if ((IsSet($_GET['fq'])) AND (IsSet($_GET['fa']))) {
	$fq = $_GET['fq'];
	$fa = $_GET['fa'];
	$drilldown = TRUE;
}

if($drilldown) die ($fq . ', ' . $fa);


//results output functions (by qtype)
function survresManyOf($qid, $total_votes, $country) {
	$sql="select answer from survanswers where question_id=$qid order by ord";
	$rsans=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$num_answers = mysql_num_rows($rsans);		
	$answer_array=array();
	for($i=1;$i<=$num_answers;$i++) {
		$answer_array[$i]=0;
	}
	$sql= "select answer from survresults, users where question_id=$qid and answer <> '0'"
		. " and users.username = survresults.username and users.country " . StripSlashes($country);
	$rsans2=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	while($rowans=mysql_fetch_object($rsans2)) {
		$tmp = explode(",",$rowans->answer);			
		for($j=0; $j<count($tmp); $j++) {
			$answer_array[$tmp[$j]] = $answer_array[$tmp[$j]] +1;
		}
	}
	echo "<table width=\"100%\" cellpadding=\"1\" cellspacing=\"0\" bordercolor=\"#c0c0c0\" border=\"1\">\n";
	echo "<tr><td><i>answer</i></td><td><i>votes</i></td><td><i>% of vote</i></td><td>&nbsp;</td></tr>\n";
	for ($i=1; $i<=$num_answers; $i++) {
		$sql="select answer from survanswers where question_id=$qid and ord=$i";
		$rstmp = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		$arow = mysql_fetch_object($rstmp);
		
		$pcnt = ($answer_array[$i] / $total_votes) * 100;
		$pcnt = round($pcnt);
		$bar_width = $pcnt*5;
		$ans = str_replace("(please e-mail us <a href=\"mailto:druginformation@palliativedrugs.com\">druginformation@palliativedrugs.com</a>)","",$arow->answer);
		echo "<tr>\n";
		echo "<td>$ans</td>\n";
		echo "<td align=\"center\">$answer_array[$i]</td>\n";
		echo "<td align=\"center\">$pcnt</td>\n";
		echo "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>\n";
		echo "</tr>\n";
	}
	echo "</table>\n";
}

function survresFreeText($qid,$country) {
	//is it 'sectioned' ?
	echo "<table width=100% border=1 cellspacing=0 cellpadding=2><tr>\n";
	$main_text = DLookUp("main_text","survquestion","id=" . $qid);
	if (!strpos($main_text,"|")==FALSE) {
		$num_secs = substr_count($main_text,'|');
		$num_cols = round(60 / $num_secs);
		$secs = explode("|",$main_text);
		for($i = 1; $i <= $num_secs; $i++) {
			echo "<td><b>".StripSlashes($secs[$i])."</b></td>\n";
		}				
		echo "</tr>\n";
		
	}

	$sql = "SELECT answer, long_answer FROM survresults, users WHERE question_id=$qid"
		. " and users.username = survresults.username and users.country " . StripSlashes($country);
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	while($row=mysql_fetch_object($rs)) {
	
		if (strlen(trim($row->answer)) > 0) {
			$this_ans = trim($row->answer);
			//echo trim($row->answer)."<br>";
		} elseif (strlen(trim($row->long_answer)) > 0) {
			$this_ans = trim($row->answer);
			//echo trim($row->answer)."<br>";
		}
	
		if (!strpos($main_text,"|")==FALSE) {
			echo "<tr>\n";
			$secs = explode("|",$this_ans);
			for($i = 0; $i <= $num_secs-1; $i++) {
				echo "<td>".StripSlashes($secs[$i])."</td>\n";
			}
			echo "</tr>\n";
		
		} else {
			if (StripSlashes($this_ans)=="NONE") {
			
			} else {
				echo "<tr><td>". StripSlashes($this_ans) . "</td></tr>\n";
			}
		}
		echo "";
	}
	echo "</table>\n";
}

function survresOneOf($qtype, $qid, $total_votes, $country) {
	echo "<table width=\"100%\" cellpadding=\"1\" cellspacing=\"0\" bordercolor=\"#c0c0c0\" border=\"1\">\n";
	echo "<tr><td><i>answer</i></td><td><i>votes</i></td><td><i>% of vote</i></td><td>&nbsp;</td></tr>\n";
	$sql = "select answer, ord from survanswers where question_id=$qid order by ord";
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	echo "<!--XXQtype=$qtype-->\n";
	while($row=mysql_fetch_object($rs)) {
		if($qtype=="yes_no") {
			$sql_votes="select count(*) as num_votes from survresults, users where question_id=$qid and answer='$row->answer'"
					. " and users.username = survresults.username and users.country " . StripSlashes($country);
			echo "<!--YESNO-->";
		} else {
			$sql_votes="select count(*) as num_votes from survresults, users where question_id=$qid and answer='$row->ord'"
					. " and users.username = survresults.username and users.country " . StripSlashes($country);
		}
		echo "<!--qType=$qtype, SQL=$sql-->\n";
			
		$rs_votes=mysql_query($sql_votes) or die(mysql_error()."<hr>".$sql_votes);
		$row_votes = mysql_fetch_object($rs_votes);
		$pcnt = ($row_votes->num_votes / $total_votes) * 100;
		$pcnt = round($pcnt);
		$bar_width=$pcnt*5;
		$ans = str_replace("(please e-mail us <a href=\"mailto:druginformation@palliativedrugs.com\">druginformation@palliativedrugs.com</a>)","",$row->answer);
		$ans = str_replace(", please <a href=mailto:hq@palliativedrugs.com>e-mail</a> us with further details","",$ans);
		echo "<tr>\n";
		echo "<td>$ans</td>\n";
		echo "<td align=\"center\">$row_votes->num_votes</td>\n";
		echo "<td align=\"center\">$pcnt%</td>\n";
		echo "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>\n";
		echo "</tr>\n";
	}
	echo "</table>\n";
}

function survresYesNo($qid, $total_votes, $country) {
	echo "<table width=\"100%\" cellpadding=\"1\" cellspacing=\"0\" bordercolor=\"#c0c0c0\" border=\"1\">\n";
	echo "<tr><td><i>answer</i></td><td><i>votes</i></td><td><i>% of vote</i></td><td>&nbsp;</td></tr>\n";

	$sql_votes="select count(*) as num_votes from survresults, users where question_id=$qid and answer='Yes'"
					. " and users.username = survresults.username and users.country " . StripSlashes($country);
	$rs_votes=mysql_query($sql_votes) or die(mysql_error()."<hr>".$sql_votes);
	$row_votes = mysql_fetch_object($rs_votes);
	$pcnt = ($row_votes->num_votes / $total_votes) * 100;
	$pcnt = round($pcnt);
	$bar_width=$pcnt*5;
	echo "<tr>\n";
	echo "<td>Yes</td>\n";
	echo "<td align=\"center\">$row_votes->num_votes</td>\n";
	echo "<td align=\"center\">$pcnt%</td>\n";
	echo "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>\n";
	echo "</tr>\n";
					

	$sql_votes="select count(*) as num_votes from survresults, users where question_id=$qid and answer='No'"
					. " and users.username = survresults.username and users.country " . StripSlashes($country);
	$rs_votes=mysql_query($sql_votes) or die(mysql_error()."<hr>".$sql_votes);
	$row_votes = mysql_fetch_object($rs_votes);
	$pcnt = ($row_votes->num_votes / $total_votes) * 100;
	$pcnt = round($pcnt);
	$bar_width=$pcnt*5;
	echo "<tr>\n";
	echo "<td>No</td>\n";
	echo "<td align=\"center\">$row_votes->num_votes</td>\n";
	echo "<td align=\"center\">$pcnt%</td>\n";
	echo "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>palliativedrugs.com/survey results</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size="-2">admin</font></a> / 
<a href="survAdmin.php"><font size="-2">survey admin</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<font size="+1"><b>survey results</b></font><br>
<?
$sql = "select distinct survresults.username from survresults, users where survey_id=$sid"
	. "  and users.username = survresults.username and users.country " . StripSlashes($country);
$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
$total_votes = mysql_num_rows($rs);
mysql_free_result($rs);

/*if ($total_votes==0) {
	$sql = "select distinct username from survrespondent where survey_id=$sid";
	$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$total_votes = mysql_num_rows($rs);
	mysql_free_result($rs);
}*/

if ($total_votes==0) {
	echo "there are no respondents to this survey yet.";
	exit;
}

$sql = "SELECT title, active, date_published, closing_date, is_default FROM survsurvey WHERE id=$sid";
$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
$row = mysql_fetch_object($rs);
?>
<table border="1" cellspacing="0" cellpadding="4">
<tr>
	<td bgcolor="#c0c0c0">title</td>
	<td bgcolor="#ededed"><?=$row->title?></td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">active</td>
	<td bgcolor="#ededed">
	<? 
	if($row->active == 1) {
		echo "yes";
	} else {
		echo "no";
	}
	?>
	</td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">published</td>
	<td bgcolor="#ededed">
	<?
	if(IsSet($row->date_published)) {
		echo FormatDbDateTime($row->date_published);
	} else {
		echo "not published";
	}	
	?>
	</td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">closing date</td>
	<td bgcolor="#ededed">
	<?
	if(IsSet($row->closing_date)) {
		echo FormatDbDateTime($row->closing_date);
	} else {
		echo "none set";
	}
	?>
	</td>
</tr>
<tr>
	<td bgcolor="#c0c0c0">respondents</td>
	<td bgcolor="#ededed"><?=$total_votes?></td>
</tr>
</table>
<br><a href="survExport2.php?id=<?=$sid?>&filt=<?=urlencode(StripSlashes($country))?>">export results to CSV</a>&nbsp;&nbsp; (<a href="survExport-Help.php">help</a>)<br>
<a href="survCleanData2.php?ClrFilt=1&survey_id=<? echo $sid?>">clean data</a><br>
<form action="<?=$PHP_SELF?>" method="POST">
<input type="hidden" name="sid" value="<?= $sid?>">
country filter <input type="text" name="filter" size="50" value="<?=StripSlashes($country)?>"> <input type="submit" class="small_btn" value="update">
</form>
<br><br>
<b>questions:</b><br>
<table border="0" cellspacing="0" cellpadding="4" width="530">
<?
$sql = "SELECT id, type, title_text, ord, active FROM survquestion WHERE survey_id=$sid order by ord";
$rs=mysql_query($sql) or die(mysql_error()."<hr>".$sql);
while($row=mysql_fetch_object($rs)) {
	$qid = $row->id;
	$qtype = $row->type;
	echo "<tr><td>&nbsp;</td></tr>\n";
	echo "<tr bgcolor=\"#c0c0c0\">\n";
	echo "<td align=\"left\" colspan=\"2\">$row->ord) $row->title_text</td>\n";
	echo "<td align=\"right\">($qtype)</td>\n";
	echo "</tr>\n";
	echo "<tr bgcolor=\"#ededed\"><td colspan=\"3\"><table border=\"0\" width=\"100%\"><tr><td>\n";  //<td A>
	if($qtype=="many_of") {
		survresManyOf($qid, $total_votes, $country);
	} elseif($qtype=="freetext") {
		survresFreeText($qid, $country);
	} elseif($qtype=="yes_no") {
		survresYesNo($qid, $total_votes, $country);
	} else {
		echo "<!--calling survresOneOf($qtype, $qid, $total_votes, $country)-->\n";
		survresOneOf($qtype, $qid, $total_votes, $country);
	}
	echo "</td></tr></table>\n"; //<td A>
}
echo "</td></tr><tr><td colspan=3>&nbsp;</td></tr>\n";
echo "</table>\n";

?>
</body>
</html>
