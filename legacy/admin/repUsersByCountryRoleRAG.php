<?php
include($_SERVER['DOCUMENT_ROOT'] . '/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/_include/visitor.class.php');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/_include/secure.inc.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>&copy;2008 palliativedrugs.com</title>
<link rel="STYLESHEET" type="text/css" href="/admin/admin.css">
</head>
<?
$rs = mysql_query("SELECT COUNT(*) as total_users FROM users WHERE active=1 AND vetted=1") or die(mysql_error());
$row = mysql_fetch_object($rs);
$total_users = $row->total_users;
mysql_free_result($rs);

$rs = mysql_query("select role, count(*) as num_users from users group by role order by 2 desc") or die(mysql_error());

?>
<body>
<font size=+1>palliativedrugs.com: RAG & Total Users by Country/Role</font><br>
<br>
<font size=-1>
date: <?echo Date("F d, Y")?><br>
total users: <?=$total_users?></font>
<br><br>
<table bgcolor=#f0f0f0>
<?

$sql = "SELECT id, name FROM country";
$rs_country = mysql_query($sql) or die (mysql_error());

while($row_country = mysql_fetch_object($rs_country)) {
	$sql = "SELECT id FROM users WHERE country = '". $row_country->name . "' AND active=1 AND vetted=1";
	$rs_tmp = mysql_query($sql) or die (mysql_error());
	if(mysql_num_rows($rs_tmp)) {
		echo '<table cellpadding="4" width="400" align="center">';
		echo '<tr>';
		echo '<th colspan="5">' . $row_country->name . ' (' . mysql_num_rows($rs_tmp) . ')</th>';
		echo '</tr>';
		echo '<tr>';
		echo '<th>role</th>';
		echo '<th># users</th>';
		echo '<th>% users</th>';
		echo '<th># RAG</th>';
		echo '<th>% RAG</th>';
		echo '</tr>';		
		
		$sql = "SELECT DISTINCT role FROM users WHERE country = '". $row_country->name . "' AND active=1 AND vetted=1";
		$sql = "select DISTINCT role, count(*) as num_users from users where active=1 AND vetted=1 group by role order by 2 desc";
		$rs2 = mysql_query($sql) or die (mysql_error());
		while($row2=mysql_fetch_object($rs2)) {
			$num_role = $rs2->num_users;
			$pcnt_role = ($num_role / mysql_num_rows($rs_tmp)) * 100 ;  // format / round-up
			$num_rag = 9;
			$pcnt_rag = 9;
			$tot_role = mysql_num_rows($rs_tmp);
			$tot_rag = 999;			
			echo '<tr>';
			echo '<td>' . $row2->role . '</td>';
			echo '<td>' . $num_role . '</td>';
			echo '<td>' . $pcnt_role. '</td>';
			echo '<td>' . $num_rag . '</td>';
			echo '<td>' . $pcnt_rag . '</td>';
			echo '</tr>';	
		}
		echo '<tr><td>&nbsp;</td><td>' . $tot_role . '</td><td>&nbsp;</td><td>' . $tot_rag . '</td><td>&nbsp;</td></tr>';
		
		echo '</table><br /><br />';
			
	}
}


/*
while($row=mysql_fetch_object($rs)) {
	$pcnt = ($row->num_users / $total_users) * 100;
	$pcnt = number_format($pcnt, 2);
	$bar_width = $pcnt * 6;
	echo "<tr>"
		. "<td>$row->role</td>"
		. "<td>$row->num_users</td>"
		. "<td>(" . round($pcnt) ."%)</td>"
		. "<td><img src=\"bar.gif\" height=\"14\" width=\"$bar_width\"></td>"
		. "</tr>";
}
*/
?>
</table>
</body>
</html>
