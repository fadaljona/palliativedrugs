<?
include("$_SERVER[DOCUMENT_ROOT]/_include/config.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/_include/db_func.inc.php");
require_once("$_SERVER[DOCUMENT_ROOT]/_include/visitor.class.php");
session_start();
$allowed="admin";
require_once("$_SERVER[DOCUMENT_ROOT]/_include/secure.inc.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:Export Survey Results</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<?
if ($_SERVER['REQUEST_METHOD']=="GET") {
?>
select a survey to export data
<form action="<?=$_SERVER[PHP_SELF]?>" method="POST">
<?= selbox("surveyid", "", "SELECT id, title FROM survsurvey", "", "select ...", "")?>
 <input type="submit" value="go">
</form>
<? } else { 
	// do the export ...
	$sid = $_POST['surveyid'];
	$sql = "select role, speciality, organisation, addr1, country,
			survquestion.id, survquestion.type, survquestion.title_text, 
			survresults.answer, users.username, users.country
			FROM survquestion, survresults, users
			WHERE survquestion.id = survresults.question_id
			and users.username = survresults.username
			and survresults.survey_id=" . $sid;
			
	echo "<table border=1>";
	echo "<tr><th>username</th><th>role</th><th>speciality</th><th>organisation</th><th>adrr1</th>"
		."<th>country</th><th>question</th><th>answer</th></tr>";
	$rs = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	while($row=mysql_fetch_object($rs)) {
		echo "<tr>";
		echo "<td>&nbsp;".$row->username ."</td>";
		echo "<td>&nbsp;".$row->role ."</td>";
		echo "<td>&nbsp;".$row->speciality ."</td>";
		echo "<td>&nbsp;".$row->organisation ."</td>";
		echo "<td>&nbsp;".$row->addr1 ."</td>";
		echo "<td>&nbsp;".$row->country ."</td>";
	//	echo "<td>&nbsp;".$row->id ."</td>";
	//	echo "<td>&nbsp;".$row->type ."</td>";
		echo "<td>&nbsp;".$row->title_text ."</td>";
		echo "<td>&nbsp;";
		if ($row->type=="freetext") {
			echo $row->answer;
		} elseif ($row->type=="one_of") {
			$where = "question_id=" . $row->id . " AND ord=" . trim($row->answer);
			//echo $where;
			echo dLookUp("answer","survanswers",$where);
		} elseif ($row->type=="yes_no") {
			echo $row->answer;
		} else {
			$arr = explode(",",$row->answer);
			for ($i=0; $i < count($arr); $i++) {
				$where = "question_id=" . $row->id . " AND ord=" . $arr[$i];
				echo dLookUp("answer","survanswers",$where) . ", ";
			}
		}
		echo "</td>";
		echo "</tr>";
	} //while($row=mysql_fetch_object($rs))
	echo "</table><br><br><b>DONE:</b> <a href=survAdmin.php>continue</a><br>";
}?>
</body>
</html>
