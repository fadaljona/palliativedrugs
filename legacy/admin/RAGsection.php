<?php require_once('../Connections/cnPall.php'); ?>
<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/app_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/form_func.inc.php");
include("$_SERVER[DOCUMENT_ROOT]/legacy//_include/RAG_func.inc.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . $_SERVER['QUERY_STRING'];
}

mysql_select_db($database_cnPall, $cnPall);

if (IsSet($_GET[ac])) {
	if ($_GET[ac]=="del") {
		$sql = "DELETE FROM rag_entry WHERE id=".$_GET[entryid];
		mysql_query($sql) or die (mysql_error()."<hr>$sql");
	}
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "frmSection")) {
  $updateSQL = sprintf("UPDATE rag_section SET title=%s, notes=%s, active=%s WHERE id=%s",
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString(isset($_POST['active']) ? "true" : "", "defined","1","0"),
                       GetSQLValueString($_POST['id'], "int"));

  
  $Result1 = mysql_query($updateSQL, $cnPall) or die(mysql_error());

  $updateGoTo = "RAGsection.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "frmSection")) {
  $insertSQL = sprintf("INSERT INTO rag_section (title, notes, active) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['title'], "text"),
                       GetSQLValueString($_POST['notes'], "text"),
                       GetSQLValueString(isset($_POST['active']) ? "true" : "", "defined","1","0"));

  mysql_select_db($database_cnPall, $cnPall);
  $Result1 = mysql_query($insertSQL, $cnPall) or die(mysql_error());
  $insertGoTo = "RAGsection.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
  $insertGoTo .= "secid=" . mysql_insert_id();
  header(sprintf("Location: %s", $insertGoTo));
}

$colname_rsSection = "1";
if (isset($_GET['secid'])) {
  $colname_rsSection = (get_magic_quotes_gpc()) ? $_GET['secid'] : addslashes($_GET['secid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsSection = sprintf("SELECT * FROM rag_section WHERE id = %s", $colname_rsSection);
$rsSection = mysql_query($query_rsSection, $cnPall) or die(mysql_error());
$row_rsSection = mysql_fetch_assoc($rsSection);
$totalRows_rsSection = mysql_num_rows($rsSection);

$colname_rsEntries = "1";
if (isset($_GET['secid'])) {
  $colname_rsEntries = (get_magic_quotes_gpc()) ? $_GET['secid'] : addslashes($_GET['secid']);
}
mysql_select_db($database_cnPall, $cnPall);
$query_rsEntries = sprintf("SELECT id, title, active FROM rag_entry WHERE section_id = %s", $colname_rsEntries);
$rsEntries = mysql_query($query_rsEntries, $cnPall) or die(mysql_error());
$row_rsEntries = mysql_fetch_assoc($rsEntries);
$totalRows_rsEntries = mysql_num_rows($rsEntries);
?>
<html>
<head>
<title>rag:topics</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="admin.css" rel="stylesheet" type="text/css">
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0>
<tr>
    <td ><a href="index.php"><font size=-2>admin</font></a> / <a href="RAGadmin.php"><font size=-2>RAG admin</font></a></td>
  </tr>
</table>
<!-- end quick-nav bar -->
<p>RAG Topic</p>
<form action="<?php echo $editFormAction; ?>" method="POST" name="frmSection" id="frmSection">
  <table border="0" align="center" cellpadding="2" cellspacing="1">
    <tr> 
      <th>title</th>
      <td colspan="2"> <input name="title" type="text" id="title" value="<?php echo $row_rsSection['title']; ?>" size="80" maxlength="255"> 
      </td>
    </tr>
    <tr> 
      <th>&nbsp;notes&nbsp;</th>
      <td colspan="2"><textarea name="notes" cols="70" rows="8" id="notes"><?php echo $row_rsSection['notes']; ?></textarea> 
      </td>
    </tr>
    <tr> 
      <th>active</th>
      <td><input name="active" type="checkbox" id="active" <?php echo ($row_rsSection['active'] ==1) ?  " checked" : "" ?>> 
        <input name="id" type="hidden" id="id" value="<?php echo $_GET['secid']; ?>"> 
      </td>
      <td align="right">
        <?php if (0==$_GET['secid']) { ?>
        <input name="btnUpdate" type="submit" id="btnUpdate5" value="save">
        <?php } // show if var=val ?>
		  <?php if (0!=$_GET['secid']) { ?>
		<input name="btnUpdate" type="submit" id="btnUpdate5" value="update">
		<?php } // show if var=val ?>
      </td>
    </tr>
    <tr> 
      <td colspan="3" align="center">&nbsp; </td>
    </tr>
  </table>
  
<?php 
if ($_GET['secid']==0) { ?>
  <input type="hidden" name="MM_insert" value="frmSection">
 <? } else { ?>
 	<input type="hidden" name="MM_update" value="frmSection">
 <? } ?>
</form>
<p>Contents (<?php echo $totalRows_rsEntries ?>) 
  <?php if ($totalRows_rsSection > 0) { // Show if recordset not empty ?>
  <a href="RAGentry.php?secid=<?php echo $_GET['secid']; ?>&entryid=0">add entry</a> 
  <?php } // Show if recordset not empty ?>
</p>
<table width="80%" border="1" align="center" cellpadding="1" cellspacing="0">
  <tr>
    <th>title</th>
    <th># docs</th>
    <th># surveys</th>
    <th>active</th>
    <th>actions</th>
  </tr>
    <?php if ($totalRows_rsEntries > 0) { // Show if recordset not empty ?>
  <?php do { ?>
  <tr> 
    <td><?php echo $row_rsEntries['title']; ?></td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center"><?php echo $row_rsEntries['active']; ?></td>
    <td align="center"><a href="RAGentry.php?secid=<?php echo $_GET['secid'];?>&entryid=<?php echo $row_rsEntries['id'];?>">edit</a> 
      | <a href="RAGsection.php?ac=del&secid=<?php echo $_GET['secid'];?>&entryid=<?php echo $row_rsEntries['id'];?>">delete</a></td>
  </tr>
  <?php } while ($row_rsEntries = mysql_fetch_assoc($rsEntries)); ?>
  <?php } // Show if recordset not empty ?>
</table>
<p>&nbsp; </p>
</body>
</html>
<?php
mysql_free_result($rsSection);

mysql_free_result($rsEntries);
?>

