<?
include("$DOCUMENT_ROOT/_include/config.inc.php");
include("$DOCUMENT_ROOT/_include/db_func.inc.php");
include("$DOCUMENT_ROOT/_include/form_func.inc.php");
require_once("$DOCUMENT_ROOT/_include/visitor.class.php");
session_start();
$allowed="admin";
require_once("$DOCUMENT_ROOT/_include/secure.inc.php");

if ($REQUEST_METHOD=="POST") {
	// form subbed, so save or update ...
	$sid = $_POST["hidSid"];
	$qid = $_POST["hidQid"];
	$type = $_POST["txtType"];
	$num_rows = $_POST["txtNumRows"];
	$title_text = AddSlashes(trim($_POST["txtTitleText"]));
	$main_text = AddSlashes(trim($_POST["txtMainText"])) . " ";
	$required = ($_POST["chkRequired"]) ? 1 : 0;
	$include =  ($_POST["chkActive"]) ? 1 : 0;
				
	if ($qid==0) {
		//it's a new one, so determine new ord, then insert...			
		$sql = "SELECT MAX(ord)+1 AS new_ord FROM survquestion WHERE survey_id=$sid";
		$rs=mysql_query($sql) or die (mysql_error()."<hr>".$sql);
		$row=mysql_fetch_object($rs);
		$new_ord = $row->new_ord;		
		if ($new_ord=="") {
			$new_ord = 1;
		} 
		$sql= "INSERT INTO survquestion"
			. "(survey_id, type, num_rows,  title_text, main_text, ord, required, active)"
			. " VALUES ($sid, '$type', $num_rows,'$title_text','$main_text', $new_ord, $required, $include)";
							
		mysql_query($sql) or die("add question<hr>new_ord=$new_ord<hr>".mysql_error()."<hr>$sql");
		header("Location:  survEditSurvey.php?sid=$sid");
		/*
		echo "new question posted<br>";
		echo "<a href=\"survEditSurvey.php?sid=$sid\">survEditSurvey.php?sid=";
		echo $sid . "</a>";
		*/
		exit;
	} else {
		// it's an update
		$sql = "UPDATE survquestion"
			. "	SET survey_id=$sid," 
			. "	type='$type',"
			. "	num_rows=$num_rows,"
			. "	title_text='$title_text',"
			. "	main_text='$main_text',"
			. "	required=$required,"
			. "	active=$include"
			. " WHERE id=$qid";
		
		mysql_query($sql) or die("surv update<hr>".mysql_error()."<hr>$sql");
		header("Location:  survQuestion.php?sid=$sid&&qid=$qid");
		/*
		echo "question updated";
		echo "<a href=\"survQuestion.php?sid=$sid&&qid=$qid\">survQuestion.php?sid=$sid&&qid=$qid</a>";
		*/
		exit;
	}//if $sid==0
} else {
	$sid = $_GET["sid"];
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>pall:admin</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
	<script language="JavaScript">
	function DeleteAnswer(qid, ord, answer, sid)
	{
	   if (confirm("DELETE: " + unescape(answer) + " ?")) {	   		
			location.href="/admin/survDelAnswer.php?qid=" + qid + "&ord=" + ord + "&sid=" + sid;
			return true;	
	   }
	}
	</script>
</head>
<body>
<!-- start quick-nav bar -->
<table width=100% bgcolor=#e0e0e0 border=1 cellspacing=0 cellpadding=0><tr><td>
<a href="index.php"><font size="-2">admin</font></a> / 
<a href="survAdmin.php"><font size="-2">survey admin</font></a> /
<a href="survEditSurvey.php?sid=<?=$sid?>"><font size="-2">edit survey</font></a>
</td></tr></table>
<!-- end quick-nav bar -->
<?
$qid = $_GET["qid"];
if ($qid==0) {
	// new one
	echo "<font size=\"+1\"><b>New Question</b></font><br>";
	$type = "";
	$num_rows = 1;
	$title = "";
	$main_text = "";
	$ord = 0;
	$requred = 0;
	$active = 1;
} else {
	// existing one
	echo "<font size=\"+1\"><b>Edit Question</b></font><br>";
	$sql = "SELECT * FROM survquestion WHERE id=$qid";
	$rs = mysql_query($sql) or die(mysql_error() . "<hr>" . $sql);
	$row = mysql_fetch_object($rs);
	$type = StripSlashes($row->type);
	$num_rows = $row->num_rows;
	$title = StripSlashes($row->title_text);
	$main_text = StripSlashes($row->main_text);
	$ord =$row->ord;
	$required =$row->required;
	$active = $row->active;
//	$sid = $row->survey_id; // not-necessary...
//	$qid = $row->id; // not-necessary...	
	echo "active=$active<br>";
}//if $qid==0
?>
<br>
<form action="<?=$PHP_SELF?>" method="POST">
<input type="hidden" name="hidQid" value="<?=$qid?>">
<input type="hidden" name="hidSid" value="<?=$sid?>">
<table border="1" cellspacing="0" cellpadding="4">
<tr>
	<td>type</td>
	<td>
		<?
		$qtype_array=array('one_of','many_of','yes_no','freetext');
		arrayToListBox('txtType',$qtype_array,$type);
		?>
<!--		<select name=txtType>
			<option value="one_of" <cfif #vType# is "one_of">selected</cfif>>one_of</option>
			<option value="many_of" <cfif #vType# is "many_of">selected</cfif>>many_of</option>
			<option value="yes_no" <cfif #vType# is "yes_no">selected</cfif>>yes_no</option>
			<option value="freetext" <cfif #vType# is "freetext">selected</cfif>>FreeText</option>
		</select>-->
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		num rows<input type="text" name="txtNumRows" size="2" maxlength="2" value="<?=$num_rows?>"> (for FreeText)
	</td>
</tr>
<tr>
	<td>title text</td>
	<td><textarea name="txtTitleText" rows="3" cols="80"><?=$title?></textarea></td>
</tr>
<tr>
	<td>main text</td>
	<td><textarea name="txtMainText" rows="8" cols="80"><?=$main_text?></textarea></td>
</tr>
<tr>
	<td>required</td>
	<td>
		<?$reqchk = ($required) ? "checked" : ""?>
		<input type="checkbox" name="chkRequired" <?=$reqchk?>>
	</td>
</tr>
<tr>
	<td>include</td>
	<td>
		<?$actchk = ($active) ? "checked" : ""?>
		<input type="checkbox" name="chkActive" <?=$actchk?>>
	</td>
</tr>
<tr>	
	<td colspan=2>
		<?$btn_caption = ($qid==0) ? "save" : "update"?>
		<input type="submit" value="<?=$btn_caption?>">
	</td>	
</tr>
</table>
</form>
<br>
<?
if ($qid != 0) {
	$sql = "SELECT question_id, answer, ord FROM survanswers WHERE question_id=$qid order by ord";
	$rs = mysql_query($sql) or die (mysql_error()."<hr>".$sql);
	echo "<b>answers:</b><br><br>";
	//if ($active==0) {
		echo "<a href=\"survAnswer.php?qid=$qid&&sid=$sid&&aord=0\">new answer</a><br><br>";
	//}
	echo "<table border=\"1\" cellspacing=\"0\" cellpadding=\"4\">"
		. "<tr bgcolor=\"#c0c0c0\">"
		. "<td>&nbsp;</td>"
		. "<td>answer</td>"
		. "<td colspan=\"4\">&nbsp;</td>"
		. "</tr>";
	while ($row=mysql_fetch_object($rs)) {
		echo "<tr>";
		echo "<td>$row->ord</td>";
		echo "<td>$row->answer</td>";
		echo "<td><a href=\"survAnswer.php?aord=$row->ord&&qid=$qid&&sid=$sid\">edit</a></td>";
		echo "<td>";
		if ($row->ord==1) {
			echo "<img src=\"/images/btnUp-disabled.gif\">";
		} else {
			echo "<a href=\"survReOrderAnswer.php?sid=$sid&&qid=$qid&&aord=$row->ord&&ud=up\">";
			echo "<img src=\"/images/btnUp.gif\" border=\"0\" alt=\"move up\">";
			echo "</a>";
		}
		echo "</td>";
		
		echo "<td>";
		if ($row->ord == mysql_num_rows($rs)) {
			echo "<img src=\"/images/btnDown-disabled.gif\">";
		} else {
			echo "<a href=\"survReOrderAnswer.php?sid=$sid&&qid=$qid&&aord=$row->ord&&ud=dn\">";
			echo "<img src=\"/images/btnDown.gif\" border=0 alt=\"move down\">";
			echo "</a>";
		}
		echo "</td>";
		
		echo "<td><a href=\"Javascript:DeleteAnswer($qid, $row->ord, '". urlencode($row->answer)."', $sid)\">";
		echo "<img src=\"/images/trash.gif\" border=\"0\" alt=\"delete\"></a></td>";

		echo "</tr>";
	}//while ($row=mysql_fetch_object($rs))
	echo "</table>";
}//if ($qid != 0) 
?>
</body>
</html>
