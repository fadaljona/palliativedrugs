<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:export survey results help</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<h2>Exporting Survey Data to Ms-Access</h2>
1. click the 'export results to CSV' link on the survey results page.<br>
<br>
2. when the page has finished generating -
<li>in web-browser do 'Edit/Select ALL' (Ctrl-A)</li>
<li>open Notepad</li>
<li>in notepad do 'Edit/Paste' (Ctrl-V)</li>
<li>in notepad do 'File/Save As' - give it a name (eg. surv25export.txt)</li>
<br><br>
3. Open Ms-Access, select or create a database to work with, then
<li>from the File menu, choose 'Get External Data/Import'</li>
<li>browse for & open the text file saved in the previous step</li>
you should now be looking at the 'Import Text Wizard' - use the following options :-
<li><b>Delimited</b> [next]</li>
<li>choose delimiter <b>Comma</b> [next]</li>
<li>first row contains field names <b>YES (checkbox)</b> [next]</li>
<li>where to store <b>in a new table</b> [next]</li>
<li>Field Options - leave all on defaults [next]</li>
<li>select <b>No primary key</b> [next]</li>
<li>enter a table-name for this data [finish]</li>
<br>
</body>
</html>