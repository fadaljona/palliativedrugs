<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

if (IsSet($_GET["ClrFilt"])) {
	unset($fltUsername);
	unset($fltLastName);
	unset($fltRole);
	unset($fltCountry);
	$sql = "select * from users ";
	$stored_sql = $sql;	
}

if ($REQUEST_METHOD=="POST") {
	$fltUsername=$_POST["fltUsername"];
	$fltLastName=$_POST["fltLastName"];
	$fltRole=$_POST["fltRole"];
	$fltCountry=$_POST["fltCountry"];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>&copy;2002 palliativedrugs.com</title>
<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<table width="100%" cellpadding="2" cellspacing="0" border="1">
<tr><td align="left"> <a href="/legacy/admin/index.php">admin</a></td></tr>
</table>
<h1>users</h1>
<form name="frmFilter" action="<?=$PHP_SELF?>" method="POST">
<table border="1" cellspacing="0" cellpadding="4" width="100%">
<tr>
	<th>username</th>
	<th>title</th>
	<th>first name</th>
	<th>last name</th>
	<th>role</th>
	<th>country</th>
	<th>date reg'd</th>
</tr>
<tr>
	<th colspan="7" align="left">
	<b>filter:</b> <a href="javascript:frmFilter.submit();">apply filter</a> | <a href="/legacy/admin/users.php?ClrFilt=1">remove filter</a>
	</th>
</tr>
<tr>
	<th><input type="text" size="20" name="fltUsername" value="<?=StripSlashes($fltUsername)?>"></th>
	<th>&nbsp;</th>
	<th>&nbsp;</th>
	<th><input type="text" size="20" name="fltLastName" value="<?=StripSlashes($fltLastName)?>"></th>
	<th>
		<?
			$options=array("any", "Doctor","Nurse","Pharmacist");
			$default = (IsSet($fltRole)) ? $fltRole : "any";
			arrayToListBox("fltRole",$options,$default);
		?>
	</th>
	<th>
		<?
			$options=array("any", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", 
			"Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia",
			"Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh",
			"Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", 
			"Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", 
			"British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso",
			"Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
			"Central African Republic", "Chad", "Chile", "China", "Christmas Island", 
			"Cocos Keeling Islands", "Colombia", "Comoros", "Congo (Brazzaville)", 
			"Congo, Democratic Republic of", "Cook Islands", "Costa Rica", "Croatia Hrvatska",
			"Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
			"Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", 
			"Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", 
			"Falkland Islands, Malvinas", "Faroe Islands", "Fiji", "Finland", "France",
			"French Guiana", "French Polynesia", "French Southern Territories", "Gabon",
			"Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Great Britain (UK)",
			"Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea",
			"Guinea-Bissau", "Guyana", "Haiti", "Heard and McDonald Islands", "Honduras",
			"Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland",
			"Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati",
			"Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon",
			"Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau",
			"Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta",
			"Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico",
			"Micronesia", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique",
			"Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", 
			"New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island",
			"Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", 
			"Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", 
			"Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", 
			"S. Georgia and S. Sandwich Isls.", "Saint Kitts and Nevis", "Saint Lucia", 
			"Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", 
			"Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovak Republic", 
			"Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", 
			"St. Helena & Dependencies", "St. Pierre and Miquelon", "Sudan", "Suriname", 
			"Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syria", 
			"Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", 
			"Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", 
			"Tuvalu", "U.S. Minor Outlying Islands", "Uganda", "Ukraine", "United Arab Emirates", 
			"United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", 
			"Vatican City State", "Venezuela", "Viet Nam", "Virgin Islands (British)", 
			"Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", 
			"Yugoslavia", "Zambia", "Zimbabwe");
			$default = (IsSet($fltCountry)) ? $fltCountry : "any";
			arrayToListBox("fltCountry",$options,$default);
		?>
	</th>
	<th>&nbsp;</th>
</tr>

<?php
$pagesize=30;

if (IsSet($_GET["page"])) {
	//use sql from last time (session var)
	if (IsSet($stored_sql)) {
		$sql = $stored_sql;
	} else {
		$sql = "select * from users ";
	}	
} else {
	if ($REQUEST_METHOD=="POST") {
		// build sql filter
		$filt = FALSE;
		$sql = "select * from users ";
		if ($fltUsername) {
			$sql .= "where username like '%".$fltUsername."%' ";
			$filt = TRUE;
		}
		if ($fltLastName) {
			if ($filt === FALSE) {
				$sql .= "where ";
			} else {
				$sql .= "and ";
			}
			$sql .= "LastName like '%".AddSlashes($fltLastName)."%' ";
			$filt = TRUE;
		}
		if ($fltRole != "any") {
			if ($filt === FALSE) {
				$sql .= "where ";
			} else {
				$sql .= "and ";
			}
			$sql .= "role='$fltRole' ";
			$filt = TRUE;
		}
		if ($fltCountry != "any") {
			if ($filt === FALSE) {
				$sql .= "where ";
			} else {
				$sql .= "and ";
			}
			$sql .= "country='$fltCountry' ";
			$filt = TRUE;
		}
	} else {
		$sql = "select * from users ";
	}
}

$stored_sql = $sql;
session_register("stored_sql");

if(!$page or !(is_numeric($page)) or $page < 0) 
	$page=1;

$sql .= "order by DateRegistered desc "
	. "LIMIT " . ($pagesize*($page-1)) . ", " . ($pagesize+1);

//echo $sql;
	
$rs = mysql_query($sql) or die(mysql_error() . "<hr>$sql<hr>");

echo "<!--AFF:" . mysql_affected_rows() . ",  NUM:" . mysql_num_rows($rs) . "-->\n";

if (mysql_affected_rows() > $pagesize)
	$moremessages=1;
while($row=mysql_fetch_object($rs)) {
	echo "<tr>"
		."<td><a href=\"edit_user.php?uid=$row->id\">$row->username</a></td>"
        ."<td>".StripSlashes($row->title)."</td>"
        ."<td>".StripSlashes($row->FirstName)."</td>"
        ."<td>".StripSlashes($row->LastName)."</td>"
        ."<td>".StripSlashes($row->role)."</td>"
        ."<td>".StripSlashes($row->Country)."</td>"
        ."<td>". substr($row->DateRegistered,0,10) ."</td>"
        ."</tr>\n";																	 
}
echo "</table>";

if ($page>1) {
	print "<a href=\"/admin/users.php?page=". ($page-1) ."\">back</a>";
	if ($moremessages) echo " / ";
}
if ($moremessages) {
	print "<a href=\"/admin/users.php?page=".($page+1)."\">next</a>\n";
}

mysql_free_result($rs);
?>

</body>
</html>
