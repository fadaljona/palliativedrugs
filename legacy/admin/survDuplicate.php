<?
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:Duplicate Survey</title>
	<link rel="STYLESHEET" type="text/css" href="admin.css">
</head>
<body>
<?
if ($_SERVER['REQUEST_METHOD']=="GET") {
?>
select a survey to duplicate
<form action="<?=$_SERVER['PHP_SELF']?>" method="POST">
<?= selbox("surveyid", "", "SELECT id, title FROM survsurvey", "", "select ...", "")?>
 <input type="submit" value="go">
</form>
<? } else { 
	// do the dupe ...
	$sid = $_POST['surveyid'];
	$sql = "SELECT * FROM survsurvey WHERE id=" . $sid;
	$rs = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$row = mysql_fetch_object($rs);
	
	echo "<h2>" . $row->title . "</h2>\n";
	
	$sql = "INSERT INTO survsurvey (title, intro, active, acknowledgement_msg) "
		. "VALUES ('" . AddSlashes($row->title) . "', '" . AddSlashes($row->intro) ."', 0, '" . AddSlashes($row->acknowledgement_msg) . "')";
	mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	$new_survey_id = mysql_insert_id();

	echo "<b>new survey_id is " . $new_survey_id . "</b><br>";
	
	$sql = "SELECT * FROM survquestion WHERE survey_id = " . $sid;
	$rs = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
	while($row=mysql_fetch_object($rs)) {
		echo "-Q- " . $row->title_text ."<br>\n";
		$sql = "INSERT INTO survquestion (survey_id) VALUES (" . $new_survey_id . ")";
		mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		$new_question_id = mysql_insert_id();
		$sql = "UPDATE survquestion SET "
				. "type='" . AddSlashes($row->type) . "', "
				. "title_text='" . AddSlashes($row->title_text) . "', "
				. "main_text='" . AddSlashes($row->main_text) . "', "
				. "ord=" . $row->ord . ", "
				. "required=" . $row->required . ", "
				. "active=" . $row->active . ", "
				. "num_rows=" . $row->num_rows . ", "
				. "continuation=" . $row->continuation . " "
				. "WHERE id=" . $new_question_id;
		mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		
		//now do answers ...
		$sql = "SELECT * FROM survanswers WHERE question_id = " . $row->id;
		$rs_ans = mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		while($row_ans=mysql_fetch_object($rs_ans)) {
			echo "---A- " . $row_ans->answer . "<br>\n";
			$sql = "INSERT INTO survanswers VALUES (".$new_question_id.", '".AddSlashes($row_ans->answer)."', ".$row_ans->ord.", ".$row_ans->is_default.")";
			mysql_query($sql) or die(mysql_error()."<hr>".$sql);
		} //while($row=mysql_fetch_object($rs_ans)) 
		echo "<br>";
	} //while($row=mysql_fetch_object($rs))
	echo "<br><br><b>DONE:</b> <a href=survAdmin.php>continue</a><br>";
}?>
</body>
</html>
