<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/css/styles.css" type="text/css">
</head>

<body style="background-color:#FFFFFF; background-image:none; margin:5px;">
<table width="560" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><p><strong>Compatibility definitions </strong></p>
      <p><strong>Definition of observational  compatibility data</strong></p>
      <p>Observational data  gives an indication of the likely physical compatibility of the combination in  use, i.e. no obvious physical change in appearance, e.g. discoloration,  clouding, the precipitation of particles or crystals. It is subjective, limited  and imprecise and generally, only major incompatibilities will be identified in  this way. </p>
      <p><strong>Definition of physical laboratory  compatibility data</strong></p>
      <p>Laboratory data is  generally derived from microscopic examination of a drug mixture under  polarised light at specified concentrations and several time points when kept  under controlled conditions. It may include other laboratory tests to determine  whether any physical changes have taken place. It is more robust than  observational data, but is still not definitive as a solution may remain  physically clear even when there is chemical incompatibility.</p>
      <p><strong>Definition of chemical  compatibility data</strong></p>
      <p>If mixing two or  more drugs does not result in a chemical change leading to loss or degradation  of one or more of the drugs, the mixture is said to be chemically compatible.  Chemical compatibility data are generally obtained by analyzing the drug  mixture by high-performance liquid chromatography (HPLC) at specified concentrations  and several time points when kept under controlled conditions<br>
        Occasionally a  drug combination has been shown to be chemically compatible but physically  incompatible. Thus, physical compatibility should be checked before proceeding  to chemical analysis.</p>    </td>
  </tr>
  <?
    	if($_SERVER['QUERY_STRING']=='p') { ?>
  <tr>
    <td class="flinks"><div align="center"><a href="javascript:window.close();">Close window </a></div></td>
  </tr>
  <? } else { ?>
  <tr>
    <td class="flinks"><p><span style="float:right"><a href="/sddb.php">Continue to SDSD search page</a></span><a href="sddb_intro.php">Back to SDSD home</a></p></td>
  </tr> <? }  ?>
</table>
</body>
</html>
