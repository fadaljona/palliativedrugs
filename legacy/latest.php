<?php
require_once('_include/pall_db.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');
$since = (IsSet($_GET['since']) ? $_GET['since'] : '');
if($since=='last') {
	$from_date = $_SESSION['previous_visit'];
} elseif($since=='all') {
	$from_date = date("Y-m-d", mktime(0,0,0,1,1,1999));
} else {
	$from_date = date( "Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$since, date("y")) );
}
?>
<div id="newsfilters">
Show additions: from last <a href="/latest.html?since=30">30 days</a> (default), 
<a href="/latest.html?since=60">60 days</a>,
<a href="/latest.html?since=90">90 days</a>, 
<a href="/latest.html?since=last">last visit</a>
or <a href="/latest.html?since=all">all</a>
</div>
<?php

if($_GET['nyear']!='' && $_GET['nmonth']!=''){
	$nyear	=	$_GET['nyear'];
	$nmonth	=	$_GET['nmonth'];
}else if($_GET['ntitle']!=''){
	$selNews	=	$pall_db->query("SELECT n.news_id,n.title,n.alias_title,YEAR(n.news_date) as newsyear,MONTHNAME(n.news_date) as newsmonth FROM 3bit_news as n WHERE 1 ")or die(mysql_error());
	if(mysql_num_rows($selNews)>0){			 		
		while($nres	=	$pall_db->getRow($selNews)){
		   $newstitle	=	stripslashes($nres['title']);
		   $naltitle	=	stripslashes($nres['alias_title']);
		   $newsmonth	=	$nres['newsmonth'];
		   $newsyear	=	$nres['newsyear'];		  
		   if((remSpecial($newstitle)==$_GET['ntitle']|| remSpecial($naltitle)== $_GET['ntitle'])&& strtolower($newsmonth)==$_GET['nmonth']){
				$gnewsid	=	$nres['news_id'];
		   }
		}
	}
}else{
	if($gnewsid=='' && $since==''){
		$selNews	=	$pall_db->query("SELECT n.news_id,n.title,YEAR(n.news_date) as newsyear,MONTHNAME(n.news_date) as newsmonth FROM 3bit_news as n WHERE 1 AND type='addition' ORDER BY news_date DESC LIMIT 0,1")or die(mysql_error());
		if(mysql_num_rows($selNews)>0){			 		
			while($nres	=	$pall_db->getRow($selNews)){
			   $newstitle	=	stripslashes($nres['title']);
			   $newsmonth	=	$nres['newsmonth'];
			   $newsyear	=	$nres['newsyear'];
			   $gnewsid	=	$nres['news_id'];		   
			}
		}
	}
}

if($_GET['nyear']!='' && $_GET['nmonth']!=''){
    $sql = "SELECT *,DATE_FORMAT(news_date,'%D %M %Y')as ndate FROM 3bit_news WHERE status=1 AND type='addition' AND DATE_FORMAT(news_date,'%M') = '".ucfirst($_GET['nmonth'])."' AND YEAR(news_date)= '".$_GET['nyear']."' ORDER BY news_date DESC";
	$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
	while($row=$pall_db->getRow($rs)) {
		echo '<div class="news-item">
					<h2>' . $row['title'].' </h2><h3>'. '' . $row['ndate']. '</h3>';
		$content1		=	preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', stripslashes($row['content']));
		echo		'<p>'.html_entity_decode($content1) . '</p>';
		if(strpos($row['link'],'ttp://')!=FALSE) {
			$target = 'target="_blank" ';
		} else { $target='';}
		if($row['url']) {
			echo "<p>".$row['url']."</p><br />";
		} elseif($row['link']) {
			echo "<p><a $target href=\"" . $row['link'] . "\">click here to view</a></p><br />";
		}

		echo "</div>";
	} 
}else if($gnewsid!=''){
	$sql = "SELECT *,DATE_FORMAT(news_date,'%D %M %Y')as ndate FROM 3bit_news WHERE status=1 AND news_id = '".$gnewsid."' ORDER BY news_date DESC";
	$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
	while($row=$pall_db->getRow($rs)) {	
		echo '<div class="news-item"><h2>' . $row['title'].'</h2><h3>'. '' .$row['ndate']. '</h3>';
		$content1		=	preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', stripslashes($row['content']));
		echo		'<p>'.html_entity_decode($content1) . '</p>';
		if(strpos($row['link'],'ttp://')!=FALSE) {
			$target = 'target="_blank" ';
		} else { $target='';}
		if($row['url']) {
			echo "<p>".$row['url']."</p>";
		} elseif($row['link']) {
			echo "<p><a $target href=\"" . $row['link'] . "\">click here to view</a></p>";
		}
		echo "</div>";
	}
}else if($since!=''){
	$sql = "SELECT *,DATE_FORMAT(news_date,'%D %M %Y')as ndate FROM 3bit_news WHERE status=1 AND type='addition' AND news_date > '" . $from_date . "' ORDER BY news_date DESC";
	$rs = $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
	while($row=$pall_db->getRow($rs)) {
		echo '<div class="news-item">
					<h2>' . $row['title'].' </h2><h3>'. '' . $row['ndate']. '</h3>';
		$content1		=	preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', stripslashes($row['content']));
		echo		'<p>'.html_entity_decode($content1) . '</p>';
		if(strpos($row['link'],'ttp://')!=FALSE) {
			$target = 'target="_blank" ';
		} else { $target='';}
		if($row['url']) {
			echo "<p>".$row['url']."</p>";
		} elseif($row['link']) {
			echo "<p><a $target href=\"" . $row['link'] . "\">click here to view</a></p>";
		}

		echo "</div>";
	} 
}



echo "</div>";
echo "</div>";




echo '<div id="newslisting">';

$sql	= "SELECT *,DATE_FORMAT(news_date,'%M') as nmonth FROM 3bit_news WHERE type='addition' and status=1 ORDER BY news_date DESC LIMIT 0,10";
$rs		= $pall_db->query($sql) or die(mysql_error()."<hr>".$sql); 
$numrows= $pall_db->getRecordCount( $rs );
if($numrows>0){
	echo '<div id="lastnewsitems">
			<h1>Recent additions</h1>
			<ul>';

	while($row=$pall_db->getRow($rs)) {
		if($row['alias_title']!='')
			$link = '/latest/' .remSpecial($row['nmonth']).'/'.remSpecial($row['alias_title']).'.html';
		else
			$link = '/latest/' .remSpecial($row['nmonth']).'/'.remSpecial($row['title']).'.html';
		
		echo '<li><a href="' . $link . '">'.$row['title'].'</a></li>';
	}

	echo "	</ul>
		</div>";	
}

 echo '<div id="newsarchive">'; 
		 
$newsarch	= "SELECT YEAR(`news_date`) as ayear FROM 3bit_news WHERE 1 AND type='addition' GROUP BY YEAR(`news_date`) ORDER BY ayear DESC ";
$ares		= $pall_db->query($newsarch) or die(mysql_error());
$arows		= $pall_db->getRecordCount( $ares );
if($arows>0){
	echo "<h1>Additions archive</h1>
			<ul>\n";
	while($arow=$pall_db->getRow($ares)) {
	   echo "<li>".$arow['ayear']."\n";	
		
	   $monsql	=	"SELECT DATE_FORMAT(news_date,'%b')mname, MONTH(news_date) as amon,DATE_FORMAT(news_date,'%M') as mfull FROM 3bit_news WHERE 1 AND YEAR(news_date) = '".$arow['ayear']."' AND type='addition' GROUP BY YEAR(news_date),MONTH(news_date) ORDER BY amon ";
	   $mres		= $pall_db->query($monsql) or die(mysql_error());
		echo "<ul>\n";
			
			while($mrow=$pall_db->getRow($mres)) {
			  echo "<li><a href='/latest/".$arow['ayear']."/".strtolower($mrow['mfull']).".html'>".$mrow['mname']."</a></li>\n";
			}

		echo "</ul>\n";

	   echo "</li>\n";
	}			  
	echo "<ul>\n";
}
echo "</div>";
?>
