<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"266";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:14:"Privacy policy";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:7:"privacy";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:9269:"<h1>PRIVACY POLICY</h1>

<p>Palliativedrugs.com Limited ("We") are committed to protecting and respecting your privacy.</p>

<p>This policy (together with our <a href="terms-and-conditions.html">Terms and conditions</a> of use and any other documents referred to on it) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us.  Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it.</p>

<p>For the purpose of the Data Protection Act 1998 ("the Act"), the data controller is Palliativedrugs.com Limited of The Study Centre, Hayward House, Nottingham University  Hospitals NHS Trust, City Campus, Hucknall Road, Nottingham, Nottinghamshire NG5 1PB. Our nominated representative for the purpose of the Act is Dr Andrew Wilcock.</p>

<h3>Information we may collect from you</h3>
<p>We may collect and process the following data about you:</p>
<ul>
<li>information that you provide by filling in forms on our site www.palliativedrugs.com ("our site"). This includes information provided at the time of registering to use our site, posting material or requesting further services, and includes your name, profession, work address, telephone number, and e-mail address. We may also ask you for information when you complete surveys, and when you report a problem with our site. As part of the registration process, you will also be allocated a unique registration number that will be used to identify you ("Unique Registration Number");</li>
<li>if you contact us, we may keep a record of that correspondence;</li>
<li>we may also ask you to complete surveys that we use for research purposes, although you do not have to respond to them;</li>
<li>details of transactions you carry out through our site and of the fulfilment of your orders;</li>
<li>details of your visits to our site including, but not limited to, traffic data, location data, and the resources that you access.</li>
</ul>

<h3>IP addresses and cookies</h3>
<p>We may collect information about your computer, including where available your IP address, operating system and browser type, for system administration and to report aggregate information for operational business purposes. This is statistical data about our users' browsing actions and patterns, and does not identify any individual.
Our site also "cookies". Cookies contain information stored on your computer's hard drive. They help us to improve our site and to deliver a better and more personalised service. They enable us:</p>
<ul>
<li>to estimate our audience size and usage pattern;</li>
<li>to store information about your preferences, and so allow us to customise our site according to your individual interests, and</li>
<li>to recognise you when you return to our site.</li>
</ul>
<p>You may refuse to accept cookies by activating the setting on your browser which allows you to refuse the setting of cookies. However, if you select this setting our site may not function properly for you. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you log on to our site.</p>

<h3>Where we store your personal data</h3>
<p>The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by organisations operating outside the EEA who work for us or for one of our suppliers. Such organisations maybe engaged in, among other things, the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
<p>All information you provide to us is stored on our secure servers. Any payment transactions will be encrypted. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
<p>Unfortunately, the transmission of information via the Internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>

<h3>Uses made of the information</h3>
<p>We use information held about you in the following ways:</p>
<ul>
<li>to ensure those who register with our site meet our eligibility criteria, i.e. <strong>you are a health professional, whether qualified, working in an identifiable clinical or academic setting or in training at an identifiable higher education institute</strong>;</li>
<li>to ensure that content from our site is presented in the most effective manner for you and for your computer;</li>
<li>to provide you with information, products or services that you request from us; 
<li>to carry out our obligations arising from any contracts entered into between you and us;</li>
<li>to allow you to participate in interactive features of our service, when you choose to do so;</li>
<li>to invite you to take part in, and otherwise administer, surveys that we think would be of interest to you or we think would be benefited by your participation;</li>
<li>to monitor who has completed surveys they have been invited to take part in; and
<li>to notify you about changes to our service and our site.</li>
</ul>
<p>We do not disclose information about identifiable individuals to those who conduct surveys on our behalf or those on whose behalf we conduct surveys, but we may provide them with aggregate information about our users (for example, we may inform them that we have so many doctors registered in the UK or nurses in the USA). We may also use such aggregate information to help those who conduct surveys on our behalf or those on whose behalf we conduct surveys ensure that they reach the audience they want to target (for example, specialist palliative care nurses in the UK). We may make use of the personal data we have collected from you to enable us to help target the invites to undertake surveys to those which are relevant to you in line with the requests of those who conduct surveys on our behalf or those on whose behalf we conduct surveys.</p>

<h3>Disclosure of your information</h3>
<p>We may disclose your personal information to third parties:</p>
<ul>
<li>your Unique Registration Number will be disclosed to organisations who conduct surveys on our behalf. These organisations will not be able to identify you from your Unique Registration Number, but it will allow us to keep track of who has fully completed surveys they have been invited to take part in;</li>
<li>in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;</li>
<li>if Palliativedrugs.com Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets; or</li>
<li>if we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our <a href="terms-and-conditions.html">Terms and conditions</a> of use, or <a href="http://www.palliativebooks.com/index.php?act=viewDoc&docId=3""target=blank">Terms and conditions of sale</a> and other agreements; or to protect the rights, property, or safety of Palliativedrugs.com Limited, our customers or registered users, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>
</ul>

<h3>Third party websites</h3>
<p>Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers, affiliates, organisations who conduct surveys on our behalf or organisations on whose behalf we conduct surveys.  If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies.  Please check these policies before you submit any personal data to these websites.</p>

<h3>Access to information</h3>
<p>The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of &#163;10 to meet our costs in providing you with details of the information we hold about you.</p>

<h3>Changes to our privacy policy</h3>
<p>Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail.</p>

<h3>Contact</h3>
<p>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>
<p><br /></p>";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:2:"15";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1217357990";s:8:"editedby";s:1:"3";s:8:"editedon";s:10:"1311615064";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1217357990";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h1>PRIVACY POLICY</h1>

<p>Palliativedrugs.com Limited ("We") are committed to protecting and respecting your privacy.</p>

<p>This policy (together with our <a href="terms-and-conditions.html">Terms and conditions</a> of use and any other documents referred to on it) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us.  Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it.</p>

<p>For the purpose of the Data Protection Act 1998 ("the Act"), the data controller is Palliativedrugs.com Limited of The Study Centre, Hayward House, Nottingham University  Hospitals NHS Trust, City Campus, Hucknall Road, Nottingham, Nottinghamshire NG5 1PB. Our nominated representative for the purpose of the Act is Dr Andrew Wilcock.</p>

<h3>Information we may collect from you</h3>
<p>We may collect and process the following data about you:</p>
<ul>
<li>information that you provide by filling in forms on our site www.palliativedrugs.com ("our site"). This includes information provided at the time of registering to use our site, posting material or requesting further services, and includes your name, profession, work address, telephone number, and e-mail address. We may also ask you for information when you complete surveys, and when you report a problem with our site. As part of the registration process, you will also be allocated a unique registration number that will be used to identify you ("Unique Registration Number");</li>
<li>if you contact us, we may keep a record of that correspondence;</li>
<li>we may also ask you to complete surveys that we use for research purposes, although you do not have to respond to them;</li>
<li>details of transactions you carry out through our site and of the fulfilment of your orders;</li>
<li>details of your visits to our site including, but not limited to, traffic data, location data, and the resources that you access.</li>
</ul>

<h3>IP addresses and cookies</h3>
<p>We may collect information about your computer, including where available your IP address, operating system and browser type, for system administration and to report aggregate information for operational business purposes. This is statistical data about our users' browsing actions and patterns, and does not identify any individual.
Our site also "cookies". Cookies contain information stored on your computer's hard drive. They help us to improve our site and to deliver a better and more personalised service. They enable us:</p>
<ul>
<li>to estimate our audience size and usage pattern;</li>
<li>to store information about your preferences, and so allow us to customise our site according to your individual interests, and</li>
<li>to recognise you when you return to our site.</li>
</ul>
<p>You may refuse to accept cookies by activating the setting on your browser which allows you to refuse the setting of cookies. However, if you select this setting our site may not function properly for you. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you log on to our site.</p>

<h3>Where we store your personal data</h3>
<p>The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by organisations operating outside the EEA who work for us or for one of our suppliers. Such organisations maybe engaged in, among other things, the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
<p>All information you provide to us is stored on our secure servers. Any payment transactions will be encrypted. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
<p>Unfortunately, the transmission of information via the Internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>

<h3>Uses made of the information</h3>
<p>We use information held about you in the following ways:</p>
<ul>
<li>to ensure those who register with our site meet our eligibility criteria, i.e. <strong>you are a health professional, whether qualified, working in an identifiable clinical or academic setting or in training at an identifiable higher education institute</strong>;</li>
<li>to ensure that content from our site is presented in the most effective manner for you and for your computer;</li>
<li>to provide you with information, products or services that you request from us; 
<li>to carry out our obligations arising from any contracts entered into between you and us;</li>
<li>to allow you to participate in interactive features of our service, when you choose to do so;</li>
<li>to invite you to take part in, and otherwise administer, surveys that we think would be of interest to you or we think would be benefited by your participation;</li>
<li>to monitor who has completed surveys they have been invited to take part in; and
<li>to notify you about changes to our service and our site.</li>
</ul>
<p>We do not disclose information about identifiable individuals to those who conduct surveys on our behalf or those on whose behalf we conduct surveys, but we may provide them with aggregate information about our users (for example, we may inform them that we have so many doctors registered in the UK or nurses in the USA). We may also use such aggregate information to help those who conduct surveys on our behalf or those on whose behalf we conduct surveys ensure that they reach the audience they want to target (for example, specialist palliative care nurses in the UK). We may make use of the personal data we have collected from you to enable us to help target the invites to undertake surveys to those which are relevant to you in line with the requests of those who conduct surveys on our behalf or those on whose behalf we conduct surveys.</p>

<h3>Disclosure of your information</h3>
<p>We may disclose your personal information to third parties:</p>
<ul>
<li>your Unique Registration Number will be disclosed to organisations who conduct surveys on our behalf. These organisations will not be able to identify you from your Unique Registration Number, but it will allow us to keep track of who has fully completed surveys they have been invited to take part in;</li>
<li>in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;</li>
<li>if Palliativedrugs.com Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets; or</li>
<li>if we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our <a href="terms-and-conditions.html">Terms and conditions</a> of use, or <a href="http://www.palliativebooks.com/index.php?act=viewDoc&docId=3""target=blank">Terms and conditions of sale</a> and other agreements; or to protect the rights, property, or safety of Palliativedrugs.com Limited, our customers or registered users, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>
</ul>

<h3>Third party websites</h3>
<p>Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers, affiliates, organisations who conduct surveys on our behalf or organisations on whose behalf we conduct surveys.  If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies.  Please check these policies before you submit any personal data to these websites.</p>

<h3>Access to information</h3>
<p>The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of &#163;10 to meet our costs in providing you with details of the information we hold about you.</p>

<h3>Changes to our privacy policy</h3>
<p>Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail.</p>

<h3>Contact</h3>
<p>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>
<p><br /></p>
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
