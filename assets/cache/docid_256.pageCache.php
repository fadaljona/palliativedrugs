<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"256";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:36:"Syringe Driver Database Introduction";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:36:"syringe-driver-database-introduction";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:5476:"<h3>Welcome to the Syringe Driver Survey Database (SDSD) </h3>
<p class="greybox"><a href="compatibility-charts">Click here to view the Syringe driver drug compatibility charts</a></p>

<p>
The SDSD is a database of the visual appearance of drug combinations reported in the palliativedrugs.com prospective international multicentre survey and also from individual reporting. This data is categorised as &lsquo;<a href="#" onMouseover="fixedtooltip('<p><i>Observational compatibility data</i></p><p>Observational data gives an indication of the likely physical compatibility of the combination in use, i.e. no obvious physical change in appearance, e.g. discoloration, clouding, the precipitation of particles or crystals. It is subjective, limited and imprecise and generally, only major incompatibilities will be identified in this way.</p>', this, event, '250px')" onMouseout="delayhidetip()">observational data</a>&rsquo;, and a description of &lsquo;appeared compatible&rsquo; has been assigned if there was no obvious physical change in appearance of the combination in use, e.g. discoloration, clouding, or the precipitation of particles/crystals.</p><br />
<p>
<strong>Use of the SDSD</strong> 
</p>
<p>
Drug combinations can be searched for using the drop down menu or by browsing the index. 
</p>
<p>
It is important to ascertain whether the compatibility data displayed are relevant to the situation of intended use: 
</p>
<ul>
<li>drug combinations may be compatible at certain concentrations but not at others, thus the <em>concentration</em> of each drug in the solution (the dose of each drug divided by the total final volume being used) should be compared not the dose</li>
<li>different diluents and longer infusion periods may also cause compatibility problems and the diluent used and the duration of the infusion should also be compared</li>
<li>other factors such as temperature (ambient or body temperature), light exposure, order of mixing and delivery system material may also affect drug compatibility and may also contribute to conflicting reports on compatibility.</li>
</ul>
<p>
<strong><br />
Addition of further entries</strong> 
</p>
<p>
We would like to encourage you to enter any unique drug combinations that you have used for which you have had little or no <a href="#" onMouseover="fixedtooltip('<p><i>Physical laboratory compatibility data</i></p><p>Laboratory data is generally derived from microscopic examination of a drug mixture under polarised light at specified concentrations and several time points when kept under controlled conditions. It may include other laboratory tests to determine whether any physical changes have taken place. It is more robust than observational data, but is still not definitive as a solution may remain physically clear even when there is chemical incompatibility.</p>', this, event, '250px')" onMouseout="delayhidetip()">physical laboratory</a> or <a href="#" onMouseover="fixedtooltip('<p><i>Chemical compatibility data</i></p><p>If mixing two or more drugs does not result in a chemical change leading to loss or degradation of one or more of the drugs, the mixture is said to be chemically compatible. Chemical compatibility data are generally obtained by analyzing the drug mixture by high-performance liquid chromatography (HPLC) at specified concentrations and several time points when kept under controlled conditions. Occasionally a drug combination has been shown to be chemically compatible but physically incompatible. Thus, physical compatibility should be checked before proceeding to chemical analysis.</p>', this, event, '250px')" onMouseout="delayhidetip()">chemical compatibility</a> data, especially if you have experienced incompatibility. All entries will be viewed by palliativedrugs.com pharmacists before addition to the live database. 
</p>
<p>
We aim to co-ordinate further surveys in the near future trying to capture data for alfentanil, morphine, oxycodone and hydromorphone combinations. If you would like to contribute to these please <a href="mailto:hq@palliativedrugs.com">contact us</a>.
</p>


<p align="center"><a href="syringe-driver-database.html">Click here to continue to SDSD search page</a></p>

<p>
<strong>Acknowledgments</strong> 
</p>
<p>
We are very grateful to the following participants of the 2004 and 2006 prospective international multicentre surveys. Without their help this resource would not have been possible: Judith Earl, Helen Neil, Siao Ching, Liz Knowles, Tim McCarthy, Elisa Tarr (Royal Marsden Hospital, London, UK), Nichola Jackson (Royal Hallamshire Hospital, Sheffield, UK), Jo Thomas, Jayin Jacob, Sarah Charlesworth (Hayward House, Nottingham, UK), John Beale (Sobell House, Oxford, UK), Janet Trundle (Renfrewshire and Inverclyde PCT, Scotland, UK), Susan Williams (Phyllis Tuckwell Hospice, Surrey, UK), Clare Amass (Garden House Hospice, Hertfordshire,UK), Elayne Harris and Carolyn Mackay (Greater Glasgow PCT, Scotland, UK), Vanessa Smith, Heather Sanders, Sue Watson (Cranford Hospice, New Zealand), Moira Cookson (St Gemma's Hospice, Leeds, UK), Helen Allsop (Ulster Hospital and Marie Curie Centre, Belfast), Margaret Gibbs (St Christopher's Hospice, London, UK), Paul Wilson (Fife Acute Hospitals, Scotland, UK), Christine Hirsch (Compton Hospice, Wolverhampton, UK), Ollie Minton (St Barnabas Hospice, Worthing, UK) and Jane Butterworth (Stoke Manderville Hospital, Aylesbury, UK).</p>";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:2:"13";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1213980300";s:8:"editedby";s:1:"3";s:8:"editedon";s:10:"1254232667";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1213980300";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h3>Welcome to the Syringe Driver Survey Database (SDSD) </h3>
<p class="greybox"><a href="compatibility-charts">Click here to view the Syringe driver drug compatibility charts</a></p>

<p>
The SDSD is a database of the visual appearance of drug combinations reported in the palliativedrugs.com prospective international multicentre survey and also from individual reporting. This data is categorised as &lsquo;<a href="#" onMouseover="fixedtooltip('<p><i>Observational compatibility data</i></p><p>Observational data gives an indication of the likely physical compatibility of the combination in use, i.e. no obvious physical change in appearance, e.g. discoloration, clouding, the precipitation of particles or crystals. It is subjective, limited and imprecise and generally, only major incompatibilities will be identified in this way.</p>', this, event, '250px')" onMouseout="delayhidetip()">observational data</a>&rsquo;, and a description of &lsquo;appeared compatible&rsquo; has been assigned if there was no obvious physical change in appearance of the combination in use, e.g. discoloration, clouding, or the precipitation of particles/crystals.</p><br />
<p>
<strong>Use of the SDSD</strong> 
</p>
<p>
Drug combinations can be searched for using the drop down menu or by browsing the index. 
</p>
<p>
It is important to ascertain whether the compatibility data displayed are relevant to the situation of intended use: 
</p>
<ul>
<li>drug combinations may be compatible at certain concentrations but not at others, thus the <em>concentration</em> of each drug in the solution (the dose of each drug divided by the total final volume being used) should be compared not the dose</li>
<li>different diluents and longer infusion periods may also cause compatibility problems and the diluent used and the duration of the infusion should also be compared</li>
<li>other factors such as temperature (ambient or body temperature), light exposure, order of mixing and delivery system material may also affect drug compatibility and may also contribute to conflicting reports on compatibility.</li>
</ul>
<p>
<strong><br />
Addition of further entries</strong> 
</p>
<p>
We would like to encourage you to enter any unique drug combinations that you have used for which you have had little or no <a href="#" onMouseover="fixedtooltip('<p><i>Physical laboratory compatibility data</i></p><p>Laboratory data is generally derived from microscopic examination of a drug mixture under polarised light at specified concentrations and several time points when kept under controlled conditions. It may include other laboratory tests to determine whether any physical changes have taken place. It is more robust than observational data, but is still not definitive as a solution may remain physically clear even when there is chemical incompatibility.</p>', this, event, '250px')" onMouseout="delayhidetip()">physical laboratory</a> or <a href="#" onMouseover="fixedtooltip('<p><i>Chemical compatibility data</i></p><p>If mixing two or more drugs does not result in a chemical change leading to loss or degradation of one or more of the drugs, the mixture is said to be chemically compatible. Chemical compatibility data are generally obtained by analyzing the drug mixture by high-performance liquid chromatography (HPLC) at specified concentrations and several time points when kept under controlled conditions. Occasionally a drug combination has been shown to be chemically compatible but physically incompatible. Thus, physical compatibility should be checked before proceeding to chemical analysis.</p>', this, event, '250px')" onMouseout="delayhidetip()">chemical compatibility</a> data, especially if you have experienced incompatibility. All entries will be viewed by palliativedrugs.com pharmacists before addition to the live database. 
</p>
<p>
We aim to co-ordinate further surveys in the near future trying to capture data for alfentanil, morphine, oxycodone and hydromorphone combinations. If you would like to contribute to these please <a href="mailto:hq@palliativedrugs.com">contact us</a>.
</p>


<p align="center"><a href="syringe-driver-database.html">Click here to continue to SDSD search page</a></p>

<p>
<strong>Acknowledgments</strong> 
</p>
<p>
We are very grateful to the following participants of the 2004 and 2006 prospective international multicentre surveys. Without their help this resource would not have been possible: Judith Earl, Helen Neil, Siao Ching, Liz Knowles, Tim McCarthy, Elisa Tarr (Royal Marsden Hospital, London, UK), Nichola Jackson (Royal Hallamshire Hospital, Sheffield, UK), Jo Thomas, Jayin Jacob, Sarah Charlesworth (Hayward House, Nottingham, UK), John Beale (Sobell House, Oxford, UK), Janet Trundle (Renfrewshire and Inverclyde PCT, Scotland, UK), Susan Williams (Phyllis Tuckwell Hospice, Surrey, UK), Clare Amass (Garden House Hospice, Hertfordshire,UK), Elayne Harris and Carolyn Mackay (Greater Glasgow PCT, Scotland, UK), Vanessa Smith, Heather Sanders, Sue Watson (Cranford Hospice, New Zealand), Moira Cookson (St Gemma's Hospice, Leeds, UK), Helen Allsop (Ulster Hospital and Marie Curie Centre, Belfast), Margaret Gibbs (St Christopher's Hospice, London, UK), Paul Wilson (Fife Acute Hospitals, Scotland, UK), Christine Hirsch (Compton Hospice, Wolverhampton, UK), Ollie Minton (St Barnabas Hospice, Worthing, UK) and Jane Butterworth (Stoke Manderville Hospital, Aylesbury, UK).</p>
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
