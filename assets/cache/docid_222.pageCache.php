<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"222";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:15:"Editorial Staff";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:15:"editorial-staff";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:9894:"<h1>ABOUT US</h1>

<p>
<a href="#about">About palliativedrugs.com Ltd.</a><br />
<a href="#staff">About the editorial staff</a><br />
<a href="#ack">Acknowledgements</a>
</p>

<a name="about"></a><h3>About palliativedrugs.com Ltd.</h3>
<p>Palliativedrugs.com Ltd. was founded in 2000 by Robert Twycross and Andrew Wilcock in order to promote and disseminate information about the use of drugs in palliative care, through the website and book versions of the Palliative Care Formulary. Profits from the company will be used to fund research and education initiatives in Palliative care emanating from Oxford and Nottingham.
</p>
<p class="indented">
In addition to providing drug information we are keen to develop a sense of community for members of the website. We welcome comments about how easy the website is to use, any information about the drugs featured or suggestions for other drugs to be included. A bulletin board is provided on the website in order to stimulate questions and debate, and for you to share your experiences with other colleagues. We continually highlight particular areas of interest relating to the use of drugs in palliative care.
</p>
<p class="indented">
We are committed to keeping palliativedrugs.com a free-access resource. Our income is derived from sales of the Palliative Care Formulary books and market research. We do not host commercial advertisements. You can help us by buying the Palliative Care Formulary, and other books published by palliativedrugs.com from our bookshop, and by completing market research surveys when invited to do so from time to time.
</p>
<a name="staff"></a><h3>About the editorial staff</h3>
<p><strong>Editors-in-Chief</strong><br /><br />
<strong>Robert Twycross</strong> DM Oxon, FRCP London
is Emeritus Clinical Reader in Palliative Medicine, Oxford University, former Head, WHO Collaborating Centre for Palliative Care, and Director, palliativedrugs.com Ltd. He has written some 300 articles, chapters and editorials, and is the author of several books, notably Pain Relief in Advanced Cancer. Churchill Livingstone, Edinburgh 1994; Symptom Management in Advanced Cancer (3nd edition, with Andrew Wilcock). Radcliffe Medical Press, Oxon, 2001; and Introducing Palliative Care (4th edition). Radcliffe Medical Press, Oxon 2003. He is a founder member of the International Association for the Study of Pain, the Association for Palliative Medicine (UK), the British Lymphology Society, the European Association for Palliative Care, and the Palliative Care Research Society (UK). He has taught in over 40 countries, and has had particularly close academic ties with Argentina, Hungary, India and Poland.</p>
<p><strong>Andrew Wilcock</strong> DM FRCP is Macmillan Clinical Reader in Palliative Medicine and Medical Oncology, Nottingham University; Consultant Physician, Hayward House, Macmillan Specialist Palliative Care Unit, Nottingham City Hospital. He leads a Research Team with a particular interest in breathlessness and exercise capacity with an overall aim of helping to keep people with cancer as physically independent as possible for as long as possible. Active member of the NCRI Palliative Care Studies Group, including previous Chair of the cachexia subgroup. Palliative care prescribing advisor to the British National Formulary. An author of the 3rd edition of Symptom Management in Advanced Cancer and the 5th edition of Introducing Palliative Care. Previously Chair of the Mid-Trent Cancer Services Network Palliative Care Group, Nottingham Cancer Centre Palliative Care Group, inaugural Secretary for the Science Committee of the Association for Palliative Medicine of Great Britain and Ireland and member of the National Institute for Clinical Excellence Lung Cancer Guidelines Development Group.</p>

<p><strong>Senior Editor</strong><br /><br />
<strong>Julie Mortimer</strong> BPharm (Hons) MRPharmS is a freelance pharmaceutical writer and editor. She graduated from Nottingham University in 1981 and qualified as a pharmacist in 1982 after completing her preregistration year at Nottingham General Hospital. Following basic grade rotation at Nottingham City Hospital, she became Staff Pharmacist (Formulary) for Leicestershire hospitals. Her duties involved updating the Leicestershire Prescribing Guide and assisting in the Regional Medicines Information Centre, Leicester Royal Infirmary. Subsequently, became a freelance pharmaceutical writer and editor, with principal clients including ADIS International Ltd and Douglas Science Editing Services, Japan. She has worked with palliativedrugs.com since 2004, helping with editing and copy editing of the PCF and liaising with overseas editors (US and Canadian editions).</p>

<p><strong>Editors</strong>
</p>
<p><strong>Sarah Charlesworth</strong> BPharm (Hons) MRPharmS is a Specialist Pharmacist in Palliative Care Information and Website Management for www.palliativedrugs.com at Hayward House Macmillan Specialist Palliative Care Unit, Nottingham University Hospitals NHS Trust. She has a background in hospital clinical pharmacy and pharmacy sterile production management including active involvement in adult and paediatric nutrition teams. She has a Pharmacy Clinical Diploma and a distinction in the Postgraduate Certificate for Managing Health Services (Nottingham Trent University). Primarily responsible for several PCF sections (CSCI, compatibility charts, drugs via feeding tubes) and the continual updating of several website sections (news, additions, surveys, document library, Syringe Driver Survey Database (SDSD)).</p>

<p><strong>Paul Howard</strong> BMedSci BMBS Nottm, MRCP London, Consultant in Palliative Medicine at Duchess of Kent House, Reading, Berkshire. He trained in general then palliative medicine in Nottingham, Mansfield and Derby. He has experience of interventional, pharmaco-epidemiological and laboratory-based research. His publications include drug errors and the causation and preventability of drug-related morbidity. He has a strong interest in therapeutics and medicines management, and has chaired a Data Safety and Monitoring Committee. Specific contributions to the Palliative Care Formulary include neuropharmacology, adjuvant analgesia, and drugs and driving.</p>

<p><strong>Claire Stark Toller</strong> BM BCh Oxon, BA Oxon, MA London, MRCP London, completes her training in palliative medicine in the Oxford deanery in 2011. Early studies majored on cardiac physiology and pharmacology, neuropharmacology, and psychiatric disorders. Her MA was in Medical Law and Ethics, with a dissertation on Slippery Slopes and End-of-Life Decision-Making. She has conducted research into decision making at the end of life, and is a former member of the Ethics committee of the Association for Palliative Medicine. She is co-author of the 4th edition of Symptom Management in Advanced Cancer. Specific contributions to the Palliative Care Formulary (4th edition) include the endocrine system, urinary tract disorders, and anaemia.</p>

<a name="ack"><h3>Acknowledgements</h3></a>
<p>
The production of the PCF3 book, which forms the basis for this website, depends partly on the help and advice of numerous colleagues, both past and present. We acknowledge with gratitude the support of close colleagues, particularly, Ray Corcoran, Patrick Costello, Vincent Crosby, Bisharat El Khoury, Annabella Marks and Claire Stark Toller, and those members of palliativedrugs.com who have provided feedback on one or more of the monographs or contributed to the Syringe Driver Survey Database. Figures 18.1 and 18.2 were kindly provided by Clinical Services Support, P.A. Smiths Medical International Ltd. We acknowledge with thanks the contributions from the following advisors and correspondents.</p>
<p>
	The principal advisors for this edition were: Sara Booth (oxygen), Keith Budd (buprenorphine), Tim Carter (analgesic drugs and fitness to drive), Jo Chambers (renal effects of opioids), Albert Dahan (buprenorphine), Andrew Davies (Ear nose and Oropharynx), Tony Dickenson (strong opioids), Ken Gillman (serotonin toxicity), Vaughan Keeley (AIEs), Henry McQuay (management of postoperative pain in opioid-dependent patients), Peter Mortimer (AIEs), Simon Noble (LMWH), Victor Pace (NSAIDs and nabumetone), John Shuster (antidepressants), Vanessa Siddall (oral nutritional supplements), Anne Tattersfield (asthma and COPD), Hywel Williams (Skin).
</p>
<p>
	Correspondents included: Claire Amass (glycopyrronium oral solution formula), David Baldwin (asthma and COPD), Kathryn Blount (oral nutritional supplements), Richard Burden (prescribing in renal impairment), Rachel Howard (drug concentration interpretation), Ian Johnston (asthma and COPD), Judy Lawrence (oral nutritional supplements), Martin Lennard (cytochrome P450), Staffan Lundstr&ouml;m (propofol), Roger Knaggs (management of postoperative pain in opioid-dependent patients), Wolfgang Koppert (buprenorphine), John MacKenzie (management of postoperative pain in opioid-dependent patients), Heather Major (analgesic drugs and fitness to drive), Jim Mason (management of postoperative pain in opioid-dependent patients), Willie McGhee (oxygen), John Moyle (propofol), Felicity Murtagh (renal effects of opioids), Mark Nelson (oxygen), Don Page (oxygen), Judith Palmer (prescribing in renal impairment), Lukas Radbruch (buprenorphine), Reinhard Sittl (buprenorphine), Richard Sloan (p.r.n. prescribing), Mike Stroud (oral nutritional supplements), Jo Thomas (continuous subcutaneous infusions), Adrian Tookman (phenobarbital).
</p>
<p>
	We are also most grateful to Karen Isaac, Susan Wright and Susan Brown for their contributions in relation to general secretarial assistance, the preparation of the typescript, and copy-editing respectively.
</p>
<p>
<br /><br /><br /><br /><br /><br />
</p>

";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:1:"8";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"3";s:9:"createdon";s:10:"1204046742";s:8:"editedby";s:1:"3";s:8:"editedon";s:10:"1285311340";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1204046742";s:11:"publishedby";s:1:"3";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h1>ABOUT US</h1>

<p>
<a href="#about">About palliativedrugs.com Ltd.</a><br />
<a href="#staff">About the editorial staff</a><br />
<a href="#ack">Acknowledgements</a>
</p>

<a name="about"></a><h3>About palliativedrugs.com Ltd.</h3>
<p>Palliativedrugs.com Ltd. was founded in 2000 by Robert Twycross and Andrew Wilcock in order to promote and disseminate information about the use of drugs in palliative care, through the website and book versions of the Palliative Care Formulary. Profits from the company will be used to fund research and education initiatives in Palliative care emanating from Oxford and Nottingham.
</p>
<p class="indented">
In addition to providing drug information we are keen to develop a sense of community for members of the website. We welcome comments about how easy the website is to use, any information about the drugs featured or suggestions for other drugs to be included. A bulletin board is provided on the website in order to stimulate questions and debate, and for you to share your experiences with other colleagues. We continually highlight particular areas of interest relating to the use of drugs in palliative care.
</p>
<p class="indented">
We are committed to keeping palliativedrugs.com a free-access resource. Our income is derived from sales of the Palliative Care Formulary books and market research. We do not host commercial advertisements. You can help us by buying the Palliative Care Formulary, and other books published by palliativedrugs.com from our bookshop, and by completing market research surveys when invited to do so from time to time.
</p>
<a name="staff"></a><h3>About the editorial staff</h3>
<p><strong>Editors-in-Chief</strong><br /><br />
<strong>Robert Twycross</strong> DM Oxon, FRCP London
is Emeritus Clinical Reader in Palliative Medicine, Oxford University, former Head, WHO Collaborating Centre for Palliative Care, and Director, palliativedrugs.com Ltd. He has written some 300 articles, chapters and editorials, and is the author of several books, notably Pain Relief in Advanced Cancer. Churchill Livingstone, Edinburgh 1994; Symptom Management in Advanced Cancer (3nd edition, with Andrew Wilcock). Radcliffe Medical Press, Oxon, 2001; and Introducing Palliative Care (4th edition). Radcliffe Medical Press, Oxon 2003. He is a founder member of the International Association for the Study of Pain, the Association for Palliative Medicine (UK), the British Lymphology Society, the European Association for Palliative Care, and the Palliative Care Research Society (UK). He has taught in over 40 countries, and has had particularly close academic ties with Argentina, Hungary, India and Poland.</p>
<p><strong>Andrew Wilcock</strong> DM FRCP is Macmillan Clinical Reader in Palliative Medicine and Medical Oncology, Nottingham University; Consultant Physician, Hayward House, Macmillan Specialist Palliative Care Unit, Nottingham City Hospital. He leads a Research Team with a particular interest in breathlessness and exercise capacity with an overall aim of helping to keep people with cancer as physically independent as possible for as long as possible. Active member of the NCRI Palliative Care Studies Group, including previous Chair of the cachexia subgroup. Palliative care prescribing advisor to the British National Formulary. An author of the 3rd edition of Symptom Management in Advanced Cancer and the 5th edition of Introducing Palliative Care. Previously Chair of the Mid-Trent Cancer Services Network Palliative Care Group, Nottingham Cancer Centre Palliative Care Group, inaugural Secretary for the Science Committee of the Association for Palliative Medicine of Great Britain and Ireland and member of the National Institute for Clinical Excellence Lung Cancer Guidelines Development Group.</p>

<p><strong>Senior Editor</strong><br /><br />
<strong>Julie Mortimer</strong> BPharm (Hons) MRPharmS is a freelance pharmaceutical writer and editor. She graduated from Nottingham University in 1981 and qualified as a pharmacist in 1982 after completing her preregistration year at Nottingham General Hospital. Following basic grade rotation at Nottingham City Hospital, she became Staff Pharmacist (Formulary) for Leicestershire hospitals. Her duties involved updating the Leicestershire Prescribing Guide and assisting in the Regional Medicines Information Centre, Leicester Royal Infirmary. Subsequently, became a freelance pharmaceutical writer and editor, with principal clients including ADIS International Ltd and Douglas Science Editing Services, Japan. She has worked with palliativedrugs.com since 2004, helping with editing and copy editing of the PCF and liaising with overseas editors (US and Canadian editions).</p>

<p><strong>Editors</strong>
</p>
<p><strong>Sarah Charlesworth</strong> BPharm (Hons) MRPharmS is a Specialist Pharmacist in Palliative Care Information and Website Management for www.palliativedrugs.com at Hayward House Macmillan Specialist Palliative Care Unit, Nottingham University Hospitals NHS Trust. She has a background in hospital clinical pharmacy and pharmacy sterile production management including active involvement in adult and paediatric nutrition teams. She has a Pharmacy Clinical Diploma and a distinction in the Postgraduate Certificate for Managing Health Services (Nottingham Trent University). Primarily responsible for several PCF sections (CSCI, compatibility charts, drugs via feeding tubes) and the continual updating of several website sections (news, additions, surveys, document library, Syringe Driver Survey Database (SDSD)).</p>

<p><strong>Paul Howard</strong> BMedSci BMBS Nottm, MRCP London, Consultant in Palliative Medicine at Duchess of Kent House, Reading, Berkshire. He trained in general then palliative medicine in Nottingham, Mansfield and Derby. He has experience of interventional, pharmaco-epidemiological and laboratory-based research. His publications include drug errors and the causation and preventability of drug-related morbidity. He has a strong interest in therapeutics and medicines management, and has chaired a Data Safety and Monitoring Committee. Specific contributions to the Palliative Care Formulary include neuropharmacology, adjuvant analgesia, and drugs and driving.</p>

<p><strong>Claire Stark Toller</strong> BM BCh Oxon, BA Oxon, MA London, MRCP London, completes her training in palliative medicine in the Oxford deanery in 2011. Early studies majored on cardiac physiology and pharmacology, neuropharmacology, and psychiatric disorders. Her MA was in Medical Law and Ethics, with a dissertation on Slippery Slopes and End-of-Life Decision-Making. She has conducted research into decision making at the end of life, and is a former member of the Ethics committee of the Association for Palliative Medicine. She is co-author of the 4th edition of Symptom Management in Advanced Cancer. Specific contributions to the Palliative Care Formulary (4th edition) include the endocrine system, urinary tract disorders, and anaemia.</p>

<a name="ack"><h3>Acknowledgements</h3></a>
<p>
The production of the PCF3 book, which forms the basis for this website, depends partly on the help and advice of numerous colleagues, both past and present. We acknowledge with gratitude the support of close colleagues, particularly, Ray Corcoran, Patrick Costello, Vincent Crosby, Bisharat El Khoury, Annabella Marks and Claire Stark Toller, and those members of palliativedrugs.com who have provided feedback on one or more of the monographs or contributed to the Syringe Driver Survey Database. Figures 18.1 and 18.2 were kindly provided by Clinical Services Support, P.A. Smiths Medical International Ltd. We acknowledge with thanks the contributions from the following advisors and correspondents.</p>
<p>
	The principal advisors for this edition were: Sara Booth (oxygen), Keith Budd (buprenorphine), Tim Carter (analgesic drugs and fitness to drive), Jo Chambers (renal effects of opioids), Albert Dahan (buprenorphine), Andrew Davies (Ear nose and Oropharynx), Tony Dickenson (strong opioids), Ken Gillman (serotonin toxicity), Vaughan Keeley (AIEs), Henry McQuay (management of postoperative pain in opioid-dependent patients), Peter Mortimer (AIEs), Simon Noble (LMWH), Victor Pace (NSAIDs and nabumetone), John Shuster (antidepressants), Vanessa Siddall (oral nutritional supplements), Anne Tattersfield (asthma and COPD), Hywel Williams (Skin).
</p>
<p>
	Correspondents included: Claire Amass (glycopyrronium oral solution formula), David Baldwin (asthma and COPD), Kathryn Blount (oral nutritional supplements), Richard Burden (prescribing in renal impairment), Rachel Howard (drug concentration interpretation), Ian Johnston (asthma and COPD), Judy Lawrence (oral nutritional supplements), Martin Lennard (cytochrome P450), Staffan Lundstr&ouml;m (propofol), Roger Knaggs (management of postoperative pain in opioid-dependent patients), Wolfgang Koppert (buprenorphine), John MacKenzie (management of postoperative pain in opioid-dependent patients), Heather Major (analgesic drugs and fitness to drive), Jim Mason (management of postoperative pain in opioid-dependent patients), Willie McGhee (oxygen), John Moyle (propofol), Felicity Murtagh (renal effects of opioids), Mark Nelson (oxygen), Don Page (oxygen), Judith Palmer (prescribing in renal impairment), Lukas Radbruch (buprenorphine), Reinhard Sittl (buprenorphine), Richard Sloan (p.r.n. prescribing), Mike Stroud (oral nutritional supplements), Jo Thomas (continuous subcutaneous infusions), Adrian Tookman (phenobarbital).
</p>
<p>
	We are also most grateful to Karen Isaac, Susan Wright and Susan Brown for their contributions in relation to general secretarial assistance, the preparation of the typescript, and copy-editing respectively.
</p>
<p>
<br /><br /><br /><br /><br /><br />
</p>


			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
