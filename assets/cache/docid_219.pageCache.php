<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"219";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:10:"Drug names";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:10:"drug-names";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:8044:"<h1> DRUG NAMES</h1>

<!--
<p class="pagedochead">
DRUG NAMES
</p>
<p>
&nbsp;
</p>
<hr width="100%" />
-->
<p class="first">
Following a European Union directive, all drugs marketed in Europe are now known by their recommended International Non-proprietary
Names (rINNs). Previously, in the UK, drugs were known by their British Approved Names (BANs). Differences between BANs and
rINNs are listed in Table 1.
</p>
<p class="indented">
Where United States Adopted Names (USANs) differ from rINNs and BANs, the USANs have also been included to aid understanding
of the US literature.
</p>
<p class="indented">
With compound preparations such as codeine and paracetamol (USAN acetaminophen) or diphenoxylate and atropine, the UK conventional
name is shown in Table 2, e.g. co-codamol or co-phenotrope.
</p>

<p class="first">
<strong>Table 1</strong>&nbsp;&nbsp;Drug names relevant to palliative care for which the rINN, BAN and/or USAN differ
</p>
<table border="1" cellpadding="2" cellspacing="0" frame="above" id="tbl" rules="groups" width="95%">
	<thead>
		<tr>
			<td>rINN</td>
			<td>BAN</td>
			<td>USAN</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Alimemazine</td>
			<td>Trimeprazine</td>
			<td>Trimeprazine</td>
		</tr>
		<tr>
			<td>Aluminium</td>
			<td>&nbsp;</td>
			<td>Aluminum</td>
		</tr>
		<tr>
			<td>Amfetamine</td>
			<td>&nbsp;</td>
			<td>Amphetamine</td>
		</tr>
		<tr>
			<td>Amobarbital</td>
			<td>Amylobarbitone</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Beclometasone</td>
			<td>Beclomethasone</td>
			<td>Beclomethasone</td>
		</tr>
		<tr>
			<td>Bendroflumethiazide</td>
			<td>Bendrofluazide</td>
			<td>Bendroflumethiazide</td>
		</tr>
		<tr>
			<td>Benorilate</td>
			<td>Benorylate</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Benzathine benzylpenicillin</td>
			<td>Benzathine penicillin</td>
			<td>Benzathine penicillin</td>
		</tr>
		<tr>
			<td>Benzatropine</td>
			<td>Benztropine</td>
			<td>Benztropine</td>
		</tr>
		<tr>
			<td>Benzylpenicillin</td>
			<td>&nbsp;</td>
			<td>Penicillin G</td>
		</tr>
		<tr>
			<td>Calcitonin (salmon)</td>
			<td>Salcatonin</td>
			<td>Calcitonin</td>
		</tr>
		<tr>
			<td>Carmellose</td>
			<td>&nbsp;</td>
			<td>Carboxymethylcellulose</td>
		</tr>
		<tr>
			<td>Cefalexin (etc.)</td>
			<td>Cephalexin (etc.)</td>
			<td>Cephalexin (etc.)</td>
		</tr>
		<tr>
			<td>Chlorphenamine</td>
			<td>Chlorpheniramine</td>
			<td>Chlorpheniramine</td>
		</tr>
		<tr>
			<td>Ciclosporin</td>
			<td>Cyclosporin</td>
			<td>Cyclosporine</td>
		</tr>
		<tr>
			<td>Clomethiazole</td>
			<td>Chlormethiazole</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Colestyramine</td>
			<td>Cholestyramine</td>
			<td>Cholestyramine</td>
		</tr>
		<tr>
			<td>Dantron</td>
			<td>Danthron</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Dexamfetamine</td>
			<td>Dexamphetamine</td>
			<td>Dextroamphetamine</td>
		</tr>
		<tr>
			<td>Dextropropoxyphene</td>
			<td>&nbsp;</td>
			<td>Propoxyphene</td>
		</tr>
		<tr>
			<td>Dicycloverine</td>
			<td>Dicyclomine</td>
			<td>Dicyclomine</td>
		</tr>
		<tr>
			<td>Dienestrol</td>
			<td>Dienoestrol</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Diethylstilbestrol</td>
			<td>Stilboestrol</td>
			<td>Diethylstilbestrol</td>
		</tr>
		<tr>
			<td>Dimeticone</td>
			<td>Dimethicone</td>
			<td>Dimethicone</td>
		</tr>
		<tr>
			<td>Dosulepin</td>
			<td>Dothiepin</td>
			<td>Dothiepin</td>
		</tr>
		<tr>
			<td>Estradiol</td>
			<td>Oestradiol</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Etamsylate</td>
			<td>Ethamsylate</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Furosemide</td>
			<td>Frusemide</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Glibenclamide</td>
			<td>&nbsp;</td>
			<td>Glyburide</td>
		</tr>
		<tr>
			<td>Glyceryl trinitrate</td>
			<td>&nbsp;</td>
			<td>Nitroglycerin</td>
		</tr>
		<tr>
			<td>Glycopyrronium</td>
			<td>&nbsp;</td>
			<td>Glycopyrrolate</td>
		</tr>
		<tr>
			<td>Hyoscine</td>
			<td>&nbsp;</td>
			<td>Scopolamine</td>
		</tr>
		<tr>
			<td>Indometacin</td>
			<td>Indomethacin</td>
			<td>Indomethacin</td>
		</tr>
		<tr>
			<td>Isoprenaline</td>
			<td>&nbsp;</td>
			<td>Isoproterenol</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Ispaghula</td>
			<td>Psyllium</td>
		</tr>
		<tr>
			<td>Levomepromazine</td>
			<td>Methotrimeprazine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Levothyroxine</td>
			<td>Thyroxine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Lidocaine</td>
			<td>Lignocaine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Liquid paraffin</td>
			<td>&nbsp;</td>
			<td>Mineral oil</td>
		</tr>
		<tr>
			<td>Meclozine</td>
			<td>&nbsp;</td>
			<td>Meclizine</td>
		</tr>
		<tr>
			<td>Methenamine hippurate</td>
			<td>Hexamine hippurate</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Mitoxantrone</td>
			<td>Mitozantrone</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Oxetacaine</td>
			<td>Oxethazine</td>
			<td>Oxethazine</td>
		</tr>
		<tr>
			<td>Paracetamol</td>
			<td>&nbsp;</td>
			<td>Acetaminophen</td>
		</tr>
		<tr>
			<td>Pethidine</td>
			<td>&nbsp;</td>
			<td>Meperidine</td>
		</tr>
		<tr>
			<td>Phenobarbital</td>
			<td>Phenobarbitone</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Phenoxymethylpenicillin</td>
			<td>&nbsp;</td>
			<td>Penicillin V</td>
		</tr>
		<tr>
			<td>Phytomenadione</td>
			<td>&nbsp;</td>
			<td>Phytonadione</td>
		</tr>
		<tr>
			<td>Procaine benzylpenicillin</td>
			<td>Procaine penicillin</td>
			<td>Procaine penicillin</td>
		</tr>
		<tr>
			<td>Retinol</td>
			<td>Vitamin A</td>
			<td>Vitamin A</td>
		</tr>
		<tr>
			<td>Rifampicin</td>
			<td>&nbsp;</td>
			<td>Rifampin</td>
		</tr>
		<tr>
			<td>Salbutamol</td>
			<td>&nbsp;</td>
			<td>Albuterol</td>
		</tr>
		<tr>
			<td>Simeticone<sup>a</sup></td>
			<td>Simethicone</td>
			<td>Simethicone</td>
		</tr>
		<tr>
			<td>Sodium cromoglicate</td>
			<td>Sodium cromoglycate</td>
			<td>Cromolyn sodium</td>
		</tr>
		<tr>
			<td>Sulfasalazine</td>
			<td>Sulphasalazine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Sulfathiazole</td>
			<td>Sulphathiazole</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Sulfonamides</td>
			<td>Sulphonamides</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tetracaine</td>
			<td>Amethocaine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Trihexyphenidyl</td>
			<td>Benzhexol</td>
			<td>Trihexyphenidyl</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td class="box_footnote" colspan="3">a. silica-activated dimeticone; known in some countries as activated dimethylpolysiloxane.</td>
		</tr>
	</tfoot>
</table>
<p>
&nbsp;
</p>
<p class="first">
<strong>Table 2</strong>&nbsp;&nbsp;UK names for compound preparations
</p>
<table border="1" cellpadding="2" cellspacing="0" frame="above" id="tbl" rules="groups" width="95%">
	<thead>
		<tr>
			<td>Contents</td>
			<td>UK name</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Amoxicillin-clavulanate</td>
			<td>Co-amoxiclav</td>
		</tr>
		<tr>
			<td>Diphenoxylate-atropine</td>
			<td>Co-phenotrope</td>
		</tr>
		<tr>
			<td>Magnesium hydroxide-aluminium<sup>a</sup> hydroxide
			</td>
			<td>Co-magaldrox</td>
		</tr>
		<tr>
			<td>Paracetamol<sup>b</sup>-codeine phosphate
			</td>
			<td>Co-codamol</td>
		</tr>
		<tr>
			<td>Paracetamol-dextropropoxyphene<sup>c</sup></td>
			<td>Co-proxamol</td>
		</tr>
		<tr>
			<td>Paracetamol-dihydrocodeine</td>
			<td>Co-dydramol</td>
		</tr>
		<tr>
			<td>Sulfamethoxazole-trimethoprim</td>
			<td>Co-trimoxazole</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td class="box_footnote" colspan="2">
				a. aluminum (USAN)<br />
				b. acetaminophen (USAN)<br />
				c. propoxyphene (USAN).<br />
			</td>
		</tr>
	</tfoot>
</table>

";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:1:"5";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"3";s:9:"createdon";s:10:"1204046257";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1217759038";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1204046257";s:11:"publishedby";s:1:"3";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h1> DRUG NAMES</h1>

<!--
<p class="pagedochead">
DRUG NAMES
</p>
<p>
&nbsp;
</p>
<hr width="100%" />
-->
<p class="first">
Following a European Union directive, all drugs marketed in Europe are now known by their recommended International Non-proprietary
Names (rINNs). Previously, in the UK, drugs were known by their British Approved Names (BANs). Differences between BANs and
rINNs are listed in Table 1.
</p>
<p class="indented">
Where United States Adopted Names (USANs) differ from rINNs and BANs, the USANs have also been included to aid understanding
of the US literature.
</p>
<p class="indented">
With compound preparations such as codeine and paracetamol (USAN acetaminophen) or diphenoxylate and atropine, the UK conventional
name is shown in Table 2, e.g. co-codamol or co-phenotrope.
</p>

<p class="first">
<strong>Table 1</strong>&nbsp;&nbsp;Drug names relevant to palliative care for which the rINN, BAN and/or USAN differ
</p>
<table border="1" cellpadding="2" cellspacing="0" frame="above" id="tbl" rules="groups" width="95%">
	<thead>
		<tr>
			<td>rINN</td>
			<td>BAN</td>
			<td>USAN</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Alimemazine</td>
			<td>Trimeprazine</td>
			<td>Trimeprazine</td>
		</tr>
		<tr>
			<td>Aluminium</td>
			<td>&nbsp;</td>
			<td>Aluminum</td>
		</tr>
		<tr>
			<td>Amfetamine</td>
			<td>&nbsp;</td>
			<td>Amphetamine</td>
		</tr>
		<tr>
			<td>Amobarbital</td>
			<td>Amylobarbitone</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Beclometasone</td>
			<td>Beclomethasone</td>
			<td>Beclomethasone</td>
		</tr>
		<tr>
			<td>Bendroflumethiazide</td>
			<td>Bendrofluazide</td>
			<td>Bendroflumethiazide</td>
		</tr>
		<tr>
			<td>Benorilate</td>
			<td>Benorylate</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Benzathine benzylpenicillin</td>
			<td>Benzathine penicillin</td>
			<td>Benzathine penicillin</td>
		</tr>
		<tr>
			<td>Benzatropine</td>
			<td>Benztropine</td>
			<td>Benztropine</td>
		</tr>
		<tr>
			<td>Benzylpenicillin</td>
			<td>&nbsp;</td>
			<td>Penicillin G</td>
		</tr>
		<tr>
			<td>Calcitonin (salmon)</td>
			<td>Salcatonin</td>
			<td>Calcitonin</td>
		</tr>
		<tr>
			<td>Carmellose</td>
			<td>&nbsp;</td>
			<td>Carboxymethylcellulose</td>
		</tr>
		<tr>
			<td>Cefalexin (etc.)</td>
			<td>Cephalexin (etc.)</td>
			<td>Cephalexin (etc.)</td>
		</tr>
		<tr>
			<td>Chlorphenamine</td>
			<td>Chlorpheniramine</td>
			<td>Chlorpheniramine</td>
		</tr>
		<tr>
			<td>Ciclosporin</td>
			<td>Cyclosporin</td>
			<td>Cyclosporine</td>
		</tr>
		<tr>
			<td>Clomethiazole</td>
			<td>Chlormethiazole</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Colestyramine</td>
			<td>Cholestyramine</td>
			<td>Cholestyramine</td>
		</tr>
		<tr>
			<td>Dantron</td>
			<td>Danthron</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Dexamfetamine</td>
			<td>Dexamphetamine</td>
			<td>Dextroamphetamine</td>
		</tr>
		<tr>
			<td>Dextropropoxyphene</td>
			<td>&nbsp;</td>
			<td>Propoxyphene</td>
		</tr>
		<tr>
			<td>Dicycloverine</td>
			<td>Dicyclomine</td>
			<td>Dicyclomine</td>
		</tr>
		<tr>
			<td>Dienestrol</td>
			<td>Dienoestrol</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Diethylstilbestrol</td>
			<td>Stilboestrol</td>
			<td>Diethylstilbestrol</td>
		</tr>
		<tr>
			<td>Dimeticone</td>
			<td>Dimethicone</td>
			<td>Dimethicone</td>
		</tr>
		<tr>
			<td>Dosulepin</td>
			<td>Dothiepin</td>
			<td>Dothiepin</td>
		</tr>
		<tr>
			<td>Estradiol</td>
			<td>Oestradiol</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Etamsylate</td>
			<td>Ethamsylate</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Furosemide</td>
			<td>Frusemide</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Glibenclamide</td>
			<td>&nbsp;</td>
			<td>Glyburide</td>
		</tr>
		<tr>
			<td>Glyceryl trinitrate</td>
			<td>&nbsp;</td>
			<td>Nitroglycerin</td>
		</tr>
		<tr>
			<td>Glycopyrronium</td>
			<td>&nbsp;</td>
			<td>Glycopyrrolate</td>
		</tr>
		<tr>
			<td>Hyoscine</td>
			<td>&nbsp;</td>
			<td>Scopolamine</td>
		</tr>
		<tr>
			<td>Indometacin</td>
			<td>Indomethacin</td>
			<td>Indomethacin</td>
		</tr>
		<tr>
			<td>Isoprenaline</td>
			<td>&nbsp;</td>
			<td>Isoproterenol</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Ispaghula</td>
			<td>Psyllium</td>
		</tr>
		<tr>
			<td>Levomepromazine</td>
			<td>Methotrimeprazine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Levothyroxine</td>
			<td>Thyroxine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Lidocaine</td>
			<td>Lignocaine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Liquid paraffin</td>
			<td>&nbsp;</td>
			<td>Mineral oil</td>
		</tr>
		<tr>
			<td>Meclozine</td>
			<td>&nbsp;</td>
			<td>Meclizine</td>
		</tr>
		<tr>
			<td>Methenamine hippurate</td>
			<td>Hexamine hippurate</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Mitoxantrone</td>
			<td>Mitozantrone</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Oxetacaine</td>
			<td>Oxethazine</td>
			<td>Oxethazine</td>
		</tr>
		<tr>
			<td>Paracetamol</td>
			<td>&nbsp;</td>
			<td>Acetaminophen</td>
		</tr>
		<tr>
			<td>Pethidine</td>
			<td>&nbsp;</td>
			<td>Meperidine</td>
		</tr>
		<tr>
			<td>Phenobarbital</td>
			<td>Phenobarbitone</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Phenoxymethylpenicillin</td>
			<td>&nbsp;</td>
			<td>Penicillin V</td>
		</tr>
		<tr>
			<td>Phytomenadione</td>
			<td>&nbsp;</td>
			<td>Phytonadione</td>
		</tr>
		<tr>
			<td>Procaine benzylpenicillin</td>
			<td>Procaine penicillin</td>
			<td>Procaine penicillin</td>
		</tr>
		<tr>
			<td>Retinol</td>
			<td>Vitamin A</td>
			<td>Vitamin A</td>
		</tr>
		<tr>
			<td>Rifampicin</td>
			<td>&nbsp;</td>
			<td>Rifampin</td>
		</tr>
		<tr>
			<td>Salbutamol</td>
			<td>&nbsp;</td>
			<td>Albuterol</td>
		</tr>
		<tr>
			<td>Simeticone<sup>a</sup></td>
			<td>Simethicone</td>
			<td>Simethicone</td>
		</tr>
		<tr>
			<td>Sodium cromoglicate</td>
			<td>Sodium cromoglycate</td>
			<td>Cromolyn sodium</td>
		</tr>
		<tr>
			<td>Sulfasalazine</td>
			<td>Sulphasalazine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Sulfathiazole</td>
			<td>Sulphathiazole</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Sulfonamides</td>
			<td>Sulphonamides</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tetracaine</td>
			<td>Amethocaine</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Trihexyphenidyl</td>
			<td>Benzhexol</td>
			<td>Trihexyphenidyl</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td class="box_footnote" colspan="3">a. silica-activated dimeticone; known in some countries as activated dimethylpolysiloxane.</td>
		</tr>
	</tfoot>
</table>
<p>
&nbsp;
</p>
<p class="first">
<strong>Table 2</strong>&nbsp;&nbsp;UK names for compound preparations
</p>
<table border="1" cellpadding="2" cellspacing="0" frame="above" id="tbl" rules="groups" width="95%">
	<thead>
		<tr>
			<td>Contents</td>
			<td>UK name</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Amoxicillin-clavulanate</td>
			<td>Co-amoxiclav</td>
		</tr>
		<tr>
			<td>Diphenoxylate-atropine</td>
			<td>Co-phenotrope</td>
		</tr>
		<tr>
			<td>Magnesium hydroxide-aluminium<sup>a</sup> hydroxide
			</td>
			<td>Co-magaldrox</td>
		</tr>
		<tr>
			<td>Paracetamol<sup>b</sup>-codeine phosphate
			</td>
			<td>Co-codamol</td>
		</tr>
		<tr>
			<td>Paracetamol-dextropropoxyphene<sup>c</sup></td>
			<td>Co-proxamol</td>
		</tr>
		<tr>
			<td>Paracetamol-dihydrocodeine</td>
			<td>Co-dydramol</td>
		</tr>
		<tr>
			<td>Sulfamethoxazole-trimethoprim</td>
			<td>Co-trimoxazole</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td class="box_footnote" colspan="2">
				a. aluminum (USAN)<br />
				b. acetaminophen (USAN)<br />
				c. propoxyphene (USAN).<br />
			</td>
		</tr>
	</tfoot>
</table>


			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
