<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"244";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:3:"FAQ";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:3:"faq";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:8841:"<h1>FREQUENTLY ASKED QUESTIONS</h1>

<h3>What is the best way to view the site?</h3>
<p>At a resolution of 1024x768, using Internet Explorer version 6 or above, Firefox version 1.5 or above, Opera version 8 or above.</p>
<br />
<h3>How do I change the text size?</h3>
<p>Use the text size icon on the top menu bar.</p>
<br />
<h3>How do I change registration details and preferences regarding bulletin board and other messages?</h3>
<p>Go to the home page of the site and login. 'Preferences' should appear at the left hand side of the screen. Select this and you should be presented with your registration details. Changes to e-mail addresses etc. can be made here.  Always click on 'update' to save any changes you make.</p>
<p>Towards the bottom of the list of your details, there is a YES/NO selection box, relating to the bulletin board messages ('Accept bulletin board digest e-mails'). If you wish to stop receiving these, select NO and then click on 'update' to save the change in details. Please note that because of the way messages are pre-approved and queued to be sent out, you may receive 1-2 messages after removing yourself from the list.</p>
<p>We may, from time to time, e-mail members with news of important updates to the site, publication of new palliativedrugs.com books, etc. If you wish to stop receiving these, there is a YES/NO selection box, relating to these messages ('Accept e-mails relating to site updates'). Select NO and then click on 'update' to save the change in details.</p>
<p>We may also send you e-mails containing an invitation to undertake market research. It is part of our terms and conditions of your use of the site that you agree to allow us to send you these. You are under no obligation to complete the market research. However, by doing so, you will be helping to keep palliativedrugs.com a free resource.</p>
<br />
<h3>How do I use the bulletin board?</h3>
<p>Go to the home page of the site and login. Select 'Bulletin board' from the top menu. Read the terms and conditions of use of the bulletin board and then click on the 'Click here to access the bulletin board' hyperlink.</p>

<h4>To start a new topic</h4>
<p>Click on the 'new topic' hyperlink at the top of the table. Please remember to search previous postings first to try and prevent the repetition of questions. This can be done by using the search facility hyperlink at the top of the table.</p>

<h4>To reply to a topic</h4>
<p>Select the topic you wish to reply to from the hyperlink in the table. This will bring up all the messages posted in response to that topic. Either click on the 'reply' hyperlink of the appropriate message that you wish to respond to, or scroll to the bottom of the message thread.</p>
<p>Your name, role, organisation and country will be automatically displayed. Complete the message and subject title (for new topics)</p>
<p>Submit your entry by using the 'post' button at the bottom of the screen. Please refrain from using abbreviations and use generic and recognised international names (rINNs) for drugs to aid understanding for an international membership.</p>
<p>Note. Your message will not be displayed until it has been screened by palliativedrugs.com staff. Once it has been accepted, it will be displayed on the bulletin board and e-mailed in a daily digest to registered users.</p>

<p><i>Please remember if you wish to respond to a bulletin board message go to the bulletin board facility on the site. We are not able to process messages that are sent as return e-mail.</i></p>

<h4>Why can't I reply to bulletin board messages directly using the e-mail reply button?</h4>
<p>The bulletin board was set up in this way so that we can screen messages and to prevent the distribution of viruses that can occur with an e-mail based system.</p>

<h4>Why don&rsquo;t you send out bulletin board messages individually anymore?</h4>
<p>With the success of the website and growing membership, each message was talking several hours to be sent out and backlogs developed frequently. Some members also found the number of messages too much. The decision was taken to send out one e-mail digest message generally once a day.</p>

<h4>I posted a message on the bulletin board but it has not appeared, why not?</h4>
<p>Your message will not be displayed until it has been screened by palliativedrugs.com staff, this will usually occur within 24h. Once it has been accepted, it will be displayed on the bulletin board and e-mailed in a digest to registered users. However, not all messages are approved; reasons for not posting include:</p>
<p>&bull; the topic has come up before with no apparent attempt to search the bulletin board<br />
&bull; the information is accessible in the formulary section of the website with no apparent attempt to access this<br />
&bull; blanket requests with no apparent attempt to answer their own question, e.g. "I am doing a project on pain, please help"<br />
&bull; use of trade rather than generic/recognised international names (rINNs) for drugs or unfamiliar abbreviations<br />
&bull; personal messages.<br /></p>

<p>At the discretion of palliativedrugs.com staff, a posting may be allowed on the board, but will not be circulated.</p>

<h4>I don&rsquo;t wish to receive the bulletin board messages, is there an alternative?</h4>
<p>Yes, you can remove yourself from the e-mail circulation list easily by changing your preferences (see 'How do I change registration details and preferences regarding bulletin board messages?' above). You can still view all bulletin board messages and replies by going to the bulletin board itself and browsing through the message titles, expanding where necessary to read all the replies to a particular topic. </p>

<h4>Is there a way to suspend bulletin board e-mail messages whilst on holiday?</h4>
<p>Yes, you can remove yourself from the e-mail circulation list and then rejoin easily by changing your preferences as above. Please note that because of the way messages are pre-approved and queued to be sent out, you may receive 1-2 messages after removing yourself from the list.</p>

<h4>I am receiving duplicate e-mail messages?</h4>
<p>This may be because you have registered more than once. Either delete one registration from receiving e-mails or contact us at <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>

<br />
<h3>Although signed up for bulletin board digests and other messages, I am not receiving them, or have suddenly stopped receiving them. Why?</h3>

<p>Your e-mail service, IT department and Internet Service Provider (ISP) all work hard to protect you from spam, but this can sometimes mean that e-mails you actually want do not get through. If you are not getting the bulletin board digests (generally sent out several times a week, daily when busy) try the following:</p>

<h4>Your e-mail software</h4>
<p>&bull; add bulletinboard@palliativedrugs.com and www.palliativedrugs.com to your safe sender list<br />
&bull; add bulletinboard@palliativedrugs.com to your address book (Hotmail and gmail users)<br /></p>

<h4>Your anti-spam software</h4>
<p>&bull; add bulletinboard@palliativedrugs.com and www.palliativedrugs.com to your safe sender list of your anti-spam software<br /></p>

<h4>Your IT department or ISP</h4> 
<p>&bull; if the above fails, your IT department or ISP may be blocking our e-mail<br />
&bull; ask your IT department and ISP to allow e-mails to come through to you from bulletinboard@palliativedrugs.com and from our IP address 46.29.88.154<br />
&bull; this is called "whitelisting"; if you aren't getting the e-mail, many others could be missing out too, so it is well worth the effort.<br /></p>

<br />
<h3>How do I access the old newsletters?</h3>
<p>Newsletters, produced between November 2000 and April/May 2008, are accessible via a link on the news page.</p>

<br />
<h3>Is there a place where all the guidelines are collated for download?</h3>
<p>The documents previously featured in the newsletters and RAG panel are now available to download from the Document library. Please continue to send your guidelines, audit proformas, patient group directives etc. for sharing via the website, to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>, preferably in word doc or pdf format.</p>

<br />
<h3>Can I print off sections from the site?</h3>
<p>The website is &copy;palliativedrugs.com Ltd. Please see the intellectual property rights section in the <a href="terms-and-conditions">Terms and conditions</a> for details regarding the reproduction of any of its content. If you would like a paper copy, please buy the PCF from our online bookshop. By doing so, you will be helping to keep palliativedrugs.com a free-access resource.</p>

";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:1:"9";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1209752705";s:8:"editedby";s:1:"3";s:8:"editedon";s:10:"1322834458";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1209752705";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h1>FREQUENTLY ASKED QUESTIONS</h1>

<h3>What is the best way to view the site?</h3>
<p>At a resolution of 1024x768, using Internet Explorer version 6 or above, Firefox version 1.5 or above, Opera version 8 or above.</p>
<br />
<h3>How do I change the text size?</h3>
<p>Use the text size icon on the top menu bar.</p>
<br />
<h3>How do I change registration details and preferences regarding bulletin board and other messages?</h3>
<p>Go to the home page of the site and login. 'Preferences' should appear at the left hand side of the screen. Select this and you should be presented with your registration details. Changes to e-mail addresses etc. can be made here.  Always click on 'update' to save any changes you make.</p>
<p>Towards the bottom of the list of your details, there is a YES/NO selection box, relating to the bulletin board messages ('Accept bulletin board digest e-mails'). If you wish to stop receiving these, select NO and then click on 'update' to save the change in details. Please note that because of the way messages are pre-approved and queued to be sent out, you may receive 1-2 messages after removing yourself from the list.</p>
<p>We may, from time to time, e-mail members with news of important updates to the site, publication of new palliativedrugs.com books, etc. If you wish to stop receiving these, there is a YES/NO selection box, relating to these messages ('Accept e-mails relating to site updates'). Select NO and then click on 'update' to save the change in details.</p>
<p>We may also send you e-mails containing an invitation to undertake market research. It is part of our terms and conditions of your use of the site that you agree to allow us to send you these. You are under no obligation to complete the market research. However, by doing so, you will be helping to keep palliativedrugs.com a free resource.</p>
<br />
<h3>How do I use the bulletin board?</h3>
<p>Go to the home page of the site and login. Select 'Bulletin board' from the top menu. Read the terms and conditions of use of the bulletin board and then click on the 'Click here to access the bulletin board' hyperlink.</p>

<h4>To start a new topic</h4>
<p>Click on the 'new topic' hyperlink at the top of the table. Please remember to search previous postings first to try and prevent the repetition of questions. This can be done by using the search facility hyperlink at the top of the table.</p>

<h4>To reply to a topic</h4>
<p>Select the topic you wish to reply to from the hyperlink in the table. This will bring up all the messages posted in response to that topic. Either click on the 'reply' hyperlink of the appropriate message that you wish to respond to, or scroll to the bottom of the message thread.</p>
<p>Your name, role, organisation and country will be automatically displayed. Complete the message and subject title (for new topics)</p>
<p>Submit your entry by using the 'post' button at the bottom of the screen. Please refrain from using abbreviations and use generic and recognised international names (rINNs) for drugs to aid understanding for an international membership.</p>
<p>Note. Your message will not be displayed until it has been screened by palliativedrugs.com staff. Once it has been accepted, it will be displayed on the bulletin board and e-mailed in a daily digest to registered users.</p>

<p><i>Please remember if you wish to respond to a bulletin board message go to the bulletin board facility on the site. We are not able to process messages that are sent as return e-mail.</i></p>

<h4>Why can't I reply to bulletin board messages directly using the e-mail reply button?</h4>
<p>The bulletin board was set up in this way so that we can screen messages and to prevent the distribution of viruses that can occur with an e-mail based system.</p>

<h4>Why don&rsquo;t you send out bulletin board messages individually anymore?</h4>
<p>With the success of the website and growing membership, each message was talking several hours to be sent out and backlogs developed frequently. Some members also found the number of messages too much. The decision was taken to send out one e-mail digest message generally once a day.</p>

<h4>I posted a message on the bulletin board but it has not appeared, why not?</h4>
<p>Your message will not be displayed until it has been screened by palliativedrugs.com staff, this will usually occur within 24h. Once it has been accepted, it will be displayed on the bulletin board and e-mailed in a digest to registered users. However, not all messages are approved; reasons for not posting include:</p>
<p>&bull; the topic has come up before with no apparent attempt to search the bulletin board<br />
&bull; the information is accessible in the formulary section of the website with no apparent attempt to access this<br />
&bull; blanket requests with no apparent attempt to answer their own question, e.g. "I am doing a project on pain, please help"<br />
&bull; use of trade rather than generic/recognised international names (rINNs) for drugs or unfamiliar abbreviations<br />
&bull; personal messages.<br /></p>

<p>At the discretion of palliativedrugs.com staff, a posting may be allowed on the board, but will not be circulated.</p>

<h4>I don&rsquo;t wish to receive the bulletin board messages, is there an alternative?</h4>
<p>Yes, you can remove yourself from the e-mail circulation list easily by changing your preferences (see 'How do I change registration details and preferences regarding bulletin board messages?' above). You can still view all bulletin board messages and replies by going to the bulletin board itself and browsing through the message titles, expanding where necessary to read all the replies to a particular topic. </p>

<h4>Is there a way to suspend bulletin board e-mail messages whilst on holiday?</h4>
<p>Yes, you can remove yourself from the e-mail circulation list and then rejoin easily by changing your preferences as above. Please note that because of the way messages are pre-approved and queued to be sent out, you may receive 1-2 messages after removing yourself from the list.</p>

<h4>I am receiving duplicate e-mail messages?</h4>
<p>This may be because you have registered more than once. Either delete one registration from receiving e-mails or contact us at <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>

<br />
<h3>Although signed up for bulletin board digests and other messages, I am not receiving them, or have suddenly stopped receiving them. Why?</h3>

<p>Your e-mail service, IT department and Internet Service Provider (ISP) all work hard to protect you from spam, but this can sometimes mean that e-mails you actually want do not get through. If you are not getting the bulletin board digests (generally sent out several times a week, daily when busy) try the following:</p>

<h4>Your e-mail software</h4>
<p>&bull; add bulletinboard@palliativedrugs.com and www.palliativedrugs.com to your safe sender list<br />
&bull; add bulletinboard@palliativedrugs.com to your address book (Hotmail and gmail users)<br /></p>

<h4>Your anti-spam software</h4>
<p>&bull; add bulletinboard@palliativedrugs.com and www.palliativedrugs.com to your safe sender list of your anti-spam software<br /></p>

<h4>Your IT department or ISP</h4> 
<p>&bull; if the above fails, your IT department or ISP may be blocking our e-mail<br />
&bull; ask your IT department and ISP to allow e-mails to come through to you from bulletinboard@palliativedrugs.com and from our IP address 46.29.88.154<br />
&bull; this is called "whitelisting"; if you aren't getting the e-mail, many others could be missing out too, so it is well worth the effort.<br /></p>

<br />
<h3>How do I access the old newsletters?</h3>
<p>Newsletters, produced between November 2000 and April/May 2008, are accessible via a link on the news page.</p>

<br />
<h3>Is there a place where all the guidelines are collated for download?</h3>
<p>The documents previously featured in the newsletters and RAG panel are now available to download from the Document library. Please continue to send your guidelines, audit proformas, patient group directives etc. for sharing via the website, to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>, preferably in word doc or pdf format.</p>

<br />
<h3>Can I print off sections from the site?</h3>
<p>The website is &copy;palliativedrugs.com Ltd. Please see the intellectual property rights section in the <a href="terms-and-conditions">Terms and conditions</a> for details regarding the reproduction of any of its content. If you would like a paper copy, please buy the PCF from our online bookshop. By doing so, you will be helping to keep palliativedrugs.com a free-access resource.</p>


			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
