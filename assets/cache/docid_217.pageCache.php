<?php die('Unauthorized access.'); ?>a:38:{s:2:"id";s:3:"217";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:28:"Getting the most out of PCF3";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:28:"getting-the-most-out-of-PCF3";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:13764:"<table border="0" cellpadding="0" cellspacing="0" width="98%">
	<tbody>
		<tr>
			<td align="left" class="ttl">GETTING THE MOST OUT OF PCF3</td>
		</tr>
	</tbody>
</table>
<hr width="100%" />
<!--
<p class="pagedochead">
GETTING THE MOST OUT OF PCF3
</p>
-->
<p>
<img height="27" src="c:/pcf-usa/symbols/space.gif" width="1" /> 
</p>
<p class="first">
The literature on the pharmacology of pain and symptom management in end-stage disease is growing continually, and it is impossible for anyone to be familiar with all of it. This is where a book like <em>PCF3</em> comes into its own as a major accessible resource for prescribing clinicians involved in palliative care. On the other hand, <em>PCF3</em> is not an easy read, indeed it was never intended that it would be read from cover to cover. It is essentially a reference book &ndash; to study the monograph of an individual drug, or class of drugs, with fairly specific questions in mind. 
</p>
<p class="indented">
The main sections in Part 1 of <em>PCF3</em> generally follow the systematic order of the <em>British National Formulary</em> (<em>BNF</em>). Part 2 and the appendices deal with themes that transcend the drug monographs, e.g. important drug interactions, the use of nebulized drugs, named patient suppliers. Drugs marked with an asterisk (*) should generally be used only by, or after consultation with, a specialist palliative care service. Drug prices are net prices based on those in the <em>BNF</em> No. 52 (September 2006). 
</p>
<p class="indented">
<em>PCF3</em> does not replace the <em>BNF</em> or books on pain and symptom management; it is for use alongside them. <em>Symptom Management in Advanced Cancer</em> (Twycross, Wilcock and Toller, 4<sup>th</sup> edition (in preparation), palliativedrugs.com Ltd, Nottingham) should be seen as the companion book to <em>PCF3</em>. 
</p>
<h1>Contra-indications and cautions</h1>
<p class="first">
Contra-indications and cautions listed in Summaries of Product Characteristics (SPCs) can vary between different manufacturers of the same drug, or within a class of drugs. We have generally <em>not</em> included a contra-indication from the SPC if the use of the drug in the stated circumstance is accepted prescribing practice in palliative care. 
</p>
<p class="indented">
Instead, we advise a more cautious approach in some patient groups, e.g. the frail elderly, patients with hepatic impairment, renal impairment, and respiratory insufficiency. The contra-indications listed in PCF3 are thus limited to the most relevant and specific for a particular drug. For a full list of the manufacturer's contra-indications and cautions, readers should refer to a drug's SPC. 
</p>
<h1>Undesirable effects of drugs</h1>
<p class="first">
In <em>PCF3</em>, the term &lsquo;undesirable effect&rsquo; is used rather than side effect or adverse drug reaction, as recommended by the European Commission. Undesirable effects are categorized as: 
</p>
<ul>
	<li>very common (&gt;10%)</li>
	<li>common (&lt;10%, &gt;1%)</li>
	<li>uncommon (&lt;1%, &gt;0.1%)</li>
	<li>rare (&lt;0.1%, &gt;0.01%)</li>
	<li>very rare (&lt;0.01%).</li>
</ul>
<p class="first">
However, as yet, all SPCs are not compiled in this way. 
</p>
<p class="indented">
Generally, <em>PCF3</em> includes information on the very common and common undesirable effects. Selected other undesirable effects are also included, e.g. uncommon or rare ones which may have serious consequences. The manufacturer's SPC should be consulted for a full list of undesirable effects. 
</p>
<h1>Reliable knowledge and levels of evidence</h1>
<p class="first">
Research is the pursuit of reliable knowledge. The randomized controlled trial (RCT) is <em>not</em> the only source of reliable knowledge, and various levels of evidence have been categorized.<sup>1</sup> A modified system is used by NICE to indicate the level of evidence on which recommendations are based Box A.<sup>2</sup> 
</p>
<p class="indented">
Broadly speaking, there are several sources of knowledge, which can be conveniently grouped under three headings: 
</p>
<ul>
	<li><em>instrumental</em>, includes RCT data and data from other high-quality studies </li>
	<li><em>interactive</em>, refers to anecdotal data (shared clinical experience), including retrospective and prospective surveys </li>
	<li><em>critical</em>, data unique to the individual in question (e.g. personal choice) and societal/cultural factors (e.g. financial and logistic considerations).<sup>3</sup></li>
</ul>
<p class="first">
Relying on one type of knowledge alone is <em>not</em> good practice. All three sources must be exploited in the process of therapeutic decision-making. 
</p>
<ul class="code" id="tbx">
	<p class="boxlbl">
	<strong>Box A</strong>&nbsp;&nbsp;Hierarchy of evidence and recommendations grading scheme<sup>2,4,5</sup> 
	</p>
	<table>
		<tbody>
			<tr>
				<td><strong>Level</strong></td>
				<td><strong>Type of evidence</strong></td>
				<td><strong>Grade</strong></td>
				<td><strong>Evidence</strong></td>
			</tr>
			<tr>
				<td valign="top">I</td>
				<td valign="top">Evidence obtained from a single randomized controlled trial or a meta-analysis of randomized controlled trials<br />
				</td>
				<td valign="top">A</td>
				<td valign="top">At least one randomized controlled trial as part of a body of literature of overall good quality and consistency addressing the specific recommendation (evidence level I) without extrapolation<br />
				<br />
				</td>
			</tr>
			<tr>
				<td valign="top">IIa</td>
				<td valign="top">Evidence obtained from atleast one well-designed controlled study without randomization<br />
				</td>
				<td valign="top">B</td>
				<td>Well-conducted clinical studies but no randomized clinical trials on the topic of recommendation (evidence levels II or III); or extrapolated from level I evidence<br />
				</td>
			</tr>
			<tr>
				<td valign="top">IIb</td>
				<td>Evidence obtained from at least one other well-designed quasi-experimental study<br />
				<br />
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">III</td>
				<td>Evidence obtained from well-designed non-experimental descriptive studies, such as comparative studies, correlation studies and case studies<br />
				<br />
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">IV</td>
				<td valign="top">Evidence obtained from expert committee reports or opinions and/or clinical experiences of respected authorities</td>
				<td valign="top">C</td>
				<td>Expert committee reports oropinions and/or clinical experiences of respected authorities (evidence level IV). This grading indicates that directly applicable clinical studies of good quality are absent or not readily available<br />
				<br />
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td valign="top">Good Practice Point (GPP)<br />
				<br />
				</td>
				<td valign="top">Recommended good practice based on the clinical experienceof the Guidelines Development Group (GDG)<br />
				<br />
				</td>
			</tr>
			<tr>
				<td valign="top">NICE</td>
				<td valign="top">Evidence from NICE guideline or technology appraisal</td>
				<td valign="top">NICE</td>
				<td valign="top">Evidence from NICE guideline or technology appraisal</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
		</tbody>
	</table>
</ul>

<h1>Pharmaceutical company information</h1>
<p class="first">
The manufacturer's SPC is an important source of information about a drug in specific formulations. However, many published studies are sponsored by industry. This can lead to a conflict between a desire to provide objective data and the desire for a company to make its own drug as attractive as possible.<sup>6</sup> It is thus sensible to regard information from company representatives as inevitably biased. We would stress that the information provided by <em>PCF3</em> is commercially independent, and should serve as a counterbalance to manufacturer bias. 
</p>
<p class="indented">
Remember: it is often safer to stick with an &lsquo;old favourite&rsquo; rather than seek to be one of the first to prescribe a newly marketed drug. Most new drugs today are &lsquo;me-too&rsquo; drugs rather than true innovations.<sup>6</sup> 
</p>
<h1>Generic drugs</h1>
<p class="first">
It is the policy of <em>PCF3</em> to use generic drug names, and to encourage generic prescribing. With occasional exceptions, e.g. for m/r formulations of diltiazem, nifedipine and theophylline, there is little reliable evidence that different preparations of the same drug are significantly different in terms of bio-availability and efficacy.<sup>7</sup> However, the Department of Health (London) recommends including the brand name of opioid analgesics on the prescription and dispensing label, particularly for oral morphine preparations, to avoid confusion over the various strengths and formulations available.<sup>8</sup> 
</p>
<h1>Literature references</h1>
<p class="first">
In choosing references, articles in hospice and palliative care journals have frequently been selected preferentially. Such journals are likely to be more readily available to our readers, and often contain detailed discussion. 
</p>
<p class="indented">
It is clearly not feasible to reference every statement in <em>PCF3</em>. However, readers are invited to enter into constructive dialogue with the Editorial Team via the <em>Bulletin Board</em> on www.palliativedrugs.com. This is currently accessed by thousands of&nbsp;health professionals. 
</p>
<h1>Electronic sources of information</h1>
<p class="first">
Several of the sources cited in <em>PCF3</em> can be accessed free online by UK users. To facilitate access to the relevant documents, website details are given below. <br />
<br />
Bandolier (evidence-based articles for health professionals): available from <br />
<a href="http://www.jr2.ox.ac.uk/bandolier/"target="_blank">www.jr2.ox.ac.uk/bandolier/</a> <br />
<br />
<em>British National Formulary</em>: two editions/year, March and September. Latest edition available from <br />
<a href="http://www.bnf.org.uk/bnf/"target="_blank">www.bnf.org.uk/bnf/</a> <br />
Free registration required. <br />
<br />
Current Problems in Pharmacovigilance: available via MHRA website at <br />
<a href="http://www.mhra.gov.uk/home/idcplg?IdcService=SS_GET_PAGE&amp;nodeId=368"target="_blank">www.mhra.gov.uk/home/idcplg?IdcService=SS_GET_PAGE&amp;nodeId=368</a> <br />
<br />
MeReC Bulletin: available via National Prescribing Centre website at <br />
<a href="http://www.npc.co.uk/merec_bulletins.htm"target="_blank">www.npc.co.uk/merec_bulletins.htm</a> <br />
<br />
<em>Pharmaceutical Journal</em> (official weekly journal of the Royal Pharmaceutical Society of Great Britain): available from <br />
<a href="http://www.pjonline.com"target="_blank">www.pjonline.com</a> <br />
Site also gives access to Hospital Pharmacist (London). <br />
<br />
UK manufacturers' SPCs: available from <br />
<a href="http://www.medicines.org.uk"target="_blank">www.medicines.org.uk</a> <br />
<br />
The Cochrane Library: available from <br />
<a href="http://www3.interscience.wiley.com/cgi-bin/mrwhome/106568753/HOME"target="_blank">www3.interscience.wiley.com/cgi-bin/mrwhome/106568753/HOME</a> <br />
Collection of evidence-based systematic reviews. <br />
A subscription is required in certain other countries. <br />
<br />
Various other sources and full-text core journals (e.g. the <em>British Medical Journal</em> and the <em>Lancet</em>) are available free to UK NHS staff with an Athens password through the National Library for Health (NLH) at <br />
<a href="http://www.library.nhs.uk/Default.aspx"target="_blank">www.library.nhs.uk/Default.aspx</a> 
</p>
<div class="ref">
<hr width="100%" />
<ol start="1" type="1">
	<li>Agency for Health Care Policy and Research (1992) Acute pain management, operative or medical procedures and trauma 92-0032. In: <em>Clinical Practice Guideline Quick Reference Guide for Clinicians</em>. AHCPR Publications, Rockville, Maryland, USA, pp. 1&ndash;22. </li>
	<li>NICE (2004) <em>Depression: Management of depression in primary and secondary care</em>. In: National Clinical Practice Guideline Number 23. National Institute for Clinical Excellence. Available from: <a href="http://www.nice.org.uk/page.aspx?o=236667"target="_blank">www.nice.org.uk/page.aspx?o=236667</a></li>
	<li>Aoun SM and Kristjanson LJ (2005) Challenging the framework for evidence in palliative care research. <em>Palliative Medicine</em>. <strong>19</strong>: 461&ndash;465. </li>
	<li>DoH (1996) <em>Clinical Guidelines: Using Clinical Guidelines to Improve Patient Care Within the NHS</em>. Department of Health: NHS Executive, Leeds. </li>
	<li>Eccles M and Mason J (2001) How to develop cost-conscious guidelines. <em>Health Technology Assessment</em>. <strong>5 (16)</strong>. </li>
	<li>Angell M (2004) <em>The Truth About the Drug Companies: how they deceive us and what to do about it</em>. Random House, New York. </li>
	<li>National Prescribing Centre (2000) Modified-release preparations. <em>MeReC Bulletin</em>. <strong>11</strong>: 13&ndash;16. </li>
	<li>Smith J (2004) <em>Building a Safer NHS for Patients - Improving Medication Safety</em>. Department of Health, London, pp. 105&ndash;111. Available from: <a href="http://www.dh.gov.uk/assetRoot/04/08/49/61/04084961.pdf"target="_blank">www.dh.gov.uk/assetRoot/04/08/49/61/04084961.pdf</a></li>
</ol>
</div>";s:8:"richtext";s:1:"0";s:8:"template";s:2:"21";s:9:"menuindex";s:1:"3";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"3";s:9:"createdon";s:10:"1204045831";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1217947043";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1204045831";s:11:"publishedby";s:1:"3";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>&copy; palliativedrugs.com</title>
<link type="text/css" media="screen" rel="stylesheet" href="/css/template_styles.css" />

<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<link type="text/css" media="screen" rel="stylesheet" href="/css/SimpleTreeView.css">
<link type="text/css" media="screen" rel="stylesheet" href="/mk/style.css" />
<!-- putting this here overwrites CSS default width for right column -->
<style>
	#contentcolumn {
		margin: 0 15 0 230px; /*Margins should be "0 0 0 LeftColumnWidth*/
	}

       #contentcolumn {
		line-height: 140%;
       }

/* TEMP - TO BE MOVED !! */
blockquote {
    margin-top: 18px;
}

/*sup {
    font-size:0.9em; 
    position:relative; 
    top:0.2em; 
    left:0;
}

sub {
    font-size:0.9em; 
    position:relative; 
    top: -0.1em; 
    left:0;
}*/

p {
    line-height: 1.6;
}

li.secord {
	list-style-image: url('/assets/templates/pall/triangleright.gif');
	margin-left:18px;
}
div.nicetitle p {color: #fff;}
</style>
<link type="text/css" media="screen" rel="stylesheet" href="/css/formulary.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/formulary-medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/formulary-large.css" />
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="Javascript">
function HideLeft() {
	var l = document.getElementById('leftcolumn');
	var m = document.getElementById('contentcolumn');
	var g = document.getElementById('hideleft');
	if(l.style.display=='none') {
		//show...
		l.style.display='block';
		m.style.margin='20px 15px 10px 230px';
		g.value='hide';
	} else {
		//hide
		l.style.display='none';
		m.style.margin='20px 15px 10px 15px';
		g.value='show';
	}
}
</script>
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
</head>
<body>
	<div id="container">
	 [!header!]
  	[!nav!]

	<div id="contentwrapper">
		<div class="tr"><div class="bl"><div class="br"><div id="contentcolumn">
			<table border="0" cellpadding="0" cellspacing="0" width="98%">
	<tbody>
		<tr>
			<td align="left" class="ttl">GETTING THE MOST OUT OF PCF3</td>
		</tr>
	</tbody>
</table>
<hr width="100%" />
<!--
<p class="pagedochead">
GETTING THE MOST OUT OF PCF3
</p>
-->
<p>
<img height="27" src="c:/pcf-usa/symbols/space.gif" width="1" /> 
</p>
<p class="first">
The literature on the pharmacology of pain and symptom management in end-stage disease is growing continually, and it is impossible for anyone to be familiar with all of it. This is where a book like <em>PCF3</em> comes into its own as a major accessible resource for prescribing clinicians involved in palliative care. On the other hand, <em>PCF3</em> is not an easy read, indeed it was never intended that it would be read from cover to cover. It is essentially a reference book &ndash; to study the monograph of an individual drug, or class of drugs, with fairly specific questions in mind. 
</p>
<p class="indented">
The main sections in Part 1 of <em>PCF3</em> generally follow the systematic order of the <em>British National Formulary</em> (<em>BNF</em>). Part 2 and the appendices deal with themes that transcend the drug monographs, e.g. important drug interactions, the use of nebulized drugs, named patient suppliers. Drugs marked with an asterisk (*) should generally be used only by, or after consultation with, a specialist palliative care service. Drug prices are net prices based on those in the <em>BNF</em> No. 52 (September 2006). 
</p>
<p class="indented">
<em>PCF3</em> does not replace the <em>BNF</em> or books on pain and symptom management; it is for use alongside them. <em>Symptom Management in Advanced Cancer</em> (Twycross, Wilcock and Toller, 4<sup>th</sup> edition (in preparation), palliativedrugs.com Ltd, Nottingham) should be seen as the companion book to <em>PCF3</em>. 
</p>
<h1>Contra-indications and cautions</h1>
<p class="first">
Contra-indications and cautions listed in Summaries of Product Characteristics (SPCs) can vary between different manufacturers of the same drug, or within a class of drugs. We have generally <em>not</em> included a contra-indication from the SPC if the use of the drug in the stated circumstance is accepted prescribing practice in palliative care. 
</p>
<p class="indented">
Instead, we advise a more cautious approach in some patient groups, e.g. the frail elderly, patients with hepatic impairment, renal impairment, and respiratory insufficiency. The contra-indications listed in PCF3 are thus limited to the most relevant and specific for a particular drug. For a full list of the manufacturer's contra-indications and cautions, readers should refer to a drug's SPC. 
</p>
<h1>Undesirable effects of drugs</h1>
<p class="first">
In <em>PCF3</em>, the term &lsquo;undesirable effect&rsquo; is used rather than side effect or adverse drug reaction, as recommended by the European Commission. Undesirable effects are categorized as: 
</p>
<ul>
	<li>very common (&gt;10%)</li>
	<li>common (&lt;10%, &gt;1%)</li>
	<li>uncommon (&lt;1%, &gt;0.1%)</li>
	<li>rare (&lt;0.1%, &gt;0.01%)</li>
	<li>very rare (&lt;0.01%).</li>
</ul>
<p class="first">
However, as yet, all SPCs are not compiled in this way. 
</p>
<p class="indented">
Generally, <em>PCF3</em> includes information on the very common and common undesirable effects. Selected other undesirable effects are also included, e.g. uncommon or rare ones which may have serious consequences. The manufacturer's SPC should be consulted for a full list of undesirable effects. 
</p>
<h1>Reliable knowledge and levels of evidence</h1>
<p class="first">
Research is the pursuit of reliable knowledge. The randomized controlled trial (RCT) is <em>not</em> the only source of reliable knowledge, and various levels of evidence have been categorized.<sup>1</sup> A modified system is used by NICE to indicate the level of evidence on which recommendations are based Box A.<sup>2</sup> 
</p>
<p class="indented">
Broadly speaking, there are several sources of knowledge, which can be conveniently grouped under three headings: 
</p>
<ul>
	<li><em>instrumental</em>, includes RCT data and data from other high-quality studies </li>
	<li><em>interactive</em>, refers to anecdotal data (shared clinical experience), including retrospective and prospective surveys </li>
	<li><em>critical</em>, data unique to the individual in question (e.g. personal choice) and societal/cultural factors (e.g. financial and logistic considerations).<sup>3</sup></li>
</ul>
<p class="first">
Relying on one type of knowledge alone is <em>not</em> good practice. All three sources must be exploited in the process of therapeutic decision-making. 
</p>
<ul class="code" id="tbx">
	<p class="boxlbl">
	<strong>Box A</strong>&nbsp;&nbsp;Hierarchy of evidence and recommendations grading scheme<sup>2,4,5</sup> 
	</p>
	<table>
		<tbody>
			<tr>
				<td><strong>Level</strong></td>
				<td><strong>Type of evidence</strong></td>
				<td><strong>Grade</strong></td>
				<td><strong>Evidence</strong></td>
			</tr>
			<tr>
				<td valign="top">I</td>
				<td valign="top">Evidence obtained from a single randomized controlled trial or a meta-analysis of randomized controlled trials<br />
				</td>
				<td valign="top">A</td>
				<td valign="top">At least one randomized controlled trial as part of a body of literature of overall good quality and consistency addressing the specific recommendation (evidence level I) without extrapolation<br />
				<br />
				</td>
			</tr>
			<tr>
				<td valign="top">IIa</td>
				<td valign="top">Evidence obtained from atleast one well-designed controlled study without randomization<br />
				</td>
				<td valign="top">B</td>
				<td>Well-conducted clinical studies but no randomized clinical trials on the topic of recommendation (evidence levels II or III); or extrapolated from level I evidence<br />
				</td>
			</tr>
			<tr>
				<td valign="top">IIb</td>
				<td>Evidence obtained from at least one other well-designed quasi-experimental study<br />
				<br />
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">III</td>
				<td>Evidence obtained from well-designed non-experimental descriptive studies, such as comparative studies, correlation studies and case studies<br />
				<br />
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">IV</td>
				<td valign="top">Evidence obtained from expert committee reports or opinions and/or clinical experiences of respected authorities</td>
				<td valign="top">C</td>
				<td>Expert committee reports oropinions and/or clinical experiences of respected authorities (evidence level IV). This grading indicates that directly applicable clinical studies of good quality are absent or not readily available<br />
				<br />
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td valign="top">Good Practice Point (GPP)<br />
				<br />
				</td>
				<td valign="top">Recommended good practice based on the clinical experienceof the Guidelines Development Group (GDG)<br />
				<br />
				</td>
			</tr>
			<tr>
				<td valign="top">NICE</td>
				<td valign="top">Evidence from NICE guideline or technology appraisal</td>
				<td valign="top">NICE</td>
				<td valign="top">Evidence from NICE guideline or technology appraisal</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
		</tbody>
	</table>
</ul>

<h1>Pharmaceutical company information</h1>
<p class="first">
The manufacturer's SPC is an important source of information about a drug in specific formulations. However, many published studies are sponsored by industry. This can lead to a conflict between a desire to provide objective data and the desire for a company to make its own drug as attractive as possible.<sup>6</sup> It is thus sensible to regard information from company representatives as inevitably biased. We would stress that the information provided by <em>PCF3</em> is commercially independent, and should serve as a counterbalance to manufacturer bias. 
</p>
<p class="indented">
Remember: it is often safer to stick with an &lsquo;old favourite&rsquo; rather than seek to be one of the first to prescribe a newly marketed drug. Most new drugs today are &lsquo;me-too&rsquo; drugs rather than true innovations.<sup>6</sup> 
</p>
<h1>Generic drugs</h1>
<p class="first">
It is the policy of <em>PCF3</em> to use generic drug names, and to encourage generic prescribing. With occasional exceptions, e.g. for m/r formulations of diltiazem, nifedipine and theophylline, there is little reliable evidence that different preparations of the same drug are significantly different in terms of bio-availability and efficacy.<sup>7</sup> However, the Department of Health (London) recommends including the brand name of opioid analgesics on the prescription and dispensing label, particularly for oral morphine preparations, to avoid confusion over the various strengths and formulations available.<sup>8</sup> 
</p>
<h1>Literature references</h1>
<p class="first">
In choosing references, articles in hospice and palliative care journals have frequently been selected preferentially. Such journals are likely to be more readily available to our readers, and often contain detailed discussion. 
</p>
<p class="indented">
It is clearly not feasible to reference every statement in <em>PCF3</em>. However, readers are invited to enter into constructive dialogue with the Editorial Team via the <em>Bulletin Board</em> on www.palliativedrugs.com. This is currently accessed by thousands of&nbsp;health professionals. 
</p>
<h1>Electronic sources of information</h1>
<p class="first">
Several of the sources cited in <em>PCF3</em> can be accessed free online by UK users. To facilitate access to the relevant documents, website details are given below. <br />
<br />
Bandolier (evidence-based articles for health professionals): available from <br />
<a href="http://www.jr2.ox.ac.uk/bandolier/"target="_blank">www.jr2.ox.ac.uk/bandolier/</a> <br />
<br />
<em>British National Formulary</em>: two editions/year, March and September. Latest edition available from <br />
<a href="http://www.bnf.org.uk/bnf/"target="_blank">www.bnf.org.uk/bnf/</a> <br />
Free registration required. <br />
<br />
Current Problems in Pharmacovigilance: available via MHRA website at <br />
<a href="http://www.mhra.gov.uk/home/idcplg?IdcService=SS_GET_PAGE&amp;nodeId=368"target="_blank">www.mhra.gov.uk/home/idcplg?IdcService=SS_GET_PAGE&amp;nodeId=368</a> <br />
<br />
MeReC Bulletin: available via National Prescribing Centre website at <br />
<a href="http://www.npc.co.uk/merec_bulletins.htm"target="_blank">www.npc.co.uk/merec_bulletins.htm</a> <br />
<br />
<em>Pharmaceutical Journal</em> (official weekly journal of the Royal Pharmaceutical Society of Great Britain): available from <br />
<a href="http://www.pjonline.com"target="_blank">www.pjonline.com</a> <br />
Site also gives access to Hospital Pharmacist (London). <br />
<br />
UK manufacturers' SPCs: available from <br />
<a href="http://www.medicines.org.uk"target="_blank">www.medicines.org.uk</a> <br />
<br />
The Cochrane Library: available from <br />
<a href="http://www3.interscience.wiley.com/cgi-bin/mrwhome/106568753/HOME"target="_blank">www3.interscience.wiley.com/cgi-bin/mrwhome/106568753/HOME</a> <br />
Collection of evidence-based systematic reviews. <br />
A subscription is required in certain other countries. <br />
<br />
Various other sources and full-text core journals (e.g. the <em>British Medical Journal</em> and the <em>Lancet</em>) are available free to UK NHS staff with an Athens password through the National Library for Health (NLH) at <br />
<a href="http://www.library.nhs.uk/Default.aspx"target="_blank">www.library.nhs.uk/Default.aspx</a> 
</p>
<div class="ref">
<hr width="100%" />
<ol start="1" type="1">
	<li>Agency for Health Care Policy and Research (1992) Acute pain management, operative or medical procedures and trauma 92-0032. In: <em>Clinical Practice Guideline Quick Reference Guide for Clinicians</em>. AHCPR Publications, Rockville, Maryland, USA, pp. 1&ndash;22. </li>
	<li>NICE (2004) <em>Depression: Management of depression in primary and secondary care</em>. In: National Clinical Practice Guideline Number 23. National Institute for Clinical Excellence. Available from: <a href="http://www.nice.org.uk/page.aspx?o=236667"target="_blank">www.nice.org.uk/page.aspx?o=236667</a></li>
	<li>Aoun SM and Kristjanson LJ (2005) Challenging the framework for evidence in palliative care research. <em>Palliative Medicine</em>. <strong>19</strong>: 461&ndash;465. </li>
	<li>DoH (1996) <em>Clinical Guidelines: Using Clinical Guidelines to Improve Patient Care Within the NHS</em>. Department of Health: NHS Executive, Leeds. </li>
	<li>Eccles M and Mason J (2001) How to develop cost-conscious guidelines. <em>Health Technology Assessment</em>. <strong>5 (16)</strong>. </li>
	<li>Angell M (2004) <em>The Truth About the Drug Companies: how they deceive us and what to do about it</em>. Random House, New York. </li>
	<li>National Prescribing Centre (2000) Modified-release preparations. <em>MeReC Bulletin</em>. <strong>11</strong>: 13&ndash;16. </li>
	<li>Smith J (2004) <em>Building a Safer NHS for Patients - Improving Medication Safety</em>. Department of Health, London, pp. 105&ndash;111. Available from: <a href="http://www.dh.gov.uk/assetRoot/04/08/49/61/04084961.pdf"target="_blank">www.dh.gov.uk/assetRoot/04/08/49/61/04084961.pdf</a></li>
</ol>
</div>
		</div></div></div></div>
	</div>

	<div id="footer">
		<p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

	</div>
	</div>
</body>
</html>