<?php die('Unauthorized access.'); ?>a:38:{s:2:"id";s:1:"1";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:9:"home page";s:9:"longtitle";s:43:"Welcome to the palliativedrugs.com website.";s:11:"description";s:0:"";s:5:"alias";s:4:"home";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"0";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:824:"<h1><strong>Welcome to the updated palliativedrugs.com website</strong></h1>
<p>
The site provides essential independent information for health professionals 
worldwide about drugs used in palliative and hospice care. The content is 
based on the UK Palliative Care Formulary (PCF, 3rd edition) which was Highly Commended in the British Medical Association 2008 book competition. It includes details about unlicensed (unlabeled) indications and routes, and the administration of multiple drugs by continuous subcutaneous infusion.
</p>
<p>
There are regularly updated 'Latest additions' and 'News' sections, and a Bulletin board which covers drug and non-drug issues. There is also a search facility which covers the whole site.
</p>
[!hp_press_news!]
<img src="/mk/alden_html/css/space.gif" width="1" height="20">";s:8:"richtext";s:1:"0";s:8:"template";s:2:"16";s:9:"menuindex";s:1:"0";s:10:"searchable";s:1:"0";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1130304721";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1294326081";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1130304721";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:4:"Home";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"1";s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/tooltip.js"></script>
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script>
<!-- MK-VIS-TEST-->
	<script type="text/javascript" src="/js/common.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/subModal.css" />

</head>
<body>
<div id="container">
  [!header!]
  [!nav!]
<div class='exposed' id='e1' style='display:none;'>		      
	<fieldset>		
		<h3>Survey</h3>
		<p>A new survey has been published...</p>
		<form action='' class='appnitro' method='post' name='frmsurvey' id='frmsurvey'>	
		       <p class='error'></p>
		        <input type='button' name='btnyes' id='btnyes' value='Yes, complete now'>
                       <input type='button' name='btnlater' id='btnlater' value='Yes, complete later'>
			 <input type='button' name='btnnever' id='btnnever' value='No, decline survey'>
			 
		</form>
	</fieldset>
</div>
  <div id="content">
	<div id="col01">
    	<div class="tr">	
    		<div class="bl">
    			<div class="br">
					[!user_menu!]
    				<div id="honcode">		
						<p><a href="https://www.healthonnet.org/HONcode/Conduct.html?HONConduct825189" onclick="window.open(this.href); return false;" > <img src="http://www.honcode.ch/HONcode/Seal/HONConduct825189_s.gif" hspace="2" vspace="2" align="left" width="49" height="72" style="border:0px" title="This website is certified by Health On the Net Foundation. Click to verify." alt="This website is certified by Health On the Net Foundation. Click to verify." /></a> This site complies with the <a href="http://www.healthonnet.org/HONcode/Conduct.html" onclick="window.open(this.href); return false;"> HONcode standard for trustworthy health</a> information: <br /><a href="https://www.healthonnet.org/HONcode/Conduct.html?HONConduct825189" onclick="window.open(this.href); return false;">verify here</a>. </p>		
    				</div>
<br clear = all />
<!-- <a href="http://www.palliativebooks.com/" target="_blank" style="margin: 6px 0 6px 6px" onClick="javascript: pageTracker._trackPageview("/delegates/palliativebooks");"><img src="http://www.palliativedrugs.com/assets/banners/confdelegates.gif" alt="Conference Delegates" name="" width="246" height="60" border="0" /></a> -->
<div id="bookbanners" style="margin-left:5px"> 
  <!-- container for the slides -->
  <style>
.hpbanners {
	/*border:1px solid #ccc;*/
	position:relative;	
	height: 136px;
	
	width: 246px;
	float:left;
	cursor:pointer; margin: 0; }

/* single slide */
.hpbanners div {
	display:none;
	position:absolute;
	top:0;
	left:0;		
	margin: 0;
	padding: 0;
	height: 136px;
	font-size:12px;
}

</style>
  <div class="hpbanners"> 
    
    <!-- first slide -->
    <div><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=282" target="_blank"><img src="http://www.palliativedrugs.com/assets/banners2011/pcfuk-banner_2.gif" width="246" height="136" border="0" /></a></div>
    
    <!-- second slide -->
    <div><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=280" target="_blank"><img src="http://www.palliativedrugs.com/assets/banners2011/smac-banner.gif" width="246" height="136" border="0" /></a></div>
    
    <!-- third slide -->
    <div> <a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=19" target="_blank"><img src="http://www.palliativedrugs.com/assets/banners2011/hpcf-banner.gif" width="246" height="136" border="0" /></a></div>
    
    <!-- Fourth slide -->
    <div><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=281" target="_blank"><img src="http://www.palliativedrugs.com/assets/banners2011/pcfcn-banner.gif" width="246" height="136" border="0" /></a></div>
  </div>
  
  <!-- the tabs -->
  <div class="slidetabs"> <a href="#"></a> <a href="#"></a> <a href="#"></a> <a href="#"></a> </div>
<h3 id="germanpcf">PCF Website jetzt auf Deutsch! <a href="http://www.palliativedrugs.com/palliative-care-formulary.html" title="PCF Website jetzt auf Deutsch"><span>Hier f�r Zugang klicken</span></a></h3>
</div>



    			</div>
    		</div>
		</div>
    </div>
    <div id="col02">
      <div class="bg_right">
        <div class="tr">
          <div class="bl">
            <div class="br">
              <div id="col02_1">
                <!--[!breadcrumbs!]-->
				<h1><strong>Welcome to the updated palliativedrugs.com website</strong></h1>
<p>
The site provides essential independent information for health professionals 
worldwide about drugs used in palliative and hospice care. The content is 
based on the UK Palliative Care Formulary (PCF, 3rd edition) which was Highly Commended in the British Medical Association 2008 book competition. It includes details about unlicensed (unlabeled) indications and routes, and the administration of multiple drugs by continuous subcutaneous infusion.
</p>
<p>
There are regularly updated 'Latest additions' and 'News' sections, and a Bulletin board which covers drug and non-drug issues. There is also a search facility which covers the whole site.
</p>
[!hp_press_news!]
<img src="/mk/alden_html/css/space.gif" width="1" height="20">
              </div>
              <div id="col02_2">
				 [!sidebar_survey!]
 				 [!sidebar_updates!]
 				[!sidebar_news!]				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
      <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>

[!survey_expose!]
</body>
</html>
