<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"220";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:21:"List of abbreviations";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:21:"list-of-abbreviations";s:15:"link_attributes";s:21:"list-of-abbreviations";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:13299:"<h1>ABBREVIATIONS</h1>
<br />
<p><strong>Drug administration</strong></p>
<p class="first">
<strong>Table 3</strong>&nbsp;&nbsp;Drug administration times
</p>
<table border="1" cellpadding="2" cellspacing="0" frame="above" id="tbl" rules="groups" width="95%">
	<thead>
		<tr>
			<td>Times</td>
			<td>UK</td>
			<td>Latin</td>
			<td>USA</td>
			<td>Latin</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Once daily</td>
			<td>o.d</td>
			<td><em>omni die</em></td>
			<td>q.d.</td>
			<td><em>quaque die</em></td>
		</tr>
		<tr>
			<td>Every morning</td>
			<td>o.m.</td>
			<td><em>omni mane</em></td>
			<td>q.a.m.</td>
			<td><em>quaque ante meridiem</em></td>
		</tr>
		<tr>
			<td>At bedtime</td>
			<td>o.n.</td>
			<td><em>omni nocte</em></td>
			<td>h.s.</td>
			<td><em>hora somni</em></td>
		</tr>
		<tr>
			<td>Twice daily</td>
			<td>b.d.</td>
			<td><em>bis die</em></td>
			<td>b.i.d.</td>
			<td><em>bis in die</em></td>
		</tr>
		<tr>
			<td>Three times daily</td>
			<td>t.d.s.</td>
			<td><em>ter die sumendus</em></td>
			<td>t.i.d.</td>
			<td><em>ter in die</em></td>
		</tr>
		<tr>
			<td>Four times daily</td>
			<td>q.d.s.</td>
			<td><em>quarta die sumendus</em></td>
			<td>q.i.d.</td>
			<td><em>quarta in die</em></td>
		</tr>
		<tr>
			<td>Every 4 hours etc.</td>
			<td>q4h</td>
			<td><em>quaque quarta hora</em></td>
			<td>q4h</td>
			<td><em>quaque quarta hora</em></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Rescue medication</td>
			<td>p.r.n.</td>
			<td><em>pro re nata</em></td>
			<td>p.r.n.</td>
			<td><em>pro re nata</em></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>(as needed/required)</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Give immediately</td>
			<td>stat</td>
			<td>&nbsp;</td>
			<td>stat</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
	</tfoot>
</table>
<img height="27" src="c:/pcf-usa/css/space.gif" width="1" />
<table id="tbl">
	<tbody>
		<tr>
			<td width="100">a.c.</td>
			<td>ante cibum (before food)</td>
		</tr>
		<tr>
			<td>amp</td>
			<td>ampoule containing a single dose (cf. vial)</td>
		</tr>
		<tr>
			<td>CD</td>
			<td>preparation subject to prescription requirements under the Misuse of Drugs Act (UK); for regulations see BNF</td>
		</tr>
		<tr>
			<td>CIVI</td>
			<td>continuous intravenous infusion</td>
		</tr>
		<tr>
			<td>CSCI</td>
			<td>continuous subcutaneous infusion</td>
		</tr>
		<tr>
			<td>e/c</td>
			<td>enteric-coated</td>
		</tr>
		<tr>
			<td>ED</td>
			<td>epidural</td>
		</tr>
		<tr>
			<td>IM</td>
			<td>intramuscular</td>
		</tr>
		<tr>
			<td>IT</td>
			<td>intrathecal</td>
		</tr>
		<tr>
			<td>IV</td>
			<td>intravenous</td>
		</tr>
		<tr>
			<td>IVI</td>
			<td>intravenous infusion</td>
		</tr>
		<tr>
			<td>m/r</td>
			<td>modified-release; alternatives, slow-release, sustained-release, controlled-release, extended-release</td>
		</tr>
		<tr>
			<td><img src="/mk/nhs_slant.png"></td>
			<td>not prescribable on NHS prescriptions</td>
		</tr>
		<tr>
			<td>OTC</td>
			<td>over the counter (i.e. can be obtained without a prescription)</td>
		</tr>
		<tr>
			<td>p.c.</td>
			<td>post cibum (after food)</td>
		</tr>
		<tr>
			<td>PO</td>
			<td>per os, by mouth</td>
		</tr>
		<tr>
			<td>POM</td>
			<td>prescription-only medicine</td>
		</tr>
		<tr>
			<td>PR</td>
			<td>per rectum</td>
		</tr>
		<tr>
			<td>PV</td>
			<td>per vaginum</td>
		</tr>
		<tr>
			<td>SC</td>
			<td>subcutaneous</td>
		</tr>
		<tr>
			<td>SL</td>
			<td>sublingual</td>
		</tr>
		<tr>
			<td>TD</td>
			<td>transdermal</td>
		</tr>
		<tr>
			<td>vial</td>
			<td>sterile container with a rubber bung containing either a single or multiple doses (cf. amp)</td>
		</tr>
		<tr>
			<td>WFI</td>
			<td>water for injections</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<strong>General</strong></td>
		</tr>
		<tr>
			<td>*</td>
			<td>specialist use only</td>
		</tr>
		<tr>
			<td>&dagger;</td>
			<td>unlicensed use</td>
		</tr>
		<tr>
			<td>BNF</td>
			<td>British National Formulary</td>
		</tr>
		<tr>
			<td>BP</td>
			<td>British Pharmacopoeia</td>
		</tr>
		<tr>
			<td>CHM</td>
			<td>Commission on Human Medicines</td>
		</tr>
		<tr>
			<td>CSM</td>
			<td>Committee on Safety of Medicines (now part of CHM)</td>
		</tr>
		<tr>
			<td>EMEA</td>
			<td>European Medicines Agency</td>
		</tr>
		<tr>
			<td>EORTC</td>
			<td>European Organisation for Research and Treatment of Cancer</td>
		</tr>
		<tr>
			<td>FDA</td>
			<td>Food and Drug Administration (USA)</td>
		</tr>
		<tr>
			<td>IASP</td>
			<td>International Association for the Study of Pain</td>
		</tr>
		<tr>
			<td>IDIS</td>
			<td>International Drug Information Service</td>
		</tr>
		<tr>
			<td>MCA</td>
			<td>Medicines Control Agency (now MHRA)</td>
		</tr>
		<tr>
			<td>MHRA</td>
			<td>Medicines and Healthcare products Regulatory Agency (formerly MCA)</td>
		</tr>
		<tr>
			<td>NICE</td>
			<td>National Institute for Health and Clinical Excellence</td>
		</tr>
		<tr>
			<td>NPF</td>
			<td>Nurse Prescribers' Formulary</td>
		</tr>
		<tr>
			<td>PCS/PCU</td>
			<td>Palliative care service/unit</td>
		</tr>
		<tr>
			<td>PIL</td>
			<td>Patient Information Leaflet</td>
		</tr>
		<tr>
			<td>rINN</td>
			<td>recommended International Non-proprietary Name</td>
		</tr>
		<tr>
			<td>SPC</td>
			<td>Summary of Product Characteristics</td>
		</tr>
		<tr>
			<td>UK</td>
			<td>United Kingdom</td>
		</tr>
		<tr>
			<td>USA</td>
			<td>United States of America</td>
		</tr>
		<tr>
			<td>VAS</td>
			<td>visual analogue scale, 0&ndash;100mm</td>
		</tr>
		<tr>
			<td>WHO</td>
			<td>World Health Organization</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<strong>Medical</strong></td>
		</tr>
		<tr>
			<td>ACD</td>
			<td>anaemia of chronic disease</td>
		</tr>
		<tr>
			<td>ACE</td>
			<td>angiotensin-converting enzyme</td>
		</tr>
		<tr>
			<td>ADH</td>
			<td>antidiuretic hormone (vasopressin)</td>
		</tr>
		<tr>
			<td>AUC</td>
			<td>area under the plasma concentration&ndash;time curve</td>
		</tr>
		<tr>
			<td><img src="/mk/beta.png"><sub>2</sub></td>
			<td>beta 2 adrenergic (receptor)</td>
		</tr>
		<tr>
			<td>BUN</td>
			<td>blood urea nitrogen</td>
		</tr>
		<tr>
			<td>CHF</td>
			<td>congestive heart failure</td>
		</tr>
		<tr>
			<td>CNS</td>
			<td>central nervous system</td>
		</tr>
		<tr>
			<td>COX</td>
			<td>cyclo-oxygenase; alternative, prostaglandin synthase</td>
		</tr>
		<tr>
			<td>COPD</td>
			<td>chronic obstructive pulmonary disease</td>
		</tr>
		<tr>
			<td>CRP</td>
			<td>C-reactive protein</td>
		</tr>
		<tr>
			<td>CSF</td>
			<td>cerebrospinal fluid</td>
		</tr>
		<tr>
			<td>CT</td>
			<td>computed tomography</td>
		</tr>
		<tr>
			<td><img src="/mk/delta.png"></td>
			<td>delta-opioid (receptor)</td>
		</tr>
		<tr>
			<td>D<sub>2</sub></td>
			<td>dopamine type 2 (receptor)</td>
		</tr>
		<tr>
			<td>DIC</td>
			<td>disseminated intravascular coagulation</td>
		</tr>
		<tr>
			<td>DVT</td>
			<td>deep vein thrombosis</td>
		</tr>
		<tr>
			<td>ECG</td>
			<td>electrocardiogram</td>
		</tr>
		<tr>
			<td>ECT</td>
			<td>electroconvulsive therapy</td>
		</tr>
		<tr>
			<td>FEV<sub>1</sub></td>
			<td>forced expiratory volume in 1 second</td>
		</tr>
		<tr>
			<td>FRC</td>
			<td>functional residual capacity</td>
		</tr>
		<tr>
			<td>FSH</td>
			<td>follicle-stimulating hormone</td>
		</tr>
		<tr>
			<td>FVC</td>
			<td>forced vital capacity of lungs</td>
		</tr>
		<tr>
			<td>GABA</td>
			<td>gamma-aminobutyric acid</td>
		</tr>
		<tr>
			<td>GI</td>
			<td>gastro-intestinal</td>
		</tr>
		<tr>
			<td>H<sub>1</sub>, H<sub>2</sub></td>
			<td>histamine type 1, type 2 (receptor)</td>
		</tr>
		<tr>
			<td>Ig</td>
			<td>immunoglobulin</td>
		</tr>
		<tr>
			<td>INR</td>
			<td>international normalized ratio</td>
		</tr>
		<tr>
			<td><img src="/mk/kappa.png"></td>
			<td>kappa-opioid (receptor)</td>
		</tr>
		<tr>
			<td>LABA</td>
			<td>long-acting &beta;<sub>2</sub>-adrenergic receptor agonist</td>
		</tr>
		<tr>
			<td>LFTs</td>
			<td>liver function tests</td>
		</tr>
		<tr>
			<td>LH</td>
			<td>luteinising hormone</td>
		</tr>
		<tr>
			<td>LMWH</td>
			<td>low molecular weight heparin</td>
		</tr>
		<tr>
			<td>MAOI</td>
			<td>mono-amine oxidase inhibitor</td>
		</tr>
		<tr>
			<td>MARI</td>
			<td>mono-amine re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>MRI</td>
			<td>magnetic resonance imaging</td>
		</tr>
		<tr>
			<td>MSU</td>
			<td>mid-stream specimen of urine</td>
		</tr>
		<tr>
			<td><img src="/mk/mu.png"></td>
			<td>mu-opioid (receptor)</td>
		</tr>
		<tr>
			<td>NaSSA</td>
			<td>noradrenergic and specific serotoninergic antidepressant</td>
		</tr>
		<tr>
			<td>NDRI</td>
			<td>noradrenaline (norepinephrine) and dopamine re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>NG</td>
			<td>nasogastric</td>
		</tr>
		<tr>
			<td>NJ</td>
			<td>nasojejunal</td>
		</tr>
		<tr>
			<td>NMDA</td>
			<td>N-methyl D-aspartate</td>
		</tr>
		<tr>
			<td>NNH</td>
			<td>number needed to harm, i.e. the number of patients needed to be treated in order to harm one patient sufficiently to cause withdrawal from a drug trial</td>
		</tr>
		<tr>
			<td>NNT</td>
			<td>number needed to treat, i.e. the number of patients needed to be treated in order to achieve 50% improvement in one patient compared with placebo</td>
		</tr>
		<tr>
			<td>NRI</td>
			<td>noradrenaline (norepinephrine) re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>NSAID</td>
			<td>non-steroidal anti-inflammatory drug</td>
		</tr>
		<tr>
			<td>PaCO<sub>2</sub></td>
			<td>arterial partial pressure of carbon dioxide</td>
		</tr>
		<tr>
			<td>PaO<sub>2</sub></td>
			<td>arterial partial pressure of oxygen</td>
		</tr>
		<tr>
			<td>PCA</td>
			<td>patient-controlled analgesia</td>
		</tr>
		<tr>
			<td>PE</td>
			<td>pulmonary embolus/embolism</td>
		</tr>
		<tr>
			<td>PG</td>
			<td>prostaglandin</td>
		</tr>
		<tr>
			<td>PPI</td>
			<td>proton pump inhibitor</td>
		</tr>
		<tr>
			<td>PUB</td>
			<td>gastro-intestinal perforation, ulceration or bleeding (in relation to serious GI events caused by NSAIDs)</td>
		</tr>
		<tr>
			<td>RCT</td>
			<td>randomized controlled trial</td>
		</tr>
		<tr>
			<td>RIMA</td>
			<td>reversible inhibitor of mono-amine oxidase type A</td>
		</tr>
		<tr>
			<td>RTI</td>
			<td>respiratory tract infection</td>
		</tr>
		<tr>
			<td>SNRI</td>
			<td>serotonin and noradrenaline (norepinephrine) re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>SSRI</td>
			<td>selective serotonin re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>TCA</td>
			<td>tricyclic antidepressant</td>
		</tr>
		<tr>
			<td>TIBC</td>
			<td>total iron-binding capacity; alternative, plasma transferrin concentration</td>
		</tr>
		<tr>
			<td>Tl<sub>CO</sub></td>
			<td>transfer factor of the lung for carbon monoxide</td>
		</tr>
		<tr>
			<td>UTI</td>
			<td>urinary tract infection</td>
		</tr>
		<tr>
			<td>VIP</td>
			<td>vaso-active intestinal polypeptide</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<strong>Units</strong></td>
		</tr>
		<tr>
			<td>cm</td>
			<td>centimetre(s)</td>
		</tr>
		<tr>
			<td>cps</td>
			<td>cycles per sec</td>
		</tr>
		<tr>
			<td>dL</td>
			<td>decilitre(s)</td>
		</tr>
		<tr>
			<td>g</td>
			<td>gram(s)</td>
		</tr>
		<tr>
			<td>Gy</td>
			<td>Gray(s), a measure of radiation</td>
		</tr>
		<tr>
			<td>h</td>
			<td>hour(s)</td>
		</tr>
		<tr>
			<td>Hg</td>
			<td>mercury</td>
		</tr>
                <tr>
			<td>kcal</td>
			<td>kilocalories</td>
		</tr>
		<tr>
			<td>kg</td>
			<td>kilogram(s)</td>
		</tr>
		<tr>
			<td>L</td>
			<td>litre(s)</td>
		</tr>
		<tr>
			<td>mg</td>
			<td>milligram(s)</td>
		</tr>
		<tr>
			<td>micromol</td>
			<td>micromole(s)</td>
		</tr>
		<tr>
			<td>ml</td>
			<td>millilitre(s)</td>
		</tr>
		<tr>
			<td>mm</td>
			<td>millimetre(s)</td>
		</tr>
		<tr>
			<td>mmol</td>
			<td>millimole(s)</td>
		</tr>
		<tr>
			<td>min</td>
			<td>minute(s)</td>
		</tr>
		<tr>
			<td>mosmol</td>
			<td>milli-osmole(s)</td>
		</tr>
		<tr>
			<td>msec</td>
			<td>millisecond</td>
		</tr>
		<tr>
			<td>nm</td>
			<td>nanometre(s)</td>
		</tr>
		<tr>
			<td>nmol</td>
			<td>nanomole(s); alternative, nM</td>
		</tr>
		<tr>
			<td>sec</td>
			<td>second(s)</td>
		</tr>
	</tbody>
</table>
<p>
&nbsp;
</p>
";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:1:"6";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"3";s:9:"createdon";s:10:"1204046421";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1217759003";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1204046421";s:11:"publishedby";s:1:"3";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h1>ABBREVIATIONS</h1>
<br />
<p><strong>Drug administration</strong></p>
<p class="first">
<strong>Table 3</strong>&nbsp;&nbsp;Drug administration times
</p>
<table border="1" cellpadding="2" cellspacing="0" frame="above" id="tbl" rules="groups" width="95%">
	<thead>
		<tr>
			<td>Times</td>
			<td>UK</td>
			<td>Latin</td>
			<td>USA</td>
			<td>Latin</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Once daily</td>
			<td>o.d</td>
			<td><em>omni die</em></td>
			<td>q.d.</td>
			<td><em>quaque die</em></td>
		</tr>
		<tr>
			<td>Every morning</td>
			<td>o.m.</td>
			<td><em>omni mane</em></td>
			<td>q.a.m.</td>
			<td><em>quaque ante meridiem</em></td>
		</tr>
		<tr>
			<td>At bedtime</td>
			<td>o.n.</td>
			<td><em>omni nocte</em></td>
			<td>h.s.</td>
			<td><em>hora somni</em></td>
		</tr>
		<tr>
			<td>Twice daily</td>
			<td>b.d.</td>
			<td><em>bis die</em></td>
			<td>b.i.d.</td>
			<td><em>bis in die</em></td>
		</tr>
		<tr>
			<td>Three times daily</td>
			<td>t.d.s.</td>
			<td><em>ter die sumendus</em></td>
			<td>t.i.d.</td>
			<td><em>ter in die</em></td>
		</tr>
		<tr>
			<td>Four times daily</td>
			<td>q.d.s.</td>
			<td><em>quarta die sumendus</em></td>
			<td>q.i.d.</td>
			<td><em>quarta in die</em></td>
		</tr>
		<tr>
			<td>Every 4 hours etc.</td>
			<td>q4h</td>
			<td><em>quaque quarta hora</em></td>
			<td>q4h</td>
			<td><em>quaque quarta hora</em></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Rescue medication</td>
			<td>p.r.n.</td>
			<td><em>pro re nata</em></td>
			<td>p.r.n.</td>
			<td><em>pro re nata</em></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>(as needed/required)</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Give immediately</td>
			<td>stat</td>
			<td>&nbsp;</td>
			<td>stat</td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">&nbsp;</td>
		</tr>
	</tfoot>
</table>
<img height="27" src="c:/pcf-usa/css/space.gif" width="1" />
<table id="tbl">
	<tbody>
		<tr>
			<td width="100">a.c.</td>
			<td>ante cibum (before food)</td>
		</tr>
		<tr>
			<td>amp</td>
			<td>ampoule containing a single dose (cf. vial)</td>
		</tr>
		<tr>
			<td>CD</td>
			<td>preparation subject to prescription requirements under the Misuse of Drugs Act (UK); for regulations see BNF</td>
		</tr>
		<tr>
			<td>CIVI</td>
			<td>continuous intravenous infusion</td>
		</tr>
		<tr>
			<td>CSCI</td>
			<td>continuous subcutaneous infusion</td>
		</tr>
		<tr>
			<td>e/c</td>
			<td>enteric-coated</td>
		</tr>
		<tr>
			<td>ED</td>
			<td>epidural</td>
		</tr>
		<tr>
			<td>IM</td>
			<td>intramuscular</td>
		</tr>
		<tr>
			<td>IT</td>
			<td>intrathecal</td>
		</tr>
		<tr>
			<td>IV</td>
			<td>intravenous</td>
		</tr>
		<tr>
			<td>IVI</td>
			<td>intravenous infusion</td>
		</tr>
		<tr>
			<td>m/r</td>
			<td>modified-release; alternatives, slow-release, sustained-release, controlled-release, extended-release</td>
		</tr>
		<tr>
			<td><img src="/mk/nhs_slant.png"></td>
			<td>not prescribable on NHS prescriptions</td>
		</tr>
		<tr>
			<td>OTC</td>
			<td>over the counter (i.e. can be obtained without a prescription)</td>
		</tr>
		<tr>
			<td>p.c.</td>
			<td>post cibum (after food)</td>
		</tr>
		<tr>
			<td>PO</td>
			<td>per os, by mouth</td>
		</tr>
		<tr>
			<td>POM</td>
			<td>prescription-only medicine</td>
		</tr>
		<tr>
			<td>PR</td>
			<td>per rectum</td>
		</tr>
		<tr>
			<td>PV</td>
			<td>per vaginum</td>
		</tr>
		<tr>
			<td>SC</td>
			<td>subcutaneous</td>
		</tr>
		<tr>
			<td>SL</td>
			<td>sublingual</td>
		</tr>
		<tr>
			<td>TD</td>
			<td>transdermal</td>
		</tr>
		<tr>
			<td>vial</td>
			<td>sterile container with a rubber bung containing either a single or multiple doses (cf. amp)</td>
		</tr>
		<tr>
			<td>WFI</td>
			<td>water for injections</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<strong>General</strong></td>
		</tr>
		<tr>
			<td>*</td>
			<td>specialist use only</td>
		</tr>
		<tr>
			<td>&dagger;</td>
			<td>unlicensed use</td>
		</tr>
		<tr>
			<td>BNF</td>
			<td>British National Formulary</td>
		</tr>
		<tr>
			<td>BP</td>
			<td>British Pharmacopoeia</td>
		</tr>
		<tr>
			<td>CHM</td>
			<td>Commission on Human Medicines</td>
		</tr>
		<tr>
			<td>CSM</td>
			<td>Committee on Safety of Medicines (now part of CHM)</td>
		</tr>
		<tr>
			<td>EMEA</td>
			<td>European Medicines Agency</td>
		</tr>
		<tr>
			<td>EORTC</td>
			<td>European Organisation for Research and Treatment of Cancer</td>
		</tr>
		<tr>
			<td>FDA</td>
			<td>Food and Drug Administration (USA)</td>
		</tr>
		<tr>
			<td>IASP</td>
			<td>International Association for the Study of Pain</td>
		</tr>
		<tr>
			<td>IDIS</td>
			<td>International Drug Information Service</td>
		</tr>
		<tr>
			<td>MCA</td>
			<td>Medicines Control Agency (now MHRA)</td>
		</tr>
		<tr>
			<td>MHRA</td>
			<td>Medicines and Healthcare products Regulatory Agency (formerly MCA)</td>
		</tr>
		<tr>
			<td>NICE</td>
			<td>National Institute for Health and Clinical Excellence</td>
		</tr>
		<tr>
			<td>NPF</td>
			<td>Nurse Prescribers' Formulary</td>
		</tr>
		<tr>
			<td>PCS/PCU</td>
			<td>Palliative care service/unit</td>
		</tr>
		<tr>
			<td>PIL</td>
			<td>Patient Information Leaflet</td>
		</tr>
		<tr>
			<td>rINN</td>
			<td>recommended International Non-proprietary Name</td>
		</tr>
		<tr>
			<td>SPC</td>
			<td>Summary of Product Characteristics</td>
		</tr>
		<tr>
			<td>UK</td>
			<td>United Kingdom</td>
		</tr>
		<tr>
			<td>USA</td>
			<td>United States of America</td>
		</tr>
		<tr>
			<td>VAS</td>
			<td>visual analogue scale, 0&ndash;100mm</td>
		</tr>
		<tr>
			<td>WHO</td>
			<td>World Health Organization</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<strong>Medical</strong></td>
		</tr>
		<tr>
			<td>ACD</td>
			<td>anaemia of chronic disease</td>
		</tr>
		<tr>
			<td>ACE</td>
			<td>angiotensin-converting enzyme</td>
		</tr>
		<tr>
			<td>ADH</td>
			<td>antidiuretic hormone (vasopressin)</td>
		</tr>
		<tr>
			<td>AUC</td>
			<td>area under the plasma concentration&ndash;time curve</td>
		</tr>
		<tr>
			<td><img src="/mk/beta.png"><sub>2</sub></td>
			<td>beta 2 adrenergic (receptor)</td>
		</tr>
		<tr>
			<td>BUN</td>
			<td>blood urea nitrogen</td>
		</tr>
		<tr>
			<td>CHF</td>
			<td>congestive heart failure</td>
		</tr>
		<tr>
			<td>CNS</td>
			<td>central nervous system</td>
		</tr>
		<tr>
			<td>COX</td>
			<td>cyclo-oxygenase; alternative, prostaglandin synthase</td>
		</tr>
		<tr>
			<td>COPD</td>
			<td>chronic obstructive pulmonary disease</td>
		</tr>
		<tr>
			<td>CRP</td>
			<td>C-reactive protein</td>
		</tr>
		<tr>
			<td>CSF</td>
			<td>cerebrospinal fluid</td>
		</tr>
		<tr>
			<td>CT</td>
			<td>computed tomography</td>
		</tr>
		<tr>
			<td><img src="/mk/delta.png"></td>
			<td>delta-opioid (receptor)</td>
		</tr>
		<tr>
			<td>D<sub>2</sub></td>
			<td>dopamine type 2 (receptor)</td>
		</tr>
		<tr>
			<td>DIC</td>
			<td>disseminated intravascular coagulation</td>
		</tr>
		<tr>
			<td>DVT</td>
			<td>deep vein thrombosis</td>
		</tr>
		<tr>
			<td>ECG</td>
			<td>electrocardiogram</td>
		</tr>
		<tr>
			<td>ECT</td>
			<td>electroconvulsive therapy</td>
		</tr>
		<tr>
			<td>FEV<sub>1</sub></td>
			<td>forced expiratory volume in 1 second</td>
		</tr>
		<tr>
			<td>FRC</td>
			<td>functional residual capacity</td>
		</tr>
		<tr>
			<td>FSH</td>
			<td>follicle-stimulating hormone</td>
		</tr>
		<tr>
			<td>FVC</td>
			<td>forced vital capacity of lungs</td>
		</tr>
		<tr>
			<td>GABA</td>
			<td>gamma-aminobutyric acid</td>
		</tr>
		<tr>
			<td>GI</td>
			<td>gastro-intestinal</td>
		</tr>
		<tr>
			<td>H<sub>1</sub>, H<sub>2</sub></td>
			<td>histamine type 1, type 2 (receptor)</td>
		</tr>
		<tr>
			<td>Ig</td>
			<td>immunoglobulin</td>
		</tr>
		<tr>
			<td>INR</td>
			<td>international normalized ratio</td>
		</tr>
		<tr>
			<td><img src="/mk/kappa.png"></td>
			<td>kappa-opioid (receptor)</td>
		</tr>
		<tr>
			<td>LABA</td>
			<td>long-acting &beta;<sub>2</sub>-adrenergic receptor agonist</td>
		</tr>
		<tr>
			<td>LFTs</td>
			<td>liver function tests</td>
		</tr>
		<tr>
			<td>LH</td>
			<td>luteinising hormone</td>
		</tr>
		<tr>
			<td>LMWH</td>
			<td>low molecular weight heparin</td>
		</tr>
		<tr>
			<td>MAOI</td>
			<td>mono-amine oxidase inhibitor</td>
		</tr>
		<tr>
			<td>MARI</td>
			<td>mono-amine re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>MRI</td>
			<td>magnetic resonance imaging</td>
		</tr>
		<tr>
			<td>MSU</td>
			<td>mid-stream specimen of urine</td>
		</tr>
		<tr>
			<td><img src="/mk/mu.png"></td>
			<td>mu-opioid (receptor)</td>
		</tr>
		<tr>
			<td>NaSSA</td>
			<td>noradrenergic and specific serotoninergic antidepressant</td>
		</tr>
		<tr>
			<td>NDRI</td>
			<td>noradrenaline (norepinephrine) and dopamine re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>NG</td>
			<td>nasogastric</td>
		</tr>
		<tr>
			<td>NJ</td>
			<td>nasojejunal</td>
		</tr>
		<tr>
			<td>NMDA</td>
			<td>N-methyl D-aspartate</td>
		</tr>
		<tr>
			<td>NNH</td>
			<td>number needed to harm, i.e. the number of patients needed to be treated in order to harm one patient sufficiently to cause withdrawal from a drug trial</td>
		</tr>
		<tr>
			<td>NNT</td>
			<td>number needed to treat, i.e. the number of patients needed to be treated in order to achieve 50% improvement in one patient compared with placebo</td>
		</tr>
		<tr>
			<td>NRI</td>
			<td>noradrenaline (norepinephrine) re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>NSAID</td>
			<td>non-steroidal anti-inflammatory drug</td>
		</tr>
		<tr>
			<td>PaCO<sub>2</sub></td>
			<td>arterial partial pressure of carbon dioxide</td>
		</tr>
		<tr>
			<td>PaO<sub>2</sub></td>
			<td>arterial partial pressure of oxygen</td>
		</tr>
		<tr>
			<td>PCA</td>
			<td>patient-controlled analgesia</td>
		</tr>
		<tr>
			<td>PE</td>
			<td>pulmonary embolus/embolism</td>
		</tr>
		<tr>
			<td>PG</td>
			<td>prostaglandin</td>
		</tr>
		<tr>
			<td>PPI</td>
			<td>proton pump inhibitor</td>
		</tr>
		<tr>
			<td>PUB</td>
			<td>gastro-intestinal perforation, ulceration or bleeding (in relation to serious GI events caused by NSAIDs)</td>
		</tr>
		<tr>
			<td>RCT</td>
			<td>randomized controlled trial</td>
		</tr>
		<tr>
			<td>RIMA</td>
			<td>reversible inhibitor of mono-amine oxidase type A</td>
		</tr>
		<tr>
			<td>RTI</td>
			<td>respiratory tract infection</td>
		</tr>
		<tr>
			<td>SNRI</td>
			<td>serotonin and noradrenaline (norepinephrine) re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>SSRI</td>
			<td>selective serotonin re-uptake inhibitor</td>
		</tr>
		<tr>
			<td>TCA</td>
			<td>tricyclic antidepressant</td>
		</tr>
		<tr>
			<td>TIBC</td>
			<td>total iron-binding capacity; alternative, plasma transferrin concentration</td>
		</tr>
		<tr>
			<td>Tl<sub>CO</sub></td>
			<td>transfer factor of the lung for carbon monoxide</td>
		</tr>
		<tr>
			<td>UTI</td>
			<td>urinary tract infection</td>
		</tr>
		<tr>
			<td>VIP</td>
			<td>vaso-active intestinal polypeptide</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<strong>Units</strong></td>
		</tr>
		<tr>
			<td>cm</td>
			<td>centimetre(s)</td>
		</tr>
		<tr>
			<td>cps</td>
			<td>cycles per sec</td>
		</tr>
		<tr>
			<td>dL</td>
			<td>decilitre(s)</td>
		</tr>
		<tr>
			<td>g</td>
			<td>gram(s)</td>
		</tr>
		<tr>
			<td>Gy</td>
			<td>Gray(s), a measure of radiation</td>
		</tr>
		<tr>
			<td>h</td>
			<td>hour(s)</td>
		</tr>
		<tr>
			<td>Hg</td>
			<td>mercury</td>
		</tr>
                <tr>
			<td>kcal</td>
			<td>kilocalories</td>
		</tr>
		<tr>
			<td>kg</td>
			<td>kilogram(s)</td>
		</tr>
		<tr>
			<td>L</td>
			<td>litre(s)</td>
		</tr>
		<tr>
			<td>mg</td>
			<td>milligram(s)</td>
		</tr>
		<tr>
			<td>micromol</td>
			<td>micromole(s)</td>
		</tr>
		<tr>
			<td>ml</td>
			<td>millilitre(s)</td>
		</tr>
		<tr>
			<td>mm</td>
			<td>millimetre(s)</td>
		</tr>
		<tr>
			<td>mmol</td>
			<td>millimole(s)</td>
		</tr>
		<tr>
			<td>min</td>
			<td>minute(s)</td>
		</tr>
		<tr>
			<td>mosmol</td>
			<td>milli-osmole(s)</td>
		</tr>
		<tr>
			<td>msec</td>
			<td>millisecond</td>
		</tr>
		<tr>
			<td>nm</td>
			<td>nanometre(s)</td>
		</tr>
		<tr>
			<td>nmol</td>
			<td>nanomole(s); alternative, nM</td>
		</tr>
		<tr>
			<td>sec</td>
			<td>second(s)</td>
		</tr>
	</tbody>
</table>
<p>
&nbsp;
</p>

			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
