<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"252";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:18:"Terms & conditions";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:20:"terms-and-conditions";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:21485:"<h1>TERMS AND CONDITIONS OF USE</h1>

<p>Thank you for visiting our site.</p>
<p>This section tells you the terms of use on which you may make use of our website www.palliativedrugs.com ("our site"), whether as a guest or a registered user. Please read these terms of use carefully before you start to use our site. By using our site, you indicate that you accept these terms of use and that you agree to abide by them. If you do not agree to these terms of use, please refrain from using our site.</p>

<h3>Information about us</h3>
<p>www.palliativedrugs.com is operated by Palliativedrugs.com Limited ("We"). We are registered in England and Wales under company number 03920371 and have our registered office and main trading address at The Study Centre, Hayward House, Nottingham University Hospitals NHS Trust, Hucknall Road, Nottingham, Nottinghamshire NG5 1PB. Our VAT number is 763-5239-18.</p>

<h3>Accessing our site</h3>
<p>Access to our site is free of charge.</p>
<p>Access to our site is permitted on a temporary basis, and we reserve the right to withdraw or amend the resources we provide on our site without notice (see below). We will not be liable to you if for any reason our site is unavailable at any time or for any period.</p>
<p>As a guest, you may browse our site, but full access to the entire site, including: (a) the <a href="palliative-care-formulary.html">Palliative Care Formulary</a>; (b) the <a href="bulletin-board-introduction.html">Bulletin board</a> forum ("Forum"); and (c) our <a href="rag-panel.html">Document library</a> ("Document library"); (collectively "the Resources") is restricted to users who have registered with us.</p>
<p>When using our site, you must comply with the provisions of these terms of use.</p>
<p>You are responsible for making all arrangements necessary for you to have access to our site.</p>

<a name="eligibility-criteria"></a>
<h3>Registering with our site / eligibility criteria</h3>
<p>You can only register with our site if you are eligible, i.e. <strong>you are a health professional, whether qualified, working in an identifiable clinical or academic setting or in training at an identifiable higher education institute</strong>. If you are eligible and wish to register, click on the register button on the home page and follow the instructions. Information that you provide while registering with our site must be complete and accurate to help us to validate your eligibility for membership.</p>
<p>
Before you submit your registration details you will have an opportunity to identify and correct any input errors in your registration for our site.</p>
<p>
After you submit your registration for our site we will send you a copy of the information you have submitted, using the e-mail you have provided during the registration process. Please print out this acknowledgement e-mail for your future reference. This e-mail will be an acknowledgement of the submission of your registration details and not the validation of your eligibility.</p>
<p>
Once you have completed the registration process, we will give you immediate access to the Resources and other restricted parts of our site by activating your account. If upon processing your registration details we discover that you do not meet our eligibility criteria, we cannot verify the information you have provided or you do not provide the information we require, we may immediately suspend your account and access to the Resources and other restricted parts of our site without notice to you.
As part of your registration, a unique registration number will be assigned to your entry on our database that will be used to identify you.</p>
<p>
When registering with our site, you choose a username and password. You must treat such information as confidential, and you must not disclose it to any third party. You agree to indemnify us against any losses or damages incurred by us as a result of your failure to comply with this confidentiality obligation.</p>
<p>
You can terminate your registration with our site at any time by sending an e-mail to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>

<h3>Our surveys</h3>
<p>
Our site is a free resource. To keep it free we must generate revenue from other sources. One of the ways we generate revenue is by having our membership complete surveys based on their specialist area. Given our desire to remain a free resource, it is a condition of your registration with our site that you allow us to send you invitations to take part in surveys we believe will be of interest to you or which we believe will be benefited by your participation. However, you are not obliged to take part and may reject the invitation. The terms and conditions of participation for each survey will be provided with the invitation for that survey.</p>

<h3>Intellectual property rights</h3>
<p>
Except as otherwise stated on our site:</p>
<ul>
<li>we are the owner or the licensee of all intellectual property rights in our site, and in the material published on it. These works are protected by copyright laws and treaties around the world. All such rights are reserved;</li>
<li>you may print off one copy, and may download extracts, of any page(s) from our site for your personal reference and you may draw the attention of others within your organisation to material posted on our site;</li>
<li>you must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text; and</li>
<li>you must not use any part of the materials on our site for commercial purposes without obtaining a licence to do so from us or the identified contributor of the material.</li>
</ul>
<p>
Our status (and that of any identified contributors) as the authors of material on our site must always be acknowledged.</p>
<p> 
If you print off, copy or download any part of our site in breach of these terms of use (or any documents referred to in them), your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>

<h3>Submission of contributions to the Forum or Document library</h3>
<p>
For instructions on how to add a contribution to the Forum see the <a href="faq.html">FAQ</a> section. To add a contribution to the Document library, please send a word or pdf of the document to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>. Your name, location and professional category will appear as part of your contribution.</p>
<p>
We have the right to decline or remove any material or posting you make on our site if, in our opinion, such material does not comply with the Content standards below.</p>

<h3>Content standards for contributions to the Forum and Document library</h3>
<p>
These content standards ("Content standards") apply to the contribution you make to the Forum or Document library and they must be complied with in spirit as well as to the letter. We will determine, in our discretion, whether a contribution breaches the Content standards. A contribution must:</p>
<ul>
<li>be accurate (where it states facts);</li> 
<li>be genuinely held (where it states opinions);</li>
<li>comply with the law applicable in England and Wales and in any country from which it is posted; and</li>
<li>be relevant.</li>
</ul>
<p>
A contribution must not:</p>
<ul>
<li>be defamatory of any person;</li>
<li>be obscene, offensive, hateful or inflammatory;</li> 
<li>promote discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</li> 
<li>disclose the name, address, telephone, mobile or fax number, e-mail address or any other personal data in respect of any individual (except with the specific consent of the person to whom the personal information relates);</li> 
<li>infringe any copyright, database right, trade mark or other intellectual property right of any other person;</li> 
<li>breach any legal duty owed to a third party, such as a contractual duty or a duty of confidence;</li> 
<li>be in contempt of court;</li> 
<li>be likely to harass, upset, embarrass, alarm or annoy any other person; impersonate any person, or misrepresent your identity or affiliation with any person;</li> 
<li>give the impression that the contribution emanates from another person if this is not the case; advocate, promote, incite any third party to commit, or assist any unlawful or criminal act;</li> 
<li>contain a statement which you know or believe, or have reasonable grounds for believing, that those to whom the statement is, or is to be, published are likely to understand as a direct or indirect encouragement or other inducement to the commission, preparation or instigation of acts of terrorism; or</li>
<li>contain any advertising or promote any services or web links to other sites.</li>
</ul>
<p> 
Although the Content standards exist, a contribution on the Forum or the Document library, or both, may at any given time not comply with the Content standards. Accordingly, the presence of a contribution on the Forum or Document library, or both, is not to be taken as signifying the compliance of any contribution with the Content standards.</p>

<h3>Your use of contributions</h3> 
<p>
By submitting a contribution to the Forum or the Document library, or both, you grant:</p>
<ul>
<li>to us a perpetual, world-wide, non-exclusive, royalty free licence to host your contribution on our site and make it available to our registered users from time to time; and</li>
<li>to each of our registered users from time to time a perpetual, world-wide, non-exclusive, royalty free licence to print off one copy of, and download extracts of, your contribution from our site for their personal reference.</li>
</ul>
<p>
Note that, in accordance with the Content standards, you must ensure that your contribution does not infringe any copyright, database right, trade mark or other intellectual property right of any other person or any obligation of confidence.</p>
<p>
By submitting your contribution to the Forum or the Document library, or both, you are warranting that you have the right to grant to us and to our registered users from time to time the licence described above. If you are not in a position to grant such a licence to us and our registered users from time to time, please do not submit the contribution to the Forum or the Document library, or both.</p>

<h3>Moderation of contributions you submit to the Forum or Document library</h3>
<p>
Any contribution submitted to the Forum or the Document library, or both, is the sole responsibility of the person from whom the contribution has originated. This means that you, and not we, are entirely responsible for any contribution that you submit to the Forum or Document library, or both. Accordingly, the Forum and Document library are not moderated or contributions evaluated and we will not be responsible as author, editor or publisher of any contribution submitted to either the Forum or Document library and we expressly exclude our liability for any loss or damage arising from your use of the Forum or Document library, or both, or of any other contravention of these terms. We reserve the right to promptly remove any contribution which we deem to be potentially defamatory of any person, unlawful, in breach of any third party rights or in breach of these terms of use.</p> 

<h3>Complaints</h3>
<p>
If you wish to complain about any contribution to the Forum or the Document library, or both, please contact us at <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>. We will then review the contribution and decide whether it complies with these terms. We will deal with any contribution which, in our opinion, breaches these terms as described in the "Breach of these terms" section below. We will inform you of the outcome of our review within a reasonable time of receiving your complaint.</p>

<h3>Reliance on information posted</h3>
<p>
Commentary and other materials posted on our site (whether as part of the Resources or otherwise) are not intended to amount to information or advice on which reliance should be placed. We therefore disclaim all liability and responsibility arising from any reliance placed on such materials by any user of our site, or by anyone who may be informed of any of its contents. Our site is intended for health professionals who require access to the information on it for their professional or educational work. Therefore, you must satisfy yourself as to suitability and appropriateness of any information obtained through our site (including through the Resources) before using it or relying on it. Information on our site is not suitable for diagnosis, self-diagnoses or dispensing without expert evaluation of the information on which you are relying.</p>
<p>
The information contained on our site may relate to possible effects and uses of certain drugs outside the scope of their licensed use. Any reference to the use of a drug which is outside its licensed use must not be taken as an indication or recommendation that such a use is an authorised or proper use of that drug in that manner.  We shall not be held liable for any damage resulting from any treatment, use, administration, further procurement involving the use of any drug outside the scope of its licensed use or otherwise.</p>

<h3>Breach of these terms</h3>
<p>
When we consider that a breach of the Content standards has occurred, we may at our discretion take such action as we deem appropriate. Failure to comply with these terms constitutes a material breach of the terms of use on which you are permitted to use the Forum and Document library, and may result in our taking all or any of the following actions:</p>
<ul>
<li>immediate, temporary or permanent withdrawal of your right to use the Forum or the Document library, or both;</li> 
<li>immediate, temporary or permanent removal of any contribution on the Forum or Document library, or both;</li> 
<li>issue of a warning to you;</li> 
<li>legal proceedings against you for reimbursement of all costs on an indemnity basis (including, but not limited to, reasonable administrative and legal costs) resulting from the breach;</li> 
<li>further legal action against you; or</li> 
<li>disclosure of such information to law enforcement authorities as we reasonably feel is necessary or as required by law.</li>
</ul>
<p>
We exclude our liability for all action we may take in response to breaches of these terms. The actions described above are not limited, and we may take any other action we reasonably deem appropriate.</p>
<p>
You agree to indemnify us against any liability we incur arising from you making a contribution on the Forum or the Document library, or both, that is in breach of our Content standards.</p>
<p>
Notwithstanding and without prejudice to our rights noted above, we reserve the right to terminate your access to the Resources and restricted areas of our site on notice for any reason, including, if we believe in our opinion you have failed to comply with any of the provisions of these terms of use (or any of the documents referred to in them).</p>

<h3>Our site changes regularly</h3>
<p>
We take reasonable care to ensure that the information on our site is accurate. We aim to update our site regularly, and may change the content at any time. However, any of the material on our site may be out of date at any given time, and we are under no obligation to update such material. Therefore, you must satisfy yourself as to correctness and accuracy of any information obtained through our site before using it or relying on it. If the need arises, we may suspend access to our site, or close it indefinitely.</P>

<h3>Our liability</h3>
<p>
The material displayed on our site is provided without any guarantees, conditions or warranties as to its accuracy or appropriateness. To the extent permitted by law, we expressly exclude:</p>
<ul>
<li>all conditions, warranties and other terms which might otherwise be implied by statute, common law or the law of equity; and</li>
<li>any liability for any direct, indirect or consequential loss or damage incurred by any user in connection with our site or in connection with the use, inability to use, or results of the use of our site, any websites linked to it and any materials posted on it, including, without limitation any liability for:</li>
</ul>
<ul class="secord"> 
<li>loss of income or revenue;</li>
<li>loss of business;</li>
<li>loss of profits or contracts;</li>
<li>loss of anticipated savings;</li>
<li>loss of data;</li>
<li>loss of goodwill;</li>
<li>wasted management or office time; and 
for any other loss or damage of any kind, however arising and whether caused by tort (including negligence), breach of contract or otherwise, even if foreseeable.</li>
</ul>
<p>
This does not affect our liability for death or personal injury arising from our negligence, nor our liability for fraudulent misrepresentation or misrepresentation as to a fundamental matter, nor any other liability which cannot be excluded or limited under applicable law.</p>

<h3>Information about you and your visits to our site</h3>
<p>
We process information about you in accordance with our <a href="privacy.html">Privacy policy</a>. By using our site, you consent to such processing and you warrant that all data provided by you is accurate.</p>

<h3>Transactions concluded through our site</h3>
<p>
Contracts with us for the supply of goods formed through our site or as a result of visits made by you are governed by our <a href="http://www.palliativebooks.com/index.php?act=viewDoc&docId=3""target=blank">Terms and conditions of sale</a>.</p>

<h3>Viruses, hacking and other offences</h3>
<p>
You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack.</p>
<p>
By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.</p>
<p>
We will not be liable for any loss or damage caused by a distributed denial-of-service attack, viruses or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of our site or to your downloading of any material posted on it, or on any website linked to it.</p>

<h3>Linking to our site</h3>
<p>
You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, but you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</p>
<p>
You must not establish a link from any website that is not owned by you.</p>
<p> 
Our site must not be framed on any other site, nor may you create a link to any part of our site other than the home page. We reserve the right to withdraw linking permission without notice. The website from which you are linking must comply in all respects with our Content standards above.</p>
<p>
If you wish to make any use of material on our site other than that set out above, please address your request to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>

<h3>Links from our site</h3>
<p>
Where our site contains links to other sites and resources provided by third parties, these links are provided for your information only. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them.</p> 

<h3>Jurisdiction and applicable law</h3>
<p>
The English courts will have exclusive jurisdiction over any claim arising from, or related to, a visit to our site although we retain the right to bring proceedings against you for breach of these Terms and conditions of use in your country of residence or any other relevant country. These terms of use are governed by English law.</p>

<h3>Changes to the terms</h3>
<p>
We may revise these terms of use at any time by amending this page. You are expected to check this page from time to time to take notice of any changes we make, as they are binding on you. We will highlight any such changes to you via your registered e-mail address. Some of the provisions contained in these terms of use may also be superseded by provisions or notices published elsewhere on our site.</p>

<h3>Your concerns</h3>
<p>
If you have any concerns about material which appears on our site, please contact <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>
<p><br /></p>

";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:2:"11";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1213929137";s:8:"editedby";s:1:"3";s:8:"editedon";s:10:"1311614816";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1213929137";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h1>TERMS AND CONDITIONS OF USE</h1>

<p>Thank you for visiting our site.</p>
<p>This section tells you the terms of use on which you may make use of our website www.palliativedrugs.com ("our site"), whether as a guest or a registered user. Please read these terms of use carefully before you start to use our site. By using our site, you indicate that you accept these terms of use and that you agree to abide by them. If you do not agree to these terms of use, please refrain from using our site.</p>

<h3>Information about us</h3>
<p>www.palliativedrugs.com is operated by Palliativedrugs.com Limited ("We"). We are registered in England and Wales under company number 03920371 and have our registered office and main trading address at The Study Centre, Hayward House, Nottingham University Hospitals NHS Trust, Hucknall Road, Nottingham, Nottinghamshire NG5 1PB. Our VAT number is 763-5239-18.</p>

<h3>Accessing our site</h3>
<p>Access to our site is free of charge.</p>
<p>Access to our site is permitted on a temporary basis, and we reserve the right to withdraw or amend the resources we provide on our site without notice (see below). We will not be liable to you if for any reason our site is unavailable at any time or for any period.</p>
<p>As a guest, you may browse our site, but full access to the entire site, including: (a) the <a href="palliative-care-formulary.html">Palliative Care Formulary</a>; (b) the <a href="bulletin-board-introduction.html">Bulletin board</a> forum ("Forum"); and (c) our <a href="rag-panel.html">Document library</a> ("Document library"); (collectively "the Resources") is restricted to users who have registered with us.</p>
<p>When using our site, you must comply with the provisions of these terms of use.</p>
<p>You are responsible for making all arrangements necessary for you to have access to our site.</p>

<a name="eligibility-criteria"></a>
<h3>Registering with our site / eligibility criteria</h3>
<p>You can only register with our site if you are eligible, i.e. <strong>you are a health professional, whether qualified, working in an identifiable clinical or academic setting or in training at an identifiable higher education institute</strong>. If you are eligible and wish to register, click on the register button on the home page and follow the instructions. Information that you provide while registering with our site must be complete and accurate to help us to validate your eligibility for membership.</p>
<p>
Before you submit your registration details you will have an opportunity to identify and correct any input errors in your registration for our site.</p>
<p>
After you submit your registration for our site we will send you a copy of the information you have submitted, using the e-mail you have provided during the registration process. Please print out this acknowledgement e-mail for your future reference. This e-mail will be an acknowledgement of the submission of your registration details and not the validation of your eligibility.</p>
<p>
Once you have completed the registration process, we will give you immediate access to the Resources and other restricted parts of our site by activating your account. If upon processing your registration details we discover that you do not meet our eligibility criteria, we cannot verify the information you have provided or you do not provide the information we require, we may immediately suspend your account and access to the Resources and other restricted parts of our site without notice to you.
As part of your registration, a unique registration number will be assigned to your entry on our database that will be used to identify you.</p>
<p>
When registering with our site, you choose a username and password. You must treat such information as confidential, and you must not disclose it to any third party. You agree to indemnify us against any losses or damages incurred by us as a result of your failure to comply with this confidentiality obligation.</p>
<p>
You can terminate your registration with our site at any time by sending an e-mail to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>

<h3>Our surveys</h3>
<p>
Our site is a free resource. To keep it free we must generate revenue from other sources. One of the ways we generate revenue is by having our membership complete surveys based on their specialist area. Given our desire to remain a free resource, it is a condition of your registration with our site that you allow us to send you invitations to take part in surveys we believe will be of interest to you or which we believe will be benefited by your participation. However, you are not obliged to take part and may reject the invitation. The terms and conditions of participation for each survey will be provided with the invitation for that survey.</p>

<h3>Intellectual property rights</h3>
<p>
Except as otherwise stated on our site:</p>
<ul>
<li>we are the owner or the licensee of all intellectual property rights in our site, and in the material published on it. These works are protected by copyright laws and treaties around the world. All such rights are reserved;</li>
<li>you may print off one copy, and may download extracts, of any page(s) from our site for your personal reference and you may draw the attention of others within your organisation to material posted on our site;</li>
<li>you must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text; and</li>
<li>you must not use any part of the materials on our site for commercial purposes without obtaining a licence to do so from us or the identified contributor of the material.</li>
</ul>
<p>
Our status (and that of any identified contributors) as the authors of material on our site must always be acknowledged.</p>
<p> 
If you print off, copy or download any part of our site in breach of these terms of use (or any documents referred to in them), your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>

<h3>Submission of contributions to the Forum or Document library</h3>
<p>
For instructions on how to add a contribution to the Forum see the <a href="faq.html">FAQ</a> section. To add a contribution to the Document library, please send a word or pdf of the document to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>. Your name, location and professional category will appear as part of your contribution.</p>
<p>
We have the right to decline or remove any material or posting you make on our site if, in our opinion, such material does not comply with the Content standards below.</p>

<h3>Content standards for contributions to the Forum and Document library</h3>
<p>
These content standards ("Content standards") apply to the contribution you make to the Forum or Document library and they must be complied with in spirit as well as to the letter. We will determine, in our discretion, whether a contribution breaches the Content standards. A contribution must:</p>
<ul>
<li>be accurate (where it states facts);</li> 
<li>be genuinely held (where it states opinions);</li>
<li>comply with the law applicable in England and Wales and in any country from which it is posted; and</li>
<li>be relevant.</li>
</ul>
<p>
A contribution must not:</p>
<ul>
<li>be defamatory of any person;</li>
<li>be obscene, offensive, hateful or inflammatory;</li> 
<li>promote discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</li> 
<li>disclose the name, address, telephone, mobile or fax number, e-mail address or any other personal data in respect of any individual (except with the specific consent of the person to whom the personal information relates);</li> 
<li>infringe any copyright, database right, trade mark or other intellectual property right of any other person;</li> 
<li>breach any legal duty owed to a third party, such as a contractual duty or a duty of confidence;</li> 
<li>be in contempt of court;</li> 
<li>be likely to harass, upset, embarrass, alarm or annoy any other person; impersonate any person, or misrepresent your identity or affiliation with any person;</li> 
<li>give the impression that the contribution emanates from another person if this is not the case; advocate, promote, incite any third party to commit, or assist any unlawful or criminal act;</li> 
<li>contain a statement which you know or believe, or have reasonable grounds for believing, that those to whom the statement is, or is to be, published are likely to understand as a direct or indirect encouragement or other inducement to the commission, preparation or instigation of acts of terrorism; or</li>
<li>contain any advertising or promote any services or web links to other sites.</li>
</ul>
<p> 
Although the Content standards exist, a contribution on the Forum or the Document library, or both, may at any given time not comply with the Content standards. Accordingly, the presence of a contribution on the Forum or Document library, or both, is not to be taken as signifying the compliance of any contribution with the Content standards.</p>

<h3>Your use of contributions</h3> 
<p>
By submitting a contribution to the Forum or the Document library, or both, you grant:</p>
<ul>
<li>to us a perpetual, world-wide, non-exclusive, royalty free licence to host your contribution on our site and make it available to our registered users from time to time; and</li>
<li>to each of our registered users from time to time a perpetual, world-wide, non-exclusive, royalty free licence to print off one copy of, and download extracts of, your contribution from our site for their personal reference.</li>
</ul>
<p>
Note that, in accordance with the Content standards, you must ensure that your contribution does not infringe any copyright, database right, trade mark or other intellectual property right of any other person or any obligation of confidence.</p>
<p>
By submitting your contribution to the Forum or the Document library, or both, you are warranting that you have the right to grant to us and to our registered users from time to time the licence described above. If you are not in a position to grant such a licence to us and our registered users from time to time, please do not submit the contribution to the Forum or the Document library, or both.</p>

<h3>Moderation of contributions you submit to the Forum or Document library</h3>
<p>
Any contribution submitted to the Forum or the Document library, or both, is the sole responsibility of the person from whom the contribution has originated. This means that you, and not we, are entirely responsible for any contribution that you submit to the Forum or Document library, or both. Accordingly, the Forum and Document library are not moderated or contributions evaluated and we will not be responsible as author, editor or publisher of any contribution submitted to either the Forum or Document library and we expressly exclude our liability for any loss or damage arising from your use of the Forum or Document library, or both, or of any other contravention of these terms. We reserve the right to promptly remove any contribution which we deem to be potentially defamatory of any person, unlawful, in breach of any third party rights or in breach of these terms of use.</p> 

<h3>Complaints</h3>
<p>
If you wish to complain about any contribution to the Forum or the Document library, or both, please contact us at <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>. We will then review the contribution and decide whether it complies with these terms. We will deal with any contribution which, in our opinion, breaches these terms as described in the "Breach of these terms" section below. We will inform you of the outcome of our review within a reasonable time of receiving your complaint.</p>

<h3>Reliance on information posted</h3>
<p>
Commentary and other materials posted on our site (whether as part of the Resources or otherwise) are not intended to amount to information or advice on which reliance should be placed. We therefore disclaim all liability and responsibility arising from any reliance placed on such materials by any user of our site, or by anyone who may be informed of any of its contents. Our site is intended for health professionals who require access to the information on it for their professional or educational work. Therefore, you must satisfy yourself as to suitability and appropriateness of any information obtained through our site (including through the Resources) before using it or relying on it. Information on our site is not suitable for diagnosis, self-diagnoses or dispensing without expert evaluation of the information on which you are relying.</p>
<p>
The information contained on our site may relate to possible effects and uses of certain drugs outside the scope of their licensed use. Any reference to the use of a drug which is outside its licensed use must not be taken as an indication or recommendation that such a use is an authorised or proper use of that drug in that manner.  We shall not be held liable for any damage resulting from any treatment, use, administration, further procurement involving the use of any drug outside the scope of its licensed use or otherwise.</p>

<h3>Breach of these terms</h3>
<p>
When we consider that a breach of the Content standards has occurred, we may at our discretion take such action as we deem appropriate. Failure to comply with these terms constitutes a material breach of the terms of use on which you are permitted to use the Forum and Document library, and may result in our taking all or any of the following actions:</p>
<ul>
<li>immediate, temporary or permanent withdrawal of your right to use the Forum or the Document library, or both;</li> 
<li>immediate, temporary or permanent removal of any contribution on the Forum or Document library, or both;</li> 
<li>issue of a warning to you;</li> 
<li>legal proceedings against you for reimbursement of all costs on an indemnity basis (including, but not limited to, reasonable administrative and legal costs) resulting from the breach;</li> 
<li>further legal action against you; or</li> 
<li>disclosure of such information to law enforcement authorities as we reasonably feel is necessary or as required by law.</li>
</ul>
<p>
We exclude our liability for all action we may take in response to breaches of these terms. The actions described above are not limited, and we may take any other action we reasonably deem appropriate.</p>
<p>
You agree to indemnify us against any liability we incur arising from you making a contribution on the Forum or the Document library, or both, that is in breach of our Content standards.</p>
<p>
Notwithstanding and without prejudice to our rights noted above, we reserve the right to terminate your access to the Resources and restricted areas of our site on notice for any reason, including, if we believe in our opinion you have failed to comply with any of the provisions of these terms of use (or any of the documents referred to in them).</p>

<h3>Our site changes regularly</h3>
<p>
We take reasonable care to ensure that the information on our site is accurate. We aim to update our site regularly, and may change the content at any time. However, any of the material on our site may be out of date at any given time, and we are under no obligation to update such material. Therefore, you must satisfy yourself as to correctness and accuracy of any information obtained through our site before using it or relying on it. If the need arises, we may suspend access to our site, or close it indefinitely.</P>

<h3>Our liability</h3>
<p>
The material displayed on our site is provided without any guarantees, conditions or warranties as to its accuracy or appropriateness. To the extent permitted by law, we expressly exclude:</p>
<ul>
<li>all conditions, warranties and other terms which might otherwise be implied by statute, common law or the law of equity; and</li>
<li>any liability for any direct, indirect or consequential loss or damage incurred by any user in connection with our site or in connection with the use, inability to use, or results of the use of our site, any websites linked to it and any materials posted on it, including, without limitation any liability for:</li>
</ul>
<ul class="secord"> 
<li>loss of income or revenue;</li>
<li>loss of business;</li>
<li>loss of profits or contracts;</li>
<li>loss of anticipated savings;</li>
<li>loss of data;</li>
<li>loss of goodwill;</li>
<li>wasted management or office time; and 
for any other loss or damage of any kind, however arising and whether caused by tort (including negligence), breach of contract or otherwise, even if foreseeable.</li>
</ul>
<p>
This does not affect our liability for death or personal injury arising from our negligence, nor our liability for fraudulent misrepresentation or misrepresentation as to a fundamental matter, nor any other liability which cannot be excluded or limited under applicable law.</p>

<h3>Information about you and your visits to our site</h3>
<p>
We process information about you in accordance with our <a href="privacy.html">Privacy policy</a>. By using our site, you consent to such processing and you warrant that all data provided by you is accurate.</p>

<h3>Transactions concluded through our site</h3>
<p>
Contracts with us for the supply of goods formed through our site or as a result of visits made by you are governed by our <a href="http://www.palliativebooks.com/index.php?act=viewDoc&docId=3""target=blank">Terms and conditions of sale</a>.</p>

<h3>Viruses, hacking and other offences</h3>
<p>
You must not misuse our site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of service attack.</p>
<p>
By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.</p>
<p>
We will not be liable for any loss or damage caused by a distributed denial-of-service attack, viruses or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of our site or to your downloading of any material posted on it, or on any website linked to it.</p>

<h3>Linking to our site</h3>
<p>
You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it, but you must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</p>
<p>
You must not establish a link from any website that is not owned by you.</p>
<p> 
Our site must not be framed on any other site, nor may you create a link to any part of our site other than the home page. We reserve the right to withdraw linking permission without notice. The website from which you are linking must comply in all respects with our Content standards above.</p>
<p>
If you wish to make any use of material on our site other than that set out above, please address your request to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>

<h3>Links from our site</h3>
<p>
Where our site contains links to other sites and resources provided by third parties, these links are provided for your information only. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them.</p> 

<h3>Jurisdiction and applicable law</h3>
<p>
The English courts will have exclusive jurisdiction over any claim arising from, or related to, a visit to our site although we retain the right to bring proceedings against you for breach of these Terms and conditions of use in your country of residence or any other relevant country. These terms of use are governed by English law.</p>

<h3>Changes to the terms</h3>
<p>
We may revise these terms of use at any time by amending this page. You are expected to check this page from time to time to take notice of any changes we make, as they are binding on you. We will highlight any such changes to you via your registered e-mail address. Some of the provisions contained in these terms of use may also be superseded by provisions or notices published elsewhere on our site.</p>

<h3>Your concerns</h3>
<p>
If you have any concerns about material which appears on our site, please contact <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.</p>
<p><br /></p>


			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
