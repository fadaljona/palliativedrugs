<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"282";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:12:"Testimonials";s:9:"longtitle";s:29:"Testimonials and book reviews";s:11:"description";s:29:"Testimonials and book reviews";s:5:"alias";s:12:"testimonials";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:17528:"<h3><a href="#site">Palliativedrugs.com website</a></h3>
<h3><a href="#pcf4">Palliative Care Formulary UK 4th edition</a></h3>
<h3><a href="#HPCF">Hospice and Palliative Care Formulary USA 2nd edition</a></h3>
<h3><a href="#PCFCanadian">Palliative Care Formulary Canadian edition</a></h3>
<h3><a href="#SMAC">Symptom Management in Advanced Cancer 4th edition</a></h3><br />
<hr />
<br />
<h3><strong><a name="site" title="site"></a>Palliativedrugs.com website</strong></h3>
<p>
Palliativedrugs.com was 10 years old in 2010 and here's what you've told us recently: 
</p>
<p class="quote">
I truly feel this site is fantastic! It has been a wonderful resource and a great aid in teaching my staff. Thank you.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
I&rsquo;m just glad you are there!<br />
<em>Doctor, USA</em> 
</p>
<p class="quote">
Excellent site, really useful resource. Have recommended it to many.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
An excellent, informative and clinically useful site. I like the 'drug physiology' in the formulary - useful to jog my memory as to how certain drugs work.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I think the site is invaluable. I use it regularly to keep in touch with what is happening and also learn from other practitioners experiences. Thank you and keep up the great work.<br />
<em>Nurse, Australia</em> 
</p>
<p class="quote">
I am very new to your services but have been EXTREMELY impressed thus far! Thank you for making this information available.<br />
<em>Pharmacist, Canada</em> 
</p>
<p class="quote">
This is such a useful site - I cannot imagine what I did without it. <br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
I have introduced colleagues and students to the site and am very grateful to have access to such a wonderful resource - thank you and keep up the good work!<br />
<em>Nurse, Australia</em> 
</p>
<p class="quote">
Goes from strength to strength. Well done. An excellent resource for primary care.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
Love it.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
Very useful source to provide palliative care information that can be passed onto hospice, hospital and community colleagues. Thanks.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Very useful. Thank you.<br />
<em>Doctor, India</em> 
</p>
<p class="quote">
The on-line versions of the compatibility charts are easier to navigate than the version in the book. Excellent.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Extremely useful resource. Thank you for hosting it and continuing great support for those of us in the community.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I am grateful for this web site. The experience of those who frequently write enriches my fund of knowledge.<br />
<em>Doctor, USA</em> 
</p>
<p class="quote">
Wonderful having a database updated by experts from all over the world. When I have posted questions I have had excellent advice.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
I advise all my colleagues to visit your site and also read the questions and comments. Please, continue!<br />
<em>Doctor, Netherlands</em> 
</p>
<p class="quote">
Palliativedrugs is an excellent resource that supports my practice. The updated sections in the PCF3 are really helpful, thanks.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
The bulletin board provides a valuable learning resource.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Great site. Just love it. I have suggested it to all I have worked with - very good feedback from them all.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
The site is great, I use it often for my clinical work. The formulary is just excellent, really useful and relevant information and so impressively kept up to date, which must be a mammoth task.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
A truly world-wide network which provides an excellent forum.<br />
<em>Doctor, Australia</em> 
</p>
<p class="quote">
Palliativedrugs is a fantastic resource - I am new to the field of Palliative Medicine and am finding it very helpful.<br />
<em>Doctor, Australia</em> 
</p>
<p class="quote">
Extremely useful website - I use it often and encourage other pharmacists to use it.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
How did we manage before the PCF??? Thank you to authors and webmasters!<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I think this is the most useful resource that we as palliative care professionals have available and is highly respected as an excellent source of information generally. Keep up the good work and thank you.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Overall a useful resource and I love the PCF!<br />
<em>Doctor, Australia</em> 
</p>
<p class="quote">
A wonderful resource for the specialty - trusted and relevant.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I&rsquo;d like to complement you and anyone else involved on palliativedrugs.com. It is a really excellent resource. I started working in palliative care last April, and have learnt so much from this site.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="helppull">
Help support palliativedrugs.com by telling your colleagues about this website.<br />
We have created a pdf flyer suitable for e-mailing or printing to help you to do this.<br />
<a class="downloadweb" href="downloadweb.php">Download PDF</a> 
</p>
<p>
&nbsp;
</p>
<h3><a id="pcf4" name="pcf4" title="pcf4"></a>Palliative Care Formulary 4th edition (UK)</h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=17"><img align="left" alt="Palliative Care Formulary 4th edition" border="0" id="pcf4" name="pcf4" src="assets/pcf4/pcf4cover.jpg" /></a> <span class="prelim"><a href="SamplePDF/PCF4/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('pcf4/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample seventeen"><a href="SamplePDF/PCF4/Chapter17.pdf" onclick="javascript: pageTracker._trackPageview('pcf4/sample.pdf');" target="_blank">View chapter 17</a></span> <span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=282" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
PCF is a comprehensive compendium containing a wealth of essential therapeutic information about drugs used in palliative and hospice care. With cumulative global sales of over 100 000 and the third edition highly commended in the British Medical Association book competition, PCF is the established resource in its field and receives worldwide acclaim. 
</p>
<p>
No other publication embraces the range and depth of information contained in the PCF. It is indispensable for all doctors, nurses and pharmacists who provide palliative care, and Trust formulary committees. It is an essential resource for all departments and centres caring for patients with cancer or other progressive end-stage disease. 
</p>
<p>
This expanded forth edition incorporates numerous important updates and new data, bringing together a wealth of important information about drugs commonly used in palliative care and about drugs for use in special circumstances by, or in conjunction with, a specialist in palliative care. In all, PCF4 is 200 pages longer than the previous edition, equating to 30% more information. 
</p>
<p>
PCF provides the most authoritative and thorough guide to off-label indications or routes for drugs used in palliative care, and deals comprehensively with giving multiple drugs by continuous subcutaneous infusion. 
</p>
<p>
Previous editions translated in German, Italian, Japanese and Polish. 
</p>
<p class="quote">
'Just occasionally you pick up a book which makes you wonder how you ever managed without it. PCF is one of those happy occasions.'<br />
<em>Palliative Medicine</em> 
</p>
<p class="quote">
'Accessible, clear and extremely well referenced, the PCF is a succinct pharmacological text for palliative care physicians and nurses.'<br />
<em>Hospice Bulletin</em> 
</p>
<p class="quote">
'First class. Highly recommended.'<br />
<em>International Association for Hospice and Palliative Care</em> 
</p>
<p class="quote">
'A must.'<br />
<em>Hospital Doctor </em>
</p>
<p class="quote">
&lsquo;The PCF has always been, in my opinion, a core text for the specialty and has no serious rivals.&rsquo;<br />
<em>Palliative Medicine</em> 
</p>
<p class="helppull">
Help support palliativedrugs.com by telling your colleagues about Palliative Care Formulary 4th edition. We have created a pdf flyer suitable for e-mailing or printing to help you to do this.<br />
<a class="downloadpcf4" href="downloadPCF4e.php">Download PDF</a> 
</p>
</div>
<p>
&nbsp;
</p>
<h3><a name="HPCF" title="HPCF"></a>Hospice and Palliative Care Formulary USA 2nd edition </h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=19"><img align="left" alt="HPCF USA" border="0" id="hpcf" name="hpcf" src="SamplePDF/hpcf-cover.png" /></a> <span class="prelim"><a href="SamplePDF/HPCF2/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('hpcf/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample"><a href="SamplePDF/HPCF2/Chapter1.pdf" onclick="javascript: pageTracker._trackPageview('hpcf/sample.pdf');" target="_blank">View chapter 1</a></span> <span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=19" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
HPCF USA is a comprehensive compendium which brings together a wealth of essential therapeutic information about drugs commonly used in hospice and palliative care, and about drugs for use in special circumstances by (or in conjunction with) specialists in hospice and palliative care. 
</p>
<p>
It has been developed from the British Palliative Care Formulary specifically for health professionals working in the USA. With cumulative global sales approaching 100,000 copies, the Formulary is an established resource which has received widespread acclaim. This expanded second edition incorporates numerous updates, providing a wealth of important new information. 
</p>
<p>
HPCF USA is unparalleled in the range and depth of information it contains. It is an essential resource also for oncologists and other specialists who care for patients with advanced cancer or other progressive end-stage disease. 
</p>
<p>
HPCF USA highlights drugs given for off-label indications or routes, and deals comprehensively with the administration of multiple drugs by continuous subcutaneous infusion. 
</p>
<p class="quote">
&lsquo;The information is up to date and prepared with extreme care. This handbook should be available to staff involved with inpatient consultation, inpatient units of care, and home visits.&rsquo; <br />
<em>Journal of Pain and Symptom Management</em> 
</p>
<p class="quote">
&lsquo;...provides just about all you need to know about prescribing medications in palliative care.&rsquo; <br />
<em>International Association for Hospice and Palliative Care</em> 
</p>
<p class="quote">
&quot;Invaluable resource for all professionals in the US caring for patients approaching the end of life. Every hospital, pharmacy, hospice should have one.&quot;<br />
<em>Dr. Gary R. Johnson DVM, DO, Assistant Clinical Professor at John Burns School of Medicine, University of Hawai'i, USA</em> 
</p>
<p class="quote">
&quot;I have put the book into service and I love it. It's truly a most useful and engaging piece of work. I'm going to buy several more to give as Christmas gifts!&quot;<br />
<em>Stanley M. Hall, APRN-BC, FNP, ACHPN,&nbsp;Department of Palliative Medicine, St. Luke's Health System of Idaho, USA</em> 
</p>
<p>
Help support palliativedrugs.com by telling your colleagues about HPCF USA. 
</p>
</div>
<p>
&nbsp;
</p>
<h3><a id="PCFCanadian" name="PCFCanadian" title="PCFCanadian"></a>Palliative Care Formulary Canadian edition</h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=281"><img align="left" alt="Palliative Care Formulary Canadian Edition" border="0" height="297" id="smac" name="smac" src="images/PCFcnlrg.png" width="178" /></a><span class="prelim"><a href="SamplePDF/PCFcanadian/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('pcfcanadian/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample"><a href="SamplePDF/PCFcanadian/Chapter1.pdf" onclick="javascript: pageTracker._trackPageview('pcfcanadian/Chapter1.pdf');" target="_blank">View chapter 1</a></span><span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=281" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
PCF is a comprehensive compendium which brings together a wealth of essential therapeutic information about drugs commonly used in hospice and palliative care, and about drugs for use in special circumstances by (or in conjunction with) specialists in hospice and palliative care. 
</p>
<p>
This edition has been prepared specifically for health professionals in Canada. The corresponding UK edition, was highly commended in the Medical Book Competition of the British Medical Association. With cumulative global sales of 100,000 copies, the Formulary is an established resource which has received widespread acclaim. 
</p>
<p>
PCF is unparalleled in the range and depth of information it contains. It is an essential resource also for oncologists and other specialists who care for patients with advanced cancer or other progressive end-stage disease. 
</p>
<p>
PCF highlights drugs given for off-label indications or routes, and deals comprehensively with the administration of multiple drugs by continuous subcutaneous infusion. 
</p>
<p class="quote">
&lsquo;Since its first edition in 1998, PCF has established itself as one of the essential books for any healthcare professional involved in palliative care... PCF [is] a core text for the specialty and has no serious rivals.&rsquo; Palliative Medicine 
</p>
<p class="quote">
&lsquo;The information is up to date and prepared with extreme care. This handbook should be available to staff involved with inpatient consultation, inpatient units of care, and home visits.&rsquo; Journal of Pain and Symptom Management 
</p>
<p class="quote">
&lsquo;First class. Highly recommended.&rsquo; International Association for Hospice and Palliative Care 
</p>
<p class="helppull">
Help support palliativedrugs.com by telling your colleagues about PCF Canadian edition. We have created a pdf flyer suitable for e-mailing or printing to help you to do this.<br />
<a class="downloadhpcfca" href="downloadPCFca.php">Download PDF</a> 
</p>
</div>
<p>
&nbsp;
</p>
<h3><a id="SMAC" name="SMAC" title="SMAC"></a>Symptom Management in Advanced Cancer 4th edition</h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=280"><img align="left" alt="Symptom Management in Advanced Cancer" border="0" id="smac" name="smac" src="SamplePDF/smac-cover.png" width="178" /></a> <span class="prelim"><a href="SamplePDF/SMAC4e/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('smac/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample thirteen"><a href="SamplePDF/SMAC4e/Chapter13.pdf" target="_blank">View chapter 1</a></span> <span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=280" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
Symptom Management in Advanced Cancer is an established text which provides a framework of knowledge to enable clinicians to develop a systematic and scientific approach to treatment. 
</p>
<p>
It offers practical information and advice on the wide range of symptoms encountered in patients with advanced cancer. 
</p>
<p>
This edition has been extensively revised and expanded, and has a new chapter on Last Days. 
</p>
<p>
Japanese translation rights licensed to IGAKU-SHOIN, previous editions translated in German, Japanese and Romanian. 
</p>
<p class="quote">
&lsquo;A masterly work: clear, concise and practical.&rsquo; <br />
<em>Age and Ageing</em> 
</p>
<p class="quote">
&lsquo;A masterpiece&hellip;an essential book.&rsquo;<br />
<em>Geriatric Medicine</em> 
</p>
<p class="quote">
&lsquo;Without doubt the best of the numerous offerings on this subject. It is up to date, the information flows logically and the reader acquires concrete information in a few minutes. I have no reservations about recommending it.&rsquo;<br />
<em>Oncology in Practice</em> 
</p>
<p class="quote">
&lsquo;Should be within arm&rsquo;s reach of all clinicians involved in the management of patients with advanced cancer.&rsquo;<br />
<em>Palliative Medicine</em> 
</p>
<p class="quote">
&lsquo;Every general practitioner, hospital doctor (medical and surgical) and especially oncologist and palliative care physicians should have this book on their shelf.&rsquo; <em><br />
British Journal of Hospital Medicine</em> 
</p>
</div>
";s:8:"richtext";s:1:"1";s:8:"template";s:2:"18";s:9:"menuindex";s:2:"18";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1264673804";s:8:"editedby";s:1:"3";s:8:"editedon";s:10:"1322918780";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1264673804";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h3><a href="#site">Palliativedrugs.com website</a></h3>
<h3><a href="#pcf4">Palliative Care Formulary UK 4th edition</a></h3>
<h3><a href="#HPCF">Hospice and Palliative Care Formulary USA 2nd edition</a></h3>
<h3><a href="#PCFCanadian">Palliative Care Formulary Canadian edition</a></h3>
<h3><a href="#SMAC">Symptom Management in Advanced Cancer 4th edition</a></h3><br />
<hr />
<br />
<h3><strong><a name="site" title="site"></a>Palliativedrugs.com website</strong></h3>
<p>
Palliativedrugs.com was 10 years old in 2010 and here's what you've told us recently: 
</p>
<p class="quote">
I truly feel this site is fantastic! It has been a wonderful resource and a great aid in teaching my staff. Thank you.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
I&rsquo;m just glad you are there!<br />
<em>Doctor, USA</em> 
</p>
<p class="quote">
Excellent site, really useful resource. Have recommended it to many.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
An excellent, informative and clinically useful site. I like the 'drug physiology' in the formulary - useful to jog my memory as to how certain drugs work.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I think the site is invaluable. I use it regularly to keep in touch with what is happening and also learn from other practitioners experiences. Thank you and keep up the great work.<br />
<em>Nurse, Australia</em> 
</p>
<p class="quote">
I am very new to your services but have been EXTREMELY impressed thus far! Thank you for making this information available.<br />
<em>Pharmacist, Canada</em> 
</p>
<p class="quote">
This is such a useful site - I cannot imagine what I did without it. <br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
I have introduced colleagues and students to the site and am very grateful to have access to such a wonderful resource - thank you and keep up the good work!<br />
<em>Nurse, Australia</em> 
</p>
<p class="quote">
Goes from strength to strength. Well done. An excellent resource for primary care.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
Love it.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
Very useful source to provide palliative care information that can be passed onto hospice, hospital and community colleagues. Thanks.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Very useful. Thank you.<br />
<em>Doctor, India</em> 
</p>
<p class="quote">
The on-line versions of the compatibility charts are easier to navigate than the version in the book. Excellent.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Extremely useful resource. Thank you for hosting it and continuing great support for those of us in the community.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I am grateful for this web site. The experience of those who frequently write enriches my fund of knowledge.<br />
<em>Doctor, USA</em> 
</p>
<p class="quote">
Wonderful having a database updated by experts from all over the world. When I have posted questions I have had excellent advice.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
I advise all my colleagues to visit your site and also read the questions and comments. Please, continue!<br />
<em>Doctor, Netherlands</em> 
</p>
<p class="quote">
Palliativedrugs is an excellent resource that supports my practice. The updated sections in the PCF3 are really helpful, thanks.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
The bulletin board provides a valuable learning resource.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Great site. Just love it. I have suggested it to all I have worked with - very good feedback from them all.<br />
<em>Nurse, UK</em> 
</p>
<p class="quote">
The site is great, I use it often for my clinical work. The formulary is just excellent, really useful and relevant information and so impressively kept up to date, which must be a mammoth task.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
A truly world-wide network which provides an excellent forum.<br />
<em>Doctor, Australia</em> 
</p>
<p class="quote">
Palliativedrugs is a fantastic resource - I am new to the field of Palliative Medicine and am finding it very helpful.<br />
<em>Doctor, Australia</em> 
</p>
<p class="quote">
Extremely useful website - I use it often and encourage other pharmacists to use it.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
How did we manage before the PCF??? Thank you to authors and webmasters!<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I think this is the most useful resource that we as palliative care professionals have available and is highly respected as an excellent source of information generally. Keep up the good work and thank you.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="quote">
Overall a useful resource and I love the PCF!<br />
<em>Doctor, Australia</em> 
</p>
<p class="quote">
A wonderful resource for the specialty - trusted and relevant.<br />
<em>Doctor, UK</em> 
</p>
<p class="quote">
I&rsquo;d like to complement you and anyone else involved on palliativedrugs.com. It is a really excellent resource. I started working in palliative care last April, and have learnt so much from this site.<br />
<em>Pharmacist, UK</em> 
</p>
<p class="helppull">
Help support palliativedrugs.com by telling your colleagues about this website.<br />
We have created a pdf flyer suitable for e-mailing or printing to help you to do this.<br />
<a class="downloadweb" href="downloadweb.php">Download PDF</a> 
</p>
<p>
&nbsp;
</p>
<h3><a id="pcf4" name="pcf4" title="pcf4"></a>Palliative Care Formulary 4th edition (UK)</h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=17"><img align="left" alt="Palliative Care Formulary 4th edition" border="0" id="pcf4" name="pcf4" src="assets/pcf4/pcf4cover.jpg" /></a> <span class="prelim"><a href="SamplePDF/PCF4/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('pcf4/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample seventeen"><a href="SamplePDF/PCF4/Chapter17.pdf" onclick="javascript: pageTracker._trackPageview('pcf4/sample.pdf');" target="_blank">View chapter 17</a></span> <span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=282" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
PCF is a comprehensive compendium containing a wealth of essential therapeutic information about drugs used in palliative and hospice care. With cumulative global sales of over 100 000 and the third edition highly commended in the British Medical Association book competition, PCF is the established resource in its field and receives worldwide acclaim. 
</p>
<p>
No other publication embraces the range and depth of information contained in the PCF. It is indispensable for all doctors, nurses and pharmacists who provide palliative care, and Trust formulary committees. It is an essential resource for all departments and centres caring for patients with cancer or other progressive end-stage disease. 
</p>
<p>
This expanded forth edition incorporates numerous important updates and new data, bringing together a wealth of important information about drugs commonly used in palliative care and about drugs for use in special circumstances by, or in conjunction with, a specialist in palliative care. In all, PCF4 is 200 pages longer than the previous edition, equating to 30% more information. 
</p>
<p>
PCF provides the most authoritative and thorough guide to off-label indications or routes for drugs used in palliative care, and deals comprehensively with giving multiple drugs by continuous subcutaneous infusion. 
</p>
<p>
Previous editions translated in German, Italian, Japanese and Polish. 
</p>
<p class="quote">
'Just occasionally you pick up a book which makes you wonder how you ever managed without it. PCF is one of those happy occasions.'<br />
<em>Palliative Medicine</em> 
</p>
<p class="quote">
'Accessible, clear and extremely well referenced, the PCF is a succinct pharmacological text for palliative care physicians and nurses.'<br />
<em>Hospice Bulletin</em> 
</p>
<p class="quote">
'First class. Highly recommended.'<br />
<em>International Association for Hospice and Palliative Care</em> 
</p>
<p class="quote">
'A must.'<br />
<em>Hospital Doctor </em>
</p>
<p class="quote">
&lsquo;The PCF has always been, in my opinion, a core text for the specialty and has no serious rivals.&rsquo;<br />
<em>Palliative Medicine</em> 
</p>
<p class="helppull">
Help support palliativedrugs.com by telling your colleagues about Palliative Care Formulary 4th edition. We have created a pdf flyer suitable for e-mailing or printing to help you to do this.<br />
<a class="downloadpcf4" href="downloadPCF4e.php">Download PDF</a> 
</p>
</div>
<p>
&nbsp;
</p>
<h3><a name="HPCF" title="HPCF"></a>Hospice and Palliative Care Formulary USA 2nd edition </h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=19"><img align="left" alt="HPCF USA" border="0" id="hpcf" name="hpcf" src="SamplePDF/hpcf-cover.png" /></a> <span class="prelim"><a href="SamplePDF/HPCF2/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('hpcf/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample"><a href="SamplePDF/HPCF2/Chapter1.pdf" onclick="javascript: pageTracker._trackPageview('hpcf/sample.pdf');" target="_blank">View chapter 1</a></span> <span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=19" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
HPCF USA is a comprehensive compendium which brings together a wealth of essential therapeutic information about drugs commonly used in hospice and palliative care, and about drugs for use in special circumstances by (or in conjunction with) specialists in hospice and palliative care. 
</p>
<p>
It has been developed from the British Palliative Care Formulary specifically for health professionals working in the USA. With cumulative global sales approaching 100,000 copies, the Formulary is an established resource which has received widespread acclaim. This expanded second edition incorporates numerous updates, providing a wealth of important new information. 
</p>
<p>
HPCF USA is unparalleled in the range and depth of information it contains. It is an essential resource also for oncologists and other specialists who care for patients with advanced cancer or other progressive end-stage disease. 
</p>
<p>
HPCF USA highlights drugs given for off-label indications or routes, and deals comprehensively with the administration of multiple drugs by continuous subcutaneous infusion. 
</p>
<p class="quote">
&lsquo;The information is up to date and prepared with extreme care. This handbook should be available to staff involved with inpatient consultation, inpatient units of care, and home visits.&rsquo; <br />
<em>Journal of Pain and Symptom Management</em> 
</p>
<p class="quote">
&lsquo;...provides just about all you need to know about prescribing medications in palliative care.&rsquo; <br />
<em>International Association for Hospice and Palliative Care</em> 
</p>
<p class="quote">
&quot;Invaluable resource for all professionals in the US caring for patients approaching the end of life. Every hospital, pharmacy, hospice should have one.&quot;<br />
<em>Dr. Gary R. Johnson DVM, DO, Assistant Clinical Professor at John Burns School of Medicine, University of Hawai'i, USA</em> 
</p>
<p class="quote">
&quot;I have put the book into service and I love it. It's truly a most useful and engaging piece of work. I'm going to buy several more to give as Christmas gifts!&quot;<br />
<em>Stanley M. Hall, APRN-BC, FNP, ACHPN,&nbsp;Department of Palliative Medicine, St. Luke's Health System of Idaho, USA</em> 
</p>
<p>
Help support palliativedrugs.com by telling your colleagues about HPCF USA. 
</p>
</div>
<p>
&nbsp;
</p>
<h3><a id="PCFCanadian" name="PCFCanadian" title="PCFCanadian"></a>Palliative Care Formulary Canadian edition</h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=281"><img align="left" alt="Palliative Care Formulary Canadian Edition" border="0" height="297" id="smac" name="smac" src="images/PCFcnlrg.png" width="178" /></a><span class="prelim"><a href="SamplePDF/PCFcanadian/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('pcfcanadian/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample"><a href="SamplePDF/PCFcanadian/Chapter1.pdf" onclick="javascript: pageTracker._trackPageview('pcfcanadian/Chapter1.pdf');" target="_blank">View chapter 1</a></span><span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=281" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
PCF is a comprehensive compendium which brings together a wealth of essential therapeutic information about drugs commonly used in hospice and palliative care, and about drugs for use in special circumstances by (or in conjunction with) specialists in hospice and palliative care. 
</p>
<p>
This edition has been prepared specifically for health professionals in Canada. The corresponding UK edition, was highly commended in the Medical Book Competition of the British Medical Association. With cumulative global sales of 100,000 copies, the Formulary is an established resource which has received widespread acclaim. 
</p>
<p>
PCF is unparalleled in the range and depth of information it contains. It is an essential resource also for oncologists and other specialists who care for patients with advanced cancer or other progressive end-stage disease. 
</p>
<p>
PCF highlights drugs given for off-label indications or routes, and deals comprehensively with the administration of multiple drugs by continuous subcutaneous infusion. 
</p>
<p class="quote">
&lsquo;Since its first edition in 1998, PCF has established itself as one of the essential books for any healthcare professional involved in palliative care... PCF [is] a core text for the specialty and has no serious rivals.&rsquo; Palliative Medicine 
</p>
<p class="quote">
&lsquo;The information is up to date and prepared with extreme care. This handbook should be available to staff involved with inpatient consultation, inpatient units of care, and home visits.&rsquo; Journal of Pain and Symptom Management 
</p>
<p class="quote">
&lsquo;First class. Highly recommended.&rsquo; International Association for Hospice and Palliative Care 
</p>
<p class="helppull">
Help support palliativedrugs.com by telling your colleagues about PCF Canadian edition. We have created a pdf flyer suitable for e-mailing or printing to help you to do this.<br />
<a class="downloadhpcfca" href="downloadPCFca.php">Download PDF</a> 
</p>
</div>
<p>
&nbsp;
</p>
<h3><a id="SMAC" name="SMAC" title="SMAC"></a>Symptom Management in Advanced Cancer 4th edition</h3>
<div class="bookimg">
<a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=280"><img align="left" alt="Symptom Management in Advanced Cancer" border="0" id="smac" name="smac" src="SamplePDF/smac-cover.png" width="178" /></a> <span class="prelim"><a href="SamplePDF/SMAC4e/Prelims.pdf" onclick="javascript: pageTracker._trackPageview('smac/prelims.pdf');" target="_blank">View prelims</a></span> <span class="chaptersample thirteen"><a href="SamplePDF/SMAC4e/Chapter13.pdf" target="_blank">View chapter 1</a></span> <span class="buynow"><a href="http://www.palliativebooks.com/index.php?act=viewProd&amp;productId=280" target="_blank">Buy Now</a></span> 
</div>
<div class="pcfcontent">
<p>
Symptom Management in Advanced Cancer is an established text which provides a framework of knowledge to enable clinicians to develop a systematic and scientific approach to treatment. 
</p>
<p>
It offers practical information and advice on the wide range of symptoms encountered in patients with advanced cancer. 
</p>
<p>
This edition has been extensively revised and expanded, and has a new chapter on Last Days. 
</p>
<p>
Japanese translation rights licensed to IGAKU-SHOIN, previous editions translated in German, Japanese and Romanian. 
</p>
<p class="quote">
&lsquo;A masterly work: clear, concise and practical.&rsquo; <br />
<em>Age and Ageing</em> 
</p>
<p class="quote">
&lsquo;A masterpiece&hellip;an essential book.&rsquo;<br />
<em>Geriatric Medicine</em> 
</p>
<p class="quote">
&lsquo;Without doubt the best of the numerous offerings on this subject. It is up to date, the information flows logically and the reader acquires concrete information in a few minutes. I have no reservations about recommending it.&rsquo;<br />
<em>Oncology in Practice</em> 
</p>
<p class="quote">
&lsquo;Should be within arm&rsquo;s reach of all clinicians involved in the management of patients with advanced cancer.&rsquo;<br />
<em>Palliative Medicine</em> 
</p>
<p class="quote">
&lsquo;Every general practitioner, hospital doctor (medical and surgical) and especially oncologist and palliative care physicians should have this book on their shelf.&rsquo; <em><br />
British Journal of Hospital Medicine</em> 
</p>
</div>

			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
