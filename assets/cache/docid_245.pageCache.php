<?php die('Unauthorized access.'); ?>a:41:{s:2:"id";s:3:"245";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:10:"Contact us";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:10:"contact-us";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:1259:"<h1>CONTACT US</h1>
<p>
It is our aim to make this site as useful as possible for health professionals. Its ultimate success will depend in part on the feedback that you provide. Apart from general comments about the site structure and function, we would particularly like to be made aware of any new information relating to the drugs already featured on the site or suggestions for other drugs to be considered for inclusion. Donation of information relating to the compatibility of drug combinations used in syringe drivers (via the SDSD) and of guidelines, protocols, information leaflets or audit proformas for the document library (send a pdf or word doc) is particularly welcomed.<br />
<br />
<strong>Please contact: </strong><a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a><br />
<br />
For comments relating to clinical matters, we encourage you to make use 
of the bulletin board.<br />
<br />
<strong>Palliativedrugs.com Limited </strong><br />
The Study Centre, <br />
Hayward House, <br />
Nottingham University Hospitals NHS Trust, <br />
City Campus,
Hucknall Road, <br />
Nottingham, NG5 1PB. <br />
<br />
Tel: +44 115 962 7778
</p>
<p>
Registered Number 03290371<br />
<br />
VAT No. 763-5239-18
</p>";s:8:"richtext";s:1:"0";s:8:"template";s:2:"18";s:9:"menuindex";s:2:"10";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1209752950";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1217164471";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1209752950";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:7:"wrapper";a:5:{i:0;s:7:"wrapper";i:1;s:0:"";i:2;s:8:"viewport";i:3;s:69:"&vpid=wrapper&width=100%&borsize=0&height=950px&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:13:"wrapper_param";a:5:{i:0;s:13:"wrapper_param";i:1;s:72:"@EVAL return "/legacy/newsletter/newsletter.php?&nlid=" . $_GET['nlid'];";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:18:"wrapper_forum_read";a:5:{i:0;s:18:"wrapper_forum_read";i:1;s:63:"http://testing.palldev.co.uk/forum/read.php?f=1&i=13823&t=13823";i:2;s:8:"viewport";i:3;s:56:"&width=100%&height=950px&borsize=0&sbar=Auto&aheight=Yes";i:4;s:4:"text";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/large.css" />
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<style>
.treeview ul{ /*CSS for Simple Tree Menu*/
    margin: 0;
    padding: 0;
    list-style-type: none;
    list-style-image: none;
}

.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
    background: url(/mk/list2.gif) no-repeat left center;
    list-style-type: none;
    list-style-image: none;
    padding-left: 22px;
    margin-bottom: 3px;
}

.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
    background: url(/mk/closed.gif) no-repeat left 1px;
    cursor: hand !important;
    cursor: pointer !important;
}

.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
    display: none; /*Hide them by default. Don't delete. */
}

.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
    cursor: default;
}
</style>
<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<script language="javascript" type="text/javascript" src="js/prototype.lite.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.js"></script>
<script language="javascript" type="text/javascript" src="js/moo.fx.pack.js"></script>
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript">
	function init(){
		var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
		var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
		//accordion effect
		var myAccordion = new fx.Accordion(
			toggles, stretchers, {opacity: true, duration: 400}
		);
		//hash function
		function checkHash(){
			var found = false;
			toggles.each(function(h2, i){
				if (window.location.href.indexOf(h2.title)>0){
					myAccordion.showThisHideOpen(stretchers[i]);
					found = true;
				}
			});
			return found;
		}
		if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
	}	
</script>
</head>
<body id="article">
<div id="container">
  [!header!]
  	[!nav!]
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <div class="col01_1">
				<!--  [!breadcrumbs!]<br /> -->
				<h1>CONTACT US</h1>
<p>
It is our aim to make this site as useful as possible for health professionals. Its ultimate success will depend in part on the feedback that you provide. Apart from general comments about the site structure and function, we would particularly like to be made aware of any new information relating to the drugs already featured on the site or suggestions for other drugs to be considered for inclusion. Donation of information relating to the compatibility of drug combinations used in syringe drivers (via the SDSD) and of guidelines, protocols, information leaflets or audit proformas for the document library (send a pdf or word doc) is particularly welcomed.<br />
<br />
<strong>Please contact: </strong><a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a><br />
<br />
For comments relating to clinical matters, we encourage you to make use 
of the bulletin board.<br />
<br />
<strong>Palliativedrugs.com Limited </strong><br />
The Study Centre, <br />
Hayward House, <br />
Nottingham University Hospitals NHS Trust, <br />
City Campus,
Hucknall Road, <br />
Nottingham, NG5 1PB. <br />
<br />
Tel: +44 115 962 7778
</p>
<p>
Registered Number 03290371<br />
<br />
VAT No. 763-5239-18
</p>
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer">
    <p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

  </div>
</div>
<script type="text/javascript">
		Element.cleanWhitespace('container');
		init();		
	</script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-8576276-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
