<?php die('Unauthorized access.'); ?>a:38:{s:2:"id";s:3:"218";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:44:"Using licensed drugs for unlicensed purposes";s:9:"longtitle";s:0:"";s:11:"description";s:0:"";s:5:"alias";s:44:"using-licensed-drugs-for-unlicensed-purposes";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"213";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:16283:"<table border="0" cellpadding="0" cellspacing="0" width="98%">
	<tbody>
		<tr>
			<td class="ttl" align="left">USING LICENSED DRUGS FOR UNLICENSED PURPOSES </td>			
		</tr>
	</tbody>
</table>
<hr width="100%">
<!--
<p class="ttl">
USING LICENSED DRUGS FOR UNLICENSED PURPOSES 
</p>
<p>
&nbsp;
</p>
<hr width="100%" />
-->
<p>
<img height="27" src="c:/pcf-usa/symbols/space.gif" width="1" /> 
</p>
<p class="first">
In palliative care, up to a quarter of all prescriptions written are for licensed drugs given for unlicensed indications, and/or via an unlicensed route,<sup>1,2</sup> and this is reflected in the recommendations contained in <em>PCF3</em>. The symbol &dagger; is used to draw attention to such use. However, it is impossible to highlight every example of unlicensed use. Often it is simply a matter of the route or dose being different from those in the manufacturer's SPC. Thus, it is important to recognize that the licensing process for drugs regulates the marketing activities of pharmaceutical companies and not a doctor's prescribing practice. Unlicensed use of drugs by prescribers is often appropriate and may represent standard practice, and a doctor's clinical freedom to prescribe in this way is specifically safeguarded in the Medicines Act 1968. Further, drugs prescribed outside the licence can be dispensed by pharmacists<sup>3</sup> and administered by nurses or midwives.<sup>4</sup> 
</p>
<h1>The licensing process</h1>
<p class="first">
A marketing licence is necessary in the UK for a product for which therapeutic claims are made. New medicines for use in Europe are first evaluated by the Committee for Medicinal Products for Human Use (CHMP) of the European Medicines Agency (EMEA). After receiving satisfactory evidence of quality, safety and efficacy, the CHMP issues a positive opinion recommending marketing. Subject to further scrutiny to satisfy its own criteria, the Licensing Authority of the Medicines and Healthcare products Regulatory Agency (MHRA) then grants a licence called the Marketing Authorization. This allows a pharmaceutical company to market and supply a product in the UK for the specific indications listed in its SPC. Restrictions are imposed by the MHRA if evidence of safety and efficacy is unavailable in particular patient groups, e.g. children. Once a product is marketed, further clinical trials and experience may reveal other indications. For these to become licensed, additional evidence needs to be submitted. The considerable expense of this, perhaps coupled with a small market for the new indication, often means that a revised application is not made. 
</p>
<h1>Prescribing outside the licence</h1>
<p class="first">
In the UK, a doctor may legally: 
</p>
<ul>
	<li>prescribe unlicensed medicines</li>
	<li>use unlicensed products specially prepared, imported or supplied for a named patient</li>
	<li>use or advise using a licensed medicine for indications or in doses or by routes of administration outside the licensed recommendations</li>
	<li>supply another doctor with an unlicensed medicine</li>
	<li>override the warnings and precautions given in the licence</li>
	<li>use unlicensed drugs in clinical trials.</li>
</ul>
<p class="first">
Further, nurses, pharmacists, chiropodists/podiatrists, physiotherapists and radiographers who are registered as supplementary prescribers can, in partnership with a doctor or dentist (the independent prescriber), prescribe: 
</p>
<ul>
	<li>licensed medicines outside their licensed indications</li>
	<li>unlicensed medicines</li>
</ul>
<p class="first">
provided this is done in the framework of an agreed clinical management plan for a specific patient.<sup>5</sup> 
</p>
<p class="indented">
Nurses or pharmacists who are registered as independent prescribers can prescribe licensed medicines outside their licence if this is accepted clinical practice, but cannot prescribe unlicensed medicines.<sup>6</sup> 
</p>
<p class="indented">
The responsibility for the consequences of these actions lies with the prescriber.<sup>5&ndash;7</sup> In addition to clinical trials such prescriptions may be justified: 
</p>
<ul>
	<li>when prescribing generic formulations for which indications are not described</li>
	<li>with established drugs for proven but unlicensed indications</li>
	<li>with drugs for conditions for which there are no other treatments (even in the absence of strong evidence)</li>
	<li>when using drugs in individuals not covered by the licence, e.g. children.</li>
</ul>
<p>
&nbsp;
</p>
<p class="first">
Prescription of a drug (whether licensed use/route or not) requires the prescriber, in the light of published evidence, to balance both the potential good and the potential harm which might ensue. Prescribers have a duty to act with reasonable care and skill in a manner consistent with the practice of professional colleagues of similar standing. Thus, when prescribing outside the terms of the licence, prescribers must be fully informed about the actions and uses of the drug, and be assured of the quality of the particular product. It is possible to draw a hierarchy of degrees of reasonableness relating to the use of unlicensed drugs (Figure 1).<sup>8</sup> The more dangerous the medicine and the more flimsy the evidence the more difficult it is to justify its prescription.<br />
<br />
<table align="center" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td><img border="0" src="assets/images/pall/prelimsgr1.jpg"/></td>
		</tr>
	</tbody>
</table>
<br />
<span class="articletype"><b>Figure 1</b></span>&nbsp;&nbsp;Factors influencing the reasonableness of prescribing decisions.<sup>8</sup> <br />
</p>
<p>&nbsp;</p>
<p  class="indented">
It has been recommended that when prescribing a drug outside its licence, a prescriber should:<sup>4,6&ndash;9</sup> 
</p>
<ul>
	<li>document in the patient's records the reasons for the decision to prescribe outside the licensed indications</li>
	<li>where possible, explain the position to the patient (and family as appropriate) in sufficient detail to allow them to give informed consent; the Patient Information Leaflet obviously does not contain information about unlicensed indications </li>
	<li>inform other professionals, e.g. pharmacist, nurses, general practitioner, involved in the care of the patient to avoid misunderstandings.</li>
</ul>
<p class="first">
However, in palliative care, the use of drugs for unlicensed uses or by unlicensed routes is so widespread that such an approach is impractical. Indeed, in the UK, a survey showed that few (&lt;5%) palliative medicine consultants <em>always</em> obtain verbal or written consent, document in the notes or inform other professionals when using licensed drugs for unlicensed purposes/routes.<sup>10</sup> Concern was expressed that not only would it be impractical to do so, but it would be burdensome for the patient, increase anxiety and might result in refusal of beneficial treatment. Some half to two-thirds indicated that they would <em>sometimes</em> obtain verbal consent (53%), document in the notes (41%) and inform other professionals (68%) when using treatments which are not widely used within the specialty, e.g. ketamine, octreotide, ketorolac. 
</p>
<p class="indented">
This is a grey area and each clinician must decide how explicit to be. Some NHS Trusts and other institutions have policies in place and have produced information cards or leaflets for patients and caregivers (Box B). A position statement has also been produced by the Association for Palliative Medicine and the Pain Society (Box C).<br />
</p>
<ul class="code" id="tbx">
	<p class="boxlbl">
	<strong>Box B</strong>&nbsp;&nbsp;Example of a patient information leaflet about the use of medicines outside their licence 
	</p>
	<div style="margin: 0px 8px 8px">
	<p align="center">
	<strong>Use of Medicines Outside their Licence</strong> 
	</p>
	<p>
	This leaflet contains important information about your medicines, so please read it carefully. <br />
	<br />
	Medicines prescribed by your doctor or bought over-the-counter from a pharmacist are licensed for use by the Medicines and Healthcare products Regulatory Agency (MHRA). <br />
	<br />
	The product licence (or &lsquo;marketing authorization&rsquo;) specifies the conditions for which the medicine should be used and how it should be given. <br />
	<br />
	Patient Information Leaflets (PILs) which accompany the medicines reflect the product licence. <br />
	<br />
	Medicines are often used for conditions or in ways that are not specified on the product licence. This is true for a lot of medicines used in palliative care. <br />
	<br />
	Your doctor will use medicines outside the product licence only when there is research and experience to back up such use. <br />
	<br />
	Medicines used very successfully outside the product licence include some antidepressants and anti-epileptics (anticonvulsants) which are used to relieve some types of pain. <br />
	<br />
	Also, instead of being injected into a vein or muscle, medicines are often injected subcutaneously (under the skin) because this is more comfortable and convenient for you. <br />
	<br />
	When a medicine is used outside the product licence, the information on the PIL may not be relevant to how you are taking the medicine. <br />
	<br />
	If you find this confusing, your doctor or pharmacist will be happy to help. <br />
	Alternatively, contact: <br />
	<br />
	Dr/Nurse ........................................................................................<br />
	Hosptial .........................................................................................<br />
	.......................................................................................................<br />
	.......................................................................................................<br />
	Tel ............................................
	</p>
	</div>
</ul>
<br />
<br />
<ul class="code" id="tbx">
	<p class="boxlbl">
	<strong>Box C</strong>&nbsp;&nbsp;The recommendations of the Association for Palliative Medicine and the Pain Society
	</p>
	<div style="margin-top: -8px; margin-bottom: 8px; margin-left: 4px;" id="numlist">
	<p class="first">
	<strong>The use of drugs beyond licence in palliative care and pain management</strong> 
	</p>
	<table border="0" cellpadding="4" cellspacing="0" width="95%">
		<tr>
			<td align="right" valign="top"><b>1</b></td>
			<td>This statement should be seen as reflecting the views of a responsible body of opinion within the clinical specialties of palliative medicine and pain management.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>2</b></td>
			<td>The use of drugs beyond licence should be seen as a legitimate aspect of clinical practice.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>3</b></td>
			<td>The use of drugs beyond licence in palliative care and pain management practice is currently both necessary and common.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>4</b></td>
			<td>Choice of treatment requires partnership between patients and health professionals, and informed consent should be obtained, whenever possible, before prescribing any drug. Patients should be informed of any identifiable risks and details of any information given should be recorded. It is often unnecessary to take additional steps when recommending drugs beyond licence.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>5</b></td>
			<td>Patients, carers and health professionals need accurate, clear and specific information that meets their needs. The Association for Palliative Medicine and the Pain Society should work in conjunction with pharmaceutical companies to design accurate information for patients and their carers about the use of drugs beyond licence.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>6</b></td>
			<td>Health professionals involved in prescribing, dispensing and administering drugs beyond licence should select those drugs that offer the best balance of benefit against harm for any given patient.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>7</b></td>
			<td>Health professionals should inform, change and monitor their practice with regard to drugs beyond licence in the light of evidence from audit and published research.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>8</b></td>
			<td>The Department of Health should work with health professionals and the pharmaceutical industry to enable and encourage the extension of product licences where there is evidence of benefit in circumstances of defined clinical need.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>9</b></td>
			<td>Organizations providing palliative care and pain management services should support therapeutic practices that are underpinned by evidence and advocated by a responsible body of professional opinion.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>10</b></td>
			<td>There is urgent need for the Department of Health to assist healthcare professionals to formulate national frameworks, guidelines and standards for the use of drugs beyond licence. The Pain Society and the Association for Palliative Medicine should work with the Department of Health, NHS Trusts, voluntary organizations and the pharmaceutical industry to design accurate information for staff, patients and their carers in clinical areas where drugs are used beyond their licence (off-label). Practical support is necessary to facilitate and expedite surveillance and audit which are essential to develop this initiative.</td>
		</tr>
	</table>
	</div>
</ul>
<div class="ref">
<hr width="100%" />
<ol start="1" type="1">
	<li>Atkinson C and Kirkham S (1999) Unlicensed uses for medication in a palliative care unit. <em>Palliative Medicine</em>. <strong>13</strong>: 145&ndash;152. </li>
	<li>Todd J and Davies A (1999) Use of unlicensed medication in palliative medicine. <em>Palliative Medicine</em>. <strong>13</strong>: 466. </li>
	<li>Royal Pharmaceutical Society of Great Britain (2004) <em>Fitness to practise and legal affairs directorate fact sheet: five. The use of unlicensed medicines in pharmacy</em>. Pharmaceutical Society of Great Britain. Available from: <a href="http://www.rpsgb.org/pdfs/factsheet5.pdf"target="_blank">www.rpsgb.org/pdfs/factsheet5.pdf</a></li>
	<li>DTB (1992) Prescribing unlicensed drugs or using drugs for unlicensed indications. <em>Drug and Therapeutics Bulletin</em>. <strong>30</strong>: 97&ndash;99. </li>
	<li>DoH (2005) <em>Supplementary prescribing by nurses, pharmacists, chiropodists/podiatrists, physiotherapists and radiographers within the NHS in England: a guide for implementation</em>. HMSO, London. Available from: <a href="http://www.dh.gov.uk/en/Publicationsandstatistics/Publications/PublicationsPolicyAndGuidance/DH_4110032"target="_blank">www.dh.gov.uk/en/Publicationsandstatistics/Publications/PublicationsPolicyAndGuidance/DH_4110032</a></li>
	<li>DoH (2006) <em>Improving patients' access to medicines: a guide to implementing nurse and pharmacist independent prescribing within the NHS in England</em>. HMSO, London. Available from: <a href="http://www.dh.gov.uk/assetRoot/04/13/37/47/04133747.pdf"target="_blank">www.dh.gov.uk/assetRoot/04/13/37/47/04133747.pdf</a></li>
	<li>Tomkins C (1988) Drugs without a product licence. <em>Journal of the Medical Defence Union</em>. <strong>Spring</strong>: 7. </li>
	<li>Ferner R (1996) Prescribing licensed medicines for unlicensed indications. <em>Prescribers' Journal</em>. <strong>36</strong>: 73&ndash;79. </li>
	<li>Cohen P (1997) Off-label use of prescription drugs: legal, clinical and policy considerations. <em>European Journal of Anaesthesiology</em>. <strong>14</strong>: 231&ndash;235. </li>
	<li>Pavis H and Wilcock A (2001) Prescribing of drugs for use outside their licence in palliative care: survey of specialists in the United Kingdom. <em>British Medical Journal</em>. <strong>323</strong>: 484&ndash;485. </li>
</ol>
</div>
";s:8:"richtext";s:1:"0";s:8:"template";s:2:"21";s:9:"menuindex";s:1:"4";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"3";s:9:"createdon";s:10:"1204046086";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1217947098";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1204046086";s:11:"publishedby";s:1:"3";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>&copy; palliativedrugs.com</title>
<link type="text/css" media="screen" rel="stylesheet" href="/css/template_styles.css" />

<script language="javascript" type="text/javascript" src="/js/SimpleTreeMenu.js "></script>
<script language="javascript" type="text/javascript" src="/js/FixedToolTip.js"></script>
<link type="text/css" media="screen" rel="stylesheet" href="/css/SimpleTreeView.css">
<link type="text/css" media="screen" rel="stylesheet" href="/mk/style.css" />
<!-- putting this here overwrites CSS default width for right column -->
<style>
	#contentcolumn {
		margin: 0 15 0 230px; /*Margins should be "0 0 0 LeftColumnWidth*/
	}

       #contentcolumn {
		line-height: 140%;
       }

/* TEMP - TO BE MOVED !! */
blockquote {
    margin-top: 18px;
}

/*sup {
    font-size:0.9em; 
    position:relative; 
    top:0.2em; 
    left:0;
}

sub {
    font-size:0.9em; 
    position:relative; 
    top: -0.1em; 
    left:0;
}*/

p {
    line-height: 1.6;
}

li.secord {
	list-style-image: url('/assets/templates/pall/triangleright.gif');
	margin-left:18px;
}
div.nicetitle p {color: #fff;}
</style>
<link type="text/css" media="screen" rel="stylesheet" href="/css/formulary.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="css/formulary-medium.css" />
<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="css/formulary-large.css" />
<!--This script should appear below your LINK stylesheet tags -->

<script src="/js/styleswitch.js" type="text/javascript"></script>
<script language="Javascript">
function HideLeft() {
	var l = document.getElementById('leftcolumn');
	var m = document.getElementById('contentcolumn');
	var g = document.getElementById('hideleft');
	if(l.style.display=='none') {
		//show...
		l.style.display='block';
		m.style.margin='20px 15px 10px 230px';
		g.value='hide';
	} else {
		//hide
		l.style.display='none';
		m.style.margin='20px 15px 10px 15px';
		g.value='show';
	}
}
</script>
<link href="css/tooltips.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript" src="js/tooltip.js"></script>
</head>
<body>
	<div id="container">
	 [!header!]
  	[!nav!]

	<div id="contentwrapper">
		<div class="tr"><div class="bl"><div class="br"><div id="contentcolumn">
			<table border="0" cellpadding="0" cellspacing="0" width="98%">
	<tbody>
		<tr>
			<td class="ttl" align="left">USING LICENSED DRUGS FOR UNLICENSED PURPOSES </td>			
		</tr>
	</tbody>
</table>
<hr width="100%">
<!--
<p class="ttl">
USING LICENSED DRUGS FOR UNLICENSED PURPOSES 
</p>
<p>
&nbsp;
</p>
<hr width="100%" />
-->
<p>
<img height="27" src="c:/pcf-usa/symbols/space.gif" width="1" /> 
</p>
<p class="first">
In palliative care, up to a quarter of all prescriptions written are for licensed drugs given for unlicensed indications, and/or via an unlicensed route,<sup>1,2</sup> and this is reflected in the recommendations contained in <em>PCF3</em>. The symbol &dagger; is used to draw attention to such use. However, it is impossible to highlight every example of unlicensed use. Often it is simply a matter of the route or dose being different from those in the manufacturer's SPC. Thus, it is important to recognize that the licensing process for drugs regulates the marketing activities of pharmaceutical companies and not a doctor's prescribing practice. Unlicensed use of drugs by prescribers is often appropriate and may represent standard practice, and a doctor's clinical freedom to prescribe in this way is specifically safeguarded in the Medicines Act 1968. Further, drugs prescribed outside the licence can be dispensed by pharmacists<sup>3</sup> and administered by nurses or midwives.<sup>4</sup> 
</p>
<h1>The licensing process</h1>
<p class="first">
A marketing licence is necessary in the UK for a product for which therapeutic claims are made. New medicines for use in Europe are first evaluated by the Committee for Medicinal Products for Human Use (CHMP) of the European Medicines Agency (EMEA). After receiving satisfactory evidence of quality, safety and efficacy, the CHMP issues a positive opinion recommending marketing. Subject to further scrutiny to satisfy its own criteria, the Licensing Authority of the Medicines and Healthcare products Regulatory Agency (MHRA) then grants a licence called the Marketing Authorization. This allows a pharmaceutical company to market and supply a product in the UK for the specific indications listed in its SPC. Restrictions are imposed by the MHRA if evidence of safety and efficacy is unavailable in particular patient groups, e.g. children. Once a product is marketed, further clinical trials and experience may reveal other indications. For these to become licensed, additional evidence needs to be submitted. The considerable expense of this, perhaps coupled with a small market for the new indication, often means that a revised application is not made. 
</p>
<h1>Prescribing outside the licence</h1>
<p class="first">
In the UK, a doctor may legally: 
</p>
<ul>
	<li>prescribe unlicensed medicines</li>
	<li>use unlicensed products specially prepared, imported or supplied for a named patient</li>
	<li>use or advise using a licensed medicine for indications or in doses or by routes of administration outside the licensed recommendations</li>
	<li>supply another doctor with an unlicensed medicine</li>
	<li>override the warnings and precautions given in the licence</li>
	<li>use unlicensed drugs in clinical trials.</li>
</ul>
<p class="first">
Further, nurses, pharmacists, chiropodists/podiatrists, physiotherapists and radiographers who are registered as supplementary prescribers can, in partnership with a doctor or dentist (the independent prescriber), prescribe: 
</p>
<ul>
	<li>licensed medicines outside their licensed indications</li>
	<li>unlicensed medicines</li>
</ul>
<p class="first">
provided this is done in the framework of an agreed clinical management plan for a specific patient.<sup>5</sup> 
</p>
<p class="indented">
Nurses or pharmacists who are registered as independent prescribers can prescribe licensed medicines outside their licence if this is accepted clinical practice, but cannot prescribe unlicensed medicines.<sup>6</sup> 
</p>
<p class="indented">
The responsibility for the consequences of these actions lies with the prescriber.<sup>5&ndash;7</sup> In addition to clinical trials such prescriptions may be justified: 
</p>
<ul>
	<li>when prescribing generic formulations for which indications are not described</li>
	<li>with established drugs for proven but unlicensed indications</li>
	<li>with drugs for conditions for which there are no other treatments (even in the absence of strong evidence)</li>
	<li>when using drugs in individuals not covered by the licence, e.g. children.</li>
</ul>
<p>
&nbsp;
</p>
<p class="first">
Prescription of a drug (whether licensed use/route or not) requires the prescriber, in the light of published evidence, to balance both the potential good and the potential harm which might ensue. Prescribers have a duty to act with reasonable care and skill in a manner consistent with the practice of professional colleagues of similar standing. Thus, when prescribing outside the terms of the licence, prescribers must be fully informed about the actions and uses of the drug, and be assured of the quality of the particular product. It is possible to draw a hierarchy of degrees of reasonableness relating to the use of unlicensed drugs (Figure 1).<sup>8</sup> The more dangerous the medicine and the more flimsy the evidence the more difficult it is to justify its prescription.<br />
<br />
<table align="center" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td><img border="0" src="assets/images/pall/prelimsgr1.jpg"/></td>
		</tr>
	</tbody>
</table>
<br />
<span class="articletype"><b>Figure 1</b></span>&nbsp;&nbsp;Factors influencing the reasonableness of prescribing decisions.<sup>8</sup> <br />
</p>
<p>&nbsp;</p>
<p  class="indented">
It has been recommended that when prescribing a drug outside its licence, a prescriber should:<sup>4,6&ndash;9</sup> 
</p>
<ul>
	<li>document in the patient's records the reasons for the decision to prescribe outside the licensed indications</li>
	<li>where possible, explain the position to the patient (and family as appropriate) in sufficient detail to allow them to give informed consent; the Patient Information Leaflet obviously does not contain information about unlicensed indications </li>
	<li>inform other professionals, e.g. pharmacist, nurses, general practitioner, involved in the care of the patient to avoid misunderstandings.</li>
</ul>
<p class="first">
However, in palliative care, the use of drugs for unlicensed uses or by unlicensed routes is so widespread that such an approach is impractical. Indeed, in the UK, a survey showed that few (&lt;5%) palliative medicine consultants <em>always</em> obtain verbal or written consent, document in the notes or inform other professionals when using licensed drugs for unlicensed purposes/routes.<sup>10</sup> Concern was expressed that not only would it be impractical to do so, but it would be burdensome for the patient, increase anxiety and might result in refusal of beneficial treatment. Some half to two-thirds indicated that they would <em>sometimes</em> obtain verbal consent (53%), document in the notes (41%) and inform other professionals (68%) when using treatments which are not widely used within the specialty, e.g. ketamine, octreotide, ketorolac. 
</p>
<p class="indented">
This is a grey area and each clinician must decide how explicit to be. Some NHS Trusts and other institutions have policies in place and have produced information cards or leaflets for patients and caregivers (Box B). A position statement has also been produced by the Association for Palliative Medicine and the Pain Society (Box C).<br />
</p>
<ul class="code" id="tbx">
	<p class="boxlbl">
	<strong>Box B</strong>&nbsp;&nbsp;Example of a patient information leaflet about the use of medicines outside their licence 
	</p>
	<div style="margin: 0px 8px 8px">
	<p align="center">
	<strong>Use of Medicines Outside their Licence</strong> 
	</p>
	<p>
	This leaflet contains important information about your medicines, so please read it carefully. <br />
	<br />
	Medicines prescribed by your doctor or bought over-the-counter from a pharmacist are licensed for use by the Medicines and Healthcare products Regulatory Agency (MHRA). <br />
	<br />
	The product licence (or &lsquo;marketing authorization&rsquo;) specifies the conditions for which the medicine should be used and how it should be given. <br />
	<br />
	Patient Information Leaflets (PILs) which accompany the medicines reflect the product licence. <br />
	<br />
	Medicines are often used for conditions or in ways that are not specified on the product licence. This is true for a lot of medicines used in palliative care. <br />
	<br />
	Your doctor will use medicines outside the product licence only when there is research and experience to back up such use. <br />
	<br />
	Medicines used very successfully outside the product licence include some antidepressants and anti-epileptics (anticonvulsants) which are used to relieve some types of pain. <br />
	<br />
	Also, instead of being injected into a vein or muscle, medicines are often injected subcutaneously (under the skin) because this is more comfortable and convenient for you. <br />
	<br />
	When a medicine is used outside the product licence, the information on the PIL may not be relevant to how you are taking the medicine. <br />
	<br />
	If you find this confusing, your doctor or pharmacist will be happy to help. <br />
	Alternatively, contact: <br />
	<br />
	Dr/Nurse ........................................................................................<br />
	Hosptial .........................................................................................<br />
	.......................................................................................................<br />
	.......................................................................................................<br />
	Tel ............................................
	</p>
	</div>
</ul>
<br />
<br />
<ul class="code" id="tbx">
	<p class="boxlbl">
	<strong>Box C</strong>&nbsp;&nbsp;The recommendations of the Association for Palliative Medicine and the Pain Society
	</p>
	<div style="margin-top: -8px; margin-bottom: 8px; margin-left: 4px;" id="numlist">
	<p class="first">
	<strong>The use of drugs beyond licence in palliative care and pain management</strong> 
	</p>
	<table border="0" cellpadding="4" cellspacing="0" width="95%">
		<tr>
			<td align="right" valign="top"><b>1</b></td>
			<td>This statement should be seen as reflecting the views of a responsible body of opinion within the clinical specialties of palliative medicine and pain management.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>2</b></td>
			<td>The use of drugs beyond licence should be seen as a legitimate aspect of clinical practice.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>3</b></td>
			<td>The use of drugs beyond licence in palliative care and pain management practice is currently both necessary and common.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>4</b></td>
			<td>Choice of treatment requires partnership between patients and health professionals, and informed consent should be obtained, whenever possible, before prescribing any drug. Patients should be informed of any identifiable risks and details of any information given should be recorded. It is often unnecessary to take additional steps when recommending drugs beyond licence.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>5</b></td>
			<td>Patients, carers and health professionals need accurate, clear and specific information that meets their needs. The Association for Palliative Medicine and the Pain Society should work in conjunction with pharmaceutical companies to design accurate information for patients and their carers about the use of drugs beyond licence.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>6</b></td>
			<td>Health professionals involved in prescribing, dispensing and administering drugs beyond licence should select those drugs that offer the best balance of benefit against harm for any given patient.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>7</b></td>
			<td>Health professionals should inform, change and monitor their practice with regard to drugs beyond licence in the light of evidence from audit and published research.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>8</b></td>
			<td>The Department of Health should work with health professionals and the pharmaceutical industry to enable and encourage the extension of product licences where there is evidence of benefit in circumstances of defined clinical need.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>9</b></td>
			<td>Organizations providing palliative care and pain management services should support therapeutic practices that are underpinned by evidence and advocated by a responsible body of professional opinion.</td>
		</tr>
		<tr>
			<td align="right" valign="top"><b>10</b></td>
			<td>There is urgent need for the Department of Health to assist healthcare professionals to formulate national frameworks, guidelines and standards for the use of drugs beyond licence. The Pain Society and the Association for Palliative Medicine should work with the Department of Health, NHS Trusts, voluntary organizations and the pharmaceutical industry to design accurate information for staff, patients and their carers in clinical areas where drugs are used beyond their licence (off-label). Practical support is necessary to facilitate and expedite surveillance and audit which are essential to develop this initiative.</td>
		</tr>
	</table>
	</div>
</ul>
<div class="ref">
<hr width="100%" />
<ol start="1" type="1">
	<li>Atkinson C and Kirkham S (1999) Unlicensed uses for medication in a palliative care unit. <em>Palliative Medicine</em>. <strong>13</strong>: 145&ndash;152. </li>
	<li>Todd J and Davies A (1999) Use of unlicensed medication in palliative medicine. <em>Palliative Medicine</em>. <strong>13</strong>: 466. </li>
	<li>Royal Pharmaceutical Society of Great Britain (2004) <em>Fitness to practise and legal affairs directorate fact sheet: five. The use of unlicensed medicines in pharmacy</em>. Pharmaceutical Society of Great Britain. Available from: <a href="http://www.rpsgb.org/pdfs/factsheet5.pdf"target="_blank">www.rpsgb.org/pdfs/factsheet5.pdf</a></li>
	<li>DTB (1992) Prescribing unlicensed drugs or using drugs for unlicensed indications. <em>Drug and Therapeutics Bulletin</em>. <strong>30</strong>: 97&ndash;99. </li>
	<li>DoH (2005) <em>Supplementary prescribing by nurses, pharmacists, chiropodists/podiatrists, physiotherapists and radiographers within the NHS in England: a guide for implementation</em>. HMSO, London. Available from: <a href="http://www.dh.gov.uk/en/Publicationsandstatistics/Publications/PublicationsPolicyAndGuidance/DH_4110032"target="_blank">www.dh.gov.uk/en/Publicationsandstatistics/Publications/PublicationsPolicyAndGuidance/DH_4110032</a></li>
	<li>DoH (2006) <em>Improving patients' access to medicines: a guide to implementing nurse and pharmacist independent prescribing within the NHS in England</em>. HMSO, London. Available from: <a href="http://www.dh.gov.uk/assetRoot/04/13/37/47/04133747.pdf"target="_blank">www.dh.gov.uk/assetRoot/04/13/37/47/04133747.pdf</a></li>
	<li>Tomkins C (1988) Drugs without a product licence. <em>Journal of the Medical Defence Union</em>. <strong>Spring</strong>: 7. </li>
	<li>Ferner R (1996) Prescribing licensed medicines for unlicensed indications. <em>Prescribers' Journal</em>. <strong>36</strong>: 73&ndash;79. </li>
	<li>Cohen P (1997) Off-label use of prescription drugs: legal, clinical and policy considerations. <em>European Journal of Anaesthesiology</em>. <strong>14</strong>: 231&ndash;235. </li>
	<li>Pavis H and Wilcock A (2001) Prescribing of drugs for use outside their licence in palliative care: survey of specialists in the United Kingdom. <em>British Medical Journal</em>. <strong>323</strong>: 484&ndash;485. </li>
</ol>
</div>

		</div></div></div></div>
	</div>

	<div id="footer">
		<p style="width:800px;">&copy;&nbsp;2000&ndash;2011&nbsp;palliativedrugs.com&nbsp;Ltd.</p>
<h4><a href="/terms-and-conditions.html">Terms&nbsp;and&nbsp;conditions</a>&nbsp;|&nbsp;<a href="/privacy.html">Privacy&nbsp;policy</a></h4>

	</div>
	</div>
</body>
</html>