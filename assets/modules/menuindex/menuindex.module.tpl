// +----------------------------------------------------------------------+
// | Ajax Menu Index Editor                                               |
// +----------------------------------------------------------------------+
// | Created January 2006                                                 |
// +----------------------------------------------------------------------+
// | Author: Mark Kaplan                                                 |
// +----------------------------------------------------------------------+
//

$id= isset($_POST['new_parent'])? $_POST['new_parent']: 0;
$actionkey = isset($_POST['actionkey'])? $_POST['actionkey']: 0;
if(isset($_POST['sortableListsSubmitted'])) {$actionkey =1;}

$header = '
<html>
<head>
	<title>MODx Menu Index Editor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" type="text/css" href="media/style/MODx/style.css?" />
	
	<link rel="stylesheet" type="text/css" href="media/style/MODx/coolButtons2.css?" />
<style type="text/css">

input.button {
display: none;

}

ul.sortableList {
	padding-left: 20px;
	margin: 0px;
	width: 300px;
	font-family: Arial, sans-serif;
}
ul.sortableList li {
	font-weight: bold;
	cursor: move;
	color: white;
	padding: 2px 2px;
	margin: 2px 0px;
	border: 1px solid #000000;
	background-image: url(media/style/MODx/images/grid_hdr.gif);
	background-repeat: repeat-x;
}
#bttn .bttnheight {
	height: 25px !important;
	padding: 0px; 
	padding-top: 6px;
	float: left;
	vertical-align:		middle !important;

}
#bttn a{
	cursor: 			default !important;
	font: 				icon !important;
	color:				black !important;
	border:				0px !important;
	padding:			5px 5px 7px 5px!important;
	white-space:		nowrap !important;
	vertical-align:		middle !important;
	background:	transparent !important;
}

#bttn a:hover {
	border:		1px solid darkgreen !important;
	padding:			4px 4px 6px 4px !important;		
	background-image:	url("media/style/MODx/images/button_dn.gif") !important;
	text-decoration: none;
}
#bttn a img {
	vertical-align: middle !important;
}
</style>

	<script type="text/javascript" language="JavaScript" src="media/script/modx.js"></script>
	<script type="text/javascript" language="JavaScript" src="media/script/cb2.js"></script>

	<script type="text/javascript" language="JavaScript">
	function reset()
	
	{
	document.resetform.submit();
	}
	
	';
if ($actionkey == 1){
$header .= '

	function save()
	{
	populateHiddenVars();
	if (document.getElementById("updated")) {new Effect.Fade(\'updated\',{duration:0});}
	new Effect.Appear(\'updating\',{duration:0.5});
	setTimeout("document.sortableListForm.submit()",1000);
	}
	';
}else {
$header .= '

	function save()
	{
	document.newdocumentparent.submit();
	}
	

parent.menu.ca = "move";

function setMoveValue(pId, pName) {
	if (pId==0 || checkParentChildRelation(pId, pName)) {
		document.newdocumentparent.new_parent.value=pId;
		document.getElementById(\'parentName\').innerHTML = "Parent: <b>" + pId + "</b> (" + pName + ")";
	}
}

// check if the selected parent is a child of this document
function checkParentChildRelation(pId, pName) {
	var sp;
	var id = document.newdocumentparent.id.value;
	var tdoc = parent.menu.document;
	var pn = (tdoc.getElementById) ? tdoc.getElementById("node"+pId) : tdoc.all["node"+pId];
	if (!pn) return;
		while (pn.p>0) {
			pn = (tdoc.getElementById) ? tdoc.getElementById("node"+pn.p) : tdoc.all["node"+pn.p];
			if (pn.id.substr(4)==id) {
				alert("Illegal Parent");
				return;
			}
		}
	
	return true;
}


	';
}
$header .= '	
</script>
</head>
<body>
<form action="" method="post" name="resetform" style="display: none;">

<input name="actionkey" type="hidden" value="0" />

</form>

<div class="subTitle" id="bttn">
	<span class="right"><img src="media/images/_tx_.gif" width="1" height="5"><br />Menu Index Editor</span>

';

if ($actionkey == 1){

$header .= '<div class="bttnheight"><a id="Button1" onclick="save();"><img src="media/images/icons/save.gif"> Save</a></div>
<div class="bttnheight"><a id="Button4" onclick="reset();"><img src="media/images/icons/sort.gif"> Sort Another</a></div>';

}else {

$header .= '<div class="bttnheight"><a id="Button1" onclick="save();"><img src="media/images/icons/save.gif"> Go</a></div>';

}
$header .= '
		
		
		<div class="bttnheight"><a id="Button5" onclick="document.location.href=\'index.php?a=106\';"><img src="media/images/icons/cancel.gif"> Cancel</a></div>

	<div class="stay">   
	</div>
</div>

<div class="sectionHeader"><img src="media/images/misc/dot.gif" alt="." />&nbsp;';
$middle = '</div><div class="sectionBody">';
$footer = ' 
	</div>
	</body>
	</html>
';
echo $header."Menu Index Editor".$middle;

switch ($actionkey) {
case 0:
   echo '
<form method="post" action="" name=\'newdocumentparent\'>
<span id="parentName" class="warning">Please click the site root or parent document that you\'d like to sort.</span><br>
<input name="actionkey" type="hidden" value="1" />
<input type="hidden" name="new_parent" value="" class="inputBox"> 
<br />
<input type=\'save\' value="save" style="display:none">
</form>
';
   echo $footer;
   break;
   
case 1:
	$basePath = $modx->config['base_path'];
	$siteURL = $modx->config['site_url'];
	$include = $basePath.'assets/modules/menuindex/SLLists.class.php';
	
require_once($include);
$sortableLists = new SLLists($siteURL.'manager/media/script/scriptaculous/');
$sortableLists->addList('categories','categoriesListOrder');
$sortableLists->debug = false;

echo "<span class=\"warning\" style=\"display:none;\" id=\"updating\">Updating...<br /><br /> </span>";
$sortableLists->printTopJS();
$tblContent= $modx->getFullTableName('site_content');
if(isset($_POST['sortableListsSubmitted'])) {
echo "<span class=\"warning\" id=\"updated\">Updated!<br /><br /> </span>";
	//$tbl = $modx->getFullTableName("site_content");
	$orderArray = $sortableLists->getOrderArray($_POST['categoriesListOrder'],'categories');
	foreach($orderArray as $item) {
		$sql = "UPDATE $tblContent set menuindex=".$item['order']." WHERE id=".$item['element'];
		$modx->db->query($sql);
	}

}

			$query= "SELECT id , pagetitle , parent , menuindex FROM $tblContent WHERE parent = $id ORDER BY menuindex ASC";
		
			if (!$rs= $modx->db->query($query)) {
				return '';
			}
			while ($row= $modx->db->getRow($rs)) {
				$resource[]= $row;
			}

echo '<ul id="categories" class="sortableList">';

$cnt = count($resource);
if ($cnt < 1) {echo "Parent does not have any children $footer"; die;} else {

foreach ($resource as $item) {
echo '<li id="item_'.$item['id'].'">'.$item['pagetitle'].'</li>';

}
}
echo '</ul>';



$sortableLists->printForm('', 'POST', 'Submit', 'button');

echo '<br>';

$sortableLists->printBottomJS();

echo $footer;
break;
}