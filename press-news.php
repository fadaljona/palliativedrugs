<?php $doc_root	=	$_SERVER['DOCUMENT_ROOT'];

include("$doc_root/siteconfig/configs/config_include.php");

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<?php include ("$doc_root/includes/inc-article-header.php")?>

</head>

<body id="news">

<div id="container">

  <?php include ("$doc_root/includes/inc-topnav.php")?>

  <?php include ("$doc_root/includes/inc-nav.php")?>

  <div id="content">

    <div id="col01">

     <div class="testbg">

      <div class="tr">

        <div class="bl">

          <div class="br">

           <div id="newscontent">

                <div class="col01_1">

					<?php include ("$doc_root/includes/inc-pressnewsfilters.php")?>

					<?php include ("$doc_root/includes/inc-pressnews-list.php")?>					

				  </div>

				 </div>

				 <div id="newslisting">

                                        <div id="lastnewsitems">

       <h1>Previous press releases</h1>

	<ul>

{{repeat}}

         <li><a href="/press-news/{{latpnews.nmonth}}/{{latpnews.alias_title}}.html">{{latpnews.title}}</a></li>

{{/repeat}}

          </ul>

</div>

					<?php include ("$doc_root/includes/inc-pressnewsarchives.php")?>

				</div>

			</div>

          </div>

        </div>

      </div>

    </div>

  </div>

  <div id="footer">

   <?php include ("$doc_root/includes/inc-footer.php")?>

  </div>

</div>

<?php include ("$doc_root/includes/inc-analytics.php")?>

</body>

</html>

