<?php $doc_root	=	$_SERVER['DOCUMENT_ROOT'];

include("$doc_root/siteconfig/configs/config_include.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<?php include ("$doc_root/includes/inc-article-header.php")?>

</head>

<body id="article">

<div id="container">

	<?php include ("$doc_root/includes/inc-topnav.php")?>

	<?php include ("$doc_root/includes/inc-nav.php")?>	

	<div id="content">

	    <div id="col01">

    		<div class="tr">	

    			<div class="bl">

    				<div class="br">

					<div class="col01_1">

                                           <h3>Preferences</h3>

                                            <p>After making any changes, click the 'update' button at the bottom of the page.</p>

                                           <br /><br />

                                           <form action="/user-preferences.html" method="POST" onsubmit="return checkOrgType();">

                                           <table>

				           <?php include ("$doc_root/includes/inc-user-preferences.php")?>

					</div>					

				</div>

	    		</div>

			</div>

	    </div>		

	</div>

	<?php include ("$doc_root/includes/inc-footer.php")?>

</div>

<?php include ("$doc_root/includes/inc-analytics.php")?>

</body>

</html>