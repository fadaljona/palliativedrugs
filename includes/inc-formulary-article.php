<div id="langcontent">
<?php
if(!empty($_POST['set_lang'])){
         $_SESSION['site_language']=$_POST['set_lang']; 
}else if(empty($_SESSION['site_language'])){
        $_SESSION['site_language']='en';
}
if($_SESSION['site_language']=='en' || $_SESSION['site_language']=='' ){
?>	  

<?php }else{?>
	<img src="/images/de-cancer-logo.gif">              
	<p>Das Buch �Arzneimitteltherapie in der Palliativmedizin� (Elsevier, M�nchen, ISBN 3-437-23670-9) erschien 2005 als erste deutsche Printausgabe des Palliative Care Formulary (PCF). Wir freuen uns nun dank der finanziellen Unterst�tzung durch die Deutschen Krebshilfe die zweite Ausgabe als Online Version unter www.palliativedrugs.com vorstellen zu k�nnen. Diese ist vollst�ndig �berarbeitet und aktualisiert und ber�cksichtigt u.a. die aktuellen Auflagen aus Gro�britannien, den USA sowie Kanada. Die deutsche Fassung ist zum jetzigen Zeitpunkt noch nicht vollst�ndig, wird jedoch �ber die kommenden Wochen und Monate st�ndig erg�nzt werden. Zum Einstieg empfehlen wir das Kapitel �<a href="http://www.palliativedrugs.com/formulary/de/arzneimittel-in-der-palliativmedizin-apm-optimal-nutzen.html">Arzneimittel in der Palliativmedizin (APM) optimal nutzen</a>� zu lesen. Wir m�chten uns herzlich f�r die finanzielle Unterst�tzung durch die Deutsche Krebshilfe bedanken, die dieses Projekt erm�glicht hat.</p>
	<p><br>
	The first edition of the German Palliative Care Formulary was published in 2005 as Arzneimitteltherapie in der Palliativmedizin (Elsevier, Munich, ISBN 3-437-23670-9). As a result of financial support received from the German Cancer Aid we are now delighted to present the second edition online at www.palliativedrugs.com. This has been fully updated, taking into account the changes resulting from the production of further UK, USA and Canadian editions. Sections will be added as they are completed over the coming weeks-months. We would like to acknowledge with gratitude the support received from the German Cancer Aid which has made this possible.<br>
	</p>
	<p style="text-align:right">Claudia Bausewein<br>
	Constanze Remi<br>
	Robert Twycross<br>
	Andrew Wilcock<br>
	March 2011</p>
<?php } ?>
</div>