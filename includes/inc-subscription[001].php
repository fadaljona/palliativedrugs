<div class="bookshop list detail">
  <?php include ("$doc_root/includes/inc-product-category-menu.php")?>
  <?php /*
if($_SESSION['visitor']->acl=='for-profit'){
	$selFeature	=	open_SQL("SELECT p.*,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='3' AND p.parent_id='0' AND p.product_shortname='Subscriptions For Profit' LIMIT 0,1 ");
	  if(mysql_num_rows($selFeature)>0){
		  while($brow		=	mysql_fetch_array($selFeature)){
			  $fprod_id		=	$brow['product_id'];
			  $product_image=	$brow['product_image'];
			  $product_name	=	stripslashes($brow['product_name']);			  
			  $product_price=	$brow['price'];
			  $product_url	=	'/shop/'.remSpecial($brow['product_name']).'.html';
			  $imgTag		=	'';
			  $selAsset		=	open_SQL("SELECT asset_category_id,asset_name,asset_title FROM 3bit_assets WHERE asset_id='".$product_image."'");
			  if(mysql_num_rows($selAsset)>0){
					$ass_cat	=	mysql_result($selAsset,0,0); 	
					$thumbName	=	mysql_result($selAsset,0,1); 	
					$ass_tit	=	mysql_result($selAsset,0,2);
					$imgPath	=	'/assets/images/'.$ass_cat.'/'.$thumbName;
					$imgTag		=	"<img src='".$imgPath."' alt='".$ass_tit."'>";
			  }
			  unset($selAsset);
?>
  <div class="feature">
    <div class="col01">
      <h1>Palliative Care Formulary (PCF5)<br>
      on-line subscription</h1>
      <p>The on-line Palliative Care Formulary provides essential independent information for health professionals about drugs used in palliative and hospice care. It includes unlicensed (unlabeled) indications and routes, and details about the administration of multiple drugs by continuous subcutaneous infusion. Please see our <a href="/testimonials.html#pcf4">example sections and testimonials</a>.</p>
      <p>&nbsp;</p>
    </div>
    <?php if(!empty($imgTag)){ ?>
    <div class="col02">
      <?=$imgTag?>
    </div>
    <?php } ?>
    <form action="#" class="subscriptions" name="prolist" id="prolist">
      <input type='hidden' name='sessid' id='sessid' value="<?=$_SESSION['csess_id']?>">
      <fieldset>
        <h1>Individual subscription packages</h1>
        <table border="0" cellspacing="0" cellpadding="0">
          <? if($_SESSION['visitor']->id){ ?>
          <tr>
            <th><strong>Individual</strong></th>
            <th class="center">Total price inc. VAT</th>
            <th>&nbsp;</th>
          </tr>
          <?php $selVarient = open_SQL("SELECT * FROM 3bit_products WHERE (product_id='".$fprod_id."' OR parent_id = '".$fprod_id."') AND status='1'"); 
					if(mysql_num_rows($selVarient)>0){
					  while($vrow		=	mysql_fetch_array($selVarient)){
				  ?>
          <tr>
            <td><input type="radio" name="product_id" value="<?=$vrow['product_id']?>" id="<?=$vrow['product_id']?>" />
              <label for="<?=$vrow['product_id']?>">
                <?=stripslashes($vrow['style']);?>
              </label></td>
            <td class="center"><strong>&pound;
              <?=$vrow['price']?>
              </strong></td>
            <td align="right"></td>
          </tr>
          <?php     }
					   } 
				?>
          <tr>
            <td>&nbsp;</td>
            <td align="right"><input type="button" value="Add to cart" class="addtocart" onclick="addprodtocart('prolist');"/></td>
          </tr>
          <?}else{?>
          <tr>
            <td colspan="2"><p>To subscribe to the on-line formulary:</p>
              <p>1. Please <a href="/register.html">register</a> with the website<br />
                2. Log on to the site using the username and password you have created<br />
                3. Return to the Store and select on-line subscription.</p></td>
          </tr>
          <?}?>
        </table>
        <? if($_SESSION['visitor']->id){ ?>
        <hr noshade="noshade" />
        <h1>Corporate subscription packages</h1>
        <table cellspacing="0" cellpadding="0" border="0" class="corporate" id="corporate">
          <tbody>
            <tr>
              <th class="center">No. of users</th>
              <th class="center">Total price</th>
              <th class="center">Discount</th>
              <th class="center">You save</th>
            </tr>
            <tr>
              <td class="center">10</td>
              <td class="center"><strong>&pound;900</strong></td>
              <td class="center">10%</td>
              <td class="center">&pound;100</td>
            </tr>
            <tr>
              <td class="center">20</td>
              <td class="center"><strong>&pound;1700</strong></td>
              <td class="center">15%</td>
              <td class="center">&pound;300</td>
            </tr>
            <tr>
              <td class="center">30</td>
              <td class="center"><strong>&pound;2400</strong></td>
              <td class="center">20%</td>
              <td class="center">&pound;600</td>
            </tr>
            <tr>
              <td class="center">40</td>
              <td class="center"><strong>&pound;3000</strong></td>
              <td class="center">25%</td>
              <td class="center">&pound;1000</td>
            </tr>
            <tr class="border-bottom">
              <td class="center">50</td>
              <td class="center"><strong>&pound;3500</strong></td>
              <td class="center">30%</td>
              <td class="center">&pound;1500</td>
            </tr>
          </tbody>
        </table>
        <p>To set up a corporate account, or for information on more than 50 users, e-mail <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a></p>
        <?php } ?>
      </fieldset>
    </form>
  </div>
  <div class="side">
    <h2>What People Say</h2>
    <div class="quote">
      <blockquote>PCF [is] a core text for the specialty and has no serious rivals.</blockquote>
      <p>Palliative Medicine</p>
    </div>
    <div class="quote">
      <blockquote>The information is up to date and prepared with extreme care. This handbook should be available to staff involved with inpatient consultation, inpatient units of care, and home visits.</blockquote>
      <p>Journal of Pain and Symptom Management</p>
    </div>
    <div class="quote">
      <blockquote>First class. Highly recommended.</blockquote>
      <p>International Association for Hospice and Palliative Care</p>
    </div>
  </div>
  <?php }
	  }
}else{ */
	$selFeature	=	open_SQL("SELECT p.*,pc.procat_title FROM 3bit_products p,3bit_product_category pc WHERE FIND_IN_SET(pc.procat_id,p.procat_id) AND pc.procat_id='3' AND p.parent_id='0' AND p.product_shortname='Subscriptions Non Profit' LIMIT 0,1 ");
	  if(mysql_num_rows($selFeature)>0){
		  while($brow		=	mysql_fetch_array($selFeature)){
			  $fprod_id		=	$brow['product_id'];
			  $product_image=	$brow['product_image'];
			  $product_name	=	stripslashes($brow['product_name']);			  
			  $product_price=	$brow['price'];
			  $product_url	=	'/shop/'.remSpecial($brow['product_name']).'.html';
			  $imgTag		=	'';
			  $selAsset		=	open_SQL("SELECT asset_category_id,asset_name,asset_title FROM 3bit_assets WHERE asset_id='".$product_image."'");
			  if(mysql_num_rows($selAsset)>0){
					$ass_cat	=	mysql_result($selAsset,0,0); 	
					$thumbName	=	mysql_result($selAsset,0,1); 	
					$ass_tit	=	mysql_result($selAsset,0,2);
					$imgPath	=	'/assets/images/'.$ass_cat.'/'.$thumbName;
					$imgTag		=	"<img src='".$imgPath."' alt='".$ass_tit."'>";
			  }
			  unset($selAsset);
?>
  <div class="feature">
    <div class="col01">
      <h1>Palliative Care Formulary (PCF6) <br>
      on-line subscription</h1>
      <p>The on-line Palliative Care Formulary provides essential independent information for health professionals about drugs used in palliative and hospice care. It includes unlicensed (unlabeled) indications and routes, and details about the administration of multiple drugs by continuous subcutaneous infusion. Please see our <a href="/testimonials.html#pcf4">example sections and testimonials</a>.</p>
      <p>&nbsp;</p>
    </div>
    <?php if(!empty($imgTag)){ ?>
    <div class="col02">
      <?=$imgTag?>
    </div>
    <?php } ?>
    <form action="#" class="subscriptions" name="prolist" id="prolist" method="post">
      <input type='hidden' name='sessid' id='sessid' value="<?=$_SESSION['csess_id']?>">
      <fieldset>
        <h1>Individual subscription packages</h1>
        <table border="0" cellspacing="0" cellpadding="0">
          <? 

			$selFeature		=	open_SQL("SELECT u.Country, c.hinari FROM new_users u, 3bit_country c WHERE u.id='".$_SESSION['visitor']->id."' AND u.active='1' AND u.Country =c.country ");

			if(mysql_num_rows($selFeature)>0){
				$brow		=	mysql_fetch_array($selFeature);
				$isHinari	= $brow['hinari'];
			}
	
			if($_SESSION['visitor']->id!='' && $isHinari != '0'){
			
				?>
          <tr>
            <td colspan="2"><br>
              As a member registered from a country on the <a href="http://www.who.int/hinari/en/" target="_blank">HINARI</a> A and B lists, you may be eligible for free on-line access. To request this, e-mail <a href="mailto:hq@palliativedrugs.com?subject=HINARI">hq@palliativedrugs.com</a> with details of the organisation you work for. </td>
          </tr>
          <?}else if($_SESSION['visitor']->id){ ?>
          <tr>
            <th><strong>Individual</strong></th>
            <th align="right" style="text-align:right">Total price inc. VAT</th>
          </tr>
          <?php $selVarient = open_SQL("SELECT * FROM 3bit_products WHERE (product_id='".$fprod_id."' OR parent_id = '".$fprod_id."') AND status='1'"); 
				$k=0;
				if(mysql_num_rows($selVarient)>0){
				  while($vrow		=	mysql_fetch_array($selVarient)){
					$selected='';
					if($k==0)
						$selected='checked';
	  		  ?>
          <tr>
            <td><input type="radio" name="product_id" value="<?=$vrow['product_id']?>" id="<?=$vrow['product_id']?>" <?=$selected?>/>
              <label for="<?=$vrow['product_id']?>">
                <?=stripslashes($vrow['style']);?>
              </label></td>
            <td align="right"><strong>&pound;
              <?=$vrow['price']?>
              </strong></td>
          </tr>
          <?php     $k++;  }
			   } 
		?>
          <tr>
            <td>&nbsp;</td>
            <td align="right"><input type="button" value="Add to cart" class="addtocart" onclick="addprodtocart('prolist');"/></td>
          </tr>
          <?}else{?>
          <tr>
            <td colspan="2"><p>To subscribe to the on-line formulary:</p>
              <p>1. Please <a href="/register.html">register</a> with the website<br />
                2. Log on to the site using the username and password you have created<br />
                3. Return to the Store and select on-line subscription.</p></td>
          </tr>
          <?}?>
        </table>
        <? if($_SESSION['visitor']->id && $isHinari == '0' ){ ?>
<!--
        <hr noshade="noshade" />
        <h1>Corporate subscription packages</h1>
        <table cellspacing="0" cellpadding="0" border="0" class="corporate" id="corporate">
          <tbody>
            <tr>
              <th class="center">No. of users</th>
              <th class="center">Total price</th>
              <th class="center">Discount</th>
              <th class="center">You save</th>
            </tr>
            <tr>
              <td class="center">10</td>
              <td class="center"><strong>&pound;450</strong></td>
              <td class="center">10%</td>
              <td class="center">&pound;50</td>
            </tr>
            <tr>
              <td class="center">20</td>
              <td class="center"><strong>&pound;850</strong></td>
              <td class="center">15%</td>
              <td class="center">&pound;150</td>
            </tr>
            <tr>
              <td class="center">30</td>
              <td class="center"><strong>&pound;1200</strong></td>
              <td class="center">20%</td>
              <td class="center">&pound;300</td>
            </tr>
            <tr>
              <td class="center">40</td>
              <td class="center"><strong>&pound;1500</strong></td>
              <td class="center">25%</td>
              <td class="center">&pound;500</td>
            </tr>
            <tr class="border-bottom">
              <td class="center">50</td>
              <td class="center"><strong>&pound;1750</strong></td>
              <td class="center">30%</td>
              <td class="center">&pound;750</td>
            </tr>
          </tbody>
        </table>
-->
<p style="margin: 10px -10px;padding: 10px;background-color: lightgoldenrodyellow;"><strong>Please Note</br>Only for renewals to 31 December 2018.  For renewals from 1 January 2019 please email <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a> who will introduce you to your account manager</strong></p>
        <p>For further information or online access for more than one user please email <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a> with your contact details and a member of our team will be in touch.&nbsp;</p>
<?php } ?>
      </fieldset>
    </form>
  </div>
  <div class="side">
    <h2>What People Say</h2>
    <div class="quote">
      <blockquote>PCF [is] a core text for the specialty and has no serious rivals.</blockquote>
      <p>Palliative Medicine</p>
    </div>
    <div class="quote">
      <blockquote>The information is up to date and prepared with extreme care. This handbook should be available to staff involved with inpatient consultation, inpatient units of care, and home visits.</blockquote>
      <p>Journal of Pain and Symptom Management</p>
    </div>
    <div class="quote">
      <blockquote>First class. Highly recommended.</blockquote>
      <p>International Association for Hospice and Palliative Care</p>
    </div>
  </div>
  <?php }
	  }
    ?>
  <?php //} ?>
</div>
