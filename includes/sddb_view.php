<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/configs/config_include.php'); 
require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/functions/form_func.inc.php');
session_name('palliativedrugs');
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/inc-secure.php');
?>
<div style="font-family:arial,helvetica,sans; font-size:13px;">
<link href="/css/palliative.css" rel="stylesheet" type="text/css">
<?php
	$sql		=	"SELECT * FROM sddb WHERE id=" . $_GET['id'];
	$rs			=	open_SQL($sql);
	$numdrugs	=	2;
	$row		=	mysql_fetch_object($rs);
	//need to know how many drugs in this combo for rowspanning
	if($row->drug6_name) {
		$numdrugs=6;
	} elseif($row->drug5_name) {
		$numdrugs=5;
	} elseif($row->drug4_name) {
		$numdrugs=4;
	} elseif($row->drug3_name) {
		$numdrugs=3;
	} else {
		$numdrugs=2;
	}		
?>
<table cellpadding="1" cellspacing="3" width="98%" align="center">
<tr><td>
		<table width="100%" bgcolor="#e0e0e0">
			<tr><td>
			<table width="100%">
			<tr>
				<th rowspan="2">Drug</th>
				<th rowspan="2">Dose in syringe (mg)</th>
				<th rowspan="2">Volume in syringe (ml)</th>
				<th rowspan="2">Concentration (mg/ml)</th>
				<th rowspan="2">Diluent</th>
				<th colspan="3">Compatibility information</th>
				<th rowspan="2">Site reaction reported</th>				
			</tr>
			<tr>
				<th>Outcome</th>
				<th>Duration</th>
				<th>Data type</th>				
			</tr>
			<tr>
				<td><?=$row->drug1_name?></td>
				<td align="center"><?=($row->drug1_dose) ? $row->drug1_dose : '';?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=($row->volume) ? $row->volume : '';?></td>
				<td align="center"><?=($row->drug1_dose) ? @number_format($row->drug1_dose / $row->volume,2) : '';?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->diluent?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->compatibility?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->duration?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->datatype?></td>
				<td align="center" rowspan="<?=$numdrugs?>"><?=$row->site_reaction?></td>				
			</tr>
			<tr>
				<td><?=$row->drug2_name?></td>
				<td align="center"><?=($row->drug2_dose) ? $row->drug2_dose : '';?></td>
				<td align="center"><?=($row->drug2_dose) ? @number_format($row->drug2_dose / $row->volume,2) : '';?></td>
			</tr>
			
			<? if($row->drug3_name) { ?>
			<tr>
				<td><?=$row->drug3_name?></td>
				<td align="center"><?=($row->drug3_dose) ? $row->drug3_dose : '';?></td>
				<td align="center"><?=($row->drug3_dose) ? @number_format($row->drug3_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug4_name) { ?>
			<tr>
				<td><?=$row->drug4_name?></td>
				<td align="center"><?=($row->drug4_dose) ? $row->drug4_dose : '';?></td>
				<td align="center"><?=($row->drug4_dose) ? @number_format($row->drug4_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug5_name) { ?>
			<tr>
				<td><?=$row->drug5_name?></td>
				<td align="center"><?=($row->drug5_dose) ? $row->drug5_dose : '';?></td>
				<td align="center"><?=($row->drug5_dose) ? @number_format($row->drug5_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			<? if($row->drug6_name) { ?>
			<tr>
				<td><?=$row->drug6_name?></td>
				<td align="center"><?=($row->drug6_dose) ? $row->drug6_dose : '';?></td>
				<td align="center"><?=($row->drug6_dose) ? @number_format($row->drug6_dose / $row->volume,2) : '';?></td>
			</tr>
			<? } ?>			
			
			</table>
		
			</td></tr>
			<tr><td colspan="4">&nbsp;</td></tr>
	
		 </table>
<hr>
<strong>comments</strong><br>
<? echo $row->comments;?>

<? if($row->support_data) { ?>
	<hr>
	<strong>references</strong><br>
	<? echo $row->support_data;?>
<? } ?>
</div>