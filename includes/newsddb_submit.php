<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/configs/config_include.php'); 
require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/functions/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/functions/db_func.inc.php');
session_name('palliativedrugs');
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/inc-secure.php');

if($_SERVER['REQUEST_METHOD']	==	'POST') {
	$unit_code		=	$_POST['unit_code'];
	$unit_ref		=	$_POST['unit_ref'];
	$drug1_name		=	$_POST['drug1_name'];
	$drug1_dose		=	$_POST['drug1_dose'];
	$drug2_name		=	$_POST['drug2_name'];
	$drug2_dose		=	$_POST['drug2_dose'];
	$drug3_name		=	$_POST['drug3_name'];
	$drug3_dose		=	$_POST['drug3_dose'];
	$drug4_name		=	$_POST['drug4_name'];
	$drug4_dose		=	$_POST['drug4_dose'];
	$drug5_name		=	$_POST['drug5_name'];
	$drug5_dose		=	$_POST['drug5_dose'];
	$drug6_name		=	$_POST['drug6_name'];
	$drug6_dose		=	$_POST['drug6_dose'];
	$volume			=	$_POST['volume'];
	$diluent		=	$_POST['diluent'];
	$duration		=	$_POST['duration'];
	$duration_other =	$_POST['duration_other'];
	$compatibility	=	$_POST['compatibility'];
	$datatype		=	$_POST['datatype'];
	$site_reaction	=	$_POST['site_reaction'];
	$comments		=	$_POST['comments'];
	$support_data	=	$_POST['support_data'];
	$added_by		=	$_SESSION['visitor']->id;		//still stored here ???

	if(isset($_POST['reedit'])) {
		$page_title = "SDSD Survey09 - Edit Entry";
	} else {

	// error-checking...
	$errors = array();
	if($unit_code=='') {
		$errors['Unit Code'] = 'Please enter unit code.';
	}

	if($unit_ref=='') {
		$errors['Record No'] = 'Please enter record number.';
	}

	if($drug1_name){
		if(empty($drug1_dose)) {
			$errors["Drug 1 Dose"] = "Please enter the dose for drug 1.";
		} else
		if(!is_numeric($drug1_dose)) {
			$errors["Drug 1 Dose"] = "Drug 1 dose must be numbers only.";
		}
	} else {
		$errors["Drug 1 Name"] = "You must enter at least two drugs.";
	}

	if($drug2_name){
		if(empty($drug2_dose)) {
			$errors["Drug 2 Dose"] = "Please enter the dose for drug 2.";
		} else
		if(!is_numeric($drug2_dose)) {
			$errors["Drug 2 Dose"] = "Drug 2 dose must be numbers only.";
		}
	} else {
		$errors["Drug 2 Name"] = "You must enter at least two drugs.";
	}

	if($drug3_name){
		if(empty($drug3_dose)) {
			$errors["Drug 3 Dose"] = "Please enter the dose for drug 3.";
		} else
		if(!is_numeric($drug3_dose)) {
			$errors["Drug 3 Dose"] = "Drug 3 dose must be numbers only.";
		}
	} 

	if($drug4_name){
		if(empty($drug4_dose)) {
			$errors["Drug 4 Dose"] = "Please enter the dose for drug 4.";
		} else
		if(!is_numeric($drug4_dose)) {
			$errors["Drug 4 Dose"] = "Drug 4 dose must be numbers only.";
		}
	} 
	
	if($drug5_name){
		if(empty($drug5_dose)) {
			$errors["Drug 5 Dose"] = "Please enter the dose for drug 5.";
		} else
		if(!is_numeric($drug5_dose)) {
			$errors["Drug 5 Dose"] = "Drug 5 dose must be numbers only.";
		}
	} 

	if($drug6_name){
		if(empty($drug6_dose)) {
			$errors["Drug 6 Dose"] = "Please enter the dose for drug 6.";
		} else
		if(!is_numeric($drug6_dose)) {
			$errors["Drug 6 Dose"] = "Drug 6 dose must be numbers only.";
		}
	} 

	if($diluent=="Please select ...") {
			$errors["Diluent"] = "Please enter the diluent used.";
	}

	if(empty($volume)) {
		$errors["Volume"] = "Please enter the final volume.";
	} elseif (!is_numeric($volume)) {
		$errors["Volume"] = "Volume must be numbers only.";
	}

	if($duration=='Please select ...') {
		$errors["Duration"] = "Please enter the duration.";
	} else {
		if($duration=='other') {
			if(empty($duration_other)) {
				$errors['Duration'] = 'Please specify the duration in the \'other\' box.';
			} else
			if (!is_numeric($duration_other)) {
				$errors['Duration'] = 'Duration \'other\' must be numbers only.';
			}
		}
	}

	if($compatibility=='Please select ...') {
		$errors['Compatibility'] = 'Please indicate the compatibility.';
	} else {
		if($compatibility=='Incompatible') {
			if(empty($comments)) {
				$errors['Compatibility'] = 'You have specified \'incompatible\', please add your comments.';
			} 
		}
	} 

	if($datatype=='Please select ...') {
		$errors['Data'] = 'Please indicate the type of data.';
	}

	if($site_reaction=='Please select ...') {
		$errors['Site reaction'] = 'Please indicate whether there was an infusion site reaction.';
	} else {
		if($site_reaction=='Yes') {
			if(empty($comments)) {
				$errors['Site reaction'] = 'You have indicated a site reaction, please add your comments.';
			}
		}
	}



	// if passed basic check display to user for confirmation...
	if(sizeof($errors)==0) {
		$confirmed = $_POST['confirmed'];
		if($confirmed==-1) {
			// display with 'save' button
			$do_confirm = TRUE;
			$page_title = "SDSD Survey09 - Confirm Entry";
		} else {
			// save to database
			$sql		=	"INSERT INTO sddb_temp (drug1_name) VALUES ('')";
			open_SQL($sql);
			$id			=	mysql_insert_id();
			$added_date =	GetDbTimestamp();
			
			$sql = "UPDATE sddb_temp SET "
					. "unit_code	=	'".$unit_code."', "
					. "unit_ref		=	'".$unit_ref."', "
					. "drug1_name	=	'".$drug1_name."', "
					. "drug1_dose	=	'".$drug1_dose."', "
					. "drug2_name	=	'".$drug2_name."', "
					. "drug2_dose	=	'".$drug2_dose."', ";
			if(!empty($drug3_name)) {
				$sql .= "drug3_name	=	'".$drug3_name."', "
					. "drug3_dose	=	'".$drug3_dose."', ";
			}
			if(!empty($drug4_name)) {
				$sql .= "drug4_name	=	'".$drug4_name."', "
					. "drug4_dose	=	'".$drug4_dose."', ";
			}
			if(!empty($drug5_name)) {
				$sql .= "drug5_name	=	'".$drug5_name."', "
					. "drug5_dose	=	'".$drug5_dose."', ";
			}
			if(!empty($drug6_name)) {
				$sql .= "drug6_name	=	'".$drug6_name."', "
					. "drug6_dose	=	'".$drug6_dose."', ";
			}
			$sql .= "volume			=	'".$volume."', "
					. "diluent		=	'".$diluent."', "
					. "duration		=	'".$duration."', "
					. "duration_other=	'".$duration_other."', "
					. "compatibility=	'".$compatibility."', "
					. "datatype		=	'".$datatype."', "
					. "site_reaction=	'".$site_reaction."', "			
					. "comments		=	'".$comments."', "
					. "support_data	=	'".$support_data."', "
					. "added_by		=	'".$added_by."', ";
			if(IsSet($added_date)) {
				$sql .= "added_date	=	'".$added_date."', ";
			}
			$sql .= "approved	=	0 "	. "WHERE id='".$id."'";

			open_SQL($sql);
			$page_title		= "SDSD Survey09 - Entry Saved";
			$thankyou		= "Thank you for your submission.<br />Please be aware that new entries will not appear on the website until approved by a pharmacist.";
		}
	}
	} // if isset _post[reedit]
} else {
	$page_title =	"SDSD Survey09- new entry";
	$unit_code	=	"";
	$unit_ref	=	"";
	$drug1_name =	"";	$drug1_dose =  "";	
	$drug2_name =	"";	$drug2_dose =  "";	
	$drug3_name =	"";	$drug3_dose =  "";	
	$drug4_name =	"";	$drug4_dose =  "";	
	$drug5_name =	"";	$drug5_dose =  "";	
	$drug6_name =	"";	$drug6_dose =  "";	
	$volume		=	"";
	$diluent	=	"";
	$duration	=	"";
	$compatibility = "";
	$datatype	=	"";
	$site_reaction = "";
	$comments	=	"";
	$support_data = "";
	$added_by	=	"";
	$added_date =	"";
	$approved	=	FALSE;			
	$confirmed	=	-1; 
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>pall:sdsd:submit</title> 
	<link rel="STYLESHEET" type="text/css" href="/css/styles.css">
	<link rel="alternate stylesheet" type="text/css" media="screen" title="medium-theme" href="/css/medium.css" />
	<link rel="alternate stylesheet" type="text/css" media="screen" title="large-theme" href="/css/large.css" />
	<link href="/css/tooltips.css" type="text/css" rel="stylesheet" />

	<Script Language="JavaScript">
	function load(url) {
		var load = window.open(url,'','scrollbars=no,menubar=no,height=560,width=600,resizable=yes,toolbar=no,location=no,status=no');
	}
	</Script>
	<style>
		.confirmdata {
			font-weight: bold;
			color: #ff0000;
		}
		.treeview ul{ /*CSS for Simple Tree Menu*/
			margin: 0;
			padding: 0;
			list-style-type: none;
			list-style-image: none;
		}

		.treeview li{ /*Style for LI elements in general (excludes an LI that contains sub lists)*/
			 background: url(/mk/list2.gif) no-repeat left center;
			list-style-type: none;
			list-style-image: none;
			padding-left: 22px;
			margin-bottom: 3px;
		}

		.treeview li.submenu{ /* Style for LI that contains sub lists (other ULs). */
			background: url(/mk/closed.gif) no-repeat left 1px;
			cursor: hand !important;
			cursor: pointer !important;
		}

		.treeview li.submenu ul{ /*Style for ULs that are children of LIs (submenu) */
			display: none; /*Hide them by default. Don't delete. */
		}

		.treeview .submenu ul li{ /*Style for LIs of ULs that are children of LIs (submenu) */
			cursor: default;
		}
	</style>

</head>
<body id='survey2009' style="background-image:none;">

<style>
	.legacy {
		font-family:arial,helvetica,sans;
		font-size:13px;
	}
	
	.legacy th {
		background-color:#CDCECD;
	}
</style> 
 
<div class="legacy">
<p>
<h3><?=$page_title?></h3><br />
<?php
if($thankyou) {
	echo nl2br($thankyou);
	echo "<br><br><a target=\"_top\" href=\"/syringe-driver-database.html\">Back to SDSD search page</a> | <a href=\"/includes/newsddb_submit.php\">Submit a new survey entry</a>";
	echo '</p></body></	html>';
	exit();	
}
?>
<?
if(sizeof($errors)>0) {
	print "<b>Your entry is incomplete, please correct the following:-</b><br><br>";	
	foreach($errors as $key => $value)
		echo " - $key : $value<br>\n";
	echo "<br>";
}
?>
<form name="sddb_form" action="newsddb_submit.php" method="POST">
<?
if($do_confirm) {
?>
Please review your entry for accuracy.<br><br>
<input type="hidden" name="confirmed" value="1">
<table>
<tr>
	<td>Center / unit code</td>
	<td class="confirmdata"><?=$unit_code;?></td>
	<td><input name="unit_code" type="hidden" id="unit_code" size="20" value="<?=$unit_code;?>"></td>
</tr>
<tr>
	<td>Record number</td>
	<td class="confirmdata"><?=$unit_ref;?></td>
	<td><input name="unit_ref" type="hidden" id="unit_ref" size="10" value="<?=$unit_ref;?>"></td>
</tr>
<tr>
	<td>Drug 1 Name: </td>
	<td class="confirmdata"><?=$drug1_name?></td>
	<td><input type="hidden" name="drug1_name" value="<?=$drug1_name?>"></td>
</tr>
<tr>
	<td>Drug 1 Dose: </td>
	<td class="confirmdata"><?=$drug1_dose?>mg</td>
	<td><input type="hidden" name="drug1_dose" value="<?=$drug1_dose?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<tr>
	<td>Drug 2 Name: </td>
	<td class="confirmdata"><?=$drug2_name?></td>
	<td><input type="hidden" name="drug2_name" value="<?=$drug2_name?>"></td>
</tr>
<tr>
	<td>Drug 2 Dose: </td>
	<td class="confirmdata"><?=$drug2_dose?>mg</td>
	<td><input type="hidden" name="drug2_dose" value="<?=$drug2_dose?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>

<? if($drug3_name){ ?>
<tr>
	<td>Drug 3 Name: </td>
	<td class="confirmdata"><?=$drug3_name?></td>
	<td><input type="hidden" name="drug3_name" value="<?=$drug3_name?>"></td>
</tr>
<tr>
	<td>Drug 3 Dose: </td>
	<td class="confirmdata"><?=$drug3_dose?>mg</td>
	<td><input type="hidden" name="drug3_dose" value="<?=$drug3_dose?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<? } ?>
<? if($drug4_name){ ?>
<tr>
	<td>Drug 4 Name: </td>
	<td class="confirmdata"><?=$drug4_name?></td>
	<td><input type="hidden" name="drug4_name" value="<?=$drug4_name?>"></td>
</tr>
<tr>
	<td>Drug 4 Dose: </td>
	<td class="confirmdata"><?=$drug4_dose?>mg</td>
	<td><input type="hidden" name="drug4_dose" value="<?=$drug4_dose?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<? } ?>
<? if($drug5_name){ ?>
<tr>
	<td>Drug 5 Name: </td>
	<td class="confirmdata"><?=$drug5_name?></td>
	<td><input type="hidden" name="drug5_name" value="<?=$drug5_name?>"></td>
</tr>
<tr>
	<td>Drug 5 Dose: </td>
	<td class="confirmdata"><?=$drug5_dose?>mg</td>
	<td><input type="hidden" name="drug5_dose" value="<?=$drug5_dose?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<? } ?>
<? if($drug6_name){ ?>
<tr>
	<td>Drug 6 Name: </td>
	<td class="confirmdata"><?=$drug6_name?></td>
	<td><input type="hidden" name="drug6_name" value="<?=$drug6_name?>"></td>
</tr>
<tr>
	<td>Drug 6 Dose: </td>
	<td class="confirmdata"><?=$drug6_dose?>mg</td>
	<td><input type="hidden" name="drug6_dose" value="<?=$drug6_dose?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>
<? } ?>

<tr>
	<td>Diluent: </td>
	<td class="confirmdata"><?=$diluent?></td>
	<td><input type="hidden" name="diluent" value="<?=$diluent?>"></td>
</tr>
<tr>
	<td>Final Volume: </td>
	<td class="confirmdata"><?=$volume?>mL</td>
	<td><input type="hidden" name="volume" value="<?=$volume?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>

<tr>
	<td>Duration: </td>
	<td class="confirmdata">
		<?=$duration?>
		<?
		if($duration=='other') {
			echo " - " . $duration_other;			
		}
		?>
	</td>
	<td>
		<input type="hidden" name="duration" value="<?=$duration?>">
		<input type="hidden" name="duration_other" value="<?=$duration_other?>">
	</td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>

<tr>
	<td>Compatibility: </td>
	<td class="confirmdata"><?=$compatibility?></td>
	<td><input type="hidden" name="compatibility" value="<?=$compatibility?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>

<tr>
	<td>Data: </td>
	<td class="confirmdata"><?=$datatype?></td>
	<td><input type="hidden" name="datatype" value="<?=$datatype?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>

<tr>
	<td>Infusion site reaction: </td>
	<td class="confirmdata"><?=$site_reaction?></td>
	<td><input type="hidden" name="site_reaction" value="<?=$site_reaction?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>

<tr>
	<td>Comments: </td>
	<td class="confirmdata"><?=$comments?></td>
	<td><input type="hidden" name="comments" value="<?=$comments?>"></td>
</tr>
<tr><td colspan="3">&nbsp;</td></tr>

<?php /*?><tr>
	<td>Support Data: </td>
	<td class="confirmdata"><?=$support_data?></td>
	<td><input type="hidden" name="support_data" value="<?=$support_data?>"></td>
</tr><?php */?>
<tr><td colspan="3">&nbsp;</td></tr>

</table>

<br>
<input type="submit" name="reedit" value="re-edit">
<input type="submit" name="confirm" value="confirm & save">
<? } else { ?>
<input type="hidden" name="confirmed" value="-1">
<table width="90%" border="0" align="center">
  <tr>	
	<td>Center / unit code </td>	
	<td><input name="unit_code" type="text" id="unit_code" size="20" value="<?=$unit_code;?>"></td>
	<td>&nbsp;</td>
 </tr>
 <tr>
	<td>Record number</td>	
	<td><input name="unit_ref" type="text" id="unit_ref" size="10" value="<?=$unit_ref;?>"></td>
	<td>&nbsp;</td>
 </tr>
  <tr>
	<td>&nbsp;</td>
    <th>Drug name </th>
    <th>Dose (mg)</th>
   </tr>
     <tr>
        <th>Drug 1 </th>
        <td><?	
			$a[] = "";
			$drugs = array_merge($a,$config['drugs']);
			arrayToListBox("drug1_name",$drugs,$drug1_name, $extra="");
		?>	</td>
        <td><input name="drug1_dose" type="text" id="drug1_dose" size="10" value="<?=$drug1_dose;?>"> mg</td>
     </tr>
      <tr>
        <th>Drug 2 </th>
        <td><? arrayToListBox("drug2_name",$drugs,$drug2_name, $extra=""); ?></td>
        <td><input name="drug2_dose" type="text" id="drug2_dose" size="10" value="<?=$drug2_dose;?>"> mg</td>
       </tr>
      <tr>
        <th>Drug 3 </th>
        <td><? arrayToListBox("drug3_name",$drugs,$drug3_name, $extra=""); ?></td>
        <td><input name="drug3_dose" type="text" id="drug3_dose" size="10" value="<?=$drug3_dose;?>"> mg</td>
       </tr>
      <tr>
        <th>Drug 4 </th>
        <td><? arrayToListBox("drug4_name",$drugs,$drug4_name, $extra=""); ?></td>
        <td><input name="drug4_dose" type="text" id="drug4_dose" size="10" value="<?=$drug4_dose;?>"> mg</td>
       </tr>
      <tr>
        <th>Drug 5 </th>
        <td><? arrayToListBox("drug5_name",$drugs,$drug5_name, $extra=""); ?></td>
        <td><input name="drug5_dose" type="text" id="drug5_dose" size="10" value="<?=$drug5_dose;?>"> mg</td>
       </tr>
      <tr>
        <th>Drug 6 </th>
        <td><? arrayToListBox("drug6_name",$drugs,$drug6_name, $extra=""); ?></td>
        <td><input name="drug6_dose" type="text" id="drug6_dose" size="10" value="<?=$drug6_dose;?>"> mg</td>
       </tr>   
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      <!--    <td>&nbsp;</td> -->
        <td align="center">&nbsp;</td>
      </tr>
    <tr>
    	<td colspan="2">Diluent
		<?
		$options = $config['sddb_diluent'];
		array_unshift($options,"Please select ...");
		$default = (IsSet($diluent)) ? $diluent : "Please select ...";
		arrayToListBox("diluent",$options,$default);
		?>	
		&nbsp;&nbsp;&nbsp;made up to final volume of 
      <input name="volume" type="text" id="volume3" size="4" value="<?=$volume;?>">	ml  </td>
      <td colspan="1" align="right">&nbsp;</td>
      
    <tr><td>&nbsp;</td></tr>
    <tr>
  	<td colspan="2"><b>Actual</b> duration of infusion
			<?
		$options = $config['sddb_time'];
		array_unshift($options,"Please select ...");
		$default = (IsSet($duration)) ? $duration : "Please select ...";
		arrayToListBox("duration",$options,$default);
		?>
		&nbsp;&nbsp;(if 'other' please specify <input type="text" name="duration_other" size="6" value="<?=$duration_other;?>">hours)
	</td>
  </tr>
  
	<!--
    <tr>
    	<td colspan="5">number of recorded occurrences   <input name="occurrences" type="text" id="" size="4" value=""></td>
    </tr>
    -->
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    </tr>
  <tr>
    <th colspan="5">Outcomes</th>
  </tr>
  <tr>
  	<td>Compatibilty outcome</td>
    <td colspan="4">
		<?
		$options = $config['sddb_compatibility'];
		array_unshift($options,"Please select ...");
		$default = (IsSet($compatibility)) ? $compatibility : "Please select ...";
		arrayToListBox("compatibility",$options,$default);
		?>
		if <b>incompatible</b> please state details in Comments box below.		
	  </td>
  </tr>
    <tr>
  	<td>Data</td>
	<td>
		<?
		$options = $config['sddb_datatype'];
		array_unshift($options,"Please select ...");
		$default = (IsSet($datatype)) ? $datatype : "Please select ...";
		arrayToListBox("datatype",$options,$default);
		?>
		<a href="javascript:load('/includes/sddb_definitions.php?p')">definitions</a>	
	</td>
  </tr>
  <tr>
  	<td>Infusion site reaction</td>
    <td colspan="4"><?
		$options = $config['sddb_site'];
		array_unshift($options,"Please select ...");
		$default = (IsSet($site_reaction)) ? $site_reaction : "Please select ...";
		arrayToListBox("site_reaction",$options,$default);
		?>
		if <b>yes</b> please state details in Comments box below.
		</td>
  </tr>
  <tr>
    <td>Comments</td>
    <td colspan="4"><textarea name="comments" cols="60" rows="6"><? echo $comments; ?></textarea></td>
  </tr>
<?php /*?>  <tr>
    <td>References</td>
	<td colspan="4"><textarea name="support_data" cols="60" rows="6"><? echo $support_data; ?></textarea></td>
  </tr><?php */?>
  <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center">&nbsp;      </td>
    <td colspan="4" align="left"><input name="submit" type="submit" value="submit">
      <input name="button" type="button" onClick="Javascript:location.href='sddb.php';" value="cancel"></td>
    </tr>
</table>
<? } ?>
</form>

</body>
</html>

