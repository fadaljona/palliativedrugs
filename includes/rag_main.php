<?php include($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/configs/config_include.php'); 
require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/functions/rag_func.inc.php');
$query_rsRagSection		=	"SELECT id, title, active FROM rag_section WHERE active = '1' ORDER BY title ASC";
$rsRagSection			=	open_SQL($query_rsRagSection);
$row_rsRagSection		=	mysql_fetch_assoc($rsRagSection);
$totalRows_rsRagSection =	mysql_num_rows($rsRagSection);

$colname_rsEntries		=	$row_rsRagSection['id'];

if (isset($_GET['secid'])) {
  $colname_rsEntries	=	(get_magic_quotes_gpc()) ? $_GET['secid'] : addslashes($_GET['secid']);
}

$query_rsEntries		=	sprintf("SELECT id, section_id, title FROM rag_entry WHERE section_id = %s", $colname_rsEntries);
$rsEntries				=	open_SQL($query_rsEntries);
$row_rsEntries			=	mysql_fetch_assoc($rsEntries);
$totalRows_rsEntries	=	mysql_num_rows($rsEntries);
?>
<?php
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
// require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/inc-secure.php');
?>
<link href="/css/legacy.css" rel="stylesheet" type="text/css">
<h3>Welcome to the Document library</h3>
<?
echo $msg;
if($halt==TRUE) {
	exit;
}
?>
<p>To access the documents first click on the topic of interest followed by the contents.</p><br>
<table border="0" cellspacing="0" cellpadding="4" align="center">
  <tr> 
    <th>Topic</th>
    <td>&nbsp;</td>
    <th>Contents</th>
  </tr>
  <tr> 
    <td valign="top"> 
		<form name="frmSection">
			<select name="sectionid" size="8" id="sectionid" style="width:265px;" onchange=
		  "document.location.href=document.frmSection.sectionid.options[document.frmSection.sectionid.selectedIndex].value">
			<?php do { ?>
			<option value="rag_main.php?secid= <?php echo $row_rsRagSection['id']; ?>" <?php if ($colname_rsEntries==$row_rsRagSection['id']) {echo " selected";}?> ><?php echo $row_rsRagSection['title']; ?></option>
			<?php } while ($row_rsRagSection = mysql_fetch_assoc($rsRagSection)); ?> 
			</select>
		</form>
  </td>
  <td>&nbsp;</td>
  <td align="center">
		<form name="frmEntry">
			<select name="entryid" size="8" id="entryid" style="width:265px;" onchange=
		  "document.location.href=document.frmEntry.entryid.options[document.frmEntry.entryid.selectedIndex].value">
			  <?php do {  ?>
				<option value="rag_entry.php?secid=<?php echo $_GET['secid']; ?>&entryid=<?php echo $row_rsEntries['id']?>"><?php echo $row_rsEntries['title']?></option>
			  <?php
					} while ($row_rsEntries = mysql_fetch_assoc($rsEntries));
					  $rows = mysql_num_rows($rsEntries);
					  if($rows > 0) {
						  mysql_data_seek($rsEntries, 0);
						  $row_rsEntries = mysql_fetch_assoc($rsEntries);
					  }
			  ?>
			</select>
			<br>
			<input name="secid" type="hidden" id="secid" value="<?php echo $HTTP_GET_VARS['secid']; ?>">
		</form>
	 </td>
  </tr>  
</table>
<br>
To donate guidelines, treatment protocols, patient information leaflets or audit documentation (e.g. proformas, results), please 
send them as Word or PDF documents to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.
</body>
</html>
<?php
mysql_free_result($rsRagSection);
mysql_free_result($rsEntries);
?>
</div>
