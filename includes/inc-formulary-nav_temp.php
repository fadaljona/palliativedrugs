<div id="sidemenu"> 
<?php
if(!empty($_POST['set_lang'])){
         $_SESSION['site_language']=$_POST['set_lang']; 
}else if(empty($_SESSION['site_language'])){
        $_SESSION['site_language']='en';
}

if($_SESSION['site_language']=='en'){
    $enclass   =  ' selected';
}else{
    $declass  =  ' selected';
}
if($_GET['page']=='palliative-care-formulary'){

?>
<div class="languages">
	<form name="frmlang" id="frmlang" method="post" action="">
		<input type="hidden" name="set_lang" id="set_lang" value="" />
		 <ul>        
			   <li class='en<?php echo $enclass;?>'><a href="javascript:assign_language('en');">English</a></li>
			   <li class='de<?php echo $declass;?>'><a href="javascript:assign_language('de');">German</a></li>
		</ul>
	</form>
</div>
<?php

}else{
   if($_SESSION['site_language']=='en')
      $change_lang = 'de';
  else
     $change_lang = 'en';
?>
<form name="frmlang" id="frmlang" method="post" action="/palliative-care-formulary.html">
	<input type="hidden" name="set_lang" id="set_lang" value="" />
	<ul>        
       <li><a href="javascript:assign_language('<?php echo $change_lang;?>');">Change Language</a></li>     
	</ul>
</form>
<?php }
if($_SESSION['site_language']=='en'){
$navCnt						= 1;
$drug_monographs	        = formularyNav("0","Drug Monographs",'1');
$general_topics				= formularyNav("0","General Topics",'1');
$appendicies		        = formularyNav("0","Appendicies",'1');
$prelims		            = prelimsNav("0","Prelims",'1');
//$qpds                     = qpdNav('1');
?>
<?php if($prelims!=''){ ?>
	<h2>Prelims</h2>	
<?php echo $prelims;	
  } ?>
<ul id="treemenu1" class="treeview">	
   <?php if($drug_monographs!=''){ ?>
   <h2>Part 1 Drug Monographs</h2>&nbsp;(<a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')"><span class="sml_link">Expand All</span></a> | <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')"><span class="sml_link">Collapse All</span></a>)
	  <?php echo $drug_monographs?>
   <?php }if(!empty($general_topics)){ ?>
   <h2>Part 2 General Topics</h2> 
	<?php echo $general_topics;	?>	
  <?php }if(!empty($appendicies)){ ?>
   <h2>Appendices</h2> 
	<?php echo $appendicies;	?>	
  <?php } if($qpds!=''){ ?>
   <h2>Quick Practice Guides</h2> 
	<?php echo $qpds;	?>	
  <?php } ?>
</ul>
<?php } else if($_SESSION['site_language']=='de'){
$drug_monographs	        = formularyNav("0","Drug Monographs",'2');
$general_topics				= formularyNav("0","General Topics",'2');
$appendicies		        = formularyNav("0","Appendicies",'2');
$prelims		            = prelimsNav("0","Prelims",'2');
$qpds                       = qpdNav('2');

?>
<?php if($prelims!=''){ ?>
	<h2>Prelims</h2>	
<?php echo $prelims;	
  } ?>
<ul id="treemenu1" class="treeview">	
   <?php if($drug_monographs!=''){ ?>
   <h2>Part 1 Drug Monographs</h2>&nbsp;(<a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')"><span class="sml_link">Expand All</span></a> | <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')"><span class="sml_link">Collapse All</span></a>)
	  <?php echo $drug_monographs?>
   <?php }if(!empty($general_topics)){ ?>
   <h2>Part 2 General Topics</h2> 
	<?php echo $general_topics;	?>	
  <?php }if(!empty($appendicies)){ ?>
   <h2>Appendices</h2> 
	<?php echo $appendicies;	?>	
  <?php } if($qpds!=''){ ?>
   <h2>Quick Practice Guides</h2> 
	<?php echo $qpds;	?>	
  <?php } ?>
</ul>
  

<?php } ?>
	
<script type="text/javascript">
	ddtreemenu.createTree("treemenu1", true)
</script>
</div>