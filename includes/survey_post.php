<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/configs/config_include.php'); 
require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/functions/form_func.inc.php');
session_name('palliativedrugs');
session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/inc-secure.php');

$sid		=	$_POST['hidSid'];
$q_count	=	$_POST['hidQcount'];
$username	=	dLookUp("username","new_users","id=".$_SESSION['visitor']->id);

for ($i=1; $i<=$q_count; $i++) {
	$qid	=	$_POST["hidQid".$i];

	if (isset($_POST["hidMoc".$i])) {
		$tmp = "";
		$count = $_POST["hidMoc".$i];
		for ($j=1; $j<=$count; $j++) {
			$fldname = "fc" . $i . '_' . $j;
			if (IsSet($_POST[$fldname])) {
					$tmp .= strval($j).", ";
			}
		}
		$fld_val = substr($tmp,0,strlen($tmp)-2);
	} elseif (isset($_POST["ff".$i])) {
		$fld_val = $_POST["ff".$i];
	} else {
		$fld_val = "0";
	}

	if (is_array($fld_val)) {
		$fld_val = implode("|",$fld_val);
	}

	if (strlen($fld_val) < 255) {
		if (strlen($fld_val)==0) {
			$db_val = "NONE";
		} else {
			$db_val = $fld_val;
		}
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, answer, username) "
			. "VALUES ($sid, $qid, '".addslashes($db_val)."', '$username')";
	} else {
		$sql = "INSERT INTO survresults "
			. "(survey_id,question_id, long_answer, username) "
			. "VALUES ($sid, $qid, '".addslashes($db_val)."', '$username')";
	}//if strlen

	open_SQL($sql);
}//for

//log user to prevent multiples.

if($username!='aw') {
	$sql	=	"INSERT INTO survrespondent (survey_id, username) VALUES (" . $sid . ", '" . $username. "')";
	open_SQL($sql);
}

// get ack msg from db
$sql		=	"SELECT acknowledgement_msg FROM survsurvey WHERE id='".$sid."'";
$rs			=	open_SQL($sql);
$row		=	mysql_fetch_object($rs);
$ack_msg	=	$row->acknowledgement_msg;

mysql_free_result($rs);
echo "<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"/css/legacy.css\">";
echo "<p>" . $ack_msg . "</p>";
//header('Location:  /survey-acknowledgment');
exit();
?>