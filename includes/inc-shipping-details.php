<?php

$selBillingdet	=	open_SQL("SELECT * FROM 3bit_customers_address WHERE cart_sessionid = '".$_SESSION['csess_id']."' ");

if(mysql_num_rows($selBillingdet)>0){
	
	while($brow		=	mysql_fetch_array($selBillingdet)){

		$brow		=	array_map("stripslashes",$brow);

		$cust_id	=	$brow['customer_id'];

		$btitle		=	$brow['billing_title'];

		$bfirst		=	$brow['billing_fname'];

		$blast		=	$brow['billing_lname'];

		$bemail		=	$brow['email'];

		$bcompany	=	$brow['company'];

		$vat_number=	$brow['vat_number'];

		$baddress1	=	$brow['billing_address1'];

		$baddress2	=	$brow['billing_address2'];

		$bcity		=	$brow['billing_city'];

		$bstate		=	$brow['billing_state'];

		$bzip		=	$brow['billing_postcode'];

		$bphone		=	$brow['billing_phone'];

		$bcountry	=	$brow['billing_country'];

		$is_diffdeliver =	$brow['is_diffdeliver'];
		
		$dtitle		=	$brow['shipping_title'];

		$dfirst		=	$brow['shipping_fname'];

		$dlast		=	$brow['shipping_lname'];		

		$daddress1	=	$brow['shipping_address1'];

		$daddress2	=	$brow['shipping_address2'];

		$dcity		=	$brow['shipping_city'];

		$dstate		=	$brow['shipping_state'];

		$dzip		=	$brow['shipping_postcode'];

		$dphone		=	$brow['shipping_phone'];

		$dcountry	=	$brow['shipping_country'];			
		
	}	
}else {
	if(!empty($_SESSION['visitor']->id)){
		$selBillingdet	=	open_SQL("SELECT * FROM new_users WHERE id = '".$_SESSION['visitor']->id."' ");
		if(mysql_num_rows($selBillingdet)>0){
	
			while($brow		=	mysql_fetch_array($selBillingdet)){

				$brow		=	array_map("stripslashes",$brow);

				$btitle		=	$brow['title'];

				$bfirst		=	$brow['FirstName'];

				$blast		=	$brow['LastName'];

				$bemail		=	$brow['email'];

				$baddress1	=	$brow['addr1'];

				$baddress2	=	$brow['addr2'];

				$bcity		=	$brow['TownOrCity'];

				$bstate		=	$brow['StateOrCounty'];

				$bzip		=	$brow['PostalCode'];

				$bphone		=	$brow['phone'];

				$country_org=	open_SQL("SELECT id FROM 3bit_country WHERE LOWER(country) ='".$brow['Country']."'");

				if(mysql_num_rows($country_org)>0){
					$bcountry	=	mysql_result($country_org,0,0);					
				}

			}
		}

	}
}
?>

<form method="post" name="frmBilling" class="appnitro" id="frmBilling">
  <input type="hidden" name="editship" id="editship" value="<?=$is_diffdeliver;?>" />
  <input type="hidden" id="check_sessid" name="check_sessid" value="<?php echo $_SESSION['csess_id'];?>" />
  <fieldset id='billing'>
		<div class="form_description">
			<h1>Your Billing Address</h1>
			<p>Please enter your payment / statement address below.</p>
		</div>
  <p class="error"></p>
  <ul>
    <li id='biname'>
		<label class="description" for="bname">Name<span class="required">*</span></label>
		<span><input id="btitle" name= "btitle" class="element text" maxlength="4" size="4" value="<?=$btitle?>" tabindex="1"/>		
		<label>Title</label>         
		</span><span>
		<input id="bfirst" name= "bfirst" class="element text" maxlength="255" tabindex='2' size="8" value="<?=$bfirst;?>"/>
		<label>First</label>
		</span> <span>
		<input id="blast" name= "blast" class="element text" maxlength="255" tabindex='3' size="14" value="<?=$blast;?>"/>
		<label>Last</label>
		</span> 
     </li>
	 <li id="biemail">
		<label class="description" for="bemail">Email<span class="required">*</span></label>
		<div>
		  <input id="bemail" name="bemail" class="element text medium" type="text" maxlength="255" value="<?=$bemail;?>" tabindex="4"/>
		</div>
	  </li>	
		<li class="section_break"> </li>
		<li id="bicvno">
		 <label class="description" >Trust / Company Name</label>
			<div class="left">
				<input id="bcompany" name="bcompany" class="element text large" value="<?=$bcompany;?>" type="text" tabindex="5">
				<label for="bcompany">Company</label>
			</div>
			<div class="right">
				<input id="vat_number" name="vat_number" class="element text medium" value="<?=$vat_number;?>" type="text" tabindex="6">
				<label for="vat_number">Vat Number</label>
			</div>
		</li>
		<li class="section_break"> </li>
	  <li id="biaddr" >
		<label class="description" for="add">Address<span class="required">*</span></label>
		<div>
		  <input id="baddress1" name="baddress1" class="element text large" value="<?=$baddress1;?>" type="text" tabindex="5">
		  <label for="addr1">Street Address</label>
		</div>
		<div>
		  <input id="baddress2" name="baddress2" class="element text large" value="<?=$baddress2;?>" type="text" tabindex="6">
		  <label for="addr2">Address Line 2</label>
		</div>
		<div class="left">
		  <input id="bcity" name="bcity" class="element text medium" value="<?=$bcity;?>" type="text" tabindex="7">
		  <label for="city">City</label>
		</div>
		<div class="right">
		  <input id="bstate" name="bstate" class="element text medium" value="<?=$bstate;?>" type="text" tabindex="8">
		  <label for="state">County / State / Province / Region</label>
		</div>
		<div class="left">
		  <input id="bzipcode" name="bzipcode" class="element text medium" maxlength="15" value="<?=$bzip?>" type="text" tabindex="9">
		  <label for="pcode">Postcode</label>
		</div>
		<div class="right">
		  <?php
				$selcntry		= "SELECT * FROM 3bit_country WHERE iso_number != '' ORDER BY country";			
				$db_query		= mysql_query($selcntry)or die(mysql_query);				
				echo '<select id="bcountry" name="bcountry" class="element select medium" tabindex="10">
										      <option value="0">-Select country-</option>';
					while($db_rows = mysql_fetch_assoc($db_query)){
						if($db_rows['id'] == $bcountry)
							echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
						else if($bcountry == '' && $db_rows['id'] == '232')
						    echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
						else
							echo "<option value=\"".$db_rows['id']."\" >".ucfirst(strtolower($db_rows['country']))."</option> ";						
					}
				echo "</select>";
			  ?>		 
		  <label for="country">Country</label>
		</div>	
	  </li>
	  <li id="bitel" >
		<label class="description" for="mobile">Contact Telephone<span class="required">*</span></label>
		<div>
		  
		  <input id="bphone" name="bphone" class="element text medium" type="text" maxlength="255" value="<?=$bphone;?>"  tabindex="11" /><label for="inc">please include any dialing codes in case we need to contact you regarding your order</label>
		</div>
	 </li>    
    <li>
      <input type="checkbox" name="alter_delivery" id="alter_delivery" tabindex="12" onclick="toggleDelivery();" value="y" <?php if($is_diffdeliver=='1'){echo "checked";}?> />
      <label><strong> Tick here if you would like to enter an alternative delivery address for your order</strong></label>
    </li>
  </ul>
  </fieldset>
  <div class="shipping hidden">
    <fieldset id='shipping'>
	<div class="form_description">
    <h1>Your orders Delivery Address</h1>
    <p>Please enter details of your delivery address</p>
	</div>
    <p class="error"></p>
    <ul>
	  <li id='shname'>
		<label class="description" for="dname">Name<span class="required">*</span></label>
		<span><input id="dtitle" name= "dtitle" class="element text" maxlength="4" size="4" value="<?=$dtitle?>" tabindex="13"/>		
		<label>Title</label>         
		</span><span>
		<input id="dfirst" name= "dfirst" class="element text" maxlength="255" tabindex='14' size="8" value="<?=$dfirst;?>"/>
		<label>First</label>
		</span> <span>
		<input id="dlast" name= "dlast" class="element text" maxlength="255" tabindex='15' size="14" value="<?=$dlast;?>"/>
		<label>Last</label>
		</span> 
     </li>
	 <li id="shaddr" >
		<label class="description" for="add">Address<span class="required">*</span></label>
		<div>
		  <input id="daddress1" name="daddress1" class="element text large" value="<?=$daddress1;?>" type="text" tabindex="16">
		  <label for="addr1">Street Address</label>
		</div>
		<div>
		  <input id="daddress2" name="daddress2" class="element text large" value="<?=$daddress2;?>" type="text" tabindex="17">
		  <label for="addr2">Address Line 2</label>
		</div>
		<div class="left">
		  <input id="dcity" name="dcity" class="element text medium" value="<?=$dcity;?>" type="text" tabindex="18">
		  <label for="city">City</label>
		</div>
		<div class="right">
		  <input id="dstate" name="dstate" class="element text medium" value="<?=$dstate;?>" type="text" tabindex="19">
		  <label for="state">County / State / Province / Region</label>
		</div>
		<div class="left">
		  <input id="dzipcode" name="dzipcode" class="element text medium" maxlength="15" value="<?=$dzip?>" type="text" tabindex="20">
		  <label for="pcode">Postcode</label>
		</div>
		<div class="right">
		  <?php
				$selcntry		= "SELECT * FROM 3bit_country WHERE iso_number != '' ORDER BY country";			
				$db_query		= mysql_query($selcntry)or die(mysql_query);				
				echo '<select id="dcountry" name="dcountry" class="element select medium" tabindex="21">
										      <option value="0">-Select country-</option>';
					while($db_rows = mysql_fetch_assoc($db_query)){
						if($db_rows['id'] == $dcountry)
							echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
						else if($dcountry == '' && $db_rows['id'] == '232')
						    echo "<option value=\"".$db_rows['id']."\" selected=selected>".ucfirst(strtolower($db_rows['country']))."</option> ";
						else
							echo "<option value=\"".$db_rows['id']."\" >".ucfirst(strtolower($db_rows['country']))."</option> ";						
					}
				echo "</select>";
			  ?>		 
		  <label for="country">Country</label>
		</div>	
	  </li>
	  <li id="shtel" >
		<label class="description" for="mobile">Contact Telephone<span class="required">*</span></label>
		<div>
		  
		  <input id="dphone" name="dphone" class="element text medium" type="text" maxlength="255" value="<?=$dphone;?>"  tabindex="22" />
		<label for="inc">please include any dialing codes in case we need to contact you regarding your order</label>
		</div>
	 </li>          
    </ul>
    </fieldset>
  </div>
  <div class="otherinfo">    
    <div class="cart_action_buttons"><input type="button" value="Continue to payment" id="checkout" class="checkout-cart"/>	
      <div class="clearer"/>
		</div>
    </div>
  </div>
</form>
