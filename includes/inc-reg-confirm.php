<p>Please note that our site uses "cookies". "Cookies" are small amounts of information which are stored on your computer to make it possible for you to login to our site and to enable us to personalise the content of the site for you. Our site will not function correctly if your browser is set not to accept cookies.</p>
<form action="/user-preferences.html" method="post">
<input type="hidden" name="reg_confirm" value="yes">
<table>
	<tr><td colspan="2" bgcolor="#cccccc">Registration summary</td></tr>
	<tr><td colspan="2">Before proceeding, please check your registration details are accurate and, if necessary, correct any mistakes.<br /><br /></td></tr>

<?php
$_SESSION['reg_confirm']		=	true;
?>
