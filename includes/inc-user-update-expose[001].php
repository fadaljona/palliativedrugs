<?php
if($_SESSION['visitor']->logged_in) {
	if($_SESSION['visitor']->acl=='user') {	
          $sql         =    "SELECT title, FirstName,LastName,organisation,addr1,addr2,addr3,addr4,TownOrCity,StateOrCounty,PostalCode,Country FROM new_users WHERE id='" . $_SESSION['visitor']->id . "'";
			$recs       =     open_SQL($sql);
			if(mysql_num_rows($recs))
			{
				$row		=	mysql_fetch_assoc($recs);
				$utitle		=	stripslashes($row['title']);
				$ufname		=	stripslashes($row['FirstName']);
				$ulname		=	stripslashes($row['LastName']);
				$uorg		=	stripslashes($row['organisation']);
				$uaddr1		=	stripslashes($row['addr1']);
				$uaddr2		=	stripslashes($row['addr2']);
				$uaddr3		=	stripslashes($row['addr3']);
				$uaddr4		=	stripslashes($row['addr4']);
				$utown		=	stripslashes($row['TownOrCity']);
				$ustate		=	stripslashes($row['StateOrCounty']);
				$upostalcode=	stripslashes($row['PostalCode']);
				$ucountry	=	stripslashes($row['Country']);
			 }  
?>	
<style>
.aapple_overlay{position:absolute !important;}
</style>
<div class="apple_overlay popup_contain" id="custom">
  <div class="popup">
    
	<div id="uDetails" style="display:none;">
		<h1>Updates for Palliativedrugs.com</h1>
		<form name="frmType" id="frmType" method="post" >
		  <input type='hidden' name='userid' id='userid' value="<?=$_SESSION['visitor']->id?>">
		  <fieldset>
			<p><strong>Dear <?=$utitle?> <?=$ufname?> <?=$ulname?></strong></p>
			<p>As part of updating the website please would you confirm that your registration details are correct with regards to your employing organization: </p>
			<label for="organisation">Organization name</label>
			<input type="text" name="organisation" id="organisation" value="<?=$uorg?>" />
			<label for="address1">Work address</label>
			<input type="text" name="address1" id="address1" value="<?=$uaddr1?>" />
			<label for="address2">&nbsp;</label>
			<input type="text" name="address2" id="address2" value="<?=$uaddr2?>" />
			<label for="address3">&nbsp;</label>
			</label>
			<input type="text" name="address3" id="address3" value="<?=$uaddr3?>" />
			<label for="address4">&nbsp;</label>
			<input type="text" name="address4" id="address4"  value="<?=$uaddr4?>" />
			<label for="city">City/Town</label>
			<input type="text" name="city" id="city" value="<?=$utown?>" />
			<label for="county">State/County</label>
			<input type="text" name="county" id="county" value="<?=$ustate?>" />
			<label for="postcode">Post Code</label>
			<input type="text" name="postcode" id="postcode" value="<?=$upostalcode?>" />
			<label for="country">Country</label>
			<input type="text" name="country" id="country" value="<?=$ucountry?>" />
			<p><em>Please amend these details if necessary.</em> </p>
			<div class="block">
			<p class="error">Please ensure you have selected your organization type </p>
			<p><strong>Organization type</strong><br />
				Please can you also indicate if the organization is 'Not-for-profit', e.g. NHS, independent hospice, or 'For-profit', e.g. Commercial Pharmacy, Private Hospital.</p>          
			  <label for="NotProfit">Not for Profit</label>
			  <input type="radio" name="usertype" id="NotProfit" value="non-profit" />
			  <label for="Profit">For Profit</label>
			  <input type="radio" name="usertype" id="Profit" value="for-profit" />
			</div>
			<p>Once you have completed the above please click on <strong>"Update"</strong> to save.</p>
			<input type="button" value="UPDATE" id="update" name="submit" onclick="return validate_update();"/>
		  </fieldset>
		</form>
		<div class="foot">
		  <p>Note: if other updates are required to your registration details, you can make these changes at any time by logging on to the site and selecting 'Preferences' on the left hand side of the home page. </p>
		</div>
	</div>
	<div id="uAgree" class="popupagreement" style="height:450px;overflow-y: scroll;padding:10px;">
      <p><strong>Welcome to the new palliativedrugs.com website.  </strong></p>
      <p><em>To access the site please read  our updated Terms and conditions of use and agree to them by clicking the 'I Agree' button at the bottom of this box.</em>
<?
		$getTerms		=	open_SQL("SELECT * FROM 3bit_articles WHERE article_title = 'Terms and conditions of use'");
		$fecgetTerms	=	mysql_fetch_array($getTerms);
		echo stripslashes($fecgetTerms['long_text']);
		?>
        <br />
      </p>
      <center><input type="button" value="I Agree" onclick="showUserInfo();"><br /><br /></center>
	</div>
  </div>
</div>
<?php }
}
if($_SESSION['visitor']->acl=='user') {	
?>
<script  type='text/javascript'>
$(function() {
		var api2   =   $("#custom").overlay({mask: '#000', effect: 'apple',load: true, height:200, close:'closebtn'});
		$("#custom").css("position","top");

});

function showUserInfo(){
	$('#uAgree').hide();
	$('#uDetails').show();
}
</script>
<?php
}
?>