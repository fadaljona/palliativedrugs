<?php
//require_once('_include/pall_db.inc.php');
//require_once('_include/visitor.class.php');
echo "<p>";

if($_SERVER['REQUEST_METHOD']=='POST') {
	
	$qs			= (isset($_POST['quicksearch'])) ? TRUE : FALSE;
	
	$search_for = trim($_POST['search_for']);
	
	if(strlen($search_for) <2 ) {
		echo "Please enter your topic into the search box";		
	} else {	
		echo "Results in: <br />";
		if(isset($_POST['search_formulary']) || $qs) {
			echo "<br /><b>Formulary</b> (all)<br />";
			for($k=1;$k<=2;$k++){
				$sql		=	"SELECT a.formulary_id,a.language_id,f.formulary_title,f.alias,LOWER(l.language_code)as language FROM 3bit_formulary_articles a,3bit_formulary f,3bit_language l WHERE f.status='1' AND a.formulary_id=f.formulary_id AND a.language_id=l.language_id AND l.language_id='".$k."' AND (a.content like '%" . $search_for . "%' ) GROUP BY f.formulary_id";
	
				$result		=	open_SQL($sql);
				while( $row =	mysql_fetch_array( $result ) ) {
					$formtitle	=	stripslashes($row['formulary_title']);		
					$ftitle		=	stripslashes($row['formulary_title']);				
					if(trim($row['alias'])!='')
						$ftitle		=	stripslashes($row['alias']);
	
					$link = '/formulary/'.$row['language'].'/'.remSpecial($ftitle).'.html';
					echo '&nbsp;&nbsp;&nbsp;<a href="' . $link . '">' . $formtitle . '</a><br />';				
				}
			}
		}
		
		if(isset($_POST['search_bb']) || $qs) {
			echo "<br /><br /><b>Bulletin board</b> (up to 50 most recent)<br /><br />";			

			$sql		=	"SELECT message_id, thread, subject, datestamp, SUBSTRING(body,1,160) AS tbody FROM phorum_messages  WHERE 1 AND status='2' AND (subject like '%". $search_for."%' OR body like '%" . $search_for . "%') ORDER BY datestamp DESC ";
			$sql 		.=	" LIMIT 50";

			$rs			=	open_SQL($sql); 

			while($row=mysql_fetch_array($rs)) {				
				$link = "/forum.html?thread=".$row['thread']."&amp;msg=".$row['message_id'];			
				echo '<a href="'.$link.'">' . $row['subject'] . '</a> [' . substr($row['datestamp'],0,10) . ']<br />';
				echo $row['tbody'] . '...<br /><br />';
			}
		}
		
		if(isset($_POST['search_doclib']) || $qs) {
			echo "<br /><br /><b>Document library</b> (up to 30 most recent)<br />";
			
			$sql		=	"SELECT id, title, entry_id, DateAdded, SUBSTRING(notes,1,160) AS tnotes FROM rag_doc WHERE (title like '%".$search_for."%' OR notes like '%".$search_for."%') ORDER BY DateAdded DESC LIMIT 50";

			$rs			=	open_SQL($sql); 

			while($row=mysql_fetch_array($rs)) {				
				$link = "/doc-library.html?&entryid=".$row['entry_id'];				
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$link.'">' . $row['title'] . '</a> [' . substr($row['DateAdded'],0,10) . ']<br />';			
			}
		}
		
		if(isset($_POST['search_news']) || $qs) {

			echo "<br /><br /><b>News</b> (up to 30 most recent)<br />";			
			$sql		=	"SELECT news_id, news_date, category, title,alias_title,DATE_FORMAT(news_date,'%M') as nmonth FROM 3bit_news WHERE type='news' AND (title like '%".$search_for."%' OR alias_title like '%".$search_for."%' OR content like '%".$search_for."%') ORDER BY news_date DESC LIMIT 30";

			$rs			=	open_SQL($sql); 
			while($row=mysql_fetch_array($rs)) {
				if($row['alias_title']!='')
					$link = '/news/' .remSpecial($row['nmonth']).'/'.remSpecial($row['alias_title']).'.html';
				else
					$link = '/news/' .remSpecial($row['nmonth']).'/'.remSpecial($row['title']).'.html';
				
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$link.'">'.$row['title'].'</a> ['.substr($row['news_date'],0,10). ']'.' ['.$row['category'].']<br />';

			}
		}
		
		if(isset($_POST['search_latest']) || $qs) {
			echo "<br /><br /><b>Latest additions</b> (up to 30 most recent)<br />";			
			$sql		=	"SELECT news_id, news_date, category, title,alias_title,DATE_FORMAT(news_date,'%M') as nmonth FROM 3bit_news WHERE type='addition' AND (title like '%" . $search_for . "%' OR alias_title like '%" . $search_for . "%' OR content like '%" . $search_for . "%') ORDER BY news_date DESC LIMIT 30";

			$rs			=	open_SQL($sql); 
			while($row=mysql_fetch_array($rs)) {				
				if($row['alias_title']!='')
					$link = '/latest/' .remSpecial($row['nmonth']).'/'.remSpecial($row['alias_title']).'.html';
				else
					$link = '/latest/' .remSpecial($row['nmonth']).'/'.remSpecial($row['title']).'.html';
				
				echo '&nbsp;&nbsp;&nbsp;<a href="'.$link.'">' . $row['title'] . '</a> ['
				 . substr($row['news_date'],0,10) . ']'
				 . ' [' . $row['category'] . ']<br />';			
			}
		}
	}
} 
?>
</p>
<br />
<br />
<br />
<br />
<br />
