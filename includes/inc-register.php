<?php
// setup vars
global $pagenum, $form, $fields, $errors, $val_fields;

$form = '';
$fields = array();
$errors = array();
$pagenum = (IsSet($_POST['pagenum'])) ? $_POST['pagenum'] : 1;
$val_fields = '';

function validate($fields) {
	global $errors;
	foreach($fields as $key=>$value) {
		switch($key) {
			// page 1
			case 'username':
				// lookup duplicates...
				if(!strlen($value)) {
					$errors[] = "please enter a username";
				} elseif(dLookUp('username','new_users',"username='".$value."'")!=null) {
					$errors[] = "that username is already taken, please choose another";
				}
				break;
			case 'email':
				if(!strlen($value)) {
					$errors[] = "please enter an e-mail address";
				}
				if(!strpos($value,'@')) {
					$errors[] = 'please enter a valid e-mail address';
				}
				break;
			case 'pswd':
				if(!strlen($value)) {
					$errors[] = "please enter a password";
				}
				break;
			case 'pswd2':
				if(!strlen($value)) {
					$errors[] = "please re-enter a password";
				}
				if(!($value==$fields['pswd'])) {
					$errors[] = "your passwords do not match";
				}
				break;
			case 'firstname':
				if(!strlen($value)) {
					$errors[] = "please enter your first name";
				}
				break;
			case 'lastname':
				if(!strlen($value)) {
					$errors[] = "please enter your last name";
				}
				break;
				
			case 'title':
				if($value=='none') {
					$errors[] = "please select your title";
				}
				break;
			/*case 'role':
				if($value=='none') {
					$errors[] = "please select your professional group";
				} elseif($value=='Student other') {
					if(!strlen($fields['role_other'])) {
						$errors[] = "you selected 'student other' for professional group, please give details";
					}	
				} 
				break;
			case 'role_other':
				if($fields['role']=='Other') {
					if(!strlen($value)) {
						$errors[] = "you selected 'other' for professional group, please give details";
					}	
				} 
				break;*/
			// page 2
			case 'reg_num':
				if(!strlen($value)) {
					$errors[] = "please enter a registration number";
				}
				break;
				
			case 'prescriber':
				if($value=='none') {
					$errors[] = "please specify if you are a presciber nurse";
				}
				break;
			case 'primary':
				if($value=='none') {
					$errors[] = "please specify your primary speciality";
				}
				if($value=="other") {
					if(!strlen($fields['primary_details'])) {
						$errors[] = "you selected 'other' for main speciality, please give details";
					}	
				}
				break;
			case 'sub':
				if($value=='none') {
					$errors[] = "please specify your sub-speciality, or select 'None'";
				}
				if($value=="other") {
					if(!strlen($fields['sub_details'])) {
						$errors[] = "you selected 'other' for sub-speciality, please give details";
					}	
				}
				break;
			// page 3
			case 'organisation':
				if(!strlen($value)) {
					$errors[] = "please enter an organisation name";
				}
				break;
			/*case 'usertype':
				if(!strlen($value)) {
					$errors[] = "please select organisation type";
				}
				break;*/
			case 'addr1':
				if(!strlen($value)) {
					$errors[] = "please enter your organisation address";
				}
				break;
			case 'country':
				if($value=='none') {
					$errors[] = "please enter your country";
				}
				break;
			case 'phone':
				if(!strlen($value)) {
					$errors[] = "please enter your a phone number";
				}
				break;
			/*case 'main_workplace':
				if($value=='none') {
					$errors[] = "please select your main workplace";
				} elseif ($value=='other') {
					if(!strlen($fields['workplace_details'])) {
						$errors[] = "you selected 'other' for main workplace, please give details";
					}
				}
				break;*/
			case 'current_post':
				if($value=='none') {
					$errors[] = "please select your current post";
				} elseif ($value=='other') {
					if(!strlen($fields['current_post_details'])) {
						$errors[] = "you selected 'other' for current post, please give details";
					}
				}
				break;			
			// page 4
			case 'years_in_post':
				if($value=='none') {
					$errors[] = "please select AcceptBBmailsthe number of years in your current post";
				}
				break;
			case 'time_spent':
				if($value=='none') {
					$errors[] = "please select the time spent in direct patient care";
				}
				break;	
			case 'gender':
				if($value=='none') {
					$errors[] = "please select your gender";
				}
				break;
			case 'year_of_birth':
				if($value=='none') {
					$errors[] = "please select your year of birth";
				}
				break;
			// last page
			case 'accept_update_emails':
				if($value=='none') {
					$errors[] = "please specify whether you want to accept e-mails relating to site updates.";
				}
				break;
			case 'accept_bb_emails':
				if($value=='none') {
					$errors[] = "please specify whether you want to accept bulleting board digest e-mails.";
				}
				break;
			case 'lead': 
				if($value=='none') {
					$errors[] = "please tell us where you heard about us";
				} elseif($value=='Other (please specify)') {
					if(!strlen($fields['lead_details'])) {
						$errors[] = "you have selected 'other', please give details.";
					}
				}
				break;
		} //switch

	} //foreach
	if(sizeof($errors)==0) 
		return true;
	else
		return false;
}

if(IsSet($_SESSION['old_user_id'])) {
	// it's an old user so pre-fill some fields
	$old_user	=	true;
	$sql		=	"SELECT username, email, pwd, FirstName, LastName, title FROM users WHERE id = '".$_SESSION['old_user_id']."'";
	$rs			=	open_SQL($sql);
	$row		=	mysql_fetch_assoc($rs);
	$username	=	$row['username'];
	$email		=	$row['email'];
	$pswd		=	$row['pwd'];
	$pswd2		=	$row['pwd'];
	$firstname	=	$row['FirstName'];
	$lastname	=	$row['LastName'];
	$title		=	$row['title'];
	unset($_SESSION['old_user_id']);
}

if($_SERVER['REQUEST_METHOD']=='POST') {
	switch($pagenum) {
		case 1:
			$fields['username']		= htmlentities(stripslashes($_POST['ausername']));
			$fields['email']		= htmlentities(stripslashes($_POST['email']));
			$fields['pswd']			= htmlentities(stripslashes($_POST['apswd']));
			$fields['pswd2']		= htmlentities(stripslashes($_POST['pswd2']));
			$fields['firstname']	= htmlentities(stripslashes($_POST['firstname']));
			$fields['lastname']		= htmlentities(stripslashes($_POST['lastname']));
			$fields['title']		= htmlentities(stripslashes($_POST['sel_title']));
			$fields['role']			= htmlentities(stripslashes($_POST['sel_role']));
			$fields['role_other']	= htmlentities(stripslashes($_POST['role_other']));
			$button_label			= 'continue';
			$prescriber				= 'none';
			$primary				= 'none';
			$sub					= 'none';			
			break;
		case 2:
		case 3:
			$fields['reg_num']		= htmlentities(stripslashes($_POST['reg_num']));
			$fields['primary']		= htmlentities(stripslashes($_POST['sel_primary']));
			$fields['primary_details'] = htmlentities(stripslashes($_POST['primary_details']));
			$fields['sub']			= htmlentities(stripslashes($_POST['sel_sub']));
			$fields['sub_details']	= htmlentities(stripslashes($_POST['sub_details']));
			$fields['prescriber']	= IsSet($_POST['prescriber']) ? htmlentities(stripslashes($_POST['prescriber'])) : 'no';
			$button_label			= 'continue';
			$main_workplace			= 'none';
			$current_post			= 'none';	
			break;
		case 4:
			$fields['organisation'] = htmlentities(stripslashes($_POST['organisation']));
			$fields['usertype']		= htmlentities(stripslashes($_POST['usertype']));
			$fields['addr1']		= htmlentities(stripslashes($_POST['addr1']));
			$fields['addr2']		= htmlentities(stripslashes($_POST['addr2']));
			$fields['addr3']		= htmlentities(stripslashes($_POST['addr3']));
			$fields['addr4']		= htmlentities(stripslashes($_POST['addr4']));
			$fields['TownOrCity']	= htmlentities(stripslashes($_POST['TownOrCity']));
			$fields['StateOrCounty']= htmlentities(stripslashes($_POST['StateOrCounty']));
			$fields['PostalCode']	= htmlentities(stripslashes($_POST['PostalCode']));
			$fields['country']		= htmlentities(stripslashes($_POST['sel_country']));
			$fields['phone']		= htmlentities(stripslashes($_POST['phone']));
			$fields['main_workplace'] = htmlentities(stripslashes($_POST['main_workplace']));
			$fields['workplace_details'] = htmlentities(stripslashes($_POST['workplace_details']));
			$fields['current_post'] = htmlentities(stripslashes($_POST['current_post']));
			$fields['current_post_details'] = htmlentities(stripslashes($_POST['current_post_details']));
			$button_label			= 'continue';	
			break;
		case 5:
			$fields['years_in_post']= htmlentities(stripslashes($_POST['years_in_post']));
			$fields['time_spent']	= htmlentities(stripslashes($_POST['time_spent']));
			$fields['gender']		= htmlentities(stripslashes($_POST['gender']));
			$fields['year_of_birth']= htmlentities(stripslashes($_POST['year_of_birth']));
			$button_label			= 'continue';
			$lead					= 'none';
			break;
		case 6:
			$fields['accept_update_emails'] = htmlentities(stripslashes($_POST['accept_update_emails']));
			$fields['accept_bb_emails']		= htmlentities(stripslashes($_POST['accept_bb_emails']));
			$fields['lead']					= htmlentities(stripslashes($_POST['lead']));
			$fields['lead_details']			= htmlentities(stripslashes($_POST['lead_details']));
			$button_label					= 'continue';
		//	break;
//		case 7:
			//save data and redirect to welcome page
			
			$sql	= "INSERT INTO new_users (username) VALUES('".$_POST['val_username']."')";
			open_SQL($sql);
			$id		= mysql_insert_id();

			//INSERT TEMP SUBSCRIPTION FOR USER
			// $dateYesterday		=	date("Y-m-d H:i:s", strtotime("-1 day"));
			// $sqlSub		=	"INSERT INTO 3bit_subscribers (user_id, start_date, end_date, duration, group_id, order_id, status, created_date, modified_date) VALUES ('".$id."', '".$dateYesterday."', '2012-02-03 00:00:00', '60', '0', '0', '1', NOW(), NOW())";
			// open_SQL($sqlSub);


			$prescriber		= ($_POST['val_prescriber']=='yes') ? '1' : '0';
			$site_emails	= ($_POST['accept_update_emails']=='yes') ? '1':'0';
			$bb_emails		= ($_POST['accept_bb_emails']=='yes') ? '1':'0';
			if($_POST['val_role']=='other') {
				$role = $_POST['val_role_other'];
			} else {
				$role = $_POST['val_role'];
			}
			if($_POST['val_primary']=='other') {
				$speciality = $_POST['val_primary_details'];
			} else {
				$speciality = $_POST['val_primary'];
			}
			if($_POST['val_sub']=='other') {
				$sub_speciality = $_POST['val_sub_details'];
			} else {
				$sub_speciality = $_POST['val_sub'];
			}
			if($_POST['val_main_workplace']=='other') {
				$main_workplace = $_POST['val_workplace_details'];
			} else {
				$main_workplace = $_POST['val_main_workplace'];
			}
			if($_POST['val_current_post']=='other') {
				$current_post = $_POST['val_current_post_details'];
			} else {
				$current_post = $_POST['val_current_post'];
			}		
			if($_POST['lead']=='other') {
				$lead = $_POST['lead_details'];
			} else {
				$lead = $_POST['lead'];
			}
			if(IsSet($_POST['val_years_in_post'])) {
				$years_in_post = $_POST['val_years_in_post'];
			} else {
				$years_in_post = 0;
			}
			
			$sql = "UPDATE new_users SET "
					. "email						=		'".addslashes($_POST['val_email'])."', "
					. "pwd							=		'".addslashes($_POST['val_pswd'])."', "
					. "FirstName				=		'".addslashes($_POST['val_firstname'])."', "
					. "LastName					=		'".addslashes($_POST['val_lastname'])."', "
					. "title						=		'".addslashes($_POST['val_title'])."', "
					. "role							=		'".addslashes($role)."', "
					. "prof_reg_num			=		'".addslashes($_POST['val_reg_num'])."', "
					. "nurse_prescriber	=		'".addslashes($prescriber). "', "
					. "speciality				=		'".addslashes($speciality)."', "
					. "sub_specialty		=		'".addslashes($sub_speciality)."', "
					. "organisation			=		'".addslashes($_POST['val_organisation'])."', "
					. "addr1						=		'".addslashes($_POST['val_addr1'])."', "
					. "addr2						=		'".addslashes($_POST['val_addr2'])."', "
					. "addr3						=		'".addslashes($_POST['val_addr3'])."', "
					. "addr4						=		'".addslashes($_POST['val_addr4'])."', "
					. "TownOrCity				=		'".addslashes($_POST['val_TownOrCity'])."', "
					. "StateOrCounty		=		'".addslashes($_POST['val_StateOrCounty'])."', "
					. "PostalCode				=		'".addslashes($_POST['val_PostalCode'])."', "
					. "Country					=		'".addslashes($_POST['val_country'])."', "
					. "phone						=		'".addslashes($_POST['val_phone'])."', "
					. "main_workplace		=		'".addslashes($main_workplace)."', "
					. "current_post			=		'".addslashes($current_post)."', "
					. "years_in_post		=		'".addslashes($years_in_post)."', "
					. "patient_care_time=		'".addslashes($_POST['val_time_spent'])."', "
					. "gender						=		'".addslashes($_POST['val_gender'])."', "
					. "year_of_birth		=		'".addslashes($_POST['val_year_of_birth'])."', "
					. "accept_site_emails=	'".addslashes($site_emails)."', "
					. "accept_bb_emails	=		'".addslashes($bb_emails)."', "
					. "UserType					=		'".addslashes($_POST['val_usertype'])."', "
					. "lead							=		'".addslashes($lead)."', "
					. "DateRegistered		=		CURDATE(), "
					. "vetted						=		'0', "
					. "active						=		'0', "
					. "previous_visit		=		CURDATE(), "
					. "current_visit		=		CURDATE(), "	
					. "browser					=		'".addslashes($_SERVER['HTTP_USER_AGENT'])."', "
					. "ip								=		'".addslashes($_SERVER['REMOTE_ADDR'])."' "				
					. "WHERE id					=		'".$id."'";
			open_SQL($sql);
			$_SESSION['new_user_id'] = $id;
			header('Location:  /reg-confirm.html');			
			break;
		default:
			//die('form error (621)');
	} //switch

	if(validate($fields)) {
		//add validated fields as hidden form fields and move on to next page

		foreach($fields as $key=>$value) {
			$val_fields .= '<input type="hidden" name="val_'.$key.'" value="'. $value .'">' . "\n";
		}
		//need to determine next page based on previous answer...

		switch($pagenum) {
			case 1:
				/*if($fields['role']=='Nurse') { 
					$pagenum = 2;
				} elseif(strpos($fields['role'],'Student')===false) {
					$pagenum = 3;
				} else { 
					$pagenum = 4;
				};*/
				$pagenum = 4;
				break;
			case 2:
			case 3:
				 $pagenum = 4; 
				 break;
			case 4: 
				/*if(strpos($_POST['val_role'],'Student')===false) {
					$pagenum = 5;
				} else {
					$pagenum = 6;
				}*/
				$pagenum = 6;
				break;
//			case 'confirm':
//				header('Location: /confirm_registration.html');
			default:
				$pagenum++;
		}//switch
	} else {
		// data *not* ok, re-display current page with error message(s)
		$error_msg = "please correct your errors<br/>";
		foreach($errors as $error) 
			$error_msg .= $error . '<br />';
		$form_error_msg	= $error_msg;
		foreach($fields as $key=>$value) 
			$key=$value;
	}

	if($pagenum==7) {
		foreach($_POST as $key=>$value) {
			if(substr($key,0,4)=='val_') {
				$val_fields .= '<input type="hidden" name="val_'.str_replace('val_','',$key).'" value="'. $value .'">' . "\n";
			}
		}		
		//post all data to db here then call 'prefs' with some options to allow editable confirm page.		
		$pagenum		= 'confirm';
		$button_label	= 'register';
		
		
	} else {
		//do previously validated flds
		foreach($_POST as $key=>$value) {
			if(substr($key,0,4)=='val_') {
				$val_fields .= '<input type="hidden" name="val_'.str_replace('val_','',$key).'" value="'. $value .'">' . "\n";
			}
		}
	}
} else {
	$button_label	=	'continue';
	$role			=	'none';
}

//if(!$end) {
	$pagenum		=	$pagenum;	
	$val_fields		=	$val_fields;
	$seltitle		=	getTitle($fields['title']);	
	$selrole		=	getRole($fields['role']);
	$selpresc		=	getBool($fields['prescriber']);
	$selspec		=	getSpeciality($fields['primary']);
	$selsub			=	getSubspec($fields['sub']);
	$selcountry		=	getCountry($fields['country']);
	$selmainwplace	=	getMainworkplace($fields['main_workplace']);
	$selcurrentpost	=	getCurrentpost($fields['current_post']);
	$selyearinpost	=	getYearsinpost($fields['years_in_post']);
	$seltimespent	=	getTimespent($fields['time_spent']);
	$selgender		=	getGender($fields['gender']);
	$selyofbirth	=	getYearofbirth($fields['year_of_birth']);
	$selacceptemail	=	getBool($fields['accept_update_emails']);
	$selbbemail		=	getBool($fields['accept_bb_emails']);
	$sellead		=	getLead($fields['lead']);



    // Registration form header content =====================================================================

	if($pagenum=='confirm') {
		$form_header	='<p>Please note that our site uses "cookies". "Cookies" are small amounts of information which are stored on your computer to make it possible for you to login to our site and to enable us to personalise the content of the site for you. Our site will not function correctly if your browser is set not to accept cookies.</p>';	
	} elseif($old_user) { 
		$form_header	='<p>Please enter your personal details below. We require this data to confirm you meet our <a href="/terms-and-conditions.html#eligibility-criteria" target="_blank">eligibility criteria</a> for registration (i.e., you are a health professional, whether qualified, working in an identifiable clinical or academic setting or in training at an identifiable higher education institute); to notify you of new developments relating to our site; and to invite you to undertake market research which is relevant to you. Please read our <a href="/privacy.html"target="_blank">Privacy policy</a> first which explains how we use and protect the information that you provide.</p>
		<p>'.$form_error_msg.'</p>
		<form action="/register.html" method="POST">
		<input type="hidden" name="pagenum" value="'.$pagenum.'">
		'.$val_fields;
	} else { 
		$form_header	='<p>To register with palliativedrugs.com please complete the form below. We require this data to confirm that you meet our <a href="/terms-and-conditions.html#eligibility-criteria" target="_blank">eligibility criteria</a> for registration (i.e. you are a health professional, whether qualified, working in an identifiable clinical or academic setting or in training at an identifiable higher education institute). By submitting this form you agree to palliativedrugs.coms website user agreement, <a href="/privacy.html"target="_blank">Privacy Policy</a> and Cookie policy.</p>
		<p>'.$form_error_msg.'</p>
		<form action="/register.html" method="POST">
		<input type="hidden" name="pagenum" value="'.$pagenum.'">
		'.$val_fields;
	
		
	}	

	// Registration form main content =====================================================================
	
	$form1			=	'<!-- page 1 -->
						<table>
						<tr><td colspan="2" bgcolor="#CCCCCC">Login information</td></tr>
						<tr>
							<td>Username</td>
							<td><input type="text" name="ausername" value="'.$fields['username'].'"  style="padding:0px; width:auto;  font-size:1.2em;  "></td>
						</tr>
						<tr>
							<td>E-mail address</td>
							<td><input type="text" name="email" value="'.$fields['email'].'"  style="padding:0px; width:auto; font-size:1.2em;  "></td>
						</tr>
						<tr>
							<td>Password</td>
							<td><input type="password" name="apswd" value="'.$fields['pswd'].'"  style="padding:0px; width:auto; font-size:1.2em; " ></td>
						</tr>
						<tr>
							<td>Re-enter password</td>
							<td><input type="password" name="pswd2" value="'.$fields['pswd2'].'" style="padding:0px; width:auto;  font-size:1.2em; "></td>
						</tr>
						<tr>
							<td colspan="2" bgcolor="#CCCCCC">Professional contact
							information (all fields required)</td>
						</tr>
						<tr>
							<td>First name</td>
							<td><input type="text" name="firstname" value="'.$fields['firstname'].'" style="padding:0px; width:auto; font-size:1.2em;  "></td>
						</tr>
						<tr>
							<td>Last name</td>
							<td><input type="text" name="lastname" value="'.$fields['lastname'].'" style="padding:0px; width:auto; font-size:1.2em;  "></td>
						</tr>

						<tr>
							<td>Title</td>
							<td><select name="sel_title" id="sel_title" style="padding:0px; width:auto;  font-size:1.2em; ">							
								'.$seltitle.'
							</select></td>
						</tr>
						<!-- <tr>
							<td>Professional group</td>
							<td><select name="sel_role" id="sel_role" style="padding:0px; width:auto;  font-size:1.2em;">							
								'.$selrole.'
							</select></td>
						</tr>
						<tr>
							<td>Details (if other)</td>
							<td><input size="30" name="role_other" maxlength="200" value="'.$fields['role_other'].'" style="padding:0px; width:auto; font-size:1.2em;  "></td>
						</tr> -->
					</table>';

	 $form2		=	'<!-- page 2 (nurses) -->
						<table>
							<tr>
								<td colspan="2" bgcolor="#CCCCCC">Professional contact information (all fields required)</td>
							</tr>
							<tr>
								<td>Professional registration number with your<br />regulatory body/organization</td>
								<td valign="top"><input type="text" name="reg_num" value="'.$fields['reg_num'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<tr>
								<td>Are you a nurse prescriber ?</td>
								<td>
									<select name="prescriber"  style="padding:0px; width:auto;  font-size:1.2em; ">
									'.$selpresc.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Primary or main specialty</td>
								<td>
									<select name="sel_primary" id="sel_primary"  style="padding:0px; width:auto;  font-size:1.2em; ">
										'.$selspec.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Details (if other)</td>
								<td><input type="text" size="40" name="primary_details" value="'.$fields['primary_details'].'"  style="padding:0px; width:auto;  font-size:1.2em; "></td>
							</tr>
							<tr>
								<td>Sub-specialty</td>
								<td>
									<select name="sel_sub" id="sel_sub"  style="padding:0px; width:auto;  font-size:1.2em; ">
									'.$selsub.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Details (if other)</td>
								<td><input type="text"  size="40" name="sub_details" value="'.$fields['sub_details'].'" style="padding:0px; width:auto;  font-size:1.2em; "></td>
							</tr>
						</table>';

	$form3			=	'<!-- reg_form_3 (page-2 / others) -->
						<table>
							<tr>
								<td colspan="2" bgcolor="#CCCCCC">Professional contact information (all fields required)</th>
							</tr>
							<tr>
								<td>Professional registration number with your<br />regulatory body/organization</td>
								<td valign="top"><input type="text" name="reg_num" value="'.$fields['reg_num'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<tr>
								<td>Primary or main specialty</td>
								<td>
									<select name="sel_primary" id="sel_primary" style="padding:0px; width:auto; font-size:1.2em;">
										'.$selspec.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Details (if other)</td>
								<td><input type="text" size="40" name="primary_details" value="'.$fields['primary_details'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<tr>
								<td>Sub-specialty</td>
								<td>
									<select name="sel_sub" id="sel_sub" style="padding:0px; width:auto; font-size:1.2em;">
									'.$selsub.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Details (if other)</td>
								<td><input type="text"  size="40" name="sub_details" value="'.$fields['sub_details'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
						</table>';

	$form4			=	'<!-- reg_form_4 (page 3 / everyone) -->
						<table>
							<tr>
								<td colspan="2" bgcolor="#CCCCCC">Organisation details</tD>
							</tr>
							<tr>
								<td>Organisation name (please give in full)</td>
								<td valign="top"><input type="text" size="60" name="organisation" value="'.$fields['organisation'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<!-- <tr>
								<td>Organisation Type</td>
								<td valign="top"><table cellpadding="0" cellspacing="0" align="left" style="margin:0px;"><tr><td><input type="radio" name="usertype" id="NotProfit" value="non-profit" /></td><td style="padding-top:5px;">Not for Profit</td></tr><tr><td><input type="radio" name="usertype" id="Profit" value="for-profit" /></td><td style="padding-top:5px;">For Profit</td></tr></table></td>
							</tr> -->
							<tr> 
								<td valign="top"><a name="work address">Work Address</a></td>
								<td> 
									<input name="addr1" maxlength="200"  size=30 value="'.$fields['addr1'].'" style="padding:0px; width:auto; font-size:1.2em;"><br>
									<input name="addr2" maxlength="200"  size=30 value="'.$fields['addr2'].'" style="padding:0px; width:auto; font-size:1.2em;"><br>
									<input name="addr3" maxlength="200"  size=30 value="'.$fields['addr3'].'" style="padding:0px; width:auto; font-size:1.2em;"><br>
									<input name="addr4" maxlength="200"  size=30 value="'.$fields['addr4'].'" style="padding:0px; width:auto; font-size:1.2em;">
								</td>
							</tr>
							<tr> 
								<td valign="top"><a name="city">City</a></td>
								<td><input name="TownOrCity" maxlength="200"  size=30 value="'.$fields['TownOrCity'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<tr> 
								<td valign="top"><a name="state/county">State/Province/County</a></td>
								<td><input name="StateOrCounty" maxlength="200"  size=30 value="'.$fields['StateOrCounty'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<tr> 
								<td valign="top"><a name="postcode/zip">Zip/Postal Code</a></td>
								<td><input name="PostalCode" maxlength="200"  size=15 value="'.$fields['PostalCode'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<tr> 
								<td valign="top"><a name="country">Country</a></td>
								<td> 
									<select name="sel_country" id="sel_country" style="padding:0px; width:auto; font-size:1.2em;">
									'.$selcountry.'
									</select>
								</td>
							</tr>
							<tr><td valign="top">&nbsp;</td><td>&nbsp;</td></tr>
							<tr> 
								<td valign="top"><a name="phone">Telephone Number<br>(Inc. International/Area code)</a></td>
								<td valign="top"><input name="phone" maxlength="200"  size=30 value="'.$fields['phone'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr>
							<!-- <tr>
								<td>Main workplace</td>
								<td>
									<select name="main_workplace" id="main_workplace" style="padding:0px; width:auto; font-size:1.2em;">
									'.$selmainwplace.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Details (if other)</td>
								<td><input type="text" size="40" name="workplace_details" value="'.$fields['workplace_details'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr> -->
							<tr>
								<td>Current post/position</td>
								<td>
									<select name="current_post" id="current_post" style="padding:0px; width:auto; font-size:1.2em;">
									'.$selcurrentpost.'
									</select>
								</td>
							</tr>
							<!-- <tr>
								<td>Details (if other)</td>
								<td><input type="text" size="40" name="current_post_details" value="'.$fields['current_post_details'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
							</tr> -->
						</table>';

	$form5			=	'<!-- reg_form_5 (page-4 / non-students only) -->
						<table>
							<tr>
								<td colspan="2" bgcolor="#CCCCCC">Demographic details</td>
							</tr>
							<tr>
								<td>Years in current post</td>
								<td valign="top">
									<select name="years_in_post" id="years_in_post" style="padding:0px; width:auto; font-size:1.2em;">
									'.$selyearinpost.'
									</select>			
								</td>
							</tr>
							<tr>
								<td>Time spent in direct patient care</td>
								<td valign="top">
									<select name="time_spent" id="time_spent" style="padding:0px; width:auto; font-size:1.2em;">
									'.$seltimespent.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Gender</td>
								<td>
									<select name="gender" id="gender" style="padding:0px; width:auto; font-size:1.2em;">
									'.$selgender.'
									</select>
								</td>
							</tr>
							<tr>
								<td>Year of birth</td>
								<td>
									<select name="year_of_birth" id="year_of_birth" style="padding:0px; width:auto; font-size:1.2em;">
									'.$selyofbirth.'
									</select>
								</td>
							</tr>
						</table>';
	
	$form6			=	'<!-- reg_form_6 (page 5 / everyone) -->
						<table>
						<tr>
								<td colspan="2" bgcolor="#CCCCCC">Account preferences</td>
						</tr>
						<tr>
							<td>Accept e-mails relating to site updates</td>
							<td>
								<select name="accept_update_emails" id="accept_update_emails" style="padding:0px; width:auto; font-size:1.2em;">
								'.$selacceptemail.'
								</select>
							</td>
						</tr>
						<tr>
							<td>Accept bulletin board digest e-mails</td>
							<td>
								<select name="accept_bb_emails" id="accept_bb_emails" style="padding:0px; width:auto; font-size:1.2em;">
								'.$selbbemail.'
								</select>
							</td>
						</tr>
						<tr>
							<td>How did you hear about us</td>
							<td>
								<select name="lead" id="lead" style="padding:0px; width:auto; font-size:1.2em;">
								'.$sellead.'
								</select>
							</td>
						</tr>
						<tr>
							<td>Details (if other)</td>
							<td><input type="text" name="lead_details" size="30" value="'.$fields['lead_details'].'" style="padding:0px; width:auto; font-size:1.2em;"></td>
						</tr>
						</table>';







    // Registration form footer content =====================================================================

	$form_footer	=	'<div style="margin-left:15px;">
						<input type="submit" id="submit_button" value="'.$button_label.'" style="padding:0px; width:auto; font-size:1.2em; "></form>
						<script type="text/javascript">
						//link_enable();
						</script>
						</div>';
	
	$formname	= 'form'.$pagenum;
	$form_page	= $$formname;	
	$form		= '<div id="regform">'.$form_header . $form_page . $form_footer.'</div>';
	echo $form;
//}
?>