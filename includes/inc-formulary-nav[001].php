<div id="sidemenu"> 
<?php
if(!empty($_REQUEST['set_lang'])){
         $_SESSION['site_language']=$_REQUEST['set_lang']; 
}else if(empty($_SESSION['site_language'])){
        $_SESSION['site_language']='en';
}

if($_SESSION['site_language']=='en'){
    $enclass   =  ' selected';
}else{
    $declass  =  ' selected';
}
if($_GET['page']=='palliative-care-formulary'){

?>
<div class="languages">
	<form name="frmlang" id="frmlang" method="post" action="">
		<input type="hidden" name="set_lang" id="set_lang" value="" />
		 <ul>        
			   <li class='en<?php echo $enclass;?>'><a href="/en/palliative-care-formulary.html">English</a></li>
			   <li class='de<?php echo $declass;?>'><a href="/de/palliative-care-formulary.html">German</a></li>
		</ul>
	</form>
</div>
<div class='intro-rpp' style="clear: both">

<p>Login NOW on <a href="http://www.medicinescomplete.com/">www.MedicinesComplete.com</a> for the latest version of this formulary. You&rsquo;ll need to reset your password the first time you login. From 1 February 2019 the formulary will no longer be available on this page.</p>

<p>For login instructions via <a href="http://medicinescomplete.com/">MedicinesComplete.com</a> please <a href="/how-to-login.html">click here</a> or <a href="https://www.youtube.com/watch?v=bh94KV_DiT0">watch our video</a></p>
</div>
<?php

}else{
   if($_SESSION['site_language']=='en')
      $change_lang = 'de';
  else
     $change_lang = 'en';
?>
<form name="frmlang" id="frmlang" method="post" action="/palliative-care-formulary.html">
	<input type="hidden" name="set_lang" id="set_lang" value="" />
	<ul>        
       <li><a href="javascript:assign_language('<?php echo $change_lang;?>');">Change Language</a></li>     
	</ul>
</form>
<?php }?>

<?php
if($_SESSION['site_language']=='en'){
$navCnt						= 1;
$navCntApp					= 1;
$drug_monographs	        = formularyNav("0","Drug Monographs",'1');
$general_topics						= formularyNav("0","General Topics",'1');
$routes_of_admins					= formularyNav("0","Routes of Administration",'1');
$appendicies							= formularyNav("0","Appendicies",'1');
$prelims									= prelimsNav("0","Prelims",'1');
$qpds                     = qpdNav('1');
?>
<?php if($prelims!=''){ ?>
	<h2>Prelims</h2>	
<?php echo $prelims;	
  } ?>
	
   <?php if($drug_monographs!=''){ ?>
   <h2>Part 1 Drug Monographs</h2>&nbsp;(<a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')"><span class="sml_link">Expand All</span></a> | <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')"><span class="sml_link">Collapse All</span></a>)
	  <ul id="treemenu1" class="treeview"><?php echo $drug_monographs?></ul>
   <?php }if(!empty($general_topics)){ ?>
		<div class="prelim_nav"><h2>Part 2 General Topics</h2> 
			<ul class="treeview">
			<?php echo $general_topics;	?>
			</ul>	
		</div>
	 <?php }if(!empty($routes_of_admins)){ ?>
		<div class="prelim_nav"><h2>Part 3 Routes of Administration</h2> 
			<ul class="treeview">
			<?php echo $routes_of_admins;	?>
			</ul>	
		</div>
  <?php }if(!empty($appendicies)){ ?>
  <div class="prelim_nav"> <h2>Appendices</h2> 
	<ul class="treeview">
<?php echo $appendicies;	?>
</ul>	</div>
  <?php } if($qpds!=''){ ?>
   <div class="prelim_nav"><h2>Quick Clinical Guides</h2> 
	<ul class="treeview">
<?php //echo $qpds;	
	echo str_replace('Quick Practice Guide: ', '', $qpds);?>

  <!-- li><a href="/formulary/en/management-of-death-rattle.html">Management of death rattle (noisy respiratory secretions)</a></li>
  <li><a href="/formulary/en/opioid-induced-constipation.html">Opioid-induced constipation</a></li>
  <li><a href="/formulary/en/bowel-management-in-paraplegia-and-tetraplegia.html">Bowel management in paraplegia and tetraplegia</a></li>
  <li><a href="/formulary/en/quick-practice-guide-depression.html">Depression</a></li>
  <li><a href="/formulary/en/psychostimulants-in-depressed-patients-with-a-short-prognosis.html">Psychostimulants in depressed patients with a short prognosis</a></li>
  <li><a href="/formulary/en/management-of-nausea-and-vomiting.html">Management of nausea and vomiting</a></li>
  <li><a href="/formulary/en/management-of-procedure-related-pain.html">Management of procedure-related pain</a></li>
  <li><a href="/formulary/en/use-of-transdermal-buprenorphine.html">Use of transdermal buprenorphine</a></li>
  <li><a href="/formulary/en/use-of-transdermal-fentanyl-patches.html">Use of transdermal fentanyl patches</a></li>
  <li><a href="/formulary/en/use-of-methadone-for-cancer-pain.html">Use of methadone for cancer pain</a></li>
  <li><a href="/formulary/en/cellulitis-in-lymphoedema.html">Cellulitis in Lymphoedema</a></li>-->
  <li><a href="/formulary/en/continuous-subcutaneous-drug-infusions.html#McKinley">Setting up a McKinley T34 syringe pump for CSCI</a></li> 
  <!-- <li><a href="/formulary/en/continuous-subcutaneous-drug-infusions.html#Graseby">Setting up a Graseby MS16A or MS26 syringe driver for CSCI</a></li> -->
  <li><a href="/formulary/en/drug-administration-to-patients-with-swallowing-difficulties-or-enteral-feeding-tubes.html#QPDfeedingtube">Administration of drugs by enteral feeding tube </a></li>


    
</ul>	</div>
  <?php } ?>

<?php } else if($_SESSION['site_language']=='de'){
$navCnt						= 1;
$navCntApp					= 1;
$drug_monographs	        = formularyNav("0","Drug Monographs",'2');
$general_topics				= formularyNav("0","General Topics",'2');
$appendicies		        = formularyNav("0","Appendicies",'2');
$prelims		            = prelimsNav("0","Prelims",'2');
$qpds                       = qpdNav('2');

?>
<?php if($prelims!=''){ ?>
	<h2>Einführung</h2>	
<ul class="treeview">
<?php echo $prelims;	
  } ?>
</ul>
	
   <?php if($drug_monographs!=''){ ?>
   <h2>Teil 1: Arzneimittelmonographien</h2>&nbsp;(<a href="javascript:ddtreemenu.flatten('treemenu1', 'expand')"><span class="sml_link">Expand All</span></a> | <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')"><span class="sml_link">Collapse All</span></a>)
	  <ul id="treemenu1" class="treeview"><?php echo $drug_monographs?></ul>
   <?php }if(!empty($general_topics)){ ?>
   <h2>Teil 2: Allgemeine Themen</h2> 
	<ul class="treeview">
<?php echo $general_topics;	?>	
</ul>
  <?php }if(!empty($appendicies)){ ?>
   <h2>Anhang</h2> 
	<ul class="treeview">
<?php echo $appendicies;	?>
</ul>	
  <?php } if($qpds!=''){ ?>
   <h2>Schnelle Praxisanleitung</h2> 
	<ul class="treeview">
<?php //echo $qpds;
	echo str_replace('Quick Practice Guide: ', '', $qpds);	?>	
</ul>
  <?php } ?>

  

<?php } ?>
	
<script type="text/javascript">
	ddtreemenu.createTree("treemenu1", true)
</script>
</div>