<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>&copy; palliativedrugs.com</title>
<!--<base href="http://www.palliativedrugs.com"	/>-->
<link rel="stylesheet" type="text/css" href="/min/g=headercss"/>
<link rel="stylesheet" type="text/css" href="/min/g=shopheadercss"/>
<!--This script should appear below your LINK stylesheet tags -->
<!-- script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script-->
<script language="javascript" type="text/javascript" src="/min/g=articleheader"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8576276-1']);
  _gaq.push(['_setDomainName', 'palliativedrugs.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>