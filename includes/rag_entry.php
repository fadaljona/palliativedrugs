<?php include($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/configs/config_include.php'); 
require_once($_SERVER['DOCUMENT_ROOT'] . '/siteconfig/functions/rag_func.inc.php');

$colname_rsRagEntry		=	"1";

if (isset($_GET['entryid'])) {
  $colname_rsRagEntry	=	(get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}

$query_rsRagEntry		=	sprintf("SELECT rag_entry.id, section_id, rag_entry.title, rag_entry.notes, rag_section.title as sec_title FROM rag_entry, rag_section WHERE rag_entry.id = %s and rag_entry.section_id = rag_section.id", $colname_rsRagEntry);

$rsRagEntry				=	open_SQL($query_rsRagEntry);
$row_rsRagEntry			=	mysql_fetch_assoc($rsRagEntry);
$totalRows_rsRagEntry	=	mysql_num_rows($rsRagEntry);

$colname_rsRagDocs		=	"1";

if (isset($_GET['entryid'])) {
  $colname_rsRagDocs	=	(get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}

$query_rsRagDocs		=	sprintf("SELECT id, title, organisation, contact, file_location, notes, DateAdded FROM rag_doc WHERE entry_id = %s and active =1 and audit=0 order by DateAdded desc", $colname_rsRagDocs);
$rsRagDocs				=	open_SQL($query_rsRagDocs);
$row_rsRagDocs			=	mysql_fetch_assoc($rsRagDocs);
$totalRows_rsRagDocs	=	mysql_num_rows($rsRagDocs);

$colname_rsRagSurveys	=	"1";

if (isset($_GET['entryid'])) {
  $colname_rsRagSurveys =	(get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}


$query_rsRagSurveys		=	sprintf("SELECT rag_survey.id, rag_survey.monograph, survsurvey.title FROM rag_survey, survsurvey WHERE rag_entry_id = %s and rag_survey.survey_id = survsurvey.id", $colname_rsRagSurveys);
$rsRagSurveys			=	open_SQL($query_rsRagSurveys);
$row_rsRagSurveys		=	mysql_fetch_assoc($rsRagSurveys);
$totalRows_rsRagSurveys =	mysql_num_rows($rsRagSurveys);

$colname_rsAuditDocs	=	"1";
if (isset($_GET['entryid'])) {
  $colname_rsAuditDocs	=	(get_magic_quotes_gpc()) ? $_GET['entryid'] : addslashes($_GET['entryid']);
}

$query_rsAuditDocs		=	sprintf("SELECT id, title, organisation, contact, file_location, DateAdded, notes FROM rag_doc WHERE entry_id = %s and active =1 and audit=1 order by DateAdded desc", $colname_rsAuditDocs);
$rsAuditDocs			=	open_SQL($query_rsAuditDocs);
$row_rsAuditDocs		=	mysql_fetch_assoc($rsAuditDocs);
$totalRows_rsAuditDocs	=	mysql_num_rows($rsAuditDocs);
?>
<?php
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
// require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/inc-secure_framed.php');
?>
<html>
<head>
<title>RAG Entry</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/legacy.css" rel="stylesheet" type="text/css">
</head>
<body>

<p>Topic&nbsp;&nbsp;<strong><?php echo $row_rsRagEntry['sec_title']; ?></strong></p>
<p>Content&nbsp;&nbsp;<strong><?php echo $row_rsRagEntry['title']; ?></strong></p>
<p><?php echo $row_rsRagEntry['notes']; ?> </p>
<hr align="center" width="90%" size="1" noshade>
<? if($totalRows_rsRagDocs > 0): ?>
<p>Documents (click 'Title' to view)</p>
<table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
  <tr> 
    <th>Title</th>
    <th>Organisation</th>
    <th>Donated by</th>
    <th>Year added</th>
    <th>Notes</th>
  </tr>
  <?php do { ?>
  <tr bgcolor="#FFFFFF"> 
    <td><a href="<?php echo $row_rsRagDocs['file_location']; ?>" target="_blank"><?php echo $row_rsRagDocs['title']; ?></a></td>
    <td><?php echo $row_rsRagDocs['organisation']; ?></td>
    <td><?php echo $row_rsRagDocs['contact']; ?></td>
    <td align="center"><?php echo substr($row_rsRagDocs['DateAdded'],0,4); ?></td>
    <td><?php echo $row_rsRagDocs['notes']; ?></td>
  </tr>
  <?php } while ($row_rsRagDocs = mysql_fetch_assoc($rsRagDocs)); ?>
</table>
<br>
<? endif ?>
<? if($totalRows_rsAuditDocs > 0): ?>
<p>Audits (click 'Title' to view)</p>
<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
  <tr>
    <th>Title</th>
    <th>Organisation</th>
    <th>Contact</th>
    <th>Year Added</th>
    <th>Notes</th>
  </tr>
  <?php do { ?>
  <tr>
  	<td bgcolor="#FFFFFF"><a href="<?php echo $row_rsAuditDocs['file_location']; ?>" target="_blank"><?php echo $row_rsAuditDocs['title']; ?></a></td>
    <td bgcolor="#FFFFFF"><?php echo $row_rsAuditDocs['organisation']; ?></td>
    <td bgcolor="#FFFFFF"><?php echo $row_rsAuditDocs['contact']; ?></td>
    <td align="center" bgcolor="#FFFFFF"><?php echo substr($row_rsAuditDocs['DateAdded'],0,4); ?></td>
    <td align="center" bgcolor="#FFFFFF"><?php echo $row_rsAuditDocs['notes']; ?></td>
  </tr>
  <?php } while ($row_rsAuditDocs = mysql_fetch_assoc($rsAuditDocs)); ?>
</table>
<? endif ?>
<? if($totalRows_rsRagSurveys > 0): ?>
<br>
<p>Surveys (click on 'Take' to complete a survey and 'Results' to view results)</p>
<table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#CCCCCC">
  <tr> 
    <th>Title</th>
    <th>Linked monograph</th>
    <th>Actions</th>
  </tr>
  <?php do { ?>
  <tr bgcolor="#FFFFFF"> 
    <td><?php echo $row_rsRagSurveys['title']; ?></td>
    <td><a href="/book.php?<?php echo $row_rsRagSurveys['monograph']; ?>"><?php echo $row_rsRagSurveys['monograph']; ?></td>
    <td width="84">
		<? if($row_rsRagSurveys['id']) { ?>
		<a href="/survey/survey.php?sid=<?php echo dLookup("survey_id","rag_survey","id=".$row_rsRagSurveys['id']); ?>">Take</a>
		|
		<a href="/survey/survey_results.php?sid=<?php echo dLookup("survey_id","rag_survey","id=".$row_rsRagSurveys['id']);?>">Results</a>
		<? } ?>
	</td>
  </tr>
  <?php } while ($row_rsRagSurveys = mysql_fetch_assoc($rsRagSurveys)); ?>
</table>
<br>
<? endif ?>
<br>
To donate guidelines, treatment protocols, patient information leaflets or audit documentation (e.g. proformas, results), please 
send them as Word or PDF documents to <a href="mailto:hq@palliativedrugs.com">hq@palliativedrugs.com</a>.<br>
<br>
<a href="rag_main.php">Click here to return to Document library home page.</a><br>
</body>
</html>
<?php
mysql_free_result($rsRagEntry);

mysql_free_result($rsRagDocs);

mysql_free_result($rsRagSurveys);

mysql_free_result($rsAuditDocs);
?>

