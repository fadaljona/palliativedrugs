<?php
if($_SESSION['visitor']->logged_in) {
	if($_SESSION['visitor']->acl=='user') {	
          $sql         =    "SELECT title, FirstName,LastName,organisation,addr1,addr2,addr3,addr4,TownOrCity,StateOrCounty,PostalCode,Country FROM new_users WHERE id='" . $_SESSION['visitor']->id . "'";
			$recs       =     open_SQL($sql);
			if(mysqli_num_rows($recs))
			{
				$row		=	mysqli_fetch_array($recs);
				$utitle		=	stripslashes($row['title']);
				$ufname		=	stripslashes($row['FirstName']);
				$ulname		=	stripslashes($row['LastName']);
				$uorg		=	stripslashes($row['organisation']);
				$uaddr1		=	stripslashes($row['addr1']);
				$uaddr2		=	stripslashes($row['addr2']);
				$uaddr3		=	stripslashes($row['addr3']);
				$uaddr4		=	stripslashes($row['addr4']);
				$utown		=	stripslashes($row['TownOrCity']);
				$ustate		=	stripslashes($row['StateOrCounty']);
				$upostalcode=	stripslashes($row['PostalCode']);
				$ucountry	=	stripslashes($row['Country']);
			 }  
?>
<style>


.aapple_overlay{position:absolute !important;}
.popup_contain .popup label, .popup_contain .popup p  {
	font-size: 14px;
	color: black;
}
.popup_contain .popup h1 {
	font-size: 18px;
}
.popup_contain .popup input, .popup_contain .popup select {
	line-height: 15px; font-size: 14px;
}
.popup_contain .popup label {
	color: black;
}
</style>
<div class="apple_overlay popup_contain" id="custom">
	<div class="popup">
		<div id="uDetails" style="height:450px;overflow-y: scroll;padding:10px;">
			<h1>
				Update your password to access Palliativedrugs.com
			</h1>
			<form name="frmType" id="frmType" method="post">
				<input type='hidden' name='userid' id='userid' value="&lt;?=$_SESSION['visitor']-&gt;id?&gt;">
				<fieldset>
					<div class="block">
						<p class="error">
							Please enter valid password
						</p>
					</div>
					<p>
						<strong> Update Password </strong>
					</p>

<!-- 			<p>Enter a new password. Password should be at least 8 characters in length, and include at least one numeric and one special character, such as: !@#$%^&*-</p> -->
					<p>Please enter your new password below. The password should be:</p>
					<ul style="margin-top: 0;">
						<li><p style="padding: 0;">at least 8 characters in length</p></li>
						<li><p style="padding: 0;">Include at least one numeric character e.g.123</p></li>
						<li><p style="padding: 0;">Include one special character e.g. !@#$%^</p></li>
					</ul>
					<label for="password"> New password </label> 
					<input type="password" name="password" id="password" value="" />
					<label for="cpassword"> Confirm new password </label> 
					<input type="password" name="cpassword" id="cpassword" value="" />
					<p>Please email <a href="mailto:pharmpress-support@rpharms.com">pharmpress-support@rpharms.com</a> if you encounter any difficulties with the password reset process.</p>
					<input type="button" value="Save" id="update-prefs" name="submit" onclick="return validate_update();" style="margin:20px; width: 460px; " />
				</fieldset>
			</form>
		</div>
	</div>
</div>
<?php }
}
if($_SESSION['visitor']->acl=='user') {	
?>
<script type='text/javascript'>



$(function() {
		var api2   =   $("#custom").overlay({mask: '#000', effect: 'apple',load: true, height:200, close: 'closebtn',closeOnClick: false, closeOnEsc: false});
		$("#custom").css("position","top");

});

function showUserInfo(){
	$('#uAgree').hide();
	$('#uDetails').show();
}
</script>
<?php
}
?>
