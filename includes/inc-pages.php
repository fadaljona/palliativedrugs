<?php $doc_root	=	$_SERVER['DOCUMENT_ROOT'];
include("$doc_root/siteconfig/configs/config_include.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include ("$doc_root/includes/inc-header.php")?>
</head>
<body>
<?php include ("$doc_root/includes/inc-error-login.php")?>
<?php include ("$doc_root/includes/inc-user-update-expose.php")?>
<div id="container">
  <?php include ("$doc_root/includes/inc-topnav.php")?>
  <?php include ("$doc_root/includes/inc-nav.php")?>
  <?php include ("$doc_root/includes/inc-exposewindow.php")?>
  <div id="content">
    <div id="col01">
      <div class="tr">
        <div class="bl">
          <div class="br">
            <?php include ("$doc_root/includes/inc-loginform.php")?>
<a href="https://twitter.com/palliativedrugs" class="twitter-follow-button" data-show-count="false" data-size="large"  data-lang="en">Follow @palliativedrugs</a>
            <?php include ("$doc_root/includes/inc-newsroller.php")?>
            <?php include ("$doc_root/includes/inc-germanlink.php")?>
            <?php include ("$doc_root/includes/inc-honcode.php")?>
          </div>
        </div>
      </div>
    </div>
    <div id="col02">
      <div class="bg_right">
        <div class="tr">
          <div class="bl">
            <div class="br">
              <div id="col02_1">

                <div class="pressnews">
                  <h1>Press Release</h1>
                  <h2>Royal Pharmaceutical Society acquires Palliativedrugs.com</h2>
                  <h3>9th August 2018</h3>
                  <p>We are delighted to inform you of the transfer of palliativedrugs.com Ltd. to the ownership of Pharmaceutical Press.</p><a href="/press-news/august/royal-pharmaceutical-society-acquires-palliativedrugs-com.html"> Read more</a> </div>

                <h1><strong>Welcome to the website</strong></h1>
                <p>Palliativedrugs.com provides essential independent information for health professionals about drugs used in palliative and hospice care. It includes unauthorized indications and routes, and details about the administration of multiple drugs by continuous subcutaneous infusion. Please see our <a href="/testimonials.html#pcf4">example sections and testimonials</a>.</p>
<p>Join over 30,000 members from 169 Countries. Please register to obtain free access to most areas of the site, i.e. Latest additions, News, Bulletin board (for both drug and non-drug issues), syringe driver survey drug compatibility database (SDSD) and Document library. There is also a search facility for the whole site.</p>
<p>A subscription is required to access the on-line Palliative Care Formulary (PCF). <em>The on-line version is regularly updated and thus provides access to the most up to date PCF content</em>. To subscribe: first register, then log on and go to the <a href="/shop/books.html">Store</a>. Before use, we recommend that you read <a href="/formulary/en/getting-the-most-out-of-pcf.html">Getting the most out of PCF</a>.  We are making some important changes to the way you access the current online Palliative Care Formulary. If you are an online formulary subscriber you will shortly receive details about how to access your updated formulary account on MedicinesComplete in January 2019. Only access to the Palliative Care Formulary will be moving over to the MedicinesComplete platform.  The Bulletin board, Document Library, News, Surveys and the Syringe Driver Survey Database (SDSD) will continue to be available at www.palliativedrugs.com. The site will also contain a link to the Palliative Care Formulary on the MedicinesComplete platform. Once you have received your updated login instructions you will need to complete migration to your updated account on MedicinesComplete by 1 February 2019 when access via the palliativedrugs.com site will cease.</p>
<p>If you have any questions regarding the migration then please contact hq@palliativedrugs.com<span style="font-size: 1.2em;"> - </span><span style="font-size: small;">MedicinesComplete Team</span></p>
<p>With cumulative global sales of over 100,000, PCF is the established resource in its field and receives worldwide acclaim. Territorial specific editions of PCF are available in Canada, Germany, Japan and the USA.</p>
<p>A German language on-line PCF is available. <a href="/palliative-care-formulary.html">Hier für Zugang klicken</a>.</p>
<p> </p>

              </div>
              <div id="col02_2">
                <?php //  include ("$doc_root/includes/inc-hpsurvey.php")?>
<h2>Calling all members</h2>
<p>Please complete our short survey:<br />
  <br />
  <a href="survey.html">Prevention of skeletal-related events (SRE) in adults with bone metastases from solid tumours - what do you use?</a> </p>
<!--<p>Previous survey results<br />
  <a href="/latest.html#survey">click here</a></p>--> 

                <div class="latestnews">
                  <h1>Latest Additions</h1>
                  <ul>
                    
                    <li><small>12th December 2018</small><br />
                      Donated management of seizure guidelines <a href="/latest/december/donated-management-of-seizure-guidelines.html"> Read more</a></li>
                    
                    <li><small>31st October 2018</small><br />
                      On-line PCF updated monographs summary (October 2018) <a href="/latest/october/on-line-pcf-updated-monographs-summary-october-2018.html"> Read more</a></li>
                    
                    <li><small>24th October 2018</small><br />
                      PCF H2-receptor antagonists monograph <a href="/latest/october/pcf-h2-receptor-antagonists-monograph.html"> Read more</a></li>
                    
                    <li><small>24th October 2018</small><br />
                      PCF Proton Pump Inhibitors monograph <a href="/latest/october/pcf-proton-pump-inhibitors-monograph.html"> Read more</a></li>
                    
                  </ul>
                </div>
                <div class="latestnews">
                  <h1>News</h1>
                  <ul>
                    
                    <li><small>19th December 2018</small><br />
                      RPS Wales Palliative and End of Life Care Policy <a href="/news/december/rps-wales-palliative-and-end-of-life-care-policy.html">Read more</a></li>
                    
                    <li><small>14th December 2018</small><br />
                      NICE guidelines for COPD updated <a href="/news/december/nice-guidelines-for-copd-updated.html">Read more</a></li>
                    
                    <li><small>14th December 2018</small><br />
                      AWMSG approves tiotropium <a href="/news/december/awmsg-approves-tiotropium.html">Read more</a></li>
                    
                    <li><small>11th December 2018</small><br />
                      Palliative Care Formulary migration to MedicinesComplete - January 2019 <a href="/news/december/palliative-care-formulary-migration-to-medicinescomplete-january-2019.html">Read more</a></li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include ("$doc_root/includes/inc-footer.php")?>
</div>
<?php include ("$doc_root/includes/inc-analytics.php")?>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</body>
</html>