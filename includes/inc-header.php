<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>&copy; palliativedrugs.com</title>
<link rel="stylesheet" type="text/css" href="/min/g=headercss"/>
<!--This script should appear below your LINK stylesheet tags -->
<script language="JavaScript" type="text/javascript" src="/min/g=headerjs "></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8576276-1']);
  _gaq.push(['_setDomainName', 'palliativedrugs.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#252e39"
    },
    "button": {
      "background": "#14a7d0"
    }
  },
  "position": "bottom-right",
  "content": {
    "message": "This website uses cookies\nCookies remember you so we can grant you access to our online services. By using this website or closing this message, you are agreeing to our",
    "dismiss": "Continue",
    "link": " Cookies notice.",
    "href": "https://www.palliativedrugs.com/privacy.html"
  }
})});
</script>