<?php
if(!IsSet($_SESSION['visitor']->id)) {
	if(!IsSet($_SESSION['new_user_id'])) {
		header('Location:  /access-denied.html');
	} else {
		$user_id = $_SESSION['new_user_id'];
	}
} else {
	$user_id = $_SESSION['visitor']->id;
}

if($_SESSION['reg_confirm']=='true') $reg_confirm = true;

if($_SERVER['REQUEST_METHOD']=='GET') {
	// populate fields from database...
	$sql			=	"SELECT * FROM new_users WHERE id='".$user_id."'";
	$rs				=	open_SQL($sql);
	$row			=	mysql_fetch_assoc($rs);
	$email			=	$row['email'];
	$pswd			=	$row['pwd'];
	$firstname		=	$row['FirstName'];
	$lastname		=	$row['LastName'];
	$title			=	$row['title'];
	$role			=	trim($row['role']);

	if(in_array($role,$config['rolearr'])) {
		$role_details=	'';
	} else {
		$role		=	'other';
		$role_details= $row['role'];
	}

	$prescriber		= $row['nurse_prescriber'];

	if($prescriber==1) { 
		$prescriber =	'yes'; 
	} else { 
		$prescriber =	'no'; 
	}

	$prof_reg_num	=	$row['prof_reg_num'];
	$specialty		=	$row['speciality'];
	
	if(in_array($specialty,$config['specarr'])) {
		$specialty_details	= '';
	} else {
		$specialty	= 'other';
		$specialty_details	= $row['speciality'];
	}		

	$sub_specialty	=	$row['sub_specialty'];

	if(in_array($sub_specialty,$config['subarr'])) {
		$sub_specialty_details	= '';
	} else {
		$sub_specialty = 'other';
		$sub_specialty_details	= $row['sub_specialty'];
	}	

	$organisation	=	$row['organisation'];
	$addr1			=	$row['addr1'];
	$addr2			=	$row['addr2'];
	$addr3			=	$row['addr3'];
	$addr4			=	$row['addr4'];
	$city			=	$row['TownOrCity'];
	$state			=	$row['StateOrCounty'];
	$zip			=	$row['PostalCode'];
	$country		=	$row['Country'];
	$phone			=	$row['phone'];
	/*$main_workplace	=	$row['main_workplace'];

	if(in_array($main_workplace,$config['mainarr'])) {
		$main_workplace_details	= '';
	} else {
		$main_workplace = 'Other';
		$main_workplace_details	= $row['main_workplace'];
	}*/

	$current_post	=	$row['current_post'];

	if(in_array($current_post,$config['postarr'])) {
		$current_post_details	= '';
	} else {
		$current_post = 'Other';
		$current_post_details	= $row['current_post'];
	}

	$years_in_post	=	$row['years_in_post'];	
	$time_spent		=	$row['patient_care_time'];
	$gender			=	$row['gender'];
	$year_of_birth	=	$row['year_of_birth'];
	$accept_update_emails	= $row['accept_site_emails'];

	if($accept_update_emails==1) { 
		$accept_update_emails = 'yes'; 
	} else { 
		$accept_update_emails = 'no'; 
	}

	$accept_bb_emails=	$row['accept_bb_emails'];

	if($accept_bb_emails==1) { 
		$accept_bb_emails = 'yes'; 
	} else { 
		$accept_bb_emails = 'no'; 
	}
	
	//setup bools for optional content...
	$is_nurse		=	FALSE;
	$is_student		=	FALSE;

	switch($role) {
		case 'Nurse': 
			$is_nurse=TRUE; 
			break;
		case 'Student doctor':
		case 'Student nurse':
		case 'Student pharmacist':
		case 'Student other':
			$is_student = TRUE;
			break;
	}	
} else if(isset($_POST['email'])) {
	// it's a POST so error_check & save...
	$prescriber		=	($_POST['prescriber']=='yes') ? '1' : '0';
	$site_emails	=	($_POST['accept_site_emails']=='yes')?'1':'0';
	$bb_emails		=	($_POST['accept_bb_emails']=='yes')?'1':'0';	
	
	if($_POST['role']=='other') {
		$role		=	$_POST['role_details'];
	} else {
		$role		=	$_POST['role'];
	}

	if($_POST['primary']=='other') {
		$speciality =	$_POST['specialty_details'];
	} else {
		$speciality =	$_POST['primary'];
	}
	
	if($_POST['sub_specialty']=='other') {
		$sub_speciality = $_POST['sub_specialty_details'];
	} else {
		$sub_speciality = $_POST['sub_specialty'];
	}

	if($_POST['main_workplace']=='other') {
		$main_workplace = $_POST['main_workplace_details'];
	} else {
		$main_workplace = $_POST['main_workplace'];
	}

	if($_POST['current_post']=='other') {
		$current_post = $_POST['current_post_details'];
	} else {
		$current_post = $_POST['current_post'];
	}

	if($_POST['lead']=='other') {
		$lead	=	$_POST['lead_details'];
	} else {
		$lead	=	$_POST['lead'];
	}
	
	if(IsSet($_POST['years_in_post'])) {
		$years_in_post = $_POST['years_in_post'];
	} else {
		$years_in_post = 0;
	}

	if(strlen($_POST['pswd3'])) {
		$pwd_sql =	"pwd='" . $_POST['pswd3'] . "', ";
	} else {
		$pwd_sql =	"";
	}
	
	$sql = "UPDATE new_users SET "
					. "email			=		'".addslashes($_POST['email'])."', "
					. $pwd_sql
					. "FirstName		=		'".addslashes($_POST['firstname'])."', "
					. "LastName			=		'".addslashes($_POST['lastname'])."', "
					. "title			=		'".addslashes($_POST['title'])."', "
					. "role				=		'".addslashes($role)."', "
					. "prof_reg_num		=		'".addslashes($_POST['prof_reg_num'])."', "
					. "nurse_prescriber	=		'".addslashes($prescriber)."', "
					. "speciality		=		'".addslashes($speciality)."', "
					. "sub_specialty	=		'".addslashes($sub_speciality)."', "
					. "organisation		=		'".addslashes($_POST['organisation'])."', "
					. "addr1			=		'".addslashes($_POST['addr1'])."', "
					. "addr2			=		'".addslashes($_POST['addr2'])."', "
					. "addr3			=		'".addslashes($_POST['addr3'])."', "
					. "addr4			=		'".addslashes($_POST['addr4'])."', "
					. "TownOrCity		=		'".addslashes($_POST['city'])."', "
					. "StateOrCounty	=		'".addslashes($_POST['state'])."', "
					. "PostalCode		=		'".addslashes($_POST['zip'])."', "
					. "Country			=		'".addslashes($_POST['country'])."', "
					. "main_workplace	=		'".addslashes($main_workplace)."', "
					. "current_post		=		'".addslashes($current_post)."', "
					. "years_in_post	=		'".addslashes($years_in_post)."', "
					. "patient_care_time=		'".addslashes($_POST['time_spent'])."', "
					. "gender			=		'".addslashes($_POST['gender'])."', "
					. "year_of_birth	=		'".addslashes($_POST['year_of_birth'])."', "
					. "accept_site_emails=		'".addslashes($site_emails)."', "
					. "accept_bb_emails	=		'".addslashes($bb_emails)."', "
					. "UserType			=		'".addslashes($_POST['usertype'])."', "
					. "active			=		'1',"
					. "user_confirmed	=		'1' "
					. "WHERE id			=		'".addslashes($user_id)."'";
	open_SQL($sql);	
	if($reg_confirm) {		
		unset($_SESSION['reg_confirm']);
		// auto-login...
		$sql = "UPDATE new_users SET current_visit = FROM_UNIXTIME(" . strtotime('now') . ") WHERE id = '".$user_id."'";
		open_SQL($sql);
		$sql = "UPDATE new_users SET previous_visit = FROM_UNIXTIME(" . strtotime('now') . ") WHERE id = '".$user_id."'";
		open_SQL($sql);
		$_SESSION['visitor']->id	= $user_id;
		$_SESSION['visitor']->acl	= $_POST['usertype'];
		$_SESSION['visitor']->logged_in = TRUE;
		$_SESSION['previous_visit'] = date("Y-m-d");
		
		//send welcome email
		$sql	=	"SELECT * FROM new_users WHERE id	=	'".$user_id."'";
		$rs		=	open_SQL($sql);
		$row	=	mysql_fetch_assoc($rs);		
		//$chunkArr = array();
		$params['username']		= 'Username: ' . $row['username'];
		$params['email']		= 'Email: ' . $row['email'];
		$params['password']		= 'Password: ' . $row['pwd'];
		$params['firstname']	= $row['FirstName'];
		$params['lastname']		= $row['LastName'];
		$params['title']		= $row['title'];
		//$params['role']			= $row['role'];
		
		if(strpos($params['role'],'Student')===false) {
			$params['prof_reg_num'] = 'Professional registration number: ' . $row['prof_reg_num'];
			if($params['role']=='Nurse') {
				$params['nurse_prescriber'] = 'Nurse prescriber: ' . (($row['nurse_prescriber']==1) ? 'Yes' : 'No');
			} else {
				$params['nurse_prescriber'] = '';
			}
			$params['speciality']	= 'Specialty: ' . $row['speciality'];
			$params['sub_specialty']= 'Sub-specialty: ' . $row['sub_specialty'];
		} else {
			$params['prof_reg_num']		= '';
			$params['nurse_prescriber'] = '';
			$params['speciality']		= '';
			$params['sub_specialty']	= '';
		}
					
		$params['organisation']	= 'organisation= ' . $row['organisation'];
		$params['addr1']		= 'Address 1: ' . $row['addr1'];
		$params['addr2']		= 'Address 2: ' . $row['addr2'];
		$params['addr3']		= 'Address 3: ' . $row['addr3'];
		$params['addr4']		= 'Address 4: ' . $row['addr4'];
		$params['TownOrCity']	= 'Town/City: ' . $row['TownOrCity'];
		$params['StateOrCounty']= 'State/County: ' . $row['StateOrCounty'];
		$params['PostalCode']	= 'Postcod: ' . $row['PostalCode'];
		$params['Country']		= 'Country: ' . $row['Country'];
		$params['phone']		= 'Phone: ' . $row['phone'];
		$params['main_workplace']= 'Main workplace: ' . $row['main_workplace'];
		$params['current_post'] = 'Current post: ' . $row['current_post'];
		
		if(strpos($params['role'],'Student')===false) {
			$params['years_in_post'] = 'Years in post: ' . $row['years_in_post'];
			$params['patient_care_time'] = 'Patient care time: ' . $row['patient_care_time'];
			$params['gender'] = 'Gender: ' . (($row['gender']=='M') ? 'Male' : 'Female');
			$params['year_of_birth'] = 'Year of birth: ' . $row['year_of_birth'];
		} else {
			$params['years_in_post'] = '';
			$params['patient_care_time'] = '';
			$params['gender'] = '';
			$params['year_of_birth'] = '';
		}
		
		$params['lead']			= 'How did you hear about us: ' . $row['lead'];
		$params['accept_site_emails'] = 'Accept e-mails relating to site updates: ' . (($row['accept_site_emails']==1) ? 'Yes' : 'No');
		$params['accept_bb_emails'] = 'Accept bulletin board digest e-mails: ' . (($row['accept_bb_emails']==1) ? 'Yes' : 'No');

		$Seladmin		= 	"SELECT * FROM 3bit_messages WHERE message_type='Email Welcome' AND message_to = 'Customer' AND active='1'";
		$res			= 	open_SQL($Seladmin)or die(mysql_error());
		if(mysql_num_rows($res)>0){
			$row1			= 	mysql_fetch_assoc($res);			
			$body			= 	stripslashes($row1['message_text']);	
			$text			=	stripslashes($row1['text']);	
			$subject		=	stripslashes($row1['mail_subject']);
			//$msg = $modx->parseChunk('email_welcome', $params, '[+', '+]');		
			//$msg	=	nl2br(parseMessage("Email Welcome","Customer",$params,'{','}'));
			// masked password ===============================================================
			$password		=		$row['pwd'];
			$len				=		strlen($password);
			$pwd				=		substr($password,0, 1) . str_repeat('*', $len-2) . substr($password,-1);

			$bodytag		= 	str_replace("{title}",$row['title'], $body);
			$bodytag		= 	str_replace("{lname}",$row['LastName'], $bodytag);
			$bodytag		= 	str_replace("{lastname}",$row['LastName'], $bodytag);
			$bodytag		= 	str_replace("{username}",$row['username'], $bodytag);
			$bodytag		= 	str_replace("{password}",$pwd, $bodytag);
			$bodytag		= 	str_replace("{title}",$row['title'], $bodytag);
			$bodytag		= 	str_replace("{firstname}",$row['FirstName'], $bodytag);
			$bodytag		= 	str_replace("{role}",$row['role'], $bodytag);
			$bodytag		= 	str_replace("{prof_reg_num}",$row['prof_reg_num'], $bodytag);
			$bodytag		= 	str_replace("{nurse_prescriber}",$row['nurse_prescriber'], $bodytag);
			$bodytag		= 	str_replace("{speciality}",$row['speciality'], $bodytag);
			$bodytag		= 	str_replace("{sub_specialty}",$row['sub_specialty'], $bodytag);
			$bodytag		= 	str_replace("{organisation}",$row['organisation'], $bodytag);
			$bodytag		= 	str_replace("{addr1}",$row['addr1'], $bodytag);
			$bodytag		= 	str_replace("{addr2}",$row['addr2'], $bodytag);
			$bodytag		= 	str_replace("{addr3}",$row['addr3'], $bodytag);
			$bodytag		= 	str_replace("{addr4}",$row['addr4'], $bodytag);
			$bodytag		= 	str_replace("{TownOrCity}",$row['TownOrCity'], $bodytag);
			$bodytag		= 	str_replace("{StateOrCounty}",$row['StateOrCounty'], $bodytag);
			$bodytag		= 	str_replace("{PostalCode}",$row['PostalCode'], $bodytag);
			$bodytag		= 	str_replace("{Country}",$row['Country'], $bodytag);
			$bodytag		= 	str_replace("{phone}",$row['phone'], $bodytag);
			$bodytag		= 	str_replace("{main_workplace}",$row['main_workplace'], $bodytag);
			$bodytag		= 	str_replace("{current_post}",$row['current_post'], $bodytag);
			$bodytag		= 	str_replace("{years_in_post}",$row['years_in_post'], $bodytag);
			$bodytag		= 	str_replace("{patient_care_time}",$row['patient_care_time'], $bodytag);
			$bodytag		= 	str_replace("{gender}",(($row['gender']=='M') ? 'Male' : 'Female'), $bodytag);
			$bodytag		= 	str_replace("{year_of_birth}",$row['year_of_birth'], $bodytag);
			$bodytag		= 	str_replace("{lead}",$row['lead'], $bodytag);
			$bodytag		= 	str_replace("{accept_site_emails}",(($row['accept_site_emails']==1) ? 'Yes' : 'No'), $bodytag);
			$bodytag		= 	str_replace("{accept_bb_emails}",(($row['accept_bb_emails']==1) ? 'Yes' : 'No'), $bodytag);
			$bodytag		=	nl2br($bodytag);

			
			$texttag		= 	str_replace("{title}",$row['title'], $text);
			$texttag		= 	str_replace("{lname}",$row['LastName'], $texttag);
			$texttag		= 	str_replace("{lastname}",$row['LastName'], $texttag);
			$texttag		= 	str_replace("{username}",$row['username'], $texttag);
			$texttag		= 	str_replace("{password}",$pwd, $texttag);
			$texttag		= 	str_replace("{title}",$row['title'], $texttag);
			$texttag		= 	str_replace("{firstname}",$row['FirstName'], $texttag);
			$texttag		= 	str_replace("{role}",$row['role'], $texttag);
			$texttag		= 	str_replace("{prof_reg_num}",$row['prof_reg_num'], $texttag);
			$texttag		= 	str_replace("{nurse_prescriber}",$row['nurse_prescriber'], $texttag);
			$texttag		= 	str_replace("{speciality}",$row['speciality'], $texttag);
			$texttag		= 	str_replace("{sub_specialty}",$row['sub_specialty'], $texttag);
			$texttag		= 	str_replace("{organisation}",$row['organisation'], $texttag);
			$texttag		= 	str_replace("{addr1}",$row['addr1'], $texttag);
			$texttag		= 	str_replace("{addr2}",$row['addr2'], $texttag);
			$texttag		= 	str_replace("{addr3}",$row['addr3'], $texttag);
			$texttag		= 	str_replace("{addr4}",$row['addr4'], $texttag);
			$texttag		= 	str_replace("{TownOrCity}",$row['TownOrCity'], $texttag);
			$texttag		= 	str_replace("{StateOrCounty}",$row['StateOrCounty'], $texttag);
			$texttag		= 	str_replace("{PostalCode}",$row['PostalCode'], $texttag);
			$texttag		= 	str_replace("{Country}",$row['Country'], $texttag);
			$texttag		= 	str_replace("{phone}",$row['phone'], $texttag);
			$texttag		= 	str_replace("{main_workplace}",$row['main_workplace'], $texttag);
			$texttag		= 	str_replace("{current_post}",$row['current_post'], $texttag);
			$texttag		= 	str_replace("{years_in_post}",$row['years_in_post'], $texttag);
			$texttag		= 	str_replace("{patient_care_time}",$row['patient_care_time'], $texttag);
			$texttag		= 	str_replace("{gender}",(($row['gender']=='M') ? 'Male' : 'Female'), $texttag);
			$texttag		= 	str_replace("{year_of_birth}",$row['year_of_birth'], $texttag);
			$texttag		= 	str_replace("{lead}",$row['lead'], $texttag);
			$texttag		= 	str_replace("{accept_site_emails}",(($row['accept_site_emails']==1) ? 'Yes' : 'No'), $texttag);
			$texttag		= 	str_replace("{accept_bb_emails}",(($row['accept_bb_emails']==1) ? 'Yes' : 'No'), $texttag);

			$subtag			=	$subject;

			//SEND MAIL USING SWIFT LIBRARY
			$message = Swift_Message::newInstance()

			->setCharset('iso-8859-2')

			//Give the message a subject
			->setSubject($subtag)

			//Set the From address with an associative array
			->setFrom(array('hq@palliativedrugs.com' => 'Palliative Drugs'))

			//Set the To addresses with an associative array
			->setTo(array($row['email']))

			//Give it a body
			->setBody($texttag)

			//And optionally an alternative body
			->addPart($bodytag, 'text/html');

			$transport = Swift_MailTransport::newInstance();

			$mailer = Swift_Mailer::newInstance($transport);

			$result = $mailer->send($message);
		}
		// re-direct...
		header('Location:  /reg-thankyou.html');
	} else {
		header('Location:  /prefs-updated.html');
	}
}	
?>
<style>
.enabled_button {
	color : #000000
}
.disabled_button {
	color : #cccccc
}
</style>
<script type="text/javascript" language="javascript">
function link_enable() {				
	// ver 1.4
	link = document.getElementById('submit_button');
	chk = document.getElementById('tc_chk');

	if(chk.checked == true) {
		// enable submit link
		link.setAttribute("class", "enabled_button");
		link.setAttribute("className", "enabled_button");
		link.onclick = null;			
	} else {
		//disable submit link
		link.setAttribute("class", "disabled_button");
		link.setAttribute("className", "disabled_button");
		link.onclick = function(){return false;};
	}
}

function checkOrgType(){
	if(!document.getElementById('NotProfit').checked && !document.getElementById('Profit').checked){
		alert("Please select organisation type");
		return false;
	}
}
</script>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Login information</td>
</tr>
<tr>
	<td>Username</td>
	<td><?=$row['username'] ?></td>
</tr>
<tr>
	<td>E-mail address</td>
	<td><input type="text" name="email" size="30" value="<?=$email?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Password (leave blank to keep current password)</td>
	<td><input type="password" name="pswd3" size="30" value="" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Re-enter password</td>
	<td><input type="password" name="pswd2" size="30" value="" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Professional contact information</td>
</tr>
<tr>
	<td>First name</td>
	<td><input type="text" name="firstname" size="30" value="<?=$firstname?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Last name</td>
	<td><input type="text" name="lastname" size="30" value="<?=$lastname?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Title</td>
	<td>
		<select name="title" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getTitle($title);?>
		</select>
	</td>
</tr>
<!-- <tr>
	<td>Professional group</td>
	<td>
		<select name="role" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getRole($role);?>
		</select>		
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="role_details" size="30" value="<?=$role_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<?if($is_nurse) { ?>
<tr>
	<td>Are you a nurse prescriber</td>
	<td>
		<select name="prescriber" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getBool($prescriber);?>
		</select>		
	</td>
</tr>
<?} ?> -->
<? if(!$is_student) { ?>
<!-- <tr>
	<td>Professional registration number with your regulatory body/organization</td>
	<td><input type="text" name="prof_reg_num" size="30" value="<?=$prof_reg_num?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>

<tr>
	<td>Primary or main specialty</td>
	<td>
		<select name="primary" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getSpeciality($specialty);?>
		</select>
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="specialty_details" size="30" value="<?=$specialty_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
<tr>
	<td>Sub-specialty</td>
	<td>
		<select name="sub_specialty" style="padding:0px; width:auto; font-size:1.2em;">
			<?=getSubspec($sub_specialty);?>
		</select>
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="sub_specialty_details" size="30" value="<?=$sub_specialty_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr> -->
<? } ?>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Organisation details</td>
</tr>
<tr>
	<td>Organisation name</td>
	<td><input type="text" name="organisation" size="30" value="<?=$organisation?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<!-- <tr>
	<td>Organisation type</td>
	<td>
		<table cellpadding="0" cellspacing="0" align="left" style="margin:0px;">
			<tr>
				<td><input type="radio" name="usertype" id="NotProfit" value="non-profit" <? if($row['UserType']=="non-profit"){ echo " checked"; } ?> style="maring-left:0px;" /></td>
				<td style="padding-top:5px;">Not for Profit</td>
			</tr>
			<tr>
				<td><input type="radio" name="usertype" id="Profit" value="for-profit" <? if($row['UserType']=="for-profit"){ echo " checked"; } ?> style="maring-left:0px;" /></td>
				<td style="padding-top:5px;">For Profit</td>
			</tr>
		</table>
	</td>
</tr> -->
<tr>
	<td>Work Address</td>
	<td>
		<input type="text" name="addr1" size="30" value="<?=$addr1?>" style="padding:0px; width:auto; font-size:1.2em;"><br />
		<input type="text" name="addr2" size="30" value="<?=$addr2?>" style="padding:0px; width:auto; font-size:1.2em;"><br />
		<input type="text" name="addr3" size="30" value="<?=$addr3?>" style="padding:0px; width:auto; font-size:1.2em;"><br />
		<input type="text" name="addr4" size="30" value="<?=$addr4?>" style="padding:0px; width:auto; font-size:1.2em;">
	</td>
</tr>
<tr>
	<td>City</td>
	<td><input type="text" name="city" size="30" value="<?=$city?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
<tr>
	<td>State/Province/County</td>
	<td><input type="text" name="state" size="30" value="<?=$state?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
<tr>
	<td>Zip/Postal Code</td>
	<td><input type="text" name="zip" size="30" value="<?=$zip?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Country</td>
	<td>
		<select name="country" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getCountry($country);?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Telephone Number (Inc. International/Area code)</td>
	<td><input type="text" name="phone" size="30" value="<?=$phone?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<!-- <tr>
	<td>Main workplace</td>
	<td>
		<select name="main_workplace" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getMainworkplace($main_workplace);?>
		</select>
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="main_workplace_details" size="30" value="<?=$main_workplace_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr> -->
<tr>
	<td>Current post/position</td>
	<td>
		<select name="current_post" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getCurrentpost($current_post);?>
		</select>
	</td>
</tr>
<!-- <tr>
	<td>Details (if other)</td>
	<td><input type="text" name="current_post_details" size="30" value="<?=$current_post_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr> -->
<?if(!$is_student) { ?>
<!-- <tr>
	<td colspan="2" bgcolor="#CCCCCC">Demographic details</td>
</tr>
<tr>
	<td>Years in current post</td>
	<td>
		<select name="years_in_post" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getYearsinpost($years_in_post);?>
		</select>		
	</td>
</tr>
<tr>
	<td>Time spent in direct patient care</td>
	<td>
		<select name="time_spent" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getTimespent($time_spent);?>
		</select>
	</td>
</tr>
<tr>
	<td>Gender</td>
	<td>
		<select name="gender" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getGender($gender);?>
		</select>
	</td>
</tr>
<tr>
	<td>Year of birth</td>
	<td>
		<select name="year_of_birth" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getYearofbirth($year_of_birth);?>
		</select>
	</td>
</tr> -->
<? } ?>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Account preferences</td>
</tr>
<tr>
	<td>Accept e-mails relating to site updates</td>
	<td>
		<select name="accept_site_emails" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getBool($accept_update_emails);?>
		</select>
	</td>
</tr>
<tr>
	<td>Accept bulletin board digest e-mails</td>
	<td>
		<select name="accept_bb_emails" style="padding:0px; width:auto; font-size:1.2em;">
		<?=getBool($accept_bb_emails);?>
		</select>
	</td>
</tr>
<?if($reg_confirm) { ?>
<tr><td colspan="2" bgcolor="#cccccc">Confirm your registration</td></tr>
<tr>
	<td colspan="2">
	To submit your registration form please tick the box to confirm you have read, understood and accept our website <a href="/terms-and-conditions.html" target="_blank">Terms and conditions</a> of use.
	</td>
<tr>
	<td colspan="2" align="center">
		<br />
		<input type="checkbox" name="tc_chk" id="tc_chk" onclick="link_enable();" style="padding:0px; width:auto; font-size:1.2em;">&nbsp;I agree to the website Terms and conditions of use.<br /><br />
	</td>
</tr>
<tr>
	<td align="center">
		<input type="submit" id="submit_button" value="Confirm registration" style="padding:0px; width:auto; font-size:1.2em;">
	</td>
</tr>
<? } else { ?>
<tr>
	<td align="center">
		<input type="submit" value="Update" style="padding:0px; width:auto; font-size:1.2em;">
	</td>
</tr>

<? } ?>
</table>
</form>
<script language="Javascript">
link_enable();
</script>