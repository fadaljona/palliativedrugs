<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta http-equiv="cache-control" content="no-cache, must-revalidate" />
<meta http-equiv="pragma" content="no-cache" />
<?php
	$meta_title		=	"&copy; palliativedrugs.com";
	if(($_SESSION['site_language']=='en' && isSubscriber($_SESSION['visitor']->id))||($_SESSION['site_language']=='de')){
		if($_GET['ftitle']!='index'){
			$selFormulary	=	open_SQL("SELECT f.formulary_id,f.formulary_title,f.alias FROM 3bit_formulary f,3bit_language l WHERE 1 AND f.STATUS ='1' AND LOWER(l.language_code)='".$_GET['site_language']."' AND f.language_id=l.language_id");	
			if(mysql_num_rows( $selFormulary )>0){
				while($frow		=	mysql_fetch_array($selFormulary)){
					$formtitle	=	stripslashes($frow['formulary_title']);
					$faliastitle=	stripslashes($frow['alias']);	
					//echo urldecode($_GET['ftitle'])."===".remSpecial($formtitle)."===".$faliastitle."====".$frow['formulary_id']."<br />";		
					if((remSpecial($formtitle)==$_GET['ftitle']) || (remSpecial($faliastitle)==$_GET['ftitle'])){
						$meta_title		=	"Formulary &ndash; ".$formtitle;
					}
				}
			}
		}
	}
?>
<title><?php echo $meta_title; ?></title>
<?php if($_GET['page']=='formulary'){ ?> 
<link type="text/css" media="screen" rel="stylesheet" href="/min/g=monographs?v=1" /> 
<? }else {?> 
<link type="text/css" media="screen" rel="stylesheet" href="/min/g=formularymenu" /> 
<? } ?>
<!--This script should appear below your LINK stylesheet tags -->
<script language="javascript" type="text/javascript" src="/min/g=articleheader"></script>
<script language="javascript" type="text/javascript" src="/min/g=treemenu"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8576276-1']);
  _gaq.push(['_setDomainName', 'palliativedrugs.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
