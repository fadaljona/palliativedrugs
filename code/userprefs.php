<?php
// user preferences
//NOTE: this page isn't 'wrapped' - what are the implications for db access ? 

session_name('palliativedrugs');
session_start();

if(!IsSet($_SESSION['visitor']->id)) {
	if(!IsSet($_SESSION['new_user_id'])) {
		header('Location:  /access-denied.html');
	} else {
		$user_id = $_SESSION['new_user_id'];
	}
} else {
	$user_id = $_SESSION['visitor']->id;
}

if($_SESSION['reg_confirm']=='true') $reg_confirm = true;

require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/pall_db.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/code/function_lib.inc.php');

if($_SERVER['REQUEST_METHOD']=='GET') {
	// populate fields from database...
	$sql = "SELECT * FROM new_users WHERE id=" . $user_id;
	$rs = $pall_db->query($sql);
	$row = $pall_db->getRow($rs);
	$email	= $row['email'];
	$pswd	= $row['pwd'];
	$firstname	= $row['FirstName'];
	$lastname	= $row['LastName'];
	$title	= $row['title'];
	$role	= trim($row['role']);
	if(in_list($role,'reg_role')) {
		$role_details	= '';
	} else {
		$role = 'other';
		$role_details	= $row['role'];
	}
	$prescriber	= $row['nurse_prescriber'];
	if($prescriber==1) { $prescriber = 'yes'; } else { $prescriber = 'no'; }
	$prof_reg_num	= $row['prof_reg_num'];
	$specialty	= $row['speciality'];
	if(in_list($specialty,'reg_speciality')) {
		$specialty_details	= '';
	} else {
		$specialty = 'other';
		$specialty_details	= $row['speciality'];
	}		
	$sub_specialty	= $row['sub_specialty'];
	if(in_list($sub_specialty,'reg_sub_speciality')) {
		$sub_specialty_details	= '';
	} else {
		$sub_specialty = 'other';
		$sub_specialty_details	= $row['sub_specialty'];
	}	
	$organisation	= $row['organisation'];
	$addr1	= $row['addr1'];
	$addr2	= $row['addr2'];
	$addr3	= $row['addr3'];
	$addr4	= $row['addr4'];
	$city	= $row['TownOrCity'];
	$state	= $row['StateOrCounty'];
	$zip	= $row['PostalCode'];
	$country	= $row['Country'];
	$phone	= $row['phone'];
	$main_workplace	= $row['main_workplace'];
	if(in_list($main_workplace,'main_workplace')) {
		$main_workplace_details	= '';
	} else {
		$main_workplace = 'Other';
		$main_workplace_details	= $row['main_workplace'];
	}
	$current_post	= $row['current_post'];
	if(in_list($current_post,'current_post')) {
		$current_post_details	= '';
	} else {
		$current_post = 'Other';
		$current_post_details	= $row['current_post'];
	}
	$years_in_post	= $row['years_in_post'];	
	$time_spent	= $row['patient_care_time'];
	$gender	= $row['gender'];
	$year_of_birth	= $row['year_of_birth'];
	$accept_update_emails	= $row['accept_site_emails'];
	if($accept_update_emails==1) { $accept_update_emails = 'yes'; } else { $accept_update_emails = 'no'; }
	$accept_bb_emails		= $row['accept_bb_emails'];
	if($accept_bb_emails==1) { $accept_bb_emails = 'yes'; } else { $accept_bb_emails = 'no'; }
	
	//setup bools for optional content...
	$is_nurse = FALSE;
	$is_student = FALSE;
	switch($role) {
		case 'Nurse': 
			$is_nurse=TRUE; 
			break;
		case 'Student doctor':
		case 'Student nurse':
		case 'Student pharmacist':
		case 'Student other':
			$is_student = TRUE;
			break;
	}	
} else {
	// it's a POST so error_check & save...
	$prescriber = ($_POST['prescriber']=='yes') ? '1' : '0';
	$site_emails = ($_POST['accept_site_emails']=='yes')?'1':'0';
	$bb_emails = ($_POST['accept_bb_emails']=='yes')?'1':'0';	
	
	if($_POST['role']=='other') {
		$role = $_POST['role_details'];
	} else {
		$role = $_POST['role'];
	}
	if($_POST['primary']=='other') {
		$speciality = $_POST['specialty_details'];
	} else {
		$speciality = $_POST['primary'];
	}
	
	if($_POST['sub_specialty']=='other') {
		$sub_speciality = $_POST['sub_specialty_details'];
	} else {
		$sub_speciality = $_POST['sub_specialty'];
	}
	if($_POST['main_workplace']=='other') {
		$main_workplace = $_POST['main_workplace_details'];
	} else {
		$main_workplace = $_POST['main_workplace'];
	}
	if($_POST['current_post']=='other') {
		$current_post = $_POST['current_post_details'];
	} else {
		$current_post = $_POST['current_post'];
	}
	if($_POST['lead']=='other') {
		$lead = $_POST['lead_details'];
	} else {
		$lead = $_POST['lead'];
	}
	
	if(IsSet($_POST['years_in_post'])) {
		$years_in_post = $_POST['years_in_post'];
	} else {
		$years_in_post = 0;
	}
	if(strlen($_POST['pswd3'])) {
		$pwd_sql = "pwd='" . $_POST['pswd3'] . "', ";
	} else {
		$pwd_sql = "";
	}
	
	$sql = "UPDATE new_users SET "
					. "email='".$_POST['email']."', "
					. $pwd_sql
					. "FirstName='".$_POST['firstname']."', "
					. "LastName='".$_POST['lastname']."', "
					. "title='".$_POST['title']."', "
					. "role='".$role."', "
					. "prof_reg_num='".$_POST['prof_reg_num']."', "
					. "nurse_prescriber=". $prescriber . ", "
					. "speciality='".$speciality."', "
					. "sub_specialty='".$sub_speciality."', "
					. "organisation='".$_POST['organisation']."', "
					. "addr1='".$_POST['addr1']."', "
					. "addr2='".$_POST['addr2']."', "
					. "addr3='".$_POST['addr3']."', "
					. "addr4='".$_POST['addr4']."', "
					. "TownOrCity='".$_POST['city']."', "
					. "StateOrCounty='".$_POST['state']."', "
					. "PostalCode='".$_POST['zip']."', "
					. "Country='".$_POST['country']."', "
					. "main_workplace='".$main_workplace."', "
					. "current_post='".$current_post."', "
					. "years_in_post=".$years_in_post.", "
					. "patient_care_time='".$_POST['time_spent']."', "
					. "gender='".$_POST['gender']."', "
					. "year_of_birth='".$_POST['year_of_birth']."', "
					. "accept_site_emails=". $site_emails .", "
					. "accept_bb_emails=". $bb_emails  .", "
					. "user_confirmed=1 "
					. "WHERE id=" . $user_id;
	$pall_db->query($sql);	
	if($reg_confirm) {		
		unset($_SESSION['reg_confirm']);
		// auto-login...
		$sql = "UPDATE new_users SET current_visit = FROM_UNIXTIME(" . strtotime('now') . ") WHERE id = "  . $user_id;
		$pall_db->query($sql);
		$sql = "UPDATE new_users SET previous_visit = FROM_UNIXTIME(" . strtotime('now') . ") WHERE id = "  . $user_id;
		$pall_db->query($sql);
		$_SESSION['visitor']->id = $user_id;
		$_SESSION['visitor']->acl = 'user';
		$_SESSION['visitor']->logged_in = TRUE;
		$_SESSION['previous_visit'] = date("Y-m-d");
		
		//send welcome email
		$sql = "SELECT * FROM new_users WHERE id=" . $user_id;
		$rs = $pall_db->query($sql);
		$row = $pall_db->getRow($rs);		
//		$chunkArr = array();
		$params['username'] = 'Username: ' . $row['username'];
		$params['email'] = 'Email: ' . $row['email'];
		$params['password'] = 'Password: ' . $row['pwd'];
		$params['firstname'] = $row['FirstName'];
		$params['lastname'] = $row['LastName'];
		$params['title'] = $row['title'];
		$params['role'] = $row['role'];
		
		if(strpos($params['role'],'Student')===false) {
			$params['prof_reg_num'] = 'Professional registration number: ' . $row['prof_reg_num'];
			if($params['role']=='Nurse') {
				$params['nurse_prescriber'] = 'Nurse prescriber: ' . (($row['nurse_prescriber']==1) ? 'Yes' : 'No');
			} else {
				$params['nurse_prescriber'] = '';
			}
			$params['speciality'] = 'Specialty: ' . $row['speciality'];
			$params['sub_specialty'] = 'Sub-specialty: ' . $row['sub_specialty'];
		} else {
			$params['prof_reg_num'] = '';
			$params['nurse_prescriber'] = '';
			$params['speciality'] = '';
			$params['sub_specialty'] = '';
		}
					
		$params['organisation'] = 'organisation= ' . $row['organisation'];
		$params['addr1'] = 'Address 1: ' . $row['addr1'];
		$params['addr2'] = 'Address 2: ' . $row['addr2'];
		$params['addr3'] = 'Address 3: ' . $row['addr3'];
		$params['addr4'] = 'Address 4: ' . $row['addr4'];
		$params['TownOrCity'] = 'Town/City: ' . $row['TownOrCity'];
		$params['StateOrCounty'] = 'State/County: ' . $row['StateOrCounty'];
		$params['PostalCode'] = 'Postcod: ' . $row['PostalCode'];
		$params['Country'] = 'Country: ' . $row['Country'];
		$params['phone'] = 'Phone: ' . $row['phone'];
		$params['main_workplace'] = 'Main workplace: ' . $row['main_workplace'];
		$params['current_post'] = 'Current post: ' . $row['current_post'];
		
		if(strpos($params['role'],'Student')===false) {
			$params['years_in_post'] = 'Years in post: ' . $row['years_in_post'];
			$params['patient_care_time'] = 'Patient care time: ' . $row['patient_care_time'];
			$params['gender'] = 'Gender: ' . (($row['gender']=='M') ? 'Male' : 'Female');
			$params['year_of_birth'] = 'Year of birth: ' . $row['year_of_birth'];
		} else {
			$params['years_in_post'] = '';
			$params['patient_care_time'] = '';
			$params['gender'] = '';
			$params['year_of_birth'] = '';
		}
		
		$params['lead'] = 'How did you hear about us: ' . $row['lead'];
		$params['accept_site_emails'] = 'Accept e-mails relating to site updates: ' . (($row['accept_site_emails']==1) ? 'Yes' : 'No');
		$params['accept_bb_emails'] = 'Accept bulletin board digest e-mails: ' . (($row['accept_bb_emails']==1) ? 'Yes' : 'No');
			
		$msg = $modx->parseChunk('email_welcome', $params, '[+', '+]');		
		$headers = 'From: webmaster@palliativedrugs.com' . "\r\n" .
		    'Reply-To: webmaster@palliativedrugs.com' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();		
		mail($row['email'], "Welcome to palliativedrugs.com", $msg, $headers);

		// re-direct...
		header('Location:  /reg-thankyou.html');
	} else {
		header('Location:  /prefs-updated.html');
	}
}	
?>
<style>
.enabled_button {
	color : #000000
}
.disabled_button {
	color : #cccccc
}
</style>
<script type="text/javascript" language="javascript">
function link_enable() {				
	// ver 1.4
	link = document.getElementById('submit_button');
	chk = document.getElementById('tc_chk');

	if(chk.checked == true) {
		// enable submit link
		link.setAttribute("class", "enabled_button");
		link.setAttribute("className", "enabled_button");
		link.onclick = null;			
	} else {
		//disable submit link
		link.setAttribute("class", "disabled_button");
		link.setAttribute("className", "disabled_button");
		link.onclick = function(){return false;};
	}
}
</script>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Login information</td>
</tr>
<tr>
	<td>Username</td>
	<td><?=$row['username'] ?></td>
</tr>
<tr>
	<td>E-mail address</td>
	<td><input type="text" name="email" size="30" value="<?=$email?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Password (leave blank to keep current password)</td>
	<td><input type="password" name="pswd3" size="30" value="" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Re-enter password</td>
	<td><input type="password" name="pswd2" size="30" value="" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Professional contact information</td>
</tr>
<tr>
	<td>First name</td>
	<td><input type="text" name="firstname" size="30" value="<?=$firstname?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Last name</td>
	<td><input type="text" name="lastname" size="30" value="<?=$lastname?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Title</td>
	<td>
		<select name="title" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=reg_title&sel=<?=$title;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Professional group</td>
	<td>
		<select name="role" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=reg_role&sel=<?=$role;?>]]
		</select>		
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="role_details" size="30" value="<?=$role_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<?if($is_nurse) { ?>
<tr>
	<td>Are you a nurse prescriber</td>
	<td>
		<select name="prescriber" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=yes_no&sel=<?=$prescriber;?>]]
		</select>		
	</td>
</tr>
<?} ?>
<?if(!$is_student) { ?>
<tr>
	<td>Professional registration number with your regulatory body/organization</td>
	<td><input type="text" name="prof_reg_num" size="30" value="<?=$prof_reg_num?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>

<tr>
	<td>Primary or main specialty</td>
	<td>
		<select name="primary" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=reg_speciality&sel=<?=$specialty;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="specialty_details" size="30" value="<?=$specialty_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
<tr>
	<td>Sub-specialty</td>
	<td>
		<select name="sub_specialty" style="padding:0px; width:auto; font-size:1.2em;">
			[[generate_options?&chunk=reg_sub_speciality&sel=<?=$sub_specialty;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="sub_specialty_details" size="30" value="<?=$sub_specialty_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<? } ?>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Organisation details</td>
</tr>
<tr>
	<td>Organisation name</td>
	<td><input type="text" name="organisation" size="30" value="<?=$organisation?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Work Address</td>
	<td>
		<input type="text" name="addr1" size="30" value="<?=$addr1?>" style="padding:0px; width:auto; font-size:1.2em;"><br />
		<input type="text" name="addr2" size="30" value="<?=$addr2?>" style="padding:0px; width:auto; font-size:1.2em;"><br />
		<input type="text" name="addr3" size="30" value="<?=$addr3?>" style="padding:0px; width:auto; font-size:1.2em;"><br />
		<input type="text" name="addr4" size="30" value="<?=$addr4?>" style="padding:0px; width:auto; font-size:1.2em;">
	</td>
</tr>
<tr>
	<td>City</td>
	<td><input type="text" name="city" size="30" value="<?=$city?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
<tr>
	<td>State/Province/County</td>
	<td><input type="text" name="state" size="30" value="<?=$state?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
<tr>
	<td>Zip/Postal Code</td>
	<td><input type="text" name="zip" size="30" value="<?=$zip?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Country</td>
	<td>
		<select name="country" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=countries&sel=<?=$country;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Telephone Number (Inc. International/Area code)</td>
	<td><input type="text" name="phone" size="30" value="<?=$phone?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Main workplace</td>
	<td>
		<select name="main_workplace" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=main_workplace&sel=<?=$main_workplace;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="main_workplace_details" size="30" value="<?=$main_workplace_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<tr>
	<td>Current post/position</td>
	<td>
		<select name="current_post" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=current_post&sel=<?=$current_post;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Details (if other)</td>
	<td><input type="text" name="current_post_details" size="30" value="<?=$current_post_details;?>" style="padding:0px; width:auto; font-size:1.2em;"></td>
</tr>
<?if(!$is_student) { ?>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Demographic details</td>
</tr>
<tr>
	<td>Years in current post</td>
	<td>
		<select name="years_in_post" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=years_in_post&sel=<?=$years_in_post;?>]]
		</select>		
	</td>
</tr>
<tr>
	<td>Time spent in direct patient care</td>
	<td>
		<select name="time_spent" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=time_spent&sel=<?=$time_spent;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Gender</td>
	<td>
		<select name="gender" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=gender&sel=<?=$gender;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Year of birth</td>
	<td>
		<select name="year_of_birth" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=year_of_birth&sel=<?=$year_of_birth;?>]]
		</select>
	</td>
</tr>
<? } ?>
<tr>
	<td colspan="2" bgcolor="#CCCCCC">Account preferences</td>
</tr>
<tr>
	<td>Accept e-mails relating to site updates</td>
	<td>
		<select name="accept_site_emails" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=yes_no&sel=<?=$accept_update_emails;?>]]
		</select>
	</td>
</tr>
<tr>
	<td>Accept bulletin board digest e-mails</td>
	<td>
		<select name="accept_bb_emails" style="padding:0px; width:auto; font-size:1.2em;">
		[[generate_options?&chunk=yes_no&sel=<?=$accept_bb_emails;?>]]
		</select>
	</td>
</tr>
<?if($reg_confirm) { ?>
<tr><td colspan="2" bgcolor="#cccccc">Confirm your registration</td></tr>
<tr>
	<td colspan="2">
	To submit your registration form please tick the box to confirm you have read, understood and accept our website <a href="terms-and-conditions.html">Terms and conditions</a> of use.
	</td>
<tr>
	<td colspan="2" align="center">
		<br />
		<input type="checkbox" name="tc_chk" id="tc_chk" onclick="link_enable();" style="padding:0px; width:auto; font-size:1.2em;">&nbsp;I agree to the website Terms and conditions of use.<br /><br />
	</td>
</tr>
<tr>
	<td align="center">
		<input type="submit" id="submit_button" value="Confirm registration" style="padding:0px; width:auto; font-size:1.2em;">
	</td>
</tr>
<? } else { ?>
<tr>
	<td align="center">
		<input type="submit" value="Update" style="padding:0px; width:auto; font-size:1.2em;">
	</td>
</tr>

<? } ?>
</table>
</form>
<script language="Javascript">
link_enable();
</script>