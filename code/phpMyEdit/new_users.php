<?php
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/config.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/db_func.inc.php');
include($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/form_func.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/visitor.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/UserInfo.class.php');
session_name('palliativedrugs');
session_start();
$allowed="admin,user";
require_once($_SERVER['DOCUMENT_ROOT'] . '/legacy/_include/secure.inc.php');

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
		"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Users (new)</title>
	 <link href="phpme.css" type="text/css" rel="stylesheet" />
</head>
<body>
<h3>Users (new)</h3>
<?php

/*
 * IMPORTANT NOTE: This generated file contains only a subset of huge amount
 * of options that can be used with phpMyEdit. To get information about all
 * features offered by phpMyEdit, check official documentation. It is available
 * online and also for download on phpMyEdit project management page:
 *
 * http://platon.sk/projects/main_page.php?project_id=5
 *
 * This file was generated by:
 *
 *                    phpMyEdit version: 5.7.1
 *       phpMyEdit.class.php core class: 1.204
 *            phpMyEditSetup.php script: 1.50
 *              generating setup script: 1.50
 */

// MySQL host name, user name, password, database, and table
$opts['hn'] = 'localhost';
$opts['un'] = 'pall_dbuser-dev';
$opts['pw'] = 'xe97BRAXuFrUChe9';
$opts['db'] = 'pall_formulary';
$opts['tb'] = 'new_users';

// Name of field which is the unique key
$opts['key'] = 'id';

// Type of key field (int/real/string/date etc.)
$opts['key_type'] = 'int';

// Sorting field(s)
$opts['sort_field'] = array('id');

// Number of records to display on the screen
// Value of -1 lists all records in a table
$opts['inc'] = 10;

// Options you wish to give the users
// A - add,  C - change, P - copy, V - view, D - delete,
// F - filter, I - initial sort suppressed
$opts['options'] = 'ACPVDF';

// Number of lines to display on multiple selection filters
$opts['multiple'] = '4';

// Navigation style: B - buttons (default), T - text links, G - graphic links
// Buttons position: U - up, D - down (default)
$opts['navigation'] = 'UDBG';

// Display special page elements
$opts['display'] = array(
	'form'  => true,
	'query' => true,
	'sort'  => true,
	'time'  => false,
	'tabs'  => true
);

// Set default prefixes for variables
$opts['js']['prefix']               = 'PME_js_';
$opts['dhtml']['prefix']            = 'PME_dhtml_';
$opts['cgi']['prefix']['operation'] = 'PME_op_';
$opts['cgi']['prefix']['sys']       = 'PME_sys_';
$opts['cgi']['prefix']['data']      = 'PME_data_';

/* Get the user's default language and use it if possible or you can
   specify particular one you want to use. Refer to official documentation
   for list of available languages. */
$opts['language'] = $_SERVER['HTTP_ACCEPT_LANGUAGE'] . '-UTF8';

/* Table-level filter capability. If set, it is included in the WHERE clause
   of any generated SELECT statement in SQL query. This gives you ability to
   work only with subset of data from table.

$opts['filters'] = "column1 like '%11%' AND column2<17";
$opts['filters'] = "section_id = 9";
$opts['filters'] = "PMEtable0.sessions_count > 200";
*/

/* Field definitions
   
Fields will be displayed left to right on the screen in the order in which they
appear in generated list. Here are some most used field options documented.

['name'] is the title used for column headings, etc.;
['maxlen'] maximum length to display add/edit/search input boxes
['trimlen'] maximum length of string content to display in row listing
['width'] is an optional display width specification for the column
          e.g.  ['width'] = '100px';
['mask'] a string that is used by sprintf() to format field output
['sort'] true or false; means the users may sort the display on this column
['strip_tags'] true or false; whether to strip tags from content
['nowrap'] true or false; whether this field should get a NOWRAP
['select'] T - text, N - numeric, D - drop-down, M - multiple selection
['options'] optional parameter to control whether a field is displayed
  L - list, F - filter, A - add, C - change, P - copy, D - delete, V - view
            Another flags are:
            R - indicates that a field is read only
            W - indicates that a field is a password field
            H - indicates that a field is to be hidden and marked as hidden
['URL'] is used to make a field 'clickable' in the display
        e.g.: 'mailto:$value', 'http://$value' or '$page?stuff';
['URLtarget']  HTML target link specification (for example: _blank)
['textarea']['rows'] and/or ['textarea']['cols']
  specifies a textarea is to be used to give multi-line input
  e.g. ['textarea']['rows'] = 5; ['textarea']['cols'] = 10
['values'] restricts user input to the specified constants,
           e.g. ['values'] = array('A','B','C') or ['values'] = range(1,99)
['values']['table'] and ['values']['column'] restricts user input
  to the values found in the specified column of another table
['values']['description'] = 'desc_column'
  The optional ['values']['description'] field allows the value(s) displayed
  to the user to be different to those in the ['values']['column'] field.
  This is useful for giving more meaning to column values. Multiple
  descriptions fields are also possible. Check documentation for this.
*/

$opts['fdd']['id'] = array(
  'name'     => 'ID',
  'select'   => 'T',
  'options'  => 'AVPDRL', // auto increment
  'maxlen'   => 11,
  'default'  => '0',
  'sort'     => true,
);

$opts['fdd']['username'] = array(
  'name'     => 'Username',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'tab' => 'login info',
	'options'  => 'ACPVDFL'
);
$opts['fdd']['email'] = array(
  'name'     => 'Email',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
'options'  => 'ACPVDFL'
);
$opts['fdd']['pwd'] = array(
  'name'     => 'Pwd',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
'options'  => 'ACPVDF'
);

$opts['fdd']['FirstName'] = array(
  'name'     => 'FirstName',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
'options'  => 'ACPVDFL'
);
$opts['fdd']['LastName'] = array(
  'name'     => 'LastName',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
'options'  => 'ACPVDFL'
);
$opts['fdd']['title'] = array(
  'name'     => 'Title',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
'options'  => 'ACPVDFL'
);

$opts['fdd']['role'] = array(
  'name'     => 'Role',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'tab' => 'Professional Info',
	'options'  => 'ACPVDFL'
);
$opts['fdd']['prof_reg_num'] = array(
  'name'     => 'Prof reg num',
  'select'   => 'T',
  'maxlen'   => 128,
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['nurse_prescriber'] = array(
  'name'     => 'Nurse prescriber',
  'select'   => 'T',
  'maxlen'   => 4,
  'default'  => '0',
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['speciality'] = array(
  'name'     => 'Speciality',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true
);
$opts['fdd']['sub_specialty'] = array(
  'name'     => 'Sub specialty',
  'select'   => 'T',
  'maxlen'   => 128,
  'sort'     => true,
	'options'  => 'ACPVDF'
);

$opts['fdd']['organisation'] = array(
  'name'     => 'Organisation',
  'select'   => 'T',
  'maxlen'   => 255,
  'sort'     => true,
'tab' => 'Organisation',
	'options'  => 'ACPVDFL'
);
$opts['fdd']['addr1'] = array(
  'name'     => 'Addr1',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['addr2'] = array(
  'name'     => 'Addr2',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['addr3'] = array(
  'name'     => 'Addr3',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['addr4'] = array(
  'name'     => 'Addr4',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['TownOrCity'] = array(
  'name'     => 'TownOrCity',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['StateOrCounty'] = array(
  'name'     => 'StateOrCounty',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['PostalCode'] = array(
  'name'     => 'PostalCode',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['Country'] = array(
  'name'     => 'Country',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['phone'] = array(
  'name'     => 'Phone',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDF'
);

$opts['fdd']['main_workplace'] = array(
  'name'     => 'Main workplace',
  'select'   => 'T',
  'maxlen'   => 255,
  'sort'     => true,
'tab' => 'Demographic Info',
	'options'  => 'ACPVDF'
);
$opts['fdd']['current_post'] = array(
  'name'     => 'Current post',
  'select'   => 'T',
  'maxlen'   => 128,
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['years_in_post'] = array(
  'name'     => 'Years in post',
  'select'   => 'T',
  'maxlen'   => 11,
  'default'  => '0',
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['patient_care_time'] = array(
  'name'     => 'Patient care time',
  'select'   => 'T',
  'maxlen'   => 10,
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['gender'] = array(
  'name'     => 'Gender',
  'select'   => 'T',
  'maxlen'   => 1,
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['year_of_birth'] = array(
  'name'     => 'Year of birth',
  'select'   => 'T',
  'maxlen'   => 4,
  'sort'     => true,
	'options'  => 'ACPVDF'
);
$opts['fdd']['lead'] = array(
  'name'     => 'Lead',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
'tab' => 'Account Info',
	'options'  => 'ACPVDF'
);

$opts['fdd']['accept_site_emails'] = array(
  'name'     => 'update emails',
  'select'   => 'T',
  'maxlen'   => 1,
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['accept_bb_emails'] = array(
  'name'     => 'Accept bb emails',
  'select'   => 'T',
  'maxlen'   => 1,
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['UserType'] = array(
  'name'     => 'User<br />Type',
  'select'   => 'T',
  'maxlen'   => 65535,
  'textarea' => array(
    'rows' => 5,
    'cols' => 50),
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['DateRegistered'] = array(
  'name'     => 'Date<br />Registered',
  'select'   => 'T',
  'maxlen'   => 19,
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['RegistrationChecked'] = array(
  'name'     => 'Reg<br />Checked',
  'select'   => 'T',
  'maxlen'   => 1,
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['active'] = array(
  'name'     => 'Active',
  'select'   => 'T',
  'maxlen'   => 1,
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['vetted'] = array(
  'name'     => 'Vetted',
  'select'   => 'T',
  'maxlen'   => 3,
  'default'  => '0',
  'sort'     => true,
	'options'  => ''
);
$opts['fdd']['previous_visit'] = array(
  'name'     => 'Previous visit',
  'select'   => 'T',
  'maxlen'   => 10,
  'sort'     => true,
	'options'  => 'ACPVDFL'
);
$opts['fdd']['current_visit'] = array(
  'name'     => 'Current visit',
  'select'   => 'T',
  'maxlen'   => 10,
  'sort'     => true,
	'options'  => ''
);
$opts['fdd']['user_confirmed'] = array(
  'name'     => 'User confirmed',
  'select'   => 'T',
  'maxlen'   => 4,
  'default'  => '0',
  'sort'     => true,
	'options'  => 'ACPVDFL'
);

// Now important call to phpMyEdit
require_once 'phpMyEdit.class.php';
new phpMyEdit($opts);

?>


</body>
</html>