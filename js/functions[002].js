$(document).ready(function () {
	// For shop related functions ===================================================

	if($('#editship').val()=='' || $('#editship').val()=='0'){
		$('#alter_delivery').attr('checked', false);
		$('.shipping').filter('.hidden').hide();
	}

	$('.button').filter('.checkout-cart').click(function(){
			validate_billing(document.frmBilling);
	});

	$(".highlight").filter('.warning').hide();

	$('.button').filter('.payment-cart').click(function(){
	  if($('#terms').attr('checked')==true){
		$(this).parent().parent().find(".highlight").filter('.warning').hide();
		document.frmSummary.action='worldpay.html';
		document.frmSummary.submit();
	  }else{
		$(this).parent().parent().find(".highlight").filter('.warning').show();
	  }
	});

	$('.button').filter('.proforma-cart').click(function(){
		if($('#terms').attr('checked')==true){
			$(this).parent().parent().find(".highlight").filter('.warning').hide();
			document.frmSummary.action='proforma.html';
			document.frmSummary.submit();
		}else{
			$(this).parent().parent().find(".highlight").filter('.warning').show();
		}
	});

	// While changing the billing country //
	$('#bcountry').change(function(){			
		var country_id	=	$(this).val();
		if($('#alter_delivery').attr('checked')==true && $('#dcountry').val()!='0'){
			country_id	=	$('#dcountry').val();
		}
		if(country_id != '0')
			get_cartitems(country_id,$('#check_sessid').val());
	});

	// While changing the delivery country//
	$('#dcountry').change(function(){			
		var country_id	=	$(this).val();
		if(country_id != '0')
			get_cartitems(country_id,$('#check_sessid').val());
	});

	$('.button').filter('.pay-cart').click(function(){
		validate_payment('#frmCheckout');
	});

	var overlay_api 	=	$(".shoppingcart a[rel]").overlay({
		expose: 'darkred',
		effect: 'apple',
		api: true,
		onBeforeLoad: function() {				 
			// grab wrapper element inside content
			var wrap = this.getContent().find(".contentWrap");
			// load the page specified in the trigger
			wrap.load(this.getTrigger().attr("href"));
		}

	});


});


function init(){
	var stretchers = document.getElementsByClassName('stretcher'); //div that stretches
	var toggles = document.getElementsByClassName('displayer'); //h3s where I click on
	//accordion effect
	var myAccordion = new fx.Accordion(
		toggles, stretchers, {opacity: true, duration: 400}
	);
	//hash function
	function checkHash(){
		var found = false;
		toggles.each(function(h2, i){
			if (window.location.href.indexOf(h2.title)>0){
				myAccordion.showThisHideOpen(stretchers[i]);
				found = true;
			}
		});
		return found;
	}
	if (!checkHash()) myAccordion.showThisHideOpen(stretchers[-1]);
}

function HideLeft() {
	var l = document.getElementById('leftcolumn');
	var m = document.getElementById('contentcolumn');
	var g = document.getElementById('hideleft');
	if(l.style.display=='none') {
		//show...
		l.style.display='block';
		m.style.margin='20px 15px 10px 230px';
		g.value='hide';
	} else {
		//hide
		l.style.display='none';
		m.style.margin='20px 15px 10px 15px';
		g.value='show';
	}
}

// Function to block alphabets //

function blockNumbers(e){
	var key;
	var keychar;
	var reg;			 
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		key = e.keyCode;
	}else if(e.which) {
		// netscape
		key = e.which;
	}else {
		// no event, so pass through
		return true;
	}
	keychar = String.fromCharCode(key);
	
	if ( (key>=48 &&  key<=57) || (key==46) || (key==44)|| (key==45) || (key==34) || (key==39) || (key==32) || (key == 8) || (key == 43) || (key == 41) || (key == 40)){
		return true;
	}else{
		return false;
	}
}

///////////////   To trim the string ////////////////////
function trim(str){
	return str.replace(/^\s*|\s*$/g,"");
}

//////////// Validate email ///////////////////////
var remail=/^([_&a-zA-Z0-9-]+(\.[_&a-zA-Z0-9-]+)*@[&a-zA-Z0-9-]+\.+[&a-zA-Z0-9-]+)/;
function checkEmail(fieldvalue){
	if(remail.test(fieldvalue))
		return false;
	else
		return true;
}

// Function Add product to cart ------------------------------------

function addprodtocart(formid){			
	var ran_unrounded=Math.random()*100000;
	var ran_number=Math.floor(ran_unrounded); 
	form_params	=	$('#'+formid).serialize();	
	$.ajax({
	   type: "POST",
	   url: "/includes/ajax_functions.php",
	   data: 'action=addtocart&'+form_params+"&s=" +ran_number,
		   success: function(response){
			 var getOP	 =	response.split("^");
			 if(getOP[1]	==	'S'){
				window.location.href=window.location.href;	
			 }    
		  }
	 });	
}

// Function Update product in cart ------------------------------------

function updatetocart(frm,sessid){  
  if ((sessid == null) || (sessid == "")) return;    
    var ran_unrounded	=	Math.random()*100000;
	var ran_number		=	Math.floor(ran_unrounded); 	
	var cartid			=	frm.cart_id.value;
	var	ids				=	cartid.split(",");
	if($('#alter_delivery').attr('checked')==true && $('#dcountry').val()!='0'){
		var bcontry		=	$('#dcountry').val();
	}else
		var	bcontry		=	$('#bcountry').val();
	var qutys			=	'';
	for(var i=0;i<ids.length-1;i++){
		qtyname	=	'quantity'+ids[i];		
		qtval	=	$("#"+qtyname).val();
		if(qtval!='')
			qtval	=	qtval;
		else
			qtval	=	0;
		qutys	+=	qtval+",";
	}		
	var url_params	=	"cartid=" + escape(cartid)+"&qutys=" + escape(qutys)+"&sessid=" +escape(sessid)+"&action=updatetocart&country_id="+bcontry+"&s="+ran_number;
	$.ajax({
		type	:	'POST',
		url		:	'/includes/ajax_functions.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 		
			var OParr	=	msg.split("^");
			if(OParr[1]	==	"S"){				
				$('.cart').html(OParr[0]);									
			}
		}
	});
}

// Function delete product in cart ------------------------------------
function deletefromcart(cartid,sessid){
	if ((sessid == null) || (sessid == "")) return;    
    var ran_unrounded	=	Math.random()*100000;
	var ran_number		=	Math.floor(ran_unrounded); 	
	var url_params	=	"cartid=" + escape(cartid)+"&sessid=" +escape(sessid)+"&action=deletefromcart&s="+ran_number;
	$.ajax({
		type	:	'POST',
		url		:	'/includes/ajax_functions.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 		
			var OParr	=	msg.split("^");
			if(OParr[1]	==	"S"){			
				$('.cart').html(OParr[0]);									
			}
		}
	});
} 

function toggleDelivery(){
	$('.shipping').toggle(400);	
}

function validate_billing(frm){
	var flag		= true;
	var del_params	= '';
	var message		= '';
	var delmsg		= '';
	if($('#bfirst').val()==''){		
		message += '<li>Please insert first name</li>';
		$('#biname').addClass('error');
		flag = false;
	}else
		$('#biname').removeClass('error');
	if($('#blast').val()==''){
		message += '<li>Please insert last name</li>';
		$('#biname').addClass('error');
		flag = false;
	}else
		$('#biname').removeClass('error');

	if($('#bemail').val()=='' || ( $('#bemail').val()!='' && checkEmail($('#bemail').val()))){
		message += '<li>Please insert valid email</li>';
		$('#biemail').addClass('error');
		flag = false;
	}else
		$('#biemail').removeClass('error');

	if($('#baddress1').val()==''){
		message += '<li>Please insert address</li>';
		$('#biaddr').addClass('error');
		flag = false;
	}else
		$('#biaddr').removeClass('error');

	if($('#bcity').val()==''){
		message += '<li>Please insert city</li>';
		$('#biaddr').addClass('error');
		flag = false;
	}else
		$('#biaddr').removeClass('error');

	if($('#bzipcode').val()==''){
		message += '<li>Please insert zipcode</li>';
		$('#biaddr').addClass('error');
		flag = false;
	}else
		$('#biaddr').removeClass('error');	

	if($('#bphone').val()==''){
		message += '<li>Please enter contact number</li>';
		$('#bitel').addClass('error');
		flag = false;
	}else
		$('#bitel').removeClass('error');
	
	if($('#alter_delivery').attr('checked')==true){
		if($('#dfirst').val()==''){		
			delmsg += '<li>Please insert first name</li>';
			$('#shname').addClass('error');
			flag = false;
		}else
			$('#shname').removeClass('error');

		if($('#dlast').val()==''){
			delmsg += '<li>Please insert last name</li>';
			$('#shname').addClass('error');
			flag = false;
		}else
			$('#shname').removeClass('error');

		if($('#daddress1').val()==''){
			delmsg += '<li>Please insert address</li>';
			$('#shaddr').addClass('error');
			flag = false;
		}else
			$('#shaddr').removeClass('error');

		if($('#dcity').val()==''){
			delmsg += '<li>Please insert city</li>';
			$('#shaddr').addClass('error');
			flag = false;
		}else
			$('#shaddr').removeClass('error');

		if($('#dzipcode').val()==''){
			delmsg += '<li>Please insert zipcode</li>';
			$('#shaddr').addClass('error');
			flag = false;
		}else
			$('#shaddr').removeClass('error');
		
		if($('#dphone').val()==''){
			delmsg += '<li>Please enter contact number</li>';
			$('#shtel').addClass('error');
			flag = false;
		}else
			$('#shtel').removeClass('error');	
	}

	var params	 =	$('#frmBilling').serialize();		
	if(flag==true){				
		$('#billing').find('.error').html('');
		$('#shipping').find('.error').html('');
		register_customer(params,$('#check_sessid').val());
	}else{
		if(message != ''){
			$('#billing').find('p.error').html('<strong>There were problem(s) with your submission</strong><ul>'+message+'</ul>');
		}else
			$('#billing').find('.error').html('');
		if(delmsg != ''){
			$('#shipping').find('p.error').html('<strong>There were problem(s) with your submission</strong><ul>'+delmsg+'</ul>');
		}else
			$('#shipping').find('.error').html('');
		return flag;
	}
}

// Function to register customers with billing and shipping details//

function register_customer(params,sessid){
	if ((sessid == null) || (sessid == "")) return;    
    var ran_unrounded	=	Math.random()*100000;
	var ran_number		=	Math.floor(ran_unrounded); 
	var action			=	'register_customer';
	var url_params		=	params+"&action=" +escape(action)+"&sessid=" +escape(sessid)+"&s="+ran_number;
	$.ajax({
		type	:	'POST',
		url		:	'/includes/ajax_functions.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 	
			if(msg=="S"){
				if($('#cart_id')){
					document.frmBilling.action='/shop/summary.html';
					document.frmBilling.submit();
				}else{
					alert('Your cart is empty!');
				}
			}
		}
	});
}

// Function to validate shipping details highslide//

function validate_shipping(inv){	
	flag = true;	
	var msg	='';
	if($(inv).find('#btitle').val()==""){	
		msg += '<li>Please choose title</li>';		
		flag=false;
		$('#invname').addClass('error');
	}else
		$('#invname').removeClass('error');

	if($(inv).find('#firstname').val()==""){
		msg += '<li>Please enter your first name</li>';		
		flag=false;
		$('#invname').addClass('error');
	}else
		$('#invname').removeClass('error');

	if($(inv).find('#lastname').val()==""){
		msg += '<li>Please enter your last name</li>';			
		flag=false;
		$('#invname').addClass('error');
	}else
		$('#invname').removeClass('error');

	if($(inv).find('#address1').val()==""){
		msg += '<li>Please enter address1</li>';			
		flag=false;	
		$('#invadd').addClass('error');
	}else
		$('#invadd').removeClass('error');

	if($(inv).find('#icountry').val()==""){
		msg += '<li>Please select country</li>';	
		flag=false;
		$('#invadd').addClass('error');
	}else
		$('#invadd').removeClass('error');

	if($(inv).find('#phone').val()==""){
		msg += '<li>Please enter phone number</li>';		
		flag=false;
		$('#invtel').addClass('error');
	}else
		$('#invtel').removeClass('error');
	
	if(flag == true){
		$(inv).find('.error').html('');
		$(inv).find('.error').hide();
		var params	=$(inv).serialize();				
		update_shipping(params);
	}else{
		if(msg != ''){		
			$(inv).find('p.error').show();
			$(inv).find('p.error').html('<strong>There were problem(s) with your submission</strong><ul>'+msg+'</ul>');
		}else{
			$(inv).find('p.error').hide();
			$(inv).find('p.error').html('');
		}
		return false;
	}
	
}

// Function to update customers with billing and shipping details//

function update_shipping(params){
	if ((params == null) || (params == "")) return;    
    var ran_unrounded	=	Math.random()*100000;
	var ran_number		=	Math.floor(ran_unrounded); 
	var action			=	'update_shipping';
	var url_params		=	params+"&action=" +escape(action)+"&s="+ran_number;
	$.ajax({
		type	:	'POST',
		url		:	'/includes/ajax_functions.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 	
			var OPArr	=	msg.split("^");
			if(OPArr[2]=="S"){
				$('#invadd').html(OPArr[0]);
				$('#deladd').html(OPArr[1]);
				if(OPArr[3]!=''){						
					$('.cart_table').html(OPArr[3]);					
					$('.button').filter('.payment-cart').click(function(){
						if($('#terms').attr('checked')==true){
							$(this).parent().parent().find(".highlight").filter('.warning').hide();
							document.frmSummary.action='worldpay.html';
							document.frmSummary.submit();
						}else{
							$(this).parent().parent().find(".highlight").filter('.warning').show();
						}
					});
					$('.button').filter('.proforma-cart').click(function(){
						if($('#terms').attr('checked')==true){
							$(this).parent().parent().find(".highlight").filter('.warning').hide();
							document.frmSummary.action='proforma.html';
							document.frmSummary.submit();
						}else{
							$(this).parent().parent().find(".highlight").filter('.warning').show();
						}
					});
				}
				$('p').removeClass('error').addClass('highlight').html('Your details have been successfully updated!').show();
				$('.shoppingcart').find(".highlight").filter('.warning').hide();
			}else if(OPArr[2]=="F"){
				$('p').removeClass('error').addClass('highlight').html('Please try again!').show();
			}
		}
	});

}

function get_cartitems(country_id,sessid){
	if ((sessid == null) || (sessid == "")) return;    
    var ran_unrounded	=	Math.random()*100000;
	var ran_number		=	Math.floor(ran_unrounded); 
	var action			=	'country_cart';
	var url_params	=	"country_id=" + escape(country_id)+"&action=" +escape(action)+"&sessid=" +escape(sessid)+"&s="+ran_number;
	$.ajax({
		type	:	'POST',
		url		:	'/includes/ajax_functions.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 	
			var OPArr	=	msg.split("^");
			if(OPArr[1]=='S')
			$('.copy').filter('.wide').html(OPArr[0]);			
		}
	});
}