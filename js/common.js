/**

 * COMMON DHTML FUNCTIONS

 * These are handy functions I use all the time.

 *

 * By Seth Banks (webmaster at subimage dot com)

 * http://www.subimage.com/

 *

 * Up to date code can be found at http://www.subimage.com/dhtml/

 *

 * This code is free for you to use anywhere, just keep this comment block.

 */



/**

 * X-browser event handler attachment and detachment

 * TH: Switched first true to false per http://www.onlinetools.org/articles/unobtrusivejavascript/chapter4.html

 *

 * @argument obj - the object to attach event to

 * @argument evType - name of the event - DONT ADD "on", pass only "mouseover", etc

 * @argument fn - function to call

 */

function addEvent(obj, evType, fn){

 if (obj.addEventListener){

    obj.addEventListener(evType, fn, false);

    return true;

 } else if (obj.attachEvent){

    var r = obj.attachEvent("on"+evType, fn);

    return r;

 } else {

    return false;

 }

}

function removeEvent(obj, evType, fn, useCapture){

  if (obj.removeEventListener){

    obj.removeEventListener(evType, fn, useCapture);

    return true;

  } else if (obj.detachEvent){

    var r = obj.detachEvent("on"+evType, fn);

    return r;

  } else {

    alert("Handler could not be removed");

  }

}



/**

 * Code below taken from - http://www.evolt.org/article/document_body_doctype_switching_and_more/17/30655/

 *

 * Modified 4/22/04 to work with Opera/Moz (by webmaster at subimage dot com)

 *

 * Gets the full width/height because it's different for most browsers.

 */

function getViewportHeight() {

	if (window.innerHeight!=window.undefined) return window.innerHeight;

	if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;

	if (document.body) return document.body.clientHeight; 



	return window.undefined; 

}

function getViewportWidth() {

	var offset = 17;

	var width = null;

	if (window.innerWidth!=window.undefined) return window.innerWidth; 

	if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth; 

	if (document.body) return document.body.clientWidth; 

}


function ResizeViewPort(n,mode){ 
	if (!n || !mode || document.layers) return null;
	var f=(document.getElementById)? document.getElementById(n): document.all[n]; 
	var doc = (f && f.contentDocument)? f.contentDocument:f.Document; 
	w = (doc.body.scrollWidth)? doc.body.scrollWidth:doc.body.offsetWidth;
	h = (doc.body.scrollHeight)? doc.body.scrollHeight:doc.body.offsetHeight;
	if(mode==1||mode==3) f.style.width = w;
	if(mode==2||mode==3) f.style.height = h;
}




/**

 * Gets the real scroll top

 */

function getScrollTop() {

	if (self.pageYOffset) // all except Explorer

	{

		return self.pageYOffset;

	}

	else if (document.documentElement && document.documentElement.scrollTop)

		// Explorer 6 Strict

	{

		return document.documentElement.scrollTop;

	}

	else if (document.body) // all other Explorers

	{

		return document.body.scrollTop;

	}

}

function getScrollLeft() {

	if (self.pageXOffset) // all except Explorer

	{

		return self.pageXOffset;

	}

	else if (document.documentElement && document.documentElement.scrollLeft)

		// Explorer 6 Strict

	{

		return document.documentElement.scrollLeft;

	}

	else if (document.body) // all other Explorers

	{

		return document.body.scrollLeft;

	}

}



///////////////   To trim the string ////////////////////

function trim(str){

	return str.replace(/^\s*|\s*$/g,"");

}


//////////////// Function to update the content in expose //////////////////
function updateDesc(inviteid){
	if((inviteid=='') || (inviteid == null))return false;
	var ran_unrounded	=	Math.random()*100000;
	var ran_number		=	Math.floor(ran_unrounded); 
	var url_params		=	"inviteid="+inviteid+"&action=update_desc&s="+ran_number;
    $.ajax({
		type	:	'POST',
		url		:	'/ajax.php',
		data	:	url_params,
		error	:	function(){alert('Error loading document!');},
		success	:	function(msg){ 				
			if(trim(msg)!=""){				
				out	=	msg.split("^");
				if(out[0]!='')
					$('#e1').find('h3:eq(0)').html(out[0]);
				if(out[1]!='')
					$('#e1').find('p:eq(0)').html(out[1]);
			}
		}
	});

}



//////////////// Function to update Member /////////////////////////////////



function updateMember(pdmemid,inviteid,status){

   if((pdmemid=='') || (status == ''))return false;

	var ran_unrounded	=	Math.random()*100000;

	var ran_number		=	Math.floor(ran_unrounded); 

	var url_params		=	"status=" + escape(status)+"&pdmemid=" +escape(pdmemid)+"&inviteid="+inviteid+"&action=update_member&s="+ran_number;	

	$.ajax({

		type	:	'POST',

		url		:	'/ajax.php',

		data	:	url_params,

		error	:	function(){alert('Error loading document!');},

		success	:	function(msg){ 							

			if(trim(msg)!=""){

				response	=	msg.split("^");				

				if(response[1]=='yes'){

					$('#e1').hide();	

					$.mask.close();

					if(response[0]!=''){

						//window.open(response[0],'Survey Page','width=400,height=200');

						window.location=response[0];

					}		

				}else if(response[1]=='later' || response[1]=='never'){

					$('#e1').hide();	

					$.mask.close();

				}

				

			}else{

				$('.appnitro').find('.error').html('Please try again!').show('slow');				

			}

		}

	});	

}

/////////////////// Function to change the language //////////////////////////

function assign_language(lang){
	if(lang!=''){
		$('#set_lang').val(lang);
		$('#frmlang').submit();
	}
}

function HideLeft() {
	var l = document.getElementById('leftcolumn');
	var m = document.getElementById('contentcolumn');
	var g = document.getElementById('hideleft');
	if(l.style.display=='none') {
		//show...
		l.style.display='block';
		m.style.margin='20px 15px 10px 230px';
		g.value='Hide Formulary Menu';
	} else {
		//hide
		l.style.display='none';
		m.style.margin='20px 15px 10px 15px';
		g.value='Show Formulary Menu';
	}
}

$(document).ready(function() {

$(".slidetabs").tabs(".hpbanners > div", {

	// enable "cross-fading" effect
	effect: 'fade',
	fadeOutSpeed: "slow",

	// start from the beginning after the last tab
	rotate: true

// use the slideshow plugin. It accepts its own configuration
}).slideshow({autoplay:true,interval:10000});
});

/*function validate_update(){	
	if($('#NotProfit').attr('checked')!='checked' && $('#Profit').attr('checked')!='checked'){
		$('div .block').addClass('warning');
		return false;
	}else{
		$('div .block').removeClass('warning');
		var ran_unrounded	=	Math.random()*100000;
		var ran_number		=	Math.floor(ran_unrounded); 
		var url_params		=	$('#frmType').serialize()+"&action=update_usertype&s="+ran_number;	
		$.ajax({
			type	:	'POST',
			url		:	'/includes/ajax_functions.php',
			data	:	url_params,
			error	:	function(){alert('Error loading document!');},
			success	:	function(msg){ 							
				if(trim(msg)!=""){								
					if(msg=='S'){
						$('#custom').hide();	
						$("#custom").overlay().close();							
					}else if(msg=='F'){
						//$('#custom').hide();	
						//api2.close();
					}
					
				}
			}
		});		
	}
}*/

var pwdregex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/;
function checkPwd(fieldvalue){
	if(pwdregex.test(fieldvalue))
		return false;
	else
		return true;
}

function validate_update(){	
	if(checkPwd($('#password').val())){
		$('div .block').addClass('warning');
		alert('aaa');
		return false;
	}else if($('#password').val()!=$('#cpassword').val()){
		$('div .block').addClass('warning');
        alert('aaa');
        return false;
	}else{
		$('div .block').removeClass('warning');
		var ran_unrounded	=	Math.random()*100000;
		var ran_number		=	Math.floor(ran_unrounded); 
		var url_params		=	$('#frmType').serialize()+"&action=update_usertype&s="+ran_number;	
		$.ajax({
			type	:	'POST',
			url		:	'/includes/ajax_functions.php',
			data	:	url_params,
			error	:	function(){alert('Error loading document!');},
			success	:	function(msg){ 							
				if(trim(msg)!=""){								
					if(msg=='S'){
						$('#custom').hide();	
						$("#custom").overlay().close();							
					}else if(msg=='F'){
						//$('#custom').hide();	
						//api2.close();
					}
					
				}
			}
		});		
	}
}

function validate_orgtype(){	
	if($('#NotProfit').attr('checked')!=true && $('#Profit').attr('checked')!=true){
		alert('Please select organisation type');
		$('#NotProfit').focus();
		return false;
	}

}